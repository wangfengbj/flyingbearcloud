package pers.flyingbear.config;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * info构建器
 */
@Component
public class ServerInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        // 此处可以读取数据库
        builder.withDetail("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
    }
}
