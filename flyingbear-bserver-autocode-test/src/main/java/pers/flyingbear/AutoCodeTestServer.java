package pers.flyingbear;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;

@SpringBootApplication
public class AutoCodeTestServer {
    public static void main(String[] args) {
        //SpringApplication.run(AutoCodeTestServer.class, args);
        SpringApplication app = new SpringApplication(AutoCodeTestServer.class);
        app.setBannerMode(Banner.Mode.OFF); // 关闭logo输出
        app.setApplicationStartup(new BufferingApplicationStartup(2048)); // 启用追踪，/actuator/startup查看
        app.run(args);
    }
}
