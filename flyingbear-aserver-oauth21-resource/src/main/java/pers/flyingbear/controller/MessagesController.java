package pers.flyingbear.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息控制器
 */
@RestController
public class MessagesController {
	@GetMapping("/messages")
	public String[] getMessages() {
		return new String[] { "Message 1", "Message 2", "Message 3" };
	}
}