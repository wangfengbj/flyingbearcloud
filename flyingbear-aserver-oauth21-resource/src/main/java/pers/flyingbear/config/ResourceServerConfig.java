package pers.flyingbear.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

/**
 * 资源服务配置
 */
@EnableWebSecurity
public class ResourceServerConfig {

	protected final ResourceAuthExceptionEntryPoint resourceAuthExceptionEntryPoint;

	public ResourceServerConfig(ResourceAuthExceptionEntryPoint resourceAuthExceptionEntryPoint) {
		this.resourceAuthExceptionEntryPoint = resourceAuthExceptionEntryPoint;
	}

	// @formatter:off
	@Bean
	SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
		http.mvcMatcher("/messages/**").authorizeRequests()
				.mvcMatchers("/messages/**").access("hasAnyAuthority('SCOPE_message.read','SCOPE_openid')")
				.and().oauth2ResourceServer(
					oauth -> oauth.authenticationEntryPoint(resourceAuthExceptionEntryPoint)
				)
				.oauth2ResourceServer().jwt(); // jwt的资源服务器
		return http.build();
	}
	// @formatter:on
}
