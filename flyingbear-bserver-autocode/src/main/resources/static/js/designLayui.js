// +----------------------------------------------------------------------
// | 表单设计中间页面元素，使用layui
// +----------------------------------------------------------------------
layui.define(['cascader', 'tags'],function (exports) {
    "use strict";
    let $ = layui.$, tags = layui.tags, cascader = layui.cascader, layer = layui.layer;
    let class_item = "layui-form-item"; // 组件外层div样式
    let class_label = "layui-form-label"; // label样式
    let class_hide = "layui-hide"; // 隐藏样式
    let red_start = "<span style=\"color:red;\">* </span>";
    let class_inline = "layui-input-inline";
    let class_block = "layui-input-block";

    let obj = {
        /**
         * 控件外层包裹的div
         * @param data
         * @returns {string}
         */
        getBeforeHtml: function(data){
            let html = '<div id="' + data.div_id + '" class="'+class_item+'" data-index="' + data.index + '" data-tag="' + data.tag + '"';
            if (data.width && data.width != 100) { html += 'style="width:' + data.width + '%"'; }
            html += '>';
            return html;
        },
        /**
         * 控件前部label部分
         * @param labelHide
         * @param data
         * @param labelInline
         * @returns {string}
         */
        getBeforeLabel: function (labelHide = false, data, labelInline = false){
            let labelHtml = '<label class="'+class_label;
            if (labelHide) { labelHtml += ' ' + class_hide; }
            labelHtml += '"';
            if (data.labelWidth && data.labelWidth != 110) { labelHtml += 'style="width:' + data.labelWidth + 'px;"'; }
            labelHtml += '>';
            if (data.required) { labelHtml += red_start; }
            labelHtml += data.label + '</label>';
            if(labelInline){ labelHtml += '<div class="'+class_inline+'"';
            }else{ labelHtml += '<div class="'+class_block+'"'; }
            let styleCSS = [];
            if (labelHide) { styleCSS.push('margin-left:0'); }
            if (data.labelWidth && data.labelWidth != 110) {
                let lWidth = Number(data.labelWidth) + 30;
                styleCSS.push('margin-left:' + lWidth + 'px');
            }
            styleCSS = styleCSS.join(';');
            if (styleCSS) { labelHtml += 'style="' + styleCSS + '"'; }
            labelHtml += '>';
            return labelHtml;
        },
        /**
         * 控件部分html代码
         * @param data
         */
        getHtml: function(data){
            let html;
            if(data.tag === "input"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input type="' + data.type + '" name="' + data.name + '" class="layui-input"';
                html += ' value="' + data.default + '" placeholder="' + data.placeholder + '" ';
                if (data.type == 'number') { if (data.min) {html += ' min="' + data.min + '"';} if (data.max) {html += ' max="' + data.max + '"';}
                } else { if (data.maxlength) {html += ' maxlength="' + data.maxlength + '"';} }
                if (data.required) {html += ' lay-verify="required"';}
                if (data.readonly) {html += ' readonly=""';} // 只读
                if (data.disabled) {html += ' disabled=""';}
                if (data.verify) {html += ' lay-verify="' + data.verify + '"';}
                html += '>';
                html+="</div></div>";
            }else if(data.tag === "textarea"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<textarea name="' + data.name + '" class="layui-textarea" placeholder="' + data.placeholder + '"';
                if (data.required) {html += ' lay-verify="required"';}
                if (data.readonly) {html += ' readonly=""';}
                if (data.disabled) {html += ' disabled=""';}
                if (data.maxlength && data.maxlength != 255) {html += ' maxlength="' + data.maxlength + '"';}
                html += '></textarea>';
                html += '</div></div>';
            }else if(data.tag === "radio"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                if (data.options.length) {
                    for (let index = 0; index < data.options.length; index++) {
                        let elem = data.options[index];
                        html += '<input type="radio" name="' + data.name + '" value="' + elem.value + '" title="' + elem.title + '"';
                        if (elem.checked) { html += ' checked=""'; }
                        if (data.disabled) { html += ' disabled=""'; }
                        html += '>';
                    }
                }
                html += '</div></div>';
            }else if(data.tag === "checkbox"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                if (data.options.length) {
                    for (let index = 0; index < data.options.length; index++) {
                        let elem = data.options[index];
                        let name = data.name;
                        if (data.name) { name += '[' + elem.value + ']'; }
                        html += '<input type="checkbox" name="' + name + '" lay-skin="' + data.lay_skin + '" value="' + elem.value + '" title="' + elem.title + '"';
                        if (elem.checked) { html += ' checked=""'; }
                        if (data.disabled) { html += ' disabled=""'; }
                        html += '>';
                    }
                }
                html += '</div></div>';
            }else if(data.tag === "select"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<select name="' + data.name + '"';
                if (data.lay_search) { html += ' lay-search=""'; }
                if (data.required) { html += ' lay-verify="required"'; }
                if (data.disabled) { html += ' disabled=""'; }
                html += '>';
                html += '<option value="">请选择</option>';
                if (data.options.length) {
                    for (let index = 0; index < data.options.length; index++) {
                        let elem = data.options[index];
                        html += '<option value="' + elem.value + '">' + elem.title + '</option>';
                    }
                }
                html += '</select>';
                html += '</div></div>';
            }else if(data.tag === "selectDb"){ // 请求下拉框
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<select name="' + data.name + '"';
                if (data.lay_search) { html += ' lay-search=""'; }
                if (data.required) { html += ' lay-verify="required"'; }
                if (data.disabled) { html += ' disabled=""'; }
                html += '>';
                html += '<option value="">请选择</option>';
                html += '</select>';
                html += '</div></div>';
            }else if(data.tag === "date"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input type="text" name="' + data.name + '" class="layui-input" placeholder="' + data.placeholder + '" lay-datetime=""';
                if (data.data_range) { html += ' data-range="true"'; }
                if (data.data_dateType) { html += ' data-dateType="' + data.data_dateType + '"';  }
                if (data.data_dateformat) { html += ' data-dateformat="' + data.data_dateformat + '"'; }
                if (data.data_maxvalue) { html += ' data-maxvalue="' + data.data_maxvalue + '"'; }
                if (data.data_minvalue) { html += ' data-minvalue="' + data.data_minvalue + '"'; }
                if (data.required) { html += ' lay-verify="required"'; }
                if (data.disabled) { html += ' disabled=""'; }
                if (data.readonly) { html += ' readonly=""'; }
                html += '>';
                html += '</div></div>';
            }else if(data.tag ===  "colorPicker"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input class="layui-input layui-hide" name="' + data.name + '"/>';
                html += '<div lay-colorpicker="' + data.name + '" data-value="' + data.default + '"></div>';
                html += '</div></div>';
            }else if(data.tag === "slider"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input class="layui-input layui-hide" name="' + data.name + '"/>';
                html += '<div class="lay-slider" lay-slider="'+ data.name +'" data_default="' + data.data_default + '" data-theme="' + data.data_theme + '"';
                if (data.data_showStep) { html += ' data-showstep="' + data.data_showStep + '"'; }
                if (data.data_step) { html += ' data-step="' + data.data_step + '"'; }
                if (data.data_max) { html += ' data-max="' + data.data_max + '"'; }
                if (data.data_min) { html += ' data-min="' + data.data_min + '"'; }
                if (data.data_input) { html += ' data-input="' + data.data_input + '"'; }
                if (data.disabled) { html += ' disabled=""'; }
                if (data.readonly) { html += ' readonly=""'; }
                html += '></div>';
                html += '</div></div>';
            }else if(data.tag === "rate"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input class="layui-input layui-hide" name="' + data.name + '"/>';
                html += '<div lay-rate="' + data.name + '" data-default="' + data.data_default + '" data-class="' + data.name + '" data-theme="' + data.data_theme + '"';
                if (data.data_half) { html += ' data-half="true"'; }
                if (data.data_length) { html += ' data-length="' + data.data_length + '"'; }
                if (data.readonly) { html += ' data-readonly="true"'; }
                html += '></div>';
                html += '</div></div>';
            }else if(data.tag === "switch"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input type="checkbox" name="' + data.name + '" value="1" lay-skin="switch" />';
                html += '</div></div>';
            }else if(data.tag === "database"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data, true);
                html += '<input class="layui-input layui-hide" name="' + data.dbColumn + '"/>';
                html += '<input type="' + data.type + '" name="' + data.name + '" class="layui-input"';
                html += ' placeholder="' + data.placeholder + '" readonly=""';
                if (data.required) { html += ' lay-verify="required"'; }
                html += '></div>';
                html += '<div class="layui-form-mid" style="padding: 0!important;"><button type="button" class="layui-btn" lay-filter="'+data.name+'"><i class="layui-icon layui-icon-search"></i></button></div>';
                html+="</div>";
            }else if(data.tag === "subtraction"){
                html = this.getBeforeHtml(data);
                html += '<hr class="' + data.border + '">';
                html+="</div>";
            }else if(data.tag === "label"){
                html = this.getBeforeHtml(data);
                html += '<blockquote class="layui-elem-quote">' + data.text + '</blockquote>';
                html+="</div>";
            }else if(data.tag === "tips"){
                html = this.getBeforeHtml(data);
                html += '<div class="layui-input-inline" >';
                html += ' <i class="layui-icon layui-icon-about" lay-tips="' + data.msg + '" data-offset="' + data.offset + '" ></i>';
                html += '</div></div>';
            }else if(data.tag === "button"){
                html = this.getBeforeHtml(data);
                html += '<div class="layui-input-inline" >';
                html += '<button type="button" class="layui-btn ' + data.theme + ' ' + data.btnSize + '" >' + data.text + '</button> ';
                html += '</div></div>';
            }else if(data.tag === "space"){
                html = this.getBeforeHtml(data);
                html += '<div style="height:' + data.height + 'px;"></div>';
                html+="</div>";
            }else if(data.tag === "cascader"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input id="' + data.name + '" class="layui-input" data-parents="'+data.data_parents+'" data-value="'+data.data_value+'">';
                html += '</div></div>';
            }else if(data.tag === "editor"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<textarea id="' + data.name + '_tiny" name="' + data.name + '" class="layui-hide editorsrv" ';
                html += '></textarea>';
                html += '</div></div>';
            }else if(data.tag === "upload"){
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                switch (data.uploadType) {
                    case 'images':
                        html += '<input class="layui-input layui-input-upload ' + data.name + '" name="' + data.name + '" >';
                        html += '<button type="button" class="layui-btn" lay-choose="' + data.name + '" data-type="images" >';
                        html += '<i class="layui-icon layui-icon-windows"></i>选择</button>';
                        html += '<div class="clear"></div>';
                        html += '<div class="layui-upload-drag" lay-upload="' + data.name + '" data-type="images"';
                        html += 'data-accept="' + data.data_accept + '"';
                        html += 'data-size="' + data.data_size + '"';
                        html += '>';
                        html += '<i class="layui-icon"></i>';
                        html += '<p>点击上传，或将文件拖拽到此处</p>';
                        html += '<div class="layui-hide">';
                        html += '<hr>';
                        html += '<img src="" class="layui-upload-dragimg ' + data.name + '" alt="上传成功后渲染" >';
                        html += '<span class="layui-badge layui-upload-clear" >删除</span>';
                        html += '</div></div>';
                        break;
                    case 'multiple':
                        html += '<div class="layui-imagesbox">';
                        html += '<div class="layui-input-inline">';
                        html += '<div class="layui-upload-drag" lay-upload="' + data.name + '" data-type="multiple"';
                        html += 'data-accept="' + data.data_accept + '"';
                        html += 'data-size="' + data.data_size + '"';
                        html += '>';
                        html += '<i class="layui-icon layui-icon-upload"></i>';
                        html += '<p>点击上传，或将文件拖拽到此处</p>';
                        html += '<div class="layui-hide"></div>';
                        html += '</div>';
                        html += '<button type="button" class="layui-btn layui-btn-xs layui-btn-fluid"><i class="layui-icon layui-icon-windows"></i> 选择</button>';
                        html += '</div>';
                        html += '</div>';
                        break;
                    default:
                        html += '<input type="text" name="' + data.name + '" class="layui-input layui-input-upload ' + data.name + '"';
                        if (data.disabled) {
                            html += 'disabled=""';
                        }
                        if (data.required) {
                            html += 'lay-verify="required"';
                        }
                        html += 'data-accept="' + data.data_accept + '"';
                        html += 'data-size="' + data.data_size + '"';
                        html += '>';
                        html += '<button type="button" class="layui-btn" lay-upload="' + data.name + '" data-type="normal" >';
                        html += '<i class="layui-icon layui-icon-upload"></i>上传</button>';
                        html += '<button type="button" class="layui-btn ml10" lay-choose="' + data.name + '" data-type="normal" >';
                        html += '<i class="layui-icon layui-icon-windows"></i>选择</button>';
                        html += '</div></div>';
                        break;
                }
            }else if(data.tag === "grid"){
                html = '<div id="' + data.div_id + '" class="layui-form-item layui-row" data-index="' + data.index + '" data-tag="' + data.tag + '">';
                if (data.children.length) {
                    let col = 12 / data.column;
                    for (let index = 0; index < data.column; index++) {
                        let elem = data.children[index];
                        html += '<div class="layui-col-md' + col + ' layui-grid-' + index + ' children" data-index="' + index + '"></div>';
                    }
                }
                html += '</div>';
            }else if(data.tag === "tab"){
                html = this.getBeforeHtml(data);
                html += '<div id="' + data.name + '" class="layui-tab layui-tab-brief">';
                html += '<ul class="layui-tab-title">';
                let content = ""; let rid = Date.now();
                for (let key in data.options) {
                    let item = data.options[key]
                    html += '<li class="' + (item.checked ? 'layui-this' : '') + '">' + item.title + '</li>';
                    content += '<div id="saphp-'+rid+'-tab' + key + '" class="layui-tab-item ' + (item.checked ? 'layui-show ' : '') + 'children" data-index="' + key + '"></div>';
                }
                html += '</ul>';
                html += '<div class="layui-tab-content">';
                html += content;
                html += '</div></div>';
            }else if(data.tag === "tags"){ // 待处理
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<input type="text" lay-tags="" id="' + data.name + '" name="' + data.name + '" class="layui-input" >';
                html += '</div></div>';
            }else if(data.tag === "json"){// 待处理
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '<table class="layui-table" >';
                html += '<thead>';
                html += ' <tr>';
                html += '<th>名称</th>';
                html += '<th>变量值</th>';
                html += '<th>操作</th>';
                html += '</tr>';
                html += '</thead>';
                html += '<tbody>';
                html += '</tbody>';
                html += '</table>';
                html += '<button type="button" class="layui-btn layui-btn-normal layui-jsonvar-add" data-name="'+data.name+'" >追加</button>';
                html += '</div></div>';
            }else if(data.tag === "layout"){
                html = '<div id="'+data.div_id+'" class="fly-flex design-dw" style="'+data.divStyle+'" data-tag="' + data.tag + '">';
                html += '</div>';
            }else if(data.tag === "div"){
                html = '<div id="'+data.div_id+'" class="design-dw" style="'+data.divStyle+'" data-tag="' + data.tag + '">';
                html += '</div>';
            }else{
                html = this.getBeforeHtml(data) + this.getBeforeLabel(data.labelHide, data);
                html += '待开发</div></div>';
            }
            return html;
        },
        /**
         * 特殊组件渲染
         * @param tag
         * @param name
         */
        fieldRender: function(tag, name, element){
            let othat = this;
            switch (tag) {
                case 'date':
                    let datetime = $('*[name="'+name+'"][lay-datetime]');
                    datetime.each(function(key,obj) {
                        let t = $(obj).data('dateType') || 'datetime',
                            f = $(obj).data('dateformat') || 'yyyy-MM-dd HH:mm:ss',
                            r = $(obj).data('range') || false,
                            max = $(obj).data('maxvalue') || '2222-12-31',
                            min = $(obj).data('minvalue') || '1930-01-01';
                        layui.laydate.render({
                            elem: this, type: t, range: r, max: max, min: min, format :f
                            ,done: function(value, date, end_date) { // 选择完毕回调
                                console.log(value,date,end_date);
                            }
                        });
                    })
                    break;
                case 'colorPicker':
                    let picker = $('*[lay-colorpicker="'+name+'"]');
                    picker.each(function(index,elem){
                        layui.colorpicker.render({
                            elem: this, color: $('input[name="'+name+'"]').val()
                            ,predefine: true, alpha: true
                            ,done: function(color){ $('input[name="'+name+'"]').val(color); }
                        });
                    })
                    break;
                case 'slider':
                    let slider = $('*[lay-slider="'+name+'"]');
                    slider.each(function(index,elem){
                        let that = $(this),
                            type = that.data('type') || 'default',
                            min = that.data('min') || 0,
                            max = that.data('max') || 100,
                            theme = that.data('theme') || '#1890ff',
                            step = that.data('step') || 1,
                            input = that.data('input') || false,
                            showstep = that.data('showstep') || false;
                        // 获取滑块默认值
                        let value = $('input[name="'+name+'"]').val() || that.data('default');
                        layui.slider.render({
                            elem: elem, type: type, min: min, max: max, step: step, showstep: showstep
                            ,theme: theme, input: input, value: value
                            ,change: function(value) {
                                if (value <= min || isNaN(value)) { value = min; }
                                $('input[name="'+name+'"]').val(value);
                            }
                        })
                    })
                    break;
                case 'rate':
                    let rate = $('*[lay-rate="'+name+'"]');
                    rate.each(function(index,elem){
                        let that = $(this),
                            theme = that.data('theme') || '#1890ff',
                            length = that.data('length') || 5,
                            half = that.data('half') || false,
                            readonly = that.data('readonly') || false;
                        let el = $('input[name="'+name+'"]');
                        let value = el.val() || that.data('default');
                        layui.rate.render({
                            elem: elem, half: half, length: length, theme: theme, readonly: readonly,value: value
                            ,choose: function(value) { el.val(value); }
                        })
                    })
                    break;
                case 'database':
                    layer.msg('database特殊组件渲染开发中');
                    break;
                case 'upload':
                    layer.msg('上传组件请自行编写代码');
                    break;
                case 'tags':
                    layui.each($('.lay-tags'),function(i,e) { $(e).remove(); })
                    layui.each($('*[lay-tags]'),function(index,elem3) {
                        tags.render({
                            elem: elem3,
                            done: function(key,all) {}
                        });
                    })
                    break;
                case 'cascader': // 待处理
                    let elObj = [];
                    let caItem = 'input#' + element.name;
                    let value = $(caItem).val();
                    let propsValue = $(caItem).data('value') || 'value';
                    let parents = $(caItem).data('parents') || false;
                    if (typeof value != 'undefined' && value) {
                        value = value.split('/');
                        value = value[value.length - 1];
                        value = $.trim(value);
                    }

                    elObj[0] = cascader({
                        elem: caItem,
                        value: value,
                        clearable:true,
                        filterable: true,
                        showAllLevels: parents,
                        props: {
                            value: propsValue
                        },
                        options: cascader_data
                    });

                    elObj[0].changeEvent(function (value, node) {
                        if (node != null) {
                            if (parents == true) {
                                var arrpath = [];
                                for (const key in node.path) {
                                    var path = node.path[key].data;
                                    arrpath.push($.trim(path[propsValue]));
                                }
                                $(elem).val(arrpath.join('/'));
                            } else {
                                $(elem).val(node.data[propsValue]);
                            }
                        } else {
                            $(elem).val('');
                        }
                    });
                    break;
                case 'editor': // 待处理
                    tinymce.init({
                        selector: '#' + element.name + '_tiny',
                        language: 'zh_CN',
                    })
                    break;
                case 'tab':
                case 'grid':
                    let children = $('#' + element.div_id + ' .children');
                    let childItem = []
                    $.each(children, function (index, item) {
                        othat.gridtabSortable(item);
                        childItem[index] = item;
                    })
                    $.each(element.children, function (index, item) {
                        if (item.children.length) {
                            layui.designCommon.renderDesign(item.children, $(childItem[index]));
                        }
                    })
                    break;
                case 'layout':
                    this.layoutDivSortable(element.div_id);
                    let layout_children = element.children;
                    layui.designCommon.renderDesign(layout_children[0].children, $("#"+element.div_id), element.tag);
                    break;
                case 'div':
                    this.layoutDivSortable(element.div_id);
                    let div_children = element.children;
                    layui.designCommon.renderDesign(div_children[0].children, $("#"+element.div_id), element.tag);
                    break;
                default:
                    break;
            }
        },
        /**
         * 表格布局拖拽
         * @param itemId
         */
        gridtabSortable: function (itemId){
            new Sortable.create(itemId, {
                group: { name: "formDesign" },
                direction: 'vertical', // 拖拽方向 horizontal
                ghostClass: "sortableGhost",
                animation: 150,
                swapClass: 'highlight',
                filter: function (evt, item) {
                    return false;
                },
                onChoose: function (evt) { },
                onAdd: function (evt) {
                    // 表格内禁止嵌入表格
                    if ($(evt.item).data('tag') == 'tab') {
                        $(evt.item).remove();
                        layer.msg('禁止嵌套TAB', 'error');
                        return false;
                    }

                    let item = evt.item, id = $(item).attr('id'),
                        parentId = $(evt.item.parentElement.parentElement).attr('id'),
                        parentIndex = $(evt.item.parentElement).index();
                    if (typeof parentId == 'undefined') { parentId = $(evt.item).parents('.layui-tab').parent().attr('id'); }

                    // 查找栅格的ID
                    let gridElem = layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), parentId);
                    let data;
                    if (typeof id != 'undefined') { // 这里是把外部控件拖到tab中
                        data = JSON.parse(JSON.stringify(layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), id)));
                        //data.name = data.tag + '_' + data.index;
                        gridElem.children[parentIndex].children.splice(evt.newIndex, 0, data);
                        layui.designCommon.deleteElem(layui.designCommon.getComponentData(), data.name);
                    } else {
                        data = layui.designCommon.getField($(item).data('tag'));
                        //data.name = data.tag + '_' + data.index;
                        let rid = Date.now();
                        data.div_id = "div_" + rid;
                        data.id = "form_"+rid;
                        data.name = rid;
                        gridElem.children[parentIndex].children.splice(evt.newIndex, 0, data);
                    }
                    layui.designCommon.setSelData(data);
                    layui.designCommon.renderDesign();
                },
                onUpdate: function (evt) {
                    let parentId = $(evt.item.parentElement.parentElement).attr('id'),
                        parentIndex = evt.item.parentElement.dataset.index;
                    if (typeof parentId == 'undefined') {
                        parentId = $(evt.item).parents('.layui-tab').parent().attr('id');
                    }
                    let gridElem = layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), parentId);
                    let children = gridElem.children[parentIndex].children;
                    [children[evt.newIndex], children[evt.oldIndex]] = [children[evt.oldIndex], children[evt.newIndex]];
                    layui.designCommon.renderTree();
                },
                onEnd: function (evt) { }
            });
        },
        /**
         * 自定义布局拖拽
         * @param id
         */
        layoutDivSortable: function (id){
            Sortable.create(document.getElementById(id), {
                group: { name: "formDesign" },
                direction: 'horizontal', // 拖拽方向 horizontal  vertical
                ghostClass: "sortableGhost",
                animation: 150,
                swapClass: 'highlight',
                filter: function (evt, item) {
                    return false;
                },
                onChoose: function (evt) { },
                onAdd: function (evt) {
                    let p_tag = evt.to.dataset.tag;
                    // 表格内禁止嵌入表格
                    if (p_tag === "layout" && ($(evt.item).data('tag') == 'div' || $(evt.item).data('tag') == 'layout')) {
                        let item = evt.item, id = $(item).attr('id'), parentId = $(evt.item.parentElement).attr('id'),
                            parentIndex = $(evt.item.parentElement).index();

                        let elem = layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), parentId);
                        let data;
                        if (typeof id != 'undefined') { // 这里是把外部控件拖到tab中
                            data = JSON.parse(JSON.stringify(layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), id)));
                            //data.name = data.tag + '_' + data.index;
                            elem.children[0].children.splice(evt.newIndex, 0, data);
                            layui.designCommon.deleteElem(layui.designCommon.getComponentData(), data.name);
                        } else {
                            data = layui.designCommon.getField($(item).data('tag'));
                            //data.name = data.tag + '_' + data.index;
                            let rid = Date.now();
                            data.div_id = "div_" + rid;
                            data.id = "form_"+rid;
                            data.name = rid;
                            elem.children[0].children.splice(evt.newIndex, 0, data);
                        }
                        layui.designCommon.setSelData(data);
                        layui.designCommon.renderDesign();
                    }else if (p_tag === "div" && $(evt.item).data('tag') != 'div' && $(evt.item).data('tag') != 'layout'){
                        let item = evt.item, id = $(item).attr('id'), parentId = $(evt.item.parentElement).attr('id');

                        let elem = layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), parentId);
                        let data;
                        if (typeof id != 'undefined') { // 这里是把外部控件拖到tab中
                            data = JSON.parse(JSON.stringify(layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), id)));
                            //data.name = data.tag + '_' + data.index;
                            elem.children[0].children.splice(evt.newIndex, 0, data);
                            layui.designCommon.deleteElem(layui.designCommon.getComponentData(), data.name);
                        } else {
                            data = layui.designCommon.getField($(item).data('tag'));
                            //data.name = data.tag + '_' + data.index;
                            let rid = Date.now();
                            data.div_id = "div_" + rid;
                            data.id = "form_"+rid;
                            data.name = rid;
                            elem.children[0].children.splice(evt.newIndex, 0, data);
                        }
                        layui.designCommon.setSelData(data);
                        layui.designCommon.renderDesign();
                    }else{
                        $(evt.item).remove();
                        layer.msg('布局控件中只能放DIV或布局控件，DIV控件中不能放DIV和布局控件');
                        return false;
                    }
                },
                onUpdate: function (evt) {
                    let parentId = $(evt.item.parentElement).attr('id'),
                        parentIndex = evt.item.parentElement.dataset.index;
                    let elem = layui.designCommon.findElemByDivId(layui.designCommon.getComponentData(), parentId);
                    let children = elem.children[0].children;
                    [children[evt.newIndex], children[evt.oldIndex]] = [children[evt.oldIndex], children[evt.newIndex]];
                    layui.designCommon.renderTree();
                },
                onEnd: function (evt) { }
            });
        },


    }

    exports('designDom', obj);
})