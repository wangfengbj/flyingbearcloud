// +----------------------------------------------------------------------
// | flyingBear公用模块【layui扩展模块】
// +----------------------------------------------------------------------
layui.define(function (exports) {
    "use strict";
    let $ = layui.$, layer = layui.layer;

    let obj = {
        loading: function(){
            return layer.load(0, {
                shade: [0.1, '#fff'] // shade: false,
            });
        },
        close: function(index){ if(index){layer.close(index);}else{layer.closeAll();} },
        msg: function(message){ layer.msg(message); },
        /**
         * get异步请求，请求contentType为form
         * param查询参数
         * configObj配置项 openWait是否开启loading、errorMsg请求失败时候提示信息
         * successFun 成功回调函数
         */
        getFormAsync : function(url, param={}, configObj ={}, successFun){
            let load_index; let that = this; if(configObj && configObj.openWait === true){load_index = that.loading();}
            $.ajax({async: true, cache: false, type: "get", timeout: 1000*60, url: url, data: param,
                contentType: "application/x-www-form-urlencoded", // application/json
                dataType: "json", // 预期服务器返回类型
                success: function(result, status, xhr){ successFun(result); },
                error: function(xhr, status, error){
                    console.log("xhr:"+xhr.status+",status:"+status+",error:"+error);
                    if(configObj && configObj.errorMsg){ that.msg(errorMsg); }else{ that.msg("请求失败"); }
                },
                complete: function(xhr, status){ // 请求成功或失败之后均调用
                    if(load_index){ that.close(load_index); }
                }
            });
        },
        /**
         * post异步请求，请求contentType为form
         * param查询参数
         * configObj配置项 openWait是否开启loading、errorMsg请求失败时候提示信息
         * successFun 成功回调函数
         */
        postFormAsync : function(url, param={}, configObj ={}, successFun){
            let load_index; let that = this; if(configObj && configObj.openWait === true){load_index = that.loading();}
            $.ajax({async: true, cache: false, type: "post", timeout: 1000*60, url: url, data: param,
                traditional: true, // 是否使用传统的方式浅层序列化,若有数组参数或对象参数需要设置true!!!!!!
                contentType: "application/x-www-form-urlencoded", // application/json
                dataType: "json", // 预期服务器返回类型
                success: function(result, status, xhr){ successFun(result); },
                error: function(xhr,status,error){
                    console.log("xhr:"+xhr.status+",status:"+status+",error:"+error);
                    if(configObj && configObj.errorMsg){ that.msg(errorMsg); }else{ that.msg("请求失败"); }
                },
                complete: function(xhr, status){ // 请求成功或失败之后均调用
                    if(load_index){ that.close(load_index); }
                }
            });
        }
    };

    exports('flyingBear', obj);
})