// +----------------------------------------------------------------------
// | 表单设计公用部分代码，表单设计操作区域基于layui，拖拽基于Sortable。
// | designDom的实现可以layui，也可以bootstrap、也可以是自定义【tips使用layer】
// +----------------------------------------------------------------------
layui.define(['designDom'], function (exports) {
    "use strict";
    let form = layui.form, $ = layui.$, layer = layui.layer, tree = layui.tree;
    let component_data; // 记录拖拽的组件
    let sel_data; // 当前选择组件
    let designDom = layui.designDom;
    let design_div_id = "formBuilder"; // 设计区域的divID
    let attr_div_id = "Properties"; // 属性区域的divId
    let class_item = "layui-form-item"; // 组件外层div样式
    let class_label = "layui-form-label"; // label样式
    let class_hide = "layui-hide"; // 隐藏样式
    let red_start = "<span style=\"color:red;\">* </span>";
    let class_inline = "layui-input-inline";
    let class_block = "layui-input-block";
    // -----------这里根据所选框架不同需要修改-----------
    let date_types = [{ title: '默认', value: 'date' }, { title: '年选择器', value: 'year' }, { title: '年月选择器', value: 'month' }, { title: '时间选择器', value: 'time' }, { title: '日期时间选择器', value: 'datetime' }];
    let dateformat = ['yyyy-MM-dd HH:mm:ss', 'yyyy年MM月dd日', 'dd/MM/yyyy', 'yyyyMM', 'H点M分', 'yyyy-MM', 'yyyy年M月d日H时m分s秒'];
    let attrs_offset = { 1: '上', 4: '左', 3: '下', 2: '右' };
    let attrs_border = {'layui-border-black': '黑色', 'layui-border-red': '赤色', 'layui-border-orange': '橙色', 'layui-border-green ': '墨绿色', 'layui-border-cyan': '青色', 'layui-border-blue': '蓝色'};
    let attrs_btnTheme = { '': '默认', 'layui-btn-normal': '百搭', 'layui-btn-warm': '暖色', 'layui-btn-danger': '警告', 'layui-btn-disabled': '禁用' };
    let attrs_btnSize = { '': '默认按钮', 'layui-btn-lg': '大型按钮', 'layui-btn-sm': '小型按钮', 'layui-btn-xs': '迷你按钮', 'layui-btn-fluid': '流体按钮' };
    let arr_file = [{ title: '文件', value: 'file' }, { title: '视频', value: 'video' }, { title: '音频', value: 'audio' }, { title: '图片', value: 'images' }];
    let up_type = ['normal', 'images', 'multiple'];
    let jl_type = ['label', 'value'];
    let verify = [{ title: '手机', value: 'phone' }, { title: '邮箱', value: 'email' }, { title: '网址', value: 'url' }, { title: '数字', value: 'number' }, { title: '日期', value: 'date' }, { title: '身份证', value: 'identity' }];



    let obj = {
        init: function(){
            component_data = []; sel_data = null;
            // 右侧表单属性渲染
            form.render(null, 'formAttribute');
            let othis = this;
            // 左侧拖拽区域Sortable实例化
            new Sortable(document.getElementById("base_component_group"), {
                group: {name: "formDesign", pull: 'clone', put: false},
                sort: false, // 禁止排序
                //swapThreshold: 0, // 交换排序的阈值，如果阈值为0，A可以把组件拖拽到B，但是B的组件不可以拖拽到A
                //handle: '.handle', // 含有此样式的元素才能触发拖拽
                //filter: '.filtered', // 这个样式的不能拖拽
                //preventOnFilter: true, // 在触发过滤器`filter`的时候调用`event.preventDefault()`
                //draggable: ".item",  // 允许拖拽的项目类名
                animation: 150,
                onChoose: function (evt) { },  // 拖动前显示
                onAdd: function (evt) { },     // 添加到这里
                onEnd: function (evt) { },     // 拖动结束
            });
            // 左侧扩展组件
            new Sortable(document.getElementById("extend_component_group"), {
                group: {name: "formDesign", pull: 'clone', put: false},
                sort: false, // 禁止排序
                animation: 150,
                onChoose: function (evt) { },  // 拖动前显示
                onAdd: function (evt) { },     // 添加到这里
                onEnd: function (evt) { },     // 拖动结束
            });

            // 中间拖拽区域Sortable实例化
            new Sortable(document.getElementById(design_div_id), {
                group: { name: "formDesign"},
                animation: 150,
                ghostClass: "sortableGhost",  // drop时候的选中元素样式
                chosenClass: "sortableChosen",  // 被选中项的css类名
                dragClass: "sortable-drag",  // 正在被拖拽中的css类名，浮起来那个元素
                filter: function (evt, item) {
                    // 过滤哪些不拖拽
                    if ($(item).hasClass('layui-tab')) {return true;}
                    if ($(item).hasClass('formBuilder_main')) {return true;}
                    if ($(item).data('type') == 'space') { return true;}
                    return false;
                },
                onAdd: function (evt) {
                    let id = Date.now();
                    let field_tag = $(evt.item).data('tag');
                    let obj = othis.getField(field_tag);

                    // 待处理
                    // let item = evt.item, oid = $(item).attr('id');
                    // if (typeof oid != 'undefined') {
                    //     obj = JSON.parse(JSON.stringify(designCommon.findElemByDivId(component_data, oid)));
                    //     designCommon.deleteElem(component_data, obj.name);
                    // }

                    if(obj != null){ obj.div_id = "div_" + id; obj.id = "form_"+id; obj.name = id;
                        // 布局最外层要占满整行
                        if(obj.tag === "layout"){obj.divStyle = "width:100%;"} sel_data = obj;
                         component_data.splice(evt.newIndex, 0, obj);
                    }
                    othis.renderDesign();
                },
                onUpdate: function (evt) {
                    if (evt.to.id == design_div_id) {
                        [component_data[evt.newIndex], component_data[evt.oldIndex]] = [component_data[evt.oldIndex], component_data[evt.newIndex]];
                        othis.renderTree();
                    }
                },
                onEnd: function (evt) { }
            });
            //右侧组件树
            tree.render({
                elem: '#design_tree',
                id: 'design_tree',
                onlyIconControl:true,
                data: [{
                    id: "0",
                    title: '组件树',
                    icon: 'iconfont icon-html',
                    children:[]
                }],
                click: function (obj){
                    $("#"+obj.data.id).click();
                }
            });
        },
        getSelData:function (){ return sel_data },
        setSelData:function (data){sel_data = data},
        getComponentData:function (){ return component_data },
        /**
         * 组件属性的标签
         */
        field_label: {
            tag: "组件类型",
            div_id:'<span style="color:red;">*</span>父div的id',
            id: '<span style="color:red;">*</span>控件id',
            name: '<span style="color:red;">*</span>控件name',
            label: "<span style=\"color:red;\">*</span>标签名",
            type: '表单类型',
            placeholder: '占位提示',
            default: '默认值',
            min: '最小',          // 当类型为number的时候
            max: '最大',
            maxlength: '文本长度', // text
            verify: '验证规则',
            data_min: '最小',     // 自动渲染组件
            data_max: '最大',
            data_step: '步长',
            width: '组件宽度【%】', // 组件整个默认占比单位%
            labelWidth: '文本宽度【px】', // 标签默认宽度
            height: '组件高度单位像素',
            data_dateType: "日期类型",
            data_dateformat: "显示格式",
            data_default: '默认值',
            data_theme: "主题",
            data_size: '文件大小',
            msg: '消息提示',
            offset: '提示位置',
            text: '文本', // button、label文本
            btnSize: '按钮大小',
            theme: "皮肤",
            border: '分割线', // 分割线样式
            lay_skin: '样式',  // checkbox样式
            labelHide: '隐藏标签',
            readonly: "只读属性",
            disabled: "禁用表单",
            required: "必填项",
            lay_search: '搜索模式', // select搜索模式
            url: "请求地址【需返回JSON】", // 请求地址
            valueColumn: "作为value的字段", // 值字段
            showColumn: "作为text的字段", // 显示字段
            data_range: '左右面板', // 日期范围选择
            data_maxvalue: "最大值", // 日期
            data_minvalue: "最小值", // 日期
            data_showStep: '显示断点', // slider
            data_input: '输入框', // slider
            data_half: "显示半星", // rate
            data_length: "星星个数", // rate
            data_value: '选项值', // 级联
            data_parents: '关联父类', // 级联
            options: "选项",
            createEnum: "是否生成enum类", // radio checkbox select
            dbColumn: "显示text的id对应数据库字段",
            divStyle: "自定义样式",

            uploadType: '上传样式',
            data_accept: '上传类型',
            textarea: '多行文本', // 便签信息
            column: '栅格列数',
        },
        /**
         * 定义组件类型
         */
        component_fields: {
            input: {tag: 'input', div_id:'', id:'', name: '', label: "单行文本", type: 'text', placeholder: "请输入", default: '', labelWidth: '110', width: 100,
                maxlength: '', min: 0, max: 0, required: false, readonly: false, disabled: false, labelHide: false, verify: ''},
            textarea: {tag: 'textarea', div_id:'', id:'', name: '', label: "多行文本", placeholder: "请输入", default: '', maxlength: '', labelWidth: 110, width: 100,
                required: false, readonly: false, disabled: false, labelHide: false},
            radio: {tag: "radio", div_id:'', id:'', name: '', label: "单选框", labelWidth: 110, width: 100, createEnum: false, required: false, disabled: false, labelHide: false,
                options: [{title: '值1', value: '0', checked: true}, {title: '值2', value: '1', checked: false}]},
            checkbox: {tag: "checkbox", div_id:'', id:'', name: '', label: "多选框", lay_skin: 'primary', labelWidth: 110, width: 100, createEnum: false, required: false, disabled: false, labelHide: false,
                options: [{title: '值1', value: '1', checked: true}, {title: '值2', value: '2', checked: true}, {title: '值3', value: '3', checked: false}]},
            select: {tag: "select", div_id:'', id:'', name: '', label: "下拉框", labelWidth: 110, width: 100, createEnum: false, lay_search: false, required: false, disabled: false, labelHide: false,
                options: [{title: '选项1', value: '1', checked: false}, {title: '选项2', value: '2', checked: true}, {title: '选项3', value: '3', checked: false}]},
            // 数据源来源于链接
            selectDb: {tag: "selectDb", div_id:'', id:'', name: '', label: "数据库下拉框", url: "", valueColumn: "", showColumn: "", labelWidth: 110, width: 100,
                lay_search: false, required: false, disabled: false, labelHide: false},
            date: {tag: "date", div_id:'', id:'', name: '', label: "日期组件", data_dateType: "datetime", data_dateformat: "yyyy-MM-dd HH:mm:ss", placeholder: 'yyyy-MM-dd', data_maxvalue: "9999-12-31",
                data_minvalue: "1900-01-01", data_range: false, labelWidth: 110, width: 100, required: false, readonly: false, disabled: false, labelHide: false},
            colorPicker: {tag: "colorPicker", div_id:'', id:'', name: '', label: "颜色选择器", labelWidth: 110, width: 100, required: false, disabled: false, labelHide: false},
            slider: {tag: "slider", div_id:'', id:'',  name: '', label: "滑块", data_min: 0, data_max: 100, data_step: 1, data_default: 10, data_theme: '#009688', data_input: false,
                data_showStep: false, labelWidth: 110, width: 100, disabled: false, labelHide: false},
            rate: {tag: "rate", div_id:'', id:'',  name: '', label: "评分", data_default: 1, data_length: 5, data_half: false, data_theme: '#009688', labelWidth: 110, width: 100, readonly: false, labelHide: false},
            switch: {tag: "switch", div_id:'', id:'',  name: '', label: "开关", labelWidth: 110, width: 100, disabled: false, labelHide: false},
            database: {tag: "database", div_id:'', id:'',  name: '', label: "数据库查询", type: 'text', placeholder: "请选择", dbColumn: 'dbColumn', labelWidth: 110, width: 100, required: false, labelHide: false},
            subtraction: {tag: "subtraction", div_id:'', id:'',  name: '', border: 'layui-border-black', width: 100},
            tips: {tag: "tips", div_id:'', id:'',  name: '', msg: '消息提示', offset: 2},
            button: {tag: "button", div_id:'', id:'',  name: '', text: '按钮', theme: '', btnSize: ''},
            label: {tag: "label", div_id:'', id:'',  name: '', text: '分区标签', width: 100},
            space: {tag: "space", div_id:'', id:'',  name: '', height: 10},
            upload: {tag: "upload", div_id:'', id:'',  name: '', label: "文件上传", uploadType: 'normal', data_size: 102400, data_accept: 'file',
                labelWidth: 110, width: 100, required: false, disabled: false, labelHide: false},
            cascader: {tag: "cascader", div_id:'', id:'',  name: '', label: "级联选择器", data_value: "label", data_parents: true, labelWidth: 110, width: 100, labelHide: false},
            editor: {tag: "editor", div_id:'', id:'',  name: '', label: "编辑器", labelWidth: 110, width: 100, labelHide: false},
            tab: {tag: "tab", div_id:'', id:'',  name: '', options: [{title: '选项1', value: 'tab1', checked: true}, {title: '选项2', value: 'tab2', checked: false}],
                children: [{children: []}, {children: []}]},
            grid: {tag: "grid", div_id:'', id:'',  name: '', column: 2, children: [{children: []}, {children: []}] },
            tags: {tag: "tags", div_id:'', id:'',  name: '', label: "关键词", labelWidth: 110, width: 100, labelHide: false},
            json: {tag: "json", div_id:'', id:'',  name: '',  label: "数组组件", labelWidth: 110, width: 100, labelHide: false},
            // 扩展组件
            layout: {tag: "layout", div_id:'', id:'',  name: '', divStyle: "width:50%;", children: [{children: []}] },
            div: {tag: "div", div_id:'', id:'',  name: '', divStyle:"border: 1px solid #ccc; border-radius: 5px; width:50%; min-height: 100px;", children: [{children: []}]}
        },
        /**
         * 获取控件的所有属性
         * @param name
         * @returns {null|any}
         */
        getField: function(name){
            let field = this.component_fields[name];
            if(field){ return JSON.parse(JSON.stringify(field));
            }else{ layer.msg("控件还未开放"); return null; }
        },
        /**
         * 获取属性的标签
         * @param name
         * @returns {string|*}
         */
        getFieldLabel: function(name){
            if (typeof this.field_label[name] != 'undefined') { return this.field_label[name];
            } else { return name + ' undefined'; }
        },
        /**
         * 设计区域控件右下角按钮
         */
        getToolsHtml: function (){
            let tools = '<div class="design-tools">';
            tools += ' <i class="iconfont icon-copy1" title="复制"></i>';
            tools += ' <i class="iconfont icon-delete" title="删除"></i>';
            tools += '</div>';
            return tools;
        },
        /**
         * 删除数组中元素
         * @param array
         * @param name
         */
        deleteElem: function(array, name){
            for (let index = 0; index < array.length; index++) {
                const element = array[index];
                if (element.name == name) { array.splice(index, 1); break; }
                if (typeof element.children != 'undefined' && element.children.length) {
                    let subElem = element.children;
                    for (let i = 0; i < subElem.length; i++) {
                        if (subElem[i].children.length) { this.deleteElem(subElem[i].children, name); }
                    }
                }
            }
        },
        /**
         * 根据控件name属性查找
         * @param array
         * @param name
         * @returns {*}
         */
        findElem: function (array, name){
            for (let index = 0; index < array.length; index++) {
                const element = array[index];
                if (element.name == name) { return element; }
                if (typeof element.children != 'undefined' && element.children.length) {
                    let subElem = element.children;
                    for (let i = 0; i < subElem.length; i++) {
                        if (subElem[i].children.length) {
                            let item = this.findElem(subElem[i].children, name);
                            if (item && typeof item != 'undefined') { return item; }
                        }
                    }
                }
            }
        },
        /**
         * 根据控件父div_id查找
         * @param array
         * @param div_id
         * @returns {*}
         */
        findElemByDivId: function(array, div_id){
            for (let index = 0; index < array.length; index++) {
                const element = array[index];
                if (element.div_id == div_id) { return element; }
                if (typeof element.children != 'undefined' && element.children.length) {
                    let subElem = element.children;
                    for (let i = 0; i < subElem.length; i++) {
                        if (subElem[i].children.length) {
                            let item = this.findElemByDivId(subElem[i].children, div_id);
                            if (item && typeof item != 'undefined') { return item; }
                        }
                    }
                }
            }
        },
        /**
         * 中间区域渲染
         */
        renderDesign: function (array = component_data, elem = $("#"+design_div_id), elem_tag){
            debugger;
            // 富文本编辑器
            $(elem).find('.editorsrv').each(function (i, n) { $(n).parent().find('.tox-tinymce').remove(); $(n).remove(); });
            // 清空原来的数据
            elem.empty("");
            // 遍历数据创建html
            for (let index = 0; index < array.length; index++) {
                let d = array[index];
                let outerHTML = designDom.getHtml(d);
                elem.append($(outerHTML).append(this.getToolsHtml()).prop("outerHTML"));
                designDom.fieldRender(d.tag, d.name, d);
            }
            if(elem_tag === "layout"){
                elem.append(this.getToolsHtml());
            }
            if (typeof sel_data != 'undefined') { this.getAttributeValues(sel_data);
            } else if (array.length) {
                sel_data = array[array.length - 1];
                this.getAttributeValues(sel_data);
            }
            $('#'+design_div_id+' div.active').each(function (k, n) {$(n).removeClass('active');})
            if (typeof sel_data != 'undefined') {$('div#' + sel_data.div_id).addClass('active');}
            // 渲染表单
            form.render(null, design_div_id);
            this.renderTree();
        },
        /**
         * 组件树刷新
         */
        renderTree: function (){
            let bl = function(array, parent){
                for (let index = 0; index < array.length; index++) {
                    let d = array[index];
                    let item = {id: d.div_id, title: d.tag, spread:true, icon: 'iconfont icon-html', children:[]};
                    if(typeof d.children != 'undefined' && d.children.length){
                        let subElem = d.children;
                        for (let i = 0; i < subElem.length; i++) {
                            if (subElem[i].children.length) {
                                bl(subElem[i].children, item)
                            }
                        }
                    }
                    parent.children.push(item);
                }
            }
            let parent = {id: "0", title: '组件树', spread:true, icon: 'iconfont icon-html', children:[]};
            bl(component_data, parent);
            tree.reload('design_tree', {data: [parent]});
        },
        /**
         * 右侧属性区域组件前html
         * @param label
         * @param hide
         * @returns {string}
         */
        getAttributePrefix: function(label, hide = ''){
            let html = '<div class="'+class_item+' ' + hide + '">';
            if (label) {
                html += '<label class="'+class_label+'" style="text-align: left; width: 180px;">' + label + '</label>';
            }
            html += '<div class="'+class_inline+'">';
            return html;
        },
        /**
         * 右侧属性区域渲染，这一部分可以作为公用使用例如使用bootstrap
         * @param obj
         */
        getAttributeValues: function (obj){
            let attributeHtml = "";
            for (const key in obj) {
                if (key == 'index') { continue; }
                let value = obj[key];
                if (key == 'options' || key == 'children') {}
                else { attributeHtml += this.getAttributePrefix(this.getFieldLabel(key)); }
                switch (key) {
                    case 'tag': // 控件类型
                        attributeHtml += '<input class="layui-input layui-disabled" disabled value="' + value + '" data-type="' + key + '">';
                        break;
                    case 'div_id': // 控件父div的id
                        attributeHtml += '<input class="layui-input layui-change" value="' + value + '" id="' + key + '" placeholder="一般使用默认即可">';
                        break;
                    case 'id': // 控件id
                        attributeHtml += '<input class="layui-input layui-change" value="' + value + '" id="' + key + '" placeholder="一般使用默认即可">';
                        break;
                    case 'name': // 控件name
                        attributeHtml += '<input class="layui-input layui-change" value="' + value + '" id="' + key + '" placeholder="对应数据库列表驼峰命名">';
                        break;
                    case 'divStyle':
                        attributeHtml += '<textarea class="layui-textarea layui-change" id="' + key + '">'+value+'</textarea>';
                        break;
                    case 'msg': // 消息提示框的提示信息
                    case 'text': // button和label的文本信息
                    case 'default': // input和textarea默认值
                    case 'data_default': // slide、rate默认值
                    case 'url': // 数据库查询下拉
                    case 'valueColumn': // 数据库查询下拉
                    case 'showColumn': // 数据库查询下拉
                    case 'dbColumn': // 数据库控件显示文本对应的数据库字段
                    case 'data_theme': // slide、rate主题
                        attributeHtml += '<input class="layui-input layui-change" value="' + value + '" id="' + key + '">';
                        break;
                    case 'label':
                        attributeHtml += '<input class="layui-input layui-keyup" value="' + value + '" id="' + key + '">';
                        break;
                    case 'placeholder':
                        attributeHtml += '<input class="layui-input layui-keyup" value="' + value + '" id="' + key + '">';
                        break;
                    case 'type':
                        attributeHtml += '<select lay-filter="componentSelected" data-field="type">';
                        let inputType =  [{title: '文本', value: 'text'}, {title: '密码', value: 'password'}, {title: '数字', value: 'number'}];
                        for (let index = 0; index < inputType.length; index++) {
                            const element = inputType[index];
                            attributeHtml += '<option value="' + element.value + '"';
                            if (element.value == obj.type) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + element.title + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'maxlength': // text和textarea文本长度
                        attributeHtml += '<input type="number" min="1" id="maxlength" class="layui-input layui-change" value="' + (value ? value : 255) + '">';
                        break;
                    case 'data_length': // rate
                        attributeHtml += '<input type="number" min="1" id="' + key + '"  class="layui-input layui-change" value="' + value + '">';
                        break;
                    case 'max': // type=number最大值
                    case 'min': // type=number最小值
                    case 'height': // 间隔高度
                    case 'data_max': // slider
                    case 'data_min': // slider
                    case 'data_step': // slider步长
                    case 'data_size': // 上传文件大小
                        attributeHtml += '<input type="number" min="0" class="layui-input layui-change" value="' + value + '" id="' + key + '" >';
                        break;
                    case 'data_dateType': // 日期
                        attributeHtml += '<select lay-filter="componentSelected" data-field="data_dateType" >';

                        for (let index in date_types) {
                            const element = date_types[index];
                            attributeHtml += '<option value="' + element.value + '"';
                            if (element.value == obj.data_dateType) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + element.title + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'data_dateformat': // 日期
                        attributeHtml += '<select lay-filter="componentSelected" data-field="data_dateformat" >';
                        for (let index in dateformat) {
                            const element = dateformat[index];
                            attributeHtml += '<option value="' + element + '"';
                            if (element == obj.data_dateformat) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + element + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'offset': // 消息提示框的提示信息
                        attributeHtml += '<select lay-filter="componentSelected" data-field="offset" >';
                        for (const key in attrs_offset) {
                            attributeHtml += '<option value="' + key + '" ';
                            if (key == obj.offset) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + attrs_offset[key] + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'width': // 滑块操作
                        attributeHtml += '<div data-min="20" data-max="100" id="' + key + '" class="layui-sliders" data-value="' + value + '"></div>';
                        break;
                    case 'labelWidth':
                        attributeHtml += '<div data-min="60" data-max="255" id="' + key + '" class="layui-sliders" data-value="' + value + '"></div>';
                        break;
                    case 'border': // 分割线样式
                        attributeHtml += '<select lay-filter="componentSelected" data-field="border">';
                        for (const key in attrs_border) {
                            attributeHtml += '<option value="' + key + '" ';
                            if (key == obj.border) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + attrs_border[key] + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'theme': // 按钮主题
                        attributeHtml += '<select lay-filter="componentSelected" data-field="theme">';
                        for (const key in attrs_btnTheme) {
                            attributeHtml += '<option value="' + key + '" ';
                            if (key == obj.theme) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + attrs_btnTheme[key] + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'btnSize': // 按钮大小
                        attributeHtml += '<select lay-filter="componentSelected" data-field="btnsize">';
                        for (const key in attrs_btnSize) {
                            attributeHtml += '<option value="' + key + '" ';
                            if (key == obj.btnSize) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + attrs_btnSize[key] + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'data_accept': // 文件上传
                        attributeHtml += '<select lay-filter="componentSelected" data-field="data_accept">';
                        for (let index in arr_file) {
                            const element = arr_file[index];
                            attributeHtml += '<option value="' + element.value + '"';
                            if (element.value == obj.data_accept) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + element.title + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'uploadType': // 文件上传
                        for (const index in up_type) {
                            attributeHtml += ' <span class="layui-badge ';
                            if (up_type[index] == obj.uploadType) { attributeHtml += 'layui-bg-blue';
                            } else { attributeHtml += 'layui-bg-gray';
                            }
                            attributeHtml += '" id="'+key+'" value="'+up_type[index]+'" >'+up_type[index]+'</span>';
                        }
                        break;
                    case 'data_maxvalue': // 日期
                    case 'data_minvalue':  // 日期
                        attributeHtml += '<input class="layui-input layui-change" value="' + value + '" id="' + key + '" >';
                        break;
                    case 'data_value': // 级联
                        for (const index in jl_type) {
                            attributeHtml += ' <span class="layui-badge ';
                            if (jl_type[index] == obj.data_value) { attributeHtml += 'layui-bg-blue';
                            } else { attributeHtml += 'layui-bg-gray'; }
                            attributeHtml += '" id="'+key+'" value="'+jl_type[index]+'" >'+jl_type[index]+'</span>';
                        }
                        break;
                    case 'textarea': // 便签
                        attributeHtml += '<textarea class="layui-textarea layui-change" id="' + key + '" >' + value + '</textarea>';;
                        break;
                    case 'options':
                        attributeHtml += '<fieldset id="layui-elem-field" class="layui-elem-field layui-field-title">';
                        attributeHtml += '<legend>选项</legend>';
                        attributeHtml += ' </fieldset>';
                        attributeHtml += '<div id="form-options" class="layui-form-item">';
                        for (const key in obj.options) {
                            let option = obj.options[key];
                            attributeHtml += '<div class="layui-input-inline">';
                            //attributeHtml += '<i class="layui-icon layui-icon-slider"></i>';
                            attributeHtml += '<input class="layui-input" value="' + option.title + '" >';
                            attributeHtml += '<input class="layui-input" value="' + option.value + '" >';
                            attributeHtml += '<i class="iconfont icon-delete layui-icon-xx"></i>';
                            attributeHtml += '</div>';
                        }
                        attributeHtml += '<div class="layui-input-inline layui-inlioc">';
                        attributeHtml += '<button type="button" class="layui-btn layui-btn-xs layui-add-option">添加</button>';
                        attributeHtml += '</div>'
                        attributeHtml += '</div>';
                        break;
                    case 'column':
                        let attrs_column = [2, 3, 4];
                        attributeHtml += '<select lay-filter="componentSelected" data-field="column">';
                        for (const i in attrs_column) {
                            let val = attrs_column[i];
                            attributeHtml += '<option value="' + val + '" ';
                            if (val == obj.column) { attributeHtml += 'selected'; }
                            attributeHtml += '>' + val + '</option>';
                        }
                        attributeHtml += '</select>';
                        break;
                    case 'lay_skin':  // checkbox样式
                    case 'required':
                    case 'readonly':
                    case 'disabled':
                    case 'labelHide':
                    case 'lay_search': // select搜索模式
                    case 'data_range': // 日期范围选择
                    case 'data_showStep': // slider
                    case 'data_half': // rate
                    case 'data_input': // slider
                    case 'createEnum': // 是否生成enum类
                    case 'data_parents': // 级联
                        attributeHtml += '<input type="checkbox" lay-skin="switch" lay-filter="componentChecked" ';
                        if (obj[key]) { attributeHtml += 'checked=""'; }
                        attributeHtml += 'data-field="' + key + '">';
                        break;
                    case 'verify':
                        for (const key in verify) {
                            let check = verify[key];
                            attributeHtml += '<input type="checkbox" lay-skin="primary" lay-filter="verify" value="' + check.value + '"';
                            attributeHtml += 'title="' + check.title + '"';
                            if (obj.verify.indexOf(check.value) != -1) { attributeHtml += 'checked=""'; }
                            attributeHtml += '>';
                        }
                        break;
                    default:
                        break;
                }
                attributeHtml += '</div></div>';
            }
            //attributeHtml += '<div id="slideTest8" ></div>';
            $('#'+attr_div_id).html(attributeHtml);
            form.render(null, attr_div_id);

            // ------- 最大化时间回调操作 -------
            let othis = this;
            let datetime = $('#data_maxvalue, #data_minvalue');
            datetime.each(function (index, obj) {
                layui.laydate.render({ elem: this , type: 'date' , format: 'yyyy-MM-dd', done: function (value, date, endDate) {
                        let element = othis.findElem(component_data, sel_data.name);
                        try { element[$(obj).attr('id')] = value;
                        } catch (error) { console.log(error); }
                        othis.renderDesign();
                    }
                });
            })

            // 选项拖动接口
            if ($('#'+attr_div_id+' #form-options').length) {
                new Sortable(document.getElementById('form-options'), {
                    handle: '.layui-icon-slider',
                    animation: 150,
                    onAdd: function (evt) { },
                    onUpdate: function (evt) {
                        let element = othis.findElem(component_data, sel_data.name);
                        try {
                            let op = element.options;
                            [op[evt.newIndex], op[evt.oldIndex]] = [op[evt.oldIndex], op[evt.newIndex]];
                            othis.renderDesign();
                        } catch (error) {}
                    },
                    onEnd: function (evt) { }
                });
            }
            // slider组件
            layui.each($(".layui-sliders"), function (key, elem) {
                let othat = $(this),
                    obj = othat.attr('id'),
                    val = othat.data('value'),
                    min = othat.data('min') || 10,
                    max = othat.data('max') || 100,
                    theme = othat.data('theme') || '#009688';
                layui.slider.render({ elem: elem, min: min, max: max, input: true, theme: theme, value: val, step : 2
                    , change: function (value) {
                        if (value <= min || isNaN(value)) { value = min; }
                        let element = othis.findElem(component_data, sel_data.name);
                        try {  element[obj] = value } catch (error) { console.warn(error); }
                        othis.renderDesign();
                    }
                })
            });
        },
        /**
         * 公用事件，如果使用生成的页面也需要这部分代码
         */
        publicEvents: function (){
            // tips事件
            $(document).on("mouseenter","*[lay-tips]" , function () {
                let remind = $(this).attr("lay-tips");
                let tips = $(this).data("offset") || 4 ;
                let color = $(this).data("color") || '#000';
                layer.tips(remind, this,{ time: -1, tips: [tips, color], area: ['auto', 'auto'] });
            }).on("mouseleave", "*[lay-tips]", function () { layer.closeAll("tips"); });
        },
        initEvents: function(){
            let that = this;
            // 删除事件
            $('body').on('click', '#'+design_div_id+' .icon-delete', function (e) {
                layer.confirm('确定要删除元素吗？', {}, function (index, layero) {
                    sel_data = that.deleteElem(component_data, sel_data.name) || [];
                    that.renderDesign();
                    layer.close(index);
                })
            });
            // 复制事件
            $('body').on('click', '#'+design_div_id+' .icon-copy1', function (e) {
                e.preventDefault(); e.stopPropagation();
                let copyElem = [];
                let cycleElem = function (array, name) {
                    for (let index = 0; index < array.length; index++) {
                        const element = array[index];
                        if (element.name == name) {
                            copyElem = JSON.parse(JSON.stringify(element));
                            let rid = Date.now();
                            copyElem.div_id = "div_" + rid;
                            copyElem.id = "form_"+rid;
                            copyElem.name = rid;

                            // children的id和name重复问题，这里暂时只考虑节点有children情况，children还有children暂时不考虑
                            if (typeof copyElem.children != 'undefined' && copyElem.children.length) {
                                let subElem = copyElem.children;
                                for (let i = 0; i < subElem.length; i++) {
                                    let cdl = subElem[i].children;
                                    for(let j=0; j<cdl.length; j++){
                                        let item = cdl[j];
                                        let rid = Date.now();
                                        item.div_id = "div_" + rid;
                                        item.id = "form_"+rid;
                                        item.name = rid;
                                    }
                                }
                            }

                            array.splice(index + 1, 0, copyElem);
                            break;
                        }
                        if (typeof element.children != 'undefined' && element.children.length) {
                            let subElem = element.children;
                            for (let i = 0; i < subElem.length; i++) {
                                if (subElem[i].children.length) {
                                    let item = cycleElem(subElem[i].children, name);
                                    if (item != [] && typeof item != 'undefined') { return item; }
                                }
                            }
                        }
                    }
                }
                cycleElem(component_data, sel_data.name);
                sel_data = copyElem;
                // 重新渲染
                that.renderDesign();
            });
            // 组件点击事件
            $('body').on('click', '#'+design_div_id+' .'+class_item+',#'+design_div_id+' .design-dw', function (e) {
                // 过滤元素中按钮，禁止冒泡
                if ($(e.toElement).hasClass('layui-btn') == false
                    && $(e.toElement).hasClass('layui-upload-file') == false
                    && $(e.toElement).parent().hasClass('layui-tab-title') == false && $(e.toElement).hasClass('design-dw') == false) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                let othis = $(this), oid = othis.attr('id');
                let element = that.findElemByDivId(component_data, oid);
                if (typeof element == 'undefined' || typeof element.name == 'undefined') { return true; }
                let name = element.name;
                $('div.active').each(function (k, n) { $(n).removeClass('active'); })
                $(othis).addClass('active');
                sel_data = that.findElem(component_data, name);
                that.getAttributeValues(sel_data);
            })
            // 编辑标签事件
            $('body').on('keyup', '#'+attr_div_id+' #label', function (e) {
                $('#' + sel_data.div_id).find('label:eq(0)').text($(this).val());
                let element = that.findElem(component_data, sel_data.name);
                element.label = $(this).val();
            });

            // 占位提示事件
            $('body').on('keyup', '#'+attr_div_id+' #placeholder', function (e) {
                $('#' + sel_data.div_id).find(sel_data.tag + ':eq(0)').prop('placeholder', $(this).val());
                let element = that.findElem(component_data, sel_data.name);
                element.placeholder = $(this).val();
            });

            // layui-change 更新后操作   layui-keyup 键入后操作
            $('body').on('change', '#'+attr_div_id+' .layui-change', function (e) {
                let othis = this;
                let oid = $(othis).attr('id');
                let oval = $(othis).val();
                if (oid == 'name') {
                    let existing = that.findElem(component_data, oval);
                    if (oval.length <= 1 || existing) { layer.msg('name设置太短或者已经存在', 'error'); return false; }
                }
                let element = that.findElem(component_data, sel_data.name);
                try {
                    if (typeof element[oid] != 'undefined') { element[oid] = oval; }
                } catch (error) { console.log('undefined element'); }
                that.renderDesign();
            });

            // 添加选项
            $('body').on('click', 'button.layui-add-option', function (click) {
                let element = that.findElem(component_data, $('#'+attr_div_id+' #name').val());
                element.options.splice(element.options.length, 0, {
                    title: '选项' + element.options.length,
                    value: 'value',
                    checked: false
                });
                if (element.tag == 'tab') { element.children.push({ children: [] }); }
                that.renderDesign();
            });

            // 删除选项
            $('body').on('click', '#form-options .layui-icon-xx', function (click) {
                let element = that.findElem(component_data, $('#'+attr_div_id+' #name').val());
                for (const key in element.options) {
                    let item = element.options[key];
                    if (item.value == $(this).prev().val()) {
                        element.options.splice(key, 1);
                        if (element.tag == 'tab') { element.children.splice(key, 1); }
                        break;
                    }
                }
                that.renderDesign();
            });

            // 编辑radio checkbox组件信息，刷新中间区域
            $('body').on('change', '#form-options input', function (click) {
                let othis = $(this), parentIndex = $(othis).parent('.layui-input-inline').index(), selfIndex = $(othis).index() - 1, val = $(othis).val();
                let element = that.findElem(component_data, sel_data.name);
                for (const key in element.options) {
                    if (key == parentIndex) {
                        let item = element.options[key];
                        if (selfIndex) { item.value = val;
                        } else { item.title = val; }
                    }
                }
                that.renderDesign();
            });

            // 清空表单设计
            $('body').on('click', '.flyingbear-qk', function (e) {
                layer.confirm('确定要清空表单设计吗？', {}, function (index, layero) {
                    component_data = []; sel_data = null;
                    $('#'+design_div_id).html("<div id=\"formBuilder_main\">拖拽组件到此区域进行设计</div>");
                    $('#'+attr_div_id).html("<div>请点击组件修改属性</div>");
                    layer.close(index);
                })
            });
        },
        //--------------------------------------------以下更换框架需要修改--------------------------------------------------
        selfEvents: function(){
            let that = this;
            // 切换tab事件
            $('body').on('click', '#'+design_div_id+' ul li', function (e) {
                e.preventDefault(); e.stopPropagation();
                let othis = $(this), index = $(othis).index(), parentId = $(othis).parents('.layui-tab').parent().attr('id');
                let element = that.findElemByDivId(component_data, parentId);
                try {
                    $(this).parent().children("li").removeClass("layui-this");
                    $(this).addClass("layui-this");
                    let cdl = $(this).parent().next().children();
                    cdl.removeClass("layui-show");
                    $(cdl[index]).addClass("layui-show");
                    // 切换选项卡状态
                    for (let key = 0; key < element.options.length; key++) {
                        let subElem = element.options[key];
                        if (key == index) { subElem.checked = true; } else { subElem.checked = false; }
                    }

                } catch (error) { console.log(index, parentId); }
            });
            // 图片上传类型修改
            // $('body').on('click', '#Propertie #uploadtype', function (e) {
            //     var othis = this;
            //     var oval = $(othis).attr('value');
            //     var element = that.recursiveFindElem(options.data, options.state.name);
            //     try {
            //         element.uploadtype = oval;
            //     } catch (error) {
            //         console.log('undefined element');
            //     }
            //     $('#Propertie #uploadtype').each(function(i,e) {
            //         $(e).prop('class','layui-badge layui-bg-gray');
            //     })
            //     $(othis).prop('class','layui-badge layui-bg-blue');
            //
            //     that.reloadComponent();
            // })

            // 级联选择器类型
            // $('body').on('click', '#Propertie #data_value', function (e) {
            //     var othis = this;
            //     var oval = $(othis).attr('value');
            //     var element = that.recursiveFindElem(options.data, options.state.name);
            //     try {
            //         element.data_value = oval;
            //     } catch (error) {
            //         console.log('undefined element');
            //     }
            //     $('#Propertie #data_value').each(function(i,e) {
            //         $(e).prop('class','layui-badge layui-bg-gray');
            //     })
            //     $(othis).prop('class','layui-badge layui-bg-blue');
            //
            //     that.reloadComponent();
            // });
            // 右侧SELECT事件
            form.on('select(componentSelected)', function (data) {
                let field = $(data.elem).data('field'), element = that.findElem(component_data, sel_data.name);
                try {
                    // 待处理
                    if (element.tag == 'grid') {
                        let children = element.children;
                        let length = children.length;
                        if (data.value > length) { children.push({ children: [] });
                        } else if (data.value < length) {
                            let d = length - data.value;
                            for (let i = 0; i < children.length; i++) {
                                if (d <= 0) { break; }
                                // 当前无子类
                                if (!children[i].children.length) {
                                    d--;
                                    children.splice(i, 1);
                                }
                            }
                            // 如果过滤完还有的话 则还需要循环下
                            if (d >= 1) {
                                while (d) {
                                    d--;
                                    children.splice(d, 1);
                                }
                            }
                        }
                    }
                    element[field] = data.value;
                } catch (error) {
                    console.warn('元素未定义');
                }
                that.renderDesign();
            });
            // 右侧switch事件
            form.on('switch(componentChecked)', function (data) {
                let field = $(this).data('field'), element = that.findElem(component_data, sel_data.name);
                try {
                    if (field == 'lay_skin') { element.lay_skin = data.elem.checked ? 'primary' : '';
                    } else { element[field] = data.elem.checked ? true : false; }
                } catch (error) {}
                that.renderDesign();
            });
            // 其他验证规则
            form.on('checkbox(verify)', function (data) {
                let rules = $('#' + sel_data.div_id).find('input:eq(0)').attr('lay-verify') || [];
                if (typeof rules != 'object') { rules = rules.split('|'); }
                if (data.elem.checked) {
                    rules[rules.length] = $(this).val();
                    $('#' + sel_data.div_id).find('input:eq(0)').attr('lay-verify', rules.join('|'));
                } else {
                    for (let index = 0; index < rules.length; index++) {
                        const el = rules[index];
                        if (el == $(this).val()) { rules.splice(index, 1); }
                    }
                    if (rules.length) { $('#' + sel_data.div_id).find('input:eq(0)').attr('lay-verify', rules.join('|'));
                    } else { $('#' + sel_data.div_id).find('input:eq(0)').removeAttr('lay-verify'); }
                }
                let element = that.findElem(component_data, sel_data.name);
                let verify = element.verify == '' ? [] : element.verify.split('|');
                if (data.elem.checked) { verify.push(data.value);
                } else { verify.splice(data.value,1); }
                element.verify = verify.join('|');
            });

            // 预览表单
            $('body').on('click', '.flyingbear-yl', function (e) {
                let subHtml = "", formName = $('#formName').val(), html = '<div class="layui-fluid"><form id="' + formName + '" class="layui-form">';
                html += $('#'+design_div_id).html();
                html += '</form></div>';
                html = html.replace(/<ol[^>]+>/g, '').replace(/<\/ol>/g, '');
                html = html.replace(/<div class="design-tools">.*?<\/div>/g, '');
                // 获取提交代码
                subHtml = html.substring(0, html.indexOf('</form>'));
                subHtml += '<div class="layui-footer layui-form-item layui-center">';
                subHtml += '<button class="layui-btn layui-btn-primary" type="button">取消</button>';
                subHtml += '<button class="layui-btn lay-submit" onclick="javascript:layer.msg(\'提交测试\');" type="button" >提交</button>';
                subHtml += '</div>';
                subHtml += html.substring(html.indexOf('</form>'));
                html = subHtml;
                let formWidth = $('#formWidth').val();
                if (formWidth) { formWidth += 'px'; } else { formWidth = '65%'; }
                let formHeight = $('#formHeight').val();
                if (formHeight) { formHeight += 'px'; } else { formHeight = '65%'; }
                layer.open({ type: 1, title: '预览 - ' + formName, maxmin: true, area: [formWidth, formHeight], content: html,
                    success: function (layero, index) {
                        // 暂时只渲染layui元素
                        // 剩下的需要在自身页面进行渲染
                        form.render();
                    }
                });
            });

            // 查看html
            $('body').on('click', '.flyingbear-html', function (e) {
                debugger;
                let subHtml = "", formName = $('#formName').val(), html = '<div class="layui-fluid"><form id="' + formName + '" class="layui-form">';
                html += $('#'+design_div_id).html();
                html += '</form></div>';
                html = html.replace(/<ol[^>]+>/g, '').replace(/<\/ol>/g, '');
                html = html.replace(/<div class="design-tools">.*?<\/div>/g, '');
                // 获取提交代码
                subHtml = html.substring(0, html.indexOf('</form>'));
                subHtml += '<div class="layui-footer layui-form-item layui-center">';
                subHtml += '<button class="layui-btn layui-btn-primary" type="button">取消</button>';
                subHtml += '<button class="layui-btn lay-submit" onclick="javascript:layer.msg(\'提交测试\');" type="button" >提交</button>';
                subHtml += '</div>';
                subHtml += html.substring(html.indexOf('</form>'));
                html = subHtml;
                // js_beautify html_beautify  css_beautify
                $('#fly-code').val(html_beautify(html));
                layer.open({type: 1, title: '查看html - ' + formName, maxmin: true, area: ['800px', '660px'], content: $('.fly-window')
                    //, offset: '130px'
                    ,success: function (layero, index) { }
                });
            })
        }
    };

    exports('designCommon', obj);
})