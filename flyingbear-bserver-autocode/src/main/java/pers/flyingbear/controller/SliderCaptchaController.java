package pers.flyingbear.controller;

import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/sliderCaptcha")
public class SliderCaptchaController {
    @PostMapping("/isVerify2")
    public boolean isVerify2(List<Integer> list) {
        return doVerify(list);
    }

    @PostMapping("/isVerify")
    public boolean isVerify(@RequestBody String str) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c >= '0' && c <= '9') {
                list.add(Integer.valueOf(String.valueOf(c)));
            }
        }
        return doVerify(list);
    }

    /**
     * 用户滑动轨迹到服务器端进行了 Y 轴的平方差校验，为零时才返回 false，否则返回 true，为 true 表示 Y 轴有偏移，
     * 简单的认为此操作是人为操作，因为人手拖动过程中的抖动 Y 轴理论上是不可能没有偏移的。因此依据此值进行是否是人为拖动滑块。
     * @param list
     * @return
     */
    private boolean doVerify(List<Integer> list){
        int sum = 0;
        for (Integer data : list) {
            sum += data;
        }
        double avg = sum * 1.0 / list.size();

        double sum2 = 0.0;
        for (Integer data : list) {
            sum2 += Math.pow(data - avg, 2);
        }

        double stddev = sum2 / list.size();
        System.out.println("验证码判断"+(stddev != 0));
        return stddev != 0;
    }
}
