package pers.flyingbear.controller;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.service.AutoCodeService;
import pers.flyingbear.vo.DataBaseProperties;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
public class AutoCodeController {

    private final AutoCodeService autoCodeService;

    public AutoCodeController(AutoCodeService autoCodeService) {
        this.autoCodeService = autoCodeService;
    }

    /**
     * 获取数据库类型
     * @return
     */
    @GetMapping("/getDbTypes")
    public List<Map<String,String>> getDbTypes(){
        return autoCodeService.getDbTypes();
    }


    /**
     * 查询数据库的数据表
     * @param dbp
     * @return
     */
    @PostMapping("/getTable")
    public Map<String, Object> getTable(DataBaseProperties dbp){
        return autoCodeService.getTable(dbp);
    }

    /**
     * 生成代码
     * @param dbp
     * @return
     */
    @PostMapping("/autoCode")
    public Map<String, String> autoCode(DataBaseProperties dbp){
        return autoCodeService.autoCode(dbp);
    }


    /**
     * 下载文件
     * @param path
     * @return
     */
    @GetMapping("/getFile")
    public ResponseEntity<InputStreamResource> getFile(@RequestParam("path") String path){
        try {
            return autoCodeService.getFile(path);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
