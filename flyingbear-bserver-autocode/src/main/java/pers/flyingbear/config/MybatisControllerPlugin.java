package pers.flyingbear.config;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.mybatis.dynamic.sql.SqlBuilder;
import org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider;
import org.mybatis.dynamic.sql.render.RenderingStrategies;
import org.mybatis.dynamic.sql.update.render.UpdateStatementProvider;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.JavaTypeResolver;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.internal.types.JavaTypeResolverDefaultImpl;
import org.springframework.web.bind.annotation.RequestMapping;
import pers.flyingbear.vo.DataBaseProperties;

import java.time.LocalDateTime;
import java.util.*;

/**
 * 增强功能Mybatis3DynamicSqlImpl，生成controller和service
 */
public class MybatisControllerPlugin extends PluginAdapter {
    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    /**
     * 生成额外java文件
     * @param introspectedTable
     * @return
     */
    @Override
    public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {
        Context context = introspectedTable.getContext();
        JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = context.getJavaModelGeneratorConfiguration();
        String controllerPackage = MybatisConfig.BASEPACKAGE+".controller";
        String servicePackage = MybatisConfig.BASEPACKAGE+".service";
        String baseProject = javaModelGeneratorConfiguration.getTargetProject();

        CompilationUnit addServiceImpl = addServiceImpl(introspectedTable, servicePackage);
        CompilationUnit addController = addController(introspectedTable, controllerPackage, servicePackage);

        GeneratedJavaFile gjfServiceImpl = new GeneratedJavaFile(addServiceImpl, baseProject,
                this.context.getProperty("javaFileEncoding"), this.context.getJavaFormatter());
        GeneratedJavaFile gjfController = new GeneratedJavaFile(addController, baseProject,
                this.context.getProperty("javaFileEncoding"), this.context.getJavaFormatter());
        List<GeneratedJavaFile> list = new ArrayList<>();
        list.add(gjfServiceImpl);
        list.add(gjfController);
        return list;
    }

    /**
     * 生成service
     * @param introspectedTable
     * @param targetPackage
     * @return
     */
    protected CompilationUnit addServiceImpl(IntrospectedTable introspectedTable, String targetPackage) {
        //String entityExampleClazzType = introspectedTable.getExampleType();
        // 表名
        String domainObjectName = introspectedTable.getFullyQualifiedTable().getDomainObjectName();

        // 类型转换
        JavaTypeResolver javaTypeResolver = new MybatisJavaTypeResolver();
        FullyQualifiedJavaType idType = javaTypeResolver.calculateJavaType(introspectedTable.getPrimaryKeyColumns().get(0));

        StringBuilder builder = new StringBuilder();

        TopLevelClass clazz = new TopLevelClass(
                builder.delete(0, builder.length())
                        .append(targetPackage)
                        .append(".")
                        .append(domainObjectName)
                        .append("Service") // ServiceImpl
                        .toString()
        );
        clazz.setVisibility(JavaVisibility.PUBLIC);
        clazz.addAnnotation("@Service");
//        serviceImplClazz.addSuperInterface(implInterfaceType);
//        serviceImplClazz.setSuperClass(superClazzType);
        //        Interface serviceInterface = new Interface(
//                builder.delete(0, builder.length())
//                        .append(serviceSuperPackage)
//                        .append(".")
//                        .append(domainObjectName)
//                        .append("Service")
//                        .toString()
//        );
//
//        serviceInterface.addSuperInterface(superInterfaceType);


        // 导包
        FullyQualifiedJavaType entityType = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
        clazz.addImportedType(entityType); // 实体类
        clazz.addImportedType(new FullyQualifiedJavaType("org.springframework.stereotype.Service"));
        clazz.addImportedType(new FullyQualifiedJavaType("javax.annotation.Resource"));
        clazz.addImportedType(new FullyQualifiedJavaType("java.time.LocalDateTime"));
        FullyQualifiedJavaType listType = new FullyQualifiedJavaType("java.util.List");
        clazz.addImportedType(listType);
        clazz.addImportedType(new FullyQualifiedJavaType("com.github.pagehelper.PageHelper"));
        clazz.addImportedType(new FullyQualifiedJavaType("com.github.pagehelper.PageInfo"));
        clazz.addImportedType(new FullyQualifiedJavaType("org.springframework.transaction.annotation.Transactional"));

        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            clazz.addImportedType(new FullyQualifiedJavaType("java.util.Objects"));
            clazz.addImportedType(new FullyQualifiedJavaType("org.mybatis.dynamic.sql.SqlBuilder"));
            clazz.addImportedType(new FullyQualifiedJavaType("org.mybatis.dynamic.sql.delete.render.DeleteStatementProvider"));
            clazz.addImportedType(new FullyQualifiedJavaType("org.mybatis.dynamic.sql.render.RenderingStrategies"));
            clazz.addImportedType(new FullyQualifiedJavaType("org.mybatis.dynamic.sql.select.render.SelectStatementProvider"));
            clazz.addImportedType(new FullyQualifiedJavaType("org.mybatis.dynamic.sql.update.render.UpdateStatementProvider"));
            clazz.addStaticImport("org.mybatis.dynamic.sql.SqlBuilder.*");
            clazz.addStaticImport("org.mybatis.dynamic.sql.select.SelectDSL.select");
            // ------【SqlSupport导入】------
            String sqlSupport = introspectedTable.getMyBatisDynamicSqlSupportType();
            clazz.addStaticImport(sqlSupport+".*");
        }


        // ------日志属性【可使用小辣椒】------
        FullyQualifiedJavaType logType = new FullyQualifiedJavaType("org.slf4j.Logger");
        clazz.addImportedType(logType);
        clazz.addImportedType(new FullyQualifiedJavaType("org.slf4j.LoggerFactory"));
        Field logField = new Field("log", logType);
        logField.setVisibility(JavaVisibility.PRIVATE);
        logField.setStatic(true);
        logField.setFinal(true);
        logField.setInitializationString(
                builder.delete(0, builder.length())
                        .append("LoggerFactory.getLogger(")
                        .append(domainObjectName)
                        .append("Service.class)")
                        .toString()
        );
        logField.addAnnotation("");
        //logField.addAnnotation("@SuppressWarnings(\"unused\")");
        clazz.addField(logField);


        // ------mapper属性------
        String javaMapperType = introspectedTable.getMyBatis3JavaMapperType();
        String mapperName = builder.delete(0, builder.length())
                .append(Character.toLowerCase(domainObjectName.charAt(0)))
                .append(domainObjectName.substring(1))
                .append("Mapper")
                .toString();
        FullyQualifiedJavaType JavaMapperType = new FullyQualifiedJavaType(javaMapperType);
        Field mapperField = new Field(mapperName, JavaMapperType);
        mapperField.setVisibility(JavaVisibility.PRIVATE);
        mapperField.addAnnotation("@Resource");
        clazz.addField(mapperField);
        clazz.addImportedType(JavaMapperType);


        // ------【新增方法】------
        FullyQualifiedJavaType returnType = new FullyQualifiedJavaType(MybatisConfig.APIRESPONSE);
        String p1Name = builder.delete(0, builder.length())
                .append(Character.toLowerCase(domainObjectName.charAt(0)))
                .append(domainObjectName.substring(1))
                .toString();
        clazz.addImportedType(returnType);


        Method insertMethod = new Method("insert");
        insertMethod.setVisibility(JavaVisibility.PUBLIC);
        insertMethod.setReturnType(returnType); // 返回类型
        Parameter p1 = new Parameter(entityType, "row");
        insertMethod.addParameter(p1);
        insertMethod.addAnnotation("@Transactional(rollbackFor = Exception.class)");
        insertMethod.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
        insertMethod.addBodyLine("int rows = "+mapperName+".insert(row);");
        insertMethod.addBodyLine("if(rows > 0 ){");
        insertMethod.addBodyLine("  return ApiResponse.success(\"新增成功\");");
        insertMethod.addBodyLine("} else {");
        insertMethod.addBodyLine("  return ApiResponse.error(\"新增失败\");");
        insertMethod.addBodyLine("}");
        clazz.addMethod(insertMethod);



        //------【编辑方法】------
        Method updateMethod = new Method("updateById");
        updateMethod.setVisibility(JavaVisibility.PUBLIC);
        updateMethod.setReturnType(returnType); // 返回类型
        Parameter p2 = new Parameter(entityType, "row");
        updateMethod.addParameter(p2);
        //DataBaseProperties dbp = MybatisConfig.dbp;
        updateMethod.addAnnotation("@Transactional(rollbackFor = Exception.class)");
        updateMethod.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            updateMethod.addBodyLine("UpdateStatementProvider update = SqlBuilder.update("+p1Name+")");
            updateMethod.addBodyLine("          //.set(nickName).equalTo(nickName1)"); // ------更新字段
            updateMethod.addBodyLine("          .where(id, isEqualTo(row.getId()))"); // 查询条件
            updateMethod.addBodyLine("          .build().render(RenderingStrategies.MYBATIS3);");
            updateMethod.addBodyLine("int rows = "+mapperName+".update(update);");
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            updateMethod.addBodyLine("int rows = "+mapperName+".updateById(row);");
        }
        updateMethod.addBodyLine("if(rows > 0 ){");
        updateMethod.addBodyLine("  return ApiResponse.success(\"更新成功\");");
        updateMethod.addBodyLine("} else {");
        updateMethod.addBodyLine("  return ApiResponse.error(\"更新失败\");");
        updateMethod.addBodyLine("}");
        clazz.addMethod(updateMethod);
        // mybatis3.2 之后 SqlBuilder 和 SelectBuilder (已经废弃)


        //------【删除方法】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method deleteMethod = new Method("delete");
            deleteMethod.setVisibility(JavaVisibility.PUBLIC);
            deleteMethod.setReturnType(returnType); // 返回类型
            //FullyQualifiedJavaType stringType = new FullyQualifiedJavaType("java.lang.String");
            Parameter p3 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
            deleteMethod.addParameter(p3);
            deleteMethod.addAnnotation("@Transactional(rollbackFor = Exception.class)");
            deleteMethod.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
            deleteMethod.addBodyLine("DeleteStatementProvider del = SqlBuilder.deleteFrom("+p1Name+")");
            deleteMethod.addBodyLine("          //.where(account, isEqualTo(param))"); // 查询条件
            deleteMethod.addBodyLine("          .build().render(RenderingStrategies.MYBATIS3);");
            deleteMethod.addBodyLine("int rows = "+mapperName+".delete(del);");
            deleteMethod.addBodyLine("return ApiResponse.success(\"删除成功\");");
            clazz.addMethod(deleteMethod);
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){

        }



        //------【主键删除方法】------
        Method deleteMethod2 = new Method("deleteById");
        deleteMethod2.setVisibility(JavaVisibility.PUBLIC);
        deleteMethod2.setReturnType(returnType); // 返回类型
        Parameter p4 = new Parameter(idType, "id");
        deleteMethod2.addParameter(p4);
        deleteMethod2.addAnnotation("@Transactional(rollbackFor = Exception.class)");
        deleteMethod2.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            deleteMethod2.addBodyLine("int rows = "+mapperName+".deleteByPrimaryKey(id);");
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            deleteMethod2.addBodyLine("int rows = "+mapperName+".deleteById(id);");
        }
        deleteMethod2.addBodyLine("return ApiResponse.success(\"删除成功\");");
        clazz.addMethod(deleteMethod2);



        //------【根据主键查询】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){

        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method selectOne = new Method("selectById");
            selectOne.setVisibility(JavaVisibility.PUBLIC);
            selectOne.setReturnType(returnType); // 返回类型
            Parameter ps = new Parameter(idType, "id");
            selectOne.addParameter(ps);
            selectOne.addBodyLine("if(id == null){return ApiResponse.error(\"传入参数不正确\");}");
            selectOne.addBodyLine("return ApiResponse.ok("+mapperName+".selectById(id));");
            clazz.addMethod(selectOne);
        }


        //------【查询方法】------
        Method selectMethod1 = new Method("selectAll");
        selectMethod1.setVisibility(JavaVisibility.PUBLIC);
        selectMethod1.setReturnType(new FullyQualifiedJavaType("java.util.List<"+domainObjectName+">"));
        Parameter p5 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
        selectMethod1.addParameter(p5);
        selectMethod1.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            selectMethod1.addBodyLine("SelectStatementProvider select = SqlBuilder.select("+p1Name+".allColumns())");
            selectMethod1.addBodyLine("         .from("+p1Name+")");
            selectMethod1.addBodyLine("         //.where(account, isLike(param).filter(Objects::isNull).map(s -> \"%\" + s + \"%\"))");
            selectMethod1.addBodyLine("         .orderBy(id.descending())");//.and(state, isEqualTo(0))
            selectMethod1.addBodyLine("         .build().render(RenderingStrategies.MYBATIS3);");
            selectMethod1.addBodyLine("return "+mapperName+".selectMany(select);");
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            selectMethod1.addBodyLine("return "+mapperName+".selectAll();");
        }
        clazz.addMethod(selectMethod1);



        //------【分页查询方法】------
        Method selectMethod2 = new Method("listPage");
        selectMethod2.setVisibility(JavaVisibility.PUBLIC);
        selectMethod2.setReturnType(new FullyQualifiedJavaType("java.util.Map<String, Object>"));
        clazz.addImportedType(new FullyQualifiedJavaType("java.util.Map"));
        Parameter p6 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
        selectMethod2.addParameter(p6);
        selectMethod2.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
        selectMethod2.addBodyLine("PageHelper.startPage(MyStringUtil.getPage(param.get(MyDataGrid.PAGE)), MyStringUtil.getRow(param.get(MyDataGrid.ROW)));");
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            selectMethod2.addBodyLine("SelectStatementProvider select = SqlBuilder.select("+p1Name+".allColumns())");
            selectMethod2.addBodyLine("         .from("+p1Name+")");
            selectMethod2.addBodyLine("         //.where(account, isLike(p1).filter(Objects::isNull).map(s -> \"%\" + s + \"%\"))");
            selectMethod2.addBodyLine("         .orderBy(id.descending())");//.and(state, isEqualTo(0))
            selectMethod2.addBodyLine("         .build().render(RenderingStrategies.MYBATIS3);");
            selectMethod2.addBodyLine("List<"+domainObjectName+"> list =  "+mapperName+".selectMany(select);");
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            selectMethod2.addBodyLine("List<"+domainObjectName+"> list =  "+mapperName+".selectAll();");
        }
        selectMethod2.addBodyLine("PageInfo<"+domainObjectName+"> pageInfo = new PageInfo<>(list);");
        selectMethod2.addBodyLine("return MyDataGrid.getResult(list, pageInfo.getTotal());");
        clazz.addImportedType(new FullyQualifiedJavaType(MybatisConfig.MYDATAGRID));
        clazz.addImportedType(new FullyQualifiedJavaType(MybatisConfig.MYSTRINGUTIL));
        clazz.addMethod(selectMethod2);


        //------【查询总条数方法】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method countMethod = new Method("getCount");
            countMethod.setVisibility(JavaVisibility.PUBLIC);
            countMethod.setReturnType(returnType); // 返回类型
            countMethod.addBodyLine("// 验证和特殊处理需要自己写，LocalDateTime.now()");
            countMethod.addBodyLine("SelectStatementProvider select = select(count("+p1Name+".id))");
            countMethod.addBodyLine("         .from("+p1Name+")");
            //countMethod.addBodyLine("         .where(account, isLike(p1).filter(Objects::isNull).map(s -> \"%\" + s + \"%\"))");
            countMethod.addBodyLine("         .build().render(RenderingStrategies.MYBATIS3);");
            countMethod.addBodyLine("long count =  "+mapperName+".count(select);");
            countMethod.addBodyLine("return ApiResponse.ok(count);");
            clazz.addMethod(countMethod);
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){

        }
        return clazz;
    }

    /**
     * 生成controller
     * @param introspectedTable
     * @param targetPackage
     * @return
     */
    protected CompilationUnit addController(IntrospectedTable introspectedTable, String targetPackage, String servicePackage) {
        String entityExampleClazzType = introspectedTable.getExampleType();
        String domainObjectName = introspectedTable.getFullyQualifiedTable().getDomainObjectName();

        // 类型转换
        JavaTypeResolver javaTypeResolver = new MybatisJavaTypeResolver();
        FullyQualifiedJavaType idType = javaTypeResolver.calculateJavaType(introspectedTable.getPrimaryKeyColumns().get(0));

        StringBuilder builder = new StringBuilder();

        TopLevelClass clazz = new TopLevelClass(
                builder.delete(0, builder.length())
                        .append(targetPackage)
                        .append(".")
                        .append(domainObjectName)
                        .append("Controller")
                        .toString()
        );
        clazz.setVisibility(JavaVisibility.PUBLIC);
        clazz.addAnnotation("@RestController");
        String p1Name = builder.delete(0, builder.length())
                .append(Character.toLowerCase(domainObjectName.charAt(0)))
                .append(domainObjectName.substring(1))
                .toString();
        clazz.addAnnotation("@RequestMapping(\"/"+p1Name+"\")");


        // 导包
        clazz.addImportedType(new FullyQualifiedJavaType("org.springframework.web.bind.annotation.RestController"));
        FullyQualifiedJavaType entityType = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
        clazz.addImportedType(entityType); // 实体类
        FullyQualifiedJavaType returnType = new FullyQualifiedJavaType(MybatisConfig.APIRESPONSE);
        clazz.addImportedType(returnType);
        clazz.addImportedType(new FullyQualifiedJavaType("javax.annotation.Resource"));
        FullyQualifiedJavaType listType = new FullyQualifiedJavaType("java.util.List");
        clazz.addImportedType(listType);
        String service = builder.delete(0, builder.length()).append(servicePackage).append(".").append(domainObjectName).append("Service").toString();
        FullyQualifiedJavaType serviceType = new FullyQualifiedJavaType(service);
        clazz.addImportedType(serviceType);
        FullyQualifiedJavaType requestType = new FullyQualifiedJavaType("org.springframework.web.bind.annotation.RequestMapping");
        clazz.addImportedType(requestType);
        FullyQualifiedJavaType getType = new FullyQualifiedJavaType("org.springframework.web.bind.annotation.GetMapping");
        clazz.addImportedType(getType);
        FullyQualifiedJavaType postType = new FullyQualifiedJavaType("org.springframework.web.bind.annotation.PostMapping");
        clazz.addImportedType(postType);



        // ------日志属性【可使用小辣椒】------
        FullyQualifiedJavaType logType = new FullyQualifiedJavaType("org.slf4j.Logger");
        clazz.addImportedType(logType);
        clazz.addImportedType(new FullyQualifiedJavaType("org.slf4j.LoggerFactory"));
        Field logField = new Field("log", logType);
        logField.setVisibility(JavaVisibility.PRIVATE);
        logField.setStatic(true);
        logField.setFinal(true);
        logField.setInitializationString(
                builder.delete(0, builder.length())
                        .append("LoggerFactory.getLogger(")
                        .append(domainObjectName)
                        .append("Controller.class)")
                        .toString()
        );
        logField.addAnnotation("");
        //logField.addAnnotation("@SuppressWarnings(\"unused\")");
        clazz.addField(logField);



        // ------service属性------
        String serviceName = builder.delete(0, builder.length())
                .append(Character.toLowerCase(domainObjectName.charAt(0)))
                .append(domainObjectName.substring(1))
                .append("Service")
                .toString();
        Field serviceField = new Field(serviceName, serviceType);
        serviceField.setVisibility(JavaVisibility.PRIVATE);
        serviceField.addAnnotation("@Resource");
        clazz.addField(serviceField);


        // ------【新增方法】------
        Method insertMethod = new Method("insert");
        insertMethod.setVisibility(JavaVisibility.PUBLIC);
        insertMethod.setReturnType(returnType); // 返回类型
        Parameter p1 = new Parameter(entityType, p1Name);
        insertMethod.addParameter(p1);
        insertMethod.addBodyLine("return "+serviceName+".insert("+p1Name+");");
        insertMethod.addAnnotation("@PostMapping(\"/save\")");
        clazz.addMethod(insertMethod);



        //------【编辑方法】------
        Method updateMethod = new Method("updateById");
        updateMethod.setVisibility(JavaVisibility.PUBLIC);
        updateMethod.setReturnType(returnType); // 返回类型
        Parameter p2 = new Parameter(entityType, p1Name);
        updateMethod.addParameter(p2);
        updateMethod.addBodyLine("return "+serviceName+".updateById("+p1Name+");");
        updateMethod.addAnnotation("@PostMapping(\"/update\")");
        clazz.addMethod(updateMethod);


        //------【删除方法】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method deleteMethod = new Method("delete");
            deleteMethod.setVisibility(JavaVisibility.PUBLIC);
            deleteMethod.setReturnType(returnType); // 返回类型
            FullyQualifiedJavaType stringType = new FullyQualifiedJavaType("java.lang.String");
            Parameter p3 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
            deleteMethod.addParameter(p3);
            deleteMethod.addBodyLine("return "+serviceName+".delete(param);");
            deleteMethod.addAnnotation("@PostMapping(\"/deleteCondition\")");
            clazz.addMethod(deleteMethod);
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){

        }



        //------【主键删除方法】------
        Method deleteMethod2 = new Method("deleteById");
        deleteMethod2.setVisibility(JavaVisibility.PUBLIC);
        deleteMethod2.setReturnType(returnType); // 返回类型
        Parameter p4 = new Parameter(idType, "id");
        deleteMethod2.addParameter(p4);
        deleteMethod2.addBodyLine("return "+serviceName+".deleteById(id);");
        deleteMethod2.addAnnotation("@PostMapping(\"/delete\")");
        clazz.addMethod(deleteMethod2);



        //------【根据主键查询】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){

        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method selectOne = new Method("selectById");
            selectOne.setVisibility(JavaVisibility.PUBLIC);
            selectOne.setReturnType(returnType); // 返回类型
            Parameter ps = new Parameter(idType, "id");
            selectOne.addParameter(ps);
            selectOne.addBodyLine("return "+serviceName+".selectById(id);");
            selectOne.addAnnotation("@PostMapping(\"/query\")");
            clazz.addMethod(selectOne);
        }


        //------【查询方法】------
        Method selectMethod1 = new Method("selectAll");
        selectMethod1.setVisibility(JavaVisibility.PUBLIC);
        selectMethod1.setReturnType(new FullyQualifiedJavaType("java.util.List<"+domainObjectName+">"));
        Parameter p5 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
        selectMethod1.addParameter(p5);
        selectMethod1.addBodyLine("return "+serviceName+".selectAll(param);");
        selectMethod1.addAnnotation("@GetMapping(\"/list\")");
        clazz.addMethod(selectMethod1);


        //------【分页查询方法】------
        Method selectMethod2 = new Method("listPage");
        selectMethod2.setVisibility(JavaVisibility.PUBLIC);
        selectMethod2.setReturnType(new FullyQualifiedJavaType("java.util.Map<String, Object>"));
        clazz.addImportedType(new FullyQualifiedJavaType("java.util.Map"));
        Parameter p6 = new Parameter(new FullyQualifiedJavaType("java.util.Map<String, Object>"), "param");
        selectMethod2.addParameter(p6);
        selectMethod2.addBodyLine("return "+serviceName+".listPage(param);");
        selectMethod2.addAnnotation("@GetMapping(\"/listPage\")");
        clazz.addMethod(selectMethod2);


        //------【查询总条数方法】------
        if("MyBatis3DynamicSql".equals(MybatisConfig.DBP.getTargetRuntime())){
            Method countMethod = new Method("getCount");
            countMethod.setVisibility(JavaVisibility.PUBLIC);
            countMethod.setReturnType(returnType); // 返回类型
            countMethod.addBodyLine("return "+serviceName+".getCount();");
            countMethod.addAnnotation("@GetMapping(\"/count\")");
            clazz.addMethod(countMethod);
        }else if("MyBatis3Simple".equals(MybatisConfig.DBP.getTargetRuntime())){

        }
        return clazz;
    }
}
