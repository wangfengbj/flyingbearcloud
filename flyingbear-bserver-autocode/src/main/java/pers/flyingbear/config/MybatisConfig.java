package pers.flyingbear.config;

import org.apache.commons.lang3.time.DateFormatUtils;
import pers.flyingbear.vo.DataBaseProperties;

import java.util.Date;

public class MybatisConfig {
    public static String AUTHOR = "WangFeng"; // 作者
    public static String BASEPACKAGE = ""; // 包根路径
    public static String getCurDate() {
        return DateFormatUtils.format(new Date(), "yyyy-MM-dd");
    }
    public static String APIRESPONSE = "pers.flyingbear.tool.model.ApiResponse";
    public static String MYDATAGRID = "pers.flyingbear.tool.constant.MyDataGrid";
    public static String MYASSERT = "pers.flyingbear.tool.util.MyAssert";
    public static String MYSTRINGUTIL = "pers.flyingbear.tool.util.MyStringUtil";
    public static DataBaseProperties DBP = null; // 页面配置信息
    public static boolean LOMBOK = false; // 是否小辣椒
}
