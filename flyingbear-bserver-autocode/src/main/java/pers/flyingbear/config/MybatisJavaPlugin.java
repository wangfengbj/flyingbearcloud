package pers.flyingbear.config;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.*;
import org.mybatis.generator.codegen.mybatis3.javamapper.elements.AbstractJavaMapperMethodGenerator;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * mybatis原生扩展插件
 */
public class MybatisJavaPlugin extends PluginAdapter {
    private static final Logger log = LoggerFactory.getLogger(MybatisJavaPlugin.class);
    private static final String PK = "PrimaryKey";
    private static final String ID = "Id";

    @Override
    public boolean validate(List<String> warnings) {
        return true;
    }

    /**
     * entity扩展
     * @param topLevelClass
     * @param introspectedTable
     * @return
     */
    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        if(MybatisConfig.LOMBOK){
            topLevelClass.addAnnotation("@Data"); // 使用注解使用小辣椒
            topLevelClass.addAnnotation("@Builder");
            topLevelClass.addAnnotation("@NoArgsConstructor");
            topLevelClass.addAnnotation("@AllArgsConstructor");

            topLevelClass.addImportedType("lombok.Data");
            topLevelClass.addImportedType("lombok.Builder");
            topLevelClass.addImportedType("lombok.NoArgsConstructor");
            topLevelClass.addImportedType("lombok.AllArgsConstructor");
        }
        return super.modelBaseRecordClassGenerated(topLevelClass, introspectedTable);
    }

    /**
     * 生成每一个属性的getter方法时候调用，如果我们不想生成getter，直接返回false即可
     * @param method
     * @param topLevelClass
     * @param introspectedColumn
     * @param introspectedTable
     * @param modelClassType
     * @return
     */
    @Override
    public boolean modelGetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {
        if (MybatisConfig.LOMBOK) { // 小辣椒不要get方法
            return false;
        } else {
            return true;
        }
    }

    /**
     * 生成每一个属性的setter方法时候调用，如果我们不想生成setter，直接返回false即可
     * @param method
     * @param topLevelClass
     * @param introspectedColumn
     * @param introspectedTable
     * @param modelClassType
     * @return
     */
    @Override
    public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn, IntrospectedTable introspectedTable, ModelClassType modelClassType) {
        if (MybatisConfig.LOMBOK) { // 小辣椒不要set方法
            return false;
        } else {
            return true;
        }
    }


    /**
     * xml文件
     * @param document
     * @param introspectedTable
     * @return
     */
    @Override
    public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
        AbstractXmlElementGenerator plugin = new AbstractXmlElementGenerator() {
            @Override
            public void addElements(XmlElement parentElement) {
                Field f = null;
                try {
                    f = Attribute.class.getDeclaredField("value");
                    ReflectionUtils.makeAccessible(f);
                    log.debug(Attribute.class.getName() + " exists declared field 'value'");
                } catch (NoSuchFieldException e) {
                    throw new IllegalStateException(
                            Attribute.class.getName() + " not exists declared field 'value'");
                }
                for (VisitableElement e : parentElement.getElements()) {
                    if (e instanceof XmlElement) {
                        XmlElement x = (XmlElement) e;
                        if (x.getName().equals("insert")
                                || x.getName().equals("delete")
                                || x.getName().equals("update")
                                || x.getName().equals("select")) {
                            for (Attribute a : x.getAttributes()) {
                                if (a.getName().equals("id") && a.getValue().endsWith(PK)) {
                                    ReflectionUtils.setField(f, a, renamePkToId(a.getValue()));
                                }
                            }
                        }
                    }
                }
            }
        };

        plugin.setContext(context);
        plugin.setIntrospectedTable(introspectedTable);
        plugin.addElements(document.getRootElement());

        return super.sqlMapDocumentGenerated(document, introspectedTable);
    }


    /**
     * MAPPER类
     * @param theInterface
     * @param introspectedTable
     * @return
     */
    @Override
    public boolean clientGenerated(Interface theInterface, IntrospectedTable introspectedTable) {
        theInterface.addAnnotation("@Mapper");
        theInterface.addImportedType(new FullyQualifiedJavaType("org.apache.ibatis.annotations.Mapper"));
        theInterface.addImportedType(new FullyQualifiedJavaType("org.apache.ibatis.annotations.Param"));

        AbstractJavaMapperMethodGenerator plugin = new AbstractJavaMapperMethodGenerator() {
            @Override
            public void addInterfaceElements(Interface interfaze) {
                for (Method e : interfaze.getMethods()) {
                    if (e.getName().endsWith(PK)) {
                        e.setName(renamePkToId(e.getName()));
                    }
                }
            }
        };

        plugin.setContext(context);
        plugin.setIntrospectedTable(introspectedTable);
        plugin.addInterfaceElements(theInterface);
        return super.clientGenerated(theInterface, introspectedTable);
    }


//    @Override
//    public boolean sqlMapSelectByExampleWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
//        XmlElement isNotNullElement1 = new XmlElement("if");
//        isNotNullElement1.addAttribute(new Attribute("test", "groupByClause != null"));
//        isNotNullElement1.addElement(new TextElement("group by ${groupByClause}"));
//        element.addElement(5, isNotNullElement1);
//        XmlElement isNotNullElement = new XmlElement("if");
//        isNotNullElement.addAttribute(new Attribute("test", "pageInfo != null"));
//        isNotNullElement.addElement(new TextElement("limit #{pageInfo.pageBegin} , #{pageInfo.pageSize}"));
//        element.addElement(isNotNullElement);
//        return super.sqlMapSelectByExampleWithoutBLOBsElementGenerated(element, introspectedTable);
//    }


    /**
     * 替换公用方法
     * @param name
     * @return
     */
    private static String renamePkToId(String name) {
        if (name.endsWith(PK)) { name = name.substring(0, name.length() - PK.length()) + ID; }
        return name;
    }
}