package pers.flyingbear.config;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.internal.DefaultCommentGenerator;
import org.mybatis.generator.internal.util.StringUtility;

import java.text.SimpleDateFormat;
import java.util.Properties;

/**
 * mybatis自定义注释
 */
public class MybatisCommentGenerator extends DefaultCommentGenerator {
    private Properties properties;
    private boolean suppressDate;
    private boolean suppressAllComments;
    private boolean addRemarkComments = false;

    public MybatisCommentGenerator() {
        this.properties = new Properties();
        this.suppressDate = false;
        this.suppressAllComments = false;
    }


    /**
     * 版权信息
     * @param compilationUnit
     */
//    @Override
//    public void addJavaFileComment(CompilationUnit compilationUnit) {
//        compilationUnit.addFileCommentLine("/*** copyright (c) 2022 FlyingBear  ***/");
//    }




    /**
     * 对类的注解
     * @param topLevelClass
     * @param introspectedTable
     */
    @Override
    public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        topLevelClass.addJavaDocLine("/**");

        StringBuilder sb = new StringBuilder();
        sb.append(" * Model对应的数据表是 : ");
        sb.append(introspectedTable.getFullyQualifiedTable());
        topLevelClass.addJavaDocLine(sb.toString());

        String tableRemarks = introspectedTable.getRemarks();
        if (!StringUtils.isEmpty(tableRemarks)) {
            sb.setLength(0);
            sb.append(" * 数据表注释 : ");
            sb.append(tableRemarks);
            topLevelClass.addJavaDocLine(sb.toString());
        }

        sb.setLength(0);
        sb.append(" * @author " + MybatisConfig.AUTHOR);
        topLevelClass.addJavaDocLine(sb.toString());

        sb.setLength(0);
        sb.append(" * @date ");
        sb.append(MybatisConfig.getCurDate());
        topLevelClass.addJavaDocLine(sb.toString());

        topLevelClass.addJavaDocLine(" */");
    }


    /**
     * 实体类get方法注释
     */
//    public void addGetterComment(Method method, IntrospectedTable introspectedTable,
//                                 IntrospectedColumn introspectedColumn) {
//        if (this.suppressAllComments) {
//            return;
//        }
//        method.addJavaDocLine("/**");
//        method.addJavaDocLine(" * @return " + introspectedTable.getFullyQualifiedTable().getAlias() + " : " + introspectedColumn.getRemarks());
//        method.addJavaDocLine(" */");
//    }


    /**
     * 实体类set方法注释
     */
//    public void addSetterComment(Method method, IntrospectedTable introspectedTable,
//                                 IntrospectedColumn introspectedColumn) {
//        if (this.suppressAllComments) {
//            return;
//        }
//        StringBuilder sb = new StringBuilder();
//        method.addJavaDocLine("/**");
//        Parameter param = method.getParameters().get(0);
//        sb.append(" * @param ");
//        sb.append(param.getName());
//        sb.append(" : ");
//        sb.append(introspectedColumn.getRemarks());
//        method.addJavaDocLine(sb.toString());
//        method.addJavaDocLine(" */");
//    }


    /**
     * 生成的实体增加字段的中文注释
     */
    public void addFieldComment(Field field, IntrospectedTable introspectedTable,
                                IntrospectedColumn introspectedColumn) {
        if (this.suppressAllComments) {
            return;
        }
        String bz = introspectedColumn.getRemarks();
        if(bz != null && bz.length() > 0){
            field.addJavaDocLine("/**");
            StringBuilder sb = new StringBuilder();
            sb.append(" * ");
            sb.append(bz);
            field.addJavaDocLine(sb.toString().replace("\n", " "));
            field.addJavaDocLine(" */");
        }
    }
}
