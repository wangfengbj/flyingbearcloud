package pers.flyingbear;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoCodeServer {
    public static void main(String[] args) {
        SpringApplication.run(AutoCodeServer.class, args);
    }
}
