package pers.flyingbear.constant;

import java.util.Locale;

public enum DbEnum {
    MYSQL(Const.DB_MYSQL, "com.mysql.cj.jdbc.Driver", "jdbc:mysql://", "?useUnicode=true&characterEncoding=utf-8&useInformationSchema=true&useSSL=false&serverTimezone=Asia/Shanghai");

    private String type;
    private String driver;
    private String prefix;
    private String suffix;

    DbEnum(String type, String driver, String prefix, String suffix) {
        this.type = type;
        this.driver = driver;
        this.prefix = prefix;
        this.suffix = suffix;
    }

    public String getType() {
        return type;
    }

    public String getDriver() {
        return driver;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public static DbEnum getDb(String type){
        if(type == null){ return null; }
        for (DbEnum dbEnum : values()) {
            if (dbEnum.getType().equals(type.toLowerCase(Locale.ROOT))) {
                return dbEnum;
            }
        }
        return null;
    }
}
