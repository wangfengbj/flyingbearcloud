package pers.flyingbear.constant;

import org.mybatis.generator.config.ModelType;
import org.mybatis.generator.internal.util.messages.Messages;

public enum TargetRuntimeEnum {
    MyBatis3,
    MyBatis3Simple,
    MyBatis3DynamicSql,
    MyBatis3DynamicSqlV1,
    MyBatis3Kotlin;

    public static TargetRuntimeEnum getTargetRuntime(String type) {
        if (MyBatis3.name().equalsIgnoreCase(type)) {
            return MyBatis3;
        } else if (MyBatis3Simple.name().equalsIgnoreCase(type)) {
            return MyBatis3Simple;
        } else if (MyBatis3DynamicSql.name().equalsIgnoreCase(type)) {
            return MyBatis3DynamicSql;
        } else if (MyBatis3DynamicSqlV1.name().equalsIgnoreCase(type)) {
            return MyBatis3DynamicSqlV1;
        } else if (MyBatis3Kotlin.name().equalsIgnoreCase(type)) {
            return MyBatis3Kotlin;
        } else {
            throw new RuntimeException(Messages.getString("RuntimeError.13", type));
        }
    }
}
