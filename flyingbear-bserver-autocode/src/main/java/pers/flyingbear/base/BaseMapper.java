package pers.flyingbear.base;

public interface BaseMapper<T, Example, ID> {
    long countByExample(Example example);
    int deleteByPrimaryKey(ID id);
    int insert(T record);
}
