package pers.flyingbear.base;

public interface BaseService <T, Example extends BaseExample, ID>{
    long countByExample(Example example);
    int deleteByPrimaryKey(ID id);
    int insert(T record);
}
