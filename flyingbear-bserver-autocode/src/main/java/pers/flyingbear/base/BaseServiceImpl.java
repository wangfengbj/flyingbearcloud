package pers.flyingbear.base;

public abstract class BaseServiceImpl<T,Example extends BaseExample, ID> implements BaseService<T, Example, ID> {

    private BaseMapper<T, Example, ID> mapper;

    public void setMapper(BaseMapper<T, Example, ID> mapper) {
        this.mapper = mapper;
    }

    @Override
    public long countByExample(Example example) {
        return mapper.countByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(ID id) {
        return mapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(T record) {
        return mapper.insert(record);
    }
}
