package pers.flyingbear.util;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import pers.flyingbear.constant.DbEnum;
import pers.flyingbear.vo.DataBaseProperties;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;

public class MyBatisPlusGeneratorUtil {
    private MyBatisPlusGeneratorUtil(){}

    /**
     *
     * @param dbp
     * @param is_customize  true表示自定义
     * @return
     */
    public static Map<String, String> generator(DataBaseProperties dbp, boolean is_customize){
        Map<String, String> rst = new HashMap<>();

        String path = MyBatisPlusGeneratorUtil.class.getClassLoader().getResource("").getPath();
        //String srcPath = File.separator + new Date().getTime();
        String srcPath = String.valueOf(System.currentTimeMillis());
        String buildPath = path + srcPath;
        File file = new File(buildPath);
        if(!file.exists()){
            file.mkdir();
        }


        // 代码生成器，生成 Mapper、Model、Service、Controller层的代码。AbstractTemplateEngine
        AutoGenerator mpg = new AutoGenerator();
        // 1. 全局配置
        GlobalConfig config = new GlobalConfig();
        config.setAuthor("flyingBear") // 作者
                .setOutputDir(buildPath) // 生成路径
                .setFileOverride(true) // 文件覆盖
                .setOpen(false) // 执行完，是否打开输出目录
                .setEnableCache(false) // XML 二级缓存
                .setKotlin(false) // 开启 Kotlin 模式
                .setSwagger2(false) // 开启 swagger2 模式
                // .setActiveRecord(true)// 每一个类的实例对象唯一对应一个数据库表的一行(一对一关系)
                .setBaseResultMap(true)// XML 映射文件中是否生成ResultMap配置
                .setBaseColumnList(true)// XML 生成通用sql字段
                .setDateType(DateType.ONLY_DATE) // 时间类型对应策略,默认值：TIME_PACK， 设置日期类型为Date【DateType.ONLY_DATE】(若不设置时间类型都会变成LocalDateTime部分连接池例如druid是无法识别的)
                // .setEntityName("%sEntity") //实体命名方式,默认值null。例如：%sEntity 生成 UserEntity
                // .setMapperName("%sDao") //mapper命名方式。例如：%sDao 生成 UserDao
                // .setXmlName(xmlName) //Mapper xml 命名方式。例如：%sDao 生成 UserDao.xml
                .setServiceName("%sService") // 设置生成的service接口的名字的首字母是否为I，加%s则不生成I
                // .setServiceImplName(serviceImplName) //例如：%sBusinessImpl 生成 UserBusinessImpl
                // .setControllerName(controllerName) //例如：%sAction 生成 UserAction
                .setIdType(IdType.AUTO); // 主键策略
        mpg.setGlobalConfig(config);


        // 2. 数据源配置
        DbEnum dbe = DbUtil.getSqlConfig(dbp.getDbType());
        DataSourceConfig dsConfig = new DataSourceConfig();
        dsConfig.setDbType(DbType.getDbType(dbp.getDbType())) // 设置数据库类型，DbType为plus提供的枚举类
                .setUrl(dbe.getPrefix() + dbp.getIp() + ":" + dbp.getPort() + "/" + dbp.getDataBase() + dbe.getSuffix()) // 设置连接路径
                .setDriverName(dbe.getDriver()) // 设置驱动
                .setUsername(dbp.getUsername()) // 设置用户名
                .setPassword(dbp.getPassword()); // 设置密码
        // 自定义类型转换
        dsConfig.setTypeConvert(new MySqlTypeConvert() {
            @Override
            public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                System.out.println("自定义转换类型：" + fieldType);
                String t = fieldType.toLowerCase();
                if(t.contains("tinyint")) {
                    return DbColumnType.BYTE;
                }else if(t.contains("int")){
                    return DbColumnType.LONG;
                }

                // 自定义类型转换，如需要要自己定义
                return super.processTypeConvert(globalConfig, fieldType);
            }
        });
        mpg.setDataSource(dsConfig);


        // 3. 包名策略配置
        PackageConfig pkConfig = new PackageConfig();
        pkConfig.setParent(dbp.getBasePackage())// 顶级包结构
                .setMapper("mapper") // 数据访问层
                .setService("service") // 业务逻辑层
                .setController("controller") // 控制器
                .setEntity("po") // 实体类
                .setXml("mapper")// mapper映射文件，这里跟数据访问层一个包
                .setModuleName("");
        //.setModuleName(scanner("模块名"));// 模块名;
        mpg.setPackageInfo(pkConfig);


        boolean lombok = "on".equals(dbp.getLombok())? true : false;
        String[] tableNames = new String[]{dbp.getTableName()};
        // 4. 策略配置
        StrategyConfig stConfig = new StrategyConfig();
        stConfig.setCapitalMode(true) // 全局大写命名，ORACLE注意
                .setNaming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略下划线转驼峰
                .setColumnNaming(NamingStrategy.underline_to_camel) // 列明下划线转驼峰
                //.setSuperEntityClass(String.class)   // 父类
                .setEntityLombokModel(lombok) // 小辣椒
                //.setSuperControllerClass("你自己的父类控制器,没有就不用设置!")
                .setRestControllerStyle(true)  // rest控制器
                //.setSuperEntityColumns("id") // 自定公共字段
                //.setTablePrefix(tablePrefix) // 表前缀，如t_user
                //.setControllerMappingHyphenStyle(true) //设置url中驼峰转连字符
                .setInclude(tableNames); // 表名
        mpg.setStrategy(stConfig);


        // 5、自定义配置参数
        String[] categoryConfig = dbp.getCategoryConfig();
        boolean is_controller = false;
        boolean is_service = false;
        boolean is_mapper = false;
        boolean is_po = false;
        for(String cc: categoryConfig){
            if("controller".equals(cc)){ is_controller = true; }
            if("service".equals(cc)){ is_service = true; }
            if("mapper".equals(cc)){ is_mapper = true; }
            if("po".equals(cc)){ is_po = true; }
        }

        if(is_customize){
            InjectionConfig cfg = new InjectionConfig() {
                // 注入自定义 Map 对象(注意需要setMap放进去),该对象可以传递到模板引擎通过 cfg.abc 引用
                @Override
                public void initMap() {
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("abc", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                    this.setMap(map);
                }
            };

            // 自定义输出配置模板
            List<FileOutConfig> files = new ArrayList<FileOutConfig>();
            if(is_mapper){
                files.add(new FileOutConfig(File.separator+"mbpg"+File.separator+"mapper.java.vm") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        // 这个方法是Mapper模板
                        String basePackage = dbp.getBasePackage();
                        basePackage = basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
                        String entityFile = String.format((buildPath + File.separator + basePackage + File.separator + pkConfig.getMapper() + File.separator + "%s" + ".java"), tableInfo.getMapperName());
                        return entityFile;
                    }
                });
            }
            if(is_controller){
                files.add(new FileOutConfig(File.separator+"mbpg"+File.separator+"controller.java.vm") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        // 这个方法是控制层模板
                        String basePackage = dbp.getBasePackage();
                        basePackage = basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
                        String entityFile = String.format((buildPath + File.separator + basePackage + File.separator + pkConfig.getController() + File.separator + "%s" + ".java"), tableInfo.getControllerName());
                        //String entityFile1 = projectPath + "/src/main/resources/mapper/" + pkConfig.getModuleName()+ "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
                        return entityFile;
                    }
                });
            }
            if(is_service){
                files.add(new FileOutConfig(File.separator+"mbpg"+File.separator+"service.java.vm") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        String basePackage = dbp.getBasePackage();
                        basePackage = basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
                        String entityFile = String.format((buildPath + File.separator + basePackage + File.separator + pkConfig.getService() + File.separator + "%s" + ".java"), tableInfo.getServiceName());
                        return entityFile;
                    }
                });
                files.add(new FileOutConfig(File.separator+"mbpg"+File.separator+"serviceImpl.java.vm") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        String basePackage = dbp.getBasePackage();
                        basePackage = basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
                        String impl = pkConfig.getServiceImpl().replaceAll("\\.", Matcher.quoteReplacement(File.separator));
                        String entityFile = String.format((buildPath + File.separator + basePackage + File.separator + impl + File.separator + "%s" + ".java"), tableInfo.getServiceImplName());
                        return entityFile;
                    }
                });
            }


//            if(is_po){
//                files.add(new FileOutConfig(File.separator+"mbpg"+File.separator+"entity.java.vm") {
//                    @Override
//                    public String outputFile(TableInfo tableInfo) {
//                        String basePackage = dbp.getBasePackage();
//                        basePackage = basePackage.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
//                        String entityFile = String.format((buildPath + File.separator + basePackage + File.separator + pkConfig.getEntity() + File.separator + "%s" + ".java"), tableInfo.getEntityName());
//                        return entityFile;
//                    }
//                });
//            }
            cfg.setFileOutConfigList(files);
            mpg.setCfg(cfg);
        }




        // 6、自定义模板.可以 copy 源码 mybatis-plus/src/main/resources/template 下面内容修改.放置自己项目的 src/main/resources/template 目录下。
        //上面自定义配置后，可以在下面禁用相对应的就不会生成其它模板
        TemplateConfig templateConfig = new TemplateConfig();
        // templateConfig.setEntity("templates/entity2.java");//注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别，这里跟上面模板配置类似
        if(is_customize){ // 自定义，这里需要停用默认的模板，使用上面自定义的模板
            if(is_controller){templateConfig.setController(null);}
            if(is_service){templateConfig.setService(null); templateConfig.setServiceImpl(null);}
            if(is_mapper){templateConfig.setMapper(null); templateConfig.setXml(null);}
            //if(is_po){templateConfig.setEntity(null);}
        }
        mpg.setTemplate(templateConfig);


        // 支持 Velocity（默认）、Freemarker、Beetl。如果您选择了非默认引擎，需要在 AutoGenerator 中 设置模板引擎
        // mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        // mpg.setTemplateEngine(new BeetlTemplateEngine());
        // mpg.setTemplateEngine(new CustomTemplateEngine());


        // 7.开始生成
        mpg.execute();


        // 打包zip
        String zipFileName = srcPath + ".zip";
        List<String> list = new ArrayList<>();
        list.add(buildPath);
        // 打成包
        String temp = ZipUtil.compressFiles(list, path + zipFileName);
        if (temp == null){
            rst.put("code","1");
            rst.put("msg", "生成失败，请查看后台日志！");
        }else{
            rst.put("code","0");
        }
        rst.put("path", zipFileName);
        try {
            FileUtils.deleteDirectory(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return rst;
    }
}
