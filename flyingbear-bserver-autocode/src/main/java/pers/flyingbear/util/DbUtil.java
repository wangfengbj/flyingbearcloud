package pers.flyingbear.util;

import lombok.extern.slf4j.Slf4j;
import pers.flyingbear.constant.Const;
import pers.flyingbear.constant.DbEnum;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据库工具，包含配置
 */
@Slf4j
public class DbUtil {
    private DbUtil(){}

    public static DbEnum getSqlConfig(String type){
        return DbEnum.getDb(type);
    }

    public static Connection getConnection(String type, String ip, Integer port, String dataBase, String username,
                                           String password) throws ClassNotFoundException, SQLException {
        DbEnum m = getSqlConfig(type);
        Class.forName(m.getDriver());
        String url = m.getPrefix() + ip + ":" + port + "/" + dataBase + m.getSuffix();
        return DriverManager.getConnection(url, username, password);
    }

    public static List<Map<String,String>> getMysqlTable(String ip, Integer port, String dataBase, String username,
                                             String password){
        Connection con = null;
        PreparedStatement statement = null;
        ResultSet res = null;
        List<Map<String,String>> list = new ArrayList<>();
        try {
            con = getConnection(Const.DB_MYSQL, ip, port, dataBase, username, password);
            String sql="select table_name,table_schema from information_schema.tables t where t.table_schema=?";
            statement = con.prepareStatement(sql);
            statement.setString(1, dataBase);
            res = statement.executeQuery();
            while (res.next()){
                //list.add(res.getString(1));
                Map<String,String> m = new HashMap<>();
                m.put("table_name", res.getString("table_name"));
                list.add(m);
            }
        } catch (ClassNotFoundException e) {
            log.error("找不到数据库驱动：", e);
        } catch (SQLException throwables) {
            log.error("建立数据库连接异常：", throwables);
        }finally {
            try {
                res.close();
            } catch (SQLException throwables) {
                log.error("ResultSet关闭异常：", throwables);
            }
            try {
                statement.close();
            } catch (SQLException throwables) {
                log.error("statement关闭异常：", throwables);
            }
            closeConnection(con);
        }
        return list;
    }

    public static void closeConnection(Connection connection){
        try {
            if(connection != null && !connection.isClosed()){
                connection.close();
            }
        } catch (SQLException throwables) {
            log.error("数据库关闭异常：", throwables);
        }
    }
}
