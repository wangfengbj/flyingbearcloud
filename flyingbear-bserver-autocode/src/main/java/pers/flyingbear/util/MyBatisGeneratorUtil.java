package pers.flyingbear.util;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.*;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.internal.DefaultShellCallback;
import pers.flyingbear.config.MybatisConfig;
import pers.flyingbear.constant.DbEnum;
import pers.flyingbear.constant.TargetRuntimeEnum;
import pers.flyingbear.vo.DataBaseProperties;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

/**
 * MyBatis生成代码工具类
 * 实体类生成：BaseRecordGenerator
 * Dao生成：JavaMapperGenerator生成
 * 注释生成：DefaultCommentGenerator
 * 类型转换：JavaTypeResolverDefaultImpl
 * 插件：PluginAdapter
 * mapper方法名：IntrospectedTable
 */
public class MyBatisGeneratorUtil {

    private MyBatisGeneratorUtil(){}

    public static Map<String, String> generator(DataBaseProperties dbp, boolean is_customize){
        Map<String, String> rst = new HashMap<>();
        MybatisConfig.DBP = dbp;

        String path = MyBatisGeneratorUtil.class.getClassLoader().getResource("").getPath();
        //String srcPath = File.separator + new Date().getTime();
        String srcPath = String.valueOf(System.currentTimeMillis());
        String buildPath = path + srcPath;
        File file = new File(buildPath);
        if(!file.exists()){
            file.mkdir();
        }


        // 执行过程中的警告信息
        List<String> warnings = new ArrayList<String>();
        // 覆盖已有的重名文件
        boolean overwrite = true;

        // 1. 创建 配置解析器
        ConfigurationParser parser = new ConfigurationParser(warnings);
        // 2. 获取 配置信息
        Configuration config =  new Configuration();


        /**
         * 3. xml中context配置
         * CONDITIONAL 默认这个，如果一个表的主键只有一个字段,那么不会为该字段生成单独的实体类,会将该字段合并到基本实体类中
         * FLAT 所有内容（主键，blob）等全部生成在一个对象中
         * HIERARCHICAL 主键生成一个XXKey对象(key class)，Blob等单独生成一个对象，其他简单属性在一个对象中(record class)
         */
        Context context = null;
        try {
            context = new Context(ModelType.getModelType(dbp.getModelType()));
        }catch (Exception e){
            rst.put("code","1");
            rst.put("msg", "生成失败，ModelType不存在！");
            return rst;
        }
        context.setId("flyingBearAuto");
        /**
         * 运行模式targetRuntime：
         * MyBatis3
         * MyBatis3Simple(无example)
         * MyBatis3DynamicSql(官方建议)：无XML，只生成pojo和一个mapper类；使用流式以及lambda表达式来构建sql
         * MyBatis3DynamicSqlV1
         * MyBatis3Kotlin
         */
        try {
            context.setTargetRuntime(TargetRuntimeEnum.getTargetRuntime(dbp.getTargetRuntime()).name());
        }catch (Exception e){
            rst.put("code","1");
            rst.put("msg", "生成失败，运行模式不存在！");
            return rst;
        }
        config.addContext(context);




        // 自定义插件
        if(is_customize){
            boolean lombok = "on".equals(dbp.getLombok())? true : false;
            MybatisConfig.LOMBOK = lombok;


            if("MyBatis3DynamicSql".equals(dbp.getTargetRuntime()) || "MyBatis3Simple".equals(dbp.getTargetRuntime())){
                PluginConfiguration plugin2 = new PluginConfiguration();
                plugin2.setConfigurationType("pers.flyingbear.config.MybatisControllerPlugin"); // 控制层和service层
                context.addPluginConfiguration(plugin2);
            }
            if("MyBatis3Simple".equals(dbp.getTargetRuntime())){
                PluginConfiguration plugin1 = new PluginConfiguration();
                plugin1.setConfigurationType("pers.flyingbear.config.MybatisJavaPlugin"); // 原生的类扩展
                context.addPluginConfiguration(plugin1);
            }
        }
        PluginConfiguration plugin0 = new PluginConfiguration();
        plugin0.setConfigurationType("org.mybatis.generator.plugins.UnmergeableXmlMappersPlugin"); // 解决XML重复
        context.addPluginConfiguration(plugin0);





        // 4. commentGenerator配置
        CommentGeneratorConfiguration commentGeneratorConfiguration = new CommentGeneratorConfiguration();
        // 自定义注释
        if(is_customize){
            commentGeneratorConfiguration.setConfigurationType("pers.flyingbear.config.MybatisCommentGenerator");
        }
        commentGeneratorConfiguration.addProperty("suppressAllComments", "true"); // 是否去除自动生成的注释 true【是】 false【否】
        //commentGeneratorConfiguration.addProperty("suppressDate", "true"); // 去掉生成日期那行注释
        //commentGeneratorConfiguration.addProperty("addRemarkComments", "true"); // 实体类中附带表字段的注释
        context.setCommentGeneratorConfiguration(commentGeneratorConfiguration);


        // 5. jdbcConnection配置
        DbEnum dbe = DbUtil.getSqlConfig(dbp.getDbType());
        JDBCConnectionConfiguration jdbcConnectionConfiguration = new JDBCConnectionConfiguration();
        jdbcConnectionConfiguration.setDriverClass(dbe.getDriver());
        jdbcConnectionConfiguration.setUserId(dbp.getUsername());
        jdbcConnectionConfiguration.setPassword(dbp.getPassword());
        jdbcConnectionConfiguration.setConnectionURL(dbe.getPrefix() + dbp.getIp() + ":" + dbp.getPort() + "/" + dbp.getDataBase() + dbe.getSuffix());
        context.setJdbcConnectionConfiguration(jdbcConnectionConfiguration);


        // 6. javaTypeResolver配置
        JavaTypeResolverConfiguration javaTypeResolverConfiguration = new JavaTypeResolverConfiguration();
        // 自定义类型转换
        if(is_customize){
            javaTypeResolverConfiguration.setConfigurationType("pers.flyingbear.config.MybatisJavaTypeResolver");
        }
        // 默认false，把JDBC DECIMAL和NUMERIC类型解析为Integer，为true时,把JDBC DECIMAL 和NUMERIC 类型解析为java.math.BigDecimal
        javaTypeResolverConfiguration.addProperty("forceBigDecimals","false");
        // 使用jdk8时间类LocalDateTime
        //javaTypeResolverConfiguration.addProperty("useJSR310Types","true");
        javaTypeResolverConfiguration.addProperty("useJSR310Types","false");
        context.setJavaTypeResolverConfiguration(javaTypeResolverConfiguration);


        // 7. 配置模型po、model、entity
        JavaModelGeneratorConfiguration javaModelGeneratorConfiguration = new JavaModelGeneratorConfiguration();
        javaModelGeneratorConfiguration.setTargetPackage(dbp.getBasePackage()+".po");
        MybatisConfig.BASEPACKAGE = dbp.getBasePackage();
        javaModelGeneratorConfiguration.setTargetProject(buildPath); // PO类的位置，如果本地生成.\src\main\java
        javaModelGeneratorConfiguration.addProperty("enableSubPackages","false"); // enableSubPackages:是否让schema作为包的后缀
        javaModelGeneratorConfiguration.addProperty("trimStrings","true"); // 从数据库返回的值被清理前后的空格
        context.setJavaModelGeneratorConfiguration(javaModelGeneratorConfiguration);


        // 8. XML映射文件的包名
        SqlMapGeneratorConfiguration sqlMapGeneratorConfiguration = new SqlMapGeneratorConfiguration();
        sqlMapGeneratorConfiguration.setTargetPackage(dbp.getBasePackage()+".mapper");  // resources下mapper
        sqlMapGeneratorConfiguration.setTargetProject(buildPath); // resources下.\src\main\resources
        sqlMapGeneratorConfiguration.addProperty("enableSubPackages","false"); // enableSubPackages:是否让schema作为包的后缀
        context.setSqlMapGeneratorConfiguration(sqlMapGeneratorConfiguration);


        // 9. DAO的包名也就是mapper.java
        JavaClientGeneratorConfiguration javaClientGeneratorConfiguration = new JavaClientGeneratorConfiguration();
        javaClientGeneratorConfiguration.setTargetPackage(dbp.getBasePackage()+".mapper");
        javaClientGeneratorConfiguration.setTargetProject(buildPath);
        /**
         * ANNOTATEDMAPPER
         * XMLMAPPER
         */
        if(!("XMLMAPPER".equals(dbp.getConfigType()) || "ANNOTATEDMAPPER".equals(dbp.getConfigType()))){
            rst.put("code","1");
            rst.put("msg", "生成失败，Mapper类型不存在！");
            return rst;
        }
        javaClientGeneratorConfiguration.setConfigurationType(dbp.getConfigType()); // MyBatis3DynamicSql不管设置什么都是注解
        javaClientGeneratorConfiguration.addProperty("enableSubPackages","false"); // enableSubPackages:是否让schema作为包的后缀
        context.setJavaClientGeneratorConfiguration(javaClientGeneratorConfiguration);


        // 10. 表table配置
        //List<TableConfiguration> tableConfigurations = context.getTableConfigurations();
        //tableConfigurations.clear();
        TableConfiguration tableConfiguration = new TableConfiguration(context);
        tableConfiguration.setTableName(dbp.getTableName()); // 数据库标名
        //tableConfiguration.setDomainObjectName(modelNames[i]); // 实体类名字一般不需要设置
        //tableConfiguration.setCountByExampleStatementEnabled(false);
        //tableConfiguration.setDeleteByExampleStatementEnabled(false);
        //tableConfiguration.setSelectByExampleStatementEnabled(false);
        //tableConfiguration.setUpdateByExampleStatementEnabled(false);
        //tableConfiguration.setSelectByExampleQueryId("false");
        context.addTableConfiguration(tableConfiguration);


        // 11.创建 默认命令解释调回器
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        // 11.创建 mybatis的生成器
        MyBatisGenerator myBatisGenerator = null;

        try {
            myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
            myBatisGenerator.generate(null);
            String zipFileName = srcPath + ".zip";
            List<String> list = new ArrayList<>();
            list.add(buildPath);
            // 打成包
            String temp = ZipUtil.compressFiles(list, path + zipFileName);
            if (temp == null){
                rst.put("code","1");
                rst.put("msg", "生成失败，请查看后台日志！");
            }else{
                rst.put("code","0");
            }
            rst.put("path", zipFileName);
            FileUtils.deleteDirectory(file);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            rst.put("code","1");
            rst.put("msg", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            rst.put("code","1");
            rst.put("msg", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            rst.put("code","1");
            rst.put("msg", e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
            rst.put("code","1");
            rst.put("msg", e.getMessage());
        }


        // 输出警告信息
        for (String warning : warnings) {
            System.out.println("生成代码警告信息："+warning);
        }
        System.out.println("*******生成代码完成*******");
        return rst;
    }
}
