package pers.flyingbear.util;

import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * zip工具类
 */
public class ZipUtil {
    private ZipUtil(){}

    public static String compressFiles(List<String> sourceFileList, String zipFileName){
        OutputStream outputStream = null;
        try {
            List<File> fileList = new ArrayList<>();
            for (String file : sourceFileList) {
                fileList.add(new File(file));
            }
            outputStream = new FileOutputStream(new File(zipFileName));
            ZipArchiveOutputStream zipArchiveOutputStream = new ZipArchiveOutputStream(outputStream);
            doCompress(fileList, zipArchiveOutputStream, null);
            zipArchiveOutputStream.finish();
            return zipFileName;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private static void doCompress(List<File> fileList, ZipArchiveOutputStream zipArchiveOutputStream,
                                   String parent) throws IOException {
        for (File file : fileList) {
            if (!file.exists()) {
                return;
            }
            if (file.isFile()) {
                try (InputStream inputStream = new FileInputStream(file)) {
                    zipArchiveOutputStream.setUseZip64(Zip64Mode.AsNeeded);
                    if (null != parent) {
                        zipArchiveOutputStream.putArchiveEntry(new ZipArchiveEntry(parent + File.separator + file.getName()));
                    } else {
                        zipArchiveOutputStream.putArchiveEntry(new ZipArchiveEntry(file.getName()));
                    }
                    byte[] buffer = new byte[10240];
                    int len;
                    while ((len = inputStream.read(buffer)) != -1) {
                        zipArchiveOutputStream.write(buffer, 0, len);
                    }
                    zipArchiveOutputStream.closeArchiveEntry();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                File[] files = file.listFiles();
                if (files == null || files.length == 0) {
                    if (null != parent) {
                        zipArchiveOutputStream.putArchiveEntry(new ZipArchiveEntry(parent + File.separator + file.getName() + "/"));
                    } else {
                        zipArchiveOutputStream.putArchiveEntry(new ZipArchiveEntry(file.getName() + "/"));
                    }
                    zipArchiveOutputStream.closeArchiveEntry();
                } else {
                    if (null != parent) {
                        doCompress(Arrays.asList(files), zipArchiveOutputStream, parent + File.separator + file.getName());
                    } else {
                        doCompress(Arrays.asList(files), zipArchiveOutputStream, file.getName());
                    }
                }
            }
        }
    }
}
