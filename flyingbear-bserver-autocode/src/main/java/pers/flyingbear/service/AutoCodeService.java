package pers.flyingbear.service;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import pers.flyingbear.constant.Const;
import pers.flyingbear.constant.DbEnum;
import pers.flyingbear.util.DbUtil;
import pers.flyingbear.util.MyBatisGeneratorUtil;
import pers.flyingbear.util.MyBatisPlusGeneratorUtil;
import pers.flyingbear.vo.DataBaseProperties;

import java.io.IOException;
import java.util.*;

@Service
public class AutoCodeService {
    /**
     * 获取数据库中表
     * @param dbp
     * @return
     */
    public Map<String, Object> getTable(DataBaseProperties dbp) {
        Map<String, Object> rst = new HashMap<>();
        List<Map<String,String>> data = null;
        if(Const.DB_MYSQL.equals(dbp.getDbType().toLowerCase())){
            data = DbUtil.getMysqlTable(dbp.getIp(),dbp.getPort(),dbp.getDataBase(),dbp.getUsername(),dbp.getPassword());
        }
        rst.put("code", 0);
        rst.put("count", data.size());
        rst.put("data", data);
        return rst;
    }

    public Map<String, String> autoCode(DataBaseProperties dbp) {
        if("mybatis".equals(dbp.getAutoType())){
            return MyBatisGeneratorUtil.generator(dbp, false);
        } else if("mybatisAuto".equals(dbp.getAutoType())){
            return MyBatisGeneratorUtil.generator(dbp, true);
        }else if("mybatisPlus".equals(dbp.getAutoType())){
            return MyBatisPlusGeneratorUtil.generator(dbp, false);
        } else if("mybatisPlusAuto".equals(dbp.getAutoType())){
            return MyBatisPlusGeneratorUtil.generator(dbp, true);
        } else{
            Map<String, String> m = new HashMap<>();
            m.put("code", "1");
            m.put("msg", "生成类型不存在");
            return m;
        }
    }

    public List<Map<String, String>> getDbTypes() {
        List<Map<String, String>> list = new ArrayList<>();
        for (DbEnum dbEnum : DbEnum.values()) {
            Map<String,String> m = new HashMap<>();
            m.put("value", dbEnum.getType().toLowerCase(Locale.ROOT));
            m.put("text", dbEnum.getType().toLowerCase(Locale.ROOT));
            list.add(m);
        }
        return list;
    }

    public ResponseEntity<InputStreamResource> getFile(String path) throws IOException {
        String root_path = AutoCodeService.class.getClassLoader().getResource("").getPath();
        String file_path = root_path + path;
        // 读取文件
        FileSystemResource file = new FileSystemResource(file_path);
        // 设置响应头
        //MultiValueMap<String, String> headers = new HttpHeaders();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getFilename()));
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        // 也可以返回字节数组
        // ResponseEntity<byte[]> r = new ResponseEntity<>(bytes, headers, HttpStatus.OK);
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentLength(file.contentLength())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(file.getInputStream()));
    }
}
