package pers.flyingbear.vo;

import lombok.Data;

@Data
public class DataBaseProperties {
    private String dbType;
    private String ip;
    private Integer port;
    private String dataBase;
    private String username;
    private String password;

    // 具体参数
    private String tableName;
    private String autoType;
    private String basePackage; // 包根路径
    private String lombok;
    private String modelType;
    private String targetRuntime;
    private String configType;
    private String[] categoryConfig;
}
