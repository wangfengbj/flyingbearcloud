package pers.flyingbear.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class) // 让junit和spring整合
//@SpringBootTest(classes={LearnServer.class}) // 启动类
public class SBTest {
    @Test
    @Transactional // @Transactional与@Test 一起使用时 事务是自动回滚的
    @Rollback(false) // 取消自动回滚
    public void testJPA(){

    }
}
