drop table if exists account;
create table account (
    id integer primary key autoincrement,
    name text,
    age int,
    email text
);
pragma foreign_keys = true;

insert into account values(null , '张三', 1, '1@qq.com');
insert into account values(null , '李四', 1, '1@qq.com');
insert into account values(null , '王五', 1, '1@qq.com');