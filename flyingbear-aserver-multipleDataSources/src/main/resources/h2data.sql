delete from `account`;
insert into `account` (name, age, email) values
('Jone', 18, 'test1@baomidou.com'),
('Jack', 20, 'test2@baomidou.com'),
('Tom', 28, 'test3@baomidou.com'),
('Sandy', 21, 'test4@baomidou.com'),
('user', 21, 'test6@baomidou.com'),
('Billie', 24, 'test5@baomidou.com');

delete from oauth_client_details;
insert into oauth_client_details(client_id, client_secret, scope, authorized_grant_types, web_server_redirect_uri,
                                 authorities, access_token_validity,refresh_token_validity, additional_information, autoapprove)
values ('userClient', '$2a$10$k1wpj9jcWO2b0zQMYlGKZ.eRFHL7gcZVjaCeVr/f/0Frk7VgPa5Yu', 'all',
 'authorization_code,refresh_token,password', 'http://localhost:7777/', null, 3600, 2592000, null, 'true');