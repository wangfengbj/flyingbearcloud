drop table if exists `account`;
create table `account`(
    `id` int primary key auto_increment,
    `name` varchar(30) default null comment '姓名',
    `age` int,
    `email` varchar(50) COMMENT '邮箱'
);
drop table if exists oauth_client_details;
create table oauth_client_details (
    client_id varchar(256) primary key,
    resource_ids varchar(256),
    client_secret varchar(256),
    scope varchar(256),
    authorized_grant_types varchar(256),
    web_server_redirect_uri varchar(256),
    authorities varchar(256),
    access_token_validity integer,
    refresh_token_validity integer,
    additional_information varchar(4096),
    autoapprove varchar(256),
    create_time timestamp default current_timestamp
);

create table oauth_code (
    code varchar(255),
    authentication BLOB,
    create_time timestamp default current_timestamp
);
