package pers.flyingbear.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.util.List;

/**
 * a. 配置dataSource数据源
 */
@Configuration
public class DataSourceConfig {
    @Value("${spring.datasource.myh2.schema}")
    private String h2Schema;
    @Value("${spring.datasource.myh2.data}")
    private String h2Data;
    @Value("${spring.datasource.sqlite.schema}")
    private String sqliteSchema;

    /**
     * DataSourceProperties配置
     * @return
     */
    @Bean("h2DataSourceProperties")
    @ConfigurationProperties(prefix = "spring.datasource.myh2") // DataSourceProperties接收
    public DataSourceProperties h2DataSourceProperties() {
        return new DataSourceProperties();
    }

    /**
     * h2数据源
     * @return
     */
    @Primary // 默认主数据源
    @Bean(name = "h2DataSource")
    public DataSource h2DataSource(@Qualifier(value = "h2DataSourceProperties") DataSourceProperties dataSourceProperties) {
        // DataSourceBuilder.create().build();
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    /**
     * 初始化脚本执行
     * @param dataSource
     * @return
     */
    @Bean(name = "h2DataSourceInitializer")
    public DataSourceInitializer h2DataSourceInitializer(@Qualifier(value = "h2DataSource") DataSource dataSource,
                                                         @Qualifier(value = "h2DataSourceProperties") DataSourceProperties dataSourceProperties){
        return getDataSourceInitializer(dataSource, dataSourceProperties, h2Schema, h2Data);
    }

    /**
     * H2的JdbcTemplate
     * @param dataSource
     * @return
     */
    @Bean(name = "h2Template")
    public JdbcTemplate h2JdbcTemplate(@Qualifier("h2DataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }



    /**
     * DataSourceProperties配置
     * @return
     */
    @Bean("sqliteDataSourceProperties")
    @ConfigurationProperties(prefix = "spring.datasource.sqlite") // DataSourceProperties接收
    public DataSourceProperties sqliteDataSourceProperties() {
        return new DataSourceProperties();
    }

    /**
     * sqlite数据源
     * @return
     */
    @Bean(name = "sqliteDataSource")
    public DataSource sqliteDataSource(@Qualifier(value = "sqliteDataSourceProperties") DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().build();
    }

    /**
     * 初始化脚本执行
     * @param dataSource
     * @return
     */
    @Bean(name = "sqliteDataSourceInitializer")
    public DataSourceInitializer sqliteDataSourceInitializer(@Qualifier(value = "sqliteDataSource") DataSource dataSource,
                                                         @Qualifier(value = "sqliteDataSourceProperties") DataSourceProperties dataSourceProperties){
        return getDataSourceInitializer(dataSource, dataSourceProperties, sqliteSchema, null);
    }

    /**
     * 初始化脚本
     * @param dataSource
     * @param dataSourceProperties
     * @return
     */
    private DataSourceInitializer getDataSourceInitializer(DataSource dataSource, DataSourceProperties dataSourceProperties, String schema, String data){
        DataSourceInitializer initializer = new DataSourceInitializer();
        initializer.setDataSource(dataSource);
        initializer.setEnabled(true);
        // 创建 populator
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.setContinueOnError(true);
        populator.setIgnoreFailedDrops(true);
        populator.setSqlScriptEncoding("utf-8");
        // 加入初始化脚本
        if(schema != null && schema.length()>0){
            if(schema.indexOf("classpath:") >= 0){
                schema = schema.substring(10);
                populator.addScript(new ClassPathResource(schema));
            }
        }
        // 数据
        if(data != null && data.length()>0){
            if(data.indexOf("classpath:") >= 0){
                data = data.substring(10);
                populator.addScript(new ClassPathResource(data));
            }
        }
        initializer.setDatabasePopulator(populator);
        return initializer;
    }
}
