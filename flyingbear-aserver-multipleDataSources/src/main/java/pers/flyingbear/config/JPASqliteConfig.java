package pers.flyingbear.config;

import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * b. 多数据源sqlite的JPA配置
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactorySqlite",
        transactionManagerRef="transactionManagerSqlite",
        basePackages= {"pers.flyingbear.jpasqlite"}) // Repository所在位置
public class JPASqliteConfig {
    @Resource
    private Environment env;

    @Resource(name="sqliteDataSource")
    private DataSource sqliteDataSource;


    /**
     * 数据管理器
     * @param builder
     * @return
     */
    @Bean(name = "entityManagerSqlite")        //primary实体管理器
    public EntityManager entityManagerSqlite(EntityManagerFactoryBuilder builder) {
        return entityManagerFactorySqlite(builder).getObject().createEntityManager();
    }

    /**
     * 自动注入JpaProperties多数据源时候不要使用
     * @return
     */
    @Bean(name = "jpaPropertiesSqlite")
    @ConfigurationProperties(prefix = "spring.jpa.sqlite")
    public JpaProperties jpaProperties() {
        return new JpaProperties();
    }

    /**
     * 实体惯例工厂, hibernateProperties不要注入，新建
     * @param builder
     * @return
     */
    @Bean(name = "entityManagerFactorySqlite")
    public LocalContainerEntityManagerFactoryBean entityManagerFactorySqlite(EntityManagerFactoryBuilder builder) {
        JpaProperties jpaProperties= jpaProperties();
        Map<String,Object> properties = new HibernateProperties().determineHibernateProperties(jpaProperties.getProperties(),
                new HibernateSettings());
        properties.put("hibernate.dialect", jpaProperties.getDatabasePlatform());
        properties.put("hibernate.ddl-auto", "none");
        return builder.dataSource(sqliteDataSource) // 设置数据源
                .properties(jpaProperties.getProperties()) // jpa配置
                .properties(properties) // 设置hibernate配置
                .packages("pers.flyingbear.posqlite") // 换成数据表对应实体类所在包
                .persistenceUnit("sqlitePersistenceUnit")  // 设置持久化单元名，用于@PersistenceContext注解获取EntityManager时指定数据源
                .build();
    }

    /**
     * 事务管理器
     * @param builder
     * @return
     */
    @Bean(name = "transactionManagerSqlite")
    public PlatformTransactionManager transactionManagerSqlite(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactorySqlite(builder).getObject());
    }
}
