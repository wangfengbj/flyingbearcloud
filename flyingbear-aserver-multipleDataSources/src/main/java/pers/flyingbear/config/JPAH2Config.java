package pers.flyingbear.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * b. 多数据源H2的JPA配置
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef="entityManagerFactoryH2",
        transactionManagerRef="transactionManagerH2",
        basePackages= {"pers.flyingbear.jpa"}) // Repository所在位置
public class JPAH2Config {
    @Resource
    private Environment env;

    @Resource(name="h2DataSource")
    private DataSource h2DataSource;


    /**
     * 数据管理器
     * @param builder
     * @return
     */
    @Bean(name = "entityManagerH2")        //primary实体管理器
    public EntityManager entityManagerH2(EntityManagerFactoryBuilder builder) {
        return entityManagerFactoryH2(builder).getObject().createEntityManager();
    }

    /**
     * 自动注入JpaProperties多数据源时候不要使用
     * @return
     */
    @Primary
    @Bean(name = "jpaPropertiesH2")
    @ConfigurationProperties(prefix = "spring.jpa.myh2")
    public JpaProperties jpaProperties() {
        return new JpaProperties();
    }

    /**
     * 实体惯例工厂, hibernateProperties不要注入，新建
     * @param builder
     * @return
     */
    @Primary
    @Bean(name = "entityManagerFactoryH2")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryH2(EntityManagerFactoryBuilder builder) {
        JpaProperties jpaProperties = jpaProperties();
        Map<String,Object> properties = new HibernateProperties().determineHibernateProperties(jpaProperties.getProperties(),
                new HibernateSettings());
        properties.put("hibernate.dialect", jpaProperties.getDatabasePlatform());
        properties.put("hibernate.ddl-auto", "none");
        return builder.dataSource(h2DataSource) // 设置数据源
                .properties(jpaProperties.getProperties()) // jpa配置
                .properties(properties) // 设置hibernate配置
                .packages("pers.flyingbear.poh2") // 换成数据表对应实体类所在包
                .persistenceUnit("h2PersistenceUnit")  // 设置持久化单元名，用于@PersistenceContext注解获取EntityManager时指定数据源
                .build();
    }

    /**
     * 事务管理器
     * @param builder
     * @return
     */
    @Bean(name = "transactionManagerH2")
    public PlatformTransactionManager transactionManagerH2(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(entityManagerFactoryH2(builder).getObject());
    }

    private Map getVendorProperties() {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect"); // env.getProperty("hibernate.dialect")
        properties.put("hibernate.ddl-auto", "update");
        properties.put("hibernate.physical_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        properties.put("hibernate.implicit_naming_strategy", "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        return properties;
    }
}
