package pers.flyingbear.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class MyScheduled {
    @Scheduled(cron="0 0/5 * * * ?")
    public void scheduledMethod(){
        System.out.println("scheduled定时器被触发"+new Date());
    }
}
