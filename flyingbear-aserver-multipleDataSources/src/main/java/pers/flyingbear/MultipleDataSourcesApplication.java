package pers.flyingbear;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import pers.flyingbear.filter.WfFilter3;
import pers.flyingbear.listener.WfSessionListener3;
import pers.flyingbear.servlet.WfServlet;


/**
 * 1. exclude={DataSourceAutoConfiguration.class} 多数据源不需要要自动装配数据源
 * 2. HibernateJpaAutoConfiguration.class JPA自动配置类
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class, MongoRepositoriesAutoConfiguration.class})
@ServletComponentScan  // 扫描注解@WebFilter、@WebServlet、@WebListener
// @EntityScan(basePackages = ("com.flyingbear.poh2.*"))
public class MultipleDataSourcesApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MultipleDataSourcesApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }


    /**
     * 注入servlet
     * @return
     */
    @Bean
    public ServletRegistrationBean getServletRegistrationBean(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new WfServlet());
        bean.addUrlMappings("/wf");
        return bean;
    }

    /**
     * 注入filter
     * @return
     */
    @Bean
    public FilterRegistrationBean getFilterRegistrationBean(){
        FilterRegistrationBean bean = new FilterRegistrationBean(new WfFilter3());
        bean.addUrlPatterns("/*");
        bean.setOrder(10);
        return bean;
    }

    /**
     * 注入listener
     * @return
     */
    @Bean
    public ServletListenerRegistrationBean<WfSessionListener3> getServletListenerRegistrationBean(){
        ServletListenerRegistrationBean<WfSessionListener3> bean=
                new ServletListenerRegistrationBean<WfSessionListener3>(new WfSessionListener3());
        return bean;
    }
}
