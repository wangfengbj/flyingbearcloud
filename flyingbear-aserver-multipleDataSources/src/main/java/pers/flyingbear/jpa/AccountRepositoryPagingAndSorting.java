package pers.flyingbear.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pers.flyingbear.poh2.Account;

/**
 * 3. PagingAndSortingRepository接口，提供分页和排序的操作，继承了CrudRepository
 */
@Repository
public interface AccountRepositoryPagingAndSorting extends PagingAndSortingRepository<Account,Integer> {
}
