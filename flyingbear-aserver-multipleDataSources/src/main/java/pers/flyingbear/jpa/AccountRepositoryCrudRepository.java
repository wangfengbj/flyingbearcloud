package pers.flyingbear.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pers.flyingbear.poh2.Account;

/**
 * 2. CrudRepository接口，主要完成增删改查，继承了Repository
 */
@Repository
public interface AccountRepositoryCrudRepository extends CrudRepository<Account, Integer> {
}
