package pers.flyingbear.jpa;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pers.flyingbear.poh2.Account;

/**
 * 5. JpaSpecificationExecutor主要提供多条件查询支持，可以分页与排序，单独存在完全独立
 */
@Repository
public interface AccountRepository1 extends JpaSpecificationExecutor<Account> {
}
