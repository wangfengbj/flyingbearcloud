package pers.flyingbear.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pers.flyingbear.poh2.Account;

import java.util.List;

/**
 * 4. JpaRepository接口，继承了PagingAndSortingRepository接口，对继承的父接口的返回值适配
 */
@Repository
public interface AccountRepository extends JpaRepository<Account,Integer> {
    // 方法必须驼峰式命名。findBy(关键字)+属性名称(首字母要大写)+查询条件(首字母大写)
    List<Account> findByName(String name);
    List<Account> findByNameAndAge(String name,Integer age);
    List<Account> findByNameLike(String name);
}
