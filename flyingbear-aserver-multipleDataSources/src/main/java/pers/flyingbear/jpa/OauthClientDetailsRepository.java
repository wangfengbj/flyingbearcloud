package pers.flyingbear.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pers.flyingbear.poh2.OauthClientDetails;

@Repository
public interface OauthClientDetailsRepository  extends JpaRepository<OauthClientDetails,String> {
    OauthClientDetails findByClientId(String clientId);
}
