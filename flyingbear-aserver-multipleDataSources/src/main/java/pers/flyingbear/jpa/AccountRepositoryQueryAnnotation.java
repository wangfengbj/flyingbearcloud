package pers.flyingbear.jpa;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import pers.flyingbear.poh2.Account;

import java.util.List;

/**
 * 1. 顶层Repository
 */
@org.springframework.stereotype.Repository
public interface AccountRepositoryQueryAnnotation extends Repository<Account, Integer> {
    @Query("from Account where name = ?1 ")
    List<Account> queryByNameUseHQL(String name);

    @Query(value="select * from account where name = ? ", nativeQuery=true)
    List<Account> queryByNameUseSQL(String name);


    @Query("update Account set name = ?1 where id = ?2 ")
    @Modifying //需要执行一个更新操作
    void updateAccountNameById(String name,Integer id);
}
