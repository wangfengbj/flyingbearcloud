package pers.flyingbear.jpasqlite;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pers.flyingbear.posqlite.User;

@Repository
public interface UserSqliteRepository extends JpaRepository<User,Integer> {
}
