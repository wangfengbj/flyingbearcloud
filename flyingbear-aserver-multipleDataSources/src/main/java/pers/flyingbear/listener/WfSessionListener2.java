package pers.flyingbear.listener;

import org.springframework.stereotype.Component;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 2. @WebListener+@ServletComponentScan整合
 */
@WebListener
public class WfSessionListener2 implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSessionListener.super.sessionCreated(se);
        System.out.println("整合Listener成功创建：@WebListener+@ServletComponentScan");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSessionListener.super.sessionDestroyed(se);
        System.out.println("整合Listener成功销毁：@WebListener+@ServletComponentScan");
    }
}
