package pers.flyingbear.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.jpasqlite.UserSqliteRepository;
import pers.flyingbear.posqlite.User;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class SqliteController {
    @Resource
    private UserSqliteRepository userSqliteRepository;

    @GetMapping("/jpa10")
    public List<User> jpah2(){
        return userSqliteRepository.findAll();
    }
}
