package pers.flyingbear.controller;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.jpa.AccountRepository;
import pers.flyingbear.jpa.AccountRepositoryCrudRepository;
import pers.flyingbear.jpa.AccountRepositoryPagingAndSorting;
import pers.flyingbear.jpa.AccountRepositoryQueryAnnotation;
import pers.flyingbear.poh2.Account;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class H2Controller {
    @Resource
    private AccountRepository accountRepository;

    @Resource
    private AccountRepositoryQueryAnnotation accountRepositoryQueryAnnotation;

    @Resource
    private AccountRepositoryCrudRepository accountRepositoryCrudRepository;

    @Resource
    private AccountRepositoryPagingAndSorting accountRepositoryPagingAndSorting;

    /**
     * sync = true	这个属性的意思是加锁，解决缓存击穿问题
     * root.method.name取的方法名
     * value是名称空间
     * @return
     */
    @GetMapping("/jpa0")
    @Cacheable(value = "h2", key = "#root.method.name", sync = true)
    public List<Account> jpah2(HttpSession httpSession){
        System.out.println("session---------------"+httpSession.getId());
        return accountRepository.findAll();
    }

    @GetMapping("/jpa1")
    public List<Account> jpa1(){
        return accountRepository.findByNameAndAge("Jone", 18);
    }

    @GetMapping("/jpa2")
    public List<Account> jpa2(){
        return accountRepositoryQueryAnnotation.queryByNameUseHQL("Jack");
    }


    @GetMapping("/jpa3")
    public List<Account> jpa3(){
        return (List<Account>) accountRepositoryCrudRepository.findAll();
    }

    @GetMapping("/jpa4")
    public List<Account> jpa4(){
        return (List<Account>) accountRepositoryPagingAndSorting.findAll(Sort.by(Sort.Direction.DESC, "name"));
    }

//    @GetMapping("/jpa5")
//    public List<Account> jpa5(){
//        return accountRepository1.findAll(((root,query,criteriaBuilder)->{
//            List<Predicate> list = new ArrayList<>();
//            Predicate p1 = criteriaBuilder.like(root.get("nme").as(String.class),"J");
//            list.add(p1);
//            return query.where(list.toArray(new Predicate[list.size()])).getRestriction();
//        }));
//    }
}
