package pers.flyingbear;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * oauth2认证客户端应用程序
 */
@SpringBootApplication
public class Oauth2ClientApplication {
	public static void main(String[] args) {
		SpringApplication.run(Oauth2ClientApplication.class, args);
	}
}
