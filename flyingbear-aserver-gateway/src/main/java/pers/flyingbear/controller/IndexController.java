package pers.flyingbear.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 重定向首页，需要配置重定向支持
 */
@Controller
public class IndexController {
    @RequestMapping(value={"/"})
    public String redirectIndex() {
        return "redirect:/page/index.html";
    }
}
