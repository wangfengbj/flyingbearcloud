package pers.flyingbear.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.tool.model.ApiResponse;

/**
 * 1. 默认降级处理控制器
 */
@RestController
public class DefaultHystrixController {
    private Logger log= LoggerFactory.getLogger(getClass());

    @RequestMapping("/gatewayFallback")
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    public ApiResponse defaultFallback(){
        log.info("服务熔断降级操作...");
        return ApiResponse.error("服务降级操作，请稍后重试");
    }
}