package pers.flyingbear.exception;

import io.netty.channel.ConnectTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;
import pers.flyingbear.tool.model.ApiResponse;

import java.security.SignatureException;

/**
 * 2. 具体异常映射到不同方法进行处理响应不同信息
 */
@Component
public class GateWayExceptionHandlerAdvice {
    private Logger log= LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = {Exception.class})
    public ApiResponse handle(Throwable ex){
        if (ex instanceof SignatureException) {
            return signHandle((SignatureException) ex);
        } else if (ex instanceof NotFoundException) {
            return notFoundHandle((NotFoundException) ex);
        } else if (ex instanceof ResponseStatusException) {
            return handle((ResponseStatusException) ex);
//        } else if (ex instanceof GateWayException) {
//            return badGatewayHandle((GateWayException) ex);
        } else if (ex instanceof ConnectTimeoutException) {
            return timeoutHandle((ConnectTimeoutException) ex);
        } else {
            log.error("网关Exception:{}", ex.getMessage());
            return ApiResponse.error("网络繁忙，请稍后重试");
        }
    }

    /**
     * 401 校验 异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {SignatureException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ApiResponse signHandle(SignatureException ex) {
        log.error("SignatureException:{}", ex.getMessage());
        return ApiResponse.error("鉴权失败");
    }

    /**
     * 404 服务未找到 异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiResponse notFoundHandle(NotFoundException ex) {
        log.error("not found exception:{}", ex.getMessage());
        return ApiResponse.error("请求资源不存在");
    }

    /**
     * 500   其他服务 异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {ResponseStatusException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiResponse handle(ResponseStatusException ex) {
        log.error("ResponseStatusException:{}", ex.getMessage());
        return ApiResponse.error("网络繁忙，请稍后重试");
    }

    /**
     * 502 错误网关 异常
     * @param ex
     * @return
     */
//    @ExceptionHandler(value = {GateWayException.class})
//    @ResponseStatus(HttpStatus.BAD_GATEWAY)
//    public ApiResponse badGatewayHandle(GateWayException ex) {
//        log.error("badGateway exception:{}", ex.getMessage());
//        return CommonResult.failed(ResultCode.BAD_GATEWAY);
//    }


    /**
     * 504 网关超时 异常
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {ConnectTimeoutException.class})
    @ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
    public ApiResponse timeoutHandle(ConnectTimeoutException ex) {
        log.error("connect timeout exception:{}", ex.getMessage());
        return ApiResponse.error("连接超时");
    }
}
