package pers.flyingbear.exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.DefaultErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * 1. 继承DefaultErrorWebExceptionHandler重写方法，实现统一异常响应处理
 * 版本不一样【Hoxton】，写法不一样
 */
public class CustomErrorWebExceptionHandler extends DefaultErrorWebExceptionHandler {
    private static final String DEFAULT_ERROR_CODE = "error";
    private static final String SERVER_NOT_FOUND = "Unable to find instance for ";
    private static final String NOT_AVAILABLE_SERVER = "Load balancer does not have available server";
    private static final String SERVER_NOT_FOUND_ZH = "服务器繁忙,请稍后重试";

    @Autowired
    private GateWayExceptionHandlerAdvice gateWayExceptionHandlerAdvice;


//    public CustomErrorWebExceptionHandler(ErrorAttributes errorAttributes, ResourceProperties resourceProperties,
//                                          ErrorProperties errorProperties, ApplicationContext applicationContext) {
//        super(errorAttributes, resourceProperties, errorProperties, applicationContext);
//    }
    public CustomErrorWebExceptionHandler(ErrorAttributes errorAttributes, WebProperties.Resources resources,
                                          ErrorProperties errorProperties, ApplicationContext applicationContext) {
        super(errorAttributes, resources, errorProperties, applicationContext);
    }


    /**
     * 去掉html这种路由响应方式
     * @param errorAttributes
     * @return
     */
    @Override
    protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
    }

    /**
     * 重写 异常方法 塞入自己的 gateWayExceptionHandlerAdvice
     * @param request
     * @return
     */
    @Override
    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        boolean includeStackTrace = isIncludeStackTrace(request, MediaType.ALL);
        Map<String, Object> error = getErrorAttributes(request, includeStackTrace);
        Throwable throwable = getError(request);
        return ServerResponse.status(super.getHttpStatus(error))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(gateWayExceptionHandlerAdvice.handle(throwable)));
                //.body(BodyInserters.fromValue(buildErrorMap(throwable))); // 内置方法

    }


    /**
     * 返回消息
     * @param throwable
     * @return
     */
    protected Map<String, Object> buildErrorMap(Throwable throwable) {
        String errorMessage = throwable.getMessage();
        if (errorMessage.indexOf(SERVER_NOT_FOUND) >= 0) {
            errorMessage = SERVER_NOT_FOUND_ZH;
        }else if(errorMessage.indexOf("Connection refused") >= 0){
            errorMessage = SERVER_NOT_FOUND_ZH;
        }else if(errorMessage.indexOf(NOT_AVAILABLE_SERVER) >= 0){
            errorMessage = SERVER_NOT_FOUND_ZH;
        }
        Map<String, Object> resMap = new HashMap<>();
        resMap.put("isSuccess", false);
        resMap.put("state", DEFAULT_ERROR_CODE);
        resMap.put("msg", errorMessage);
        resMap.put("data", null);
        resMap.put("result", "fail");
        resMap.put("resultInfo", errorMessage);
        return resMap;
    }
}
