package pers.flyingbear.configRoute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 1.读取路由配置，自动构建路由
 */
@Component
@Slf4j
public class MyRouteDefinitionLocator implements RouteDefinitionLocator {
    public final static String GATEWAY_ROUTER_KEY = "gateway_dynamic_route";

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 从redis获取路由信息
     * @return
     */
    @Override
    public Flux<RouteDefinition> getRouteDefinitions() {
        List<RouteDefinition> routeDefinitions = new ArrayList();
        ObjectMapper objectMapper = new ObjectMapper();
        redisTemplate.opsForHash().values(GATEWAY_ROUTER_KEY).stream().forEach(route ->
                {
                    try {
                        routeDefinitions.add(objectMapper.readValue(route.toString(), RouteDefinition.class));
                    } catch (JsonProcessingException e) {
                        log.error("从redis中获取动态路由转换失败：", e);
                    }
                }
        );
        return Flux.fromIterable(routeDefinitions);
    }

}