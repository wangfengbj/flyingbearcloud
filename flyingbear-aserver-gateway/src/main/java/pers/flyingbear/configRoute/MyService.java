package pers.flyingbear.configRoute;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import pers.flyingbear.tool.model.ApiResponse;

import javax.annotation.Resource;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MyService {
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private ApplicationEventPublisher publisher;

    /**
     * 从存储中获取数据，放redis中，添加监听刷新路由
     * @return
     */
    public ApiResponse refreshRoute() {
        try {
            // 查询路由
            RouteDefinition routeDefinition = new RouteDefinition();
            routeDefinition.setId("wf");
            routeDefinition.setUri(new URI("lb://wf"));
            // 断言
            List<PredicateDefinition> predicates = new ArrayList<>();
            PredicateDefinition predicateDefinition = new PredicateDefinition();
            predicateDefinition.setName("Path");
            Map<String, String> args = new HashMap<>();
            args.put("_genkey_0", "/wf/**");
            predicateDefinition.setArgs(args);
            predicates.add(predicateDefinition);
            routeDefinition.setPredicates(predicates);
            // 过滤器
            List<FilterDefinition> filters = new ArrayList<>();
            FilterDefinition filterDefinition = new FilterDefinition();
            filterDefinition.setName("StripPrefix");
            Map<String, String> argF = new HashMap<>();
            argF.put("_genkey_0", "1");
            filterDefinition.setArgs(argF);
            filters.add(filterDefinition);
            routeDefinition.setFilters(filters);
            ObjectMapper objectMapper = new ObjectMapper();

            redisTemplate.opsForHash().put(MyRouteDefinitionLocator.GATEWAY_ROUTER_KEY, routeDefinition.getId(), objectMapper.writeValueAsString(routeDefinition));
            this.publisher.publishEvent(new RefreshRoutesEvent(this));
            return ApiResponse.success("操作成功");
        } catch (URISyntaxException | JsonProcessingException e) {
            e.printStackTrace();
            return ApiResponse.success("操作失败");
        }
    }
}
