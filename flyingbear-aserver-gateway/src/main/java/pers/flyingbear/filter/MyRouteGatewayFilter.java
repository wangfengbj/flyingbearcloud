package pers.flyingbear.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.util.PathMatcher;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import pers.flyingbear.config.NoAuthUrlConfig;
import pers.flyingbear.util.MySessionUtil;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.List;

/**
 * 局部过滤器，只针对路由，需要工厂注入
 */
@Component
@Slf4j
public class MyRouteGatewayFilter implements GatewayFilter, Ordered {
    @Resource
    private NoAuthUrlConfig noAuthUrlConfig;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        ServerHttpResponse serverHttpResponse = exchange.getResponse();
        String url = serverHttpRequest.getURI().getPath();
        RequestPath url2 = serverHttpRequest.getPath();

        boolean is_pass = shouldSkip(url);
        if(is_pass){
            return chain.filter(exchange);
        }else{
            // 认证
            MultiValueMap<String, String> queryParams = serverHttpRequest.getQueryParams();
            String call = queryParams.getFirst("call");
            // 自己取cookie，第一取是空的
            List<String> springSessionKeys = MySessionUtil.readCookieValues(serverHttpRequest);

            if(!StringUtils.isEmpty(call)){
                serverHttpResponse.setStatusCode(HttpStatus.UNAUTHORIZED);
                //serverHttpResponse.setStatusCode(HttpStatus.OK);
                //serverHttpResponse.setStatusCode(HttpStatus.FOUND); //重定向
                //serverHttpResponse.getHeaders().set(HttpHeaders.LOCATION, call); // 设置地址
                return Mono.empty();
            }else{
                return exchange.getSession().flatMap(WebSession->{
                    String session_id = WebSession.getId();
                    log.info("WebSession的SessionId："+session_id);
                    return chain.filter(exchange);
                }).then(Mono.fromRunnable(() -> {
                    log.info("-----------走到then----------");
                }));
            }
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * 使用PathMatcher判断
     * @param currentUrl
     * @return
     */
    private boolean shouldSkip(String currentUrl) {
        List<String> skips = noAuthUrlConfig.getShouldSkipUrls();
        PathMatcher pathMatcher = new AntPathMatcher();
        for (String url : skips) {
            if (pathMatcher.match(url, currentUrl)) {
                return true;
            }
        }
        return false;
    }
}
