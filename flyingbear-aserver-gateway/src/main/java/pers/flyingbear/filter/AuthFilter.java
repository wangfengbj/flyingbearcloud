package pers.flyingbear.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 全局认证过滤器，自定义全局Filter是GlobalFilter子类，局部Filter是GatewayFilter子类
 * 1. 判断是否已经认证
 * 2. ServerHttpRequest修改请求头【默认请求头中途不让修改】
 * 3. 过滤OPTIONS请求方法【获取服务器支持的HTTP请求方法，也是黑客经常使用的方法；用来检查服务器的性能；】
 */
@Component
@Slf4j
public class AuthFilter implements GlobalFilter, Ordered {
    private static final String AUTHORIZE_TOKEN = "token";

    @Override
    public int getOrder() {
        return 3;
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        // 1. 可以设置ip白名单
        InetSocketAddress remoteAddress = request.getRemoteAddress();
        log.info("请求来源于ip:"+remoteAddress.getAddress().getHostAddress());


        // 2. 拦截特定URL地址
        log.info("请求的url地址:"+request.getPath());
        String path = request.getURI().getPath();
        if(path.equals("/login")){
            return chain.filter(exchange);
        }


        // 3. URL参数获取
        MultiValueMap<String, String> queryParams = request.getQueryParams();
        // 4. 请求头
        HttpHeaders headers = request.getHeaders();
        // 方法一
        String token = headers.getFirst(AUTHORIZE_TOKEN);
        if(StringUtils.isEmpty(token)){ return fallBack(response, HttpStatus.UNAUTHORIZED); }
        // 方法二
        if (headers.containsKey(HttpHeaders.AUTHORIZATION)) {
            // String token = exchange.getRequest().getQueryParams().getFirst("token");
            List<String> keys = headers.get(HttpHeaders.AUTHORIZATION);

            // 方案一：开放权限
            headers= HttpHeaders.writableHttpHeaders(headers);
            headers.set(HttpHeaders.AUTHORIZATION, "wangFeng");

            // 方案二：
            //request.mutate().header(HttpHeaders.AUTHORIZATION, "wangFeng").build();


            //response.setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            //return response.setComplete();
        }


        // 4. 过滤OPTIONS请求方法
        if (request.getMethod() == HttpMethod.OPTIONS) {
            response.setStatusCode(HttpStatus.OK);
            return Mono.empty(); // Mono.empty()仅让filter停止
        }

        return chain.filter(exchange);
    }


    /**
     * 参考RedirectToGatewayFilterFactory类
     * 只能在HttpStatus是3xx下返回，如果使用其他返回值无法带返回结果
     * @param response
     * @param httpStatus
     * @return
     */
    private Mono<Void> fallBack(ServerHttpResponse response, HttpStatus httpStatus){
        response.setStatusCode(httpStatus);
        // Mono.empty()仅让filter停止
        return response.setComplete();
    }


    /**
     * 返回自定义信息
     * @param response
     * @param httpStatus
     * @param message
     * @return
     */
    private Mono<Void> returnMessage(ServerHttpResponse response, HttpStatus httpStatus, String message){
        response.setStatusCode(httpStatus == null ? HttpStatus.OK: httpStatus);
        //response.getHeaders().setContentType(MediaType.APPLICATION_JSON_UTF8);

        Map<String, String> map = new HashMap<>();
        map.put("code", "000001");
        map.put("message", message);

        ObjectMapper objectMapper = new ObjectMapper();
        DataBuffer wrap = null;
        try {
            wrap = response.bufferFactory().wrap(objectMapper.writeValueAsBytes(map));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
//        DataBuffer finalWrap;
//        return response.writeWith(Mono.fromSupplier(() -> finalWrap));
        return response.writeWith(Mono.just(wrap));
    }




}
