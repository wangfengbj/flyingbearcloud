package pers.flyingbear.filter;

import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 通过工厂把局部过滤器注入spring
 */
@Component
public class MyRouteGatewayFilterFactory extends AbstractGatewayFilterFactory {

    @Resource
    private MyRouteGatewayFilter myRouteGatewayFilter;

    @Override
    public GatewayFilter apply(Object config) {
        return myRouteGatewayFilter;
    }
}
