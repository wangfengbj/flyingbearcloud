package pers.flyingbear.filter;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.*;

/**
 * 1. 解决HttpServletRequest请求头默认不能修改,使用包装器
 * HeaderMapRequestWrapper requestWrapper = new HeaderMapRequestWrapper(req);
 */
public class HeaderMapRequestWrapper extends HttpServletRequestWrapper {
    public HeaderMapRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    private Map<String, String> headerMap = new HashMap<String, String>();

    /**
     * 添加头
     * @param key
     * @param value
     */
    public void addHeader(String key, String value) {
        headerMap.put(key, value);
    }

    /**
     * 获取头内容
     * @param key
     * @return
     */
    @Override
    public String getHeader(String key) {
        String headerValue = super.getHeader(key);
        if (headerMap.containsKey(key)) {
            headerValue = headerMap.get(key);
        }
        return headerValue;
    }

    /**
     * 获取请求头的所有key
     */
    @Override
    public Enumeration<String> getHeaderNames() {
        List<String> keys = Collections.list(super.getHeaderNames());
        for (String key : headerMap.keySet()) {
            keys.add(key);
        }
        return Collections.enumeration(keys);
    }

    /**
     * 获取key的所有值
     * @param key
     * @return
     */
    @Override
    public Enumeration<String> getHeaders(String key) {
        List<String> values = Collections.list(super.getHeaders(key));
        if (headerMap.containsKey(key)) {
            values.add(headerMap.get(key));
        }
        return Collections.enumeration(values);
    }
}
