package pers.flyingbear.filter;


import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.CachedBodyOutputMessage;
import org.springframework.cloud.gateway.support.BodyInserterContext;
import org.springframework.core.Ordered;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.buffer.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.codec.HttpMessageReader;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.http.codec.multipart.FormFieldPart;
import org.springframework.http.codec.multipart.Part;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.HandlerStrategies;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import pers.flyingbear.vo.GatewayContext;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 1. 方案一解决Request的body只能读取一次，后面直接取不用放回
 */
@Component
public class MyBodyFilter implements GlobalFilter, Ordered {
    private static final Logger log = LoggerFactory.getLogger(MyBodyFilter.class);
    private static final List<HttpMessageReader<?>> messageReaders = HandlerStrategies.withDefaults().messageReaders();
    private ParameterizedTypeReference<MultiValueMap<String, Part>> MULTI_PART = new ParameterizedTypeReference<MultiValueMap<String, Part>>(){};


//    public MyBodyFilter(ServerCodecConfigurer configurer) {
//        this.messageReaders = configurer.getReaders();
//    }



    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        // body内容缓存 后面filter取值: GatewayContext gatewayContext = exchange.getAttribute(GatewayContext.CACHE_GATEWAY_CONTEXT);
        GatewayContext gatewayContext = new GatewayContext();
        exchange.getAttributes().put(GatewayContext.CACHE_GATEWAY_CONTEXT, gatewayContext);

        ServerHttpRequest request = exchange.getRequest();
        HttpHeaders headers = request.getHeaders();

        // 处理某些类型
        MediaType contentType = headers.getContentType();
        long contentLength = headers.getContentLength();
        if (contentLength > 0) {
            if (MediaType.APPLICATION_JSON.isCompatibleWith(contentType) || MediaType.APPLICATION_JSON_UTF8.isCompatibleWith(contentType)) {
                return DataBufferUtils.join(exchange.getRequest().getBody()).flatMap(dataBuffer -> {
                            //DataBufferUtils.retain(dataBuffer);
                            byte[] bytes = new byte[dataBuffer.readableByteCount()];
                            dataBuffer.read(bytes);
                            // 释放堆外内存
                            DataBufferUtils.release(dataBuffer);
                            Flux<DataBuffer> cachedFlux = Flux.defer(() -> {
                                DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
                                DataBufferUtils.retain(buffer);
                                return Mono.just(buffer);
                            });

                            /**
                             * request请求中的body内容读出来，并且使用ServerHttpRequestDecorator这个请求装饰器对request进行包装，重写getBody方法，
                             * 并把包装后的请求放到过滤器链中传递下去。这样后面的过滤器中再使用exchange.getRequest().getBody()来获取body时
                             * 实际上就是调用的重载后的getBody()方法，获取的最先已经缓存了的body数据。这样就能够实现body的多次读取了
                             */
                            ServerHttpRequest mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
                                @Override
                                public Flux<DataBuffer> getBody() {
                                    return cachedFlux;
                                }

                                @Override
                                public HttpHeaders getHeaders() {
                                    return headers;
                                }
                            };
                    ServerWebExchange mutatedExchange = exchange.mutate().request(mutatedRequest).build();
                    return chain.filter(mutatedExchange);
//                    return ServerRequest.create(mutatedExchange, messageReaders).bodyToMono(String.class)
//                            .doOnNext(objectValue -> { log.debug("[GatewayContext]Read JsonBody:{}", objectValue);
//                            }).then(chain.filter(mutatedExchange));
                });
            }else if(MediaType.MULTIPART_FORM_DATA.isCompatibleWith(contentType)){
                return readMultiPartFormData(exchange, chain, gatewayContext);
            }
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }


    private Mono<Void> readMultiPartFormData(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext) {
        // 当body为空时，只会执行这一个拦截器， 原因是fileMap中的代码没有执行，所以需要在body为空时构建一个空的缓存
        DefaultDataBufferFactory defaultDataBufferFactory = new DefaultDataBufferFactory();
        DefaultDataBuffer defaultDataBuffer = defaultDataBufferFactory.allocateBuffer(0);
        Mono<DataBuffer> mono = Flux.from(exchange.getRequest().getBody().defaultIfEmpty(defaultDataBuffer))
                .collectList().filter(list -> {
                    log.info("请求体缓存过滤器：body为空");
                    return true;
                }).map(list -> list.get(0).factory().join(list)).doOnDiscard(PooledDataBuffer.class, DataBufferUtils::release);
        return mono.flatMap(dataBuffer -> {
            byte[] bytes = new byte[dataBuffer.readableByteCount()];
            dataBuffer.read(bytes);
            DataBufferUtils.release(dataBuffer);
            ServerHttpRequestDecorator mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
                @Override
                public Flux<DataBuffer> getBody() {
                    return Flux.defer(() -> {
                        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
                        DataBufferUtils.retain(buffer);
                        return Mono.just(buffer);
                    });
                }
            };
            ServerWebExchange mutatedExchange = exchange.mutate().request(mutatedRequest).build();
            return ServerRequest.create(mutatedExchange, messageReaders).bodyToMono(MULTI_PART)
                    .doOnNext(multiPartMap -> { gatewayContext.setMultiPartParams(multiPartMap); }).then(chain.filter(mutatedExchange));
        });
    }


    private Mono<Void> readBody(ServerWebExchange exchange, GatewayFilterChain chain, GatewayContext gatewayContext) {
        // 当body为空（请求体中"{}"都不存在）时，只会执行这一个拦截器， 原因是fileMap中的代码没有执行，所以需要在body为空时构建一个空的缓存
        DefaultDataBufferFactory defaultDataBufferFactory = new DefaultDataBufferFactory();
        DefaultDataBuffer defaultDataBuffer = defaultDataBufferFactory.allocateBuffer(0);
        Mono<DataBuffer> mono = Flux.from(exchange.getRequest().getBody().defaultIfEmpty(defaultDataBuffer))
                .collectList().filter(list -> {
                    log.info("请求体缓存过滤器：body为空");
                    return true;
                }).map(list -> list.get(0).factory().join(list)).doOnDiscard(PooledDataBuffer.class, DataBufferUtils::release);
        return mono.flatMap(dataBuffer -> {
            byte[] bytes = new byte[dataBuffer.readableByteCount()];
            dataBuffer.read(bytes);
            DataBufferUtils.release(dataBuffer);
            ServerHttpRequestDecorator mutatedRequest = new ServerHttpRequestDecorator(exchange.getRequest()) {
                @Override
                public Flux<DataBuffer> getBody() {
                    return Flux.defer(() -> {
                        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
                        DataBufferUtils.retain(buffer);
                        return Mono.just(buffer);
                    });
                }
            };
            ServerWebExchange mutatedExchange = exchange.mutate().request(mutatedRequest).build();
            return ServerRequest.create(mutatedExchange, messageReaders)
                    .bodyToMono(String.class)
                    .doOnNext(objectValue -> { gatewayContext.setJsonBody(objectValue); }).then(chain.filter(mutatedExchange));
        });
    }


    /**
     * 文件表单类型读取除file外字段进行签名
     * @param multiPartParams
     * @return
     */
    private Map<String, String> readFormSignBody(MultiValueMap<String, Part> multiPartParams) {
        Map<String, String> params = Maps.newHashMap();
        if (Objects.nonNull(multiPartParams) && !multiPartParams.isEmpty()) {
            for(Map.Entry<String, List<Part>> entry : multiPartParams.entrySet()) {
                String key = entry.getKey();
                List<Part> value = entry.getValue();
                if (StringUtils.isBlank(key) || CollectionUtils.isEmpty(value)) {
                    continue;
                }
                for (Part part : entry.getValue()) {
                    // 文件不参与签名
                    if (part instanceof FilePart) {
                        continue;
                    }
                    if (!(part instanceof FormFieldPart)) {
                        log.error("multipart/formdata Part 类型即不是file也不是formfield，class - {}！", part.getClass().getCanonicalName());
                        continue;
                    }
                    AtomicReference<String> valueHolder = new AtomicReference<String>();
                    part.content().subscribe(buffer -> {
                        byte[] datas = new byte[buffer.readableByteCount()];
                        buffer.read(datas);
                        DataBufferUtils.release(buffer);
                        //if (ArrayUtil.isNotEmpty(datas)) { // 判断
                        if(datas.length > 0){
                            String paramValue = new String(datas);
                            if (StringUtils.isNotEmpty(paramValue)) {
                                valueHolder.set(paramValue);
                            }
                        }
                    });
                    params.put(key, valueHolder.get());
                }
            }
        }
        return params;
    }

    /**
     * 修改原请求体内容
     */
    private Mono<Void> transferBody(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("第三方请求过滤器处理 --- 请求体处理 ---- start");
        ServerRequest serverRequest = ServerRequest.create(exchange, HandlerStrategies.withDefaults().messageReaders());
        Mono modifiedBody = serverRequest.bodyToMono(String.class).flatMap(oldBody -> {
            // 对原始请求body进行封装，格式：{ "body": 原始 json 体}
            // 当然这里也可以将修改后的请求体覆盖到GatewayContext缓存中，这里没有覆盖是因为想要保留最原始的请求体内容
            return Mono.just("{body:\"更改后\"}");
        });
        BodyInserter bodyInserter = BodyInserters.fromPublisher(modifiedBody, String.class);
        HttpHeaders headers = new HttpHeaders();
        headers.putAll(exchange.getRequest().getHeaders());
        headers.remove(HttpHeaders.CONTENT_LENGTH);
        CachedBodyOutputMessage outputMessage = new CachedBodyOutputMessage(exchange, headers);
        Mono mono = bodyInserter.insert(outputMessage, new BodyInserterContext())
                .then(Mono.defer(() -> {
                    ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(
                            exchange.getRequest()) {
                        @Override
                        public HttpHeaders getHeaders() {
                            long contentLength = headers.getContentLength();
                            HttpHeaders httpHeaders = new HttpHeaders();
                            httpHeaders.putAll(super.getHeaders());
                            if (contentLength > 0) {
                                httpHeaders.setContentLength(contentLength);
                            } else {
                                httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                            }
                            return httpHeaders;
                        }

                        @Override
                        public Flux<DataBuffer> getBody() {
                            return outputMessage.getBody();
                        }
                    };
                    return chain.filter(exchange.mutate().request(decorator).build());
                }));
        log.info("第三方请求过滤器处理 --- 请求体处理 ---- end");
        return mono;
    }



    /**
     * 从缓存中读取请求体，exchange.getRequest().getBody()
     */
    public String resolveBodyFromRequest(Flux<DataBuffer> body) {
        AtomicReference<String> bodyRef = new AtomicReference<>();
        // 缓存读取的request body信息
        body.subscribe(dataBuffer -> {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(dataBuffer.asByteBuffer());
            DataBufferUtils.release(dataBuffer);
            bodyRef.set(charBuffer.toString());
        });
        return bodyRef.get();
    }
}
