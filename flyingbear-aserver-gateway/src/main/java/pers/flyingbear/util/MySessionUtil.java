package pers.flyingbear.util;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpCookie;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public class MySessionUtil {
    private static String cookieName = "SESSION";
    private static boolean useBase64Encoding = true;

    public static List<String> readCookieValues(HttpServletRequest request) {
        //从请求头中获取cookies
        Cookie[] cookies = request.getCookies();
        List<String> matchingCookieValues = new ArrayList<>();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                //获取存放sessionid的那个cookie，cookieName默认是SESSION
                if (cookieName.equals(cookie.getName())) {
                    //默认的话sessionid是加密的
                    String sessionId = (useBase64Encoding
                            ? new String(Base64.decodeBase64(cookie.getValue()))
                            : cookie.getValue());
                    if (sessionId == null) {
                        break;
                    }
                    matchingCookieValues.add(sessionId);
                }
            }
        }
        return matchingCookieValues;
    }

    public static List<String> readCookieValues(ServerHttpRequest request) {
        //从请求头中获取cookies
        MultiValueMap<String, HttpCookie> cookies = request.getCookies();
        List<String> matchingCookieValues = new ArrayList<>();
        if (cookies != null) {
            for (String key : cookies.keySet()) {
                //获取存放sessionid的那个cookie，cookieName默认是SESSION
                if (cookieName.equals(key)) {
                    List<HttpCookie> values = cookies.get(key);
                    for (HttpCookie value : values) {
                        //默认的话sessionid是加密的
                        String sessionId = (useBase64Encoding
                                ? new String(Base64.decodeBase64(value.getValue()))
                                : value.getValue());
                        if (sessionId == null) {
                            break;
                        }
                        matchingCookieValues.add(sessionId);
                    }
                }
            }
        }
        return matchingCookieValues;
    }

}
