package pers.flyingbear.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 哪些地址不需要鉴权
 */
@Component
@ConfigurationProperties(prefix = "noauth.gateway")
public class NoAuthUrlConfig {
    private List<String> shouldSkipUrls;

    public List<String> getShouldSkipUrls() {
        return shouldSkipUrls;
    }

    public void setShouldSkipUrls(List<String> shouldSkipUrls) {
        this.shouldSkipUrls = shouldSkipUrls;
    }
}
