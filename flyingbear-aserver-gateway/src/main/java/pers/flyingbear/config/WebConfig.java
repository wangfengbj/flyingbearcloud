package pers.flyingbear.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.ViewResolverRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.result.view.UrlBasedViewResolver;
import org.springframework.web.reactive.result.view.ViewResolver;

@Configuration
@EnableWebFlux
public class WebConfig implements WebFluxConfigurer {
    /**
     * 增加视图解析支持redirect
     * @param registry
     */
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        ViewResolver viewResolver = new UrlBasedViewResolver();
        registry.viewResolver(viewResolver);
    }
}
