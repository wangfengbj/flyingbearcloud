package pers.flyingbear.service;

import io.seata.core.context.RootContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class StorageService {
    public static final String SUCCESS = "SUCCESS";
    public static final String FAIL = "FAIL";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    //减库存
    public void deduct(String commodityCode, int count) {
        String xid = RootContext.getXID();
        System.out.println("storage减库存分布式事务id: " + xid);
        jdbcTemplate.update("update storage_tbl set count = count - ? where commodity_code = ?", new Object[] { count, commodityCode});
    }
}
