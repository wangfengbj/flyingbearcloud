package pers.flyingbear.mapper;

import org.apache.ibatis.annotations.Mapper;
import pers.flyingbear.model.QuartzBusiness;
import pers.flyingbear.model.QuartzBusinessVO;

import java.util.List;
import java.util.Map;

@Mapper
public interface QuartzBusinessMapper {
    List<QuartzBusinessVO> selectQuartzList(Map<String, Object> m);
    QuartzBusinessVO selectQuartzOne(Integer id);
    int save(QuartzBusiness quartzBusiness);
    int update(QuartzBusiness quartzBusiness);
    int updateState(Integer id);
}
