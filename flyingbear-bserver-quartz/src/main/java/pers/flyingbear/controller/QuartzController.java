package pers.flyingbear.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import pers.flyingbear.model.QuartzBusiness;
import pers.flyingbear.model.QuartzBusinessVO;
import pers.flyingbear.service.MyQuartzService;
import pers.flyingbear.tool.model.ApiResponse;
import pers.flyingbear.tool.sign.AddGroup;
import pers.flyingbear.tool.sign.UpdateGroup;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
public class QuartzController {
    @Resource
    private MyQuartzService myQuartzService;

    /**
     * 查询所有定时任务
     * @param param
     * @return
     */
    @GetMapping("/quartzList")
    public Map<String, Object> quartzList(@RequestParam Map<String, String> param){
        return myQuartzService.quartzList(param);
    }
    @PostMapping("/quartzAdd")
    public ApiResponse quartzAdd(@RequestBody @Validated({AddGroup.class}) QuartzBusiness quartzBusiness){
        return myQuartzService.quartzEdit(quartzBusiness);
    }
    @PostMapping("/quartzEdit")
    public ApiResponse quartzEdit(@RequestBody @Validated({UpdateGroup.class}) QuartzBusiness quartzBusiness){
        return myQuartzService.quartzEdit(quartzBusiness);
    }

    @GetMapping(value = "/getServicesList")
    public List<String> getServicesList(){
        return myQuartzService.getServicesList();
    }

    @GetMapping(value = "/quartzOne")
    public QuartzBusinessVO queryBusinessInfoOne(@RequestParam Integer id){
        return myQuartzService.queryBusinessInfoOne(id);
    }


    @PostMapping(value = "/startJob")
    public ApiResponse startJob(@RequestBody Map<String, Object> map){
        Integer id = Integer.valueOf(map.get("id").toString());
        return myQuartzService.startJob(id);
    }

    @PostMapping(value = "/removeJob")
    public ApiResponse removeJob(@RequestBody Map<String, Object> map){
        Integer id = Integer.valueOf(map.get("id").toString());
        return myQuartzService.removeJob(id);
    }

    @PostMapping(value = "/stopJob")
    public ApiResponse stopJob(@RequestBody Map<String, Object> map){
        Integer id = Integer.valueOf(map.get("id").toString());
        return myQuartzService.stopJob(id);
    }
}
