package pers.flyingbear.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.springframework.cloud.openfeign.FeignClientBuilder;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.util.StringUtils;
import pers.flyingbear.client.CommonClient;
import pers.flyingbear.sp.ApplicationFactory;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

/**
 * 执行JOB
 */
@DisallowConcurrentExecution  // 不开启并发
@Slf4j
public class MyJob extends QuartzJobBean {
    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("*********MyJob执行*********");
        // 预期执行时间
        Date expect_date = jobExecutionContext.getScheduledFireTime();
        // 实际执行时间
        Date actual_date = jobExecutionContext.getFireTime();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String actual_time = dtf.format(LocalDateTime.ofInstant(actual_date.toInstant(), ZoneId.systemDefault()));
        String expect_time = dtf.format(LocalDateTime.ofInstant(expect_date.toInstant(), ZoneId.systemDefault()));

        Trigger trigger = jobExecutionContext.getTrigger();
        JobDataMap jobDataMap = trigger.getJobDataMap();
        TriggerKey triggerKey = trigger.getKey();
        String group_name = triggerKey.getGroup();
        String job_name = triggerKey.getName();
        String uri = jobDataMap.get("uri") == null ? "":(String)jobDataMap.get("uri");
        String path = jobDataMap.get("path") == null ? "":(String)jobDataMap.get("path");
        jobDataMap.remove("uri");
        jobDataMap.remove("path");


        FeignClientBuilder builder = new FeignClientBuilder(ApplicationFactory.getApplicationContext());
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            if(StringUtils.hasLength(uri) && StringUtils.hasLength(path)){
                if(uri.startsWith("lb://")){ //依赖注册中线的负载均衡模式
                    uri = uri.substring(5);
                    CommonClient commonClient = builder.forType(CommonClient.class, uri).build();
                    Map<String, Object> rst = commonClient.executeJobGet(path, objectMapper.writeValueAsString(jobDataMap));
                    log.info("定时任务执行成功！group_name="+group_name+"job_name="+job_name+"预期执行时间：【"+expect_time+"】实际执行时间：【"+actual_time+"】 -> 远程访问响应信息："+rst);
                } else if(uri.startsWith("http://")){//http请求模式，只是dome，不保证可用
                    CommonClient jobClient = builder.forType(CommonClient.class, uri).url(uri).build();
                    Map<String, Object> rst = jobClient.executeJobGet(path,  objectMapper.writeValueAsString(jobDataMap));
                    log.info("定时任务执行成功！group_name="+group_name+"job_name="+job_name+"预期执行时间：【"+expect_time+"】实际执行时间：【"+actual_time+"】 -> 远程访问响应信息："+rst);
                } else {
                    log.error("定时任务执行失败！group_name="+group_name+"job_name="+job_name+"预期执行时间：【"+expect_time+"】实际执行时间：【"+actual_time+"】 -> jobDataMap="+objectMapper.writeValueAsString(jobDataMap));
                }
            }
        }catch (JsonProcessingException e){
            e.printStackTrace();
        }


    }
}
