package pers.flyingbear.util;

@FunctionalInterface
public interface StreamFunctionWrap<T, R> {
    R apply(T t) throws Exception;
}
