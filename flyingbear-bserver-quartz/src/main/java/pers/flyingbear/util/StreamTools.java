package pers.flyingbear.util;


import java.util.function.Function;

public class StreamTools {

    public static <T,R> Function<T,R> exceptionWrap(StreamFunctionWrap<T, R> dof) {
        return t -> {
            R r = null;
            try {
                r = dof.apply(t);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return r;
            }
        };
    }
}
