package pers.flyingbear;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import pers.flyingbear.tool.util.MyRandomPort;


//@EnableScheduling
//@EnableFeignClients
@SpringBootApplication
@EnableDiscoveryClient  // @EnableEurekaClient 注册中心客户端
public class QuartzServer {
    public static void main(String[] args) {
        // 热部署会导致main执行两次
        new MyRandomPort(args);
        SpringApplication.run(QuartzServer.class, args);
    }
}
