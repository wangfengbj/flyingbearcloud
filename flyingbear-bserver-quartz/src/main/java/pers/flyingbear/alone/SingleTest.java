package pers.flyingbear.alone;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.calendar.HolidayCalendar;
import org.quartz.impl.matchers.KeyMatcher;

import java.util.Date;

/**
 * quartz 单独使用，自定义JOB需要实现Job接口；Job被创建后，可以保存在Scheduler中，与Trigger是独立的，同一个Job可以有多个Trigger
 * TriggerBuilder
 * JobBuilder
 * SimpleScheduleBuilder
 * CronScheduleBuilder
 * DateBuilder
 * CalendarIntervalScheduleBuilder
 * SimpleTrigger：主要用于一次性执行的Job，或者Job在特定的时间点执行，重复执行 N 次，每次执行间隔T个时间单位
 * CronTrigger：
 * JobKey
 * TriggerKey
 * @DisallowConcurrentExecution：不要并发地执行JobDetail
 * @PersistJobDataAfterExecution：执行成功后更新JobDetail中JobDataMap的数据
 * TriggerListener
 * JobListener
 * SchedulerListener
 */
public class SingleTest implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        jobExecutionContext.getTrigger().getJobDataMap();
        jobExecutionContext.getJobDetail().getJobDataMap();
        JobDataMap dataMap = jobExecutionContext.getMergedJobDataMap(); // JobDetail中的JobDataMap和Trigger中的JobDataMap的并集
        try {
            jobExecutionContext.getScheduler().getContext();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        System.out.println("独自运行开火时间："+jobExecutionContext.getFireTime());
    }

    public static void main(String[] args) throws SchedulerException {
        try {
            // 创建一个scheduler调度器
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            scheduler.getContext().put("k1", "v1");

            // 创建一个Trigger
            Trigger trigger = TriggerBuilder.newTrigger()
                    .withDescription("描述")
                    .usingJobData("k2", "v2")  // 设置参数
                    .withIdentity("trigger1", "group1") // 设置名字和分组
                    .startNow() // 开始时间
                    //.startAt(futureDate(5, IntervalUnit.MINUTE))  // 开始时间  evenHourDate(null)下一个小时
                    //.endAt(dateOf(22, 0, 0))  // 结束时间
                    .withPriority(10)  // 优先级
                    /**
                     * SimpleTrigger使用misfire策略默认智能机制(smart policy)动态选择一种策略
                     * MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY
                     * MISFIRE_INSTRUCTION_FIRE_NOW
                     * MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_EXISTING_REPEAT_COUNT
                     * MISFIRE_INSTRUCTION_RESCHEDULE_NOW_WITH_REMAINING_REPEAT_COUNT
                     * MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT
                     * MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_EXISTING_COUNT
                     */
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(3).repeatForever().withMisfireHandlingInstructionNextWithExistingCount())  // dailyAtHourAndMinute(9, 30)   withRepeatCount(10)
                    /**
                     * CronTrigger使用Misfire策略默认智能机制(smart policy)，选择为MISFIRE_INSTRUCTION_FIRE_NOW
                     * MISFIRE_INSTRUCTION_IGNORE_MISFIRE_POLICY
                     * MISFIRE_INSTRUCTION_DO_NOTHING
                     * MISFIRE_INSTRUCTION_FIRE_NOW
                     */
                    //.withSchedule(cronSchedule("0 0/2 8-17 * * ?"))  // cron点火配置
                    //.modifiedByCalendar("myHolidays") // 设置假期时间
                    //.forJob("xx")  // 绑定job
                    .build();
            trigger.getJobDataMap().put("k3", "v3");

            // 创建一个job
            JobDetail job = JobBuilder.newJob(SingleTest.class)
                    .withDescription("描述")
                    .usingJobData("k4", "v4")  // 设置参数
                    .withIdentity("job1", "group1")  // 设置名字和分组
                    .storeDurably(true)  // 非持久并且没有trigger会被自动移除，设置后就不会
                    .requestRecovery(true)  // scheduler发生硬关闭等，重启后会被重新执行
                    .build();
            job.getJobDataMap().put("j2", "jv2");


            // 排除时间也就是假期时间
//            HolidayCalendar cal = new HolidayCalendar();
//            cal.addExcludedDate(new Date());
//            cal.addExcludedDate(new Date(10000));
//            scheduler.addCalendar("myHolidays", cal, false, false);


            // Listener并且不与jobs和触发器一起存储在JobStore中，因此每次运行应用程序时，都需要重新注册该调度程序
//            scheduler.getListenerManager().addJobListener(myJobListener, KeyMatcher.keyEquals(new JobKey("myJobName","myJobGroup")));
//            scheduler.getListenerManager().addSchedulerListener(mySchedListener);
//            scheduler.getListenerManager().removeSchedulerListener(mySchedListener);


            // 注册trigger并启动scheduler
            scheduler.scheduleJob(job, trigger);
            scheduler.start();
            Thread.sleep(10000);
            scheduler.shutdown();
        } catch (SchedulerException e){
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
