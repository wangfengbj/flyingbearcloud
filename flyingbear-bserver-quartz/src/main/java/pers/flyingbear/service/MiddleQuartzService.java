package pers.flyingbear.service;

import org.quartz.CronExpression;
import org.springframework.stereotype.Service;
import pers.flyingbear.config.QuartzTaskState;
import pers.flyingbear.job.MyJob;
import pers.flyingbear.tool.exception.MyException;
import pers.flyingbear.tool.model.ApiResponse;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class MiddleQuartzService {
    @Resource
    private JobService jobService;

    /**
     * JOB状态
     * @param groupName
     * @param jobName
     * @return
     */
    public QuartzTaskState getState(String groupName, String jobName){
        return jobService.getState(jobName, groupName);
    }

    /**
     * JOB名称
     * @param id
     * @return
     */
    public String getJobName(Integer id) {
        return "job_"+id;
    }

    /**
     * 分组名称
     * @param project
     * @param category
     * @return
     */
    public String getGroupName(String project, String category) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(project);
        stringBuilder.append("_");
        stringBuilder.append(category);
        return stringBuilder.toString().toUpperCase();
    }

    private boolean checkJobKey(String jobName, String groupName) {
        return "null".indexOf(jobName) == -1 && "null".indexOf(groupName) == -1;
    }

    /**
     * cron是否正确
     * @param cron
     * @return
     */
    private boolean checkJobCron(String cron) {
        return CronExpression.isValidExpression(cron);
    }


    public ApiResponse createJob(String groupName, String jobName, String cronExpression, String uri, String path, Map<String, Object> JobData) {
        try{
            if(this.checkJobKey(jobName,groupName)){
                if(this.checkJobCron(cronExpression)){
                    Map<String, Object> data = JobData != null ? JobData: new HashMap();
                    data.put("uri",uri);
                    data.put("path",path);
                    String className = MyJob.class.getName();
                    jobService.createJob(className,"", jobName,groupName,cronExpression,JobData);
                    return ApiResponse.success("创建任务成功！");
                }else{
                    return ApiResponse.error("创建任务失败！Cron表达式错误！");
                }
            }else{
                return ApiResponse.error("创建任务失败！请求参数格式不正确！");
            }
        } catch (MyException e) {
            return ApiResponse.error("创建任务失败！"+e.getMessage());
        } catch (Exception e) {
            return ApiResponse.error("创建任务失败！未知的异常");
        }
    }
//
//    public Map<String, Object> updateJob(Map map) {
//        try{
//            String jobName = this.getJobName(map);
//            String groupName = this.getGroupName(map);
//            if(this.checkJobKey(jobName,groupName)){
//                String cronExpression = StringTools.valueOf(map.get("cron"));
//                if(this.checkJobCron(cronExpression)){
//                    Map<String, Object> JobData = new HashMap();
//                    String uri = StringTools.valueOf(map.get("uri"));
//                    String path = StringTools.valueOf(map.get("path"));
//                    Object param = map.get("param");
//                    HashMap data = param!=null?(HashMap)param:new HashMap();
//                    JobData.put("uri",uri);JobData.put("path",path);JobData.putAll(data);
//                    quartzJobService.updateJob(jobName,groupName,cronExpression,JobData);
//                    return MyResult.success("更新任务成功！");
//                }else{
//                    return MyResult.error("更新任务失败！Cron表达式错误！");
//                }
//            }else{
//                return MyResult.error("更新任务失败！请求参数格式不正确！");
//            }
//        } catch (CrontaQuartzException e) {
//            return MyResult.error("更新任务失败！"+e.getMessage());
//        } catch (Exception e) {
//            return MyResult.error("更新任务失败！未知的异常");
//        }
//    }
//
    public ApiResponse removeJob(String groupName, String jobName) {
        try{
            if(this.checkJobKey(jobName,groupName)){
                jobService.removeJob(jobName,groupName);
                return ApiResponse.success("移除任务成功！");
            }else{
                return ApiResponse.error("移除任务失败！请求参数格式不正确！");
            }
        } catch (MyException e) {
            return ApiResponse.error("移除任务失败！"+e.getMessage());
        } catch (Exception e) {
            return ApiResponse.error("移除任务失败！未知的异常");
        }
    }

    public ApiResponse pauseJob(String groupName, String jobName) {
        try{
            if(this.checkJobKey(jobName,groupName)){
                jobService.pauseJob(jobName,groupName);
                return ApiResponse.success("暂停任务成功！");
            }else{
                return ApiResponse.error("暂停任务失败！请求参数格式不正确！");
            }
        } catch (MyException e) {
            return ApiResponse.error("暂停任务失败！"+e.getMessage());
        } catch (Exception e) {
            return ApiResponse.error("暂停任务失败！未知的异常");
        }
    }

    public ApiResponse resumeJob(String groupName, String jobName) {
        try{
            if(this.checkJobKey(jobName,groupName)){
                jobService.resumeJob(jobName,groupName);
                return ApiResponse.success("恢复任务成功！");
            }else{
                return ApiResponse.error("恢复任务失败！请求参数格式不正确！");
            }
        } catch (MyException e) {
            return ApiResponse.error("恢复任务失败！"+e.getMessage());
        } catch (Exception e) {
            return ApiResponse.error("恢复任务失败！未知的异常");
        }
    }
//
//    public Map<String, Object> runJob(Map map) {
//        try{
//            String jobName = this.getJobName(map);
//            String groupName = this.getGroupName(map);
//            if(this.checkJobKey(jobName,groupName)){
//                quartzJobService.runJob(jobName,groupName);
//                return MyResult.success("立即执行任务成功！");
//            }else{
//                return MyResult.error("立即执行任务失败！请求参数格式不正确！");
//            }
//        } catch (CrontaQuartzException e) {
//            return MyResult.error("立即执行任务失败！"+e.getMessage());
//        } catch (Exception e) {
//            return MyResult.error("立即执行任务失败！未知的异常");
//        }
//    }
//
//    public List<Map<String, String>> getAllJobs(Map map) {
//        Object opage = map.get("page");
//        Object orows = map.get("rows");
//        Integer page = ObjectTools.isNull(opage)?1:Integer.valueOf(opage.toString());
//        Integer rows = ObjectTools.isNull(orows)?10:Integer.valueOf(orows.toString());
//        return quartzJobService.getAllJobs(page, rows);
//    }
//
//    public Map<String, Object> startAllJobs() {
//        try{
//            quartzJobService.startAllJobs();
//            return MyResult.success("开启所有的任务成功！");
//        } catch (CrontaQuartzException e) {
//            return MyResult.error("开启所有的任务失败！"+e.getMessage());
//        } catch (Exception e) {
//            return MyResult.error("开启所有的任务失败！未知的异常");
//        }
//    }
//
//    public Map<String, Object> pauseAllJobs() {
//        try{
//            quartzJobService.pauseAllJobs();
//            return MyResult.success("暂停所有的任务成功！");
//        } catch (CrontaQuartzException e) {
//            return MyResult.error("暂停所有的任务失败！"+e.getMessage());
//        } catch (Exception e) {
//            return MyResult.error("暂停所有的任务失败！未知的异常");
//        }
//    }
//
//    public Map<String, Object> resumeAllJobs() {
//        try{
//            quartzJobService.resumeAllJobs();
//            return MyResult.success("恢复所有的任务成功！");
//        } catch (CrontaQuartzException e) {
//            return MyResult.error("恢复所有的任务失败！"+e.getMessage());
//        } catch (Exception e) {
//            return MyResult.error("恢复所有的任务失败！未知的异常");
//        }
//    }
}
