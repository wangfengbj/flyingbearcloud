package pers.flyingbear.service;

import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import org.quartz.impl.matchers.GroupMatcher;
import org.quartz.utils.Key;
import org.springframework.stereotype.Service;
import pers.flyingbear.config.QuartzTaskState;
import pers.flyingbear.tool.exception.MyException;
import pers.flyingbear.util.StreamTools;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class JobService {
    @Resource
    private Scheduler scheduler;

    /**
     * 创建job
     * @param className
     * @param description
     * @param jobName
     * @param groupName
     * @param cronExpression
     * @param param
     */
    public void createJob(String className, String description, String jobName, String groupName, String cronExpression, Map<String, Object> param) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(!checkExist(triggerKey)){
                Class<? extends Job> jobClass = (Class<? extends Job>) Class.forName(className);
                JobDetail jobDetail = JobBuilder.newJob(jobClass)
                        .withIdentity(jobName, groupName) // 设置名称分组
                        .withDescription(description)
                        //.requestRecovery(true)
                        //.usingJobData("k", "v")
                        //.storeDurably() // 即使没有Trigger关联时，也不需要删除该JobDetail
                        .build();
                CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
                CronTrigger trigger = TriggerBuilder.newTrigger()
                        //.forJob(jobDetail1()) // 关联上述的JobDetail
                        .withIdentity(jobName, groupName)
                        .withSchedule(scheduleBuilder)
                        .build();
                if (param != null) {//获得JobDataMap，写入数据
                    trigger.getJobDataMap().putAll(param);
                }
                scheduler.scheduleJob(jobDetail, trigger);  // 关联JobDetail和trigger
            } else {
                throw new MyException("创建任务失败！该任务已创建，请勿重复创建！");
            }
        } catch (Exception e) {
            log.error("创建任务时发生异常！", e);
            throw new MyException("创建任务时发生异常！"+e.getMessage());
        }
    }

    /**
     * 更新JOB，主要更新trigger
     * @param jobName
     * @param groupName
     * @param cronExpression
     * @param param
     */
    public void updateJob(String jobName, String groupName, String cronExpression, Map<String, Object> param) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(checkExist(triggerKey)){
                CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                if (cronExpression != null) {// 表达式调度构建器
                    CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);// 按新的cronExpression表达式重新构建trigger
                    trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
                }
                if (param != null) {//获得JobDataMap，写入数据
                    trigger.getJobDataMap().putAll(param);
                }
                scheduler.rescheduleJob(triggerKey, trigger);// 按新的trigger重新设置job执行
            } else {
                log.info("更新任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
                throw new MyException("更新任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
            }
        } catch (Exception e) {
            log.error("更新任务时发生异常！", e);
            throw new MyException("更新任务时发生异常！"+e.getMessage());
        }
    }

    /**
     * 删除JOB，暂停-》移除-》删除
     * @param jobName
     * @param groupName
     */
    public void removeJob(String jobName, String groupName) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(checkExist(triggerKey)){
                scheduler.pauseTrigger(triggerKey);
                scheduler.unscheduleJob(triggerKey);
                scheduler.deleteJob(JobKey.jobKey(jobName, groupName));
            }else{
                log.info("移除任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
                throw new MyException("移除任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
            }
        } catch (Exception e) {
            log.error("移除任务时发生异常！", e);
            throw new MyException("移除任务时发生异常！"+e.getMessage());
        }
    }


    /**
     * 暂停JOB
     * @param jobName
     * @param groupName
     */
    public void pauseJob(String jobName, String groupName) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(checkExist(triggerKey)){
                scheduler.pauseTrigger(triggerKey);
            }else{
                log.info("暂停任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
                throw new MyException("暂停任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
            }
        } catch (SchedulerException e) {
            log.error("暂停任务时发生异常！", e);
            throw new MyException("暂停任务时发生异常！"+e.getMessage());
        }
    }


    /**
     * 恢复JOB
     * @param jobName
     * @param groupName
     */
    public void resumeJob(String jobName, String groupName) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(checkExist(triggerKey)){
                scheduler.resumeTrigger(triggerKey);
            }else{
                log.info("恢复任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
                throw new MyException("恢复任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
            }
        } catch (SchedulerException e) {
            log.error("恢复任务时发生异常！", e);
            throw new MyException("恢复任务时发生异常！"+e.getMessage());
        }
    }


    /**
     * 立即运行一次JOB
     * @param jobName
     * @param groupName
     */
    public void runJobOnce(String jobName, String groupName) {
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            if(checkExist(triggerKey)){
                scheduler.triggerJob(JobKey.jobKey(jobName, groupName));
            }else{
                log.info("立即运行一次定时任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
                throw new MyException("立即运行一次定时任务失败！该任务不存在！GroupName="+groupName+"JobName"+jobName);
            }
        } catch (SchedulerException e) {
            log.error("立即运行一次定时任务时发生异常！", e);
            throw new MyException("立即运行一次定时任务时发生异常！"+e.getMessage());
        }
    }

    /**
     * 查询所有JOB
     * @param page
     * @param rows
     * @return
     */
    public List<Map<String, String>> getAllJobs(Integer page, Integer rows) {
        List<Map<String, String>> list = new ArrayList();
        try {
            list = scheduler.getJobGroupNames().parallelStream().map(groupName -> GroupMatcher.jobGroupEquals(groupName))
                    .flatMap(StreamTools.exceptionWrap(groupMatcher -> scheduler.getJobKeys(groupMatcher).parallelStream()))
                    .map(StreamTools.exceptionWrap(jobKey -> {
                        String jobName = jobKey.getName(); String groupName = jobKey.getGroup();
                        Trigger.TriggerState triggerState = scheduler.getTriggerState(TriggerKey.triggerKey(jobName, groupName));
                        QuartzTaskState quartzTaskState = this.getState(jobName, groupName);
                        Map<String, String> jobData = new HashMap();
                        jobData.put("JobName", jobName);
                        jobData.put("GroupName", groupName);
                        jobData.put("StateName", quartzTaskState.getInfo());
                        jobData.put("CrontabState", quartzTaskState.getState().toString());
                        jobData.put("TriggerState", triggerState.toString());
                        return jobData;
                    })).skip((page-1)*rows).limit(rows).collect(Collectors.toList());
        } catch (SchedulerException e) {
            log.error("查询所有JOB时发生异常！", e);
        } finally {
            return list;
        }
    }

    /**
     * 暂停所有JOB
     */
    public void pauseAllJobs() {
        try {
            scheduler.pauseAll();
        } catch (Exception e) {
            log.error("暂停所有任务时发生异常！", e);
            throw new MyException("暂停所有任务时发生异常！"+e.getMessage());
        }
    }

    /**
     * 恢复所有的JOB
     */
    public void resumeAllJobs() {
        try {
            scheduler.resumeAll();
        } catch (Exception e) {
            log.error("恢复所有任务时发生异常！", e);
            throw new MyException("恢复所有任务时发生异常！"+e.getMessage());
        }
    }

    /**
     * 查询JOB的运行状态
     * @param jobName
     * @param groupName
     * @return
     */
    public QuartzTaskState getState(String jobName, String groupName) {
        QuartzTaskState code = QuartzTaskState.NONE;
        try {
            TriggerKey triggerKey = TriggerKey.triggerKey(jobName, groupName);
            Trigger.TriggerState triggerState = scheduler.getTriggerState(triggerKey);
            if(Trigger.TriggerState.NONE.equals(triggerState)){
                code = QuartzTaskState.NONE;
            }else if(Trigger.TriggerState.NORMAL.equals(triggerState)){
                code = QuartzTaskState.NORMAL;
            }else if(Trigger.TriggerState.PAUSED.equals(triggerState)){
                code = QuartzTaskState.PAUSED;
            }else if(Trigger.TriggerState.COMPLETE.equals(triggerState)){
                code = QuartzTaskState.COMPLETE;
            }else if(Trigger.TriggerState.ERROR.equals(triggerState)){
                code = QuartzTaskState.ERROR;
            }else if(Trigger.TriggerState.BLOCKED.equals(triggerState)){
                code = QuartzTaskState.BLOCKED;
            }
        } catch (SchedulerException e) {
            log.error("JOB状态查询失败：{}", e.getMessage());
        } finally {
            return code;
        }
    }

    /**
     * 检查job或者触发器是否已经存在
     * @param key
     * @return
     */
    private boolean checkExist(Key key) {
        boolean exist = false;
        try {
            if(key instanceof TriggerKey){
                exist = scheduler.checkExists((TriggerKey) key);
            }else if(key instanceof JobKey){
                exist = scheduler.checkExists((JobKey) key);
            }
        } catch (SchedulerException e) {
            log.error("触发器或者JOB是否存在验证失败：{}", e.getMessage());
        } finally {
            return exist;
        }
    }
}
