package pers.flyingbear.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import pers.flyingbear.config.QuartzTaskState;
import pers.flyingbear.config.TypeEnum;
import pers.flyingbear.mapper.QuartzBusinessMapper;
import pers.flyingbear.model.QuartzBusiness;
import pers.flyingbear.model.QuartzBusinessVO;
import pers.flyingbear.tool.constant.MyDataGrid;
import pers.flyingbear.tool.model.ApiResponse;
import pers.flyingbear.tool.util.MyAssert;
import pers.flyingbear.tool.util.MyStringUtil;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MyQuartzService {
    @Resource
    private QuartzBusinessMapper quartzBusinessMapper;
    @Resource
    private DiscoveryClient discoveryClient;
    @Resource
    private MiddleQuartzService middleQuartzService;

    /**
     * 获取注册中心服务列表
     * @return
     */
    public List<String> getServicesList() {
        List<String> list = discoveryClient.getServices();
        return list;
    }

    /**
     * 查询业务表定时任务
     * @param param
     * @return
     */
    public Map<String, Object> quartzList(Map<String, String> param) {
        String project = param.get("project");

        // 查询
        Map<String, Object> sm = new HashMap<>();
        sm.put("project", project);
        PageHelper.startPage(MyStringUtil.getPage(param.get(MyDataGrid.PAGE)), MyStringUtil.getRow(param.get(MyDataGrid.ROW)));
        List<QuartzBusinessVO> list = quartzBusinessMapper.selectQuartzList(sm);
        PageInfo<QuartzBusinessVO> pageInfo = new PageInfo<>(list);


        // 处理定时任务状态
        list.forEach(businessInfoVO -> {
            QuartzTaskState quartzTaskState = middleQuartzService.getState(middleQuartzService.getGroupName(businessInfoVO.getProject(),businessInfoVO.getCategory()),
                    middleQuartzService.getJobName(businessInfoVO.getId()));
            businessInfoVO.setRunState(quartzTaskState.getState());
            businessInfoVO.setRunStateName(quartzTaskState.getInfo());
        });
        return MyDataGrid.getResult(list, pageInfo.getTotal());
    }


    public ApiResponse quartzEdit(QuartzBusiness quartzBusiness) {
        MyAssert.relyOnNotNull(quartzBusiness.getType().equals(TypeEnum.SERVICE.getCode()), quartzBusiness.getPath(),"微服务类型的path不能为空");
        if(quartzBusiness.getId() == null){
            int row = quartzBusinessMapper.save(quartzBusiness);
        }else{
            int row = quartzBusinessMapper.update(quartzBusiness);
        }
        return ApiResponse.success();
    }


    public QuartzBusinessVO queryBusinessInfoOne(Integer id) {
        return quartzBusinessMapper.selectQuartzOne(id);
    }


    public ApiResponse startJob(Integer id) {
        Map<String,Object> m = getJobInfo(id);
        if(m == null){
            return ApiResponse.error("启动失败");
        }
        String groupName = middleQuartzService.getGroupName((String) m.get("project"), (String) m.get("category"));
        String jobName = middleQuartzService.getJobName((Integer) m.get("id"));
        QuartzTaskState quartzTaskState = middleQuartzService.getState(groupName, jobName);
        if(QuartzTaskState.NONE == quartzTaskState) {
            ApiResponse rst = middleQuartzService.createJob(groupName, jobName, (String)m.get("cron"),
                    (String)m.get("uri"), (String)m.get("path"), (Map<String, Object>)m.get("param"));
            if("0".equals(rst.getCode())){
                return ApiResponse.success("启动成功");
            } else {
                return ApiResponse.error("启动失败");
            }
        }else {
            // Map<String, Object> rst = crontabService.resumeJob(m);
            // 删除再创建
            ApiResponse rst = middleQuartzService.removeJob(groupName, jobName);
            if("0".equals(rst.getCode())){
                rst = middleQuartzService.createJob(groupName, jobName, (String)m.get("cron"),
                        (String)m.get("uri"), (String)m.get("path"), (Map<String, Object>)m.get("param"));
                if("0".equals(rst.getCode())){
                    return ApiResponse.success("启动成功");
                }else {
                    return ApiResponse.error("启动失败");
                }
            } else {
                return ApiResponse.error("启动失败");
            }
        }

    }

    public ApiResponse removeJob(Integer id) {
        Map<String,Object> m = getJobInfo(id);
        if(m == null){
            return ApiResponse.error("删除失败");
        }
        String groupName = middleQuartzService.getGroupName((String) m.get("project"), (String) m.get("category"));
        String jobName = middleQuartzService.getJobName((Integer) m.get("id"));
        ApiResponse rst = middleQuartzService.removeJob(groupName, jobName);
        if("0".equals(rst.getCode())){
            // 更新数据库状态
            quartzBusinessMapper.updateState(id);
            return ApiResponse.success("删除成功");
        } else {
            return ApiResponse.error("删除失败");
        }
    }

    public ApiResponse stopJob(Integer id) {
        Map<String,Object> m = getJobInfo(id);
        if(m == null){
            return ApiResponse.error("停止失败");
        }
        String groupName = middleQuartzService.getGroupName((String) m.get("project"), (String) m.get("category"));
        String jobName = middleQuartzService.getJobName((Integer) m.get("id"));
        ApiResponse rst = middleQuartzService.pauseJob(groupName, jobName);
        if("0".equals(rst.getCode())){
            return ApiResponse.success("删除成功");
        } else {
            return ApiResponse.error("删除失败");
        }
    }

    public ApiResponse resumeJob(Integer id) {
        Map<String,Object> m = getJobInfo(id);
        if(m == null){
            return ApiResponse.error("删除失败");
        }
        String groupName = middleQuartzService.getGroupName((String) m.get("project"), (String) m.get("category"));
        String jobName = middleQuartzService.getJobName((Integer) m.get("id"));
        ApiResponse rst = middleQuartzService.resumeJob(groupName, jobName);
        if("0".equals(rst.getCode())){
            // 更新数据库状态
            quartzBusinessMapper.updateState(id);
            return ApiResponse.success("删除成功");
        } else {
            return ApiResponse.error("删除失败");
        }
    }

    private Map<String,Object> getJobInfo(Integer id){
        QuartzBusinessVO bi = quartzBusinessMapper.selectQuartzOne(id);
        Map<String,Object> m = new HashMap<>();
        m.put("project",bi.getProject());
        m.put("category",bi.getCategory());
        m.put("id", String.valueOf(bi.getId()));
        m.put("cron",bi.getCron());
        m.put("path",bi.getPath());
        m.put("uri",bi.getUri());
        String param = bi.getParam();
        ObjectMapper mapper = new ObjectMapper();
        try {
            HashMap hm = mapper.readValue(param,HashMap.class);
            m.put("param",hm);
            return m;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
