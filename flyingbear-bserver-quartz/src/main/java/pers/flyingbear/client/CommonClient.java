package pers.flyingbear.client;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface CommonClient {
    @PostMapping(value="{type}")
    Map<String, Object> executeJobPost(@PathVariable("type") String type, @RequestBody String json);

    @GetMapping(value="{type}")
    Map<String, Object> executeJobGet(@PathVariable("type") String type, @RequestBody String json);
}
