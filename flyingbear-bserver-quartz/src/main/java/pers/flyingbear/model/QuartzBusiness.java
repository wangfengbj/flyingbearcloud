package pers.flyingbear.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import pers.flyingbear.config.TypeEnum;
import pers.flyingbear.tool.annotation.EnumCheck;
import pers.flyingbear.tool.sign.AddGroup;
import pers.flyingbear.tool.sign.UpdateGroup;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class QuartzBusiness implements Serializable {
    @NotNull(message="{id.notNull}",groups = {UpdateGroup.class})
    private Integer id;
    @NotBlank(message="{quartzBusiness.name.notBlank}", groups = {AddGroup.class, UpdateGroup.class})
    private String name;
    @NotBlank(message="{quartzBusiness.project.notBlank}", groups = {AddGroup.class, UpdateGroup.class})
    private String project;
    @NotBlank(message="{quartzBusiness.category.notBlank}", groups = {AddGroup.class, UpdateGroup.class})
    private String category;
    private String path;
    private String uri;
    private String cron;
    private String param;
    private Byte state;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Integer createUser;
    private String createUserName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Integer updateUser;
    private String updateUserName;
//    @Max(value = 2, message = "{quartzBusiness.type.range}", groups = {AddGroup.class, UpdateGroup.class})
//    @Min(value = 1, message = "{quartzBusiness.type.range}", groups = {AddGroup.class, UpdateGroup.class})
    @NotBlank(message = "任务类型不能为空")
    @EnumCheck(clazz = TypeEnum.class, message = "任务类型不合法", groups = {AddGroup.class, UpdateGroup.class})
    private Byte type;
}
