package pers.flyingbear.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class QuartzBusinessVO extends QuartzBusiness implements Serializable {
    private Integer runState;
    private String runStateName;
}
