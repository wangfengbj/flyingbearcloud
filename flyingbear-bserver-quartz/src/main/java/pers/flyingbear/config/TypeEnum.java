package pers.flyingbear.config;


import pers.flyingbear.tool.sign.EnumValidator;

/**
 * 性别
 */
public enum TypeEnum implements EnumValidator {
	LOCAL((byte)1, "本地"), SERVICE((byte)2, "微服务");

	private byte code;

	private String name;

	TypeEnum(byte code, String name) {
		this.code = code;
		this.name = name;
	}

	public byte getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static TypeEnum stateOf(byte index) {
		for (TypeEnum state : values()) {
			if (state.getCode() == index) {
				return state;
			}
		}
		return null;
	}

	@Override
	public Byte getValue() {
		return code;
	}
}
