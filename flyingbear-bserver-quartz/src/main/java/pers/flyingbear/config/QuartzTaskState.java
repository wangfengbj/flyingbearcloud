package pers.flyingbear.config;

public enum QuartzTaskState {
	
	NONE(0, "不存在"),
	NORMAL(1, "正常"),
	PAUSED(2, "暂停"),
	COMPLETE(3, "完成"),
	ERROR(4, "错误"),
	BLOCKED(5, "阻塞");

	private Integer state;
	private String info;

	QuartzTaskState(Integer state, String info){
		this.state = state;
		this.info = info;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

}
