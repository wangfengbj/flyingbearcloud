// require.js设置url参数
require.config({
    urlArgs: 'v=' + version
});

require([ './plugins/require-config' ], function(config) {
    require(["domready", "layui", "flybear"], function(domready, layui, flybear) {
        layui.config({
            version: false //一般用于更新模块缓存，默认不开启。设为 true 即让浏览器不缓存。也可以设为一个固定的值，如：201610
            ,debug: false //用于开启调试模式，默认 false，如果设为 true，则JS模块的节点会保留在页面
        });

        layui.use('table', function(){
            let table = layui.table;
            let dropdown = layui.dropdown;
            table.render({ elem: '#quartz_table', height: 'full-50', url: WEBPATH+'/quartzList', autoSort: false // 禁用前端自动排序
                , page: true, limit: 20, even: true, toolbar: 'default'
                , cols: [[
                     {type: 'checkbox', fixed: 'left'}
                    ,{field: 'name', title: '任务名称', width:'10%', sort: true, fixed: 'left'}
                    ,{field: 'project', title: '项目编码', width:'10%'}
                    ,{field: 'category', title: '分类编码', width:'10%'}
                    ,{field: 'typeName', title: '任务类型', width:'10%'}
                    ,{field: 'uri', title: 'uri', width:'10%'}
                    ,{field: 'path', title: 'path', width:'10%'}
                    ,{field: 'cron', title: 'cron', width:'10%'}
                    ,{field: 'runStateName', title:'执行状态', width:'10%'}
                    ,{field: 'createTime', title:'创建时间', width:'10%'}
                    ,{field: 'updateTime', title:'更新时间', width:'10%'}
                    ,{field: 'cz', title:'操作', width:'15%', fixed: 'right', align:'center', templet: function(d){
                        let str='<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>'+
                            '<a class="layui-btn layui-btn-xs" lay-event="more">更多<i class="layui-icon layui-icon-down"></i></a>';
                        return str;
                    }}
                ]]
            });

            // 头工具栏事件
            table.on('toolbar(quartz_test)', function(obj){
                let checkStatus = table.checkStatus(obj.config.id), data = checkStatus.data; // 获取选中的数据
                switch(obj.event){
                    case 'add':
                        flybear.openPageInWindow("/page/addQuartzBusiness.html","/page/addQuartzBusiness.js",{
                            title: '新增定时任务',
                            area: ['800px', '600px'],
                            beforeJsStart: function(jsModel, layerDom, handle) {
                                jsModel.setParams(null);
                                jsModel.setHandle(handle);
                                jsModel.setCallback(function(handle){
                                    table.reload('quartz_table');
                                    flybear.closeWindow(handle);
                                });
                            }
                        });
                        break;
                    case 'update':
                        if(data.length === 0){
                            layer.msg('请选择一行');
                        } else if(data.length > 1){
                            layer.msg('只能同时编辑一个');
                        } else {
                            layer.alert('编辑 [id]：'+ checkStatus.data[0].id);
                        }
                        break;
                    case 'delete':
                        if(data.length === 0){
                            layer.msg('请选择一行');
                        } else {
                            layer.msg('删除');
                        }
                        break;
                };
            });
            // 单元格工具事件
            table.on('tool(quartz_test)', function(obj){
                let data = obj.data, layEvent = obj.event;
                if(layEvent === 'detail'){
                    layer.msg('查看操作');
                } else if(layEvent === 'more'){
                    //下拉菜单
                    dropdown.render({
                        elem: this // 触发事件的 DOM 对象
                        ,show: true // 外部事件触发即显示
                        ,data: [{title: '编辑', id: 'edit'
                            },{title: '删除', id: 'del'
                        }]
                        ,click: function(menuData){
                            if(menuData.id === 'del'){
                                layer.confirm('真的删除行么', function(index){
                                    obj.del(); // 删除对应行（tr）的DOM结构
                                    layer.close(index);
                                    //向服务端发送删除指令
                                });
                            } else if(menuData.id === 'edit'){
                                let param = {id: data.id}
                                flybear.openPageInWindow("/page/addQuartzBusiness.html","/page/addQuartzBusiness.js",{
                                    title: '编辑定时任务',
                                    area: ['800px', '600px'],
                                    beforeJsStart: function(jsModel, layerDom, handle) {
                                        jsModel.setParams(param);
                                        jsModel.setHandle(handle);
                                        jsModel.setCallback(function(handle){
                                            table.reload('quartz_table');
                                            flybear.closeWindow(handle);
                                        });
                                    }
                                });
                            }
                        }
                        ,align: 'right' //右对齐弹出
                        ,style: 'box-shadow: 1px 1px 10px rgb(0 0 0 / 12%);' //设置额外样式
                    })
                }
            });

        });
    });
});