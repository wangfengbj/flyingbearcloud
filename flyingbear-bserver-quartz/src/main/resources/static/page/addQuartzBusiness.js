define(["layui", "flybear"], function(layui, flybear) {
        let _handle, _param, _callback;
        let form = layui.form;
        let $ = layui.$
        let addQuartzApi = {};

        function getHtml(key, value){
            let html_str='<div class="layui-form-item">'+
                '<div class="layui-input-block layui-col-space5">'+
                '<div class="layui-col-xs5 layui-col-sm5 layui-col-md5">'+
                '<input type="text" required  lay-verify="required" placeholder="键" autocomplete="off" class="layui-input"/>'+
                '</div>'+
                '<div class="layui-col-xs5 layui-col-sm5 layui-col-md5">'+
                '<input type="text" required  lay-verify="required" placeholder="值" autocomplete="off" class="layui-input"/>'+
                '</div>'+
                '<div class="layui-col-xs2 layui-col-sm2 layui-col-md2">'+
                '<button type="button" class="layui-btn layui-btn-sm input-h" onclick="addQuartzApi.delDtcs(this)"><i class="layui-icon layui-icon-subtraction"></i></button></button>'+
                '</div>'+
                '</div>'+
                '</div>';
            return html_str;
        }
        function btnClick(){
            $("#add_dtcs").click(function(){
                $("#addQuartzPage_dtcs").append(getHtml());
            });
        }
        addQuartzApi.delDtcs = function (node){
            node.parentElement.parentElement.remove();
        }
        function saveBtn(){
            // 自定义验证
            form.verify({
                flyCode: function(value, item){ // value：表单的值、item：表单的DOM对象
                    if(!new RegExp("^[a-zA-Z0-9]+$").test(value)){
                        return '只能输入字符串和数字';
                    }
                    if(/^\d+\d+\d$/.test(value)){
                        return '不能全为数字';
                    }
                }
            });
            // 提交事件
            form.on('submit(addQuartzBtn)', function(data){
                let obj = data.field;
                // 删除前后空格
                for (let key in obj) {
                    let val = obj[key];
                    obj[key] = val.trim();
                }
                // 动态参数处理
                let dtcs_list = $("#addQuartzPage_dtcs").children();
                let dtcs = {};
                for(let i=0; i<dtcs_list.length; i++){
                    let dtcs_one_list = $(dtcs_list[i]).children();
                    let data_list = $(dtcs_one_list[0]).children()
                    let key_input = $(data_list[0]).children();
                    let v_input = $(data_list[1]).children();
                    let k = $(key_input).val();
                    let v = $(v_input).val();
                    if(k != "" && v != ""){
                        dtcs[k] = v;
                    }
                }
                obj.param = JSON.stringify(dtcs);
                console.log(JSON.stringify(obj))
                flybear.postJsonNoConfirm("/quartzAdd",obj,null, function(rst){
                    if(flybear.ajaxJsonHandle(rst)){
                        if(_callback){
                            _callback(_handle);
                        }
                    }
                })
                return false;
            });
        }
        function recoverData(){
            form.val("addQuartzBusiness", {
                "name": "123"
                ,"project": "女"
                ,"category": "女"
                ,"type": 1
                ,"uri": "女"
                ,"path": "我爱layui"
                ,"cron": "我爱layui"
            });
        }

        return {
            start: function() {
                btnClick(); // 动态按钮事件
                saveBtn(); // 保存事件
                window.addQuartzApi = addQuartzApi; // 绑定减号事件
                if(_param.id){
                    recoverData(); // 恢复数据
                }
                form.render();
            },
            setHandle: function(handle) {
                _handle = handle;
            },
            setParams: function(param) {
                if (param == null || param == "") {
                    _param = {};
                } else {
                    _param = param;
                }
            },
            setCallback: function(callback) {
                _callback = callback;
            }
        };
    });