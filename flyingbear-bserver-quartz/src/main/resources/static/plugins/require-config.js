// 全局变量
let $j = null;
let zuul_preix = ""; // 使用网关时候加前缀
let webApp = "myApp";

function getWebPath(){
	let localObj = window.location;
	let basePath = localObj.protocol+"//"+localObj.host;
	let contextPath = localObj.pathname.split("/")[1];
	if(contextPath == "page"){
		//项目放到root情况
	} else {
		//basePath += "/"+contextPath;
		basePath = localObj.origin
	}
	return basePath;
}
let WEBPATH = getWebPath();


require.config({
	baseUrl : WEBPATH+zuul_preix+"/plugins",
	paths : {
		"domready" : "domReady",  // require需要
		"layui" : "layui/layui",
		"flybear" : "flybear"
	},
	shim: {
		'layui':{
			exports: "layui"
		},
    },
    map: { // map告诉RequireJS在任何模块之前，都先载入这个css模块
        '*': {
            'css': 'require-css/css.min'
        }
    },
	waitSeconds: 15
});

require([
	// 把自己写的放后面，可以覆盖前面同名字样式
	//"css!"+WEBPATH+"/css/fyw.css",
]);