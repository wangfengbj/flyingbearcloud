//扩展以**开始
String.prototype.startWith = function(str) {
    var reg = new RegExp("^" + str);
    return reg.test(this);
}
//扩展以**结束
String.prototype.endWith = function(str) {
    var reg = new RegExp(str + "$");
    return reg.test(this);
}
//删除左右两端的空格
String.prototype.trim=function(){
    return this.replace(/(^\s*)|(\s*$)/g,'');
}
define(["layui"], function() {
    let layui = require("layui");
    let $ = layui.$
    $.ajaxSetup({ cache: false, timeout: 120000 });
    let layer = layui.layer;


    return{
        isRealNum: function (val) {
            // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
            if (val === "" || val == null) {return false;}
            else if (!isNaN(val)) {return true;}
            else {return false;}
        },
        jump: function(url) {
            window.location.href = WEBPATH + url;
        },
        openWait: function() {
            let handle = layer.load(0, {shade: 0.1});
            return handle;
        },
        closeWait: function(handle) {
            if(handle != null){ layer.close(handle); } else { layer.closeAll('loading'); }
        },
        closeWindow: function(handle) {
            if (handle) {  layer.close(handle); } else { layer.closeAll(); }
        },
        alert: function(type, title, content, yes) {
            let icon;
            if(type == 'success') { icon = 1;}
            else if (type == 'error') { icon = 2;}
            else if (type == 'warning') { icon = 0;}
            else { icon = type; }
            layer.alert(content, {icon: icon, title: title, success: function(layerDom, index){
                    this.enterEsc = function(event){ if(event.keyCode === 13){ layer.close(index); return false; } };
                    $(document).on('keydown', this.enterEsc);
                }, end: function(){ $(document).off('keydown', this.enterEsc); }
            }, function(index, layerDom) { layer.close(index); if(yes != null) { yes(); } });
        },
        msg: function(content){
            layer.msg(content, {icon: 5,anim: 6});
        },
        loginCheck: function(){
            // if(curuser==undefined || curuser==null || curuser.isreg=="0"){
            //     hxflow.jump("/page/login.html");
            //     return false;
            // }else{return true;}
        },
        ajaxJsonHandle: function(rst){
            if(typeof (rst.resultCode) != "undefined"){
                if(rst.resultCode == "success" || rst.resultCode == "0"){
                    this.alert('success','操作提示', rst.resultMsg);
                    return true;
                }else if(rst.resultCode == "error" || (rst.resultCode != "0" && rst.resultCode != "jump")){
                    this.alert('error','操作提示', rst.resultMsg);
                    return false;
                }else if(rst.resultCode == "jump"){
                    this.jump(rst.resultMsg);
                    return false;
                }
            }
            return true;
        },
        getHtml: function(url, param, succFunc, errorFunc) {
            this.openWait();
            let flyBear = this;
            if (param == null) { param = {}; }
            let asyn = function(){
                let dtd = $.Deferred(); // 异步回调函数有状态
                $.ajax({type: "GET", url: WEBPATH + url, // 读取静态html不加随机参数,进行缓存
                    contentType: "text/html; charset=utf-8", dataType: "html",
                    data: param,
                    success: function(message) {
                        // 存储流程常量
                        flyBear.closeWait();
                        dtd.resolve(message);
                    },
                    error: function(message) {
                        flyBear.closeWait();
                        dtd.reject("页面加载失败");
                    }
                });
                return dtd.promise(); // 只允许外部注册函数，不允许调用函数
            }
            let deferred = asyn();
            if (succFunc != null) { deferred.done(succFunc); }
            if (errorFunc != null) { deferred.fail(errorFunc); }
        },
        openPageInWindow: function(url, jsUrl, option){
            let flyBear = this; // 可以验证是否登录
            if(typeof(option) ==  'undefined'){option={}};
            flyBear.getHtml(url, {}, function (rst){
                layer.open({
                    id: typeof(option.id) != 'undefined'? option.id : "",
                    type: typeof(option.type) != 'undefined'? option.type : 1, // 0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
                    title: typeof(option.title) != 'undefined'? option.title : '信息',
                    content: rst,
                    skin: 'demo-class', // layui-layer-lan layui-layer-molv
                    area: typeof(option.area) != 'undefined' ? option.area : 'auto', // ['500px', '300px']/百分比
                    offset: typeof(option.offset) != 'undefined'? option.offset : 'auto',
                    btn: typeof(option.btn) != 'undefined'? option.btn : undefined,
                    btnAlign: typeof(option.btnAlign) != 'undefined'? option.btnAlign : 'r',
                    closeBtn: typeof(option.closeBtn) != 'undefined'? option.closeBtn : 1, // 默认显示关闭按钮
                    shade: typeof(option.shade) != 'undefined'? option.shade : 0.3,
                    shadeClose: typeof(option.shadeClose) != 'undefined'? option.shadeClose : false,
                    anim: 0,
                    maxmin: typeof(option.maxmin) != 'undefined'? option.maxmin : true, // 默认显示最大最小化按钮
                    fixed: typeof(option.fixed) != 'undefined' ? option.fixed : true, // 默认固定窗口，不随浏览器滚动
                    resize: typeof(option.resize) != 'undefined' ? option.resize : false, // 默认不允许拉伸
                    resizing: option.resizing, // 调整窗口尺寸时回调
                    scrollbar: typeof(option.scrollbar) != 'undefined' ? option.scrollbar : true, // 默认允许滚动条
                    zIndex: option.zIndex,
                    move: '.layui-layer-title', // 触发拖动的元素
                    moveOut: false, // 否允许拖拽到窗口外
                    success: function(layerDom, handle){
                        // 加载js
                        if (jsUrl != null && jsUrl.length > 0) {
                            require([WEBPATH + jsUrl], function(jsmodel) {
                                if (option.beforeJsStart != undefined) {
                                    option.beforeJsStart(jsmodel, layerDom, handle);
                                }
                                if(option.setData != undefined){
                                    option.setData(jsmodel, option.data);
                                }
                                jsmodel.start();
                                if (option.afterJsStart != undefined) {
                                    option.afterJsStart(jsmodel, layerDom, handle);
                                }
                            });
                        }
                        if(option.success != undefined){ option.success(layerDom, handle); }
                    }, // 弹出回调
                    cancel: option.cancel, // 关闭按钮回调
                    yes: typeof(option.yes) != 'undefined'? option.yes : undefined,
                    end: option.end, // 层销毁后触发的回调
                    full: option.full, // 窗口全屏后回调,
                    min: option.min, // 窗口最小化后回调,
                    restore: option.restore, // 窗口还原后回调
                    minStack: true, // 是否默认堆叠在左下角
                });
            }, function (rst){
                flyBear.alert("error", "操作提示","打开页面失败！");
            });

        },
        postJsonNoConfirm: function(url, param, option, succFunc, errorFunc) {
            let showWait = true;
            let flyBear = this;
            if (option != null && option.showWait!=null && !option.showWait) {showWait=false;}
            if(showWait){flyBear.openWait();}
            if (param == null) { param = {}; }
            // $j('#' + option.buttonid).closest("form");   $j('#' + option.formid)
            let asyn = function(){
                let dtd = $.Deferred(); // 异步回调函数有状态
                $.ajax({type: "POST", url: WEBPATH + url, // 读取静态html不加随机参数,进行缓存
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(param),
                    traditional: true, // 阻止深度序列化，数组和对象就不序列化了
                    dataType: "json",
                    success: function(message) {
                        // 存储流程常量
                        flyBear.closeWait();
                        dtd.resolve(message);
                    },
                    error: function(message) {
                        flyBear.closeWait();
                        dtd.reject(message);
                    }
                });
                return dtd.promise(); // 只允许外部注册函数，不允许调用函数
            }
            let deferred = asyn();
            if (succFunc != null) { deferred.done(succFunc); }
            if (errorFunc != null && typeof(errorFunc) != "undefined") { deferred.fail(errorFunc); }
            else{deferred.fail(function (message){
                let show_str;
                if(message.status === 405){
                    show_str = "请求方式不正确";
                }else if(message.status === 404){
                    show_str = "请求资源不存在";
                }else if(message.responseJSON && message.responseJSON.resultMsg){
                    show_str = message.responseJSON.resultMsg;
                }else {
                    show_str = "网络繁忙，请稍后重试";
                }
                flyBear.alert("error","提示", show_str)
            })}
        },
    }
})