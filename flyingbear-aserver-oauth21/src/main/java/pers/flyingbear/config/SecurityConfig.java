package pers.flyingbear.config;

import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.*;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.OAuth2AuthorizationServerConfiguration;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.authorization.OAuth2AuthorizationServerConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.OAuth2TokenFormat;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationConsentService;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationCodeRequestAuthenticationProvider;
import org.springframework.security.oauth2.server.authorization.client.InMemoryRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.JdbcRegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.config.ClientSettings;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;
import org.springframework.security.oauth2.server.authorization.config.TokenSettings;
import org.springframework.security.oauth2.server.authorization.web.authentication.DelegatingAuthenticationConverter;
import org.springframework.security.oauth2.server.authorization.web.authentication.OAuth2AuthorizationCodeAuthenticationConverter;
import org.springframework.security.oauth2.server.authorization.web.authentication.OAuth2ClientCredentialsAuthenticationConverter;
import org.springframework.security.oauth2.server.authorization.web.authentication.OAuth2RefreshTokenAuthenticationConverter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.RequestMatcher;
import pers.flyingbear.oauth2.OAuth2PasswordAuthenticationConverter;
import pers.flyingbear.util.MyKeys;

import java.time.Duration;
import java.util.Arrays;
import java.util.UUID;

/**
 * proxyBeanMethods = false，同一个Configuration中如果存在bean之间方法调用每调用一次就是一个新对象
 * 如果内部没有相互调用设置为false提高SpringBoot加载速度
 */
@Slf4j
@Configuration(proxyBeanMethods = false)
//@EnableWebSecurity
public class SecurityConfig {
    // 自定义用户点击授权页面
    private static final String CUSTOM_CONSENT_PAGE_URI = "/oauth2/consent";



    /**
     * 1. 用于身份验证的【SpringSecurity】过滤器链配置
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    @Order(2)
    public SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("Security配置: 初始化认证服务安全过滤链2");
        http.headers().frameOptions().sameOrigin() // 设置了<iframe>标签的“同源策略”，解决了上面出现的问题
                .and().csrf().ignoringAntMatchers("/h2/**")  // 使得h2 console页面不去做csrf检查
                .and().authorizeHttpRequests((authorize) -> authorize.mvcMatchers("/assets/**", "/webjars/**").permitAll().anyRequest().authenticated())
                // 表单登录处理重定向登录页面从授权服务器过滤器链
                .formLogin(Customizer.withDefaults());
        return http.build();
    }

    /**
     * 2. 配置UserDetails【SpringSecurity】，可以数据库，自定义，内存
     * @return
     */
    @Bean
    public UserDetailsService memoryUsers() {
        log.info("Security配置: 初始化内存用户user");
        // withDefaultPasswordEncoder过时使用PasswordEncoderFactories替代
        // PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        UserDetails userDetails = User.builder()
                .username("user")
                .password("{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(userDetails);
    }


    /**
     * 3. 密码加密器【SpringSecurity】，默认就是这种
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        log.info("Security配置: 初始化认证密码加密器");
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }







    /**
     * 4. oauth21端点的过滤器链，未认证到/login；授权服务器安全过滤器链
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SecurityFilterChain authorizationServerSecurityFilterChain(HttpSecurity http) throws Exception {
        log.info("oauth21: 初始化认证服务安全过滤链");
        // 匹配一组 AuthorizationServer相关的路由，使用默认
        OAuth2AuthorizationServerConfiguration.applyDefaultSecurity(http);
        // 未认证到登录页面
        http.exceptionHandling(exceptions ->
                        exceptions.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
        )
        // 资源服务配置
        //.oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
        ;
        return http.build();


        // 方案二，使用自定义
//        OAuth2AuthorizationServerConfigurer<HttpSecurity> authorizationServerConfigurer = new OAuth2AuthorizationServerConfigurer<>();
//        RequestMatcher endpointsMatcher = authorizationServerConfigurer.getEndpointsMatcher();
//
//        authorizationServerConfigurer.authorizationEndpoint(authorizationEndpoint -> {
//            authorizationEndpoint.consentPage(CUSTOM_CONSENT_PAGE_URI); // 自定义consent页面
//            OAuth2AuthorizationCodeRequestAuthenticationProvider oAuth2AuthorizationCodeRequestAuthenticationProvider =
//                    new OAuth2AuthorizationCodeRequestAuthenticationProvider(
//                            registeredClientRepository, oAuth2AuthorizationService, oAuth2AuthorizationConsentService);
//            oAuth2AuthorizationCodeRequestAuthenticationProvider.setAuthenticationValidatorResolver(
//                    ProjectOAuth2AuthenticationValidator.createDefaultAuthenticationValidator());
//            authorizationEndpoint.authenticationProvider(oAuth2AuthorizationCodeRequestAuthenticationProvider);
//        });
////        .tokenEndpoint(tokenEndpoint -> {
////            tokenEndpoint
////                .accessTokenRequestConverter(new DelegatingAuthenticationConverter(Arrays.asList(
////                        new OAuth2AuthorizationCodeAuthenticationConverter(),
////                        new OAuth2RefreshTokenAuthenticationConverter(),
////                        new OAuth2ClientCredentialsAuthenticationConverter(),
////                        new OAuth2PasswordAuthenticationConverter()
////                )));
////        });
//        http.requestMatcher(endpointsMatcher)
//                .authorizeRequests(authorizeRequests -> authorizeRequests.anyRequest().authenticated())
//                .csrf(csrf -> csrf.ignoringRequestMatchers(endpointsMatcher))
//                .apply(authorizationServerConfigurer);
//        return http.formLogin(Customizer.withDefaults()).build();
    }


    /**
     *  5. 客户端管理服务 oauth2-registered-client-schema.sql
     *  实现RegisteredClientRepository接口，可以实现自动数据库存储或者放入redis
     * @param jdbcTemplate
     * @return
     */
    @Bean
    public RegisteredClientRepository registeredClientRepository(JdbcTemplate jdbcTemplate) {
        log.info("oauth21: 初始化认证注册客户端");
        RegisteredClient registeredClient = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("userClient")
                .clientSecret("{noop}123456") // {noop}secret  passwordEncoder.encode("apple_secret")
                // 授权方法 client_secret_basic,client_secret_post【CLIENT_CREDENTIALS模式】,client_secret_jwt,private_key_jwt,none
                // AUTHORIZATION_CODE模式对应ClientAuthenticationMethod.CLIENT_SECRET_BASIC，Base64.encode(string(client_id:clientSecret))
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                // 授权类型，PASSWORD和IMPLICIT已经弃用 authorization_code,refresh_token,client_credentials,password,urn:ietf:params:oauth:grant-type:jwt-bearer
                // 授权码
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                // 刷新token
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                // 客户端模式
                .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
                .redirectUri("http://127.0.0.1:9006/login/oauth2/code/messaging-client-oidc")
                .redirectUri("http://127.0.0.1:9006/authorized")
                .redirectUri("http://www.baidu.com")
                // 客户端申请的作用域，也可以理解这个客户端申请访问用户的哪些信息，比如：获取用户信息，获取用户照片等
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PROFILE)
                .scope("message.read")
                .scope("message.write")
                // 配置客户端相关的配置项，包括验证密钥或者 是否需要授权页面
                .clientSettings(ClientSettings.builder().requireAuthorizationConsent(true).build())
//                .tokenSettings(TokenSettings.builder()
//                        // 使用透明方式，默认是OAuth2TokenFormat SELF_CONTAINED
//                        .accessTokenFormat(OAuth2TokenFormat.REFERENCE)
//                        // 授权码的有效期
//                        .accessTokenTimeToLive(Duration.ofHours(1))
//                        // 刷新token的有效期
//                        .refreshTokenTimeToLive(Duration.ofDays(15))
//                        .reuseRefreshTokens(true).build())
                .build();

        // 内存
        // return new InMemoryRegisteredClientRepository(registeredClient);

        // Save registered client in db as if in-memory
        JdbcRegisteredClientRepository registeredClientRepository = new JdbcRegisteredClientRepository(jdbcTemplate);
        registeredClientRepository.save(registeredClient);
        return registeredClientRepository;
    }


    /**
     * 6. 生成jwk资源，签署访问令牌的实例token加解密密钥
     * @return
     */
    @Bean
    public JWKSource<SecurityContext> jwkSource() {
        log.info("oauth21: 初始化认证jwkSource");
        // 生成密钥对,启动时生成的带有密钥的实例java.security.KeyPair构建RSAKey
        RSAKey rsaKey = MyKeys.generateRsa();
        JWKSet jwkSet = new JWKSet(rsaKey);
        return (jwkSelector, securityContext) -> jwkSelector.select(jwkSet);
    }

    @Bean
    public JwtDecoder jwtDecoder(JWKSource<SecurityContext> jwkSource) {
        log.info("oauth21: 初始化认证JwtDecoder");
        return OAuth2AuthorizationServerConfiguration.jwtDecoder(jwkSource);
    }

    /**
     * 7. ProviderSettings配置 Spring Authorization Server的实例，可以配置oauth端点
     * @return
     */
    @Bean
    public ProviderSettings providerSettings(@Value("${server.port}") Integer port) {
        log.info("oauth21: 初始化认证providerSettings");
        return ProviderSettings.builder().issuer(String.format("http://localhost:%s", port)).build();
    }

    //令牌管理服务的配置AuthorizationServerTokenServices的实现DefaultTokenServices

    /**
     * 8. 令牌管理服务【授权服务】 oauth2-authorization-schema.sql
     * @param jdbcTemplate
     * @param registeredClientRepository
     * @return
     */
    @Bean
    public OAuth2AuthorizationService authorizationService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
        log.info("oauth21: 初始化认证管理OAuth2授权信息服务");
        return new JdbcOAuth2AuthorizationService(jdbcTemplate, registeredClientRepository);
    }

    /**
     * 9. 【授权同意服务】客户端id、用户名与权限关联关系管理 oauth2-authorization-consent-schema.sql
     * @param jdbcTemplate
     * @param registeredClientRepository
     * @return
     */
    @Bean
    public OAuth2AuthorizationConsentService authorizationConsentService(JdbcTemplate jdbcTemplate, RegisteredClientRepository registeredClientRepository) {
        log.info("oauth21: 初始化认证 持久化服务接口");
        return new JdbcOAuth2AuthorizationConsentService(jdbcTemplate, registeredClientRepository);
    }
}
