package pers.flyingbear.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.jackson2.SecurityJackson2Modules;
import org.springframework.security.oauth2.server.authorization.jackson2.OAuth2AuthorizationServerJackson2Module;

import java.util.List;

/**
 * jackson序列化和反序列化配置
 */
@Configuration
public class Jackson2Config {
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		// 注册security序列化/反序列化
		//mapper.registerModule(new CoreJackson2Module());
		// spring-authorization-server Jackson Module
		//mapper.registerModule(new OAuth2AuthorizationServerJackson2Module());
		// 注册java8日期序列化/反序列化
		//mapper.registerModule(new JavaTimeModule());

		List<Module> modules = SecurityJackson2Modules.getModules(getClass().getClassLoader());
		mapper.registerModules(modules);
		return mapper;
	}

}
