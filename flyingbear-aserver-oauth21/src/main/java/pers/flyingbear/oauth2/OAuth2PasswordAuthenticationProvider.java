package pers.flyingbear.oauth2;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.*;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.SpringSecurityMessageSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.keygen.Base64StringKeyGenerator;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.security.oauth2.core.*;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.security.oauth2.server.authorization.OAuth2Authorization;
import org.springframework.security.oauth2.server.authorization.OAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AccessTokenAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2ClientAuthenticationToken;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.config.ProviderSettings;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;
import pers.flyingbear.util.JwtUtils;

import java.security.Principal;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.function.Supplier;

/**
 * 处理用户名密码授权，新版本密码模式已经被移除，这里做的就是自己扩展的密码模式
 */
public class OAuth2PasswordAuthenticationProvider extends DaoAuthenticationProvider {

	private static final StringKeyGenerator DEFAULT_REFRESH_TOKEN_GENERATOR = new Base64StringKeyGenerator(
			Base64.getUrlEncoder().withoutPadding(), 96);

	private final ProviderSettings providerSettings;

	private final JwtEncoder jwtEncoder;

	private final OAuth2AuthorizationService authorizationService;

	private final Supplier<String> refreshTokenGenerator = DEFAULT_REFRESH_TOKEN_GENERATOR::generateKey;

	private final OAuth2TokenCustomizer<JwtEncodingContext> jwtCustomizer = (context) -> {};

	private final MessageSourceAccessor messages = new MessageSourceAccessor(new SpringSecurityMessageSource(),
			Locale.CHINA);

	public OAuth2PasswordAuthenticationProvider(UserDetailsService userDetailsService,
			ProviderSettings providerSettings, JwtEncoder jwtEncoder, OAuth2AuthorizationService authorizationService) {
		super.setUserDetailsService(userDetailsService);
		this.providerSettings = providerSettings;
		this.jwtEncoder = jwtEncoder;
		this.authorizationService = authorizationService;
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// 用户名密码token
		OAuth2PasswordAuthenticationToken passwordAuthenticationToken = (OAuth2PasswordAuthenticationToken) authentication;
//
//		// 获取客户端
//		OAuth2ClientAuthenticationToken clientPrincipal = null;
//		if (OAuth2ClientAuthenticationToken.class.isAssignableFrom(authentication.getPrincipal().getClass())) {
//			clientPrincipal = (OAuth2ClientAuthenticationToken) authentication.getPrincipal();
//		}
//		if (clientPrincipal == null || !clientPrincipal.isAuthenticated()
//				|| clientPrincipal.getRegisteredClient() == null) {
//			throw new OAuth2AuthenticationException(OAuth2ErrorCodes.INVALID_CLIENT);
//		}
//		RegisteredClient registeredClient = clientPrincipal.getRegisteredClient();
//
//		// 校验该客户端是否有密码授权类型
//		if (!CollUtil.contains(registeredClient.getAuthorizationGrantTypes(), AuthorizationGrantType.PASSWORD)) {
//			throw new OAuth2AuthenticationException(OAuth2ErrorCodes.UNAUTHORIZED_CLIENT);
//		}
//
//		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//				passwordAuthenticationToken.getUsername(), passwordAuthenticationToken.getPassword());
//		// 调用AbstractUserDetailsAuthenticationProvider.authenticate()进行校验
//		Authentication usernamePasswordAuthentication;
//		try {
//			usernamePasswordAuthentication = super.authenticate(usernamePasswordAuthenticationToken);
//		}
//		catch (AuthenticationException e) {
//			throw convertLoginExceptionToOauth2Exception(authentication, e);
//		}
//
//		String issuer = this.providerSettings != null ? this.providerSettings.getIssuer() : null;
//		Set<String> authorizedScopes = registeredClient.getScopes();
//
//		JoseHeader.Builder headersBuilder = JwtUtils.headers();
//		JwtClaimsSet.Builder claimsBuilder = JwtUtils.accessTokenClaims(registeredClient, issuer,
//				clientPrincipal.getName(), authorizedScopes);
//
//		// @formatter:off
//		JwtEncodingContext context = JwtEncodingContext.with(headersBuilder, claimsBuilder)
//				.registeredClient(registeredClient)
//				.principal(usernamePasswordAuthentication)
//				.authorizedScopes(authorizedScopes)
//				.tokenType(OAuth2TokenType.ACCESS_TOKEN)
//				.authorizationGrantType(AuthorizationGrantType.PASSWORD)
//				.authorizationGrant(passwordAuthenticationToken)
//				.build();
//		// @formatter:on
//
//		this.jwtCustomizer.customize(context);
//
//		JoseHeader headers = context.getHeaders().build();
//		JwtClaimsSet claims = context.getClaims().build();
//		Jwt jwtAccessToken = this.jwtEncoder.encode(headers, claims);
//
//		OAuth2AccessToken accessToken = new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER,
//				jwtAccessToken.getTokenValue(), jwtAccessToken.getIssuedAt(), jwtAccessToken.getExpiresAt(),
//				authorizedScopes);
//
//		OAuth2RefreshToken refreshToken = null;
//		if (registeredClient.getAuthorizationGrantTypes().contains(AuthorizationGrantType.REFRESH_TOKEN) &&
//		// 不向公共客户端发送刷新令牌
//				!clientPrincipal.getClientAuthenticationMethod().equals(ClientAuthenticationMethod.NONE)) {
//			refreshToken = generateRefreshToken(registeredClient.getTokenSettings().getRefreshTokenTimeToLive());
//		}
//
//		// @formatter:off
//		OAuth2Authorization.Builder authorizationBuilder = OAuth2Authorization.withRegisteredClient(registeredClient)
//				.principalName(usernamePasswordAuthentication.getName())
//				.authorizationGrantType(AuthorizationGrantType.PASSWORD)
//				.token(accessToken,
//						(metadata) ->
//								metadata.put(OAuth2Authorization.Token.CLAIMS_METADATA_NAME, jwtAccessToken.getClaims()))
//				.attribute(OAuth2Authorization.AUTHORIZED_SCOPE_ATTRIBUTE_NAME, authorizedScopes)
//				.attribute(Principal.class.getName(), usernamePasswordAuthentication);
//		// @formatter:on
//		if (refreshToken != null) {
//			authorizationBuilder.refreshToken(refreshToken);
//		}
//
//		OAuth2Authorization authorization = authorizationBuilder.build();
//		this.authorizationService.save(authorization);
//
//		Map<String, Object> tokenAdditionalParameters = new HashMap<>();
//		claims.getClaims().forEach((key, value) -> {
//			if (!key.equals(OAuth2ParameterNames.SCOPE) && !key.equals(JwtClaimNames.IAT)
//					&& !key.equals(JwtClaimNames.EXP) && !key.equals(JwtClaimNames.NBF)) {
//				tokenAdditionalParameters.put(key, value);
//			}
//		});
//
//		return new OAuth2AccessTokenAuthenticationToken(registeredClient, clientPrincipal, accessToken, refreshToken,
//				tokenAdditionalParameters);
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return OAuth2PasswordAuthenticationToken.class.isAssignableFrom(authentication);
	}

	/**
	 * 登录异常转换为oauth2异常
	 * @param authentication 身份验证
	 * @param authenticationException 身份验证异常
	 * @return {@link OAuth2AuthenticationException}
	 */
	private OAuth2AuthenticationException convertLoginExceptionToOauth2Exception(Authentication authentication,
			AuthenticationException authenticationException) {
		if (authenticationException instanceof UsernameNotFoundException) {
			return new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodesExpand.USERNAME_NOT_FOUND,
					this.messages.getMessage("JdbcDaoImpl.notFound", new Object[] { authentication.getName() },
							"Username {0} not found"),
					""));
		}
		if (authenticationException instanceof BadCredentialsException) {
			return new OAuth2AuthenticationException(
					new OAuth2Error(OAuth2ErrorCodesExpand.BAD_CREDENTIALS, this.messages.getMessage(
							"AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"), ""));
		}
		if (authenticationException instanceof LockedException) {
			return new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodesExpand.USER_LOCKED, this.messages
					.getMessage("AbstractUserDetailsAuthenticationProvider.locked", "User account is locked"), ""));
		}
		if (authenticationException instanceof DisabledException) {
			return new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodesExpand.USER_DISABLE,
					this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.disabled", "User is disabled"),
					""));
		}
		if (authenticationException instanceof AccountExpiredException) {
			return new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodesExpand.USER_EXPIRED, this.messages
					.getMessage("AbstractUserDetailsAuthenticationProvider.expired", "User account has expired"), ""));
		}
		if (authenticationException instanceof CredentialsExpiredException) {
			return new OAuth2AuthenticationException(new OAuth2Error(OAuth2ErrorCodesExpand.CREDENTIALS_EXPIRED,
					this.messages.getMessage("AbstractUserDetailsAuthenticationProvider.credentialsExpired",
							"User credentials have expired"),
					""));
		}
		return new OAuth2AuthenticationException(OAuth2ErrorCodesExpand.UN_KNOW_LOGIN_ERROR);
	}

	/**
	 * 生成刷新令牌
	 * @param tokenTimeToLive 令牌时间生活
	 * @return {@link OAuth2RefreshToken}
	 */
	private OAuth2RefreshToken generateRefreshToken(Duration tokenTimeToLive) {
		Instant issuedAt = Instant.now();
		Instant expiresAt = issuedAt.plus(tokenTimeToLive);
		return new OAuth2RefreshToken(this.refreshTokenGenerator.get(), issuedAt, expiresAt);
	}

}
