package pers.flyingbear.oauth2;

/**
 * oauth2错误码扩展
 */
public interface OAuth2ErrorCodesExpand {

	/** 用户名未找到 */
	String USERNAME_NOT_FOUND = "username_not_found";

	/** 错误凭证 */
	String BAD_CREDENTIALS = "bad_credentials";

	/** 用户被锁 */
	String USER_LOCKED = "user_locked";

	/** 用户禁用 */
	String USER_DISABLE = "user_disable";

	/** 用户过期 */
	String USER_EXPIRED = "user_expired";

	/** 证书过期 */
	String CREDENTIALS_EXPIRED = "credentials_expired";

	/** 未知的登录异常 */
	String UN_KNOW_LOGIN_ERROR = "un_know_login_error";

}
