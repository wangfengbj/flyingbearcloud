package pers.flyingbear.oauth2;

import lombok.Getter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.SpringSecurityCoreVersion;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.server.authorization.authentication.OAuth2AuthorizationGrantAuthenticationToken;

import java.util.Map;

/**
 * 用户名密码授权token
 */
@Getter
public class OAuth2PasswordAuthenticationToken extends OAuth2AuthorizationGrantAuthenticationToken {
	private static final long serialVersionUID = SpringSecurityCoreVersion.SERIAL_VERSION_UID;

	/** 用户名 */
	private final String username;

	/** 密码 */
	private final String password;

	public OAuth2PasswordAuthenticationToken(String username, String password, Authentication clientPrincipal,
			Map<String, Object> additionalParameters) {
		super(AuthorizationGrantType.PASSWORD, clientPrincipal, additionalParameters);
		this.username = username;
		this.password = password;
	}

	@Override
	public Object getCredentials() {
		return this.password;
	}

	@Override
	public String getName() {
		return this.getUsername();
	}

}
