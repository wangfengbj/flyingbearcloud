package pers.flyingbear.util;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.experimental.UtilityClass;
import org.springframework.lang.Nullable;
import org.springframework.security.oauth2.core.endpoint.OAuth2ParameterNames;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.security.oauth2.jwt.JwtClaimsSet;

import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.time.Instant;
import java.util.Collections;
import java.util.Set;

@UtilityClass
public class JwtUtils {
    /**
     * 令牌解析
     * @param token 令牌
     * @return {@link JWTClaimsSet}
     */
    @Nullable
    public JWTClaimsSet parse(String token) {
        try {
            SignedJWT signedJWT = SignedJWT.parse(token);
            return signedJWT.getJWTClaimsSet();
        }
        catch (ParseException e) {
            return null;
        }
    }
//import org.springframework.security.oauth2.jwt.JoseHeader;
//    public JoseHeader.Builder headers() {
//        return JoseHeader.withAlgorithm(SignatureAlgorithm.RS256);
//    }

    /**
     * 访问令牌的claims
     * @param registeredClient 注册客户端
     * @param issuer 发行人
     * @param subject 主题
     * @param authorizedScopes 授权范围
     * @return {@link JwtClaimsSet.Builder}
     */
    public JwtClaimsSet.Builder accessTokenClaims(RegisteredClient registeredClient, String issuer, String subject,
                                                  Set<String> authorizedScopes) {
        Instant issuedAt = Instant.now();
        Instant expiresAt = issuedAt.plus(registeredClient.getTokenSettings().getAccessTokenTimeToLive());
        // @formatter:off
        JwtClaimsSet.Builder claimsBuilder = JwtClaimsSet.builder();
        if (StringUtils.hasText(issuer)) {
            // 认证信息者的唯一标识(授权服务的地址)
            claimsBuilder.issuer(issuer);
        }
        claimsBuilder
                // 用户
                .subject(subject)
                // clientId
                .audience(Collections.singletonList(registeredClient.getClientId()))
                // 发布时间
                .issuedAt(issuedAt)
                // 过期时间
                .expiresAt(expiresAt)
                .notBefore(issuedAt);
        if (!CollectionUtils.isEmpty(authorizedScopes)) {
            // scope
            claimsBuilder.claim(OAuth2ParameterNames.SCOPE, authorizedScopes);
        }
        // @formatter:on
        return claimsBuilder;
    }
}
