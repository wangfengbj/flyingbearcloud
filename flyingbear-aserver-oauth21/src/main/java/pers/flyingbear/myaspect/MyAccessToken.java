package pers.flyingbear.myaspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.util.Map;

/**
 * 切入旧版本oauth2，获取token
 */
// @Aspect
public class MyAccessToken {
    // @AfterReturning(value = "execution(* com.类.synchronism*(..))", returning="result")
//    @Around("execution(* org.springframework.security.oauth2.provider.endpoint.TokenEndpoint.postAccessToken(..))")
//    public Object handleControllerMethod(ProceedingJoinPoint joinPoint) throws Throwable {
//            Object proceed = joinPoint.proceed();
//            ResponseEntity<OAuth2AccessToken> responseEntity = (ResponseEntity<OAuth2AccessToken>) proceed;
//            OAuth2AccessToken body = responseEntity.getBody();
//            Map<String, Object> result = ResultHelper.getSuccessMap();
//            result.put("expires_in", "7200");
//            result.put("access_token", body.getValue());
//            return ResponseEntity.status(HttpStatus.OK).body(result);
//    }
}
