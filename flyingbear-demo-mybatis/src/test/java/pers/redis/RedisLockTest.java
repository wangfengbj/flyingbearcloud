package pers.redis;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.integration.redis.util.RedisLockRegistry;
import pers.flyingbear.UserServer;
import pers.flyingbear.sp.configRedis.JedisLockUtil;
import pers.flyingbear.sp.configRedis.RedisUtil;
import pers.flyingbear.sp.configRedis.RedissonLockUtil;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

@SpringBootTest(classes = UserServer.class)
//@ExtendWith(SpringExtension.class)
public class RedisLockTest {

//    @LocalServerPort
//    private int port;

    @Autowired
    private RedisUtil redisUtil;

    // SpringIntegration分布式锁
    @Resource
    private RedisLockRegistry redisLockRegistry;

    @Resource
    JedisLockUtil jedisLockUtil;

    @Resource
    RedissonLockUtil redissonLockUtil;

    @Test
    void test() {
        redisUtil.set("wf", "111111", 30);
    }

    /**
     * SpringIntegration分布式锁
     */
    @Test
    void testFBS() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Lock lock = redisLockRegistry.obtain("lock");
                    boolean b1 = lock.tryLock(10, TimeUnit.SECONDS);
                    System.out.println(Thread.currentThread().getName()+" b1 is : "+b1);

                    TimeUnit.SECONDS.sleep(10);

                    boolean b2 = lock.tryLock(10, TimeUnit.SECONDS);
                    System.out.println(Thread.currentThread().getName()+" b2 is : "+b2);

                    TimeUnit.SECONDS.sleep(10);
                    lock.unlock();
                    //lock.unlock();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    System.out.println("报错啦");
                }
            }
        };
        new Thread(r).start();
        new Thread(r).start(); //两个线程有一个拿到不锁会You do not own lock at spring-cloud:lock

        try {
            TimeUnit.SECONDS.sleep(70);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Test
    void testJedisLock() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String key = "userid:55689";

                String lockId = null;
                try{
                    //占用锁
                    lockId = jedisLockUtil.tryLock(key);
                    if(lockId != null){
                        System.out.println(Thread.currentThread().getName()+"获取锁");
                        //程序执行
                        TimeUnit.SECONDS.sleep(10);
                    }else{
                        System.out.println(Thread.currentThread().getName()+"未获取锁");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    //释放锁
                    jedisLockUtil.unlock(key,lockId);
                }
            }
        };
        new Thread(r).start();
        new Thread(r).start(); //两个线程有一个拿到不锁会You do not own lock at spring-cloud:lock

        try {
            TimeUnit.SECONDS.sleep(60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testRedissonLock() {
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String lockKey = "lock-wf";
                try {
                    boolean r= redissonLockUtil.tryLock(lockKey, 1, 20);
                    System.out.println(Thread.currentThread().getName()+"--------"+r);
                    TimeUnit.SECONDS.sleep(10);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    // 释放锁
                    redissonLockUtil.unlock(lockKey);
                }
            }
        };
        new Thread(r).start();
        new Thread(r).start(); //两个线程有一个拿到不锁会You do not own lock at spring-cloud:lock

        try {
            TimeUnit.SECONDS.sleep(60);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//		RLock fairLock = redisson.getFairLock("anyLock");//公平锁
//		fairLock.lock();

//		RReadWriteLock rwlock = redisson.getReadWriteLock("anyRWLock");//读写锁
//		// 最常见的加锁使用方法
//		rwlock.readLock().lock();
//		// 或
//		rwlock.writeLock().lock();



//		RSemaphore semaphore = redisson.getSemaphore("semaphore");//信号量
//        //需要获取几个信号量参数就写几，默认为1
//        boolean b = semaphore.tryAcquire();


//		RCountDownLatch latch = redisson.getCountDownLatch("anyCountDownLatch");   //闭锁
//		latch.trySetCount(3);//redis中存在一个anyCountDownLatch=3，当其等于0时就闭锁
//		latch.await();
    }
}
