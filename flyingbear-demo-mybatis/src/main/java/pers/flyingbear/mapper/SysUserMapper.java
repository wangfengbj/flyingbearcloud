package pers.flyingbear.mapper;

import java.util.List;

import pers.flyingbear.model.SysUser;
import org.apache.ibatis.annotations.Param;

public interface SysUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    List<SysUser> selectAll();

    int updateByPrimaryKey(SysUser record);
    
    // 传map进去
    SysUser getUser(@Param("loginId") String loginId, @Param("loginPassword") String loginPassword);
}