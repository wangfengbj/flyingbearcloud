package pers.flyingbear.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

/**
 * 拦截器,springmvc时候才会使用,使用api网关不需要
 */
@Slf4j
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("拦截器---preHandle执行");
//        HttpSession session = request.getSession(true);
//		Object username=session.getAttribute(Const.SESSION_USER_NAME);
//		if(null != username) {
//			// 已登录
//			response.sendRedirect(request.getContextPath()+"/index.html");
//			return true;
//		}else {
//			// 未登录，直接重定向到登录页面
//			response.sendRedirect(request.getContextPath()+"/login.html");
//			return false;
//		}



        // 以下JWT，如果不是映射到方法直接通过
//        if(!(handler instanceof HandlerMethod)){
//            return true;
//        }
//        HandlerMethod handlerMethod=(HandlerMethod)handler;
//        Method method=handlerMethod.getMethod();
//        // 检查是否有passtoken注释，有则跳过认证
//        if (method.isAnnotationPresent(PassToken.class)) {
//            PassToken passToken = method.getAnnotation(PassToken.class);
//            if (passToken.required()) {
//                return true;
//            }
//        }
//
//        // 检查有没有需要用户权限的注解。如果需要加入下面token验证就可以，这里处理不需要的，其他都要认证
//        if (method.isAnnotationPresent(LoginToken.class)) {
//            LoginToken loginToken = method.getAnnotation(LoginToken.class);
//            if (loginToken.required()) {
//                return true;
//            }
//        }
//
//
//        //获取 jwt值的请求头属性名为User-Token
//        String token = request.getHeader("Authorization");
//        logger.info("[登录校验拦截器]-从header中获取的token为:{}", token);
//        if (StringUtils.isBlank(token)) {
//            sendJsonResponse(response, 401, "你无权访问");
//            return false;
//        }
//        boolean is_yx = JWTUtil.verify(token, JWTUtil.SECRET);
//        if(is_yx) {
//            // 需要刷新token,可以使用redis存新token,页面只返回key。
//            // 也可以每次都返回新token，旧key有宽限时间，防止请求带旧key
//            //CurrentUser.put(userBO);
//            return true;
//        }else {
//            sendJsonResponse(response, 403, "无效令牌");
//            return false;
//        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("拦截器---postHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("拦截器---afterCompletion");
    }


    public static void sendJsonResponse(HttpServletResponse resp, int code, String message) {
        sendJsonResponse(resp, String.format(jsonTemplate(), code, message));
    }

    public static String jsonTemplate() {
        return "{\"resultCode\":%s,\"resultMsg\":\"%s\",\"data\":null,\"errors\":null}";
    }

    public static void sendJsonResponse(HttpServletResponse response, String json) {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(json);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
