package pers.flyingbear.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 日志注入，切面编程
 */
@Aspect
@Component
@Slf4j // private final Logger logger = LoggerFactory.getLogger(this.getClass());
public class LogAspectHandler {
    /**
     *  定义一个切面
     */
    //@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)")
    @Pointcut("execution(* pers.flyingbear.controller..*.*(..))")
    public void pointCut() {}

    @Before("pointCut()")
    public void doBefore(JoinPoint joinPoint) {
        log.info("------------------切面编程before---------------");
        // 获取签名
        Signature signature = joinPoint.getSignature();
        // 获取切入的包名
        String declaringTypeName = signature.getDeclaringTypeName();
        // 获取即将执行的方法名
        String funcName = signature.getName();
        log.info("即将执行方法为: {}，属于{}包", funcName, declaringTypeName);
    }
}
