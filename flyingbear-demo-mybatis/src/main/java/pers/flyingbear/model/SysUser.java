package pers.flyingbear.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@ApiModel(value = "用户实体类")
@Data
public class SysUser implements Serializable {
    @ApiModelProperty(value = "用户唯一标识")
    private Long id;

    private String uname;

    // 转换JSON时候不转换
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String salt;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") // 出参格式化
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") // 入参格式化,把前台传的字符串转日期
    private Date created;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updated;

    private Byte state;

    private String loginId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String loginPassword;
}