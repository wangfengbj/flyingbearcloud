package pers.flyingbear;

//import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import pers.flyingbear.tool.util.MyRandomPort;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker   // hystrix熔断支持
@MapperScan("com.flyingbear.mapper")  // 使用MapperScan批量扫描所有的Mapper接口；也可以在类上@Mapper
public class UserServer {
    public static void main(String[] args) {
        // 热部署会导致main执行两次
        new MyRandomPort(args);
        SpringApplication.run(UserServer.class, args);
    }
    /**
     * /actuator/hystrix.stream 如果不可访问解决
     * @return
     */
//    @Bean
//    public ServletRegistrationBean getDashboardServlet(){
//        HystrixMetricsStreamServlet servlet = new HystrixMetricsStreamServlet();
//        ServletRegistrationBean bean = new ServletRegistrationBean(servlet);
//        bean.setLoadOnStartup(1);
//        bean.addUrlMappings("/hystrix.stream");
//        bean.setName("hystrix");
//        return bean;
//    }

}
