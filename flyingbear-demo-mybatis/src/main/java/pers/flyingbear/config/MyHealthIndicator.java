package pers.flyingbear.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;

/**
 * Actuator检查服务健康状况，自定义健康指示器
 * 可以在这里检查数据库连接、redis等是否可用然后返回给注册中心
 */
@Component
public class MyHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        //返回健康信息
        //return Health.down().withDetail("message", "redis连接异常").build();
        return new Health.Builder(Status.UP).build();
    }
}
