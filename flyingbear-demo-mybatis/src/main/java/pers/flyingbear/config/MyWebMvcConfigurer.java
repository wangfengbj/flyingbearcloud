package pers.flyingbear.config;

import pers.flyingbear.interceptor.LoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * MVC配置（包括：拦截器，格式化器, 视图控制器、消息转换器 等等）
 */
@Slf4j
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

    /**
     * 通过@Bean注解，将我们定义的拦截器注册到Spring容器
     * @return
     */
    @Bean
    public LoginInterceptor loginInterceptor(){
        return new LoginInterceptor();
    }

    /**
     * 重写接口中的addInterceptors方法，添加自定义拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 通过registry来注册拦截器，通过addPathPatterns来添加拦截路径
        registry.addInterceptor(this.loginInterceptor()).addPathPatterns("/**");

//        registry.addInterceptor(this.loginInterceptor())
//                .addPathPatterns("/**") // 拦截所有
//                //排除样式、脚本、图片等资源文件
//                .excludePathPatterns("/css/**","/js/**","/img/**","/fonts/**","/page/**")
//                //排除登录页面
//                .excludePathPatterns("/login.html,/login.js")
//                //排除验证码
//                //.excludePathPatterns("/wechatplatformuser/defaultKaptcha")
//                //排除用户点击登录按钮
//                .excludePathPatterns("/admin/login");
    }

    /**
     * 添加跨域设置
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //定义哪些URL接受跨域
        registry.addMapping("/**")
                //定义哪些origin可以跨域请求
                .allowedOrigins("*")
                //定义接受的跨域请求方法
                .allowedMethods("POST", "GET", "PUT", "PATCH", "OPTIONS", "DELETE")
                .exposedHeaders("Set-Token")
                .allowCredentials(true)
                .allowedHeaders("*")
                .maxAge(3600);
    }
}
