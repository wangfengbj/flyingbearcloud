package pers.flyingbear.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * @author wind
 * 通过 @Configuration 注解，让 Spring 加载该配置类。再通过 @EnableSwagger2 注解来启用Swagger2
 * http://ip:port/swagger-ui.html
 */
@EnableSwagger2
@Configuration
public class SwaggerConfig {
    private static final String BASE_PACKAGE = "com.flyingbear";
    /**
     * apiInfo() 用来创建该 Api 的基本信息（这些基本信息会展现在文档页面中）
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("系统模块api文档")
                .description("系统模块项目web服务接口")
                //.termsOfServiceUrl("http://www.fyw.com")
                .version("1.1.0")
                .build();
    }
 
    /**
     * createRestApi 函数创建 Docket 的Bean
     * @param
     * @return
     */
    @Bean
    public Docket createRestApi() {
        List<ResponseMessage> responseMessageList = new ArrayList<>();
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)  // Swagger 不使用默认的 HTTP 响应消息
                .apiInfo(apiInfo())  // 创建API的基本信息
                .produces(Collections.singleton("application/json"))
                .consumes(Collections.singleton("application/json"))
                .protocols(Collections.singleton("http"))
                .select()  // select()函数返回一个ApiSelectorBuilder实例用来控制哪些接口暴露给Swagger来展现
                .apis(RequestHandlerSelectors.basePackage(BASE_PACKAGE))  // 指定扫描的包路径
                .paths(PathSelectors.any())  // Swagger会扫描该包下的所有Controller定义的API，并产生文档内容（除了被@ApiIgnore定义的请求）
                .build()
                .globalOperationParameters(operationParameters()) // 添加默认的参数
                .globalResponseMessage(RequestMethod.GET, responseMessageList) // 通过Docket的globalResponseMessage()
                // 方法全局覆盖HTTP方法的响应消息
                .globalResponseMessage(RequestMethod.POST, responseMessageList);
    }
 
    /**
     * 创建固定的请求头
     * @param
     * @return
     */
    private List<Parameter> operationParameters() {
        // 统一在请求头中增加token
        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<>();
        pars.add(tokenPar
                .name("Token")
                .description("Token")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build());
        pars.add(tokenPar
                .name("userId")
                .description("userId")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build());
        pars.add(tokenPar.build());
        return pars;
    }
}
