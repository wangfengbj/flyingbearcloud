package pers.flyingbear.config;

import com.netflix.appinfo.HealthCheckHandler;
import com.netflix.appinfo.InstanceInfo.InstanceStatus;
import org.springframework.boot.actuate.health.Status;
import org.springframework.stereotype.Component;


import javax.annotation.Resource;

/**
 * Actuator健康检查处理器
 */
@Component
public class MyHealthCheckHandler implements HealthCheckHandler {
    @Resource
    MyHealthIndicator myHealthIndicator;

    @Override
    public InstanceStatus getStatus(InstanceStatus instanceStatus) {
        Status status = myHealthIndicator.health().getStatus();
        if(status.equals(Status.UP)){
            return InstanceStatus.UP;
        }else {
            return InstanceStatus.DOWN;
        }
    }
}
