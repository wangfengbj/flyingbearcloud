package pers.flyingbear.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Order(2)
public class InitResource2 implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("实现ApplicationRunner可以让此方法在所有的SpringBeans都初始化之后,SpringApplication.run()之前执行，可以定义多个");
    }
}
