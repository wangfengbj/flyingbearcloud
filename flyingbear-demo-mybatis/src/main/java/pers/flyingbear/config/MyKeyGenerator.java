package pers.flyingbear.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 自定义缓存key格式，使用缓存时候需要指定MyKeyGenerator，否则还是使用默认的
 */
@Component // 组件
@Getter
@Setter
public class MyKeyGenerator implements KeyGenerator {
    // 项目名称，区分不同项目
    @Value("${spring.application.name}")
    private String keyPrefix;

    @Override
    public Object generate(Object o, Method method, Object... objects) {
        char sp = ':';
        StringBuilder stringBuilder = new StringBuilder();
        //
        stringBuilder.append(keyPrefix);
        stringBuilder.append(sp);
        // 类名
        stringBuilder.append(o.getClass().getSimpleName());
        stringBuilder.append(sp);
        // 方法名
        stringBuilder.append(method.getName());
        // 参数
        stringBuilder.append("[");
        for (Object obj : objects) {
            stringBuilder.append(sp).append(obj.toString());
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
