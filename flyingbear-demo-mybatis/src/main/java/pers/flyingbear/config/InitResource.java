package pers.flyingbear.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Order(1)
// @ConstructorBinding // 构造函数绑定
// @ConfigurationProperties("acme")
// @EnableConfigurationProperties(AcmeProperties.class)
// @ConfigurationPropertiesScan({ "com.example.app", "org.acme.another" })
public class InitResource implements CommandLineRunner {
//    @Value("${name}")
//    private String name;

    @Override
    public void run(String... args) throws Exception {
        log.info("实现CommandLineRunner可以让此方法在所有的SpringBeans都初始化之后,SpringApplication.run()之前执行，可以定义多个");
    }
}
