package pers.flyingbear.config;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.support.WebBindingInitializer;

/**
 * 去除表单提交时字符串的前后空格
 * @author wind
 */
@Component
@ControllerAdvice
public class TrimEditorConfig implements WebBindingInitializer {
    @InitBinder
    @Override
    public void initBinder(WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(String.class,new StringTrimmerEditor(false));
    }
}