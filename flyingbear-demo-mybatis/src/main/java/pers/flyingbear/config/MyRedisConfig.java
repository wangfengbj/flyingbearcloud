package pers.flyingbear.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.integration.redis.util.RedisLockRegistry;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * SpringCache使用redis做缓存时候配置 Ctrl+H
 * ProxyCachingConfiguration extends AbstractCachingConfiguration会加载该类
 * CacheManager RedisCacheManager
 * RedisCacheConfiguration 配置序列化和反序列化等
 * @Cacheable 触发填写缓存，例如：将方法返回值写入缓存系统
 * @CacheEvict 触发移除缓存
 * @CachePut 在不妨碍方法执行的前提下更新缓存
 * @Caching 在一个方法上将多个缓存操作组合执行
 * @CacheConfig 在类级别上添加共享的缓存配置
 */
@Configuration
@EnableCaching // 启用缓存，这个注解很重要；
@Slf4j
public class MyRedisConfig extends CachingConfigurerSupport {

    @Value("${spring.cache.redis.time-to-live}")
    private Duration timeToLive = Duration.ZERO;

    @Value("${spring.redis.host}")
    private String ip;

    @Value("${spring.redis.port}")
    private Integer port;
    
    /**
     * redisTemplate序列化和反序列化设置
     * @param redisConnectionFactory
     * @return
     */
    @Bean(name="redisTemplate")
    public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        // 配置连接工厂
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        // key的序列化
        RedisSerializer<?> redisSerializer = new StringRedisSerializer();
        redisTemplate.setKeySerializer(redisSerializer);
        redisTemplate.setHashKeySerializer(redisSerializer);
        // 值的序列化GenericJackson2JsonRedisSerializer可以序列化带泛型的，会保存序列化的对象的包名和类名
        //RedisSerializer<Object> valueSerializer = new GenericJackson2JsonRedisSerializer();
        //redisTemplate.setValueSerializer(valueSerializer);
        //redisTemplate.setHashValueSerializer(valueSerializer);

        // 值的序列化和反序列化，默认不支持带泛型的，可以先转化为JSON存储
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
        ObjectMapper objectMapper = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 数据类型存redis
        objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.WRAPPER_ARRAY);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);
        // 设置值序列化器
        redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);

        // 后续方法
        redisTemplate.afterPropertiesSet();
        log.info("-----------RedisTemplate加载完成-----------");
        return redisTemplate;
    }

    /**
     * SpringIntegration分布式锁初始化
     * @param redisConnectionFactory
     * @return
     */
    @Bean
    public RedisLockRegistry redisLockRegistry(RedisConnectionFactory redisConnectionFactory) {
        return new RedisLockRegistry(redisConnectionFactory, "sys-user-key1", 180000L);
    }

    /**
     * jedis客户端
     * @return
     */
    @Bean
    public JedisPool jedisPoolFactory() {
        log.info("JedisPool注入开始...");
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(10);
        jedisPoolConfig.setMaxWaitMillis(1500);
        // 连接耗尽时是否阻塞, false报异常,true阻塞直到超时, 默认true
        jedisPoolConfig.setBlockWhenExhausted(false);
        // 是否启用pool的jmx管理功能, 默认true
        jedisPoolConfig.setJmxEnabled(true);
        JedisPool jedisPool = new JedisPool(jedisPoolConfig, ip, port, 3000);
        log.info("JedisPool注入成功...");
        return jedisPool;
    }

    /**
     * RedissonClient客户端
     * @return
     */
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        // 本例子使用的是yaml格式的配置文件，读取使用Config.fromYAML，如果是Json文件，则使用Config.fromJSON
        // Config config = Config.fromYAML(RedissonConfig.class.getClassLoader().getResource("redisson-config.yml"));

        // 单节点
        config.useSingleServer().setAddress("redis://" + ip + ":" + port);
        config.useSingleServer().setPassword(null);
        // 主从配置

        // 集群模式配置

        // 哨兵模式
        return Redisson.create(config);
    }

    /**
     * 哨兵模式自动装配
     * redisson.master-name=mymaster
     * redisson.password=xxxx
     * redisson.sentinel-addresses=10.47.91.83:26379,10.47.91.83:26380,10.47.91.83:26381
     * @return
     */
//	@Bean
//	@ConditionalOnProperty(name = "redisson.master-name")
//	RedissonClient redissonSentinel() {
//		Config config = new Config();
//		SentinelServersConfig serverConfig = config.useSentinelServers()
//				.addSentinelAddress(redssionProperties.getSentinelAddresses())
//				.setMasterName(redssionProperties.getMasterName()).setTimeout(redssionProperties.getTimeout())
//				.setMasterConnectionPoolSize(redssionProperties.getMasterConnectionPoolSize())
//				.setSlaveConnectionPoolSize(redssionProperties.getSlaveConnectionPoolSize());
//
//		if (StringUtils.isNotBlank(redssionProperties.getPassword())) {
//			serverConfig.setPassword(redssionProperties.getPassword());
//		}
//		return Redisson.create(config);
//	}

    /**
     * 单机模式自动装配
     * redisson.address=redis://10.18.75.115:6379
     * redisson.password=
     * @return
     */
//	@Bean
//	@ConditionalOnProperty(name = "redisson.address")
//	RedissonClient redissonSingle() {
//		Config config = new Config();
//		SingleServerConfig serverConfig = config.useSingleServer().setAddress(redssionProperties.getAddress())
//				.setTimeout(redssionProperties.getTimeout())
//				.setConnectionPoolSize(redssionProperties.getConnectionPoolSize())
//				.setConnectionMinimumIdleSize(redssionProperties.getConnectionMinimumIdleSize());
//
//		if (StringUtils.isNotBlank(redssionProperties.getPassword())) {
//			serverConfig.setPassword(redssionProperties.getPassword());
//		}
//
//		return Redisson.create(config);
//	}





    /**
     * Spring和Redisson实现cache功能
     * @param redissonClient
     * @return
     */
//    @Bean
//    public CacheManager cacheManager(RedissonClient redissonClient) {
//        Map<String, CacheConfig> config = new HashMap<>();
//        // 创建一个名称为"testMap"的缓存，过期时间ttl为24分钟，同时最长空闲时maxIdleTime为12分钟。
//        config.put("testMap", new CacheConfig(24 * 60 * 1000, 12 * 60 * 1000));
//        return new RedissonSpringCacheManager(redissonClient, config);
//    }

    /**
     * SpringCache缓存设置，默认的二进制存储，这里JSON存储更直观
     * @param factory
     * @return
     */
    @Bean
    public CacheManager cacheManager(RedisConnectionFactory factory) {
        // 1. autoconfigure中RedisCacheConfiguration创建RedisCacheManager
//        return RedisCacheManager.builder(factory).cacheDefaults(this.getRedisCacheConfigurationWithTtl(timeToLive)).build();
        // 2. 特定key设置策略
        return new RedisCacheManager(
                RedisCacheWriter.nonLockingRedisCacheWriter(factory),
                this.getRedisCacheConfigurationWithTtl(Duration.ofSeconds(600)), // 默认策略，未配置的 key 会使用这个
                this.getRedisCacheConfigurationMap() // 指定 key 策略
        );
    }

    /**
     * 指定key设置特殊时间
     * @return
     */
    private Map<String, RedisCacheConfiguration> getRedisCacheConfigurationMap() {
        Map<String, RedisCacheConfiguration> redisCacheConfigurationMap = new HashMap<>();
        redisCacheConfigurationMap.put("sysUser", this.getRedisCacheConfigurationWithTtl(Duration.ofSeconds(120)));
        redisCacheConfigurationMap.put("xzOrg", this.getRedisCacheConfigurationWithTtl(Duration.ofSeconds(120)));
        return redisCacheConfigurationMap;
    }

    /**
     * 获取RedisCacheConfiguration
     * @param duration
     * @return
     */
    private RedisCacheConfiguration getRedisCacheConfigurationWithTtl(Duration duration) {
        // 值的序列化反序列化
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        // 解决查询缓存转换异常的问题
        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 数据类型存redis
        om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        jackson2JsonRedisSerializer.setObjectMapper(om);

        // key的序列化和反序列化
        RedisSerializer<String> redisSerializer = new StringRedisSerializer();

        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .entryTtl(duration)
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(redisSerializer))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(jackson2JsonRedisSerializer))
                .disableCachingNullValues();
        return redisCacheConfiguration;
    }
}
