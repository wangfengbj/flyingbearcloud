package pers.flyingbear.service;

import pers.flyingbear.model.SysUser;

import java.util.List;


public interface SysUserService {
	int deleteByPrimaryKey(Long id);

    int insert(SysUser record);

    SysUser selectByPrimaryKey(Long id);

    List<SysUser> selectAll();

    int updateByPrimaryKey(SysUser record);
    
	SysUser login(String loginId, String loginPwd);

    int updateUserInfo(Long id);
}
