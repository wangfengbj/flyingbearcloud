package pers.flyingbear.service.impl;

import java.util.List;

import pers.flyingbear.mapper.SysUserMapper;
import pers.flyingbear.model.SysUser;
import pers.flyingbear.service.SysUserService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service
public class SysUserServiceImpl implements SysUserService {
	@Resource
	SysUserMapper sysUserMapper;
	
	@Override
	public int deleteByPrimaryKey(Long id) {
		return 0;
	}

	@Override
	public int insert(SysUser record) {
		return 0;
	}

	@Override
	@Cacheable(value = {"sysUser"}, key = "#id", unless="#result == null")
	public SysUser selectByPrimaryKey(Long id) {
		return sysUserMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SysUser> selectAll() {
		return null;
	}

	@Override
	public int updateByPrimaryKey(SysUser record) {
		return 0;
	}

	@Override
	public SysUser login(String loginId, String loginPwd) {
		return sysUserMapper.getUser(loginId, loginPwd);
	}

	@Override
	@CacheEvict(value = {"sysUser"}, key = "#id")
	public int updateUserInfo(Long id) {
		return 0;
	}

}
