package pers.flyingbear.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.tool.model.ApiResponse;

@RestController
@Api(value = "HelloWorld控制器")
@Validated // 开启验证，@NotNull(message = "name不能为空")、@Max(value = 99, message = "不能大于99岁")
public class HelloController {
	private static final Logger Logger = LoggerFactory.getLogger(HelloController.class);
	
	@GetMapping("/hello")
	@ApiOperation("hello")
    public String hello(){
		Logger.info("hello方法");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			Logger.error("线程中断",e);
		}
        return "hello方法";
    }
	
	@GetMapping("/world")
	@ApiOperation("world")
    public ApiResponse world(){
		Logger.info("world方法");
        return ApiResponse.ok("world方法");
    }
}
