package pers.flyingbear.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pers.flyingbear.tool.model.ApiResponse;

@RestController
@Api(value = "Hystrix测试控制器")
public class HystrixController {
	
	@GetMapping("/hx/{id}")
	@ApiOperation("Hystrix熔断测试方法")
	@HystrixCommand(fallbackMethod = "testHystrix")//服务熔断方法
	public ApiResponse test(@PathVariable String id) {
		if("0".equals(id)) {
			throw new RuntimeException("熔断");
		}
		return ApiResponse.ok("未走Hystrix熔断");
	}
	//保险丝方法
	@ApiOperation("Hystrix熔断回调方法")
	public ApiResponse testHystrix(@PathVariable String id) {
		return ApiResponse.error("Hystrix熔断："+id);
	}
}
