package pers.flyingbear.controller;

import pers.flyingbear.model.SysUser;
import pers.flyingbear.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import pers.flyingbear.tool.model.ApiResponse;

import javax.annotation.Resource;

@RestController
@Api(value = "SysUser控制器")
public class SysUserController {
	@Resource
	SysUserService sysUserService;

	/**
	 * @param id
	 * @return
	 */
	@GetMapping("/sysUser/{id}")
	@ApiOperation("根据ID获取用户信息")
	public ApiResponse getUserInfo(@PathVariable("id") Long id) {
		SysUser sysUser = sysUserService.selectByPrimaryKey(id);
		return ApiResponse.ok(sysUser);
	}

	@PutMapping("/sysUser/{id}")
	@ApiOperation("根据ID更新用户信息")
	public ApiResponse updateUserInfo(@PathVariable("id") Long id) {
		int rows = sysUserService.updateUserInfo(id);
		return ApiResponse.ok("更新成功");
	}
}
