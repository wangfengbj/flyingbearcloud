insert into users(username,password,enabled)
values('user','{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e', TRUE);
insert into users(username,password,enabled)
values('admin','{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e', TRUE);
insert into authorities(username,authority)
values('admin','ROLE_ADMIN');
insert into authorities(username,authority)
values('admin','ROLE_USER');
insert into authorities(username,authority)
values('user','ROLE_ADMIN');