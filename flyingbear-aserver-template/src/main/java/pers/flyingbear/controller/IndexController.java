package pers.flyingbear.controller;

import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.WebAttributes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pers.flyingbear.config.SecurityUser;

import javax.servlet.http.HttpServletRequest;

@RestController
public class IndexController {
	
	@GetMapping("/index")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s，这是首页! ", name);
	}

	@GetMapping("/sys")
	public String sys() {
		return "这是超级管理员界面!" ;
	}

	/**
	 * SpringSecurity获取自定义用户信息
	 * @param authentication
	 * @return
	 */
	@GetMapping("/getUser")
	public SecurityUser getOtherInfo(Authentication authentication){
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return (SecurityUser)authentication.getPrincipal();
	}

	/**
	 * 自定义登录失败处理页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value = {"loginFail"})
	public ModelAndView loginFail(HttpServletRequest request) {
		ModelAndView modelAndView = new ModelAndView("system/login");
		String error = null;
		// 登录异常处理
		Object exception = request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		if (exception instanceof AuthenticationException) {
			if(exception instanceof UsernameNotFoundException){
				// 自己抛出异常信息
				error = ((UsernameNotFoundException) exception).getMessage();
			}else if (exception instanceof BadCredentialsException){
				error = "用户名或者密码输入错误，请重新输入!";
			}else if (exception instanceof LockedException){
				error = "账户被锁定，请联系管理员!";
			}else if (exception instanceof CredentialsExpiredException){
				error = "密码过期，请联系管理员!";
			}else if (exception instanceof AccountExpiredException){
				error = "账户过期，请联系管理员!";
			}else if (exception instanceof DisabledException){
				error = "账户被禁用，请联系管理员!";
			}else{
				error = "认证失败";
			}
		}
		modelAndView.addObject("error", error);
		return modelAndView;
	}
}
