package pers.flyingbear;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.BytesEncryptor;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.BytesKeyGenerator;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@SpringBootApplication
public class TemplateServer {
	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(16); // 默认强度10，鼓励您在自己的系统上调整和测试强度参数，以便大约需要 1 秒来验证密码
		String result = encoder.encode("123456");
		//assertTrue(encoder.matches("123456", result));
		System.out.println("------------bcrypt加密123456密码："+result);
		String salt = KeyGenerators.string().generateKey();
		//字节加密器
		BytesEncryptor be = Encryptors.stronger("123456", salt);
		//文本加密器
		TextEncryptor te = Encryptors.text("123456", salt);
		//字节密钥
		BytesKeyGenerator generator = KeyGenerators.secureRandom();
		byte[] key = generator.generateKey();
		//字符串密钥
		StringKeyGenerator keys = KeyGenerators.string();
		SpringApplication.run(TemplateServer.class, args);
	}
}
