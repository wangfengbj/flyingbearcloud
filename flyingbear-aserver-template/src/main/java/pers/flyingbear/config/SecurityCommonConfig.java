package pers.flyingbear.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.DefaultAuthenticationEventPublisher;
import org.springframework.security.authorization.AuthorizationEventPublisher;
import org.springframework.security.authorization.SpringAuthorizationEventPublisher;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.security.web.session.SessionInformationExpiredEvent;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.security.SpringSessionBackedSessionRegistry;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Security共用配置
 * 也可以集成WebSecurityConfigurerAdapter，但是已经过时
 * 新的：SecurityFilterChain配置HttpSecurity、WebSecurityCustomizer配置WebSecurity
 */
@EnableWebSecurity //SpringSecurity与SpringMVC的集成
//@EnableGlobalMethodSecurity // 来启用基于注解的安全性；(securedEnabled = true)启用@Secured注释 (prePostEnabled=true)
public class SecurityCommonConfig {

    @Resource
    private DataSource dataSource;

    //RedisOperationsSessionRepository
    @Resource
    private FindByIndexNameSessionRepository findByIndexNameSessionRepository;


    /**
     * defaultSecurityFilterChain
     * @param http
     * @return
     * @throws Exception
     */
    @Bean
    @Order(1)
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/login", "/login.html").permitAll()
                .antMatchers("/oauth/**").permitAll() // oauth接口不需要认证
                .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
                //antMatchers("/secured") 仅仅匹配/secured
                //mvcMatchers("/secured") 匹配/secured之余还匹配/secured/,/secured.html,/secured.xyz
                .mvcMatchers("/sys").hasRole("admin") // .hasAuthority("noPower") .hasAnyAuthority("user:list", "user:all") .access("@customAccessForApi.hasPermission(request,authentication)") hasAnyRole hasIpAddress
                .anyRequest().authenticated() //所有请求认证后访问   .anyRequest().denyAll()未匹配拒绝访问
                //.and().formLogin(Customizer.withDefaults()) // 基于servlet的配置，明确提供基于表单的登录。默认登录页面
                .and().formLogin().loginPage("/login")
                //.failureForwardUrl("/loginFail") // 自定义页面，默认为：loginPage + "?error"
                // 默认 SimpleLoginSuccessHandler
//                .successHandler(new AuthenticationSuccessHandler() {
//                    @Override
//                    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
//                        HttpSession session = request.getSession();
//                        // 这一步SpringSession已经做了
//                        session.setAttribute(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, authentication.getName());
//
//                        String redirectUrl = "/index";
//                        SavedRequest savedRequest = (SavedRequest) request.getSession().getAttribute("SPRING_SECURITY_SAVED_REQUEST");
//                        if(savedRequest != null) {
//                            // 登录前的页面
//                            redirectUrl =   savedRequest.getRedirectUrl();
//                            request.getSession().removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
//                        }
//                        response.sendRedirect(redirectUrl);
//                    }
//                })
                .and().httpBasic() // 开启httpBasic认证，Authorization参数中附带用户/密码的base64编码
                .and().headers().frameOptions().sameOrigin() //iframe同源策略”，解决了h2出现的问题deny
                //.and().csrf().disable() // 跨站点请求伪造CSRF
                //.and().csrf().ignoringAntMatchers("/h2/**", "/oauth/authorize", "/oauth/token", "/oauth/rest_token")
                ;

        // csrf存Cookie， JavaScript可以读取cookie
        http.csrf(csrf -> csrf.csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()));
        // csrf存Cookie， JavaScript不能读取cookie
        //http.csrf(csrf -> csrf.csrfTokenRepository(new CookieCsrfTokenRepository()));


        // 持久认证 SecurityContextRepository安全上下文存储库，
        // 1.SecurityContextPersistenceFilter负责使用SecurityContextRepository处理请求之间的SecurityContext的持久化
        // 2.SecurityContextHolderFilter使用SecurityContextRepository在处理请求之间加载SecurityContext，需要显示保存SecurityContext
        // 默认实现HttpSessionSecurityContextRepository将SecurityContext和HttpSession关联
        // 实现NullSecurityContextRepository不让SecurityContext和HttpSession关联，一般适用于oauth认证
        // 实现RequestAttributeSecurityContextRepository将SecurityContext设置为一个请求参数，一旦发生异常SecurityContext会被清除
//        http.securityContext((securityContext) -> {securityContext
//			.securityContextRepository(new RequestAttributeSecurityContextRepository());
//            // 显示保存SecurityContext
//            securityContext.requireExplicitSave(true);
//        });


        // 强制急切会话创建
        http.sessionManagement(session -> {
            //session.sessionCreationPolicy(SessionCreationPolicy.ALWAYS);
            // session失效重定向到URL
            //session.invalidSessionUrl("/invalidSession.htm");
            session.invalidSessionStrategy(new InvalidSessionStrategy() {
                @Override
                public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
                    response.setCharacterEncoding("UTF-8");
                    response.setHeader("Content-Type", "application/json");
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("application/json;charset=UTF-8");
                    try(PrintWriter writer = response.getWriter()){
                        writer.write("{\"code\":\"0001\",\"msg\":\"会话失效，请重新登录\"}");
                        writer.flush();
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            // 第二次登录将导致第一次登录无效，ConcurrentSessionFilter中doFilter执行登出，失效策略SessionInformationExpiredEvent
            session.maximumSessions(1).expiredSessionStrategy(new SessionInformationExpiredStrategy() {
                @Override
                public void onExpiredSessionDetected(SessionInformationExpiredEvent event) throws IOException, ServletException {
                    HttpServletResponse response = event.getResponse();
                    HttpServletRequest request = event.getRequest();
                    // RequestUtils.isAjax(request)
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("application/json;charset=UTF-8");
                    response.getWriter().print("{\"code\":\"0001\",\"msg\":\"账号已经再其它终端登录！\"}");
                    response.flushBuffer();
                }
            })
            .sessionRegistry(sessionRegistry()) // sessionRegistry分布式下配置
            ;
            // 第二次登录将被拒绝，这种情况直接关闭游览器会导致用户登录不上
            //session.maximumSessions(1).maxSessionsPreventsLogin(true);
        });


        // 记住我
        //http.rememberMe();


        // 退出登录删除cookie
        http.logout(logout -> {
            // get请求登出，一般不建议这么干
            logout.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login");
            //logout.logoutUrl("/logout").logoutSuccessUrl("/login");
            logout.deleteCookies("JSESSIONID", "SESSION");
        });


        // 异常处理，AbstractAccessDecisionManager处理权限
        http.exceptionHandling()
                // 认证失败，不使用默认表单form登录认证时用这个
//                .authenticationEntryPoint((request,response,authenticationException)-> {
//
//                })
                // 认证通过，无权操作
                .accessDeniedHandler(((request, response, accessDeniedException) -> {
                    String contentType = request.getHeader("content-type");
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    response.setContentType("application/json;charset=UTF-8");
                    PrintWriter wr = response.getWriter();
                    wr.write("{\"code\":\"0001\",\"msg\":\"无权限操作；"+accessDeniedException.getMessage()+"\"}");
                    wr.flush();
                    wr.close();
                    //request.getRequestDispatcher("/accessDenied").forward(request,response);
                }));

        //http.addFilterBefore(xxx.class, UsernamePasswordAuthenticationFilter.c)

        return http.build();
    }

    /**
     * session为SpringSecurity提供集群下并发的会话注册表实现类，不能和HttpSessionEventPublisher在一个配置类中
     * @return
     */
    @Bean
    public SpringSessionBackedSessionRegistry sessionRegistry(){
        return new SpringSessionBackedSessionRegistry(findByIndexNameSessionRepository);
    }


    /**
     * 身份验证事件，先配置AuthenticationEventPublisher，然后使用自定义AuthenticationEvents处理
     * @param applicationEventPublisher
     * @return
     */
    @Bean
    public AuthenticationEventPublisher authenticationEventPublisher (ApplicationEventPublisher applicationEventPublisher) {
        return new DefaultAuthenticationEventPublisher(applicationEventPublisher);
    }

    /**
     * 授权事件AuthorizationEventPublisher，然后使用自定义AuthenticationEvents处理
     * @param applicationEventPublisher
     * @return
     */
    @Bean
    public AuthorizationEventPublisher authorizationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        return new SpringAuthorizationEventPublisher(applicationEventPublisher);
    }

    /**
     * 密码加解密
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder(); // spring5之后默认这种
        //return NoOpPasswordEncoder.getInstance(); // spring5之前默认明文的
    }

    /**
     * 角色前缀配置
     * @return
     */
//    @Bean
//    static GrantedAuthorityDefaults grantedAuthorityDefaults() {
//        return new GrantedAuthorityDefaults("MYPREFIX_");
//    }


    /**
     * 跨域配置 http.cors(withDefaults())使用CorsConfigurationSource
     * @return
     */
//    @Bean
//    CorsConfigurationSource corsConfigurationSource() {
//        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("https://example.com"));
//        configuration.setAllowedMethods(Arrays.asList("GET","POST"));
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", configuration);
//        return source;
//    }







    /**
     * 基于内存操作账号密码
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "security", name = "user-mode", havingValue = "memory")
    public UserDetailsService memoryUsers() {
        UserDetails user = User.builder()
                .username("user")
                .password("{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e")
//                .password("{noop}123456") // 明文密码
                .roles("USER")
                .build();
        UserDetails admin = User.builder()
                .username("admin")
                .password("{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e")
//                .password("{noop}123456") // 明文密码
                .roles("USER", "ADMIN")
                .build();
        return new InMemoryUserDetailsManager(user, admin);
    }


    /**
     * 使用security自带的数据库操作账号密码
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "security", name = "user-mode", havingValue = "db")
    public UserDetailsService dbUsers() {
        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);
        //users.createUser(user); // 这可以插入数据库
        //users.createUser(admin); // 这可以插入数据库
        return users;
    }

    /**
     * 使用自定义数据库结构账号密码
     * @return
     */
    @Bean
    @ConditionalOnProperty(prefix = "security", name = "user-mode", havingValue = "myDb")
    public SecurityUserDetailsService securityUserDetailsService() {
        return new SecurityUserDetailsService();
    }
}
