package pers.flyingbear.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.databind.Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.jackson2.SecurityJackson2Modules;

import java.util.List;

/**
 * jackson序列化和反序列化配置
 */
@Configuration
public class Jackson2Config {
	@Bean
	public ObjectMapper objectMapper() {
		ObjectMapper mapper = new ObjectMapper();
		// SpringSecurity提供的Jackson序列化/反序列化支持；CoreJackson2Module、WebJackson2Module, WebServletJackson2Module, WebServerJackson2Module
		// spring-security-oauth2-client(OAuth2ClientJackson2Module)、spring-security-cas(CasJackson2Module)
		List<Module> modules = SecurityJackson2Modules.getModules(getClass().getClassLoader());
		mapper.registerModules(modules);

		// 注册java8日期序列化/反序列化 new JavaTimeModule()，上面会扫到module

		//OAuth2ClientJackson2Module[spring-security-oauth2-client]
		//mapper.registerModule(new OAuth2ClientJackson2Module());
		return mapper;
	}

}
