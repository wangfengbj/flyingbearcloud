package pers.flyingbear.config;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 从自定义数据库读取用户信息 UserDetailsService
 */
public class SecurityUserDetailsService implements UserDetailsService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if(!StringUtils.hasLength(username)){
            throw new UsernameNotFoundException("用户不存在！");
        }
        // ******去数据库查询******
        if(!"user".equals(username)){
            throw new UsernameNotFoundException("用户名称或者密码不正确！");
        }
        // ******角色信息和授权信息******
        String role = "ROLE_USER";
        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));

        // 如果是明文密码需要根据PasswordEncoder加密处理
        String password ="{bcrypt}$2a$16$AAdbhE9V7q72/s8avHm4WumMGF3fLOT7Rgt1N3rGYY77CKrcukw1e";
        //User user = new User(username, password, authorities);
        // User只存储用户名称，可以扩展信息
        SecurityUser user = new SecurityUser(username, password, authorities, "自定义扩展信息", true);
        return user;
    }
}
