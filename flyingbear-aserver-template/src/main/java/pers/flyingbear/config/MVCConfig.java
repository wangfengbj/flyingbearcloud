package pers.flyingbear.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * MVC 配置
 * @author wf
 *
 */
@Configuration
public class MVCConfig implements WebMvcConfigurer{
	
	/**
	 * 视图和路径映射
	 */
	public void addViewControllers(ViewControllerRegistry registry) {
		// 登录/login映射到login试图，功能相当于controller中配置一个映射
		registry.addViewController("/login").setViewName("login");
	}
}
