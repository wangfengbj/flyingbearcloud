package pers.flyingbear.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

/**
 * 数据源配置
 */
@Configuration(proxyBeanMethods = false)
public class DataSourceConfig {
    /**
     * 内嵌数据源，默认账号sa
     * @return
     */
    @Bean
    public EmbeddedDatabase embeddedDatabase() {
        //EmbeddedDatabaseFactory edf = new EmbeddedDatabaseFactory();
        return new EmbeddedDatabaseBuilder()
                .generateUniqueName(false) // 是否自动生成数据库名称
                .setName("wf")
                //.setDataSourceFactory() // 可以覆盖默认账号sa
                .setType(EmbeddedDatabaseType.H2)
                .setScriptEncoding("UTF-8")
                .addScript(JdbcDaoImpl.DEFAULT_USER_SCHEMA_DDL_LOCATION) // SpringSecurity的初始脚本
                .addScript("mydata.sql") // 如果名字data.sql不需要配置默认加载
                .build();
    }
}
