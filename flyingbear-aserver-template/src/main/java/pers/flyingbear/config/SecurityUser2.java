package pers.flyingbear.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 2. 也可以继承User扩展信息
 */
@Getter
@Setter
public class SecurityUser2 extends User {
    // 扩展信息
    private String otherInfo;

    public SecurityUser2(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }
}
