package pers.flyingbear.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

//@FeignClient(name="template-client", url = "http://${ssc.client.url}:${ssc.client.port}", path = "/ssc/file")
public interface TemplateClient {
    @PostMapping(value="/addFile", consumes= MediaType.MULTIPART_FORM_DATA_VALUE)
    Map<String, String> addFile(@RequestParam("project") String project, @RequestParam("module") String module, @RequestParam("category") String category, @RequestPart("file") MultipartFile file);

    @PostMapping(value="/addFiles", consumes=MediaType.MULTIPART_FORM_DATA_VALUE)
    Map<String, Object> addFiles(@RequestParam("project") String project, @RequestParam("module") String module, @RequestParam("category") String category, @RequestPart("file") List<MultipartFile> files);

    @PostMapping(value="/getFile")
    ResponseEntity<byte[]> getFile(@RequestParam("project") String project, @RequestParam("id") String id);

    @PostMapping(value="/getLogSysList", headers = {"content-type=application/json"})
    List<Map<String, Object>> getLogSysList(@RequestBody String json);
}
