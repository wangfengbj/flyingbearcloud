package pers.flyingbear.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.security.authorization.event.AuthorizationDeniedEvent;
import org.springframework.stereotype.Component;

/**
 * SpringSecurity授权事件
 */
@Component
@Slf4j
public class AuthorizationEvents {
    @EventListener
    public void onFailure(AuthorizationDeniedEvent failure) {
        log.info("授权失败1: {}", failure.getSource());
        log.info("授权失败2: {}", failure.getAuthentication());
    }
}
