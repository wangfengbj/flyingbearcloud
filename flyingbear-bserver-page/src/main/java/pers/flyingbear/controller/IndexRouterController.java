package pers.flyingbear.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 路由控制器
 */
@Controller
public class IndexRouterController {
    @GetMapping("/")
    public String _login() {
        return "guides.html";
    }
}
