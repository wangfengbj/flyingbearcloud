package pers.flyingbear.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 错误页面处理
 * @author wind
 *
 */
@Controller
@RequestMapping("/error")
public class ErrorController {
	/**
     * 404页面
     */
    @GetMapping(value = "/404")
    public String error_404() {
        return "/page/error/404.html";
    }

    /**
     * 500页面
     */
    @GetMapping(value = "/500")
    public String error_500() {
        return "/page/error/404_bg.html";
    }
}
