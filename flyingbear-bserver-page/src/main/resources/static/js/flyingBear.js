// 扩展以**开始
String.prototype.startWith = function(str) {
    let reg = new RegExp("^" + str);
    return reg.test(this);
}
// 扩展以**结束
String.prototype.endWith = function(str) {
    let reg = new RegExp(str + "$");
    return reg.test(this);
}
// 删除左右两端的空格
String.prototype.trim=function(){
     return this.replace(/(^\s*)|(\s*$)/g,'');
}
function isRealNum(val) {
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除
    if (val === "" || val == null) {
        return false;
    }
    if (!isNaN(val)) {
        return true;
    } else {
        return false;
    }
}
define(["jquery",  "layer", "JSON", "jquery-cookie"], function() {
	// ----------------------全局初始化开始----------------------------
    let jquery = require("jquery");
    let $j = jquery.noConflict();
    $j.ajaxSetup({
        cache: false,
        timeout: 120000
    });
	
    // 设置layer路径，按照该路径加载layer.css
    let layer = require("layer");
    layer.config({
        path: plugins_path +'/layer/'    // layer.js的路径，此处使用绝对路径
    });
    // ----------------------全局初始化结束----------------------------
    
    
    return{
        openWait: function() {
        	//打开加载层
            var layer = require('layer');
            var handle = layer.load(0, {shade: 0.3});
            return handle;
        },
        closeWait: function(handle) {
        	//关闭加载层(不传参数，关闭所有加载层)
            var layer = require('layer');
            if(handle != null){
                layer.close(handle);
            } else {
                layer.closeAll('loading');
            }
        },
        alert: function(type, title, content, yes) {
            var icon;
            if(type == 'success') {
                icon = 1;
            } else if (type == 'error') {
                icon = 2;
            } else if (type == 'warning') {
                icon = 0;
            } else {
                icon = type;
            }
            layer.alert(content, {icon: icon, title: title, success: function(handle,index){
            	this.enterEsc = function(event){
        	      if(event.keyCode === 13){
        	        layer.close(index);
        	        return false; //阻止系统默认回车事件
        	      }
        	    };
        	    $j(document).on('keydown', this.enterEsc);	//监听键盘事件，关闭层
            },end: function(){
            	$j(document).off('keydown', this.enterEsc);	//解除键盘关闭事件
            }
            }, function(handle) {
                layer.close(handle);
                if(yes != null) {
                    yes();
                }
            });
        },
        confirm: function(title, content, yes, cancel) {
        	var layer = require('layer');
        	layer.confirm(content, {icon: 3, title:title}, function(handle){
        		layer.close(handle);
        		if(yes != null) {
        			yes();
        		}
             }, function(handle) {
            	 layer.close(handle);
            	 if(cancel != null) {
            		 cancel();
            	 }
             });
        },
        // 给url加随机参数防止ajax请求缓存
        randomUrl: function(url) {
            if (url.indexOf("?") != -1) {
                url += "&t=" + new Date().getTime();
            } else {
                url += "?t=" + new Date().getTime();
            }
            return url;
        },
        jump: function(url) {
            window.location.href = WEBPATH + zuul_preix + url;
        },
        // 解析ajax请求返回json对象时候的同一回调处理函数
        ajaxJsonHandle: function(result) {
            if (typeof (result.code) != "undefined") {
                if (result.code == 200) {
                    this.alert('success','', result.message);
                    return true;
                } else if (result.code == 400) {
                	this.alert('warn','操作提示', result.message);
                    return false;
                } else if (result.code == "jump") {
                    window.location.href = WEBPATH + zuul_preix + result.message;
                    return true;
                }
            }
            return true;
        },
        handleToken:function(data, request){
        	if(data.token){
        		//$j.cookie('token',data.token,{expires:1,path: '/',domain:"hopeseen.com"});
        		var date = new Date();
        		date.setTime(date.getTime() + 60*60*1000);
        		$j.cookie('token',data.token,{expires:date,path: '/'});
        	}else{
        		let token = request.getResponseHeader('token');
        		if(token){
        			var date = new Date();
            		date.setTime(date.getTime() + 60*60*1000);
            		$j.cookie('token',token,{expires:date,path: '/'});
        		}
        	}
        },
        getAjaxHead:function(){
        	//请求头加token
        	let header_param = {}
        	let token = $j.cookie('token');
        	if(token){
        		header_param.AuthToken=token;
        	}
        	return header_param;
        },
        postJsonNoConfirm: function(url, param, option, succFunc, errorFunc) {
        	var dtd = $j.Deferred();
        	var showWait=true;
        	if (option != null && option.showWait!=null && !option.showWait) {
        		showWait=false;
        	}
            var waitIndex;//遮盖层index
        	if(showWait){
        		waitIndex = this.openWait();
        	}
            var vform = null, vurl = null;
            if (null != option) {
                if (option.formid == null) {
                    vform = $j('#' + option.buttonid).closest("form");
                }else {
                    vform = $j('#' + option.formid);
                }
            }
            (param == null) ? param = {} : param = param;
            var flyingBear=this;
            $j.ajax({
            	type: "POST",
            	url: WEBPATH + require('static/js/flyingBear').randomUrl(url),
            	headers:require('static/js/flyingBear').getAjaxHead(),
//            	contentType: "application/json; charset=utf-8",
//            	data: JSON.stringify(param),
            	contentType : "application/x-www-form-urlencoded",
            	data: param,//不需要使用JSON.stringify()
            	dataType: "json",
            	success: function(message, textStatus, request) {
	            	if (showWait) {
	                    flyingBear.closeWait(waitIndex);
	                }
	            	flyingBear.handleToken(message, request);
	                if (succFunc != null) {
	                    succFunc(message);
	                    dtd.resolve(message);
	                }
	            },
	            error: function(message) {
	                if (errorFunc != null) {
	                    errorFunc(message);
	                }
	                dtd.resolve();
	                if (showWait) {
	                	flyingBear.closeWait(waitIndex);
	                }
	            }
            });
            return dtd.promise();
        },
        postJson: function(url, param, option, succFunc, errorFunc) {
            // 当需要confirm的时候
            if (option.confirm != null) {
	            this.confirm('请确认', option.confirm, function() {
	                    var flyingBear = require("static/js/flyingBear");
	                    return flyingBear.postJsonNoConfirm(url, param, option, succFunc, errorFunc);
	            });
                return;
            }else{
            	return this.postJsonNoConfirm(url, param, option, succFunc, errorFunc);
            }
            
        },
        getJsonNoConfirm: function(url, param, option, succFunc, errorFunc) {
        	var dtd = $j.Deferred();
        	var showWait=true;
        	if (option != null&&option.showWait!=null&&!option.showWait) {
        		showWait=false;
        	}
        	if(showWait){
        		this.openWait();
        	}
            if (param == null) {
                param = {};
            }
            
            var vform = null, vurl = null;
            if (option != null) {
                if (option.formid == null) {
                    vform = $j('#' + option.buttonid).closest("form");
                }else {
                    vform = $j('#' + option.formid);
                }
            }
            // form字段校验
            if (vform != null && vform.length > 0) {
                if (vform.form('validate') == false) {
                    // 关闭等待条
                    this.closeWait();
                    dtd.resolve();
                    return dtd.promise();
                }
            }
            
            var flyingBear=this;
            
            $j.ajax({
            	type: "GET",
            	url: WEBPATH + require('static/js/flyingBear').randomUrl(url),
            	headers:require('static/js/flyingBear').getAjaxHead(),
            	contentType: "application/json; charset=utf-8",
            	data: param,
            	dataType: "json",
            	success: function(message, textStatus, request) {
	            	if(showWait){
	            		flyingBear.closeWait();
	            	}
	            	flyingBear.handleToken(message, request);
	                if (succFunc != null) {
	                    succFunc(message);
	                }
	                dtd.resolve(message);
	            },
	            error: function(message) {
	            	flyingBear.closeWait();
	                if (errorFunc != null) {
	                    errorFunc(message);
	                }
	            }
            });
            return dtd.promise();
        },
        getJson: function(url, param, option, succFunc, errorFunc) {
            var isConfirm = false;
            var flyingBear=this;
            // 当需要confirm的时候
            if (option != null && option.confirm != null) {
            	this.confirm('请确认', option.confirm, function() {
            		return flyingBear.getJsonNoConfirm(url, param, option, succFunc, errorFunc);
            	});
                return;
            }
            return this.getJsonNoConfirm(url, param, option, succFunc, errorFunc);
        },
        getJsonSync: function(url, param, option) {
            var isConfirm = false;
            var responseJson = null;
            var flyingBear=this;
            // 当需要confirm的时候
            if (option != null && option.confirm != null) {
            	this.confirm('请确认', option.confirm, function() {
            		responseJson = flyingBear.getJsonNoConfirmSync(url, param, option);
            	});
                this.closeWait();
                return responseJson;
            }
            responseJson = this.getJsonNoConfirmSync(url, param, option);
            this.closeWait();
            return responseJson;
        },
        getJsonNoConfirmSync: function(url, param, option) {
            var showWait=true;
        	if (option != null&&option.showWait!=null&&!option.showWait) {
        		showWait=false;
        	}
        	if(showWait){
        		this.openWait();
        	}
            var vform = null, vurl = null;
            if (option != null) {
                if (option.formid == null) {
                    vform = $j('#' + option.buttonid).closest("form");
                }else {
                    vform = $j('#' + option.formid);
                }
            }
            // form字段校验
            if (vform != null && vform.length > 0) {
                if (vform.form('validate') == false) {
                    // 关闭等待条
                	if(showWait){
                		require("static/js/flyingBear").closeWait();
                    }
                    return;
                }
            }
            
            var res = null;
            $j.ajax({
            	type: "GET",
            	url: WEBPATH + require('static/js/flyingBear').randomUrl(url),
            	headers:require('static/js/flyingBear').getAjaxHead(),
            	contentType: "application/json; charset=utf-8",
            	async: false,
            	data: param,
            	dataType: "json",
            	success: function(message, textStatus, request) {
            		if(showWait){
            			require("static/js/flyingBear").closeWait();
            		}
            		require("static/js/flyingBear").handleToken(message, request);
            		res = message;
            	},
            	error: function(message) {
            		if(showWait){
            			require("static/js/flyingBear").closeWait();
            		}
            		alert(url + "请求失去响应");
            	}
            });
            return res;
        },
        postJsonNoConfirmSync: function(url, param, option, succFunc, errorFunc) {
            //var dtd = $j.Deferred();
            var showWait=true;
            if (option != null&&option.showWait!=null&&!option.showWait) {
                showWait=false;
            }
            if(showWait){
                this.openWait();
            }
            var vform = null, vurl = null;
            if (null != option) {
                if (option.formid == null) {
                    vform = $j('#' + option.buttonid).closest("form");
                }else {
                    vform = $j('#' + option.formid);
                }
            }
            (param == null) ? param = {} : param = param;
            // form字段校验
            if (vform != null && vform.length > 0) {
                //解决下拉选项遮盖校验的问题
                $j(".select2-drop-active").insertBefore($j(".tooltip"));
                if (vform.form('validate') == false) {
                    console.log("validate false");
                    // 关闭等待条
                    this.closeWait();
                    //dtd.resolve();
                    //return dtd.promise();
                }
            }
            var flyingBear=this;
            $j.ajax({
                type: "POST",
                async: false,
                url: WEBPATH + require('static/js/flyingBear').randomUrl(url),
                headers:require('static/js/flyingBear').getAjaxHead(),
//                contentType: "application/json; charset=utf-8",
//                data: JSON.stringify(param),
                contentType : "application/x-www-form-urlencoded",
            	data: param,//不需要使用JSON.stringify()
                dataType: "json",
                success: function(message, textStatus, request) {
                    flyingBear.closeWait();
                    flyingBear.handleToken(message, request);
                    if (succFunc != null) {
                        succFunc(message);
                        //dtd.resolve(message);
                    }
                },
                error: function(message) {
                    if (errorFunc != null) {
                        errorFunc(message);
                    }
                    //dtd.resolve();
                    flyingBear.closeWait();
                }
            });
            //return dtd.promise();
        },
        postJsonSync: function(url, param, option, succFunc, errorFunc) {
            // 当需要confirm的时候
            if (option.confirm != null) {
                this.confirm('请确认', option.confirm, function() {
                    var flyingBear = require("static/js/flyingBear");
                    return flyingBear.postJsonNoConfirmSync(url, param, option, succFunc, errorFunc);
                });
                return;
            }
            else{
                return this.postJsonNoConfirmSync(url, param, option, succFunc, errorFunc);
            }
        },
        
        
        getHtml: function(url, param, succFunc, errorFunc) {
    		var dtd = $j.Deferred();
            this.openWait();
            var flyingBear=this;
            if (param == null) {
                param = {};
            }
            $j.ajax({
            	type: "GET",
            	// 读取静态html不加随机参数,进行缓存
            	url: WEBPATH + zuul_preix+ url,
            	contentType: "text/html; charset=utf-8",
            	data: param,
            	dataType: "html",
            	success: function(message) {
            		flyingBear.closeWait();
            		if (succFunc != null) {
            			succFunc(message);
            			dtd.resolve(message);
            		}
            	},
            	error: function(message) {
            		flyingBear.closeWait();
            		if (errorFunc != null) {
            			errorFunc(message);
            			dtd.resolve(null);
            		}
            	}
            });
            return dtd.promise();
        },
        pageRender:function(domobj){
        	//和easyui的tootip不冲突
        	//$j.fn.tooltip.noConflict();
        	if(domobj){
        		//easyui 渲染
                $j.parser.parse(domobj);
            	//viewer图片渲染
            	$j('.viewer', domobj).viewer();
        	}
            //flyingBear 渲染
            var flyingBear=this;
            $j("flyingBear").each(function(){
                var here=$j(this);
                eval("flyingBear." + $j(this).text());
            });
        },
        includePage:function(oldEle,page,pageJs){
         	this.getHtml(page,{},function(res){
         		//var oldEle=$j("#"+ele);
 				oldEle.replaceWith(res); 
				if(pageJs!=null&&pageJs.length>0){
					require([pageJs], function(jsmodel) {
 		        			jsmodel.start();
        			});
        		}
			});
         },
         openPageInDiv: function(divId,url,beforeJsStart,initData,jsurl){
//             let flyingBear=this;
//             if(!flyingBear.loginCheck()){
//                 return;
//             }
        	 if (!jsurl) {
                 jsurl = url;
                 if (jsurl.lastIndexOf('?') != -1) {
                     jsurl = jsurl.substring(0, jsurl.lastIndexOf('?'));
                 }
                 jsurl = '..' + jsurl.replace(/\.html/g, '');
             }
             this.getHtml(url,{},function(res){
                 $j("#"+divId).empty();
                 $j("#"+divId).html(res);
                 if(jsurl != null && jsurl.length>0){
                     require([jsurl], function(jsmodel) {
                         if(null != beforeJsStart){
                             beforeJsStart(jsmodel);
                         }
                         if(null != initData){
                             jsmodel.setData(initData);
                         }
                         jsmodel.start();
                     });
                 }//end if
             });//end getHtml
         },
         
         
    }
	
})