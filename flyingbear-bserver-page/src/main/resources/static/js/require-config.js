// 全局变量
let $j=null;
// let zuul_prefix="/static"; // 统一前缀添加，处理网关映射问题
let zuul_prefix="";
let webApp="myApp";

// 项目名称
function getWebPath(){
	let localObj = window.location;
	let basePath = localObj.protocol+"//"+localObj.host;
	//
	let contextPath = localObj.pathname.split("/")[1];
	if(contextPath == "page"){
		//项目放到root情况
	} else {
		//basePath += "/"+contextPath;
		basePath = localObj.origin
	}
	return basePath;
}
let WEBPATH=getWebPath();
let plugins_path = WEBPATH+zuul_prefix+"/plugins";
let js_path = WEBPATH+zuul_prefix+"/js";

require.config({
	baseUrl : plugins_path,
	paths : {
		"jquery" : "jquery/jquery-3.5.1.min",
		"jquery-raty" : "jqueryRaty/jquery.raty",
		"jquery-cookie" : "jquery/jquery.cookie.min",
		"JSON":"json2.min",
		"domready" : "domReady", // require需要
		"bootstrap":"bootstrap/js/bootstrap.bundle.min",
		"flyingBear" : js_path+"/flyingBear",
		"layer":"layer/layer",//提示框
		'moment':'moment.min',//时间
		"avalon":"avalon/avalon.min",//mvvm
		"vue":"vue/vue",
		"vue-router":"vue/vue-router",
		"angular": "angular/angular.min",
		'angularAMD': 'angular/angularAMD.min',
        'ngload': 'angular/ngload.min',
        "app": "angular/app",
		"ztree": "ztree/jquery.ztree.all.min"
	},
	shim: {
		'jquery-raty': ['jquery', "css!" + plugins_path + "s/jqueryRaty/jquery.raty.css"],
		'bootstrap':{deps:['jquery']},//shim在这里表示bootstrap模块在加载之前需要先加载jquery
		"JSON":{
        	deps: [],
            exports: "JSON"
        },
        'jquery-cookie':['jquery'],
        "angular":{
            exports:"angular"
        },
        'angularAMD': ['angular'],
        'ngload': ['angularAMD'],
        "ztree": [ "css!" + plugins_path + "/ztree/css/zTreeStyle/zTreeStyle.css"],
    },
    map: {//map告诉RequireJS在任何模块之前，都先载入这个css模块
        '*': {
            'css': plugins_path +'/require-css/css.min'
        }
    },
	waitSeconds: 15
});

require([
	// 把自己写的放后面，可以覆盖前面同名字样式
	//"css!"+WEBPATH+"/css/fyw.css",
]);