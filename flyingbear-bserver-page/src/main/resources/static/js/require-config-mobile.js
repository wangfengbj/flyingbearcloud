//全局变量
var $jm = null;

function getWebPath(){
	
	var localObj = window.location;

	var contextPath = localObj.pathname.split("/")[1];

	var basePath = localObj.protocol+"//"+localObj.host+"/"+contextPath;

	var server_context=basePath;
	
	return server_context;
}

var WEBPATH=getWebPath();


require.config({
	baseUrl : WEBPATH+"/js",
	paths : {
		"jquery-weui" : "mobile/jquery-weui",
		"jquery-form" : "mobile/jquery.form",
		"jquery" : "jquery-3.5.1.min",
		"domready" : "domReady",
		"easyui" : "easyui/jquery.easyui.min",
		"easyui-cn" : "easyui/easyui-lang-zh_CN",
		"JSON":"json2.min",
		"hxflow_mobile":"hxflow_mobile",
		"hxcache_mobile":"hxcache_mobile",
		"avalon":"avalon.min.new",
		"uflow":"uflow_mobile",
		'bootstrap':"bootstrap/4.5.0/bootstrap.bundle.min"
	},
	 shim: {
		 'jquery-weui':['jquery'],
		 'notebook':['jquery'],
		 'bootstrap':['jquery'],
		 'bootstrap-switch':['jquery'],
	        //easyui-lang-zh_CN.js也依赖jquery
	        'easyui-cn': ['jquery','easyui'],
	        'easyui': ['jquery'],
	        'tool':['jquery'],
	        "JSON":{
	        	deps: [],
	            exports: "JSON"
	        }
	    },
	   map: {
	        '*': {
	            'css': 'css'
	        }
	    },
	 waitSeconds: 15
});

require([   
	"css!"+WEBPATH+"/css/mobile/weui.min.css",
	"css!"+WEBPATH+"/css/mobile/jquery-weui.css",
	"css!"+WEBPATH+"/css/mobile/hopeseenmobile.css",
	"css!"+WEBPATH+"/css/hxflow.css",
]);

