
//document.origin = "https://www.amap.com/";
let themap = {};
let cityImgRectLnglats = []; //最后抓取瓦片的左上右下经纬度
let allCenterLnglats = [];  //所有地图窗口的中心点
let loopIndex = 0;
let clientWidth = 1520;  //地图瓦片的宽度和高度
let clientHeight = 630;
let windowIndexs = [0,0]; //图片下标二维数组，用户进行图片拼接
let xCount;
let yCount;

let getAllCenterLnglats = function(lefttop, rightbottom){
	//获取左上角和右下角范围内坐标
	let minLng = lefttop[0]
	let maxLat = lefttop[1]
	let maxLng = rightbottom[0]
	let minLat = rightbottom[1]
	
	//抓取范围对应屏幕坐标范围
	let leftPoint = themap.lngLatToContainer(new AMap.LngLat(minLng, maxLat))
	let rightPoint = themap.lngLatToContainer(new AMap.LngLat(maxLng, minLat))
	let minX = leftPoint.x;
	let minY = leftPoint.y;
	let maxX = rightPoint.x;
	let maxY = rightPoint.y;
	
	//根据单个瓦片大小计算划分数量
	let gapX = maxX-minX
	let gapY = maxY-minY

	xCount = parseInt(gapX/clientWidth) +1
	yCount = parseInt(gapY/clientHeight) +1
	
	let rectLefttopPoint = new AMap.Pixel(0,0);
	let rectRightbottomPoint = new AMap.Pixel(0,0);
	//以左上角平面坐标为起始抓取中心点，重新计算完整抓取的瓦片左上角和右下角屏幕坐标
	rectLefttopPoint.x = leftPoint.x-clientWidth/2;
	rectLefttopPoint.y = leftPoint.y-clientWidth/2;
	rectRightbottomPoint.x = rectLefttopPoint.x+clientWidth*xCount;
	rectRightbottomPoint.y = rectLefttopPoint.y+clientHeight*yCount;
	
	//完整抓取范围的屏幕坐标转换为经纬度坐标
	cityImgRectLnglats[0] = themap.containerToLngLat(rectLefttopPoint);
	cityImgRectLnglats[1] = themap.containerToLngLat(rectRightbottomPoint);
	
	
	let loopPoints=[]  //计算所有可能的视窗中心点
	let loopPoint = new AMap.Pixel(0,0)
	let startX = leftPoint.x
	let startY = leftPoint.y
	for (let i = 0; i < yCount; i++) {
		loopPoint.y = startY*1.0+i*clientHeight
		for (let j = 0; j < xCount; j++) {
			loopPoint.x = startX*1.0+j*clientWidth 
			loopPoints.push(loopPoint)
			//屏幕坐标转为经纬度
			allCenterLnglats.push(themap.containerToLngLat(loopPoint))
		}  
	}
	console.log('total-' + allCenterLnglats.length)
	console.log('xCount-' + xCount)
	console.log('yCount-' + yCount)

}

//发送拼图四个角的经纬度
let outPutRectLnglats=function(){
	let lnglatsTxt=''
	lnglatsTxt = lnglatsTxt+'左上角：'+cityImgRectLnglats[0].lng+','+cityImgRectLnglats[0].lat+';\n'
	lnglatsTxt = lnglatsTxt+'右下角：'+cityImgRectLnglats[1].lng+','+cityImgRectLnglats[1].lat+';\n'
	//写文本
	let blobShape = new Blob([lnglatsTxt], {type: "text/plain;charset=utf-8"});
    saveAs(blobShape, 'outlnglat.txt');
}


//moveend事件后再截图
let mapMoveend=function(){
    document.getElementById("status").innerHTML = '抓取进度：'+loopIndex + "/" + allCenterLnglats.length;
	setTimeout(() => { 
		getImage()     
	}, 1000);  //等待渲染好再截图
}


//移动视窗
let moveMap = function(){
    windowIndexs = getArrayIndex(loopIndex);
	lnglat = allCenterLnglats[loopIndex];
	themap.panTo([lnglat.lng, lnglat.lat]);
	loopIndex++
}

//获取所有的高德地图视图窗口img
let getAllImages = function(){
    moveMap();
}

//计算瓦片下标编号
let getArrayIndex = function(index){
	index_0 = parseInt(index/xCount)
	index_1 = index%xCount
	return [index_0,index_1]
}




// ------------------------------------------------wangfeng---------------------------------------------------------
// 点击按钮渲染图层选项到下拉框
function setSelect(){
	let layersData = themap.getLayers();
	let layersSelect = document.getElementById("layersSelect");
	for(let i=0; i<layersData.length; i++){
		layersSelect.options.add(new Option(layersData[i].CLASS_NAME, i));
	}
}

// 图层生成图片
function cutImg(){
	let layersData = themap.getLayers();
	let sv = document.getElementById("layersSelect").value;
	let layerData = layersData[sv];
	if(layerData.canvas){
		let canvas = layerData.canvas;
		let imgUrl = canvas.toDataURL("image/png"); //image/jpeg
        document.getElementById("imgshow").setAttribute('src',imgUrl);
	}else if(layerData.map.canvas){
		let canvas = layerData.map.canvas;
		var imgUrl = canvas.toDataURL("image/png");
        document.getElementById("imgshow").setAttribute('src',imgUrl);
	}
}


// 高德地图增加canvas画点
function drawPoint(){
	var canvas = document.createElement('canvas');
    canvas.width = canvas.height = 200;

    var context = canvas.getContext('2d')
    context.fillStyle = 'rgb(0,100,255)';
    context.strokeStyle = 'white';
    context.globalAlpha = 1;
    context.lineWidth = 2;


	var radious = 0;
    var draw = function () {
        context.clearRect(0, 0, 200, 200)
        context.globalAlpha = (context.globalAlpha - 0.01 + 1) % 1;
        radious = (radious + 1) % 100;

        context.beginPath();
        context.arc(100, 100, radious, 0, 2 * Math.PI);
        context.fill();
        context.stroke();

        // 刷新渲染图层
        CanvasLayer.reFresh();

		//动画
        AMap.Util.requestAnimFrame(draw);
    };


    // 高德图层
    var CanvasLayer = new AMap.CanvasLayer({
        canvas: canvas,
        bounds: new AMap.Bounds(
            [116.328911, 39.937229],
            [116.342659, 39.946275]
        ),
        zooms: [3, 18],
    });
    themap.addLayer(CanvasLayer);

    // 画
	draw();
}


//下载图片按钮
let testGetImg = function(){
    getImage();
}


//下载截图
let getImage=function(){
	let x = windowIndexs[0];
	let	y = windowIndexs[1];
	// ********* 获取地图canvas节点 **********
	//let container = document.body; // 整个body截图
	//let container = document.getElementsByClassName("mapContainer")[0];  // 整个map容器截图
	let container = document.getElementsByClassName("amap-layer")[0];  //amap-layer

	//themap.render();  //******webgl动画导不出来，截图前掉一次渲染代码，截图一个瞬间，
	
	html2canvas(container, {
       //useCORS: false, // 解决跨域保存图片的问题设置true
    }).then(canvas => {
        canvas.toBlob((blob) => {
		    saveAs(blob, x+'-'+y+".png") // 下载截图
			//递归移动地图
			if(loopIndex<allCenterLnglats.length){
			    moveMap();
		    }else{
		       document.getElementById("status").innerHTML = '抓取完成!'
		    } 
	    });		
    })
}


window.onload = function(){
	let layers = new AMap.createDefaultLayer({ id: "mapLayer", zIndex: 0, detectRetina: !0 });
	let options = {
    	layers: [layers],
    	mapStyle: 'amap://styles/normal',  //地图中只有用户配置的要素
        viewMode: "2D",  // 默认使用 2D 模式，如果希望使用带有俯仰角的 3D 模式，请设置 viewMode: '3D'
        buildingAnimation: !1,
        maxPitch: 60,
        rotateEnable: !0,
        isHotspot: !0,
        resizeEnable: !0,  //resizeEnable: true,  
        expandZoomRange: !0, //expandZoomRange: true, 
        zoom: 11,      //初始化地图层级
        zooms: [3, 20],
        zIndex: 100,
        turboMode: !1,
        showIndoorMap: !0,
        center: [116.397428, 39.90923],  //初始化地图中心 [114.0222,22.52342]
		//showLabel: false,
		//dragEnable: false,   
        labelzIndex: 112,
        WebGLParams: {
        	preserveDrawingBuffer: true, //******保留图形缓冲区******
        }
    }

    // 根据分辨率调整层级范围
    let p = window.document.body.clientWidth;
	p > 1792 && (options.zooms = [4, 20]);
    p > 3840 && (options.zooms = [5, 20]);


	// ******源码解析******官网https://www.amap.com/build/map.de486f3eafaef08cdb24.js 有地图初始化代码 window.themap = new AMap.Map("themap",l)
	themap = new AMap.Map('mapContainer', options);
	// window.themap = new AMap.Map("themap",l)
	

	// 工具栏
 	themap.plugin(["AMap.ToolBar"], function() {
 		themap.addControl(new AMap.Scale());
        themap.addControl(new AMap.ToolBar());
    });




	debugger;
	//const canvasLayer = themap.getCanvasLayer();
	//canvasLayer.canvas.willReadFrequently = true;
	



	//计算所有视窗中心经纬度
	//getAllCenterLnglats([113.969492,22.613207], [114.102182,22.511500]);
	//输出瓦片左上右下经纬度
	//outPutRectLnglats();

	
	
	//地图加载完成
	themap.on('complete', function() {
		console.log("地图加载完成");
		//getAllImages();  
	});

	//注册事件，下载截图--------开启疯狂下载模式
	//themap.on('moveend', mapMoveend);
}




// 销毁地图；请尽量不要进行地图的重新创建和销毁，如果有图层的隐藏和显示需求，请使用图层的 show/hide 方法
function destroyMap() {
  themap && themap.destroy();
  console.log("地图已销毁");
}

//使用JSAPI加载器，可以避免异步加载、重复加载等常见错误加载错误
//需要引入<script src="https://webapi.amap.com/loader.js"></script>，不需要引入https://webapi.amap.com/maps
// AMapLoader.load({ //首次调用 load
//     key:'608d75903d29ad471362f8c58c550daf',//首次load key为必填
//     version:'2.0',
//     plugins:['AMap.Scale','AMap.ToolBar']
// }).then((AMap)=>{
//     map = new AMap.Map('mapContainer');
//     map.addControl(new AMap.Scale())
//     map.addControl(new AMap.ToolBar())
//     map.add(new AMap.Marker({
//         position:map.getCenter()
//     }));
// }).catch((e)=>{
//     console.error(e);
// });

// AMapLoader.load({ //可多次调用load
//     plugins:['AMap.MapType']
// }).then((AMap)=>{
//     map.addControl(new AMap.MapType())
// }).catch((e)=>{
//     console.error(e);
// });

