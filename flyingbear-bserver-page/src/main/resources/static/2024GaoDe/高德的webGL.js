(function() {
    "use strict";
    var extendStatics = function(e, r) {
        extendStatics = Object.setPrototypeOf || {
            __proto__: []
        }instanceof Array && function(e, r) {
            e.__proto__ = r
        }
        || function(e, r) {
            for (var t in r)
                if (r.hasOwnProperty(t))
                    e[t] = r[t]
        }
        ;
        return extendStatics(e, r)
    };
    function __extends(e, r) {
        extendStatics(e, r);
        function t() {
            this.constructor = e
        }
        e.prototype = r === null ? Object.create(r) : (t.prototype = r.prototype,
        new t)
    }
    var __assign = function() {
        __assign = Object.assign || function e(r) {
            for (var t, i = 1, a = arguments.length; i < a; i++) {
                t = arguments[i];
                for (var n in t)
                    if (Object.prototype.hasOwnProperty.call(t, n))
                        r[n] = t[n]
            }
            return r
        }
        ;
        return __assign.apply(this, arguments)
    };
    function __decorate(e, r, t, i) {
        var a = arguments.length, n = a < 3 ? r : i === null ? i = Object.getOwnPropertyDescriptor(r, t) : i, o;
        if (typeof Reflect === "object" && typeof Reflect.yV === "function")
            n = Reflect.yV(e, r, t, i);
        else
            for (var f = e.length - 1; f >= 0; f--)
                if (o = e[f])
                    n = (a < 3 ? o(n) : a > 3 ? o(r, t, n) : o(r, t)) || n;
        return a > 3 && n && Object.defineProperty(r, t, n),
        n
    }
    function __spreadArrays() {
        for (var e = 0, r = 0, t = arguments.length; r < t; r++)
            e += arguments[r].length;
        for (var i = Array(e), a = 0, r = 0; r < t; r++)
            for (var n = arguments[r], o = 0, f = n.length; o < f; o++,
            a++)
                i[a] = n[o];
        return i
    }
    var freeGlobal = typeof global == "object" && global && global.Object === Object && global;
    var freeSelf = typeof self == "object" && self && self.Object === Object && self;
    var root = freeGlobal || freeSelf || Function("return this")();
    var Symbol = root.Symbol;
    var objectProto = Object.prototype;
    var hasOwnProperty = objectProto.hasOwnProperty;
    var nativeObjectToString = objectProto.toString;
    var symToStringTag = Symbol ? Symbol.toStringTag : undefined;
    function getRawTag(e) {
        var r = hasOwnProperty.call(e, symToStringTag)
          , t = e[symToStringTag];
        try {
            e[symToStringTag] = undefined;
            var i = true
        } catch (e) {}
        var a = nativeObjectToString.call(e);
        if (i) {
            if (r) {
                e[symToStringTag] = t
            } else {
                delete e[symToStringTag]
            }
        }
        return a
    }
    var objectProto$1 = Object.prototype;
    var nativeObjectToString$1 = objectProto$1.toString;
    function objectToString(e) {
        return nativeObjectToString$1.call(e)
    }
    var nullTag = "[object Null]"
      , undefinedTag = "[object Undefined]";
    var symToStringTag$1 = Symbol ? Symbol.toStringTag : undefined;
    function baseGetTag(e) {
        if (e == null) {
            return e === undefined ? undefinedTag : nullTag
        }
        return symToStringTag$1 && symToStringTag$1 in Object(e) ? getRawTag(e) : objectToString(e)
    }
    function isObjectLike(e) {
        return e != null && typeof e == "object"
    }
    var symbolTag = "[object Symbol]";
    function isSymbol(e) {
        return typeof e == "symbol" || isObjectLike(e) && baseGetTag(e) == symbolTag
    }
    function arrayMap(e, r) {
        var t = -1
          , i = e == null ? 0 : e.length
          , a = Array(i);
        while (++t < i) {
            a[t] = r(e[t], t, e)
        }
        return a
    }
    var isArray = Array.isArray;
    var INFINITY = 1 / 0;
    var symbolProto = Symbol ? Symbol.prototype : undefined
      , symbolToString = symbolProto ? symbolProto.toString : undefined;
    function baseToString(e) {
        if (typeof e == "string") {
            return e
        }
        if (isArray(e)) {
            return arrayMap(e, baseToString) + ""
        }
        if (isSymbol(e)) {
            return symbolToString ? symbolToString.call(e) : ""
        }
        var r = e + "";
        return r == "0" && 1 / e == -INFINITY ? "-0" : r
    }
    var reWhitespace = /\s/;
    function trimmedEndIndex(e) {
        var r = e.length;
        while (r-- && reWhitespace.test(e.charAt(r))) {}
        return r
    }
    var reTrimStart = /^\s+/;
    function baseTrim(e) {
        return e ? e.slice(0, trimmedEndIndex(e) + 1).replace(reTrimStart, "") : e
    }
    function isObject(e) {
        var r = typeof e;
        return e != null && (r == "object" || r == "function")
    }
    var NAN = 0 / 0;
    var reIsBadHex = /^[-+]0x[0-9a-f]+$/i;
    var reIsBinary = /^0b[01]+$/i;
    var reIsOctal = /^0o[0-7]+$/i;
    var freeParseInt = parseInt;
    function toNumber(e) {
        if (typeof e == "number") {
            return e
        }
        if (isSymbol(e)) {
            return NAN
        }
        if (isObject(e)) {
            var r = typeof e.valueOf == "function" ? e.valueOf() : e;
            e = isObject(r) ? r + "" : r
        }
        if (typeof e != "string") {
            return e === 0 ? e : +e
        }
        e = baseTrim(e);
        var t = reIsBinary.test(e);
        return t || reIsOctal.test(e) ? freeParseInt(e.slice(2), t ? 2 : 8) : reIsBadHex.test(e) ? NAN : +e
    }
    var INFINITY$1 = 1 / 0
      , MAX_INTEGER = 17976931348623157e292;
    function toFinite(e) {
        if (!e) {
            return e === 0 ? e : 0
        }
        e = toNumber(e);
        if (e === INFINITY$1 || e === -INFINITY$1) {
            var r = e < 0 ? -1 : 1;
            return r * MAX_INTEGER
        }
        return e === e ? e : 0
    }
    function toInteger(e) {
        var r = toFinite(e)
          , t = r % 1;
        return r === r ? t ? r - t : r : 0
    }
    function identity(e) {
        return e
    }
    var asyncTag = "[object AsyncFunction]"
      , funcTag = "[object Function]"
      , genTag = "[object GeneratorFunction]"
      , proxyTag = "[object Proxy]";
    function isFunction(e) {
        if (!isObject(e)) {
            return false
        }
        var r = baseGetTag(e);
        return r == funcTag || r == genTag || r == asyncTag || r == proxyTag
    }
    var coreJsData = root["__core-js_shared__"];
    var maskSrcKey = function() {
        var e = /[^.]+$/.exec(coreJsData && coreJsData.keys && coreJsData.keys.Xb || "");
        return e ? "Symbol(src)_1." + e : ""
    }();
    function isMasked(e) {
        return !!maskSrcKey && maskSrcKey in e
    }
    var funcProto = Function.prototype;
    var funcToString = funcProto.toString;
    function toSource(e) {
        if (e != null) {
            try {
                return funcToString.call(e)
            } catch (e) {}
            try {
                return e + ""
            } catch (e) {}
        }
        return ""
    }
    var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
    var reIsHostCtor = /^\[object .+?Constructor\]$/;
    var funcProto$1 = Function.prototype
      , objectProto$2 = Object.prototype;
    var funcToString$1 = funcProto$1.toString;
    var hasOwnProperty$1 = objectProto$2.hasOwnProperty;
    var reIsNative = RegExp("^" + funcToString$1.call(hasOwnProperty$1).replace(reRegExpChar, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
    function baseIsNative(e) {
        if (!isObject(e) || isMasked(e)) {
            return false
        }
        var r = isFunction(e) ? reIsNative : reIsHostCtor;
        return r.test(toSource(e))
    }
    function getValue(e, r) {
        return e == null ? undefined : e[r]
    }
    function getNative(e, r) {
        var t = getValue(e, r);
        return baseIsNative(t) ? t : undefined
    }
    var WeakMap = getNative(root, "WeakMap");
    function apply(e, r, t) {
        switch (t.length) {
        case 0:
            return e.call(r);
        case 1:
            return e.call(r, t[0]);
        case 2:
            return e.call(r, t[0], t[1]);
        case 3:
            return e.call(r, t[0], t[1], t[2])
        }
        return e.apply(r, t)
    }
    var HOT_COUNT = 800
      , HOT_SPAN = 16;
    var nativeNow = Date.now;
    function shortOut(t) {
        var i = 0
          , a = 0;
        return function() {
            var e = nativeNow()
              , r = HOT_SPAN - (e - a);
            a = e;
            if (r > 0) {
                if (++i >= HOT_COUNT) {
                    return arguments[0]
                }
            } else {
                i = 0
            }
            return t.apply(undefined, arguments)
        }
    }
    function constant(e) {
        return function() {
            return e
        }
    }
    var defineProperty = function() {
        try {
            var e = getNative(Object, "defineProperty");
            e({}, "", {});
            return e
        } catch (e) {}
    }();
    var baseSetToString = !defineProperty ? identity : function(e, r) {
        return defineProperty(e, "toString", {
            configurable: true,
            enumerable: false,
            value: constant(r),
            writable: true
        })
    }
    ;
    var setToString = shortOut(baseSetToString);
    function arrayEach(e, r) {
        var t = -1
          , i = e == null ? 0 : e.length;
        while (++t < i) {
            if (r(e[t], t, e) === false) {
                break
            }
        }
        return e
    }
    function baseFindIndex(e, r, t, i) {
        var a = e.length
          , n = t + (i ? 1 : -1);
        while (i ? n-- : ++n < a) {
            if (r(e[n], n, e)) {
                return n
            }
        }
        return -1
    }
    var MAX_SAFE_INTEGER = 9007199254740991;
    var reIsUint = /^(?:0|[1-9]\d*)$/;
    function isIndex(e, r) {
        var t = typeof e;
        r = r == null ? MAX_SAFE_INTEGER : r;
        return !!r && (t == "number" || t != "symbol" && reIsUint.test(e)) && (e > -1 && e % 1 == 0 && e < r)
    }
    function baseAssignValue(e, r, t) {
        if (r == "__proto__" && defineProperty) {
            defineProperty(e, r, {
                configurable: true,
                enumerable: true,
                value: t,
                writable: true
            })
        } else {
            e[r] = t
        }
    }
    function eq(e, r) {
        return e === r || e !== e && r !== r
    }
    var objectProto$3 = Object.prototype;
    var hasOwnProperty$2 = objectProto$3.hasOwnProperty;
    function assignValue(e, r, t) {
        var i = e[r];
        if (!(hasOwnProperty$2.call(e, r) && eq(i, t)) || t === undefined && !(r in e)) {
            baseAssignValue(e, r, t)
        }
    }
    function copyObject(e, r, t, i) {
        var a = !t;
        t || (t = {});
        var n = -1
          , o = r.length;
        while (++n < o) {
            var f = r[n];
            var s = i ? i(t[f], e[f], f, t, e) : undefined;
            if (s === undefined) {
                s = e[f]
            }
            if (a) {
                baseAssignValue(t, f, s)
            } else {
                assignValue(t, f, s)
            }
        }
        return t
    }
    var nativeMax = Math.max;
    function overRest(n, o, f) {
        o = nativeMax(o === undefined ? n.length - 1 : o, 0);
        return function() {
            var e = arguments
              , r = -1
              , t = nativeMax(e.length - o, 0)
              , i = Array(t);
            while (++r < t) {
                i[r] = e[o + r]
            }
            r = -1;
            var a = Array(o + 1);
            while (++r < o) {
                a[r] = e[r]
            }
            a[o] = f(i);
            return apply(n, this, a)
        }
    }
    function baseRest(e, r) {
        return setToString(overRest(e, r, identity), e + "")
    }
    var MAX_SAFE_INTEGER$1 = 9007199254740991;
    function isLength(e) {
        return typeof e == "number" && e > -1 && e % 1 == 0 && e <= MAX_SAFE_INTEGER$1
    }
    function isArrayLike(e) {
        return e != null && isLength(e.length) && !isFunction(e)
    }
    function isIterateeCall(e, r, t) {
        if (!isObject(t)) {
            return false
        }
        var i = typeof r;
        if (i == "number" ? isArrayLike(t) && isIndex(r, t.length) : i == "string" && r in t) {
            return eq(t[r], e)
        }
        return false
    }
    function createAssigner(f) {
        return baseRest(function(e, r) {
            var t = -1
              , i = r.length
              , a = i > 1 ? r[i - 1] : undefined
              , n = i > 2 ? r[2] : undefined;
            a = f.length > 3 && typeof a == "function" ? (i--,
            a) : undefined;
            if (n && isIterateeCall(r[0], r[1], n)) {
                a = i < 3 ? undefined : a;
                i = 1
            }
            e = Object(e);
            while (++t < i) {
                var o = r[t];
                if (o) {
                    f(e, o, t, a)
                }
            }
            return e
        })
    }
    var objectProto$4 = Object.prototype;
    function isPrototype(e) {
        var r = e && e.constructor
          , t = typeof r == "function" && r.prototype || objectProto$4;
        return e === t
    }
    function baseTimes(e, r) {
        var t = -1
          , i = Array(e);
        while (++t < e) {
            i[t] = r(t)
        }
        return i
    }
    var argsTag = "[object Arguments]";
    function baseIsArguments(e) {
        return isObjectLike(e) && baseGetTag(e) == argsTag
    }
    var objectProto$5 = Object.prototype;
    var hasOwnProperty$3 = objectProto$5.hasOwnProperty;
    var propertyIsEnumerable = objectProto$5.propertyIsEnumerable;
    var isArguments = baseIsArguments(function() {
        return arguments
    }()) ? baseIsArguments : function(e) {
        return isObjectLike(e) && hasOwnProperty$3.call(e, "callee") && !propertyIsEnumerable.call(e, "callee")
    }
    ;
    function stubFalse() {
        return false
    }
    var freeExports = typeof exports == "object" && exports && !exports.nodeType && exports;
    var freeModule = freeExports && typeof module == "object" && module && !module.nodeType && module;
    var moduleExports = freeModule && freeModule.exports === freeExports;
    var Buffer = moduleExports ? root.Zb : undefined;
    var nativeIsBuffer = Buffer ? Buffer.isBuffer : undefined;
    var isBuffer = nativeIsBuffer || stubFalse;
    var argsTag$1 = "[object Arguments]"
      , arrayTag = "[object Array]"
      , boolTag = "[object Boolean]"
      , dateTag = "[object Date]"
      , errorTag = "[object Error]"
      , funcTag$1 = "[object Function]"
      , mapTag = "[object Map]"
      , numberTag = "[object Number]"
      , objectTag = "[object Object]"
      , regexpTag = "[object RegExp]"
      , setTag = "[object Set]"
      , stringTag = "[object String]"
      , weakMapTag = "[object WeakMap]";
    var arrayBufferTag = "[object ArrayBuffer]"
      , dataViewTag = "[object DataView]"
      , float32Tag = "[object Float32Array]"
      , float64Tag = "[object Float64Array]"
      , int8Tag = "[object Int8Array]"
      , int16Tag = "[object Int16Array]"
      , int32Tag = "[object Int32Array]"
      , uint8Tag = "[object Uint8Array]"
      , uint8ClampedTag = "[object Uint8ClampedArray]"
      , uint16Tag = "[object Uint16Array]"
      , uint32Tag = "[object Uint32Array]";
    var typedArrayTags = {};
    typedArrayTags[float32Tag] = typedArrayTags[float64Tag] = typedArrayTags[int8Tag] = typedArrayTags[int16Tag] = typedArrayTags[int32Tag] = typedArrayTags[uint8Tag] = typedArrayTags[uint8ClampedTag] = typedArrayTags[uint16Tag] = typedArrayTags[uint32Tag] = true;
    typedArrayTags[argsTag$1] = typedArrayTags[arrayTag] = typedArrayTags[arrayBufferTag] = typedArrayTags[boolTag] = typedArrayTags[dataViewTag] = typedArrayTags[dateTag] = typedArrayTags[errorTag] = typedArrayTags[funcTag$1] = typedArrayTags[mapTag] = typedArrayTags[numberTag] = typedArrayTags[objectTag] = typedArrayTags[regexpTag] = typedArrayTags[setTag] = typedArrayTags[stringTag] = typedArrayTags[weakMapTag] = false;
    function baseIsTypedArray(e) {
        return isObjectLike(e) && isLength(e.length) && !!typedArrayTags[baseGetTag(e)]
    }
    function baseUnary(r) {
        return function(e) {
            return r(e)
        }
    }
    var freeExports$1 = typeof exports == "object" && exports && !exports.nodeType && exports;
    var freeModule$1 = freeExports$1 && typeof module == "object" && module && !module.nodeType && module;
    var moduleExports$1 = freeModule$1 && freeModule$1.exports === freeExports$1;
    var freeProcess = moduleExports$1 && freeGlobal.process;
    var nodeUtil = function() {
        try {
            var e = freeModule$1 && freeModule$1.Qb && freeModule$1.Qb("util").types;
            if (e) {
                return e
            }
            return freeProcess && freeProcess.tg && freeProcess.tg("util")
        } catch (e) {}
    }();
    var nodeIsTypedArray = nodeUtil && nodeUtil.rg;
    var isTypedArray = nodeIsTypedArray ? baseUnary(nodeIsTypedArray) : baseIsTypedArray;
    var objectProto$6 = Object.prototype;
    var hasOwnProperty$4 = objectProto$6.hasOwnProperty;
    function arrayLikeKeys(e, r) {
        var t = isArray(e)
          , i = !t && isArguments(e)
          , a = !t && !i && isBuffer(e)
          , n = !t && !i && !a && isTypedArray(e)
          , o = t || i || a || n
          , f = o ? baseTimes(e.length, String) : []
          , s = f.length;
        for (var u in e) {
            if ((r || hasOwnProperty$4.call(e, u)) && !(o && (u == "length" || a && (u == "offset" || u == "parent") || n && (u == "buffer" || u == "byteLength" || u == "byteOffset") || isIndex(u, s)))) {
                f.push(u)
            }
        }
        return f
    }
    function overArg(r, t) {
        return function(e) {
            return r(t(e))
        }
    }
    var nativeKeys = overArg(Object.keys, Object);
    var objectProto$7 = Object.prototype;
    var hasOwnProperty$5 = objectProto$7.hasOwnProperty;
    function baseKeys(e) {
        if (!isPrototype(e)) {
            return nativeKeys(e)
        }
        var r = [];
        for (var t in Object(e)) {
            if (hasOwnProperty$5.call(e, t) && t != "constructor") {
                r.push(t)
            }
        }
        return r
    }
    function keys(e) {
        return isArrayLike(e) ? arrayLikeKeys(e) : baseKeys(e)
    }
    var objectProto$8 = Object.prototype;
    var hasOwnProperty$6 = objectProto$8.hasOwnProperty;
    var assign = createAssigner(function(e, r) {
        if (isPrototype(r) || isArrayLike(r)) {
            copyObject(r, keys(r), e);
            return
        }
        for (var t in r) {
            if (hasOwnProperty$6.call(r, t)) {
                assignValue(e, t, r[t])
            }
        }
    });
    var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/
      , reIsPlainProp = /^\w*$/;
    function isKey(e, r) {
        if (isArray(e)) {
            return false
        }
        var t = typeof e;
        if (t == "number" || t == "symbol" || t == "boolean" || e == null || isSymbol(e)) {
            return true
        }
        return reIsPlainProp.test(e) || !reIsDeepProp.test(e) || r != null && e in Object(r)
    }
    var nativeCreate = getNative(Object, "create");
    function hashClear() {
        this.ng = nativeCreate ? nativeCreate(null) : {};
        this.size = 0
    }
    function hashDelete(e) {
        var r = this.has(e) && delete this.ng[e];
        this.size -= r ? 1 : 0;
        return r
    }
    var HASH_UNDEFINED = "__lodash_hash_undefined__";
    var objectProto$9 = Object.prototype;
    var hasOwnProperty$7 = objectProto$9.hasOwnProperty;
    function hashGet(e) {
        var r = this.ng;
        if (nativeCreate) {
            var t = r[e];
            return t === HASH_UNDEFINED ? undefined : t
        }
        return hasOwnProperty$7.call(r, e) ? r[e] : undefined
    }
    var objectProto$a = Object.prototype;
    var hasOwnProperty$8 = objectProto$a.hasOwnProperty;
    function hashHas(e) {
        var r = this.ng;
        return nativeCreate ? r[e] !== undefined : hasOwnProperty$8.call(r, e)
    }
    var HASH_UNDEFINED$1 = "__lodash_hash_undefined__";
    function hashSet(e, r) {
        var t = this.ng;
        this.size += this.has(e) ? 0 : 1;
        t[e] = nativeCreate && r === undefined ? HASH_UNDEFINED$1 : r;
        return this
    }
    function Hash(e) {
        var r = -1
          , t = e == null ? 0 : e.length;
        this.clear();
        while (++r < t) {
            var i = e[r];
            this.set(i[0], i[1])
        }
    }
    Hash.prototype.clear = hashClear;
    Hash.prototype["delete"] = hashDelete;
    Hash.prototype.get = hashGet;
    Hash.prototype.has = hashHas;
    Hash.prototype.set = hashSet;
    function listCacheClear() {
        this.ng = [];
        this.size = 0
    }
    function assocIndexOf(e, r) {
        var t = e.length;
        while (t--) {
            if (eq(e[t][0], r)) {
                return t
            }
        }
        return -1
    }
    var arrayProto = Array.prototype;
    var splice = arrayProto.splice;
    function listCacheDelete(e) {
        var r = this.ng
          , t = assocIndexOf(r, e);
        if (t < 0) {
            return false
        }
        var i = r.length - 1;
        if (t == i) {
            r.pop()
        } else {
            splice.call(r, t, 1)
        }
        --this.size;
        return true
    }
    function listCacheGet(e) {
        var r = this.ng
          , t = assocIndexOf(r, e);
        return t < 0 ? undefined : r[t][1]
    }
    function listCacheHas(e) {
        return assocIndexOf(this.ng, e) > -1
    }
    function listCacheSet(e, r) {
        var t = this.ng
          , i = assocIndexOf(t, e);
        if (i < 0) {
            ++this.size;
            t.push([e, r])
        } else {
            t[i][1] = r
        }
        return this
    }
    function ListCache(e) {
        var r = -1
          , t = e == null ? 0 : e.length;
        this.clear();
        while (++r < t) {
            var i = e[r];
            this.set(i[0], i[1])
        }
    }
    ListCache.prototype.clear = listCacheClear;
    ListCache.prototype["delete"] = listCacheDelete;
    ListCache.prototype.get = listCacheGet;
    ListCache.prototype.has = listCacheHas;
    ListCache.prototype.set = listCacheSet;
    var Map = getNative(root, "Map");
    function mapCacheClear() {
        this.size = 0;
        this.ng = {
            hash: new Hash,
            map: new (Map || ListCache),
            string: new Hash
        }
    }
    function isKeyable(e) {
        var r = typeof e;
        return r == "string" || r == "number" || r == "symbol" || r == "boolean" ? e !== "__proto__" : e === null
    }
    function getMapData(e, r) {
        var t = e.ng;
        return isKeyable(r) ? t[typeof r == "string" ? "string" : "hash"] : t.map
    }
    function mapCacheDelete(e) {
        var r = getMapData(this, e)["delete"](e);
        this.size -= r ? 1 : 0;
        return r
    }
    function mapCacheGet(e) {
        return getMapData(this, e).get(e)
    }
    function mapCacheHas(e) {
        return getMapData(this, e).has(e)
    }
    function mapCacheSet(e, r) {
        var t = getMapData(this, e)
          , i = t.size;
        t.set(e, r);
        this.size += t.size == i ? 0 : 1;
        return this
    }
    function MapCache(e) {
        var r = -1
          , t = e == null ? 0 : e.length;
        this.clear();
        while (++r < t) {
            var i = e[r];
            this.set(i[0], i[1])
        }
    }
    MapCache.prototype.clear = mapCacheClear;
    MapCache.prototype["delete"] = mapCacheDelete;
    MapCache.prototype.get = mapCacheGet;
    MapCache.prototype.has = mapCacheHas;
    MapCache.prototype.set = mapCacheSet;
    var FUNC_ERROR_TEXT = "Expected a function";
    function memoize(a, n) {
        if (typeof a != "function" || n != null && typeof n != "function") {
            throw new TypeError(FUNC_ERROR_TEXT)
        }
        var o = function() {
            var e = arguments
              , r = n ? n.apply(this, e) : e[0]
              , t = o.cache;
            if (t.has(r)) {
                return t.get(r)
            }
            var i = a.apply(this, e);
            o.cache = t.set(r, i) || t;
            return i
        };
        o.cache = new (memoize.Cache || MapCache);
        return o
    }
    memoize.Cache = MapCache;
    var MAX_MEMOIZE_SIZE = 500;
    function memoizeCapped(e) {
        var r = memoize(e, function(e) {
            if (t.size === MAX_MEMOIZE_SIZE) {
                t.clear()
            }
            return e
        });
        var t = r.cache;
        return r
    }
    var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;
    var reEscapeChar = /\\(\\)?/g;
    var stringToPath = memoizeCapped(function(e) {
        var a = [];
        if (e.charCodeAt(0) === 46) {
            a.push("")
        }
        e.replace(rePropName, function(e, r, t, i) {
            a.push(t ? i.replace(reEscapeChar, "$1") : r || e)
        });
        return a
    });
    function toString(e) {
        return e == null ? "" : baseToString(e)
    }
    function castPath(e, r) {
        if (isArray(e)) {
            return e
        }
        return isKey(e, r) ? [e] : stringToPath(toString(e))
    }
    var INFINITY$2 = 1 / 0;
    function toKey(e) {
        if (typeof e == "string" || isSymbol(e)) {
            return e
        }
        var r = e + "";
        return r == "0" && 1 / e == -INFINITY$2 ? "-0" : r
    }
    function baseGet(e, r) {
        r = castPath(r, e);
        var t = 0
          , i = r.length;
        while (e != null && t < i) {
            e = e[toKey(r[t++])]
        }
        return t && t == i ? e : undefined
    }
    function get(e, r, t) {
        var i = e == null ? undefined : baseGet(e, r);
        return i === undefined ? t : i
    }
    function arrayPush(e, r) {
        var t = -1
          , i = r.length
          , a = e.length;
        while (++t < i) {
            e[a + t] = r[t]
        }
        return e
    }
    function stackClear() {
        this.ng = new ListCache;
        this.size = 0
    }
    function stackDelete(e) {
        var r = this.ng
          , t = r["delete"](e);
        this.size = r.size;
        return t
    }
    function stackGet(e) {
        return this.ng.get(e)
    }
    function stackHas(e) {
        return this.ng.has(e)
    }
    var LARGE_ARRAY_SIZE = 200;
    function stackSet(e, r) {
        var t = this.ng;
        if (t instanceof ListCache) {
            var i = t.ng;
            if (!Map || i.length < LARGE_ARRAY_SIZE - 1) {
                i.push([e, r]);
                this.size = ++t.size;
                return this
            }
            t = this.ng = new MapCache(i)
        }
        t.set(e, r);
        this.size = t.size;
        return this
    }
    function Stack(e) {
        var r = this.ng = new ListCache(e);
        this.size = r.size
    }
    Stack.prototype.clear = stackClear;
    Stack.prototype["delete"] = stackDelete;
    Stack.prototype.get = stackGet;
    Stack.prototype.has = stackHas;
    Stack.prototype.set = stackSet;
    function arrayFilter(e, r) {
        var t = -1
          , i = e == null ? 0 : e.length
          , a = 0
          , n = [];
        while (++t < i) {
            var o = e[t];
            if (r(o, t, e)) {
                n[a++] = o
            }
        }
        return n
    }
    function stubArray() {
        return []
    }
    var objectProto$b = Object.prototype;
    var propertyIsEnumerable$1 = objectProto$b.propertyIsEnumerable;
    var nativeGetSymbols = Object.getOwnPropertySymbols;
    var getSymbols = !nativeGetSymbols ? stubArray : function(r) {
        if (r == null) {
            return []
        }
        r = Object(r);
        return arrayFilter(nativeGetSymbols(r), function(e) {
            return propertyIsEnumerable$1.call(r, e)
        })
    }
    ;
    function baseGetAllKeys(e, r, t) {
        var i = r(e);
        return isArray(e) ? i : arrayPush(i, t(e))
    }
    function getAllKeys(e) {
        return baseGetAllKeys(e, keys, getSymbols)
    }
    var DataView = getNative(root, "DataView");
    var Promise$1 = getNative(root, "Promise");
    var Set = getNative(root, "Set");
    var mapTag$1 = "[object Map]"
      , objectTag$1 = "[object Object]"
      , promiseTag = "[object Promise]"
      , setTag$1 = "[object Set]"
      , weakMapTag$1 = "[object WeakMap]";
    var dataViewTag$1 = "[object DataView]";
    var dataViewCtorString = toSource(DataView)
      , mapCtorString = toSource(Map)
      , promiseCtorString = toSource(Promise$1)
      , setCtorString = toSource(Set)
      , weakMapCtorString = toSource(WeakMap);
    var getTag = baseGetTag;
    if (DataView && getTag(new DataView(new ArrayBuffer(1))) != dataViewTag$1 || Map && getTag(new Map) != mapTag$1 || Promise$1 && getTag(Promise$1.resolve()) != promiseTag || Set && getTag(new Set) != setTag$1 || WeakMap && getTag(new WeakMap) != weakMapTag$1) {
        getTag = function(e) {
            var r = baseGetTag(e)
              , t = r == objectTag$1 ? e.constructor : undefined
              , i = t ? toSource(t) : "";
            if (i) {
                switch (i) {
                case dataViewCtorString:
                    return dataViewTag$1;
                case mapCtorString:
                    return mapTag$1;
                case promiseCtorString:
                    return promiseTag;
                case setCtorString:
                    return setTag$1;
                case weakMapCtorString:
                    return weakMapTag$1
                }
            }
            return r
        }
    }
    var getTag$1 = getTag;
    var Uint8Array$1 = root.Uint8Array;
    var HASH_UNDEFINED$2 = "__lodash_hash_undefined__";
    function setCacheAdd(e) {
        this.ng.set(e, HASH_UNDEFINED$2);
        return this
    }
    function setCacheHas(e) {
        return this.ng.has(e)
    }
    function SetCache(e) {
        var r = -1
          , t = e == null ? 0 : e.length;
        this.ng = new MapCache;
        while (++r < t) {
            this.add(e[r])
        }
    }
    SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
    SetCache.prototype.has = setCacheHas;
    function arraySome(e, r) {
        var t = -1
          , i = e == null ? 0 : e.length;
        while (++t < i) {
            if (r(e[t], t, e)) {
                return true
            }
        }
        return false
    }
    function cacheHas(e, r) {
        return e.has(r)
    }
    var COMPARE_PARTIAL_FLAG = 1
      , COMPARE_UNORDERED_FLAG = 2;
    function equalArrays(e, r, t, i, a, n) {
        var o = t & COMPARE_PARTIAL_FLAG
          , f = e.length
          , s = r.length;
        if (f != s && !(o && s > f)) {
            return false
        }
        var u = n.get(e);
        var l = n.get(r);
        if (u && l) {
            return u == r && l == e
        }
        var v = -1
          , c = true
          , h = t & COMPARE_UNORDERED_FLAG ? new SetCache : undefined;
        n.set(e, r);
        n.set(r, e);
        while (++v < f) {
            var d = e[v]
              , _ = r[v];
            if (i) {
                var g = o ? i(_, d, v, r, e, n) : i(d, _, v, e, r, n)
            }
            if (g !== undefined) {
                if (g) {
                    continue
                }
                c = false;
                break
            }
            if (h) {
                if (!arraySome(r, function(e, r) {
                    if (!cacheHas(h, r) && (d === e || a(d, e, t, i, n))) {
                        return h.push(r)
                    }
                })) {
                    c = false;
                    break
                }
            } else if (!(d === _ || a(d, _, t, i, n))) {
                c = false;
                break
            }
        }
        n["delete"](e);
        n["delete"](r);
        return c
    }
    function mapToArray(e) {
        var t = -1
          , i = Array(e.size);
        e.forEach(function(e, r) {
            i[++t] = [r, e]
        });
        return i
    }
    function setToArray(e) {
        var r = -1
          , t = Array(e.size);
        e.forEach(function(e) {
            t[++r] = e
        });
        return t
    }
    var COMPARE_PARTIAL_FLAG$1 = 1
      , COMPARE_UNORDERED_FLAG$1 = 2;
    var boolTag$1 = "[object Boolean]"
      , dateTag$1 = "[object Date]"
      , errorTag$1 = "[object Error]"
      , mapTag$2 = "[object Map]"
      , numberTag$1 = "[object Number]"
      , regexpTag$1 = "[object RegExp]"
      , setTag$2 = "[object Set]"
      , stringTag$1 = "[object String]"
      , symbolTag$1 = "[object Symbol]";
    var arrayBufferTag$1 = "[object ArrayBuffer]"
      , dataViewTag$2 = "[object DataView]";
    var symbolProto$1 = Symbol ? Symbol.prototype : undefined
      , symbolValueOf = symbolProto$1 ? symbolProto$1.valueOf : undefined;
    function equalByTag(e, r, t, i, a, n, o) {
        switch (t) {
        case dataViewTag$2:
            if (e.byteLength != r.byteLength || e.byteOffset != r.byteOffset) {
                return false
            }
            e = e.buffer;
            r = r.buffer;
        case arrayBufferTag$1:
            if (e.byteLength != r.byteLength || !n(new Uint8Array$1(e), new Uint8Array$1(r))) {
                return false
            }
            return true;
        case boolTag$1:
        case dateTag$1:
        case numberTag$1:
            return eq(+e, +r);
        case errorTag$1:
            return e.name == r.name && e.message == r.message;
        case regexpTag$1:
        case stringTag$1:
            return e == r + "";
        case mapTag$2:
            var f = mapToArray;
        case setTag$2:
            var s = i & COMPARE_PARTIAL_FLAG$1;
            f || (f = setToArray);
            if (e.size != r.size && !s) {
                return false
            }
            var u = o.get(e);
            if (u) {
                return u == r
            }
            i |= COMPARE_UNORDERED_FLAG$1;
            o.set(e, r);
            var l = equalArrays(f(e), f(r), i, a, n, o);
            o["delete"](e);
            return l;
        case symbolTag$1:
            if (symbolValueOf) {
                return symbolValueOf.call(e) == symbolValueOf.call(r)
            }
        }
        return false
    }
    var COMPARE_PARTIAL_FLAG$2 = 1;
    var objectProto$c = Object.prototype;
    var hasOwnProperty$9 = objectProto$c.hasOwnProperty;
    function equalObjects(e, r, t, i, a, n) {
        var o = t & COMPARE_PARTIAL_FLAG$2
          , f = getAllKeys(e)
          , s = f.length
          , u = getAllKeys(r)
          , l = u.length;
        if (s != l && !o) {
            return false
        }
        var v = s;
        while (v--) {
            var c = f[v];
            if (!(o ? c in r : hasOwnProperty$9.call(r, c))) {
                return false
            }
        }
        var h = n.get(e);
        var d = n.get(r);
        if (h && d) {
            return h == r && d == e
        }
        var _ = true;
        n.set(e, r);
        n.set(r, e);
        var g = o;
        while (++v < s) {
            c = f[v];
            var y = e[c]
              , m = r[c];
            if (i) {
                var p = o ? i(m, y, c, r, e, n) : i(y, m, c, e, r, n)
            }
            if (!(p === undefined ? y === m || a(y, m, t, i, n) : p)) {
                _ = false;
                break
            }
            g || (g = c == "constructor")
        }
        if (_ && !g) {
            var b = e.constructor
              , x = r.constructor;
            if (b != x && ("constructor"in e && "constructor"in r) && !(typeof b == "function" && b instanceof b && typeof x == "function" && x instanceof x)) {
                _ = false
            }
        }
        n["delete"](e);
        n["delete"](r);
        return _
    }
    var COMPARE_PARTIAL_FLAG$3 = 1;
    var argsTag$2 = "[object Arguments]"
      , arrayTag$1 = "[object Array]"
      , objectTag$2 = "[object Object]";
    var objectProto$d = Object.prototype;
    var hasOwnProperty$a = objectProto$d.hasOwnProperty;
    function baseIsEqualDeep(e, r, t, i, a, n) {
        var o = isArray(e)
          , f = isArray(r)
          , s = o ? arrayTag$1 : getTag$1(e)
          , u = f ? arrayTag$1 : getTag$1(r);
        s = s == argsTag$2 ? objectTag$2 : s;
        u = u == argsTag$2 ? objectTag$2 : u;
        var l = s == objectTag$2
          , v = u == objectTag$2
          , c = s == u;
        if (c && isBuffer(e)) {
            if (!isBuffer(r)) {
                return false
            }
            o = true;
            l = false
        }
        if (c && !l) {
            n || (n = new Stack);
            return o || isTypedArray(e) ? equalArrays(e, r, t, i, a, n) : equalByTag(e, r, s, t, i, a, n)
        }
        if (!(t & COMPARE_PARTIAL_FLAG$3)) {
            var h = l && hasOwnProperty$a.call(e, "__wrapped__")
              , d = v && hasOwnProperty$a.call(r, "__wrapped__");
            if (h || d) {
                var _ = h ? e.value() : e
                  , g = d ? r.value() : r;
                n || (n = new Stack);
                return a(_, g, t, i, n)
            }
        }
        if (!c) {
            return false
        }
        n || (n = new Stack);
        return equalObjects(e, r, t, i, a, n)
    }
    function baseIsEqual(e, r, t, i, a) {
        if (e === r) {
            return true
        }
        if (e == null || r == null || !isObjectLike(e) && !isObjectLike(r)) {
            return e !== e && r !== r
        }
        return baseIsEqualDeep(e, r, t, i, baseIsEqual, a)
    }
    var COMPARE_PARTIAL_FLAG$4 = 1
      , COMPARE_UNORDERED_FLAG$2 = 2;
    function baseIsMatch(e, r, t, i) {
        var a = t.length
          , n = a
          , o = !i;
        if (e == null) {
            return !n
        }
        e = Object(e);
        while (a--) {
            var f = t[a];
            if (o && f[2] ? f[1] !== e[f[0]] : !(f[0]in e)) {
                return false
            }
        }
        while (++a < n) {
            f = t[a];
            var s = f[0]
              , u = e[s]
              , l = f[1];
            if (o && f[2]) {
                if (u === undefined && !(s in e)) {
                    return false
                }
            } else {
                var v = new Stack;
                if (i) {
                    var c = i(u, l, s, e, r, v)
                }
                if (!(c === undefined ? baseIsEqual(l, u, COMPARE_PARTIAL_FLAG$4 | COMPARE_UNORDERED_FLAG$2, i, v) : c)) {
                    return false
                }
            }
        }
        return true
    }
    function isStrictComparable(e) {
        return e === e && !isObject(e)
    }
    function getMatchData(e) {
        var r = keys(e)
          , t = r.length;
        while (t--) {
            var i = r[t]
              , a = e[i];
            r[t] = [i, a, isStrictComparable(a)]
        }
        return r
    }
    function matchesStrictComparable(r, t) {
        return function(e) {
            if (e == null) {
                return false
            }
            return e[r] === t && (t !== undefined || r in Object(e))
        }
    }
    function baseMatches(r) {
        var t = getMatchData(r);
        if (t.length == 1 && t[0][2]) {
            return matchesStrictComparable(t[0][0], t[0][1])
        }
        return function(e) {
            return e === r || baseIsMatch(e, r, t)
        }
    }
    function baseHasIn(e, r) {
        return e != null && r in Object(e)
    }
    function hasPath(e, r, t) {
        r = castPath(r, e);
        var i = -1
          , a = r.length
          , n = false;
        while (++i < a) {
            var o = toKey(r[i]);
            if (!(n = e != null && t(e, o))) {
                break
            }
            e = e[o]
        }
        if (n || ++i != a) {
            return n
        }
        a = e == null ? 0 : e.length;
        return !!a && isLength(a) && isIndex(o, a) && (isArray(e) || isArguments(e))
    }
    function hasIn(e, r) {
        return e != null && hasPath(e, r, baseHasIn)
    }
    var COMPARE_PARTIAL_FLAG$5 = 1
      , COMPARE_UNORDERED_FLAG$3 = 2;
    function baseMatchesProperty(t, i) {
        if (isKey(t) && isStrictComparable(i)) {
            return matchesStrictComparable(toKey(t), i)
        }
        return function(e) {
            var r = get(e, t);
            return r === undefined && r === i ? hasIn(e, t) : baseIsEqual(i, r, COMPARE_PARTIAL_FLAG$5 | COMPARE_UNORDERED_FLAG$3)
        }
    }
    function baseProperty(r) {
        return function(e) {
            return e == null ? undefined : e[r]
        }
    }
    function basePropertyDeep(r) {
        return function(e) {
            return baseGet(e, r)
        }
    }
    function property(e) {
        return isKey(e) ? baseProperty(toKey(e)) : basePropertyDeep(e)
    }
    function baseIteratee(e) {
        if (typeof e == "function") {
            return e
        }
        if (e == null) {
            return identity
        }
        if (typeof e == "object") {
            return isArray(e) ? baseMatchesProperty(e[0], e[1]) : baseMatches(e)
        }
        return property(e)
    }
    function createBaseFor(s) {
        return function(e, r, t) {
            var i = -1
              , a = Object(e)
              , n = t(e)
              , o = n.length;
            while (o--) {
                var f = n[s ? o : ++i];
                if (r(a[f], f, a) === false) {
                    break
                }
            }
            return e
        }
    }
    var baseFor = createBaseFor();
    function baseForOwn(e, r) {
        return e && baseFor(e, r, keys)
    }
    function createBaseEach(n, o) {
        return function(e, r) {
            if (e == null) {
                return e
            }
            if (!isArrayLike(e)) {
                return n(e, r)
            }
            var t = e.length
              , i = o ? t : -1
              , a = Object(e);
            while (o ? i-- : ++i < t) {
                if (r(a[i], i, a) === false) {
                    break
                }
            }
            return e
        }
    }
    var baseEach = createBaseEach(baseForOwn);
    function castFunction(e) {
        return typeof e == "function" ? e : identity
    }
    function forEach(e, r) {
        var t = isArray(e) ? arrayEach : baseEach;
        return t(e, castFunction(r))
    }
    function arrayEvery(e, r) {
        var t = -1
          , i = e == null ? 0 : e.length;
        while (++t < i) {
            if (!r(e[t], t, e)) {
                return false
            }
        }
        return true
    }
    function baseEvery(e, i) {
        var a = true;
        baseEach(e, function(e, r, t) {
            a = !!i(e, r, t);
            return a
        });
        return a
    }
    function every(e, r, t) {
        var i = isArray(e) ? arrayEvery : baseEvery;
        if (t && isIterateeCall(e, r, t)) {
            r = undefined
        }
        return i(e, baseIteratee(r))
    }
    function createFind(o) {
        return function(e, r, t) {
            var i = Object(e);
            if (!isArrayLike(e)) {
                var a = baseIteratee(r);
                e = keys(e);
                r = function(e) {
                    return a(i[e], e, i)
                }
            }
            var n = o(e, r, t);
            return n > -1 ? i[a ? e[n] : n] : undefined
        }
    }
    var nativeMax$1 = Math.max;
    function findIndex(e, r, t) {
        var i = e == null ? 0 : e.length;
        if (!i) {
            return -1
        }
        var a = t == null ? 0 : toInteger(t);
        if (a < 0) {
            a = nativeMax$1(i + a, 0)
        }
        return baseFindIndex(e, baseIteratee(r), a)
    }
    var find = createFind(findIndex);
    function baseMap(e, i) {
        var a = -1
          , n = isArrayLike(e) ? Array(e.length) : [];
        baseEach(e, function(e, r, t) {
            n[++a] = i(e, r, t)
        });
        return n
    }
    function map(e, r) {
        var t = isArray(e) ? arrayMap : baseMap;
        return t(e, baseIteratee(r))
    }
    function isEqual(e, r) {
        return baseIsEqual(e, r)
    }
    function isUndefined(e) {
        return e === undefined
    }
    function baseSome(e, i) {
        var a;
        baseEach(e, function(e, r, t) {
            a = i(e, r, t);
            return !a
        });
        return !!a
    }
    function some(e, r, t) {
        var i = isArray(e) ? arraySome : baseSome;
        if (t && isIterateeCall(e, r, t)) {
            r = undefined
        }
        return i(e, baseIteratee(r))
    }
    var LocalZoom = 13;
    var zoomRange = [2, 26];
    var CONSTS = {
        ID: "__id__",
        Rp: zoomRange[1],
        Mp: zoomRange[0],
        jp: .8,
        Pp: .2,
        YM: 5,
        KM: {
            dom: "div",
            id: "_amap_custom_labellayer_div_",
            className: "amap-layer",
            style: {
                cssText: "position:absolute;top:0;left:0;",
                zIndex: "",
                width: "",
                height: "",
                visible: ""
            }
        },
        Dp: {
            Op: 180,
            Ad: 70,
            Td: 360,
            t1: 300,
            e1: 30,
            Bd: Math.PI / 10
        },
        XM: {
            top: 3,
            right: 3,
            bottom: 3,
            left: 3
        },
        JM: {
            ZM: "top",
            QM: "right",
            tj: "bottom",
            LEFT: "left",
            nj: "middle",
            ij: "center",
            ej: "alphabetic"
        },
        oj: {
            CLICK: "click",
            MOUSEOVER: "mouseover",
            MOUSEOUT: "mouseout",
            MOUSEMOVE: "mousemove",
            MOUSEDOWN: "mousedown",
            MOUSEUP: "mouseup",
            aj: "touchstart",
            uj: "touchend"
        },
        sj: {
            rank: 1,
            angle: 0,
            opacity: 1,
            offset: [0, 0],
            zooms: zoomRange,
            icon: {
                type: "image",
                image: "://visuallocal.amap.com/public/poi-marker.png",
                clipOrigin: [0, 0],
                clipSize: [36, 36],
                size: [36, 36],
                anchor: "bottom-center",
                angel: 0,
                qq: true,
                zooms: zoomRange
            },
            text: {
                type: "billboard",
                direction: "top",
                offset: [0, 0],
                zooms: zoomRange,
                style: {
                    fj: 6,
                    fontFamily: "sans-serif",
                    fontSize: 12,
                    fontWeight: "normal",
                    fillColor: [0, 0, 0, 1],
                    strokeWidth: 0,
                    strokeColor: [.988, .988, .988, 1],
                    borderColor: "transparent",
                    padding: [3, 3, 3, 3],
                    backgroundColor: ""
                }
            }
        },
        hj: {
            cj: {
                opacity: .2
            },
            vj: "fadeIn",
            lj: "fadeOut",
            dj: {
                bj: "start",
                RUNNING: "running",
                pj: "complete"
            },
            yj: {
                mj: "opacity",
                gj: "size"
            }
        },
        _d: 3,
        wj: "_AMap_sdf_com_words",
        Mj: 128,
        pc: ["://sdf.amap.com", "://sdf01.amap.com", "://sdf02.amap.com", "://sdf03.amap.com", "://sdf04.amap.com"],
        sp: 4,
        Cd: 200 / 256,
        Ld: 205 / 256,
        Aj: {
            kj: "纹理加载失败，请重试",
            Oj: "资源加载失败，请重试"
        },
        xj: "bufferChanged",
        Ij: "frameBufferChanged",
        Tj: "combineFrameBufferChanged",
        ic: 1,
        ud: 13,
        Cj: 12,
        Ej: "__icon_combination",
        Nj: "__text_combination",
        Lj: {
            file: {
                0: "://vdata.amap.com/style_icon/2.0/icon-normal-big.png",
                1: "://vdata.amap.com/style_icon/2.0/icon-biz-big.png"
            },
            size: [40, 40],
            Rj: 10
        },
        Kp: {
            buffer: 3,
            family: "ios9",
            size: 24
        },
        Color: {
            Uj: [0, 0, 0, 1],
            Gp: [0, 0, 0, 0]
        },
        gH: "sdf",
        RB: "sdfJson",
        FN: "version"
    };
    var StaticSourceID;
    (function(e) {
        e["nebula"] = "nebula";
        e["nebulaLabel"] = "nebulaLabel"
    }
    )(StaticSourceID || (StaticSourceID = {}));
    var DangerousFontSize = 24;
    var EnumTextureIndex;
    (function(e) {
        e[e["dynamic"] = 0] = "dynamic";
        e[e["icon"] = 1] = "icon"
    }
    )(EnumTextureIndex || (EnumTextureIndex = {}));
    var TextureMaxLength = 2;
    var ImageStatus;
    (function(e) {
        e["BLOB"] = "blob";
        e["TOLOAD"] = "toload";
        e["LOADING"] = "loading";
        e["LOADED"] = "loaded";
        e["UNLOADED"] = "unloaded";
        e["ERROR"] = "error";
        e["RELOADING"] = "reloading";
        e["UNCOMBINED"] = "uncombined";
        e["COMBINING"] = "combining";
        e["COMBINED"] = "combined"
    }
    )(ImageStatus || (ImageStatus = {}));
    var ColorMode = function() {
        function e(e, r) {
            this.Ci = e;
            this.mask = r
        }
        e.wi = {
            ZERO: 0,
            ONE: 1,
            SRC_ALPHA: 770,
            ONE_MINUS_SRC_ALPHA: 771
        };
        e.tH = [true, true, true, true];
        e.iH = [false, false, false, false];
        e.disabled = new e([e.wi.ONE, e.wi.ZERO, e.wi.ONE, e.wi.ZERO],e.iH);
        e.Mi = new e([e.wi.ONE, e.wi.ZERO, e.wi.ONE, e.wi.ZERO],e.tH);
        e.Ri = new e([e.wi.SRC_ALPHA, e.wi.ONE_MINUS_SRC_ALPHA, e.wi.SRC_ALPHA, e.wi.ONE_MINUS_SRC_ALPHA],e.tH);
        e.zi = new e([e.wi.ONE, e.wi.ONE_MINUS_SRC_ALPHA, e.wi.ONE, e.wi.ONE_MINUS_SRC_ALPHA],e.tH);
        e.Si = new e([e.wi.SRC_ALPHA, e.wi.ONE_MINUS_SRC_ALPHA, e.wi.ONE, e.wi.ONE_MINUS_SRC_ALPHA],e.tH);
        e.eee = new e([e.wi.SRC_ALPHA, e.wi.ONE_MINUS_SRC_ALPHA, e.wi.ONE, e.wi.ZERO],e.tH);
        return e
    }();
    var StencilMode = function() {
        function e(e, r, t) {
            this.stencilFunc = e;
            this.stencilOp = r;
            this.stencilMask = t
        }
        e.wi = {
            NOTEQUAL: 517,
            ALWAYS: 519,
            EQUAL: 514,
            GEQUAL: 518,
            GREATER: 516,
            LESS: 513,
            NEVER: 512,
            KEEP: 7680,
            REPLACE: 7681,
            ZERO: 0,
            INCR_WRAP: 34055,
            INCR: 7682,
            DECR: 7683
        };
        e.disable = new e({
            test: e.wi.ALWAYS,
            Li: 0,
            mask: 0
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],0);
        e.Ai = new e({
            test: e.wi.ALWAYS,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.REPLACE],255);
        e.writeWithStencil = new e({
            test: e.wi.LESS,
            Li: 240,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.nH = new e({
            test: e.wi.EQUAL,
            Li: 0,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.$H = new e({
            test: e.wi.EQUAL,
            Li: 0,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.INCR_WRAP],255);
        e.DH = new e({
            test: e.wi.EQUAL,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.FH = new e({
            test: e.wi.EQUAL,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.DECR],255);
        e.PH = new e({
            test: e.wi.ALWAYS,
            Li: 255,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.INCR],255);
        e.HH = new e({
            test: e.wi.NOTEQUAL,
            Li: 1,
            mask: 1
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.js = new e({
            test: e.wi.NOTEQUAL,
            Li: 1,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.nbStencil = new e({
            test: e.wi.EQUAL,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.outseaStencil = new e({
            test: e.wi.ALWAYS,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.REPLACE],255);
        e.TE = new e({
            test: e.wi.EQUAL,
            Li: 0,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.INCR],255);
        e.FE = new e({
            test: e.wi.EQUAL,
            Li: 1,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.ZERO],255);
        e.test = new e({
            test: e.wi.NOTEQUAL,
            Li: 254,
            mask: 255
        },[e.wi.ZERO, e.wi.ZERO, e.wi.INCR],255);
        e.HE = new e({
            test: e.wi.EQUAL,
            Li: 1,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],0);
        e.ree = new e({
            test: e.wi.EQUAL,
            Li: 0,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.INCR_WRAP],255);
        e.iee = new e({
            test: e.wi.EQUAL,
            Li: 1,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.KEEP],255);
        e.aee = new e({
            test: e.wi.EQUAL,
            Li: 1,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.INCR_WRAP],255);
        e.nee = new e({
            test: e.wi.NOTEQUAL,
            Li: 254,
            mask: 255
        },[e.wi.KEEP, e.wi.KEEP, e.wi.REPLACE],255);
        return e
    }();
    var globalInstance = typeof self === undefined ? window : self;
    var assert = function(e) {
        var r = [];
        for (var t = 1; t < arguments.length; t++) {
            r[t - 1] = arguments[t]
        }
        if (!Boolean(e)) {
            console.log.apply(console, r)
        }
    };
    var URLObject = typeof window !== "undefined" ? window.URL || window.webkitURL || {
        createObjectURL: function() {
            console.error("URL not exist, please use more popular browser")
        }
    } : URL || webkitURL || {
        createObjectURL: function() {
            console.error("URL not exist, please use more popular browser")
        }
    };
    var AbstractBaseValue = function() {
        function e(e) {
            this.Oi = same;
            this.gl = e.gl;
            this.default = this.Fi();
            this.current = this.Fi();
            this.Pi = false
        }
        e.prototype.set = function(e) {
            this.current = e
        }
        ;
        e.prototype.get = function() {
            return this.current
        }
        ;
        e.prototype.setDirty = function() {
            this.Pi = true
        }
        ;
        e.prototype.reset = function() {
            this.set(this.default)
        }
        ;
        e.prototype.ji = function(e) {
            return this.Pi || !this.Oi(e, this.current)
        }
        ;
        return e
    }();
    var Blend = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return false
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            if (e) {
                r.enable(r.BLEND)
            } else {
                r.disable(r.BLEND)
            }
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var Viewport = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            var e = this.gl;
            return [0, 0, e.drawingBufferWidth, e.drawingBufferHeight]
        }
        ;
        r.prototype.set = function(e) {
            var r = this.current;
            if (e[0] === r[0] && e[1] === r[1] && e[2] === r[2] && e[3] === r[3] && !this.Pi) {
                return
            }
            this.gl.viewport(e[0], e[1], e[2], e[3]);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var DepthTest = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return false
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            if (e) {
                r.enable(r.DEPTH_TEST);
                r.depthFunc(r.LEQUAL)
            } else {
                r.disable(r.DEPTH_TEST)
            }
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var DepthFunc = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return {
                test: this.gl.DEPTH_TEST,
                oee: this.gl.LESS
            }
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.depthFunc(e.oee);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var StencilTest = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return false
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            if (e) {
                r.enable(r.STENCIL_TEST)
            } else {
                r.disable(r.STENCIL_TEST)
            }
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var StencilFunc = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return {
                test: this.gl.ALWAYS,
                Li: 1,
                mask: 65535
            }
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.stencilFunc(e.test, e.Li, e.mask);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var StencilOp = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            var e = this.gl;
            return [e.KEEP, e.KEEP, e.KEEP]
        }
        ;
        r.prototype.set = function(e) {
            var r;
            if (!this.ji(e)) {
                return
            }
            (r = this.gl).stencilOp.apply(r, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var CullFace = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return false
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            if (e) {
                r.enable(r.CULL_FACE)
            } else {
                r.disable(r.CULL_FACE)
            }
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BlendFunc = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return [this.gl.ONE, this.gl.ZERO]
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.blendFunc(e[0], e[1]);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BlendFuncSeparate = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            var e = this.gl.ONE;
            var r = this.gl.ZERO;
            return [e, r, e, r]
        }
        ;
        r.prototype.set = function(e) {
            var r;
            if (!this.ji(e)) {
                return
            }
            (r = this.gl).blendFuncSeparate.apply(r, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BlendEquation = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return this.gl.FUNC_ADD
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.blendEquation(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BlendEquationSeparate = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return [this.gl.FUNC_ADD, this.gl.FUNC_ADD]
        }
        ;
        r.prototype.set = function(e) {
            var r;
            if (!this.ji(e)) {
                return
            }
            (r = this.gl).blendEquationSeparate.apply(r, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var CullFaceSide = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            var e = this.gl;
            return e.BACK
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.cullFace(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var FrontFace = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return this.gl.CCW
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e) && !this.Pi) {
                return
            }
            this.gl.frontFace(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var DepthClear = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return 1
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.clearDepth(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var ColorClear = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return [0, 0, 0, 0]
        }
        ;
        r.prototype.set = function(e) {
            var r;
            if (!this.ji(e)) {
                return
            }
            (r = this.gl).clearColor.apply(r, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var StencilClear = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return 0
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.clearStencil(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var ColorMask = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return [true, true, true, true]
        }
        ;
        r.prototype.set = function(e) {
            var r;
            if (!this.ji(e)) {
                return
            }
            (r = this.gl).colorMask.apply(r, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var DepthMask = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return true
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.depthMask(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var StencilMask = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return 65535
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.stencilMask(e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BindFramebuffer = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return null
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            r.bindFramebuffer(r.FRAMEBUFFER, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BindProgram = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return null
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            if (this.current && this.current !== e) {
                this.current.Ni()
            }
            if (e) {
                this.gl.useProgram(e.$i)
            } else {
                this.gl.useProgram(null)
            }
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var ActiveTextureUnit = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return this.gl.TEXTURE0
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.activeTexture(this.gl.TEXTURE0 + e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BindTexture = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return null
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            var r = this.gl;
            r.bindTexture(r.TEXTURE_2D, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var AbstractFramebufferAttachment = function(i) {
        __extends(e, i);
        function e(e, r) {
            var t = i.call(this, e) || this;
            t.context = e;
            t.parent = r;
            return t
        }
        return e
    }(AbstractBaseValue);
    var ColorAttachment = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.set = function(e) {
            if (!this.Pi && !this.ji(e)) {
                return
            }
            var r = this.gl;
            var t = this.context.bindFramebuffer.current;
            this.context.bindFramebuffer.set(this.parent.framebuffer);
            r.framebufferTexture2D(r.FRAMEBUFFER, r.COLOR_ATTACHMENT0, r.TEXTURE_2D, e, 0);
            this.context.bindFramebuffer.set(t);
            this.current = e
        }
        ;
        r.prototype.Bi = function() {
            var e = this.current;
            var r = this.gl;
            if (e) {
                r.deleteTexture(e);
                this.current = null
            }
        }
        ;
        r.prototype.Fi = function() {
            return null
        }
        ;
        return r
    }(AbstractFramebufferAttachment);
    var DepthAttachment = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.set = function(e) {
            if (!this.Pi && !this.ji(e)) {
                return
            }
            var r = this.gl;
            this.context.bindFramebuffer.set(this.parent.framebuffer);
            r.framebufferRenderbuffer(r.FRAMEBUFFER, r.DEPTH_ATTACHMENT, r.RENDERBUFFER, e);
            this.current = e
        }
        ;
        r.prototype.Bi = function() {
            var e = this.current;
            var r = this.gl;
            if (e) {
                r.deleteRenderbuffer(e);
                this.current = null
            }
        }
        ;
        r.prototype.Fi = function() {
            return null
        }
        ;
        return r
    }(AbstractFramebufferAttachment);
    var BindArrayBuffer = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return null
        }
        ;
        r.prototype.set = function(e) {
            if (e && e.xe) {
                assert(true, "WebGLBuffer deleted")
            }
            if (!this.ji(e)) {
                return
            }
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var BindElementArrayBuffer = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return null
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var UnpackFlipYWebgl = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Fi = function() {
            return false
        }
        ;
        r.prototype.set = function(e) {
            if (!this.ji(e)) {
                return
            }
            this.gl.pixelStorei(this.gl.UNPACK_FLIP_Y_WEBGL, e);
            this.current = e;
            this.Pi = false
        }
        ;
        return r
    }(AbstractBaseValue);
    var Framebuffer = function() {
        function e(e, r, t) {
            this.context = e;
            this.width = r;
            this.height = t;
            var i = e.gl;
            var a = i.createFramebuffer();
            if (!a) {
                throw Error("createFramebuffer error")
            }
            this.framebuffer = a;
            this.Wi = new ColorAttachment(e,this);
            this.Gi = new DepthAttachment(e,this)
        }
        e.prototype.resize = function(e, r) {
            this.Wi.Bi();
            this.Gi.Bi();
            this.width = e;
            this.height = r
        }
        ;
        e.prototype.destroy = function() {
            var e = this.context.gl;
            this.Wi.Bi();
            this.Gi.Bi();
            e.deleteFramebuffer(this.framebuffer)
        }
        ;
        e.prototype.get = function() {
            return this.framebuffer
        }
        ;
        return e
    }();
    var UglyBrowser = M["Support"];
    var Texture = function() {
        function e(e, r, t) {
            this.context = e;
            this.texture = e.gl.createTexture();
            this.update(r, t)
        }
        e.prototype.update = function(e, r, t) {
            var i = e.width
              , a = e.height;
            var n = r && r.resize || (!this.size || this.size[0] !== i || this.size[1] !== a) && !t;
            var o = this.context.gl;
            var f = r && r.format || o.RGBA;
            this.Zi = Boolean(r && r.Zi);
            o.bindTexture(o.TEXTURE_2D, this.texture);
            if (r && r.Yi) {
                o.pixelStorei(o.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 1)
            }
            if (r && r.flipY) {
                this.context.hB.set(true)
            } else {
                this.context.hB.set(false)
            }
            if (n) {
                this.size = [i, a];
                if (e instanceof HTMLImageElement || e instanceof HTMLCanvasElement || e instanceof HTMLVideoElement || e instanceof ImageData || UglyBrowser.imageBitmap && e instanceof ImageBitmap) {
                    o.texImage2D(o.TEXTURE_2D, 0, o.RGBA, o.RGBA, o.UNSIGNED_BYTE, e)
                } else {
                    o.texImage2D(o.TEXTURE_2D, 0, f, i, a, 0, f, o.UNSIGNED_BYTE, e.data)
                }
            } else {
                var s = t || {
                    x: 0,
                    y: 0
                }
                  , u = s.x
                  , l = s.y;
                if (e instanceof HTMLImageElement || e instanceof HTMLCanvasElement || e instanceof HTMLVideoElement || e instanceof ImageData || UglyBrowser.imageBitmap && e instanceof ImageBitmap) {
                    o.texSubImage2D(o.TEXTURE_2D, 0, u, l, o.RGBA, o.UNSIGNED_BYTE, e)
                } else {
                    o.texSubImage2D(o.TEXTURE_2D, 0, u, l, i, a, f, o.UNSIGNED_BYTE, e.data)
                }
            }
            if (this.Zi && this.Vi()) {
                o.generateMipmap(o.TEXTURE_2D)
            }
            if (r && r.Yi) {
                o.pixelStorei(o.UNPACK_PREMULTIPLY_ALPHA_WEBGL, 0)
            }
            o.bindTexture(o.TEXTURE_2D, null)
        }
        ;
        e.prototype.generateMipmap = function() {
            if (this.Vi()) {
                var e = this.context.gl;
                e.bindTexture(e.TEXTURE_2D, this.texture);
                e.generateMipmap(e.TEXTURE_2D);
                e.bindTexture(e.TEXTURE_2D, null)
            } else {
                console.log("generateMipmap error")
            }
        }
        ;
        e.prototype.bind = function(e, r, t) {
            var i = this.context.gl;
            i.bindTexture(i.TEXTURE_2D, this.texture);
            if (r && r !== this.Xi) {
                i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MAG_FILTER, r);
                this.Xi = r
            }
            if (t && t !== this.Hi) {
                i.texParameteri(i.TEXTURE_2D, i.TEXTURE_MIN_FILTER, t);
                this.Hi = t
            }
            if (e && e !== this.wrap) {
                i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_S, e);
                i.texParameteri(i.TEXTURE_2D, i.TEXTURE_WRAP_T, e);
                this.wrap = e
            }
        }
        ;
        e.prototype.Vi = function() {
            return Math.log(this.size[0]) / Math.LN2 % 1 === 0 && Math.log(this.size[1]) / Math.LN2 % 1 === 0
        }
        ;
        e.prototype.destroy = function() {
            var e = this.context.gl;
            e.deleteTexture(this.texture);
            this.texture = null
        }
        ;
        e.prototype.get = function() {
            return this.texture
        }
        ;
        return e
    }();
    function diffArray(e, r) {
        if (e === r) {
            return false
        }
        if (e.length !== r.length) {
            return true
        }
        for (var t = 0, i = e.length; t < i; t++) {
            if (e[t] !== r[t]) {
                return true
            }
        }
        return false
    }
    function diffType(e, r) {
        if (typeof e !== typeof r) {
            return true
        }
        if (typeof e === "object") {
            return Array.isArray(e) !== Array.isArray(r)
        }
        return false
    }
    function fastDiff(e, r) {
        return e !== r
    }
    function diff(e, r) {
        if (diffType(e, r)) {
            return true
        }
        switch (typeof e) {
        case "boolean":
        case "number":
        case "string":
            return fastDiff(e, r);
        case "object":
            return Array.isArray(e) ? diffArray(e, r) : fastDiff(e, r);
        default:
            return true
        }
    }
    function same(e, r) {
        return !diff(e, r)
    }
    var Context = function() {
        function e(e) {
            this.te = same;
            this.gl = e;
            this.Z_ = this.gl.getSupportedExtensions();
            this.extensions = {};
            this.be = new Viewport(this);
            this.ie = new Blend(this);
            this.ee = new BlendFuncSeparate(this);
            this.blendEquation = new BlendEquation(this);
            this.cullFace = new CullFace(this);
            this.he = new CullFaceSide(this);
            this.frontFace = new FrontFace(this);
            this.depthTest = new DepthTest(this);
            this.depthFunc = new DepthFunc(this);
            this.se = new StencilTest(this);
            this.stencilFunc = new StencilFunc(this);
            this.stencilOp = new StencilOp(this);
            this.stencilMask = new StencilMask(this);
            this.le = new StencilClear(this);
            this.ue = new ColorClear(this);
            this.ce = new DepthClear(this);
            this.bindFramebuffer = new BindFramebuffer(this);
            this.ge = new BindProgram(this);
            this.bindTexture = new BindTexture(this);
            this.activeTexture = new ActiveTextureUnit(this);
            this.de = new BindArrayBuffer(this);
            this.me = new BindElementArrayBuffer(this);
            this.colorMask = new ColorMask(this);
            this.depthMask = new DepthMask(this);
            this.ye = {};
            this.hB = new UnpackFlipYWebgl(this)
        }
        e.prototype.clear = function(e) {
            var r = 0;
            var t = this.gl;
            if (e.color) {
                r = r | t.COLOR_BUFFER_BIT
            }
            if (e.depth) {
                r = r | t.DEPTH_BUFFER_BIT
            }
            if (e.stencil) {
                r = r | t.STENCIL_BUFFER_BIT
            }
            if (r !== 0) {
                t.clear(r)
            }
        }
        ;
        e.prototype.Qi = function(e) {
            if (this.te(e, ColorMode.Mi)) {
                this.ie.set(false);
                this.colorMask.set(ColorMode.tH)
            } else if (this.te(e, ColorMode.disabled)) {
                this.colorMask.set(ColorMode.iH)
            } else {
                this.ie.set(true);
                this.ee.set(e.Ci);
                this.colorMask.set(e.mask)
            }
        }
        ;
        e.prototype.ne = function(e) {
            this.depthTest.set(e)
        }
        ;
        e.prototype.ae = function(e) {
            if (this.te(e.stencilFunc.test, StencilMode.wi.ALWAYS) && !e.stencilMask) {
                this.se.set(false)
            } else {
                this.se.set(true);
                this.stencilFunc.set(e.stencilFunc);
                this.stencilOp.set(e.stencilOp);
                this.stencilMask.set(e.stencilMask)
            }
        }
        ;
        e.prototype.re = function(e) {
            if (!e.enable) {
                this.cullFace.set(false)
            } else {
                this.cullFace.set(e.enable);
                this.he.set(e.mode);
                this.frontFace.set(e.frontFace)
            }
        }
        ;
        e.prototype.reset = function() {
            this.ie.reset();
            this.ee.reset();
            this.blendEquation.reset();
            this.cullFace.reset();
            this.he.reset();
            this.frontFace.reset();
            this.depthTest.reset();
            this.se.reset();
            this.stencilFunc.reset();
            this.stencilOp.reset();
            this.stencilMask.reset();
            this.le.reset();
            this.ue.reset();
            this.ce.reset();
            this.bindFramebuffer.set(null)
        }
        ;
        e.prototype.createFramebuffer = function(e, r) {
            return new Framebuffer(this,e,r)
        }
        ;
        e.prototype.createRenderbuffer = function(e, r) {
            var t = this.gl;
            var i = t.createRenderbuffer();
            t.bindRenderbuffer(t.RENDERBUFFER, i);
            t.renderbufferStorage(t.RENDERBUFFER, t.DEPTH_COMPONENT16, e, r);
            if (!i) {
                throw Error("bindRenderbuffer error")
            }
            return i
        }
        ;
        e.prototype.createTexture = function(e, r) {
            return new Texture(this,e,r)
        }
        ;
        e.prototype.fe = function(e, r, t) {
            if (t === void 0) {
                t = "STATIC_DRAW"
            }
            var i = this.gl;
            var a = i.createBuffer();
            if (!a) {
                throw Error("createBuffer error")
            }
            a.context = this;
            a.size = r;
            a.length = e.byteLength / r;
            var n = this.de.current;
            this.de.set(a);
            i.bufferData(i.ARRAY_BUFFER, e, i[t]);
            this.de.set(n);
            return a
        }
        ;
        e.prototype.ve = function(e) {
            var r = this.gl;
            var t = r.createBuffer();
            if (!t) {
                throw Error("createBuffer error")
            }
            t.context = this;
            t.length = e.length;
            t.type = "int";
            t.G_ = true;
            if (e instanceof Uint16Array) {
                t.size = 16
            } else {
                t.size = 32
            }
            var i = this.me.current;
            this.me.set(t);
            r.bufferData(r.ELEMENT_ARRAY_BUFFER, e, r.STATIC_DRAW);
            this.me.set(i);
            return t
        }
        ;
        e.prototype.getSize = function() {
            return {
                width: this.gl.canvas.width,
                height: this.gl.canvas.height
            }
        }
        ;
        e.prototype.getParameter = function(e) {
            if (!this.ye[e]) {
                this.ye[e] = this.gl.getParameter(this.gl[e])
            }
            return this.ye[e]
        }
        ;
        e.prototype.deleteBuffer = function(e) {
            this.de.set(null);
            this.me.set(null);
            this.gl.deleteBuffer(e);
            e.xe = true
        }
        ;
        e.prototype.getExtension = function(e) {
            if (this.extensions[e]) {
                return this.extensions[e]
            }
            if (!this.Z_) {
                console.error("WebGL: supportedExtension not exist");
                return
            }
            if (this.Z_.indexOf(e) < 0) {
                console.error("WebGL: supportedExtension not exist " + e);
                return
            }
            if (!this.extensions[e]) {
                this.extensions[e] = this.gl.getExtension(e);
                if (!this.extensions[e]) {
                    console.error("WebGL: getExtension " + e + " failed")
                }
            }
        }
        ;
        e.prototype.setDirty = function() {
            this.ge.setDirty();
            this.bindTexture.setDirty();
            this.bindFramebuffer.setDirty();
            this.de.setDirty();
            this.me.setDirty();
            this.be.setDirty();
            this.activeTexture.setDirty();
            this.colorMask.setDirty();
            this.depthMask.setDirty();
            this.se.setDirty();
            this.le.setDirty();
            this.stencilMask.setDirty();
            this.stencilFunc.setDirty();
            this.stencilOp.setDirty();
            this.colorMask.setDirty();
            this.ie.setDirty();
            this.blendEquation.setDirty();
            this.ee.setDirty();
            this.depthTest.setDirty();
            this.cullFace.setDirty();
            this.he.setDirty();
            this.frontFace.setDirty();
            this.hB.setDirty()
        }
        ;
        return e
    }();
    var CullFaceMode = function() {
        function e(e, r, t) {
            this.enable = e;
            this.mode = r;
            this.frontFace = t
        }
        e.wi = {
            CW: 2304,
            CCW: 2305,
            FRONT: 1028,
            BACK: 1029,
            FRONT_AND_BACK: 1032
        };
        e.we = new e(false,e.wi.FRONT_AND_BACK,e.wi.CW);
        e._e = new e(true,e.wi.FRONT,e.wi.CW);
        e.back = new e(true,e.wi.BACK,e.wi.CW);
        return e
    }();
    var Util = AMap["Util"];
    var LayerRender = function() {
        function e() {
            this.hS = Util.stamp(this)
        }
        e.prototype.destroy = function() {}
        ;
        e.prototype.fH = function(e) {
            var r = e.Ro;
            var t = e.localCoord.center;
            var i = [r[0] - t[0], r[1] - t[1]];
            var a = [r[0] - t[0], r[3] - t[1]];
            var n = [r[2] - t[0], r[1] - t[1]];
            var o = [r[2] - t[0], r[3] - t[1]];
            return __spreadArrays(i, n, a, n, o, a)
        }
        ;
        e.prototype.OH = function(e, r) {
            if (e === void 0) {
                e = false
            }
            if (!r) {
                return StencilMode.nH
            }
            if (r && !e) {
                return StencilMode.DH
            }
            if (r && e) {
                return StencilMode.HH
            }
        }
        ;
        e.prototype.BH = function(e, r) {
            if (e === void 0) {
                e = false
            }
            if (!r) {
                return StencilMode.$H
            }
            if (r && !e) {
                return StencilMode.FH
            }
            if (r && e) {
                return StencilMode.PH
            }
        }
        ;
        return e
    }();
    var Support = M["Support"];
    var scale = Support.scale;
    var LabelsRender = function() {
        function e() {
            this.Lz = {
                De: [],
                sizes: []
            };
            this._f = null;
            this.CG = 0
        }
        e.prototype.Mz = function(e) {
            var r = e.map
              , t = e.R_
              , i = e.z_
              , a = e.$i;
            this._map = r;
            this.Mf = t;
            this.ac = i;
            this._f = a
        }
        ;
        e.prototype.Ce = function(e, r, t, i) {
            if (t === void 0) {
                t = {}
            }
            var a = CONSTS.ud
              , n = CONSTS.sp;
            var o = this._f;
            var f = e.viewState
              , s = e.size;
            if (!f) {
                return i.iterator
            }
            var O = f.mvpMatrix
              , B = f.zoom
              , D = f.viewMode;
            var u = s[0]
              , l = s[1];
            var N = i.Sa
              , v = i.from
              , U = v === void 0 ? "inner" : v
              , c = i.opacity
              , H = c === void 0 ? 1 : c
              , h = i.offset
              , z = h === void 0 ? [0, 0] : h
              , G = i.Sz
              , d = i.LF
              , j = d === void 0 ? {} : d;
            var _ = i.iterator;
            var g = 1;
            if (D === "3D") {
                g = e.map.getView().EF()
            }
            var V = t.Rz
              , W = t.buffer
              , y = t.od
              , m = t.zz;
            r.bindFramebuffer.set(null);
            r.be.set([0, 0, u * scale, l * scale]);
            var p = V.pp;
            var b = W;
            var x = this.fd(i.fee)
              , M = x.De
              , C = M === void 0 ? [] : M
              , S = x.sizes
              , w = S === void 0 ? [] : S;
            var k = a * n;
            var T = p.length;
            if (!b || !y) {
                return _
            }
            if (T) {
                r.ge.set(o);
                o.Ae({
                    a_vertex: {
                        type: "vec2",
                        buffer: b,
                        Re: k,
                        offset: 0
                    },
                    a_texcoord: {
                        type: "vec2",
                        buffer: b,
                        Re: k,
                        offset: 2 * n
                    },
                    a_origin: {
                        type: "vec2",
                        buffer: b,
                        Re: k,
                        offset: 4 * n
                    },
                    a_texIndex: {
                        type: "float",
                        buffer: b,
                        Re: k,
                        offset: 6 * n
                    },
                    a_type: {
                        type: "float",
                        buffer: b,
                        Re: k,
                        offset: 7 * n
                    },
                    a_zooms: {
                        type: "vec2",
                        buffer: b,
                        Re: k,
                        offset: 8 * n
                    },
                    a_height: {
                        type: "float",
                        buffer: b,
                        Re: k,
                        offset: 10 * n
                    },
                    a_angle: {
                        type: "float",
                        buffer: b,
                        Re: k,
                        offset: 11 * n
                    },
                    a_highflag: {
                        type: "float",
                        buffer: b,
                        Re: k,
                        offset: 12 * n
                    },
                    a_visible: {
                        type: "float",
                        buffer: y,
                        Re: 0,
                        offset: m[_] && m[_].start
                    }
                });
                var q = this.l_ === "inner" ? ColorMode.Ri : ColorMode.Si;
                for (var A = 0; A < T; A++) {
                    var I = p[A];
                    var $ = I.start
                      , F = $ === void 0 ? 0 : $
                      , P = I.end
                      , K = P === void 0 ? 0 : P
                      , Y = I.dp;
                    _++;
                    if (!(K - F)) {
                        continue
                    }
                    var L = void 0;
                    if (U === "labelsLayer" && I.pd) {
                        var Z = this.ac.getImage(I.pd);
                        if (Z.img) {
                            var X = this.dd(r, Z)
                              , R = X.texture
                              , J = X.size;
                            if (R && R.context) {
                                C[EnumTextureIndex.icon] = R;
                                L = w.slice(0);
                                L[EnumTextureIndex.icon * 2] = J[0];
                                L[EnumTextureIndex.icon * 2 + 1] = J[1]
                            }
                        }
                    }
                    var E = I.vd;
                    var Q = E.GV("fillColor");
                    var ee = E.GV("strokeColor");
                    var re = E.GV("backgroundColor");
                    var te = E.GV("borderColor");
                    o.Le({
                        u_matrix: O,
                        u_texture: {
                            De: C,
                            count: 4
                        },
                        u_gl_size: [u, l],
                        u_texsize: L ? L : w,
                        u_color: Q,
                        u_strokeColor: ee,
                        u_borderColor: te,
                        u_gamma: E.GV("u_gamma"),
                        u_borderBuffer: E.GV("u_borderBuffer"),
                        u_buffer: E.GV("u_buffer"),
                        u_backgroundColor: re,
                        u_opacity: I.opacity * H,
                        u_zoom: B,
                        u_event: 0,
                        u_delta_center: N,
                        u_offset: z,
                        u_transform: [0, 0, 1, 0],
                        u_skyHeight: g,
                        u_fontSizeFactor: E.GV("u_fontSizeFactor"),
                        u_highlightFillColor: E.GV("u_highlightFillColor"),
                        u_highlightStrokeColor: E.GV("u_highlightStrokeColor")
                    });
                    o.Pe((K - F) / a, F / a, undefined, undefined, j.depthTest, q)
                }
            }
            return _
        }
        ;
        e.prototype.Cz = function(e, r, t, i, a) {
            var n = this._f;
            var o = CONSTS.sp
              , f = CONSTS.ud;
            var s = e.viewState
              , u = e.size;
            var l = s.zoom || zoomRange[1];
            var v = s.mvpMatrix || null;
            var O = u[0]
              , B = u[1];
            var D = a.Az
              , c = a.Iz
              , N = a.centerCoord;
            var h = 1;
            if (s.viewMode === "3D") {
                h = e.map.getView().EF()
            }
            if (!v) {
                return
            }
            var d = i.$z || {};
            var _ = d.pp
              , g = d.zs
              , y = d.od;
            var m = g && g.buffer;
            var p = Math.pow(2, l - c);
            r.be.set([0, 0, O * scale, B * scale]);
            var b = this.fd(a.fee)
              , x = b.De
              , M = x === void 0 ? [] : x
              , C = b.sizes
              , U = C === void 0 ? [] : C;
            var S = _.length;
            var w = f * o;
            var H;
            var z;
            var k;
            var T;
            var A;
            var I;
            var G;
            var j = c >= l ? p : 1;
            var V = [0, 0, j, 0];
            if (!m || !y) {
                return
            }
            if (S) {
                r.ge.set(this._f);
                n.Ae({
                    a_vertex: {
                        type: "vec2",
                        buffer: m,
                        Re: w,
                        offset: 0
                    },
                    a_texcoord: {
                        type: "vec2",
                        buffer: m,
                        Re: w,
                        offset: 2 * o
                    },
                    a_origin: {
                        type: "vec2",
                        buffer: m,
                        Re: w,
                        offset: 4 * o
                    },
                    a_texIndex: {
                        type: "float",
                        buffer: m,
                        Re: w,
                        offset: 6 * o
                    },
                    a_type: {
                        type: "float",
                        buffer: m,
                        Re: w,
                        offset: 7 * o
                    },
                    a_zooms: {
                        type: "vec2",
                        buffer: m,
                        Re: w,
                        offset: 8 * o
                    },
                    a_height: {
                        type: "float",
                        buffer: m,
                        Re: w,
                        offset: 10 * o
                    },
                    a_angle: {
                        type: "float",
                        buffer: m,
                        Re: w,
                        offset: 11 * o
                    },
                    a_highflag: {
                        type: "float",
                        buffer: m,
                        Re: w,
                        offset: 12 * o
                    },
                    a_visible: {
                        type: "float",
                        buffer: y,
                        Re: 0,
                        offset: 0
                    }
                })
            }
            var $;
            for (var F = 0; F < S; F++) {
                var P = _[F];
                H = P[0];
                z = P[1];
                k = P[11];
                T = P[12];
                A = P[13];
                G = P[14];
                if (!(T - k)) {
                    continue
                }
                for (var L = 0, R = D; L < R.length; L++) {
                    var E = R[L];
                    $ = t[E];
                    if ($ && E === A) {
                        I = $.Sa
                    }
                }
                if (!I || p < .6) {
                    continue
                }
                if (!$) {
                    continue
                }
                I = [0, 0];
                n.Le({
                    u_matrix: v,
                    u_delta_center: I,
                    u_transform: V,
                    u_texture: {
                        De: M,
                        count: M.length
                    },
                    u_gl_size: u,
                    u_texsize: U,
                    u_color: P[3],
                    u_strokeColor: P[4],
                    u_gamma: P[5],
                    u_borderBuffer: P[7],
                    u_buffer: P[8],
                    u_backgroundColor: P[9],
                    u_opacity: 1,
                    u_zoom: l,
                    u_event: 0,
                    u_skyHeight: h,
                    u_fontSizeFactor: 1
                });
                n.Pe((T - k) / f, k / f, undefined, undefined, undefined, ColorMode.Si)
            }
        }
        ;
        e.prototype.gp = function(e, r) {
            var t = e.context;
            var i = e.Jn();
            var a = r;
            var n = t.fe(new Uint16Array([0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0]), 8);
            if (!a) {
                return
            }
            i.Ce({
                u_texture: a
            }, {
                a_pos: {
                    type: "vec2",
                    buffer: n,
                    Re: 8,
                    offset: 0
                },
                a_coord: {
                    type: "vec2",
                    buffer: n,
                    Re: 8,
                    offset: 4
                }
            }, 4, undefined, "TRIANGLE_FAN", undefined, ColorMode.Mi)
        }
        ;
        e.prototype.reset = function() {
            this.Df()
        }
        ;
        e.prototype.destroy = function() {
            if (this.Lz) {
                for (var e = 0, r = this.Lz.De; e < r.length; e++) {
                    var t = r[e];
                    t.destroy()
                }
                delete this.Lz
            }
        }
        ;
        e.prototype.Dz = function(e, r, t) {
            var i = t.start
              , a = i === void 0 ? 0 : i
              , n = t.dp
              , o = t.style;
            var f = 6;
            var s = a;
            var u = 0;
            var l;
            var v;
            for (var c = 0, h = n; c < h.length; c++) {
                var d = h[c];
                l = d.id;
                v = d.kd;
                var _ = r[l];
                if (e.uf < a + u + v) {
                    e.ff(e.uf * 2)
                }
                var g = e.value;
                if (_) {
                    var y = o[l] || {}
                      , m = y.showText
                      , p = m === void 0 ? 1 : m
                      , b = y.showIcon
                      , x = b === void 0 ? true : b;
                    var M = 0;
                    var C = Infinity;
                    var S = -Infinity;
                    var w = Infinity;
                    var k = -Infinity;
                    if (x) {
                        M = w = 0;
                        k = d.icon * f;
                        while (M < k) {
                            g[s + u + M] = 1;
                            M++
                        }
                    }
                    if (p) {
                        M = C = d.icon * f;
                        S = (d.icon + d.Rd + d.bg) * f;
                        while (M < S) {
                            g[s + u + M] = 1;
                            M++
                        }
                    }
                }
                u += v
            }
            s += u;
            return s
        }
        ;
        e.prototype.Df = function() {}
        ;
        e.prototype.fd = function(e) {
            var r = [];
            var t = [];
            if (e) {
                r[0] = e;
                t[0] = e.size[0];
                t[1] = e.size[1]
            }
            var i = this._map.so.texture;
            if (i) {
                r.push(i);
                var a = i.size;
                t.push(a[0], a[1])
            }
            return {
                De: r,
                sizes: t
            }
        }
        ;
        e.prototype.dd = function(e, r) {
            var t = e.gl;
            var i = r.img
              , a = r.width
              , n = r.height;
            var o = i;
            if (i && !i.context) {
                o = e.createTexture(i);
                o.bind(t.CLAMP_TO_EDGE, t.LINEAR, t.LINEAR);
                r.img = o
            }
            return {
                texture: o,
                size: [a, n]
            }
        }
        ;
        return e
    }();
    var transforms = M["transform"];
    var Support$1 = M["Support"];
    var LabelsRenderUtil = M["LabelsRenderUtil"];
    var LabelsRenderDOMRender = function() {
        function e() {
            this._size = [0, 0];
            this.Wz = 1;
            var e = this.canvas = document.createElement("canvas");
            this.Nz = e.getContext("2d")
        }
        e.prototype.Ce = function(e, r, t, i, a) {
            if (!e) {
                return
            }
            this.wf = e;
            var O = e.size;
            this._size = O;
            var B = a.centerCoord
              , n = a.Uf
              , D = n === void 0 ? {} : n
              , N = a.$f
              , o = a.Sa
              , U = o === void 0 ? [0, 0] : o;
            this.Tz();
            this.Pz();
            var f = e.viewState;
            var s = f.zoom;
            var H = f.viewMode;
            if (!this.Oz(s, r.zooms)) {
                return
            }
            if (!this._map && i) {
                var z = f.projectionId;
                this._map = i;
                this.If = transforms[z];
                this._view = i.getView();
                this.ac = i.z_
            }
            if (!t["other"]) {
                return
            }
            var u = t && t.other
              , G = u.coords
              , j = u.style
              , V = u.Nf
              , l = u.Fz
              , W = l === void 0 ? [] : l;
            var v = {
                viewState: f,
                size: this.wf.size,
                transform: this.If,
                view: this._view,
                viewMode: H,
                centerCoord: B
            };
            this.Bz(r);
            for (var c = 0, h = W; c < h.length; c++) {
                var d = h[c];
                for (var _ = 0; _ < d.length; _++) {
                    var g = d[_];
                    if (!D[g]) {
                        continue
                    }
                    var y = V[g] || {};
                    var m = G[g] || {};
                    var p = j[g] || {};
                    var q = y.data.extData || {};
                    var b = q.Sa || U || [0, 0];
                    var K = y.data || {};
                    var x = K["txt"];
                    var Y = y.opts || {};
                    var M = (p.icon || [])[0] || {};
                    var C = M.image;
                    var S = isUndefined(p.opacity) ? 1 : Y.opacity;
                    var w = p.text;
                    if (C && M && this.Oz(s, p.icon && p.icon.zooms)) {
                        var k = this.ac["getImage"](C)
                          , Z = k.img
                          , X = k.width
                          , J = k.height;
                        var Q = {
                            size: [X, J],
                            opacity: S
                        };
                        var T = m.icon.origin;
                        var A = m.icon.anchorOffset;
                        var I = LabelsRenderUtil.wu(T[0] + b[0], T[1] + b[1], s, v);
                        var ee = [I[0] + A[0], I[1] + A[1]];
                        this.drawImage(Z, M, ee, Q)
                    }
                    if (x && w && this.Oz(s, p.text && p.text.zooms)) {
                        var re = {
                            opacity: S,
                            padding: m.padding
                        };
                        var T = m.text.origin;
                        var A = m.text.offset;
                        var $ = LabelsRenderUtil.wu(T[0] + b[0], T[1] + b[1], s, v);
                        var te = w.style;
                        var ie = te.fontSize || 12;
                        var F = [$[0] + A[0], $[1] + A[1] + ie];
                        this.Ez(p.text, $, re);
                        var P = x.length;
                        var ae = te.fold === false ? 100 : 6;
                        var L = Math.ceil(P / ae);
                        var R = Math.floor(P / L);
                        var ne = 3;
                        for (var E = 0; E < L; E++) {
                            var oe = E === L - 1 ? P - E * R : R;
                            var fe = x.substr(E * R, oe);
                            this.gp(fe, w, F, re);
                            var se = ie * 5 / 4 + ne;
                            F[1] = F[1] + se
                        }
                    }
                }
            }
        }
        ;
        e.prototype.Ez = function(e, r, t) {
            var i = this.Nz;
            var a = Support$1.scale
              , n = a === void 0 ? 1 : a;
            var o = e.style.backgroundColor;
            var f = t.padding;
            var s = f.offsetX
              , u = f.offsetY
              , l = f.w
              , v = f.h;
            var c = r[0] + s;
            var h = r[1] + u;
            if (i && o) {
                i.fillStyle = o;
                i.fillRect(c * n, h * n, l * n, v * n)
            }
        }
        ;
        e.prototype.drawImage = function(e, r, t, i) {
            var a = Support$1.scale;
            var n = this.Nz;
            if (!n || !r) {
                return
            }
            var o = r["image"];
            var f = r["size"];
            var s = r["clipOrigin"];
            var u = r["angel"];
            var l = r["clipSize"];
            var v = i.opacity
              , c = v === void 0 ? 1 : v;
            var h = f[0];
            var d = f[1];
            var _ = e.width || h;
            var g = e.height || d;
            var y = l[0];
            var m = l[1];
            var p = s[0];
            var b = s[1];
            if (isUndefined(y)) {
                y = h
            }
            if (isUndefined(m)) {
                m = d
            }
            if (y > _) {
                y = _
            }
            if (m > g) {
                m = g
            }
            n.globalAlpha = c * this.Wz;
            n.drawImage(e, p, b, y, m, t[0] * a, t[1] * a, h * a, d * a)
        }
        ;
        e.prototype.gp = function(e, r, t, i) {
            var a = Support$1.scale
              , n = a === void 0 ? 1 : a;
            var o = this.Nz;
            if (!o) {
                return
            }
            var f = r.style
              , s = f === void 0 ? {} : f;
            var u = i.opacity
              , l = u === void 0 ? 1 : u;
            var v = s.fontWeight
              , c = s.fontSize
              , h = s.fontFamily
              , d = h === void 0 ? "sans-serif" : h
              , _ = s.fillColor
              , g = s.strokeColor
              , y = s.strokeWidth
              , m = y === void 0 ? 1 : y
              , p = s.textAlign
              , b = p === void 0 ? "center" : p
              , x = s.Gz
              , M = x === void 0 ? "Alphabetic" : x;
            o.globalAlpha = l;
            o.globalAlpha = 1;
            o.font = v + " " + c * n + "px " + d;
            o.textAlign = b;
            o.textBaseline = M;
            var C = t[0];
            var S = t[1];
            o.lineJoin = "round";
            if (g && m) {
                o.strokeStyle = g;
                o.lineWidth = m;
                o.strokeText(e, C * n, S * n)
            }
            o.fillStyle = _;
            o.fillText(e, C * n, S * n)
        }
        ;
        e.prototype.Pz = function() {
            var e = this.canvas;
            var r = Support$1.scale;
            var t = this._size || [0, 0]
              , i = t[0]
              , a = t[1]
        }
        ;
        e.prototype.Tz = function() {
            this.canvas.width = this.canvas.width
        }
        ;
        e.prototype.reset = function() {
            this.Pz()
        }
        ;
        e.prototype.destroy = function() {
            this.Pz()
        }
        ;
        e.prototype.Oz = function(e, r) {
            if (r === void 0) {
                r = []
            }
            var t = r[0] || zoomRange[0];
            var i = r[1] || zoomRange[1];
            if (e >= t && e <= i) {
                return true
            }
            return false
        }
        ;
        e.prototype.Bz = function(e) {
            var r = this._container;
            if (!r) {
                r = this._container = this.see(this._map)
            }
            var t = Support$1.scale;
            var i = e.zIndex
              , a = i === void 0 ? 100 : i
              , n = e.opacity
              , o = e.visible
              , f = e.zooms
              , s = f === void 0 ? zoomRange : f;
            var u = this.wf.viewState.zoom;
            var l = this.Oz(u, s);
            if (!o || !l || n === 0) {
                r.style.display = "none"
            } else {
                r.style.display = "block"
            }
            this.Wz = n;
            var v = this._size;
            r.setAttribute("width", v[0] * t + "px");
            r.setAttribute("height", v[1] * t + "px");
            r.style.width = v[0] + "px";
            r.style.height = v[1] + "px";
            r.style.zIndex = a + ""
        }
        ;
        e.prototype.see = function(e) {
            var r = e.getContainer();
            var t = r.querySelector(".amap-layers");
            if (t) {
                t.appendChild(this.canvas)
            }
            AMap.DomUtil.addClass(this.canvas, "amap-labellayers");
            return this.canvas
        }
        ;
        return e
    }();
    var SmartTypedArray = M["SmartTypedArray"];
    var LabelsLayerBaseRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.type = "labelsLayer";
            e.wf = null;
            e.Ps = {};
            e._viewMode = "2D";
            e.Wf = {};
            e.Bf = {};
            e.TG = {};
            e.uu = [0, 0];
            return e
        }
        e.prototype.Hz = function(e, r, t, i, a, n) {
            if (!this.ac) {
                this._map = a;
                this._view = a.getView();
                this.ac = a.z_;
                this.Mf = a.R_;
                this._viewMode = r.viewState.viewMode;
                this.fz = a.fS();
                if (!this._map.isDOMMode()) {
                    if (!this.Zz) {
                        this.Zz = new LabelsRender
                    }
                } else {
                    if (!this.Vz) {
                        this.Vz = new LabelsRenderDOMRender
                    }
                }
                if (this.Zz) {
                    this.Zz.Mz({
                        map: a,
                        z_: a.z_,
                        R_: a.R_,
                        $i: e.Zn()
                    })
                }
            }
        }
        ;
        e.prototype.Jz = function() {
            return {
                centerCoord: this.uu
            }
        }
        ;
        e.prototype.vi = function(e) {
            if (this.fz) {
                var r = this.fz.vi(e, "");
                return r
            }
            return null
        }
        ;
        e.prototype.destroy = function() {
            if (this.Zz) {
                this.Zz.destroy();
                delete this.Zz
            }
            if (this.Vz) {
                this.Vz.destroy();
                delete this.Vz
            }
            this.AG()
        }
        ;
        e.prototype.reset = function() {
            if (this.Zz) {
                this.Zz.reset()
            }
            if (this.Vz) {
                this.Vz.reset()
            }
        }
        ;
        e.prototype.Yz = function(e, r, t, i, a) {
            this.Vz.Ce(e, r, t, i, a)
        }
        ;
        e.prototype.SG = function() {
            var e = typeof Int8Array !== "undefined";
            var r;
            if (e) {
                r = new SmartTypedArray("uint8",5e5);
                r.value.fill(0)
            }
            return r
        }
        ;
        e.prototype.zG = function(e) {
            if (!this.TG[e]) {
                this.TG[e] = {
                    iterator: 0
                }
            }
            var r = this.TG[e];
            if (!r.LG) {
                r.LG = this.SG()
            }
            if (!r.zz) {
                r.zz = []
            }
            return this.TG[e]
        }
        ;
        e.prototype.AG = function() {
            var e = this.TG || {};
            for (var r in e) {
                if (e.hasOwnProperty(r)) {
                    var t = e[r];
                    delete t.zz;
                    if (t.od) {
                        t.od.destroy();
                        delete t.od
                    }
                    t.iterator = 0;
                    if (t.LG) {
                        t.LG.value.fill(0)
                    }
                }
            }
        }
        ;
        return e
    }(LayerRender);
    var Util$1 = AMap["Util"];
    var transforms$1 = M["transform"];
    var lcs = M["geo"]["lcs"];
    var SmartArrayBuffer = M["SmartArrayBuffer"];
    var LabelsLayerRender = function(_) {
        __extends(e, _);
        function e() {
            var e = _.call(this) || this;
            e.type = "labelsLayer";
            e.l_ = "labelsLayer";
            return e
        }
        e.prototype.Hz = function(e, r, t, i, a, n) {
            if (!i || !r) {
                return
            }
            _.prototype.Hz.call(this, e, r, t, i, a, n);
            this.wf = r;
            var o = r.viewState;
            var f = o.projectionId;
            this.If = transforms$1[f];
            var s = o.zoom;
            if (r && n.visible && n.opacity && Util$1.dS(s, n.zooms)) {
                if (!i.Nf) {
                    return
                }
                if (!this._map.isDOMMode()) {
                    var u = e.context;
                    i.upload(u)
                }
                if (!i) {
                    return
                }
                var l = i.rp
                  , v = i.buffer
                  , c = i.zo;
                var h = o.centerCoord;
                this.uu = lcs.getLocalByCoord([h[0], h[1]]).center || [0, 0];
                var d = this.Ps = i;
                this.Ps.lp = l.up;
                return d
            }
            return null
        }
        ;
        e.prototype.renderFrame = function(e, r, t, i, a) {
            var n = this.fz;
            if (n) {
                this.Wf = n.Uf;
                this.Bf = n.$f
            }
            if (!this._map) {
                return
            }
            if (!this._map.isDOMMode()) {
                this.Jh(e, t, r, i, a)
            } else {
                var o = this.Ps.zo;
                var f = Util$1.hp(o, this.wf);
                this.Yz(t, i, r, this._map, {
                    Sa: f,
                    centerCoord: this.uu,
                    Uf: this.Wf,
                    $f: this.Bf
                })
            }
        }
        ;
        e.prototype.Jz = function() {
            var e = this.Ps.zo;
            var r = Util$1.hp(e, this.wf);
            var t = {
                Sa: r,
                from: "labelsLayer",
                opacity: 1,
                offset: [0, 0],
                centerCoord: this.uu
            };
            return t
        }
        ;
        e.prototype.Jh = function(e, r, t, i, a) {
            var n = this.Zz;
            var o = e.context;
            if (!t) {
                return
            }
            if (!t["other"]) {
                return
            }
            var f = t["other"]
              , s = f.rp
              , u = f.buffer
              , l = f.zo;
            var v = Util$1.hp(l, r);
            if (!s || !s.up) {
                return
            }
            this.AG();
            var c = this.zG(this.l_);
            var h = c.LG
              , d = c.od;
            var _ = CONSTS.sp;
            var g = s && s.pp;
            var y = g.length;
            var m = 0;
            var p = 0;
            var b = [];
            var x = this.Ps.style || {};
            for (var M = 0; M < y; M++) {
                var C = g[M];
                var S = C.dp || [];
                m = p;
                p = n.Dz(h, this.Wf, {
                    start: m,
                    dp: S,
                    style: x
                });
                b.push({
                    start: m,
                    end: p
                })
            }
            if (d) {
                d.update(h.value)
            } else {
                c.od = new SmartArrayBuffer(o,h.value,_)
            }
            c.zz = b;
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            n.Ce(r, o, {
                Rz: s,
                buffer: u,
                od: c.od.buffer,
                zz: c.zz
            }, {
                from: this.type,
                Sz: true,
                Sa: v,
                oo: e,
                LF: i,
                yZ: true,
                fee: a.uee
            })
        }
        ;
        return e
    }(LabelsLayerBaseRender);
    var utils = M["Util"];
    var LabelsRenderUtil$1 = M["LabelsRenderUtil"];
    var labelsUtil = M["labelsUtil"];
    var vector = M["vector"];
    var SmartArrayBuffer$1 = M["SmartArrayBuffer"];
    var SmartTypedArray$1 = M["SmartTypedArray"];
    var rbush = M["rbush"];
    var Support$2 = M["Support"];
    var LabelLine = function() {
        function e() {
            this._viewMode = "2D";
            this.Gf = rbush();
            this.Cf = null;
            this.u_ = null;
            this.Jp = 0;
            this.bo = zoomRange[1];
            this.Xf = {};
            this.Lf = new Uint8Array(1e5).fill(1);
            this.Qp = new SmartTypedArray$1("float32",1e5)
        }
        e.prototype.Ns = function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = r.DG
              , i = r.transform
              , a = r.view
              , n = r.centerCoord
              , o = r.FG
              , f = r.context
              , s = r.Uf
              , u = r.bounds
              , l = r.Iz;
            if (!this._map) {
                this._map = r.map;
                this.b$ = r.fz
            }
            this.wf = t;
            this.aa = t.viewState;
            this.If = i;
            this._view = a;
            this._viewMode = t.viewState.viewMode;
            this.uu = n;
            this.HG = o;
            this.Wf = s;
            this.bo = l;
            this.Kz();
            if (!this.Cf) {
                this.Cf = f.fe(this.Lf, 1)
            }
            this.Xf = r.tZ;
            this.Kf(e, r);
            this.Qf(this.Xf, r);
            return this.ip(f, r.DG)
        }
        ;
        e.prototype.destroy = function() {
            this.Kz();
            delete this.Xf;
            if (this.Cf && this.Cf.context) {
                this.Cf.context.deleteBuffer(this.Cf);
                delete this.Cf
            }
        }
        ;
        e.prototype.Kf = function(O, e) {
            if (e === void 0) {
                e = {}
            }
            var r = this.aa;
            var t = (r || {}).zoom
              , i = t === void 0 ? zoomRange[1] : t;
            var a = this.If;
            var n = this.Xf;
            var B = CONSTS.Color;
            var o = e.bounds;
            if (!r) {
                return
            }
            var f = {};
            var D = r.rotation || 0;
            for (var s = 0, u = O; s < u.length; s++) {
                var l = u[s];
                var v = l.rank;
                if (!f[v]) {
                    f[v] = []
                }
                f[v].push(l)
            }
            var c = Object.keys(f);
            var N = c.length;
            var h;
            var d;
            var _;
            var g;
            var y;
            var m;
            var p;
            var b;
            var x;
            var M;
            var C;
            var S;
            var w;
            var k;
            var T;
            var A;
            var U;
            var H;
            var z;
            var I;
            var G;
            var j = 0;
            var $;
            var V;
            var W;
            var q = this._map.getMapState().Tf;
            for (var F = N - 1; F >= 0; F--) {
                h = f[c[F]];
                for (var P = 0, K = h; P < K.length; P++) {
                    var L = K[P];
                    d = L.name;
                    V = L.$G;
                    _ = L.CO;
                    g = L.EG;
                    $ = L.BG;
                    y = L.path;
                    m = L.positionType || "absolute";
                    p = L.zooms;
                    b = L.rank;
                    x = L.style;
                    S = L.Bp;
                    W = L.extData;
                    M = LabelsRenderUtil$1.kz([[-L.Sa[0], -L.Sa[1]]], S, i, {
                        centerCoord: this.uu
                    })[0];
                    C = L.key;
                    M = LabelsRenderUtil$1.MG(S, M);
                    var R = L.extData && L.extData.mainkey && L.extData.subkey ? this._map.mapStyle.dn(L.extData.mainkey, L.extData.subkey, r.optimalZoom) : null;
                    w = x.fontSize;
                    k = x.fillColor;
                    T = x.strokeColor || B.Gp;
                    if (R && R[1]) {
                        w = R[1] ? R[1].fontSize : x.fontSize;
                        k = R[1].faceColor ? R[1].faceColor.normalize() : k;
                        T = R[1].borderColor ? R[1].borderColor.normalize() : T
                    }
                    A = w * 1.3;
                    U = this.Zp(y, A, {
                        zoom: i,
                        transform: a,
                        bounds: o,
                        Bp: S,
                        BG: $,
                        positionType: m
                    });
                    j = 0;
                    for (var E = 0, Y = U; E < Y.length; E++) {
                        var Z = Y[E];
                        H = Z.length || 0;
                        if (H < 2) {
                            continue
                        }
                        z = {
                            transform: a,
                            fontSize: w,
                            qp: A,
                            zoom: i,
                            name: d,
                            CO: _,
                            EG: g,
                            BG: $,
                            Sa: M,
                            bounds: o,
                            Tf: q,
                            nF: D
                        };
                        I = this.Yp(Z, z, j++ < 1);
                        if (I.Id) {
                            if (!I.pos.length || !I.pos[0].length) {
                                continue
                            }
                        }
                        G = {
                            name: d,
                            $G: V,
                            CO: _,
                            rank: b,
                            fontSize: w,
                            fillColor: k,
                            strokeColor: T,
                            zooms: p,
                            Sa: M,
                            key: C,
                            Bp: S,
                            extData: W
                        };
                        this.Vp(I, n, G)
                    }
                }
            }
            return n
        }
        ;
        e.prototype.Zp = function(e, r, t) {
            if (e === void 0) {
                e = []
            }
            if (r === void 0) {
                r = 20
            }
            if (t === void 0) {
                t = {}
            }
            var i = CONSTS.Dp
              , a = i.Ad
              , n = i.Bd;
            var o = t.zoom
              , f = t.Bp
              , s = t.BG
              , u = t.positionType;
            var l = 0;
            var v = 0;
            var c = 0;
            var h;
            var d;
            var _;
            var g = e.length - 1;
            var y = [[]];
            var m = o < 12 ? 10 : a;
            var p = this.aa;
            var b;
            var x;
            if (!p) {
                return y
            }
            var M = {
                viewState: p,
                size: this.HG,
                transform: this.If,
                view: this._view,
                viewMode: this._viewMode,
                centerCoord: this.uu,
                positionType: u
            };
            for (var C = 0; C < g - 1; C += 2) {
                var S = LabelsRenderUtil$1.kz([[e[C], e[C + 1]], [e[C + 2], e[C + 3]]], f, o, {
                    centerCoord: this.uu
                })
                  , w = S[0]
                  , k = S[1];
                _ = h;
                h = C === 0 ? this._viewMode === "2D" ? this.If.transform(w[0], w[1], o) : LabelsRenderUtil$1.wu(w[0], w[1], o, M) : d;
                d = this._viewMode === "2D" ? this.If.transform(k[0], k[1], o) : LabelsRenderUtil$1.wu(k[0], k[1], o, M);
                v = labelsUtil.Sd(h, d);
                c = l;
                l += v;
                var T = v / r;
                var A = void 0;
                if (l > m) {
                    if (c / m < .3 && v / m >= 2) {
                        h = labelsUtil.OG(h, d, m / v)
                    }
                    if (_) {
                        var I = vector.Ip(h, _);
                        var $ = vector.Ip(d, h);
                        A = vector.Qj(I, $)
                    } else {
                        A = 0
                    }
                    if (!b) {
                        b = labelsUtil.Ed([h, d], s);
                        x = b
                    } else {
                        b = labelsUtil.Ed([h, d], s)
                    }
                    if (A <= n && b === x && (T > 1 || o < 12 && T >= .6)) {
                        y[y.length - 1].push(h)
                    } else {
                        y[y.length - 1].push(h);
                        b = x = undefined;
                        var F = y[y.length - 1];
                        if (F[0] !== undefined) {
                            y.push([])
                        }
                    }
                    if (C === g - 3) {
                        y[y.length - 1].push(d)
                    }
                }
            }
            for (var C = 0; C < y.length; C++) {
                var P = y[C];
                var L = labelsUtil.Ed(P, s);
                if (L) {
                    y[C].reverse()
                }
            }
            return y
        }
        ;
        e.prototype.Yp = function(e, r, t) {
            if (e === void 0) {
                e = []
            }
            if (r === void 0) {
                r = {}
            }
            if (t === void 0) {
                t = false
            }
            var i = 0;
            var a = CONSTS.Dp;
            var O = a.BaseInterval
              , B = a.e1;
            var n = r.transform
              , o = r.fontSize
              , f = o === void 0 ? 12 : o
              , s = r.zoom
              , u = s === void 0 ? 20 : s
              , l = r.BG
              , D = l === void 0 ? 0 : l
              , v = r.CO
              , c = v === void 0 ? [] : v
              , h = r.EG;
            var d = {
                index: 0,
                Id: 0,
                pos: [],
                Dd: [],
                Pd: [],
                points: [],
                EG: []
            };
            var _ = r.qp || 24;
            var N = e.length;
            var g = c.length;
            var y;
            var m = false;
            var p;
            var b;
            var x;
            var M;
            var C;
            var S;
            var w;
            var k;
            var T = 0;
            var A = 0;
            var I = false;
            var $ = {
                viewMode: this._viewMode,
                transform: n,
                view: this._view
            };
            while (T < N - 1 && A < 30) {
                A++;
                var F = d.index;
                if (!d.pos[F]) {
                    d.pos[F] = [];
                    d.Pd[F] = [];
                    d.points[F] = [];
                    d.Dd[F] = []
                }
                var P = void 0;
                var L = void 0;
                var R = e[T + 1];
                S = labelsUtil.getAngle(e[T], R);
                if (!I) {
                    P = e[T]
                } else {
                    P = y
                }
                var U = c[d.Id];
                var H = h[U];
                var z = 24;
                var G = f / z;
                var E = D === "ch";
                _ = E ? f * 1.3 : H && H.char[4] * G * 1.3;
                if (m) {
                    y = labelsUtil.jd(y, e[T], e[T + 1], _)
                } else {
                    y = labelsUtil.Od(P, S, _)
                }
                p = d.index;
                if (d.Id < g && t) {
                    if (this._viewMode === "2D") {
                        b = n.Xs(P[0], P[1], u);
                        x = n.Xs(y[0], y[1], u)
                    } else {
                        b = LabelsRenderUtil$1.nt(P[0], P[1], u, $);
                        x = LabelsRenderUtil$1.nt(y[0], y[1], u, $)
                    }
                    M = labelsUtil.Fd(b, x, 1)[0];
                    L = labelsUtil.Fd(P, y, 1)[0];
                    C = false;
                    w = C ? labelsUtil.Nd(y, P, E ? "" : "x") : labelsUtil.Nd(P, y, E ? "" : "x");
                    d.pos[p].push(M);
                    d.points[p].push(L);
                    d.Pd[F].push(w);
                    d.Id++
                } else {
                    t = true;
                    d.index++;
                    d.Id = 0;
                    i++;
                    T = this.zd(e, T, i * O + g * B);
                    I = false;
                    m = false;
                    continue
                }
                if (d.Id === g - 1) {
                    d.pos[p].$d = false;
                    d.EG[p] = h
                }
                k = labelsUtil.Sd(y, R);
                if (k < _) {
                    T++;
                    m = true;
                    I = true
                } else {
                    I = true;
                    m = false
                }
            }
            return d
        }
        ;
        e.prototype.Vp = function(e, r, t) {
            if (e === void 0) {
                e = {}
            }
            if (t === void 0) {
                t = {}
            }
            var i = t.name
              , a = i === void 0 ? "" : i
              , n = t.$G
              , o = n === void 0 ? [] : n
              , f = t.CO
              , s = f === void 0 ? [] : f
              , u = t.rank
              , l = u === void 0 ? 1 : u
              , v = t.fontSize
              , c = v === void 0 ? 12 : v
              , h = t.fillColor
              , d = t.strokeColor
              , _ = t.zooms
              , g = t.Sa
              , y = t.key
              , m = t.Bp
              , p = t.extData
              , b = p === void 0 ? {} : p;
            var x = s.length;
            var O = e.pos.length;
            var M;
            var C;
            var S;
            var w;
            var k;
            var T = [];
            var A = [];
            var I = [];
            var $ = false;
            var F = 0;
            for (var P = 0; P < O; P++) {
                M = e.pos[P] || [];
                T = e.Pd[P] || [];
                I = e.EG[P] || [];
                C = M.$d;
                if (M.length === x) {
                    for (F = 0; F < x; F++) {
                        S = C ? x - 1 - F : F;
                        w = M[S];
                        if (!w || !this.Ud(T, F)) {
                            $ = true;
                            break
                        }
                        if (o && o.length) {
                            k = a.substring.apply(a, o.slice(F, F + 2))
                        } else {
                            k = a.substr(F, 1)
                        }
                        var L = {
                            data: {
                                id: utils.stamp(this),
                                position: w,
                                name: a,
                                extData: {
                                    Sa: g,
                                    key: y,
                                    Bp: m,
                                    CO: s.slice(F, F + 1),
                                    Lp: b.mainkey,
                                    Ap: b.subkey
                                },
                                txt: k,
                                rank: l
                            },
                            opts: {
                                type: "normal",
                                angle: T[S] || 0,
                                zooms: _,
                                text: {
                                    direction: "center",
                                    offset: [0, 0],
                                    EG: I,
                                    style: {
                                        fontSize: c,
                                        fillColor: h,
                                        strokeColor: d,
                                        strokeWidth: 2
                                    }
                                }
                            }
                        };
                        var R = this._map.getLayerByClass("AMap.MaskLayer");
                        if (R) {
                            var E = R.getRender();
                            if (E) {
                                if (!E.EH(L.data.position, this.wf)) {
                                    return false
                                }
                            }
                        }
                        A.push(L)
                    }
                    if (!$) {
                        if (!r[y]) {
                            r[y] = []
                        }
                        if (r[y].indexOf(A) < 0) {
                            r[y].push(A)
                        }
                    }
                }
            }
        }
        ;
        e.prototype.zd = function(e, r, t) {
            if (e === void 0) {
                e = []
            }
            if (r === void 0) {
                r = 0
            }
            if (t === void 0) {
                t = 0
            }
            var i = 0;
            var a = e.length - 1;
            var n = r;
            for (; n < a; n++) {
                var o = e[n];
                var f = e[n + 1];
                var s = labelsUtil.Sd(o, f);
                i += s;
                if (i >= t) {
                    break
                }
            }
            return n
        }
        ;
        e.prototype.Ud = function(e, r) {
            if (e === void 0) {
                e = []
            }
            if (r === void 0) {
                r = 0
            }
            var t = CONSTS.Dp.Bd;
            var i = r - 1;
            if (i < 0) {
                return true
            }
            var a = e[r];
            var n = e[i];
            if (Math.abs(a - n) >= t) {
                return false
            }
            return true
        }
        ;
        e.prototype.Qf = function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = this.aa;
            if (!t) {
                return
            }
            var i = t && t.zoom || 20;
            var a = this.b$;
            var n = t.zoom
              , o = n === void 0 ? zoomRange[1] : n;
            var f = r.bounds;
            var s = this.Gf;
            var u;
            var l;
            var v;
            var c = false;
            var h = 0;
            var d = [];
            for (var _ in e) {
                if (e.hasOwnProperty(_)) {
                    v = e[_];
                    for (var g = 0, y = v; g < y.length; g++) {
                        var m = y[g];
                        for (var p = 0, b = m; p < b.length; p++) {
                            var x = b[p];
                            var M = x.data;
                            var O = x.opts;
                            var C = M.txt;
                            var S = M.position;
                            var w = M.extData;
                            var k = w.Bp;
                            var B = LabelsRenderUtil$1.kz([vector.multiply(w.Sa, -1)], k, o, {
                                centerCoord: this.uu
                            })[0];
                            var T = w.Lp ? this._map.mapStyle.dn(w.Lp, w.Ap, i) : null;
                            var A = T && T[1];
                            var I = LabelsRenderUtil$1.gz(S, f);
                            u = [];
                            if (I && A) {
                                var $ = A && A.fontSize;
                                var F = LabelsRenderUtil$1.wu(S[0], S[1], o, {
                                    viewState: t,
                                    size: this.HG,
                                    transform: this.If,
                                    view: this._view,
                                    viewMode: this._viewMode,
                                    centerCoord: this.uu,
                                    positionType: "relative"
                                });
                                var P = this.Xp(F, $);
                                l = this.b$.iC(P, {
                                    padding: 5,
                                    id: C,
                                    type: "road"
                                });
                                u = a.search(l);
                                if (!u.length) {
                                    u = s.search(l)
                                }
                            }
                            if (!I || !A) {
                                c = true;
                                break
                            }
                            var L = 0;
                            var D = u.length;
                            while (L < D) {
                                if (this.Wf[u[L].id] || u[L].type === "road") {
                                    c = true;
                                    break
                                }
                                L++
                            }
                            if (l) {
                                d.push(l)
                            }
                        }
                        if (!c) {
                            h += m.length;
                            for (var R = 0, E = d; R < E.length; R++) {
                                var N = E[R];
                                s.Xc(N)
                            }
                            d = []
                        }
                        m.hide = c;
                        c = false
                    }
                }
            }
            this.Jp = h
        }
        ;
        e.prototype.ip = function(O, B) {
            var e;
            var D = CONSTS.sp
              , N = CONSTS.Kp
              , r = CONSTS.ud;
            var t = this.Xf || {};
            var U = this.aa;
            var H = 0;
            if (U && this._map) {
                var z = U.zoom;
                var i = 0;
                var a = 2;
                var G = this.bo;
                var j = this._map.getMapState().Af;
                var V = this.Jp;
                var n = r * 6;
                var o = void 0;
                if (V * n > 1e5) {
                    this.Qp.ff(V * n);
                    o = this.Qp.value
                } else {
                    o = this.Qp.value
                }
                var W = [];
                var q = 0;
                var f = void 0;
                var s = void 0;
                var u = void 0;
                var l = void 0;
                var v = void 0;
                var K = void 0;
                var Y = void 0;
                var c = void 0;
                var h = void 0;
                var Z = void 0;
                var X = void 0;
                var J = void 0;
                var d = void 0;
                var Q = void 0;
                var ee = void 0;
                var re = void 0;
                var te = void 0;
                var ie = void 0;
                var ae = void 0;
                var ne = void 0;
                var oe = void 0;
                var _ = void 0;
                var fe = void 0;
                var g = void 0;
                var y = void 0;
                var m = void 0;
                var p = void 0;
                var b = void 0;
                var x = void 0;
                var M = void 0;
                var C = void 0;
                var S = void 0;
                var se = void 0;
                var ue = void 0;
                var le = void 0;
                var ve = void 0;
                var w = void 0;
                var ce = void 0;
                var he = void 0;
                var k = void 0;
                var T = void 0;
                var A = void 0;
                var I = void 0;
                var de = void 0;
                var _e = void 0;
                var $ = void 0;
                var F = void 0;
                var P = void 0;
                var ge = void 0;
                var ye = 0;
                var me = void 0;
                var pe = void 0;
                var be = void 0;
                var L = 0;
                for (var xe in t) {
                    if (t.hasOwnProperty(xe)) {
                        var Me = t[xe];
                        q = Me.length;
                        for (var Ce = 0; Ce < q; Ce++) {
                            pe = Me[Ce];
                            if (pe.hide) {
                                continue
                            }
                            for (var Se = 0, we = pe; Se < we.length; Se++) {
                                var ke = we[Se];
                                f = ke.data;
                                s = ke.opts;
                                u = f.position;
                                l = f.extData;
                                be = l.CO;
                                me = l.Bp;
                                v = s.angle || 0;
                                K = s.zooms;
                                Y = s.text;
                                oe = Y.EG || {};
                                c = Y.style;
                                h = c.fontSize;
                                Z = c.fillColor;
                                X = c.strokeColor;
                                J = c.strokeWidth || 0;
                                d = this.td(h, J);
                                Q = d.gamma;
                                ee = d.ed;
                                te = d.backgroundColor;
                                ie = d.strokeWidth;
                                re = d.nd;
                                ae = 1;
                                for (var R = 0; R < ae; R++) {
                                    ne = be;
                                    _ = oe[ne] || {
                                        index: EnumTextureIndex.dynamic,
                                        char: (e = this._map.yH) === null || e === void 0 ? void 0 : e.LB(xe)[ne],
                                        pos: {
                                            startX: 0,
                                            startY: 0
                                        }
                                    };
                                    if (!_ || !_.char) {
                                        continue
                                    }
                                    fe = _.index;
                                    g = _.char;
                                    y = u[R];
                                    m = u[R + 1];
                                    if (!y || !m || !g) {
                                        continue
                                    }
                                    p = K[0];
                                    b = K[1];
                                    x = h / N.size;
                                    M = 1;
                                    if (j && G > z) {
                                        x = x / Math.pow(2, G - z)
                                    }
                                    C = g[0];
                                    S = g[1];
                                    se = g[2];
                                    ue = g[3];
                                    le = g[5];
                                    ve = g[6];
                                    w = N.buffer;
                                    var Te = h / 24 * (g[4] + CONSTS.ic);
                                    var Ae = {
                                        x: -Te / 2,
                                        y: -h / 2
                                    };
                                    if (C > 0 && S > 0) {
                                        C += w * 2;
                                        S += w * 2;
                                        ce = Ae.x;
                                        he = Ae.y;
                                        k = M * (ce + (se - w) * x);
                                        T = M * (ce + (se - w + C) * x);
                                        A = M * (he - ue * x);
                                        I = M * (he + (S - ue) * x);
                                        de = 0;
                                        _e = 0;
                                        de += _.pos.startX;
                                        _e += _.pos.startY;
                                        $ = de + le;
                                        F = _e + ve;
                                        P = fe;
                                        o.set([k, A, $, F, y, m, P, a, p, b, i, v, L, T, A, $ + C, F, y, m, P, a, p, b, i, v, L, k, I, $, F + S, y, m, P, a, p, b, i, v, L, k, I, $, F + S, y, m, P, a, p, b, i, v, L, T, A, $ + C, F, y, m, P, a, p, b, i, v, L, T, I, $ + C, F + S, y, m, P, a, p, b, i, v, L], ye);
                                        ye += n;
                                        ge = H * r * 6;
                                        W.push([h, [y, m], v, Z, X, Q, w, re, ee, te, ie, ge, ge + 6 * r, l.key, me]);
                                        H++
                                    }
                                }
                            }
                        }
                    }
                }
                var E = this.u_;
                if (E) {
                    E.update(o)
                } else {
                    E = this.u_ = new SmartArrayBuffer$1(O,o,D)
                }
                return {
                    pp: W,
                    zs: E,
                    od: this.Cf
                }
            }
            return {
                pp: [],
                od: null,
                zs: null
            }
        }
        ;
        e.prototype.Xp = function(e, r) {
            if (r === void 0) {
                r = 12
            }
            var t = r * 1.3;
            var i = r * 1.3;
            var a = e[0]
              , n = e[1];
            var o = [a - t / 2, n - i / 2];
            return {
                x: o[0],
                y: o[1],
                width: t,
                height: i
            }
        }
        ;
        e.prototype.td = function(e, r) {
            if (e === void 0) {
                e = 12
            }
            if (r === void 0) {
                r = 0
            }
            var t = CONSTS.Cd
              , i = CONSTS.Ld
              , a = CONSTS.Color;
            var n = DangerousFontSize;
            var o = 0;
            var f = e < 12 ? t : i;
            if (r) {
                r = r * Support$2.scale > 10 ? 10 : r;
                o = f * (1 - r / 10.1)
            }
            var s = e > n || Support$2.scale > 1 ? 1.7 : 1.5;
            s = s * 1.4142 / e;
            var u = f - 1 / 256 * (Support$2.scale - 1);
            return {
                gamma: s,
                nd: o,
                ed: u,
                backgroundColor: a.Gp,
                strokeWidth: r
            }
        }
        ;
        e.prototype.Kz = function() {
            if (this.Gf) {
                this.Gf.clear()
            }
        }
        ;
        return e
    }();
    var _a;
    var SmartArrayBuffer$2 = M["SmartArrayBuffer"];
    var Util$2 = AMap["Util"];
    var NebulaLabelFormat = M["NebulaLabelFormat"];
    var MapboxLabelFormat = M["MapboxLabelFormat"];
    var labelsUtil$1 = M["labelsUtil"];
    var LabelsRenderUtil$2 = M["LabelsRenderUtil"];
    var vector$1 = M["vector"];
    var transforms$2 = M["transform"];
    var lcs$1 = M["geo"]["lcs"];
    var LabelFormatMap = (_a = {},
    _a[NebulaLabelFormat.id] = NebulaLabelFormat,
    _a[MapboxLabelFormat.id] = MapboxLabelFormat,
    _a["nebula"] = NebulaLabelFormat,
    _a);
    var TileLabelsLayerRender = function(O) {
        __extends(e, O);
        function e() {
            var e = O.call(this) || this;
            e.type = "nebula-labelslayer";
            e.Xz = false;
            e.l_ = "inner";
            e.vf = [];
            e.mf = {};
            e.bo = 20;
            e.Xf = {};
            e.tp = {
                pp: [],
                od: null,
                zs: null
            };
            e.kp = {};
            e.NG = {};
            e.WG = new LabelLine;
            e.lee = new LabelStyleChangeManager;
            return e
        }
        e.prototype.Hz = function(e, r, t, i, a, n) {
            if (!i || !r) {
                return
            }
            O.prototype.Hz.call(this, e, r, t, i, a, n);
            var o = r.viewState.zoom;
            if (i && r && n.visible && n.opacity && Util$2.dS(o, n.zooms)) {
                var f = [];
                var s = true;
                var u = void 0;
                this.NG = {};
                for (var l = 0; l < i.length; l++) {
                    var v = i[l];
                    var c = v.tiles || [];
                    for (var h = 0; h < c.length; h++) {
                        var d = c[h];
                        f.push(d)
                    }
                    if (c.length) {
                        s = s && v.fo
                    }
                    u = v.Ca
                }
                var _ = {};
                for (var g = 0, y = f; g < y.length; g++) {
                    var m = y[g];
                    _[m.zo.key] = m
                }
                this.Xz = s;
                if (s) {
                    this.vf = Object.keys(_);
                    this.mf = _;
                    this.mf.yf = u || []
                }
                var p = this.vf;
                if (!p) {
                    return
                }
                var b = void 0;
                var x = void 0;
                var M = void 0;
                for (var C = 0, S = p; C < S.length; C++) {
                    var w = S[C];
                    x = this.mf[w];
                    var k = Array.isArray(x.data) ? x.data : [x.data] || [];
                    for (var T = 0, A = k; T < A.length; T++) {
                        M = A[T];
                        if (M && M.labels) {
                            b = labelsUtil$1.xf(M.labels, this.mf.yf);
                            if (!this._map.isDOMMode()) {
                                for (var I = 0, $ = b; I < $.length; I++) {
                                    var F = $[I];
                                    F.upload(e.context)
                                }
                            }
                            M.gf = b
                        }
                    }
                }
                var P = r.viewState;
                var L = P.projectionId
                  , R = P.centerCoord;
                this.If = transforms$2[L];
                this.uu = lcs$1.getLocalByCoord([R[0], R[1]]).center;
                var E = this.Ps = this.Pf(this.mf, e);
                return E
            }
            return null
        }
        ;
        e.prototype.renderFrame = function(e, r, t, i, a) {
            this.wf = t;
            var n = t.viewState;
            var o = this.vf;
            var f = this.Zz;
            if (!o || !n) {
                return
            }
            var s = e.context;
            var u = this.fz;
            var l = this._map;
            if (!l) {
                return
            }
            if (this._map.bZ.MZ.drawMode !== "fast" && (!this._map.bZ.dynamic.get("firstLabelDataAllLoaded") || !this._map.bZ.dynamic.get("firstAllLoaded"))) {
                return
            }
            var v = l.qz();
            var c = l.Qz();
            var h = this._map.isDOMMode();
            var d = r.tile[0].yZ;
            if (r.tile[1]) {
                d = d || r.tile[1].yZ
            }
            if (v) {
                this.Bf = u.$f;
                this.Wf = u.Uf;
                if (!h) {
                    this.Jh(e, t, o, {
                        yZ: d,
                        fee: a.uee
                    })
                } else {
                    this.Yz(t, i, r, this._map, {
                        centerCoord: this.uu,
                        Uf: this.Wf,
                        $f: this.Bf
                    })
                }
            }
            if (h) {
                return
            }
            var _ = this._map.TL();
            var g = this.Ps;
            if (c && d) {
                if (g.Vf && !_) {
                    this.Xf = {};
                    var y = LabelsRenderUtil$2.getBounds(n, {
                        zoom: n.zoom,
                        centerCoord: this.uu,
                        view: this._view,
                        transform: this.If,
                        viewMode: this._viewMode
                    });
                    this.bo = t.viewState.zoom;
                    var m = {
                        map: this._map,
                        fz: this.fz,
                        R_: this.Mf,
                        DG: t,
                        transform: this.If,
                        view: this._view,
                        centerCoord: this.uu,
                        FG: t.size,
                        context: s,
                        Uf: this.Wf,
                        bounds: y,
                        Iz: this.bo,
                        tZ: this.Xf
                    };
                    var p = this.Jf(this.mf);
                    this.tp = this.WG.Ns(p, m)
                }
                f.Cz(t, s, this.mf, {
                    $z: this.tp
                }, {
                    Az: this.vf,
                    Iz: this.bo,
                    centerCoord: this.uu,
                    yZ: d,
                    fee: a.uee
                })
            }
        }
        ;
        e.prototype.destroy = function() {
            delete this.tp;
            delete this.kp;
            delete this.vf;
            delete this.mf;
            if (this.WG && this.WG["destroy"]) {
                this.WG.destroy();
                delete this.WG
            }
            if (this.lee) {
                delete this.lee
            }
            O.prototype.destroy.call(this)
        }
        ;
        e.prototype.reset = function() {
            this.Xf = {};
            if (this.tp.zs) {
                this.tp.pp = []
            }
            O.prototype.reset.call(this)
        }
        ;
        e.prototype.Jh = function(e, r, t, i) {
            var a = this.Zz;
            var n = e.context;
            var o = this.Ps.lp || 0;
            if (!o) {
                return
            }
            this.qf(n, this.mf);
            var f;
            var s;
            var u;
            var l;
            for (var v = 0; v < t.length; v++) {
                var c = t[v];
                l = this.mf[c];
                if (l && l.data) {
                    f = Array.isArray(l.data) ? l.data : [l.data];
                    var h = l.type;
                    var d = this.zG(h);
                    var _ = d.od
                      , g = d.zz;
                    var y = d.iterator;
                    if (!_) {
                        continue
                    }
                    for (var m = 0, p = f; m < p.length; m++) {
                        s = p[m];
                        u = l.Sa;
                        var b = s.gf;
                        u = LabelsRenderUtil$2.MG(l.zo.z, u);
                        if (b) {
                            for (var x = 0; x < b.length; x++) {
                                var M = b[x];
                                y = a.Ce(r, e.context, {
                                    Rz: M.rp,
                                    buffer: M.buffer,
                                    od: _.buffer,
                                    zz: g
                                }, {
                                    iterator: y,
                                    Sa: u,
                                    oo: e,
                                    yZ: i.yZ,
                                    fee: i.fee
                                })
                            }
                        }
                    }
                    d.iterator = y
                }
            }
        }
        ;
        e.prototype.Pf = function(O, B) {
            var e = this.wf;
            var r = e && e.viewState;
            if (!r) {
                return {}
            }
            var t = r && r.optimalZoom || 20;
            var i = r && r.zoom || 20;
            var a = false;
            var n = 0;
            var D = LabelsRenderUtil$2.getBounds(r, {
                zoom: t,
                centerCoord: this.uu,
                view: this._view,
                transform: this.If,
                viewMode: this._viewMode
            });
            var N = CONSTS.Mp
              , U = CONSTS.Rp;
            var o;
            var f;
            var s;
            var u;
            var l;
            var v;
            var c;
            var h;
            var d;
            var _;
            var g;
            var y;
            var m;
            var p;
            var b;
            var x;
            var M;
            var H = t > 20 ? 20 : t < 3 ? 3 : t;
            var C = this.vf;
            var z = C.length;
            var G = [N, U];
            var S = {};
            var w = {};
            var k = {};
            var T = {};
            var A;
            var I;
            var $ = r.zoom;
            var j = true;
            for (var F = 0; F < z; F++) {
                u = O[C[F]];
                A = Array.isArray(u.data) ? u.data : [u.data];
                for (var P = 0, V = A; P < V.length; P++) {
                    I = V[P];
                    if (I && I.gf) {
                        var W = I.gf;
                        for (var L = 0, q = W; L < q.length; L++) {
                            l = q[L];
                            v = l.coords;
                            if (v) {
                                h = l.Nf;
                                _ = l.zp;
                                g = _[H] || [];
                                d = l.rp;
                                n += d.up;
                                for (var R = 0, K = g; R < K.length; R++) {
                                    var E = K[R];
                                    j = false;
                                    m = h[E];
                                    p = v[E];
                                    y = u.Sa;
                                    y = LabelsRenderUtil$2.MG(u.zo.z, y);
                                    x = vector$1.add(p.origin, y);
                                    M = LabelsRenderUtil$2.gz(x, D);
                                    if (!M && !S[E]) {
                                        if (!this.Bf[E]) {
                                            T[E] = 1
                                        }
                                        this.Bf[E] = 1;
                                        continue
                                    }
                                    o = h[E].data.extData;
                                    f = o.Lp ? this._map.mapStyle.dn(o.Lp, o.Ap, i) : null;
                                    s = m.opts || {};
                                    b = s.zooms || G;
                                    if (!S[E] && ($ < b[0] || $ > b[1] || o.Lp && !f)) {
                                        if (!this.Bf[E]) {
                                            T[E] = 1
                                        }
                                        this.Bf[E] = 1;
                                        continue
                                    }
                                    if (T[E]) {
                                        delete this.Bf[E]
                                    }
                                    c = l.style;
                                    S[E] = m;
                                    w[E] = p;
                                    k[E] = c[E];
                                    o.Sa = y;
                                    if (o.Lp === 40001) {
                                        k[E].showText = f ? f.showText === 0 ? false : true : true;
                                        k[E].showIcon = f ? f.iconID === "0" ? false : true : true;
                                        k[E].forceShow = f ? f.forceShow : false
                                    } else {
                                        k[E].showIcon = f ? f.iconID !== "0" : true;
                                        k[E].showText = f ? f.showText : true;
                                        k[E].forceShow = f ? f.forceShow : false
                                    }
                                    a = true
                                }
                            }
                            this.lee.vee(this._map.bZ.MZ.KY.iX);
                            this.lee.cee(l, this._map.mapStyle, l.Nf, l.style, i, this._map.bZ.MZ.KY.iX);
                            l.upload(B.context)
                        }
                    }
                }
            }
            if (j) {
                a = true
            }
            return {
                Vf: a,
                lp: n,
                Nf: S,
                coords: w,
                style: k
            }
        }
        ;
        e.prototype.qf = function(e, r) {
            var t = this.Zz;
            var i = this.vf;
            var a = this.Ps.lp || 0;
            var n = a / 12;
            var o = 1;
            var f = {};
            var s;
            var u;
            var l;
            var v;
            var c;
            var h;
            var d;
            var _;
            if (!n) {
                return
            }
            this.AG();
            for (var g = 0, y = i; g < y.length; g++) {
                var m = y[g];
                s = r[m];
                var p = s.type;
                var b = this.zG(p);
                b.LG.ff(n);
                var x = f[p];
                if (!x) {
                    x = f[p] = {
                        start: 0,
                        end: 0
                    }
                }
                if (s) {
                    u = Array.isArray(s.data) ? s.data : [s.data];
                    for (var M = 0, C = u; M < C.length; M++) {
                        l = C[M];
                        var S = l.gf;
                        var w = this.Ps.style || {};
                        if (S) {
                            for (var k = 0, T = S; k < T.length; k++) {
                                var A = T[k];
                                v = A.rp;
                                c = v && v.pp;
                                h = c.length;
                                for (var I = 0; I < h; I++) {
                                    d = c[I];
                                    _ = d.dp;
                                    x.start = x.end;
                                    x.end = t.Dz(b.LG, this.Wf, {
                                        dp: _,
                                        style: w,
                                        start: x.start
                                    });
                                    b.zz.push({
                                        start: x.start,
                                        end: x.end
                                    })
                                }
                            }
                        }
                    }
                }
            }
            var $ = this.TG;
            for (var F in $) {
                if ($.hasOwnProperty(F)) {
                    var P = $[F];
                    var L = P.od;
                    var R = P.LG.value;
                    if (!L) {
                        L = new SmartArrayBuffer$2(e,R,o)
                    } else {
                        L.update(R)
                    }
                    P.od = L
                }
            }
        }
        ;
        e.prototype.Jf = function(e) {
            var r = CONSTS.Op;
            var t = this.wf;
            if (!t) {
                return
            }
            var i = t.viewState;
            var a = i && i.zoom;
            var n = [];
            var o = this.vf;
            var f = o.length;
            var s = this.Xf;
            var u;
            var l;
            var v;
            var c;
            var h;
            var d;
            var _;
            var g;
            for (var y = 0; y < f; y++) {
                var m = e[o[y]];
                h = Array.isArray(m.data) ? m.data : [m.data];
                for (var p = 0, b = h; p < b.length; p++) {
                    d = b[p];
                    if (a && d && d.XE) {
                        u = d.road || d.Dh || [];
                        _ = d.XE;
                        l = m.Sa;
                        v = m.zo.key;
                        c = m.zo.z;
                        if (s[v]) {
                            continue
                        }
                        for (var x = 0, M = _; x < M.length; x++) {
                            var C = M[x];
                            g = LabelsRenderUtil$2.wu(C.distance, 0, a, {
                                viewState: i,
                                size: t.size,
                                transform: this.If,
                                view: this._view,
                                viewMode: this._viewMode,
                                centerCoord: this.uu
                            });
                            if (Math.abs(g[0] - g[1]) < r) {
                                continue
                            }
                            C.Sa = l;
                            C.key = v;
                            C.Bp = c;
                            n.push(C)
                        }
                    }
                }
            }
            return n
        }
        ;
        return e
    }(LabelsLayerBaseRender);
    var LabelStyleChangeManager = function() {
        function e() {
            this.hee = 0;
            this.dee = "";
            this._ee = undefined;
            this.gee = -1
        }
        e.prototype.vee = function(e) {
            if (e !== this.dee && this._ee && this.gee >= 0 && this.hee >= 0) {
                this.yee(this._ee.rp.rp, this.gee, this.hee, 0);
                this._ee.destroy();
                this._ee = undefined
            }
        }
        ;
        e.prototype.yee = function(e, r, t, i) {
            for (var a = 0; a < t; a++) {
                var n = (r + a) * 13 + 12;
                e[n] = i
            }
        }
        ;
        e.prototype.cee = function(e, r, t, i, a, n) {
            var o = 0;
            for (var f = 0, s = e.rp.pp; f < s.length; f++) {
                var u = s[f];
                var l = void 0;
                for (var v = 0, c = u.dp; v < c.length; v++) {
                    var h = c[v];
                    l = h.id;
                    var d = t[h.id].data.extData;
                    if (!d || !d.Lp || !d.Ap) {
                        continue
                    }
                    var _ = r.dn(d.Lp, d.Ap, a);
                    var g = i[h.id];
                    if (!_) {
                        o += h.kd;
                        continue
                    }
                    if (!g.text) {
                        o += h.kd;
                        continue
                    }
                    if (d.Lp === 40001) {
                        u.vd.VV("u_fontSizeFactor", 1)
                    } else {
                        var y = _.fontSize;
                        var m = g.text.style.fontSize;
                        var p = y / m;
                        u.vd.VV("u_fontSizeFactor", p)
                    }
                    var b = _ && _.faceColor && _.faceColor.normalize();
                    if (n && n === d.id && _["sel-highlight"]) {
                        this.yee(e.rp.rp, o, h.kd, 1);
                        this._ee = e;
                        this.gee = o;
                        this.hee = h.kd;
                        this.dee = n;
                        if (_["sel-textFillColor"]) {
                            u.vd.VV("u_highlightFillColor", _["sel-textFillColor"])
                        }
                        if (_["sel-textStrokeColor"]) {
                            u.vd.VV("u_highlightStrokeColor", _["sel-textStrokeColor"])
                        }
                        e.destroy()
                    }
                    if (b) {
                        u.vd.VV("fillColor", b)
                    }
                    var x = _.borderColor && _.borderColor.normalize();
                    if (x) {
                        u.vd.VV("strokeColor", x)
                    }
                    var M = _["holoColor"] && _["holoColor"].normalize();
                    var C = _["label-bg"];
                    if (C && M) {
                        u.vd.VV("backgroundColor", M)
                    } else {
                        u.vd.VV("backgroundColor", [0, 0, 0, 0])
                    }
                    o += h.kd
                }
            }
        }
        ;
        return e
    }();
    var fillExtFragmentString = "precision mediump float;\n\n// uniform vec4 u_roofColor;\n// uniform vec4 u_wallColor;\nuniform vec3 u_lightColor;\nuniform vec3 u_lightDir;\nuniform mat4 u_normalMatrix;\nuniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;\nuniform vec2 u_skyBMRange;\nuniform bool u_isPick;\n\nvarying vec3 v_normal;\nvarying vec4 v_color;\n// varying vec4 v_color;\n// varying float v_flag;\nvarying float v_fullHeight;\nvarying float v_fragHeight;\nvarying vec4 v_roofColor;\nvarying vec4 v_pos;\n\nvoid main() {\n    // 环境光\n    vec3 ambientLightColor = vec3(1, 1, 1);\n    float ambientStrength = 0.92;\n    vec3 ambient = ambientStrength * ambientLightColor;\n\n    // 平行光的漫反射\n    // vec3 norm = normalize(vec3(u_normalMatrix * vec4(v_normal, 1)));  //\n    vec3 lightDir = u_lightDir;\n    vec3 lightColor = vec3(1, 1, 1);\n    float lightStrength = 0.1;\n    vec3 norm = normalize(v_normal);\n    float nDotL = max(dot(lightDir, norm), 0.0);\n    vec3 diff = lightStrength * lightColor * nDotL;\n\n    // 如果是顶面，不同建筑物的顶面透明度是要有差异的\n    float topOpacity = 1.0;\n    if(v_fragHeight >= v_fullHeight) {\n        // topOpacity = clamp((1.3 - v_fullHeight / 50.0), 0.9, 1.3);\n    }\n\n    // 根据楼高判断颜色\n    if(v_fragHeight >= v_fullHeight) {\n        gl_FragColor = v_roofColor;\n        // gl_FragColor = u_roofColor;\n    } else {\n        // 渐变色，暂时不用\n        // float hFactor = v_fragHeight / v_fullHeight / 8.0 + 1.0;\n        gl_FragColor = v_color;\n    }\n\n    gl_FragColor.rgb *= diff + ambient;\n    gl_FragColor.a *= u_opacity * topOpacity;\n\n    // 雾化\n    // float y = v_pos.y / v_pos.w;\n    // float fogHeight = 2. / u_viewHeight * 10.;  // 20 像素高度作为模糊处理\n    // vec3 fogColor = vec3(0.95, 0.85, 0.95);\n    if(u_skyHeight < 1.0) {\n        // float fogFactor = smoothstep(u_skyHeight, u_skyHeight - fogHeight, y);\n        // gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        // gl_FragColor.a *= fogFactor;\n\n        float cameraToFragDist = gl_FragCoord.z * v_pos.w;\n        float fogFactor = smoothstep(u_skyBMRange.y, u_skyBMRange.x, cameraToFragDist);\n        gl_FragColor.a *= fogFactor;\n    }\n    if(u_isPick) {\n        gl_FragColor = vec4(v_roofColor.rgba / 255.0);\n    }\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var fillExtVertextString = "precision highp float;\nattribute vec4 a_pos;\n// attribute vec4 a_color;\n// attribute float a_flag;\nattribute vec3 a_normal;\n\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nuniform float u_pitch;\nuniform float u_heightFactor;\nuniform vec4 u_roofColor;\nuniform vec4 u_wallColor;\n\nvarying vec3 v_normal;\nvarying vec4 v_color;\nvarying vec4 v_roofColor;\n// varying vec4 v_color;\n// varying float v_height;\n// varying float v_flag;\nvarying float v_fullHeight;\nvarying float v_fragHeight;\nvarying vec4 v_pos;\n\nvoid main() {\n    vec3 pos = vec3(\n        a_pos.x + u_localDeltaCenter.x + u_offset.x,\n        a_pos.y + u_localDeltaCenter.y + u_offset.y,\n        // a_pos.z * u_heightFactor * min((u_pitch + 20.0) / 50.0, 0.8)\n        a_pos.z * u_heightFactor\n    );\n    gl_Position = u_matrix * vec4(pos.xyz, 1);\n\n    // v_color = a_color;\n    // v_flag = a_flag;\n    v_pos = gl_Position;\n    // vec4(a_pos, 1);\n    v_fragHeight = a_pos.z;\n    v_fullHeight = a_pos.w;\n    // v_height = pos.z / a_pos.w;\n    v_normal = normalize(a_normal);\n    // 1.4 拿过来的\n    v_roofColor = u_roofColor;\n    v_color = u_roofColor + (u_wallColor - u_roofColor) * (a_pos.z == 0.0 ? 1.0 : 0.6);\n}\n";
    var fillExtPatternFragmentString = "precision mediump float;\n\n// uniform vec4 u_roofColor;\n// uniform vec4 u_wallColor;\nuniform vec3 u_lightColor;\nuniform vec3 u_lightDir;\nuniform mat4 u_normalMatrix;\nuniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;\nuniform vec2 u_skyBMRange;\n\n// pattern\nuniform float u_resolution;\nuniform sampler2D u_image;\nuniform vec2 u_imageSize;\n\nvarying vec3 v_normal;\nvarying vec4 v_color;\n// varying vec4 v_color;\n// varying float v_flag;\nvarying float v_fullHeight;\nvarying float v_fragHeight;\nvarying vec4 v_roofColor;\nvarying vec4 v_pos;\nvarying vec3 v_coord;\nvarying vec2 v_textureCoord;\n\n\nvoid main() {\n    // 环境光\n    vec3 ambientLightColor = vec3(1, 1, 1);\n    float ambientStrength = 0.92;\n    vec3 ambient = ambientStrength * ambientLightColor;\n\n    // 平行光的漫反射\n    // vec3 norm = normalize(vec3(u_normalMatrix * vec4(v_normal, 1)));  //\n    vec3 lightDir = u_lightDir;\n    vec3 lightColor = vec3(1, 1, 1);\n    float lightStrength = 0.1;\n    vec3 norm = normalize(v_normal);\n    float nDotL = max(dot(lightDir, norm), 0.0);\n    vec3 diff = lightStrength * lightColor * nDotL;\n\n    // 如果是顶面，不同建筑物的顶面透明度是要有差异的\n    float topOpacity = 1.0;\n    if(v_fragHeight >= v_fullHeight) {\n        // topOpacity = clamp((1.3 - v_fullHeight / 50.0), 0.9, 1.3);\n    }\n\n    // 根据楼高判断颜色\n    if(v_fragHeight >= v_fullHeight) {\n        // vec2 v_texture_pos = mod(vec2(v_coord.x,v_coord.y*-1.0)/u_resolution/u_imageSize,1.0);\n        // gl_FragColor = texture2D(u_image, v_texture_pos);\n        gl_FragColor = v_roofColor;\n    } else if(v_textureCoord.x<0.0){\n        gl_FragColor = v_color;\n      \n    }else{\n        vec2 v_texture_pos = v_textureCoord;\n        gl_FragColor = texture2D(u_image, v_texture_pos);\n        gl_FragColor.a *= v_color.a;\n    }\n\n    gl_FragColor.rgb *= diff + ambient;\n    gl_FragColor.a *= u_opacity * topOpacity;\n\n    // 雾化\n    // float y = v_pos.y / v_pos.w;\n    // float fogHeight = 2. / u_viewHeight * 10.;  // 20 像素高度作为模糊处理\n    // vec3 fogColor = vec3(0.95, 0.85, 0.95);\n    if(u_skyHeight < 1.0) {\n        // 片元颜色 = 物体颜色 * 雾化因子 + 雾的颜色 * （1 - 雾化因子）\n        // float fogFactor = smoothstep(u_skyHeight, u_skyHeight - fogHeight, y);\n        // gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        // gl_FragColor.a *= fogFactor;\n\n        float cameraToFragDist = gl_FragCoord.z * v_pos.w;\n        float fogFactor = smoothstep(u_skyBMRange.y, u_skyBMRange.x, cameraToFragDist);\n        gl_FragColor.a *= fogFactor;\n    }\n\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var fillExtPatternVertextString = "precision highp float;\nattribute vec4 a_pos;\n// attribute vec4 a_color;\n// attribute float a_flag;\nattribute vec3 a_normal;\nattribute vec2 a_textureCoord;\n\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nuniform float u_pitch;\nuniform float u_heightFactor;\nuniform vec4 u_roofColor;\nuniform vec4 u_wallColor;\n\nvarying vec3 v_normal;\nvarying vec4 v_color;\nvarying vec4 v_roofColor;\n// varying vec4 v_color;\n// varying float v_height;\n// varying float v_flag;\nvarying float v_fullHeight;\nvarying float v_fragHeight;\nvarying vec4 v_pos;\nvarying vec3 v_coord;\nvarying vec2 v_textureCoord;\n\n\nvoid main() {\n    vec3 pos = vec3(\n        a_pos.x + u_localDeltaCenter.x + u_offset.x,\n        a_pos.y + u_localDeltaCenter.y + u_offset.y,\n        // a_pos.z * u_heightFactor * min((u_pitch + 20.0) / 50.0, 0.8)\n        a_pos.z * u_heightFactor\n    );\n    gl_Position = u_matrix * vec4(pos.xyz, 1);\n\n    // v_color = a_color;\n    // v_flag = a_flag;\n    v_coord = pos;\n    v_textureCoord = a_textureCoord;\n    v_pos = gl_Position;\n    // vec4(a_pos, 1);\n    v_fragHeight = a_pos.z;\n    v_fullHeight = a_pos.w;\n    // v_height = pos.z / a_pos.w;\n    v_normal = normalize(a_normal);\n    // 1.4 拿过来的\n    v_roofColor = u_roofColor;\n    v_color = u_roofColor + (u_wallColor - u_roofColor) * (a_pos.z == 0.0 ? 1.0 : 0.6);\n}\n";
    var Color = M["Color"];
    function linearColor(e, i, a) {
        var n = [0, 0, 0, 1];
        forEach(e.rgba, function(e, r) {
            var t = i.rgba[r];
            n[r] = linearNumber(e, t, a) / 255
        });
        return n
    }
    function linearNumber(e, r, t) {
        var i = r - e;
        return e + i * t
    }
    var Property = function() {
        function e(e, r, t) {
            if (t === void 0) {
                t = "other"
            }
            this.key = e;
            this.type = r;
            this.sort = t
        }
        e.prototype.An = function(e, r, t, i, a) {
            var n = this.Tn(e, r, t, i);
            if (!n) {
                return null
            }
            var o = n[this.key];
            if (o && o.rgba) {
                return this.In(o.rgba)
            } else {
                return o
            }
        }
        ;
        e.prototype.In = function(e) {
            return [e[0] / 255, e[1] / 255, e[2] / 255, e[3] / 255]
        }
        ;
        e.prototype.Dn = function(e, r, t, i, a) {
            var n = Math.floor(i);
            var o = Math.ceil(i);
            var f = i - n < .8 ? n : o;
            var s = this.Tn(e, r, t, n);
            var u = this.Tn(e, r, t, o);
            if (!s && f === n) {
                return undefined
            }
            if (!s && !u) {
                return undefined
            }
            if (s && !u && f === o) {
                return undefined
            }
            if (!s && u && n === f) {
                return undefined
            }
            var l = i - n;
            if (!s) {
                l = (l - .8) / .2
            }
            l = Math.max(l, 0);
            if (this.type === "const") {
                return this.Pn(e, r, t, f)
            }
            if (this.type === "linear" && n === o) {
                return this.Pn(e, r, t, n)
            }
            if (!s && u) {
                var v = u[this.key];
                if (typeof v === "number") {
                    var c = 0;
                    return linearNumber(c, v, l)
                }
                var h = v.normalize();
                if (h && this.sort !== "line") {
                    h[3] = h[3] * l
                }
                return h
            }
            if (s && !u) {
                return this.Pn(e, r, t, n)
            }
            var d = s[this.key];
            var _ = u[this.key];
            if (!d) {
                if (typeof _ === "number") {
                    var g = 0;
                    return linearNumber(g, _, l)
                }
                var h = _.normalize();
                if (this.sort !== "line") {
                    h[3] = h[3] * l
                }
                return h
            } else if (typeof d === "number") {
                return linearNumber(d, _, l)
            } else {
                return linearColor(d, _, l)
            }
        }
        ;
        e.prototype.Pn = function(e, r, t, i) {
            var a;
            switch (this.sort) {
            case "line":
                a = this.jn(e, r, t, i);
                break;
            case "other":
                a = this.On(e, r, t, i);
                break;
            default:
                a = this.On(e, r, t, i)
            }
            return a
        }
        ;
        e.prototype.Tn = function(e, r, t, i) {
            var a;
            switch (this.sort) {
            case "line":
                a = this.Fn(e, r, t, i);
                break;
            case "other":
                a = this.En(e, r, t, i);
                break;
            default:
                a = this.En(e, r, t, i)
            }
            return a
        }
        ;
        e.prototype.Fn = function(e, r, t, i) {
            var a = e.dn(r, t, i);
            if (this.Nn(a)) {
                return a[0]
            }
            return undefined
        }
        ;
        e.prototype.En = function(e, r, t, i) {
            var a = e.dn(r, t, i);
            if (this.Nn(a)) {
                return a
            }
            return undefined
        }
        ;
        e.prototype.jn = function(e, r, t, i) {
            var a = Math.floor(i);
            var n = Math.ceil(i);
            var o = e.dn(r, t, a);
            if (o) {
                var f = o[0][this.key];
                if (f && f.normalize) {
                    return f.normalize()
                }
                return f
            } else {
                var s = e.dn(r, t, n);
                var f = s[0][this.key];
                if (f && f.normalize) {
                    return f.normalize()
                }
                return f
            }
        }
        ;
        e.prototype.On = function(e, r, t, i) {
            var a = Math.floor(i);
            var n = Math.ceil(i);
            var o = e.dn(r, t, a);
            if (o) {
                var f = o[this.key];
                if (f instanceof Color) {
                    return f.normalize()
                }
                return f
            } else {
                var s = e.dn(r, t, n);
                var f = s[this.key];
                if (f instanceof Color) {
                    return f.normalize()
                }
                return f
            }
        }
        ;
        return e
    }();
    var LineProperty = function(i) {
        __extends(e, i);
        function e(e, r) {
            var t = i.call(this, e, r, "line") || this;
            t.key = e;
            t.type = r;
            return t
        }
        e.prototype.Nn = function(e) {
            return e && e[0].roadColor
        }
        ;
        return e
    }(Property);
    var LineBorderProperty = function(i) {
        __extends(e, i);
        function e(e, r) {
            var t = i.call(this, e, r, "line") || this;
            t.key = e;
            t.type = r;
            return t
        }
        e.prototype.Nn = function(e) {
            return e && e[0].borderColor
        }
        ;
        return e
    }(Property);
    var FillProperty = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Nn = function(e) {
            return e && e.faceColor
        }
        ;
        return r
    }(Property);
    var BuildingProperty = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Nn = function(e) {
            return e && e.faceColor
        }
        ;
        return r
    }(Property);
    var BuildingBorderProperty = function(e) {
        __extends(r, e);
        function r() {
            return e !== null && e.apply(this, arguments) || this
        }
        r.prototype.Nn = function(e) {
            return e && e.wallColor2
        }
        ;
        return r
    }(Property);
    var fillExtUniforms = {
        u_skyBMRange: "vec2",
        u_skyHeight: "float",
        u_viewHeight: "float",
        u_heightFactor: "float",
        u_roofColor: "vec4",
        u_wallColor: "vec4",
        u_lightDir: "vec3",
        u_opacity: "float",
        u_pitch: "float",
        u_offset: "vec2",
        u_matrix: "mat4",
        u_normalMatrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_isPick: "bool"
    };
    var fillExtAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec4"
        },
        a_normal: {
            Me: "float32",
            Oe: "vec3"
        }
    };
    var fillExt = {
        uniforms: fillExtUniforms,
        attributes: fillExtAttributes,
        vertexSource: fillExtVertextString,
        fragmentSource: fillExtFragmentString
    };
    var getFillExtUniformValues = function(e, r, t, i, a, n, o) {
        if (t === void 0) {
            t = [0, 0]
        }
        var f = e.viewState.zoom;
        var s = e.viewState.pitch;
        var u = e.viewState.size[1];
        var l = [16, zoomRange[1]];
        var v = 1;
        var c = 1;
        if (i.layer.CLASS_NAME === "AMap.Buildings") {
            l = i.zooms;
            v = i.opacity;
            c = i.heightFactor
        }
        c = c * Math.min((s + 20) / 50, .8);
        if (i.layer.CLASS_NAME === "AMap.MapboxVectorTileLayer" || i.layer.CLASS_NAME === "AMap.VectorLayer") {
            l = i.zooms;
            v = i.opacity;
            c = 1
        }
        if (l[0] > f || l[1] < f) {
            return
        }
        return {
            u_skyBMRange: o,
            u_skyHeight: n,
            u_viewHeight: u,
            u_heightFactor: c,
            u_roofColor: a.roofColor,
            u_wallColor: a.wallColor,
            u_lightDir: calcSunX(e.viewState.center),
            u_opacity: v,
            u_pitch: s,
            u_matrix: e.viewState.mvpMatrix,
            u_offset: [0, 0],
            u_normalMatrix: e.viewState.modelMatrix,
            u_localDeltaCenter: t,
            u_isPick: false
        }
    };
    function calcSunX(e) {
        var r = new Date;
        var t = r.getHours() + r.getMinutes() / 60;
        var i = -15 * t + 300;
        var a = Math.sin(e[0] - i);
        return [a, 1, .75]
    }
    var fillExtPatternUniforms = {
        u_skyBMRange: "vec2",
        u_skyHeight: "float",
        u_viewHeight: "float",
        u_heightFactor: "float",
        u_roofColor: "vec4",
        u_wallColor: "vec4",
        u_lightDir: "vec3",
        u_opacity: "float",
        u_pitch: "float",
        u_offset: "vec2",
        u_matrix: "mat4",
        u_normalMatrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_image: "sampler2D",
        u_imageSize: "vec2",
        u_resolution: "float"
    };
    var fillExtPatternAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec4"
        },
        a_normal: {
            Me: "float32",
            Oe: "vec3"
        },
        a_textureCoord: {
            Me: "uint16",
            Oe: "vec2"
        }
    };
    var fillExtPattern = {
        uniforms: fillExtPatternUniforms,
        attributes: fillExtPatternAttributes,
        vertexSource: fillExtPatternVertextString,
        fragmentSource: fillExtPatternFragmentString
    };
    var fillExtProperties = {
        texture: new BuildingProperty("texture","const")
    };
    var getFillExtPatternUniformValues = function(e, r, t, i, a, n, o, f) {
        if (t === void 0) {
            t = [0, 0]
        }
        var s = e.map.bZ.MZ.drawMode === "fast" ? e.viewState.optimalZoom : e.viewState.zoom;
        var u = e.viewState.pitch;
        var l = e.viewState.size[1];
        var v = [16, zoomRange[1]];
        var c = 1;
        var h = 1;
        var d;
        if (i.layer.CLASS_NAME === "AMap.Buildings") {
            v = i.zooms;
            c = i.opacity;
            h = i.heightFactor
        }
        h = h * Math.min((u + 20) / 50, .8);
        if (i.layer.CLASS_NAME === "AMap.MapboxVectorTileLayer" || i.layer.CLASS_NAME === "AMap.VectorLayer") {
            v = i.zooms;
            c = i.opacity;
            d = r.bz;
            h = 1
        }
        if (v[0] > s || v[1] < s) {
            return
        }
        var _ = r.mainkey
          , g = r.subkey
          , y = r.minzoom;
        if (f) {
            d = fillExtProperties.texture.An(f, _, g, s, y)
        }
        if (!d) {
            return
        }
        return {
            u_skyBMRange: o,
            u_skyHeight: n,
            u_viewHeight: l,
            u_heightFactor: h,
            u_roofColor: a.roofColor,
            u_wallColor: a.wallColor,
            u_lightDir: calcSunX(e.viewState.center),
            u_opacity: c,
            u_pitch: u,
            u_matrix: e.viewState.mvpMatrix,
            u_offset: [0, 0],
            u_normalMatrix: e.viewState.modelMatrix,
            u_localDeltaCenter: t,
            u_image: d,
            u_imageSize: [1, 1],
            u_resolution: e.viewState.optimalResolution
        }
    };
    var find$1 = M["lodash"]["find"];
    var map$1 = M["lodash"]["map"];
    var TileState = M["TileState"];
    var vector$2 = M["vector"];
    var BuildingRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            if (!r.s_) {
                return
            }
            if (!a.map.mapStyle.vn) {
                return
            }
            var n = r.tiles;
            var o = {};
            for (var f = 0, s = n; f < s.length; f++) {
                var u = s[f];
                if (u.status === TileState.LOADED) {
                    var l = u.zo.z;
                    if (!o[l]) {
                        o[l] = []
                    }
                    o[l].push(u)
                }
            }
            var v = i.layer;
            var c = v.uo.getSource(v.co());
            var h = c.ra(t.viewState.optimalZoom);
            var d = map$1(Object.keys(o), function(e) {
                return parseInt(e, 10)
            });
            d.sort(function(e, r) {
                return e - r
            });
            var _ = d.indexOf(h);
            if (_ >= 0) {
                d.splice(_, 1);
                d.push(h)
            }
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            for (var g = 0, y = d; g < y.length; g++) {
                var m = y[g];
                var p = o[m];
                for (var b = 0, x = p; b < x.length; b++) {
                    var u = x[b];
                    if (u.status !== TileState.LOADED) {
                        continue
                    }
                    this.S_(e, u, t, i, a, true)
                }
                for (var M = 0, C = p; M < C.length; M++) {
                    var u = C[M];
                    if (u.status !== TileState.LOADED) {
                        continue
                    }
                    this.S_(e, u, t, i, a)
                }
            }
        }
        ;
        e.prototype.S_ = function(e, r, t, i, a, n) {
            if (n === void 0) {
                n = false
            }
            var o = r.data;
            var f = t.map;
            var O = t.viewState;
            var s = t.viewState.size;
            var u = f.getView();
            var l = {
                roofColor: i.roofColor,
                wallColor: i.wallColor
            };
            var v = f.getBuildingColor();
            var c = 1;
            if (t.viewState.viewMode === "3D") {
                c = u.EF()
            } else {
                return
            }
            var h = u.X(s[0] / 2, u.Uu() + 5);
            var d = u.X(s[0] / 2, u.Uu() - 5);
            var _ = u.pz().getPosition();
            var g = h[0] - _[0];
            var y = h[1] - _[1];
            var m = d[0] - _[0];
            var p = d[1] - _[1];
            var b = [Math.sqrt(g * g + y * y), Math.sqrt(m * m + p * p)];
            for (var x = 0, M = o; x < M.length; x++) {
                var C = M[x];
                var S = C.A_;
                if (!S) {
                    continue
                }
                var B = C.C_;
                S.upload(e.context, true);
                var w = S.zs;
                var k = find$1(t.map.getLayers(), function(e) {
                    return e.CLASS_NAME === "AMap.IndoorMap"
                });
                var T = void 0;
                if (k) {
                    T = k.yD().show
                }
                for (var A = 0, I = B.jc; A < I.length; A++) {
                    var $ = I[A];
                    var F = f.mapStyle.vn.dn($.mainkey, $.subkey, O.zoom);
                    var P = {};
                    P.roofColor = $.roofColor || l.roofColor || v.roofColor || F && F.faceColor && vector$2.Up(F.faceColor.rgba, 255);
                    P.wallColor = $.wallColor || l.wallColor || v.wallColor || F && F.faceColor && vector$2.Up(F.borderColor.rgba, 255);
                    if (!P.wallColor || !P.roofColor) {
                        return
                    }
                    var D = fillExtProperties.texture.An(a.map.mapStyle, $.mainkey, $.subkey, t.viewState.zoom, $.minzoom);
                    if (!k || !($.bz && T)) {
                        if (D) {
                            var L = getFillExtPatternUniformValues(t, $, r.Sa, i, P, c, b, a.map.mapStyle);
                            if (L) {
                                var R = a.map.getImage(L.u_image);
                                if (R) {
                                    L.u_image = R;
                                    L.u_imageSize = R.size;
                                    var E = e.sH();
                                    E.Ce(L, {
                                        a_pos: {
                                            type: "vec4",
                                            Re: w.size,
                                            offset: 0,
                                            buffer: w
                                        },
                                        a_normal: {
                                            type: "vec3",
                                            Re: w.size,
                                            offset: 4 * 4,
                                            buffer: w
                                        },
                                        a_textureCoord: {
                                            type: "vec2",
                                            Re: w.size,
                                            offset: 4 * 7,
                                            buffer: w
                                        }
                                    }, $.length, undefined, "TRIANGLES", i.depthTest, n ? ColorMode.disabled : ColorMode.Si, undefined, CullFaceMode.back, $.offset)
                                } else if (L.u_image && a.map.I_ && !a.map.I_.MH(L.u_image)) {
                                    a.map.addImage(L.u_image, {
                                        url: L.u_image,
                                        filter: "LINEAR",
                                        wrap: "REPEAT",
                                        cb: function() {
                                            return a.map.setNeedUpdate(true)
                                        }
                                    })
                                }
                            }
                        } else {
                            var L = getFillExtUniformValues(t, $, r.Sa, i, P, c, b);
                            if (L) {
                                var E = e.k_();
                                E.Ce(L, {
                                    a_pos: {
                                        type: "vec4",
                                        Re: w.size,
                                        offset: 0,
                                        buffer: w
                                    },
                                    a_normal: {
                                        type: "vec3",
                                        Re: w.size,
                                        offset: 4 * 4,
                                        buffer: w
                                    }
                                }, $.length, undefined, "TRIANGLES", i.depthTest, n ? ColorMode.disabled : ColorMode.Si, undefined, CullFaceMode.back, $.offset)
                            }
                        }
                    }
                }
            }
        }
        ;
        return e
    }(LayerRender);
    var isSupportFloat32Array = typeof Float32Array !== "undefined";
    if (!isSupportFloat32Array) {
        window["Float32Array"] = undefined;
        setMatrixArrayType(Array)
    }
    var EPSILON = 1e-6;
    var ARRAY_TYPE = typeof Float32Array !== "undefined" ? Float32Array : Array;
    function setMatrixArrayType(e) {
        ARRAY_TYPE = e
    }
    if (!Math.hypot)
        Math.hypot = function() {
            var e = 0
              , r = arguments.length;
            while (r--) {
                e += arguments[r] * arguments[r]
            }
            return Math.sqrt(e)
        }
        ;
    function equals(e, r) {
        var t = e[0]
          , i = e[1]
          , a = e[2]
          , n = e[3];
        var o = e[4]
          , f = e[5]
          , s = e[6]
          , u = e[7];
        var l = e[8]
          , v = e[9]
          , c = e[10]
          , h = e[11];
        var d = e[12]
          , _ = e[13]
          , g = e[14]
          , y = e[15];
        var m = r[0]
          , p = r[1]
          , b = r[2]
          , x = r[3];
        var M = r[4]
          , C = r[5]
          , S = r[6]
          , w = r[7];
        var k = r[8]
          , T = r[9]
          , A = r[10]
          , I = r[11];
        var $ = r[12]
          , F = r[13]
          , P = r[14]
          , L = r[15];
        return Math.abs(t - m) <= EPSILON * Math.max(1, Math.abs(t), Math.abs(m)) && Math.abs(i - p) <= EPSILON * Math.max(1, Math.abs(i), Math.abs(p)) && Math.abs(a - b) <= EPSILON * Math.max(1, Math.abs(a), Math.abs(b)) && Math.abs(n - x) <= EPSILON * Math.max(1, Math.abs(n), Math.abs(x)) && Math.abs(o - M) <= EPSILON * Math.max(1, Math.abs(o), Math.abs(M)) && Math.abs(f - C) <= EPSILON * Math.max(1, Math.abs(f), Math.abs(C)) && Math.abs(s - S) <= EPSILON * Math.max(1, Math.abs(s), Math.abs(S)) && Math.abs(u - w) <= EPSILON * Math.max(1, Math.abs(u), Math.abs(w)) && Math.abs(l - k) <= EPSILON * Math.max(1, Math.abs(l), Math.abs(k)) && Math.abs(v - T) <= EPSILON * Math.max(1, Math.abs(v), Math.abs(T)) && Math.abs(c - A) <= EPSILON * Math.max(1, Math.abs(c), Math.abs(A)) && Math.abs(h - I) <= EPSILON * Math.max(1, Math.abs(h), Math.abs(I)) && Math.abs(d - $) <= EPSILON * Math.max(1, Math.abs(d), Math.abs($)) && Math.abs(_ - F) <= EPSILON * Math.max(1, Math.abs(_), Math.abs(F)) && Math.abs(g - P) <= EPSILON * Math.max(1, Math.abs(g), Math.abs(P)) && Math.abs(y - L) <= EPSILON * Math.max(1, Math.abs(y), Math.abs(L))
    }
    function create() {
        var e = new ARRAY_TYPE(3);
        if (ARRAY_TYPE != Float32Array) {
            e[0] = 0;
            e[1] = 0;
            e[2] = 0
        }
        return e
    }
    function copy(e, r) {
        e[0] = r[0];
        e[1] = r[1];
        e[2] = r[2];
        return e
    }
    function set(e, r, t, i) {
        e[0] = r;
        e[1] = t;
        e[2] = i;
        return e
    }
    function subtract(e, r, t) {
        e[0] = r[0] - t[0];
        e[1] = r[1] - t[1];
        e[2] = r[2] - t[2];
        return e
    }
    var forEach$1 = function() {
        var s = create();
        return function(e, r, t, i, a, n) {
            var o, f;
            if (!r) {
                r = 3
            }
            if (!t) {
                t = 0
            }
            if (i) {
                f = Math.min(i * r + t, e.length)
            } else {
                f = e.length
            }
            for (o = t; o < f; o += r) {
                s[0] = e[o];
                s[1] = e[o + 1];
                s[2] = e[o + 2];
                a(s, s, n);
                e[o] = s[0];
                e[o + 1] = s[1];
                e[o + 2] = s[2]
            }
            return e
        }
    }();
    var isSupportFloat64Array = typeof Float64Array !== "undefined";
    var isSupportFloat32Array$1 = typeof Float32Array !== "undefined";
    if (isSupportFloat64Array) {
        setMatrixArrayType(Float64Array)
    } else if (isSupportFloat32Array$1) {
        setMatrixArrayType(Float32Array)
    } else {
        window["Float32Array"] = undefined;
        setMatrixArrayType(Array)
    }
    var DomUtil = AMap["DomUtil"];
    var CustomRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.type = "customLayer";
            e.OF = create();
            e.VM = create();
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            if (!i.alwaysRender && this.OF && this.EI && this.BF && this.NF && (equals(this.NF, t.viewState.mvpMatrix) || t.map.TL())) {
                var a = Math.pow(2, t.viewState.zoom - this.EI);
                subtract(this.VM, t.viewState.centerCoord, this.OF);
                set(this.VM, -(this.VM[0] / this.BF) * a, this.VM[1] / this.BF * a, 0);
                if (a !== 1) {
                    i.kv.style[DomUtil.kL] = "scale3d(" + a + "," + a + ",1)"
                }
                i.kv.style.top = this.VM[1] + "px";
                i.kv.style.left = this.VM[0] + "px"
            } else {
                var n = i.layer;
                copy(this.OF, t.viewState.centerCoord);
                this.EI = t.viewState.zoom;
                this.NF = t.viewState.mvpMatrix;
                this.BF = t.viewState.resolution;
                if (n["render"]) {
                    var o = n.getLayerConfig().zooms;
                    if (t.viewState.zoom >= o[0] && t.viewState.zoom <= o[1]) {
                        n.canvas.style.visibility = "visible";
                        n["render"]()
                    } else {
                        if (n && n.canvas && n.canvas) {
                            n.canvas.style.visibility = "hidden"
                        }
                    }
                }
                i.kv.style[DomUtil.kL] = "";
                i.kv.style.top = "";
                i.kv.style.left = "";
                i.kv.style.height = "";
                i.kv.style.visibility = ""
            }
        }
        ;
        return e
    }(LayerRender);
    var GLCustomRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.kC = true;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            if (!e) {
                return
            }
            var a = e.context;
            var n = a.gl;
            var o = i.layer;
            if (this.kC) {
                this.kC = false;
                o.init(n)
            }
            if (o.render) {
                var f = o.getLayerConfig().zooms;
                if (t.viewState.zoom >= f[0] && t.viewState.zoom <= f[1]) {
                    o.render(n, t, t.viewState, a);
                    a.setDirty()
                }
            }
            return
        }
        ;
        return e
    }(LayerRender);
    var ImageRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            var a = e.context;
            var n = e.Gn();
            var o = i.layer.getLayerConfig().zooms;
            if (t.viewState.zoom < o[0] || t.viewState.zoom > o[1]) {
                return
            }
            var f = t.viewState;
            var s = t.map;
            var u = 1;
            if (f.viewMode === "3D") {
                u = s.getView().EF()
            }
            var l = s.JF(t.viewState.optimalZoom);
            var v = l || [1, 1, 1, 0];
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            n.Ce({
                u_skyColor: v,
                u_skyHeight: u,
                u_viewHeight: f.size[1],
                u_opacity: i.opacity,
                u_texture: r.texture,
                u_mvpMatrix: f.mvpMatrix,
                u_localDeltaCenter: r.Sa,
                u_flterFlag: false,
                u_colorscale: null
            }, {
                a_Position: {
                    type: "vec4",
                    buffer: r.zs,
                    Re: 0,
                    offset: 0
                }
            }, 6, undefined, undefined, i.depthTest, ColorMode.Si, i.rejectMapMask || !t.map.getMask() ? undefined : StencilMode.writeWithStencil);
            return
        }
        ;
        return e
    }(LayerRender);
    var NebulaTagType;
    (function(e) {
        e["LITE"] = "lite";
        e["LEFT"] = "left";
        e["ALL"] = "all";
        e["NONE"] = ""
    }
    )(NebulaTagType || (NebulaTagType = {}));
    var extend = function(e) {
        var r = [];
        for (var t = 1; t < arguments.length; t++) {
            r[t - 1] = arguments[t]
        }
        var i = Array.prototype.slice.call(arguments, 1);
        var a;
        var n;
        var o;
        for (a = 0,
        n = i.length; a < n; a = a + 1) {
            o = i[a] || {};
            for (var f in o) {
                if (o.hasOwnProperty(f)) {
                    var s = o[f];
                    if (typeof s !== "function" || !e.prototype) {
                        e[f] = s
                    } else {
                        e.prototype[f] = s
                    }
                }
            }
        }
        return e
    };
    function isBrowser() {
        return typeof window !== "undefined" && typeof document !== "undefined"
    }
    function isWasmSuppport() {
        try {
            if (typeof WebAssembly === "object" && typeof WebAssembly.instantiate === "function" && TextDecoder && TextEncoder) {
                var e = new WebAssembly.Module(new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0]));
                if (e instanceof WebAssembly.Module) {
                    return new WebAssembly.Instance(e)instanceof WebAssembly.Instance
                }
            }
        } catch (e) {}
        return false
    }
    var testWepP = function(e) {
        var r = new Image;
        r.src = "data:image/webp;base64,UklGRi4AAABXRUJQVlA4TCEAAAAvAUAAEB8wAiMw" + "AgSSNtse/cXjxyCCmrYNWPwmHRH9jwMA";
        r.onload = r.onerror = function() {
            e(r.height === 2)
        }
    };
    function isWorkerEnv() {
        try {
            var e = !!document;
            return false
        } catch (e) {
            return true
        }
    }
    function getSupport(e) {
        var r = {
            runSupport: (new Date).getTime()
        };
        var O = false;
        var B = false;
        var D = false;
        var N = false;
        var U = false;
        var t = isWasmSuppport();
        var i = navigator.userAgent.toLowerCase();
        var a = function(e) {
            return i.indexOf(e) !== -1
        };
        var f = true;
        var n = a("macintosh");
        var o = a("ipad;") || a("ipad ")
          , H = a("ipod touch;")
          , s = a("iphone;") || a("iphone ")
          , u = s || o || H
          , l = (n || u) && a("safari") && a("version/")
          , z = a("macwechat")
          , G = a("windowswechat");
        var v = {
            YV: U,
            WV: n,
            Ue: O,
            g: N,
            $e: B,
            scale: 1,
            HV: D,
            DW: isWorkerEnv(),
            AJ: t,
            safari: l,
            AQ: z,
            gQ: G,
            amapRunTime: r
        };
        if (isBrowser()) {
            var c = window
              , h = document
              , d = h.documentElement
              , _ = /([a-z0-9]*\d+[a-z0-9]*)/
              , j = function(e) {
                if (!e) {
                    return null
                }
                e = e.toLowerCase();
                var r = null;
                var t = e.match(/angle \((.*)\)/);
                if (t) {
                    e = t[1];
                    e = e.replace(/\s*direct3d.*$/, "")
                }
                e = e.replace(/\s*\([^\)]*wddm[^\)]*\)/, "");
                if (e.indexOf("intel") >= 0) {
                    r = ["Intel"];
                    if (0 <= e.indexOf("mobile")) {
                        r.push("Mobile")
                    }
                    if (0 <= e.indexOf("gma") || 0 <= e.indexOf("graphics media accelerator")) {
                        r.push("GMA")
                    }
                    if (0 <= e.indexOf("haswell")) {
                        r.push("Haswell")
                    } else if (0 <= e.indexOf("ivy")) {
                        r.push("HD 4000")
                    } else if (0 <= e.indexOf("sandy")) {
                        r.push("HD 3000")
                    } else if (0 <= e.indexOf("ironlake")) {
                        r.push("HD")
                    } else {
                        if (0 <= e.indexOf("hd")) {
                            r.push("HD")
                        }
                        var i = e.match(_);
                        if (i) {
                            r.push(i[1].toUpperCase())
                        }
                    }
                    r = r.join(" ");
                    return r
                }
                if (e.indexOf("nvidia") >= 0 || e.indexOf("quadro") >= 0 || e.indexOf("geforce") >= 0 || e.indexOf("nvs") >= 0) {
                    r = ["nVidia"];
                    if (0 <= e.indexOf("geforce")) {
                        r.push("geForce")
                    }
                    if (0 <= e.indexOf("quadro")) {
                        r.push("Quadro")
                    }
                    if (0 <= e.indexOf("nvs")) {
                        r.push("NVS")
                    }
                    if (e.match(/\bion\b/)) {
                        r.push("ION")
                    }
                    if (e.match(/gtx\b/)) {
                        r.push("GTX")
                    } else if (e.match(/gts\b/)) {
                        r.push("GTS")
                    } else if (e.match(/gt\b/)) {
                        r.push("GT")
                    } else if (e.match(/gs\b/)) {
                        r.push("GS")
                    } else if (e.match(/ge\b/)) {
                        r.push("GE")
                    } else if (e.match(/fx\b/)) {
                        r.push("FX")
                    }
                    var i = e.match(_);
                    if (i) {
                        r.push(i[1].toUpperCase().replace("GS", ""))
                    }
                    if (0 <= e.indexOf("titan")) {
                        r.push("TITAN")
                    } else if (0 <= e.indexOf("ti")) {
                        r.push("Ti")
                    }
                    r = r.join(" ");
                    return r
                }
                if (e.indexOf("amd") >= 0 || e.indexOf("ati") >= 0 || e.indexOf("radeon") >= 0 || e.indexOf("firegl") >= 0 || e.indexOf("firepro") >= 0) {
                    r = ["AMD"];
                    if (0 <= e.indexOf("mobil")) {
                        r.push("Mobility")
                    }
                    var a = e.indexOf("radeon");
                    if (0 <= a) {
                        r.push("Radeon")
                    }
                    if (0 <= e.indexOf("firepro")) {
                        r.push("FirePro")
                    } else if (0 <= e.indexOf("firegl")) {
                        r.push("FireGL")
                    }
                    if (0 <= e.indexOf("hd")) {
                        r.push("HD")
                    }
                    if (a >= 0) {
                        e = e.substring(a)
                    }
                    var i = e.match(_);
                    if (i) {
                        r.push(i[1].toUpperCase().replace("HD", ""))
                    }
                    r = r.join(" ");
                    return r
                }
                return e.substring(0, 100)
            }
              , V = "google swiftshader;microsoft basic render driver;vmware svga 3d;Intel 965GM;Intel B43;Intel G41;Intel G45;Intel G965;Intel GMA 3600;Intel Mobile 4;Intel Mobile 45;Intel Mobile 965".split(";")
              , g = "ActiveXObject"in c
              , y = window["detectRetina"] == false ? false : "devicePixelRatio"in c && c["devicePixelRatio"] > 1 || g && "matchMedia"in c && c.matchMedia("(min-resolution:144dpi)") && c.matchMedia("(min-resolution:144dpi)").matches
              , m = a("windows nt")
              , W = i.search(/windows nt [1-5]\./) !== -1
              , q = i.search(/windows nt 5\.[12]/) !== -1
              , K = a("windows nt 10")
              , Y = a("windows phone")
              , Z = a("Mb2345Browser")
              , X = u && i.search(/ os [456]_/) !== -1
              , J = u && i.search(/ os [4-8]_/) !== -1
              , Q = u && i.search(/ os [78]_/) !== -1
              , ee = u && a("os 8_")
              , re = u && a("os 10_")
              , p = a("android");
            var b = 0;
            if (p) {
                b = parseInt(i.split("android")[1]) || 0
            }
            var te = p && b < 4
              , ie = p && b >= 5
              , ae = ie || i.search(/android 4.4/) !== -1
              , x = p ? "android" : u ? "ios" : m ? "windows" : n ? "mac" : "other"
              , ne = g && !c["XMLHttpRequest"]
              , oe = g && !h.querySelector
              , M = g && !h.addEventListener
              , C = g && a("msie 9")
              , fe = g && a("msie 10")
              , se = g && a("rv:11")
              , ue = M || C
              , S = a("edge")
              , le = a("qtweb")
              , w = a("ucbrowser")
              , ve = a("alipay") || p && w
              , ce = a("miuibrowser")
              , he = a("micromessenger")
              , de = a("dingtalk")
              , _e = a("mqqbrowser")
              , ge = a("baidubrowser")
              , ye = a("crios/")
              , k = a("chrome/")
              , me = (k || ye) && a("chromium")
              , pe = !me && (k && parseInt(i.split("chrome/")[1]) > 30 || ye && parseInt(i.split("crios/")[1]) > 30)
              , be = a("firefox")
              , xe = be && parseInt(i.split("firefox/")[1]) > 27
              , Me = l && parseInt(i.split("version/")[1]) > 7
              , Ce = u && a("aliapp")
              , T = p || u || Y || a("mobile")
              , Se = "ontouchstart"in h
              , we = c["navigator"] && c["navigator"]["msPointerEnabled"] && !!c["navigator"]["msMaxTouchPoints"]
              , ke = c["navigator"] && !!c["navigator"]["maxTouchPoints"]
              , Te = !Se && (ke || we)
              , Ae = Se || Te
              , Ie = function() {
                if (!T) {
                    return c.devicePixelRatio || 1
                }
                var e = document.getElementsByTagName("meta");
                if (window.parent && window.parent !== window) {
                    try {
                        if (window.parent.location.origin === window.location.origin) {
                            e = window.parent.document.getElementsByTagName("meta")
                        } else {
                            return 1
                        }
                    } catch (e) {
                        return 1
                    }
                }
                for (var r = e.length - 1; r >= 0; r -= 1) {
                    if (e[r].name === "viewport") {
                        var t = e[r].content;
                        var i = void 0
                          , a = void 0
                          , n = void 0;
                        if (t.indexOf("initial-scale") !== -1) {
                            i = parseFloat(t.split("initial-scale=")[1])
                        }
                        if (t.indexOf("minimum-scale") !== -1) {
                            a = parseFloat(t.split("minimum-scale=")[1])
                        } else {
                            a = 0
                        }
                        if (t.indexOf("maximum-scale") !== -1) {
                            n = parseFloat(t.split("maximum-scale=")[1])
                        } else {
                            n = Infinity
                        }
                        if (i) {
                            if (n >= a) {
                                if (i > n) {
                                    return n
                                } else if (i < a) {
                                    return a
                                } else {
                                    return i
                                }
                            } else {
                                console && console.log && console.log("viewport参数不合法");
                                return null
                            }
                        } else {
                            if (n >= a) {
                                if (a >= 1) {
                                    return 1
                                } else {
                                    return Math.min(n, 1)
                                }
                            } else {
                                console && console.log && console.log("viewport参数不合法");
                                return null
                            }
                        }
                    }
                }
                return undefined
            }
              , A = Ie()
              , $e = y && (!T || !!A && A >= 1)
              , Fe = g && "transition"in d.style
              , Pe = !!h.createElementNS && !!h.createElementNS("http://www.w3.org/2000/svg", "svg")["createSVGRect"]
              , I = h.createElement("canvas")
              , Le = !!(I && I.getContext)
              , Re = window["URL"] || window["webkitURL"]
              , Ee = false;
            var Oe = window["disableWorker"] !== true && !g && !(w && p && !pe) && window["Worker"] && Re && Re["createObjectURL"] && window["Blob"]
              , $ = ""
              , F = ""
              , P = 0
              , Be = window["higtQualityRender"] == false ? y ? false : true : true
              , De = window["movingDraw"]
              , L = window["forceWebGL"] ? {
                alpha: true,
                antialias: Be,
                depth: true,
                stencil: true
            } : {
                alpha: true,
                antialias: Be,
                depth: true,
                stencil: true
            }
              , Ne = function() {
                if (!Le || !Oe) {
                    return false
                }
                var e = ["webgl", "experimental-webgl", "moz-webgl"];
                var r = null;
                for (var t = 0; t < e.length; t += 1) {
                    try {
                        var i = L;
                        r = I.getContext(e[t], i)
                    } catch (e) {}
                    if (!r) {
                        continue
                    } else {
                        if (r.drawingBufferWidth !== I.width || r.drawingBufferHeight !== I.height) {
                            return false
                        }
                        if (!r.getShaderPrecisionFormat || !r.getParameter || !r.getExtension) {
                            return false
                        }
                        P = r.getParameter(r.MAX_RENDERBUFFER_SIZE);
                        var a = r.getParameter(r.MAX_VIEWPORT_DIMS);
                        if (!a) {
                            return false
                        }
                        P = Math.min(P, a[0], a[1]);
                        if (l && x === "mac") {
                            P = Math.min(P, 4096)
                        }
                        var n = Math.max(screen.width, screen.height);
                        if ($e) {
                            n *= Math.min(2, window.devicePixelRatio || 1)
                        }
                        if (n > P) {
                            return false
                        }
                        if (23 > r.getShaderPrecisionFormat(35632, 36338).precision || 23 > r.getShaderPrecisionFormat(35633, 36338).precision) {
                            return false
                        }
                        F = r.getExtension("WEBGL_debug_renderer_info") ? r.getParameter(37446) : null;
                        var o = j(F);
                        if (o) {
                            if (o.indexOf("google swiftshader") > -1) {
                                f = false;
                                return false
                            }
                            if (-1 !== V.indexOf(o)) {
                                return false
                            }
                        }
                        $ = e[t];
                        return true
                    }
                }
                return false
            }
              , Ue = Ne()
              , He = e[8] !== undefined ? e[8] : true
              , ze = window["Uint8Array"] && He && !window["forbidenWebGL"] && Ue && (window["forceWebGL"] || (pe || xe || Me || S || he || de) && x !== "other")
              , Ge = window["forceWebGLBaseRender"] ? "w" : ze ? "w" : "d"
              , je = a("webkit")
              , R = "WebKitCSSMatrix"in c && "m11"in new window["WebKitCSSMatrix"]
              , Ve = "MozPerspective"in d.style
              , We = "vV"in d.style
              , qe = Fe || R || Ve || We
              , Ke = false
              , Ye = e[12] !== undefined ? e[12] : null;
            var E = true;
            try {
                if (typeof c.localStorage === "undefined") {
                    E = false
                } else {
                    var Ze = (new Date).getTime() + "";
                    c.localStorage.setItem("_test", Ze);
                    if (c.localStorage.getItem("_test") !== Ze) {
                        E = false
                    }
                    c.localStorage.removeItem("_test")
                }
            } catch (e) {
                E = false
            }
            var Xe = function(e, r) {
                var t = {};
                extend(t, L);
                extend(t, r);
                return e.getContext($, t)
            };
            var Je = parseInt(i.split("chrome/")[1]);
            v = {
                qV: o,
                KV: s,
                size: s ? 100 : p ? 200 : 500,
                WV: n,
                ZV: m,
                JV: u,
                uT: re,
                HV: p,
                QV: te,
                cT: ve,
                eq: x,
                rq: ge,
                nq: _e,
                safari: l,
                iq: he,
                ie: g,
                aq: ne,
                oq: oe,
                sq: C,
                uq: fe,
                xv: M,
                fT: ue,
                cq: se,
                fq: S,
                hT: g && !se,
                lT: Z,
                RL: E,
                vT: Ye,
                geolocation: T || g && !M || S,
                SL: w,
                uc: w && !k,
                chrome: k,
                gT: true,
                hq: be,
                mee: f,
                $e: T,
                lq: T && je,
                dq: T && R,
                vq: T && c.opera,
                Ue: y,
                bT: A,
                Mr: $e,
                YV: Ae,
                gq: we,
                pq: ke,
                yT: Te,
                pT: k && Je >= 57,
                mT: !T && k && Je >= 64,
                g: je,
                bq: Fe,
                yq: R,
                mq: Ve,
                Tq: We,
                Mq: qe,
                Aq: Pe,
                xq: Le,
                Eb: Oe,
                TT: Ke,
                Tm: ze,
                AT: $,
                xT: L,
                ST: F,
                wT: P,
                MT: Ee,
                movingDraw: De,
                baseRender: He ? Ge : "d",
                scale: y ? 2 : 1,
                getContext: Xe,
                mH: false,
                AJ: t,
                amapRunTime: r,
                sY: Ne
            };
            testWepP(function(e) {
                v.mH = e
            })
        }
        return v
    }
    var Browser = getSupport(typeof config === "undefined" ? [] : config);
    if (typeof createImageBitmap !== "undefined" && typeof ImageBitmap !== "undefined") {
        Browser.imageBitmap = true
    }
    var canceledWorkerFetch = Boolean(Browser.DW ? !Browser.safari && !Browser.AQ && !Browser.gQ && self.fetch && self.Request && self.AbortController && self.Request.prototype.hasOwnProperty("signal") : !Browser.safari && !Browser.AQ && !Browser.gQ && window.fetch && window.Request && window.AbortController && window.Request.prototype.hasOwnProperty("signal"));
    Browser.LZ = canceledWorkerFetch;
    Browser["amapRunTime"] = {
        workerTime: {}
    };
    var projections = {};
    var Projection = function() {
        function e(e, r, t, i) {
            this.project = r;
            this.unproject = t;
            this.getResolution = i;
            projections[e] = this
        }
        e.prototype.Sq = function(e, r, t) {
            var i = this.getResolution(t);
            var a = r[0] * i;
            var n = r[1] * i;
            var o = this.project(e[0], e[1]);
            return this.unproject(o[0] + a, o[1] + n)
        }
        ;
        e.prototype.wq = function(e, r) {
            var t = this.project(e[0], e[1]);
            var i = [t[0] + r[0], t[1] + r[1]];
            return this.unproject(i[0], i[1])
        }
        ;
        return e
    }();
    var ProjectionManager = {
        getProjection: function(e) {
            return projections[e]
        }
    };
    var LngLat = function() {
        var e = function(e, r) {
            return [e, r]
        };
        var r = function(e, r) {
            return [e, r]
        };
        var t = function(e) {
            return 180 / 256 / Math.pow(2, e)
        };
        return new Projection("EPSG:4326",e,r,t)
    }();
    var SphericalMercator = function() {
        var n = Math.PI / 180;
        var a = 180 / Math.PI;
        var o = 6378137;
        var f = 85.0511287798;
        var e = function(e, r) {
            var t = f;
            r = Math.max(Math.min(t, r), -t);
            var i = e * n;
            var a = r * n;
            a = Math.log(Math.tan(Math.PI / 4 + a / 2));
            return [i * o, a * o]
        };
        var r = function(e, r) {
            var t = e / o * a;
            var i = (2 * Math.atan(Math.exp(r / o)) - Math.PI / 2) * a;
            return [t, i]
        };
        var t = function(e) {
            return 20037508.342789244 * 2 / 256 / Math.pow(2, e)
        };
        return new Projection("EPSG:3857",e,r,t)
    }();
    var LCS = function() {
        function e() {
            this.hg = [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244];
            this.cg = 128;
            this.vg = 128;
            this.lg = (this.hg[2] - this.hg[0]) / this.cg;
            this.dg = (this.hg[3] - this.hg[1]) / this.vg
        }
        e.prototype.setMap = function(e) {
            this.map = e
        }
        ;
        e.prototype.getSize = function() {
            return [this.lg, this.dg]
        }
        ;
        e.prototype.getNum = function() {
            return [this.cg, this.vg]
        }
        ;
        e.prototype.getLocalByLnglat = function(e) {
            var r;
            var t = (r = ProjectionManager.getProjection("EPSG:3857")).project.apply(r, e);
            return this.getLocalByCoord(t)
        }
        ;
        e.prototype.getLocalByCoord = function(e) {
            var r = Math.floor(e[0] / this.lg);
            var t = Math.floor(e[1] / this.dg);
            var i = this.getLocalCenterByXY(r, t);
            var a = [e[0] - i.center[0], e[1] - i.center[1]];
            return {
                Sa: a,
                x: r,
                y: t,
                center: i.center,
                Ro: i.Ro
            }
        }
        ;
        e.prototype.getLocalCenterByXY = function(e, r) {
            var t = {
                x: e,
                y: r,
                center: [0, 0],
                Ro: [0, 0, 0, 0]
            };
            t.center = [(e + .5) * this.lg, (r + .5) * this.dg];
            t.Ro = [e * this.lg, r * this.dg, (e + 1) * this.lg, (r + 1) * this.dg];
            return t
        }
        ;
        e.prototype.getDeltaCoord = function(e) {
            var r = this.getLocalByCoord(e);
            return [e[0] - r.center[0], e[1] - r.center[1]]
        }
        ;
        return e
    }();
    var lcs$2 = new LCS;
    var E4326 = ProjectionManager.getProjection("EPSG:4326");
    var E3857 = ProjectionManager.getProjection("EPSG:3857");
    var LngLat$1 = {
        Ro: [-180, -90, 180, 90],
        project: function(e, r, t) {
            var i = E4326.project(e, r);
            return this.transform(i[0], i[1], t)
        },
        unproject: function(e, r, t) {
            var i = this.Xs(e, r, t);
            return E4326.unproject(i[0], i[1])
        },
        transform: function(e, r, t) {
            var i;
            if (t === undefined) {
                i = 180
            } else {
                i = 256 << t
            }
            var a = i;
            var n = i * 2;
            var o = this.Ro[2] - this.Ro[0];
            var f = this.Ro[3] - this.Ro[1];
            e = (e - this.Ro[0]) / o * n;
            r = a - (r - this.Ro[1]) / f * a;
            return [e, r]
        },
        Xs: function(e, r, t) {
            var i;
            if (t === undefined) {
                i = 180
            } else {
                i = 256 << t
            }
            var a = i;
            var n = i * 2;
            e = (e / n - .5) * 360;
            r = -(r / a - .5) * 180;
            return [e, r]
        },
        nt: function(e, r, t) {
            return this.Xs(e, r, t)
        },
        wu: function(e, r, t) {
            return this.Xs(e, r, t)
        },
        yo: function(e, r, t) {
            var i = 0;
            if (e / this.Ro[3] > 0) {
                i = Math.abs(Math.floor(e / this.Ro[3]))
            } else {
                i = Math.abs(Math.ceil(e / this.Ro[3]))
            }
            var a = 0;
            if (r / this.Ro[3] > 0) {
                a = Math.abs(Math.floor(r / this.Ro[3]))
            } else {
                a = Math.abs(Math.ceil(r / this.Ro[3]))
            }
            var n = e % this.Ro[3];
            var o = r % this.Ro[3];
            var f = this.transform(n, o, t);
            e = this.wu(e, e, t)[0] * i + f[0];
            r = this.wu(r, r, t)[1] * a + f[1];
            var s = this.transform(0, 0, t)
              , u = s[0]
              , l = s[1];
            return [e - u, r - l]
        },
        Bb: function(e, r, t) {
            return t.X(e, r)
        },
        qb: function(e, r, t) {
            return t.H(e, r)
        },
        Gd: function(e, r, t, i) {
            var a = i.X(e[0], e[1]);
            var n = i.X(e[0] + r, e[1] + t);
            return [n[0] - a[0], n[1] - a[1]]
        }
    };
    var SphericalMercator$1 = {
        Ro: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244],
        project: function(e, r, t) {
            var i = E3857.project(e, r);
            return this.transform(i[0], i[1], t)
        },
        unproject: function(e, r, t) {
            var i = this.Xs(e, r, t);
            return E3857.unproject(i[0], i[1])
        },
        transform: function(e, r, t) {
            e = Math.min(20037508.342789244, Math.max(e, -20037508.342789244));
            r = Math.min(20037508.342789244, Math.max(r, -20037508.342789244));
            var i;
            if (t === undefined) {
                i = 20037508.342789244 * 2
            } else {
                i = 256 * Math.pow(2, t)
            }
            var a = this.Ro[2] - this.Ro[0];
            var n = this.Ro[3] - this.Ro[1];
            e = (e - this.Ro[0]) / a * i;
            r = i - (r - this.Ro[1]) / n * i;
            return [e, r]
        },
        Xs: function(e, r, t) {
            var i;
            if (t === undefined) {
                i = 20037508.342789244 * 2
            } else {
                i = 256 * Math.pow(2, t)
            }
            e = (e / i - .5) * this.Ro[2] * 2;
            r = -(r / i - .5) * this.Ro[2] * 2;
            return [e, r]
        },
        nt: function(e, r, t, i) {
            e = this.Xs(e, r, t)[0] - this.Ro[0];
            r = this.Ro[2] - this.Xs(e, r, t)[1];
            return [e, r]
        },
        wu: function(e, r, t, i) {
            var a = i.center
              , n = i.size;
            var o = (i.rotation || 0) / 180 * Math.PI;
            var f = this.transform(e, r, t)
              , s = f[0]
              , u = f[1];
            var l = this.transform(a[0], a[1], t)
              , v = l[0]
              , c = l[1];
            var h = s - v;
            var d = u - c;
            var _ = Math.sqrt(Math.pow(h, 2) + Math.pow(d, 2));
            var g = d === 0 ? Math.PI / 2 : Math.atan(Math.abs(h / d));
            var y;
            if (h * d <= 0) {
                y = g + o
            } else {
                y = g - o
            }
            var m = h > 0 ? y : -y;
            var p = d > 0 ? 1 : -1;
            var b = n[0] / 2 + _ * Math.sin(m);
            var x = n[1] / 2 + _ * Math.cos(y) * p;
            return [b, x]
        },
        yo: function(e, r, t, i) {
            var a = 0;
            if (e / this.Ro[3] > 0) {
                a = Math.abs(Math.floor(e / this.Ro[3]))
            } else {
                a = Math.abs(Math.ceil(e / this.Ro[3]))
            }
            var n = 0;
            if (r / this.Ro[3] > 0) {
                n = Math.abs(Math.floor(r / this.Ro[3]))
            } else {
                n = Math.abs(Math.ceil(r / this.Ro[3]))
            }
            var o = e % this.Ro[3];
            var f = r % this.Ro[3];
            var s = this.transform(o, f, t);
            var u = this.transform(0, 0, t)
              , l = u[0]
              , v = u[1];
            e = l * a + s[0];
            r = v * n + s[1];
            return [e - l, r - v]
        },
        Bb: function(e, r, t) {
            return t.X(e, r)
        },
        qb: function(e, r, t) {
            return t.H(e, r)
        },
        Gd: function(e, r, t, i) {
            var a = i.X(e[0], e[1]);
            var n = i.X(e[0] + r, e[1] + t);
            return [n[0] - a[0], n[1] - a[1]]
        }
    };
    var transform = {
        "EPSG:4326": LngLat$1,
        "EPSG:3857": SphericalMercator$1
    };
    var labelsUtil$2 = {
        Bj: function(e) {
            if (e === void 0) {
                e = {}
            }
            var r = {};
            for (var t in e) {
                if (e.hasOwnProperty(t)) {
                    r[t] = 1
                }
            }
            return r
        },
        zj: function(e, r) {
            e.gs = r;
            e.qj = r.options || {};
            e.Wj = r.Wj || {};
            e.Fs = r.Fs || {};
            e.Gj = r.Gj || {};
            e.Hj = r.Hj || {};
            e.Vj = r.Vj || {};
            e.$j = r.$j || 0
        },
        Yj: function(e, r) {
            if (e === void 0) {
                e = [NaN, NaN, NaN, NaN]
            }
            if (r === void 0) {
                r = [0, 0, 0, 1]
            }
            for (var t = 0; t < e.length; t++) {
                var i = e[t];
                if (isNaN(i)) {
                    e[t] = r[t]
                }
            }
            return e
        },
        Kj: function(e) {
            var r = 255;
            var t = 65280;
            var i = 16711680;
            var a = e & r;
            var n = (e & t) >> 8;
            var o = (e & i) >> 16;
            var f = 255;
            return [o / f, n / f, a / f, 1]
        },
        Xj: function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = [];
            var i = r.qw
              , a = r.Aw
              , n = r.complete;
            for (var o = 0, f = e; o < f.length; o++) {
                var s = f[o];
                var u = new Promise(s);
                t.push(u)
            }
            var l = 0;
            Promise.all(t).then(function() {
                if (i) {
                    i.apply(void 0, arguments)
                }
                if (n && !l) {
                    l = 1;
                    n.apply(void 0, arguments)
                }
            }).catch(function() {
                if (a) {
                    a.apply(void 0, arguments)
                }
                if (n && !l) {
                    l = 1;
                    n.apply(void 0, arguments)
                }
            })
        },
        Jj: function(e, r) {
            var t = e[0]
              , i = t === void 0 ? 0 : t
              , a = e[1]
              , n = a === void 0 ? 0 : a;
            var o = r[0]
              , f = o === void 0 ? 0 : o
              , s = r[1]
              , u = s === void 0 ? 0 : s;
            var l;
            if (i <= f) {
                if (n >= f) {
                    if (n > u) {
                        l = [f, u]
                    } else {
                        l = [f, n]
                    }
                } else {
                    l = null
                }
            } else {
                l = labelsUtil$2.Jj(r, e)
            }
            return l
        },
        measureText: function(e, r, t, i) {
            if (r === void 0) {
                r = ""
            }
            if (i === void 0) {
                i = "3D"
            }
            if (i === "3D") {
                return e.measureText(r, t)
            }
        },
        lnglatToContainer: function(e, r) {
            var t = r.zoom
              , i = t === void 0 ? 20 : t;
            if (i < LocalZoom) {
                return e
            }
            var a = lcs$2.getLocalByCoord(e);
            var n = vector$3.Ip(e, a.center);
            return n
        },
        Sd: function(e, r) {
            var t = e[0]
              , i = t === void 0 ? 0 : t
              , a = e[1]
              , n = a === void 0 ? 0 : a;
            var o = r[0]
              , f = o === void 0 ? 0 : o
              , s = r[1]
              , u = s === void 0 ? 0 : s;
            var l = Math.abs(f - i);
            var v = Math.abs(u - n);
            var c = Math.sqrt(Math.pow(l, 2) + Math.pow(v, 2));
            return c
        },
        getAngle: function(e, r) {
            var t = e[0]
              , i = t === void 0 ? 0 : t
              , a = e[1]
              , n = a === void 0 ? 0 : a;
            var o = r[0]
              , f = o === void 0 ? 0 : o
              , s = r[1]
              , u = s === void 0 ? 0 : s;
            var l = f - i;
            var v = u - n;
            var c;
            if (l !== 0) {
                c = Math.atan(v / l)
            } else if (v < 0) {
                return Math.PI * 3 / 2
            } else {
                return Math.PI / 2
            }
            var h;
            if (c === 0) {
                if (l >= 0) {
                    h = c
                } else {
                    h = c + Math.PI
                }
            } else if (c > 0) {
                if (l >= 0) {
                    h = c
                } else {
                    h = c + Math.PI
                }
            } else {
                if (l > 0) {
                    h = 2 * Math.PI + c
                } else {
                    h = Math.PI + c
                }
            }
            return h
        },
        Fd: function(e, r, t) {
            var i = e[0]
              , a = i === void 0 ? 0 : i
              , n = e[1]
              , o = n === void 0 ? 0 : n;
            var f = r[0]
              , s = f === void 0 ? 0 : f
              , u = r[1]
              , l = u === void 0 ? 0 : u;
            var v = s - a;
            var c = l - o;
            var h = [];
            var d = 0;
            while (d < t) {
                var _ = a + v / (t + 1) * (d + 1);
                var g = o + c / (t + 1) * (d + 1);
                var y = [_, g];
                h.push(y);
                d++
            }
            return h
        },
        OG: function(e, r, t) {
            if (t !== 0) {
                var i = e[0] + (r[0] - e[0]) * t;
                var a = e[1] + (r[1] - e[1]) * t;
                return [i, a]
            }
            return e
        },
        Ed: function(e, r) {
            if (e === void 0) {
                e = []
            }
            if (r === void 0) {
                r = "ch"
            }
            var t = e.length;
            if (!t) {
                return false
            }
            var i = e[0];
            var a = t >= 4 ? Math.floor(t / 4 * 3) : t - 1;
            var n = e[a];
            var o = i[0]
              , f = i[1];
            var s = n[0]
              , u = n[1];
            var l = u - f;
            var v = s - o;
            var c = l / v;
            var h = [s - o, u - f];
            var d;
            if (r === "ch") {
                if (Math.abs(c) > 1) {
                    d = vector$3.Zj(h, [0, 1])
                } else {
                    d = vector$3.Zj(h, [1, 0])
                }
            } else {
                d = vector$3.Zj(h, [1, 0])
            }
            return d < 0
        },
        Nd: function(e, r, t) {
            var i = e[0]
              , a = e[1];
            var n = r[0]
              , o = r[1];
            var f = o - a;
            var s = n - i;
            var u = f / s;
            var l = [n - i, o - a];
            var v;
            var c;
            if (Math.abs(u) > 1 && t !== "x") {
                v = vector$3.Zj(l, [0, -1]);
                if (v > 0) {
                    c = vector$3.Qj(l, [0, -1])
                } else {
                    c = vector$3.Qj(l, [0, 1])
                }
                if (u > 0) {
                    c = -c
                }
            } else {
                v = vector$3.Zj(l, [1, 0]);
                if (v > 0) {
                    c = vector$3.Qj(l, [1, 0])
                } else {
                    c = vector$3.Qj(l, [-1, 0])
                }
                if (u < 0) {
                    c = -c
                }
            }
            return c
        },
        Od: function(e, r, t) {
            var i;
            var a;
            i = t * Math.sin(r);
            a = t * Math.cos(r);
            var n = e[0]
              , o = e[1];
            var f = n + a;
            var s = o + i;
            return [f, s]
        },
        jd: function(e, r, t, i) {
            var a = vector$3.Ip(e, r);
            var n = vector$3.length(a);
            var o = vector$3.Ip(t, r);
            var f = vector$3.length(o);
            var s = vector$3.Zj(a, o);
            var u = Math.acos(s / (n * f));
            var l = Math.PI - u;
            var v = n * Math.sin(l);
            var c = n * Math.cos(l);
            var h = Math.sqrt(Math.pow(i, 2) - Math.pow(v, 2));
            var d = h - c;
            var _ = o[0] === 0 ? o[1] > 0 ? Math.PI / 2 : -Math.PI / 2 : Math.atan(o[1] / o[0]);
            var g = Math.abs(d * Math.sin(_));
            var y = d * Math.cos(_);
            var m = vector$3.Zj(o, [1, 0]);
            var p = vector$3.Zj(o, [0, 1]);
            var b = [m >= 0 ? y : -y, p >= 0 ? g : -g];
            var x = vector$3.add(r, b);
            return x
        },
        rank: function(e) {
            var r = [];
            for (var t = 0; t < e.length; t++) {
                var i = parseFloat(e[t]);
                if (i < 0) {
                    e.splice(t--, 1);
                    r.push(i)
                } else {
                    e[t] = i
                }
            }
            e.sort(function(e, r) {
                return e - r
            });
            e = r.concat(e);
            return e
        },
        $p: function(e) {
            var r = CONSTS.Mp
              , t = CONSTS.Rp;
            if (e[1] >= zoomRange[1]) {
                e[1] = t
            }
            if (e[0] <= zoomRange[0]) {
                e[0] = r
            }
            return e
        },
        toString: function(e, r) {
            if (r === void 0) {
                r = 4
            }
            var t = "";
            if (!e) {
                return t
            }
            for (var i = 0, a = e; i < a.length; i++) {
                var n = a[i];
                if ((n + "").length < r) {
                    t += n
                } else {
                    t += n.toFixed(r)
                }
            }
            return t
        },
        xf: function(e, r) {
            var t = [];
            var i = r.ja
              , a = r.Fa
              , n = r.Oa
              , o = r.Ea;
            for (var f in e) {
                if (e.hasOwnProperty(f)) {
                    if (f === CONSTS.gH || f === CONSTS.FN || f === CONSTS.RB) {
                        continue
                    }
                    if (f === "shields") {
                        t.push(e[f]);
                        continue
                    }
                    if (i) {
                        var s = f.split("-");
                        var u = parseInt(s[1], 10);
                        var l = parseInt(s[2], 10);
                        if (u >= i && u <= n && l >= a && l <= o) {
                            t.push(e[f])
                        }
                    } else {
                        t.push(e[f])
                    }
                }
            }
            if (!t.length) {
                t = e
            }
            return t
        },
        EQ: function(e, r) {
            var t = [];
            var i = r.length;
            for (var a = 0; a < i - 1; a++) {
                var n = e.substring(r[a], r[a + 1]);
                t.push(n)
            }
            if (!i) {
                t = e.split("")
            }
            return t
        },
        aQ: function(e) {
            var r = [];
            for (var t = 0; t < e.length; t++) {
                var i = e[t];
                var a = [];
                for (var n = 0; n < i.length; n++) {
                    var o = i.charCodeAt(n);
                    a.push(o)
                }
                var f = a.join("|");
                r.push(f)
            }
            return r
        },
        tQ: function(e, r, t) {
            var i = "";
            var a = [];
            var n = "";
            if (e.name) {
                if (e.JQ && e.JQ.length > 0) {
                    i = getSpiltLineWithSpiltIndex(e.name, e.JQ);
                    n = "ch"
                } else if (t) {
                    var o = t.lineBreak(e.name, {
                        from: "labelsLayer",
                        CO: []
                    });
                    var f = 0;
                    var s = "";
                    if (o.oQ.length >= 2) {
                        for (var u = 1, l = o.oQ.length - 1; u < l; u++) {
                            s += e.name.slice(f, o.oQ[u]) + " ";
                            f = o.oQ[u]
                        }
                        s += e.name.slice(f)
                    } else {
                        s = e.name
                    }
                    i = s;
                    n = "ch"
                } else {
                    i = e.name;
                    n = "ch"
                }
            } else {
                switch (r) {
                case "en":
                    {
                        i = e.name_en;
                        a = e.iQ;
                        n = "en";
                        break
                    }
                case "local":
                    {
                        i = e.QQ;
                        a = e.$G;
                        n = e.CQ;
                        break
                    }
                case "zh_cn":
                    {
                        i = e.BQ;
                        n = "ch";
                        break
                    }
                default:
                    {
                        i = e.BQ;
                        n = "ch"
                    }
                }
                if (!i) {
                    if (e.name_en) {
                        i = e.name_en;
                        a = e.iQ;
                        n = "en"
                    } else if (e.QQ) {
                        i = e.QQ;
                        a = e.$G;
                        n = e.QQ
                    }
                }
            }
            return {
                name: i,
                $G: a,
                type: n
            }
        }
    };
    var vector$3 = {
        add: function(e, r) {
            var t = [];
            for (var i = 0; i < e.length; i++) {
                t[i] = e[i] + r[i]
            }
            return t
        },
        Ip: function(e, r) {
            var t = [];
            for (var i = 0; i < e.length; i++) {
                t.push(e[i] - r[i])
            }
            return t
        },
        multiply: function(e, r) {
            var t = [];
            for (var i = 0, a = e; i < a.length; i++) {
                var n = a[i];
                t.push(n * r)
            }
            return t
        },
        Up: function(e, r) {
            var t = [];
            for (var i = 0; i < e.length; i++) {
                t[i] = e[i] / r
            }
            return t
        },
        tA: function(e) {
            return this.multiply(e, -1)
        },
        Zj: function(e, r) {
            var t = e[0]
              , i = t === void 0 ? 0 : t
              , a = e[1]
              , n = a === void 0 ? 0 : a;
            var o = r[0]
              , f = o === void 0 ? 0 : o
              , s = r[1]
              , u = s === void 0 ? 0 : s;
            return i * f + n * u
        },
        Qj: function(e, r) {
            var t = vector$3.Zj(e, r);
            var i = vector$3.length(e);
            var a = vector$3.length(r);
            var n = Math.acos(t / (i * a));
            return n
        },
        length: function(e) {
            var r = e[0]
              , t = e[1];
            return Math.sqrt(Math.pow(r, 2) + Math.pow(t, 2))
        }
    };
    function getSpiltLineWithSpiltIndex(e, r) {
        r.shift();
        if (r.length === 0) {
            return e
        }
        var t = "";
        var i = 0;
        for (var a = 0, n = r.length; a < n; a++) {
            t += e.substr(i, r[a] - i) + " ";
            i = r[a]
        }
        t += e.substr(r[r.length - 1]);
        return t
    }
    var scale$1 = Browser.scale;
    var LabelsRenderUtil$3 = {
        getBounds: function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = r.zoom
              , i = r.centerCoord
              , a = r.view;
            var n = 15;
            var o = [10 * n * scale$1, 10 * n * scale$1, 10 * n * scale$1, 10 * n * scale$1];
            var f = e.boundsCoord;
            var s = f[0]
              , u = f[1]
              , l = f[2]
              , v = f[3];
            var c = this.nt(0, 0, t, r);
            var h = vector$3.Ip(this.nt(o[0], o[1], t, r), c);
            var d = vector$3.Ip(this.nt(o[2], o[3], t, r), c);
            var _ = t >= LocalZoom ? i : [0, 0]
              , g = _[0]
              , y = _[1];
            var m = [s - g - Math.abs(d[1]), u - y - Math.abs(d[0])];
            var p = [l - g + Math.abs(h[1]), v - y + Math.abs(h[0])];
            return [m, p]
        },
        gz: function(e, r) {
            var t = r[0]
              , i = r[1];
            var a = e[0]
              , n = e[1];
            var o = a - t[0];
            var f = n - t[1];
            var s = i[0] - a;
            var u = i[1] - n;
            return o >= 0 && f >= 0 && s >= 0 && u >= 0
        },
        wz: function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = r.forceShow
              , i = t === void 0 ? 0 : t
              , a = r._z
              , n = a === void 0 ? 0 : a;
            var o = e["data"] || {};
            var f = o["rank"] || 1;
            var s = n / 100;
            var u = 2e5;
            f = i ? f - 1e5 : s * u + f;
            return f
        },
        wu: function(e, r, t, i) {
            if (e === void 0) {
                e = 0
            }
            if (r === void 0) {
                r = 0
            }
            if (i) {
                var a = i.viewState
                  , n = i.size
                  , o = i.transform
                  , f = i.view
                  , s = i.viewMode
                  , u = i.centerCoord
                  , l = i.positionType
                  , v = l === void 0 ? "relative" : l;
                if (s === "2D") {
                    var c = {
                        center: a.centerCoord,
                        rotation: a.rotation,
                        size: n
                    };
                    var h = a.optimalZoom >= LocalZoom ? u : [0, 0]
                      , d = h[0]
                      , _ = h[1];
                    return o.wu(e + d, r + _, t, c)
                } else {
                    return v === "relative" ? f.H(e, r) : f._u(e, r)
                }
            }
        },
        r0: function(e, r, t, i, a) {
            if (e === void 0) {
                e = 0
            }
            if (r === void 0) {
                r = 0
            }
            if (t === void 0) {
                t = 0
            }
            if (a) {
                var n = a.viewState
                  , o = a.size
                  , f = a.transform
                  , s = a.view
                  , u = a.viewMode
                  , l = a.centerCoord
                  , v = a.positionType
                  , c = v === void 0 ? "relative" : v;
                if (u === "2D") {
                    var h = {
                        center: n.centerCoord,
                        rotation: n.rotation,
                        size: o
                    };
                    var d = n.optimalZoom >= LocalZoom ? l : [0, 0]
                      , _ = d[0]
                      , g = d[1];
                    return f.wu(e + _, r + g, i, h)
                } else {
                    return c === "relative" ? s.H(e, r, t) : s._u(e, r, t)
                }
            }
        },
        kz: function(e, r, t, i) {
            if (e === void 0) {
                e = []
            }
            if (i === void 0) {
                i = {}
            }
            var a = i.centerCoord;
            if (r < LocalZoom && t >= LocalZoom) {
                var n = a[0]
                  , o = a[1];
                var f = [];
                for (var s = 0, u = e; s < u.length; s++) {
                    var l = u[s];
                    f.push([l[0] - n, l[1] - o])
                }
                return f
            }
            return e
        },
        nt: function(e, r, t, i) {
            if (e === void 0) {
                e = 0
            }
            if (r === void 0) {
                r = 0
            }
            var a = i.view
              , n = i.transform
              , o = i.viewMode;
            if (o === "2D") {
                return n.nt(e, r, t)
            } else {
                return a.X(e, r)
            }
        },
        MG: function(e, r) {
            if (e < LocalZoom - 1) {
                r = [0, 0]
            }
            return r
        }
    };
    function isLngLat(e) {
        return e && e.className === "AMap.LngLat"
    }
    function isNumber(e) {
        var r = +e;
        return typeof +r === "number" && !isNaN(r)
    }
    function isNumberArray(e) {
        return Array.isArray(e) && isNumber(e[0])
    }
    function parseLngLatData(e) {
        if (Array.isArray(e)) {
            if (Array.isArray(e[0])) {
                for (var r = 0; r < e.length; r += 1) {
                    e[r] = parseLngLatData(e[r])
                }
            } else {
                var t = typeof e[0];
                if (t === "string" || t === "number") {
                    return new LngLat$2(e[0],e[1])
                } else {
                    return e
                }
            }
        }
        return e
    }
    var Coordinate = {
        lr: function(e, r) {
            var t = Infinity;
            for (var i = 0, a = 1, n = r.length; a < n; i = a,
            a += 1) {
                t = Math.min(t, this.ur(e, [r[i], r[a]]))
            }
            return Math.sqrt(t)
        },
        ur: function(e, r) {
            return this.cr(e, this.closestOnSegment(e, r))
        },
        cr: function(e, r) {
            var t = e[0] - r[0];
            var i = e[1] - r[1];
            return t * t + i * i
        },
        pr: function(e, r, t, i) {
            i = i || 1e-6;
            if (t[0] === r[0]) {
                var a = Math.min(r[1], t[1]);
                var n = Math.max(r[1], t[1]);
                return Math.abs(e[0] - t[0]) < i && e[1] >= a && e[1] <= n
            }
            var o = Math.min(r[0], t[0]);
            var f = Math.max(r[0], t[0]);
            var s = (t[1] - r[1]) / (t[0] - r[0]) * (e[0] - r[0]) + r[1];
            return Math.abs(s - e[1]) < i && e[0] >= o && e[0] <= f
        },
        closestOnSegment: function(e, r) {
            var t = e[0];
            var i = e[1];
            var a = r[0];
            var n = r[1];
            var o = a[0];
            var f = a[1];
            var s = n[0];
            var u = n[1];
            var l = s - o;
            var v = u - f;
            var c = l === 0 && v === 0 ? 0 : (l * (t - o) + v * (i - f)) / (l * l + v * v || 0);
            var h;
            var d;
            if (c <= 0) {
                h = o;
                d = f
            } else if (c >= 1) {
                h = s;
                d = u
            } else {
                h = o + c * l;
                d = f + c * v
            }
            return [h, d]
        },
        isClockwise: function(e) {
            var r = e.length;
            var t = 0;
            var i = e[r - 1];
            var a = i[0];
            var n = i[1];
            var o;
            var f;
            var s;
            for (var u = 0; u < r; u += 1) {
                s = e[u];
                o = s[0];
                f = s[1];
                t += (o - a) * (f + n);
                a = o;
                n = f
            }
            return t > 0
        },
        ss: function(e, r, t) {
            var i = e[0];
            var a = e[1];
            var n = false;
            var o;
            var f;
            var s;
            var u;
            var l = r.length;
            for (var v = 0, c = l - 1; v < l; c = v,
            v += 1) {
                var h = false;
                o = r[v][0];
                f = r[v][1];
                s = r[c][0];
                u = r[c][1];
                if (o === i && f === a || s === i && u === a) {
                    return t ? true : false
                }
                if (f < a === u >= a) {
                    var d = (s - o) * (a - f) / (u - f) + o;
                    if (i === d) {
                        return t ? true : false
                    } else {
                        h = i < d
                    }
                }
                if (h) {
                    n = !n
                }
            }
            return n
        },
        dr: function(e, r) {
            var t = function(e, r, t) {
                return (t[0] - r[0]) * (e[1] - r[1]) > (t[1] - r[1]) * (e[0] - r[0])
            };
            var i = function(e, r, t, i) {
                var a = [e[0] - r[0], e[1] - r[1]]
                  , n = [t[0] - i[0], t[1] - i[1]]
                  , o = e[0] * r[1] - e[1] * r[0]
                  , f = t[0] * i[1] - t[1] * i[0]
                  , s = 1 / (a[0] * n[1] - a[1] * n[0]);
                return [(o * n[0] - f * a[0]) * s, (o * n[1] - f * a[1]) * s]
            };
            var a, n, o, f;
            var s = e;
            a = r[r.length - 2];
            for (var u = 0, l = r.length - 1; u < l; u++) {
                n = r[u];
                var v = s;
                s = [];
                o = v[v.length - 1];
                for (var c = 0, h = v.length; c < h; c++) {
                    f = v[c];
                    if (t(f, a, n)) {
                        if (!t(o, a, n)) {
                            s.push(i(a, n, o, f))
                        }
                        s.push(f)
                    } else if (t(o, a, n)) {
                        s.push(i(a, n, o, f))
                    }
                    o = f
                }
                a = n
            }
            if (s.length < 3) {
                return []
            }
            s.push(s[0]);
            return s
        }
    };
    var Pixel = function() {
        function t(e, r, t) {
            if (t === void 0) {
                t = false
            }
            this.className = "AMap.Pixel";
            if (isNaN(e) || isNaN(r)) {
                if (!window._AMapConfig) {
                    throw Error("Invalid Object: Pixel(" + e + ", " + r + ")")
                }
            }
            this["x"] = t ? Math.round(e) : Number(e);
            this["y"] = t ? Math.round(r) : Number(r)
        }
        t.prototype["getX"] = function() {
            return this.x
        }
        ;
        t.prototype["round"] = function() {
            return new t(Math.round(this.x),Math.round(this.y))
        }
        ;
        t.prototype["getY"] = function() {
            return this.y
        }
        ;
        t.prototype["toString"] = function() {
            return this.x + "," + this.y
        }
        ;
        t.prototype["equals"] = function(e) {
            if (!(e instanceof t)) {
                return false
            }
            var r = Math.max(Math.abs(this.x - e.x), Math.abs(this.y - e.y));
            return r <= 1e-9
        }
        ;
        t.prototype["toArray"] = function() {
            return [this.x, this.y]
        }
        ;
        t.prototype["subtract"] = function(e, r) {
            return new t(this.x - e.x,this.y - e.y,r)
        }
        ;
        t.prototype["multiplyBy"] = function(e, r) {
            return new t(this.x * e,this.y * e,r)
        }
        ;
        t.prototype["direction"] = function() {
            var e = this.x;
            var r = this.y;
            if (e === 0 && r === 0) {
                return null
            }
            if (e === 0) {
                return r > 0 ? 90 : 270
            }
            var t = Math.atan(r / e) * 180 / Math.PI;
            if (e < 0 && r > 0) {
                return t + 180
            } else if (e < 0 && r < 0) {
                return t + 180
            } else if (e > 0 && r < 0) {
                return t + 360
            } else {
                return t
            }
        }
        ;
        t.prototype.toJSON = function() {
            return [this.x, this.y]
        }
        ;
        return t
    }();
    var commonjsGlobal = typeof globalThis !== "undefined" ? globalThis : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : {};
    function unwrapExports(e) {
        return e && e.gP && Object.prototype.hasOwnProperty.call(e, "default") ? e["default"] : e
    }
    function createCommonjsModule(e, r) {
        return r = {
            exports: {}
        },
        e(r, r.exports),
        r.exports
    }
    function getCjsExportFromNamespace(e) {
        return e && e["default"] || e
    }
    var dist = createCommonjsModule(function(e, t) {
        (function(e, r) {
            r(t)
        }
        )(commonjsGlobal, function(e) {
            function $(e, r) {
                return e > r ? 1 : e < r ? -1 : 0
            }
            var g = function e(r, t) {
                if (r === void 0)
                    r = $;
                if (t === void 0)
                    t = false;
                this.bP = r;
                this.yP = null;
                this._size = 0;
                this.mP = !!t
            };
            var r = {
                size: {
                    configurable: true
                }
            };
            g.prototype.TP = function e(r) {
                var t = r.right;
                if (t) {
                    r.right = t.left;
                    if (t.left) {
                        t.left.parent = r
                    }
                    t.parent = r.parent
                }
                if (!r.parent) {
                    this.yP = t
                } else if (r === r.parent.left) {
                    r.parent.left = t
                } else {
                    r.parent.right = t
                }
                if (t) {
                    t.left = r
                }
                r.parent = t
            }
            ;
            g.prototype.wP = function e(r) {
                var t = r.left;
                if (t) {
                    r.left = t.right;
                    if (t.right) {
                        t.right.parent = r
                    }
                    t.parent = r.parent
                }
                if (!r.parent) {
                    this.yP = t
                } else if (r === r.parent.left) {
                    r.parent.left = t
                } else {
                    r.parent.right = t
                }
                if (t) {
                    t.right = r
                }
                r.parent = t
            }
            ;
            g.prototype.AP = function e(r) {
                while (r.parent) {
                    var t = r.parent;
                    if (!t.parent) {
                        if (t.left === r) {
                            this.wP(t)
                        } else {
                            this.TP(t)
                        }
                    } else if (t.left === r && t.parent.left === t) {
                        this.wP(t.parent);
                        this.wP(t)
                    } else if (t.right === r && t.parent.right === t) {
                        this.TP(t.parent);
                        this.TP(t)
                    } else if (t.left === r && t.parent.right === t) {
                        this.wP(t);
                        this.TP(t)
                    } else {
                        this.TP(t);
                        this.wP(t)
                    }
                }
            }
            ;
            g.prototype.xP = function e(r) {
                var t, i, a, n, o;
                while (r.parent) {
                    t = r.parent;
                    i = t.parent;
                    if (i && i.parent) {
                        a = i.parent;
                        if (a.left === i) {
                            a.left = r
                        } else {
                            a.right = r
                        }
                        r.parent = a
                    } else {
                        r.parent = null;
                        this.yP = r
                    }
                    n = r.left;
                    o = r.right;
                    if (r === t.left) {
                        if (i) {
                            if (i.left === t) {
                                if (t.right) {
                                    i.left = t.right;
                                    i.left.parent = i
                                } else {
                                    i.left = null
                                }
                                t.right = i;
                                i.parent = t
                            } else {
                                if (n) {
                                    i.right = n;
                                    n.parent = i
                                } else {
                                    i.right = null
                                }
                                r.left = i;
                                i.parent = r
                            }
                        }
                        if (o) {
                            t.left = o;
                            o.parent = t
                        } else {
                            t.left = null
                        }
                        r.right = t;
                        t.parent = r
                    } else {
                        if (i) {
                            if (i.right === t) {
                                if (t.left) {
                                    i.right = t.left;
                                    i.right.parent = i
                                } else {
                                    i.right = null
                                }
                                t.left = i;
                                i.parent = t
                            } else {
                                if (o) {
                                    i.left = o;
                                    o.parent = i
                                } else {
                                    i.left = null
                                }
                                r.right = i;
                                i.parent = r
                            }
                        }
                        if (n) {
                            t.right = n;
                            n.parent = t
                        } else {
                            t.right = null
                        }
                        r.left = t;
                        t.parent = r
                    }
                }
            }
            ;
            g.prototype.replace = function e(r, t) {
                if (!r.parent) {
                    this.yP = t
                } else if (r === r.parent.left) {
                    r.parent.left = t
                } else {
                    r.parent.right = t
                }
                if (t) {
                    t.parent = r.parent
                }
            }
            ;
            g.prototype.MP = function e(r) {
                if (r === void 0)
                    r = this.yP;
                if (r) {
                    while (r.left) {
                        r = r.left
                    }
                }
                return r
            }
            ;
            g.prototype.SP = function e(r) {
                if (r === void 0)
                    r = this.yP;
                if (r) {
                    while (r.right) {
                        r = r.right
                    }
                }
                return r
            }
            ;
            g.prototype.Xc = function e(r, t) {
                var i = this.yP;
                var a = null;
                var n = this.bP;
                var o;
                if (this.mP) {
                    while (i) {
                        a = i;
                        o = n(i.key, r);
                        if (o === 0) {
                            return
                        } else if (n(i.key, r) < 0) {
                            i = i.right
                        } else {
                            i = i.left
                        }
                    }
                } else {
                    while (i) {
                        a = i;
                        if (n(i.key, r) < 0) {
                            i = i.right
                        } else {
                            i = i.left
                        }
                    }
                }
                i = {
                    key: r,
                    data: t,
                    left: null,
                    right: null,
                    parent: a
                };
                if (!a) {
                    this.yP = i
                } else if (n(a.key, i.key) < 0) {
                    a.right = i
                } else {
                    a.left = i
                }
                this.xP(i);
                this._size++;
                return i
            }
            ;
            g.prototype.find = function e(r) {
                var t = this.yP;
                var i = this.bP;
                while (t) {
                    var a = i(t.key, r);
                    if (a < 0) {
                        t = t.right
                    } else if (a > 0) {
                        t = t.left
                    } else {
                        return t
                    }
                }
                return null
            }
            ;
            g.prototype.contains = function e(r) {
                var t = this.yP;
                var i = this.bP;
                while (t) {
                    var a = i(r, t.key);
                    if (a === 0) {
                        return true
                    } else if (a < 0) {
                        t = t.left
                    } else {
                        t = t.right
                    }
                }
                return false
            }
            ;
            g.prototype.remove = function e(r) {
                var t = this.find(r);
                if (!t) {
                    return false
                }
                this.xP(t);
                if (!t.left) {
                    this.replace(t, t.right)
                } else if (!t.right) {
                    this.replace(t, t.left)
                } else {
                    var i = this.MP(t.right);
                    if (i.parent !== t) {
                        this.replace(i, i.right);
                        i.right = t.right;
                        i.right.parent = i
                    }
                    this.replace(t, i);
                    i.left = t.left;
                    i.left.parent = i
                }
                this._size--;
                return true
            }
            ;
            g.prototype.removeNode = function e(r) {
                if (!r) {
                    return false
                }
                this.xP(r);
                if (!r.left) {
                    this.replace(r, r.right)
                } else if (!r.right) {
                    this.replace(r, r.left)
                } else {
                    var t = this.MP(r.right);
                    if (t.parent !== r) {
                        this.replace(t, t.right);
                        t.right = r.right;
                        t.right.parent = t
                    }
                    this.replace(r, t);
                    t.left = r.left;
                    t.left.parent = t
                }
                this._size--;
                return true
            }
            ;
            g.prototype.$P = function e(r) {
                var t = this.find(r);
                if (!t) {
                    return
                }
                this.xP(t);
                var i = t.left;
                var a = t.right;
                var n = null;
                if (i) {
                    i.parent = null;
                    n = this.SP(i);
                    this.xP(n);
                    this.yP = n
                }
                if (a) {
                    if (i) {
                        n.right = a
                    } else {
                        this.yP = a
                    }
                    a.parent = n
                }
                this._size--
            }
            ;
            g.prototype.pop = function e() {
                var r = this.yP
                  , t = null;
                if (r) {
                    while (r.left) {
                        r = r.left
                    }
                    t = {
                        key: r.key,
                        data: r.data
                    };
                    this.remove(r.key)
                }
                return t
            }
            ;
            g.prototype.next = function e(r) {
                var t = r;
                if (t) {
                    if (t.right) {
                        t = t.right;
                        while (t && t.left) {
                            t = t.left
                        }
                    } else {
                        t = r.parent;
                        while (t && t.right === r) {
                            r = t;
                            t = t.parent
                        }
                    }
                }
                return t
            }
            ;
            g.prototype.NM = function e(r) {
                var t = r;
                if (t) {
                    if (t.left) {
                        t = t.left;
                        while (t && t.right) {
                            t = t.right
                        }
                    } else {
                        t = r.parent;
                        while (t && t.left === r) {
                            r = t;
                            t = t.parent
                        }
                    }
                }
                return t
            }
            ;
            g.prototype.forEach = function e(r) {
                var t = this.yP;
                var i = []
                  , a = false
                  , n = 0;
                while (!a) {
                    if (t) {
                        i.push(t);
                        t = t.left
                    } else {
                        if (i.length > 0) {
                            t = i.pop();
                            r(t, n++);
                            t = t.right
                        } else {
                            a = true
                        }
                    }
                }
                return this
            }
            ;
            g.prototype.range = function e(r, t, i, a) {
                var n = [];
                var o = this.bP;
                var f = this.yP, s;
                while (n.length !== 0 || f) {
                    if (f) {
                        n.push(f);
                        f = f.left
                    } else {
                        f = n.pop();
                        s = o(f.key, t);
                        if (s > 0) {
                            break
                        } else if (o(f.key, r) >= 0) {
                            if (i.call(a, f)) {
                                return this
                            }
                        }
                        f = f.right
                    }
                }
                return this
            }
            ;
            g.prototype.keys = function e() {
                var r = this.yP;
                var t = []
                  , i = []
                  , a = false;
                while (!a) {
                    if (r) {
                        t.push(r);
                        r = r.left
                    } else {
                        if (t.length > 0) {
                            r = t.pop();
                            i.push(r.key);
                            r = r.right
                        } else {
                            a = true
                        }
                    }
                }
                return i
            }
            ;
            g.prototype.values = function e() {
                var r = this.yP;
                var t = []
                  , i = []
                  , a = false;
                while (!a) {
                    if (r) {
                        t.push(r);
                        r = r.left
                    } else {
                        if (t.length > 0) {
                            r = t.pop();
                            i.push(r.data);
                            r = r.right
                        } else {
                            a = true
                        }
                    }
                }
                return i
            }
            ;
            g.prototype.at = function e(r) {
                var t = this.yP;
                var i = []
                  , a = false
                  , n = 0;
                while (!a) {
                    if (t) {
                        i.push(t);
                        t = t.left
                    } else {
                        if (i.length > 0) {
                            t = i.pop();
                            if (n === r) {
                                return t
                            }
                            n++;
                            t = t.right
                        } else {
                            a = true
                        }
                    }
                }
                return null
            }
            ;
            g.prototype.load = function e(r, t, i) {
                if (r === void 0)
                    r = [];
                if (t === void 0)
                    t = [];
                if (i === void 0)
                    i = false;
                if (this._size !== 0) {
                    throw new Error("bulk-load: tree is not empty")
                }
                var a = r.length;
                if (i) {
                    u(r, t, 0, a - 1, this.bP)
                }
                this.yP = l(null, r, t, 0, a);
                this._size = a;
                return this
            }
            ;
            g.prototype.min = function e() {
                var r = this.MP(this.yP);
                if (r) {
                    return r.key
                } else {
                    return null
                }
            }
            ;
            g.prototype.max = function e() {
                var r = this.SP(this.yP);
                if (r) {
                    return r.key
                } else {
                    return null
                }
            }
            ;
            g.prototype.isEmpty = function e() {
                return this.yP === null
            }
            ;
            r.size.get = function() {
                return this._size
            }
            ;
            g.LP = function e(r, t, i, a, n) {
                return new g(i,n).load(r, t, a)
            }
            ;
            Object.defineProperties(g.prototype, r);
            function l(e, r, t, i, a) {
                var n = a - i;
                if (n > 0) {
                    var o = i + Math.floor(n / 2);
                    var f = r[o];
                    var s = t[o];
                    var u = {
                        key: f,
                        data: s,
                        parent: e
                    };
                    u.left = l(u, r, t, i, o);
                    u.right = l(u, r, t, o + 1, a);
                    return u
                }
                return null
            }
            function u(e, r, t, i, a) {
                if (t >= i) {
                    return
                }
                var n = e[t + i >> 1];
                var o = t - 1;
                var f = i + 1;
                while (true) {
                    do {
                        o++
                    } while (a(e[o], n) < 0);
                    do {
                        f--
                    } while (a(e[f], n) > 0);
                    if (o >= f) {
                        break
                    }
                    var s = e[o];
                    e[o] = e[f];
                    e[f] = s;
                    s = r[o];
                    r[o] = r[f];
                    r[f] = s
                }
                u(e, r, t, f, a);
                u(e, r, f + 1, i, a)
            }
            var o = 0;
            var s = 1;
            var v = 2;
            var c = 3;
            var y = 0;
            var f = 1;
            var m = 2;
            var h = 3;
            function p(e, r, t) {
                if (r === null) {
                    e.PP = false;
                    e.OP = true
                } else {
                    if (e._P === r._P) {
                        e.PP = !r.PP;
                        e.OP = r.OP
                    } else {
                        e.PP = !r.OP;
                        e.OP = r.NP() ? !r.PP : r.PP
                    }
                    if (r) {
                        e.CP = !a(r, t) || r.NP() ? r.CP : r
                    }
                }
                var i = a(e, t);
                if (i) {
                    e.hN = F(e, t)
                } else {
                    e.hN = 0
                }
            }
            function a(e, r) {
                switch (e.type) {
                case o:
                    switch (r) {
                    case y:
                        return !e.OP;
                    case f:
                        return e.OP;
                    case m:
                        return e._P && e.OP || !e._P && !e.OP;
                    case h:
                        return true
                    }
                    break;
                case v:
                    return r === y || r === f;
                case c:
                    return r === m;
                case s:
                    return false
                }
                return false
            }
            function F(e, r) {
                var t = !e.PP;
                var i = !e.OP;
                var a;
                switch (r) {
                case y:
                    a = t && i;
                    break;
                case f:
                    a = t || i;
                    break;
                case h:
                    a = t ^ i;
                    break;
                case m:
                    if (e._P) {
                        a = t && !i
                    } else {
                        a = i && !t
                    }
                    break
                }
                return a ? +1 : -1
            }
            var d = function e(r, t, i, a, n) {
                this.left = t;
                this.point = r;
                this.IP = i;
                this._P = a;
                this.type = n || o;
                this.PP = false;
                this.OP = false;
                this.CP = null;
                this.hN = 0;
                this.lN = -1;
                this.dN = -1;
                this.DP = true
            };
            var t = {
                EP: {
                    configurable: true
                }
            };
            d.prototype.FP = function e(r) {
                var t = this.point
                  , i = this.IP.point;
                return this.left ? (t[0] - r[0]) * (i[1] - r[1]) - (i[0] - r[0]) * (t[1] - r[1]) > 0 : (i[0] - r[0]) * (t[1] - r[1]) - (t[0] - r[0]) * (i[1] - r[1]) > 0
            }
            ;
            d.prototype.RP = function e(r) {
                return !this.FP(r)
            }
            ;
            d.prototype.NP = function e() {
                return this.point[0] === this.IP.point[0]
            }
            ;
            t.EP.get = function() {
                return this.hN !== 0
            }
            ;
            d.prototype.clone = function e() {
                var r = new d(this.point,this.left,this.IP,this._P,this.type);
                r.UP = this.UP;
                r.hN = this.hN;
                r.CP = this.CP;
                r.DP = this.DP;
                r.PP = this.PP;
                r.OP = this.OP;
                return r
            }
            ;
            Object.defineProperties(d.prototype, t);
            function _(e, r) {
                if (e[0] === r[0]) {
                    if (e[1] === r[1]) {
                        return true
                    } else {
                        return false
                    }
                }
                return false
            }
            var i = 11102230246251565e-32;
            var R = 134217729;
            var U = (3 + 8 * i) * i;
            function E(e, r, t, i, a) {
                var n, o, f, s;
                var u = r[0];
                var l = i[0];
                var v = 0;
                var c = 0;
                if (l > u === l > -u) {
                    n = u;
                    u = r[++v]
                } else {
                    n = l;
                    l = i[++c]
                }
                var h = 0;
                if (v < e && c < t) {
                    if (l > u === l > -u) {
                        o = u + n;
                        f = n - (o - u);
                        u = r[++v]
                    } else {
                        o = l + n;
                        f = n - (o - l);
                        l = i[++c]
                    }
                    n = o;
                    if (f !== 0) {
                        a[h++] = f
                    }
                    while (v < e && c < t) {
                        if (l > u === l > -u) {
                            o = n + u;
                            s = o - n;
                            f = n - (o - s) + (u - s);
                            u = r[++v]
                        } else {
                            o = n + l;
                            s = o - n;
                            f = n - (o - s) + (l - s);
                            l = i[++c]
                        }
                        n = o;
                        if (f !== 0) {
                            a[h++] = f
                        }
                    }
                }
                while (v < e) {
                    o = n + u;
                    s = o - n;
                    f = n - (o - s) + (u - s);
                    u = r[++v];
                    n = o;
                    if (f !== 0) {
                        a[h++] = f
                    }
                }
                while (c < t) {
                    o = n + l;
                    s = o - n;
                    f = n - (o - s) + (l - s);
                    l = i[++c];
                    n = o;
                    if (f !== 0) {
                        a[h++] = f
                    }
                }
                if (n !== 0 || h === 0) {
                    a[h++] = n
                }
                return h
            }
            function H(e, r) {
                var t = r[0];
                for (var i = 1; i < e; i++) {
                    t += r[i]
                }
                return t
            }
            function n(e) {
                return commonjsGlobal.Float64Array ? new Float64Array(e) : new Array(e)
            }
            var P = (3 + 16 * i) * i;
            var z = (2 + 12 * i) * i;
            var G = (9 + 64 * i) * i * i;
            var O = n(4);
            var B = n(8);
            var D = n(12);
            var j = n(16);
            var N = n(4);
            function L(e, r, t, i, a, n, o) {
                var f, s, u, l;
                var v, c, h, d, _, g, y, m, p, b, x, M, C, S;
                var w = e - a;
                var k = t - a;
                var T = r - n;
                var A = i - n;
                b = w * A;
                c = R * w;
                h = c - (c - w);
                d = w - h;
                c = R * A;
                _ = c - (c - A);
                g = A - _;
                x = d * g - (b - h * _ - d * _ - h * g);
                M = T * k;
                c = R * T;
                h = c - (c - T);
                d = T - h;
                c = R * k;
                _ = c - (c - k);
                g = k - _;
                C = d * g - (M - h * _ - d * _ - h * g);
                y = x - C;
                v = x - y;
                O[0] = x - (y + v) + (v - C);
                m = b + y;
                v = m - b;
                p = b - (m - v) + (y - v);
                y = p - M;
                v = p - y;
                O[1] = p - (y + v) + (v - M);
                S = m + y;
                v = S - m;
                O[2] = m - (S - v) + (y - v);
                O[3] = S;
                var I = H(4, O);
                var $ = z * o;
                if (I >= $ || -I >= $) {
                    return I
                }
                v = e - w;
                f = e - (w + v) + (v - a);
                v = t - k;
                u = t - (k + v) + (v - a);
                v = r - T;
                s = r - (T + v) + (v - n);
                v = i - A;
                l = i - (A + v) + (v - n);
                if (f === 0 && s === 0 && u === 0 && l === 0) {
                    return I
                }
                $ = G * o + U * Math.abs(I);
                I += w * l + A * f - (T * u + k * s);
                if (I >= $ || -I >= $) {
                    return I
                }
                b = f * A;
                c = R * f;
                h = c - (c - f);
                d = f - h;
                c = R * A;
                _ = c - (c - A);
                g = A - _;
                x = d * g - (b - h * _ - d * _ - h * g);
                M = s * k;
                c = R * s;
                h = c - (c - s);
                d = s - h;
                c = R * k;
                _ = c - (c - k);
                g = k - _;
                C = d * g - (M - h * _ - d * _ - h * g);
                y = x - C;
                v = x - y;
                N[0] = x - (y + v) + (v - C);
                m = b + y;
                v = m - b;
                p = b - (m - v) + (y - v);
                y = p - M;
                v = p - y;
                N[1] = p - (y + v) + (v - M);
                S = m + y;
                v = S - m;
                N[2] = m - (S - v) + (y - v);
                N[3] = S;
                var F = E(4, O, 4, N, B);
                b = w * l;
                c = R * w;
                h = c - (c - w);
                d = w - h;
                c = R * l;
                _ = c - (c - l);
                g = l - _;
                x = d * g - (b - h * _ - d * _ - h * g);
                M = T * u;
                c = R * T;
                h = c - (c - T);
                d = T - h;
                c = R * u;
                _ = c - (c - u);
                g = u - _;
                C = d * g - (M - h * _ - d * _ - h * g);
                y = x - C;
                v = x - y;
                N[0] = x - (y + v) + (v - C);
                m = b + y;
                v = m - b;
                p = b - (m - v) + (y - v);
                y = p - M;
                v = p - y;
                N[1] = p - (y + v) + (v - M);
                S = m + y;
                v = S - m;
                N[2] = m - (S - v) + (y - v);
                N[3] = S;
                var P = E(F, B, 4, N, D);
                b = f * l;
                c = R * f;
                h = c - (c - f);
                d = f - h;
                c = R * l;
                _ = c - (c - l);
                g = l - _;
                x = d * g - (b - h * _ - d * _ - h * g);
                M = s * u;
                c = R * s;
                h = c - (c - s);
                d = s - h;
                c = R * u;
                _ = c - (c - u);
                g = u - _;
                C = d * g - (M - h * _ - d * _ - h * g);
                y = x - C;
                v = x - y;
                N[0] = x - (y + v) + (v - C);
                m = b + y;
                v = m - b;
                p = b - (m - v) + (y - v);
                y = p - M;
                v = p - y;
                N[1] = p - (y + v) + (v - M);
                S = m + y;
                v = S - m;
                N[2] = m - (S - v) + (y - v);
                N[3] = S;
                var L = E(P, D, 4, N, j);
                return j[L - 1]
            }
            function V(e, r, t, i, a, n) {
                var o = (r - n) * (t - a);
                var f = (e - a) * (i - n);
                var s = o - f;
                if (o === 0 || f === 0 || o > 0 !== f > 0) {
                    return s
                }
                var u = Math.abs(o + f);
                if (Math.abs(s) >= P * u) {
                    return s
                }
                return -L(e, r, t, i, a, n, u)
            }
            function b(e, r, t) {
                var i = V(e[0], e[1], r[0], r[1], t[0], t[1]);
                if (i > 0) {
                    return -1
                }
                if (i < 0) {
                    return 1
                }
                return 0
            }
            function x(e, r) {
                var t = e.point;
                var i = r.point;
                if (t[0] > i[0]) {
                    return 1
                }
                if (t[0] < i[0]) {
                    return -1
                }
                if (t[1] !== i[1]) {
                    return t[1] > i[1] ? 1 : -1
                }
                return W(e, r, t)
            }
            function W(e, r, t, i) {
                if (e.left !== r.left) {
                    return e.left ? 1 : -1
                }
                if (b(t, e.IP.point, r.IP.point) !== 0) {
                    return !e.FP(r.IP.point) ? 1 : -1
                }
                return !e._P && r._P ? 1 : -1
            }
            function M(e, r, t) {
                var i = new d(r,false,e,e._P);
                var a = new d(r,true,e.IP,e._P);
                if (_(e.point, e.IP.point)) {
                    console.warn("what is that, a collapsed segment?", e)
                }
                i.UP = a.UP = e.UP;
                if (x(a, e.IP) > 0) {
                    e.IP.left = true;
                    a.left = false
                }
                e.IP.IP = a;
                e.IP = i;
                t.push(a);
                t.push(i);
                return t
            }
            function C(e, r) {
                return e[0] * r[1] - e[1] * r[0]
            }
            function S(e, r) {
                return e[0] * r[0] + e[1] * r[1]
            }
            function q(e, r, t, i, a) {
                var n = [r[0] - e[0], r[1] - e[1]];
                var o = [i[0] - t[0], i[1] - t[1]];
                function f(e, r, t) {
                    return [e[0] + r * t[0], e[1] + r * t[1]]
                }
                var s = [t[0] - e[0], t[1] - e[1]];
                var u = C(n, o);
                var l = u * u;
                var v = S(n, n);
                if (l > 0) {
                    var c = C(s, o) / u;
                    if (c < 0 || c > 1) {
                        return null
                    }
                    var h = C(s, n) / u;
                    if (h < 0 || h > 1) {
                        return null
                    }
                    if (c === 0 || c === 1) {
                        return a ? null : [f(e, c, n)]
                    }
                    if (h === 0 || h === 1) {
                        return a ? null : [f(t, h, o)]
                    }
                    return [f(e, c, n)]
                }
                u = C(s, n);
                l = u * u;
                if (l > 0) {
                    return null
                }
                var d = S(n, s) / v;
                var _ = d + S(n, o) / v;
                var g = Math.min(d, _);
                var y = Math.max(d, _);
                if (g <= 1 && y >= 0) {
                    if (g === 1) {
                        return a ? null : [f(e, g > 0 ? g : 0, n)]
                    }
                    if (y === 0) {
                        return a ? null : [f(e, y < 1 ? y : 1, n)]
                    }
                    if (a && g === 0 && y === 1) {
                        return null
                    }
                    return [f(e, g > 0 ? g : 0, n), f(e, y < 1 ? y : 1, n)]
                }
                return null
            }
            function w(e, r, t) {
                var i = q(e.point, e.IP.point, r.point, r.IP.point);
                var a = i ? i.length : 0;
                if (a === 0) {
                    return 0
                }
                if (a === 1 && (_(e.point, r.point) || _(e.IP.point, r.IP.point))) {
                    return 0
                }
                if (a === 2 && e._P === r._P) {
                    return 0
                }
                if (a === 1) {
                    if (!_(e.point, i[0]) && !_(e.IP.point, i[0])) {
                        M(e, i[0], t)
                    }
                    if (!_(r.point, i[0]) && !_(r.IP.point, i[0])) {
                        M(r, i[0], t)
                    }
                    return 1
                }
                var n = [];
                var o = false;
                var f = false;
                if (_(e.point, r.point)) {
                    o = true
                } else if (x(e, r) === 1) {
                    n.push(r, e)
                } else {
                    n.push(e, r)
                }
                if (_(e.IP.point, r.IP.point)) {
                    f = true
                } else if (x(e.IP, r.IP) === 1) {
                    n.push(r.IP, e.IP)
                } else {
                    n.push(e.IP, r.IP)
                }
                if (o && f || o) {
                    r.type = s;
                    e.type = r.PP === e.PP ? v : c;
                    if (o && !f) {
                        M(n[1].IP, n[0].point, t)
                    }
                    return 2
                }
                if (f) {
                    M(n[0], n[1].point, t);
                    return 3
                }
                if (n[0] !== n[3].IP) {
                    M(n[0], n[1].point, t);
                    M(n[1], n[2].point, t);
                    return 3
                }
                M(n[0], n[1].point, t);
                M(n[3].IP, n[2].point, t);
                return 3
            }
            function K(e, r) {
                if (e === r) {
                    return 0
                }
                if (b(e.point, e.IP.point, r.point) !== 0 || b(e.point, e.IP.point, r.IP.point) !== 0) {
                    if (_(e.point, r.point)) {
                        return e.FP(r.IP.point) ? -1 : 1
                    }
                    if (e.point[0] === r.point[0]) {
                        return e.point[1] < r.point[1] ? -1 : 1
                    }
                    if (x(e, r) === 1) {
                        return r.RP(e.point) ? -1 : 1
                    }
                    return e.FP(r.point) ? -1 : 1
                }
                if (e._P === r._P) {
                    var t = e.point
                      , i = r.point;
                    if (t[0] === i[0] && t[1] === i[1]) {
                        t = e.IP.point;
                        i = r.IP.point;
                        if (t[0] === i[0] && t[1] === i[1]) {
                            return 0
                        } else {
                            return e.UP > r.UP ? 1 : -1
                        }
                    }
                } else {
                    return e._P ? -1 : 1
                }
                return x(e, r) === 1 ? 1 : -1
            }
            function Y(e, r, t, i, a, n) {
                var o = new g(K);
                var f = [];
                var s = Math.min(i[2], a[2]);
                var u, l, v;
                while (e.length !== 0) {
                    var c = e.pop();
                    f.push(c);
                    if (n === y && c.point[0] > s || n === m && c.point[0] > i[2]) {
                        break
                    }
                    if (c.left) {
                        l = u = o.Xc(c);
                        v = o.MP();
                        if (u !== v) {
                            u = o.NM(u)
                        } else {
                            u = null
                        }
                        l = o.next(l);
                        var h = u ? u.key : null;
                        var d = void 0;
                        p(c, h, n);
                        if (l) {
                            if (w(c, l.key, e) === 2) {
                                p(c, h, n);
                                p(c, l.key, n)
                            }
                        }
                        if (u) {
                            if (w(u.key, c, e) === 2) {
                                var _ = u;
                                if (_ !== v) {
                                    _ = o.NM(_)
                                } else {
                                    _ = null
                                }
                                d = _ ? _.key : null;
                                p(h, d, n);
                                p(c, h, n)
                            }
                        }
                    } else {
                        c = c.IP;
                        l = u = o.find(c);
                        if (u && l) {
                            if (u !== v) {
                                u = o.NM(u)
                            } else {
                                u = null
                            }
                            l = o.next(l);
                            o.remove(c);
                            if (l && u) {
                                w(u.key, l.key, e)
                            }
                        }
                    }
                }
                return f
            }
            var Z = function e() {
                this.points = [];
                this.vN = [];
                this.gN = null;
                this.depth = null
            };
            Z.prototype.pN = function e() {
                return this.gN == null
            }
            ;
            function X(e) {
                var r, t, i, a;
                var n = [];
                for (t = 0,
                i = e.length; t < i; t++) {
                    r = e[t];
                    if (r.left && r.EP || !r.left && r.IP.EP) {
                        n.push(r)
                    }
                }
                var o = false;
                while (!o) {
                    o = true;
                    for (t = 0,
                    i = n.length; t < i; t++) {
                        if (t + 1 < i && x(n[t], n[t + 1]) === 1) {
                            a = n[t];
                            n[t] = n[t + 1];
                            n[t + 1] = a;
                            o = false
                        }
                    }
                }
                for (t = 0,
                i = n.length; t < i; t++) {
                    r = n[t];
                    r.lN = t
                }
                for (t = 0,
                i = n.length; t < i; t++) {
                    r = n[t];
                    if (!r.left) {
                        a = r.lN;
                        r.lN = r.IP.lN;
                        r.IP.lN = a
                    }
                }
                return n
            }
            function J(e, r, t, i) {
                var a = e + 1, n = r[e].point, o;
                var f = r.length;
                if (a < f) {
                    o = r[a].point
                }
                while (a < f && o[0] === n[0] && o[1] === n[1]) {
                    if (!t[a]) {
                        return a
                    } else {
                        a++
                    }
                    o = r[a].point
                }
                a = e - 1;
                while (t[a] && a > i) {
                    a--
                }
                return a
            }
            function Q(e, r, t) {
                var i = new Z;
                if (e.CP != null) {
                    var a = e.CP;
                    var n = a.dN;
                    var o = a.hN;
                    if (o > 0) {
                        var f = r[n];
                        if (f.gN != null) {
                            var s = f.gN;
                            r[s].vN.push(t);
                            i.gN = s;
                            i.depth = r[n].depth
                        } else {
                            r[n].vN.push(t);
                            i.gN = n;
                            i.depth = r[n].depth + 1
                        }
                    } else {
                        i.gN = null;
                        i.depth = r[n].depth
                    }
                } else {
                    i.gN = null;
                    i.depth = 0
                }
                return i
            }
            function ee(e) {
                var o, r;
                var f = X(e);
                var s = {};
                var u = [];
                var t = function() {
                    if (s[o]) {
                        return
                    }
                    var r = u.length;
                    var e = Q(f[o], u, r);
                    var t = function(e) {
                        s[e] = true;
                        f[e].dN = r
                    };
                    var i = o;
                    var a = o;
                    var n = f[o].point;
                    e.points.push(n);
                    while (true) {
                        t(i);
                        i = f[i].lN;
                        t(i);
                        e.points.push(f[i].point);
                        i = J(i, f, s, a);
                        if (i == a) {
                            break
                        }
                    }
                    u.push(e)
                };
                for (o = 0,
                r = f.length; o < r; o++)
                    t();
                return u
            }
            var re = k;
            var te = k;
            function k(e, r) {
                if (!(this instanceof k)) {
                    return new k(e,r)
                }
                this.data = e || [];
                this.length = this.data.length;
                this.compare = r || ie;
                if (this.length > 0) {
                    for (var t = (this.length >> 1) - 1; t >= 0; t--) {
                        this.zP(t)
                    }
                }
            }
            function ie(e, r) {
                return e < r ? -1 : e > r ? 1 : 0
            }
            k.prototype = {
                push: function(e) {
                    this.data.push(e);
                    this.length++;
                    this.El(this.length - 1)
                },
                pop: function() {
                    if (this.length === 0) {
                        return undefined
                    }
                    var e = this.data[0];
                    this.length--;
                    if (this.length > 0) {
                        this.data[0] = this.data[this.length];
                        this.zP(0)
                    }
                    this.data.pop();
                    return e
                },
                GP: function() {
                    return this.data[0]
                },
                El: function(e) {
                    var r = this.data;
                    var t = this.compare;
                    var i = r[e];
                    while (e > 0) {
                        var a = e - 1 >> 1;
                        var n = r[a];
                        if (t(i, n) >= 0) {
                            break
                        }
                        r[e] = n;
                        e = a
                    }
                    r[e] = i
                },
                zP: function(e) {
                    var r = this.data;
                    var t = this.compare;
                    var i = this.length >> 1;
                    var a = r[e];
                    while (e < i) {
                        var n = (e << 1) + 1;
                        var o = n + 1;
                        var f = r[n];
                        if (o < this.length && t(r[o], f) < 0) {
                            n = o;
                            f = r[o]
                        }
                        if (t(f, a) >= 0) {
                            break
                        }
                        r[e] = f;
                        e = n
                    }
                    r[e] = a
                }
            };
            re.default = te;
            var ae = Math.max;
            var ne = Math.min;
            var T = 0;
            function oe(e, r, t, i, a, n) {
                var o, f, s, u, l, v;
                for (o = 0,
                f = e.length - 1; o < f; o++) {
                    s = e[o];
                    u = e[o + 1];
                    l = new d(s,false,undefined,r);
                    v = new d(u,false,l,r);
                    l.IP = v;
                    if (s[0] === u[0] && s[1] === u[1]) {
                        continue
                    }
                    l.UP = v.UP = t;
                    if (!n) {
                        l.DP = false;
                        v.DP = false
                    }
                    if (x(l, v) > 0) {
                        v.left = true
                    } else {
                        l.left = true
                    }
                    var c = s[0]
                      , h = s[1];
                    a[0] = ne(a[0], c);
                    a[1] = ne(a[1], h);
                    a[2] = ae(a[2], c);
                    a[3] = ae(a[3], h);
                    i.push(l);
                    i.push(v)
                }
            }
            function fe(e, r, t, i, a) {
                var n = new re(null,x);
                var o, f, s, u, l, v;
                for (s = 0,
                u = e.length; s < u; s++) {
                    o = e[s];
                    for (l = 0,
                    v = o.length; l < v; l++) {
                        f = l === 0;
                        if (f) {
                            T++
                        }
                        oe(o[l], true, T, n, t, f)
                    }
                }
                for (s = 0,
                u = r.length; s < u; s++) {
                    o = r[s];
                    for (l = 0,
                    v = o.length; l < v; l++) {
                        f = l === 0;
                        if (a === m) {
                            f = false
                        }
                        if (f) {
                            T++
                        }
                        oe(o[l], false, T, n, i, f)
                    }
                }
                return n
            }
            var A = [];
            function se(e, r, t) {
                var i = null;
                if (e.length * r.length === 0) {
                    if (t === y) {
                        i = A
                    } else if (t === m) {
                        i = e
                    } else if (t === f || t === h) {
                        i = e.length === 0 ? r : e
                    }
                }
                return i
            }
            function ue(e, r, t, i, a) {
                var n = null;
                if (t[0] > i[2] || i[0] > t[2] || t[1] > i[3] || i[1] > t[3]) {
                    if (a === y) {
                        n = A
                    } else if (a === m) {
                        n = e
                    } else if (a === f || a === h) {
                        n = e.concat(r)
                    }
                }
                return n
            }
            function I(e, r, t) {
                if (typeof e[0][0][0] === "number") {
                    e = [e]
                }
                if (typeof r[0][0][0] === "number") {
                    r = [r]
                }
                var i = se(e, r, t);
                if (i) {
                    return i === A ? null : i
                }
                var a = [Infinity, Infinity, -Infinity, -Infinity];
                var n = [Infinity, Infinity, -Infinity, -Infinity];
                var o = fe(e, r, a, n, t);
                i = ue(e, r, a, n, t);
                if (i) {
                    return i === A ? null : i
                }
                var f = Y(o, e, r, a, n, t);
                var s = ee(f);
                var u = [];
                for (var l = 0; l < s.length; l++) {
                    var v = s[l];
                    if (v.pN()) {
                        var c = [v.points];
                        for (var h = 0; h < v.vN.length; h++) {
                            var d = v.vN[h];
                            c.push(s[d].points)
                        }
                        u.push(c)
                    }
                }
                return u
            }
            function le(e, r) {
                return I(e, r, f)
            }
            function ve(e, r) {
                return I(e, r, m)
            }
            function ce(e, r) {
                return I(e, r, h)
            }
            function he(e, r) {
                return I(e, r, y)
            }
            var de = {
                VP: f,
                BP: m,
                HP: y,
                WP: h
            };
            e.ZP = ve;
            e.qP = he;
            e.YP = de;
            e.$q = le;
            e.xor = ce;
            Object.defineProperty(e, "gP", {
                value: true
            })
        })
    });
    var martinez = unwrapExports(dist);
    var Const = {
        vr: Math.PI / 180,
        mr: 180 / Math.PI,
        yr: 6378137
    };
    function getSphericalCrs(e, r) {
        function t(e) {
            switch (e) {
            case "EPSG3857":
                return ProjectionManager.getProjection("EPSG:3857");
            case "EPSG4326":
                return ProjectionManager.getProjection("EPSG:4326")
            }
            return ProjectionManager.getProjection("EPSG3857")
        }
        var i = t(e);
        return {
            project: function(e) {
                if (Util$3.isArray(e)) {
                    e = new LngLat$2(e[0],e[1])
                }
                return i.project(e.lng, e.lat)
            },
            unproject: function(e) {
                if (Util$3.isArray(e)) {
                    e = new Pixel(e[0],e[1])
                }
                return i.unproject(e.x, e.y)
            },
            normalizePoint: function(e) {
                return Util$3.parseLngLatData(e)
            },
            distance: function(e, r) {
                r = this["normalizePoint"](r);
                if (Util$3.isArray(r)) {
                    return this["distanceToLine"](e, r)
                }
                e = this["normalizePoint"](e);
                var t = Const.vr;
                var i = Math.cos;
                var a = e.lat * t
                  , n = e.lng * t
                  , o = r.lat * t
                  , f = r.lng * t;
                var s = Const.yr * 2;
                var u = o - a;
                var l = f - n;
                var v = (1 - i(u) + (1 - i(l)) * i(a) * i(o)) / 2;
                return s * Math.asin(Math.sqrt(v))
            },
            ringArea: function(e) {
                e = this["normalizeLine"](e);
                var r = Const.yr * Const.vr;
                var t = 0
                  , i = e
                  , a = i.length;
                if (a < 3) {
                    return 0
                }
                var n = 0;
                for (; n < a - 1; n += 1) {
                    var o = i[n];
                    var f = i[n + 1];
                    var s = o.lng * r * Math.cos(o.lat * Const.vr);
                    var u = o.lat * r;
                    var l = f.lng * r * Math.cos(f.lat * Const.vr);
                    var v = f.lat * r;
                    t += s * v - l * u
                }
                var c = i[n];
                var h = i[0];
                var d = c.lng * r * Math.cos(c.lat * Const.vr);
                var _ = c.lat * r;
                var g = h.lng * r * Math.cos(h.lat * Const.vr);
                var y = h.lat * r;
                t += d * y - g * _;
                var m = .5 * Math.abs(t);
                return m
            },
            sphericalCalotteArea: function(e) {
                var r = Const.yr;
                var t = e / r;
                var i = r - r * Math.cos(t);
                return 2 * Math.PI * r * i
            }
        }
    }
    function getPlanarCrs() {
        return {
            normalizePoint: function(e) {
                if (e && e["x"] && e["y"]) {
                    return [e["x"], e["y"]]
                }
                return e
            },
            distance: function(e, r) {
                var t = e[0] - r[0]
                  , i = e[1] - r[1];
                return Math.sqrt(t * t + i * i)
            },
            project: function(e) {
                return e
            },
            unproject: function(e) {
                return e
            },
            ringArea: function(e) {
                var r = [0, 0];
                var t = [0, 0];
                var i = 0;
                var a = e[0];
                var n = e.length;
                for (var o = 2; o < n; o++) {
                    var f = e[o - 1];
                    var s = e[o];
                    r[0] = a[0] - s[0];
                    r[1] = a[1] - s[1];
                    t[0] = a[0] - f[0];
                    t[1] = a[1] - f[1];
                    i += r[0] * t[1] - r[1] * t[0]
                }
                return i / 2
            }
        }
    }
    function isClockwise(e) {
        var r = 0;
        var t = e.length;
        for (var i = 0; i < t - 1; i++) {
            var a = e[i]
              , n = e[i + 1];
            r += (n[0] - a[0]) * (n[1] + a[1])
        }
        if (e[t - 1][0] !== e[0][0] || e[t - 1][1] !== e[0][1]) {
            var a = e[t - 1]
              , n = e[0];
            r += (n[0] - a[0]) * (n[1] + a[1])
        }
        return r > 0
    }
    var GeometryUtilCls = function() {
        function r(e) {
            this.CLASS_NAME = "AMap.GeometryUtil";
            this._opts = assign({
                onSegmentTolerance: 5,
                crs: "EPSG3857",
                maxZoom: zoomRange[1]
            }, e);
            this["setCrs"](this._opts["crs"])
        }
        r.prototype["clone"] = function(e) {
            return new r(assign({}, this._opts, e))
        }
        ;
        r.prototype["isPoint"] = function(e) {
            return e && (e instanceof LngLat$2 || Util$3.isArray(e) && !isNaN(e[0]))
        }
        ;
        r.prototype["normalizePoint"] = function(e) {
            return e
        }
        ;
        r.prototype["normalizeLine"] = function(e) {
            var r = [];
            for (var t = 0, i = e.length; t < i; t++) {
                r.push(this["normalizePoint"](e[t]))
            }
            return r
        }
        ;
        r.prototype["normalizeMultiLines"] = function(e) {
            if (Util$3.isArray(e) && this["isPoint"](e[0])) {
                e = [e]
            }
            var r = [];
            for (var t = 0, i = e.length; t < i; t++) {
                r.push(this["normalizeLine"](e[t]))
            }
            return r
        }
        ;
        r.prototype["setCrs"] = function(e) {
            var r;
            if (e && e["project"] && e["unproject"]) {
                r = e
            } else if (e === "plane") {
                r = getPlanarCrs()
            } else {
                r = getSphericalCrs(e, this._opts["maxZoom"])
            }
            assign(this, r);
            return
        }
        ;
        r.prototype["distance"] = function(e, r) {
            throw new Error("distance Not implemented!")
        }
        ;
        r.prototype.xr = function(e, r) {
            e = this["normalizeLine"](e);
            if (!this["isPoint"](e[0])) {
                e = e[0]
            }
            var t = [];
            for (var i = 0, a = e.length; i < a; i++) {
                t.push(this["project"](e[i]))
            }
            if (r === true) {
                t = this["makesureClockwise"](t)
            } else if (r === false) {
                t = this["makesureClockwise"](t);
                t.reverse()
            }
            return t
        }
        ;
        r.prototype.br = function(e) {
            var r = [];
            for (var t = 0, i = e.length; t < i; t++) {
                r.push(this["unproject"](e[t]))
            }
            return r
        }
        ;
        r.prototype["closestOnSegment"] = function(e, r, t) {
            var i = Coordinate.closestOnSegment(this["project"](e), this.xr([r, t]));
            return this["unproject"](i)
        }
        ;
        r.prototype["closestOnLine"] = function(e, r) {
            r = this["normalizeLine"](r);
            var t = Infinity, i;
            for (var a = 0, n = r.length; a < n - 1; a++) {
                var o = this["closestOnSegment"](e, r[a], r[a + 1])
                  , f = this["distance"](e, o);
                if (f < t) {
                    t = f;
                    i = o
                }
            }
            return i
        }
        ;
        r.prototype["distanceToSegment"] = function(e, r, t) {
            return this["distanceToLine"](e, [r, t])
        }
        ;
        r.prototype["distanceToLine"] = function(e, r) {
            r = this["normalizeLine"](r);
            if (!this["isPoint"](r[0])) {
                r = r[0]
            }
            var t = Infinity;
            for (var i = 0, a = r.length; i < a - 1; i++) {
                var n = this["closestOnSegment"](e, r[i], r[i + 1]);
                t = Math.min(t, this["distance"](e, n))
            }
            return t
        }
        ;
        r.prototype["distanceToPolygon"] = function(e, r) {
            if (this["isPointInRing"](e, r)) {
                return 0
            } else {
                return this["distanceToLine"](e, r)
            }
        }
        ;
        r.prototype["isPointOnSegment"] = function(e, r, t, i) {
            if (!i && i !== 0 || i < 0) {
                i = this._opts["onSegmentTolerance"]
            }
            return this["distanceToSegment"](e, r, t) <= i
        }
        ;
        r.prototype["isPointOnLine"] = function(e, r, t) {
            r = this["normalizeLine"](r);
            for (var i = 0, a = r.length; i < a - 1; i++) {
                if (this["isPointOnSegment"](e, r[i], r[i + 1], t)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["isPointOnRing"] = function(e, r, t) {
            r = this["normalizeLine"](r);
            for (var i = 0, a = r.length; i < a; i++) {
                if (this["isPointOnSegment"](e, r[i], r[i === a - 1 ? 0 : i + 1], t)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["isPointOnPolygon"] = function(e, r, t) {
            r = this["normalizeMultiLines"](r);
            for (var i = 0, a = r.length; i < a; i++) {
                if (this["isPointOnRing"](e, r[i], t)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["makesureClockwise"] = function(e) {
            if (!isClockwise(e)) {
                e = [].concat(e);
                e.reverse()
            }
            return e
        }
        ;
        r.prototype["makesureAntiClockwise"] = function(e) {
            if (isClockwise(e)) {
                e = [].concat(e);
                e.reverse()
            }
            return e
        }
        ;
        r.prototype["pointInRing"] = function(e, r, t) {
            var i = false;
            for (var a = 0; a < r.length && !i; a++) {
                if (f(e, r[a][0], t)) {
                    var n = false;
                    var o = 1;
                    while (o < r[a].length && !n) {
                        if (f(e, r[a][o], !t)) {
                            n = true
                        }
                        o++
                    }
                    if (!n) {
                        i = true
                    }
                }
            }
            function f(e, r, t) {
                var i = false;
                if (r[0][0] === r[r.length - 1][0] && r[0][1] === r[r.length - 1][1]) {
                    r = r.slice(0, r.length - 1)
                }
                for (var a = 0, n = r.length - 1; a < r.length; n = a++) {
                    var o = r[a][0];
                    var f = r[a][1];
                    var s = r[n][0];
                    var u = r[n][1];
                    var l = e[1] * (o - s) + f * (s - e[0]) + u * (e[0] - o) === 0 && (o - e[0]) * (s - e[0]) <= 0 && (f - e[1]) * (u - e[1]) <= 0;
                    if (l) {
                        return !t
                    }
                    var v = f > e[1] !== u > e[1] && e[0] < (s - o) * (e[1] - f) / (u - f) + o;
                    if (v) {
                        i = !i
                    }
                }
                return i
            }
            return i
        }
        ;
        r.prototype["isPointInBbox"] = function(e, r) {
            var t = Infinity;
            var i = Infinity;
            var a = -Infinity;
            var n = -Infinity;
            for (var o = 0; o < r.length; o++) {
                var f = r[o];
                t = Math.min(t, f[0]);
                a = Math.max(a, f[0]);
                i = Math.min(i, f[1]);
                n = Math.max(n, f[1])
            }
            return e[0] > t && e[0] < a && e[1] > i && e[1] < n
        }
        ;
        r.prototype["isPointInRing"] = function(e, r) {
            r = this["normalizeLine"](r);
            var t = this.xr(r, true);
            var i = Coordinate.ss(this["project"](e), t, false);
            return i
        }
        ;
        r.prototype["isRingInRing"] = function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                if (!this["isPointInRing"](e[t], r)) {
                    return false
                }
            }
            return true
        }
        ;
        r.prototype["isRingInRingByOutsea"] = function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                if (this["isPointInRing"](e[t], r)) {
                    return true
                }
            }
            for (var t = 0, i = r.length; t < i; t++) {
                if (this["isPointInRing"](r[t], e)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["isRingInRingByMapbox"] = function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                if (!this["isPointInRing"](e[t], r)) {
                    return false
                }
            }
            return true
        }
        ;
        r.prototype["isRingInRingByMapboxB"] = function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                if (this["isPointInRing"](e[t], r)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["isPixelRingInRing"] = function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                var a = Coordinate.ss(e[t], r, false);
                if (a) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["isPointInPolygon"] = function(e, r) {
            r = this["normalizeMultiLines"](r);
            var t;
            for (var i = 0, a = r.length; i < a; i += 1) {
                t = this["isPointInRing"](e, r[i]);
                if (i > 0) {
                    t = !t
                }
                if (!t) {
                    break
                }
            }
            return Boolean(t)
        }
        ;
        r.prototype["isPointInPolygons"] = function(e, r) {
            for (var t = 0; t < r.length; t++) {
                var i = r[t];
                i = this["normalizeMultiLines"](i);
                var a = void 0;
                for (var n = 0, o = i.length; n < o; n += 1) {
                    a = this["isPointInRing"](e, i[n]);
                    if (n > 0) {
                        a = !a
                    }
                    if (!a) {
                        break
                    }
                }
                if (a) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesSegmentsIntersect"] = function(e, r, t, i) {
            var a = this.xr([e, r, t, i]);
            e = a[0];
            r = a[1];
            t = a[2];
            i = a[3];
            var n = false;
            var o = (i[0] - t[0]) * (e[1] - t[1]) - (i[1] - t[1]) * (e[0] - t[0]);
            var f = (r[0] - e[0]) * (e[1] - t[1]) - (r[1] - e[1]) * (e[0] - t[0]);
            var s = (i[1] - t[1]) * (r[0] - e[0]) - (i[0] - t[0]) * (r[1] - e[1]);
            if (s !== 0) {
                var u = o / s;
                var l = f / s;
                if (0 <= u && u <= 1 && 0 <= l && l <= 1) {
                    n = true
                }
            }
            return n
        }
        ;
        r.prototype["doesSegmentLineIntersect"] = function(e, r, t) {
            t = this["normalizeLine"](t);
            for (var i = 0, a = t.length; i < a - 1; i++) {
                if (this["doesSegmentsIntersect"](e, r, t[i], t[i + 1])) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesSegmentRingIntersect"] = function(e, r, t) {
            t = this["normalizeLine"](t);
            for (var i = 0, a = t.length; i < a; i++) {
                if (this["doesSegmentsIntersect"](e, r, t[i], t[i === a - 1 ? 0 : i + 1])) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesSegmentPolygonIntersect"] = function(e, r, t) {
            t = this["normalizeMultiLines"](t);
            for (var i = 0, a = t.length; i < a; i++) {
                if (this["doesSegmentRingIntersect"](e, r, t[i])) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesLineLineIntersect"] = function(e, r) {
            e = this["normalizeLine"](e);
            for (var t = 0, i = e.length; t < i - 1; t++) {
                if (this["doesSegmentLineIntersect"](e[t], e[t + 1], r)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesLineRingIntersect"] = function(e, r) {
            e = this["normalizeLine"](e);
            for (var t = 0, i = e.length; t < i - 1; t++) {
                if (this["doesSegmentRingIntersect"](e[t], e[t + 1], r)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["doesPolygonPolygonIntersect"] = function(e, r) {
            if (this["doesRingRingIntersect"](r, e) || this["isRingInRing"](e, r) || this["isRingInRing"](r, e)) {
                return true
            }
            return false
        }
        ;
        r.prototype["doesRingRingIntersect"] = function(e, r) {
            e = this["normalizeLine"](e);
            for (var t = 0, i = e.length; t < i; t++) {
                if (this["doesSegmentRingIntersect"](e[t], e[t === i - 1 ? 0 : t + 1], r)) {
                    return true
                }
            }
            return false
        }
        ;
        r.prototype["along"] = function(e, r) {
            var t = 0;
            for (var i = 0; i < e.length - 1; i += 1) {
                var a = this["distance"](e[i], e[i + 1]);
                if (a + t < r) {
                    t += a;
                    continue
                }
                var n = (r - t) / a;
                return [e[i][0] + n * (e[i + 1][0] - e[i][0]), e[i][1] + n * (e[i + 1][1] - e[i][1]), i]
            }
            return null
        }
        ;
        r.prototype.gr = function(e, r) {
            e = this["makesureAntiClockwise"](e);
            r = this["makesureClockwise"](r);
            var n, o, f, s;
            var t = function(e) {
                return (o[0] - n[0]) * (e[1] - n[1]) > (o[1] - n[1]) * (e[0] - n[0])
            };
            var i = function() {
                var e = [n[0] - o[0], n[1] - o[1]]
                  , r = [f[0] - s[0], f[1] - s[1]]
                  , t = n[0] * o[1] - n[1] * o[0]
                  , i = f[0] * s[1] - f[1] * s[0]
                  , a = 1 / (e[0] * r[1] - e[1] * r[0]);
                return [(t * r[0] - i * e[0]) * a, (t * r[1] - i * e[1]) * a]
            };
            var a = e;
            n = r[r.length - 1];
            for (var u = 0, l = r.length; u < l; u++) {
                o = r[u];
                var v = a;
                a = [];
                f = v[v.length - 1];
                for (var c = 0, h = v.length; c < h; c++) {
                    s = v[c];
                    if (t(s)) {
                        if (!t(f)) {
                            a.push(i())
                        }
                        a.push(s)
                    } else if (t(f)) {
                        a.push(i())
                    }
                    f = s
                }
                n = o
            }
            return a
        }
        ;
        r.prototype["ringRingClip"] = function(e, r) {
            e = this.xr(e);
            r = this.xr(r);
            if (!isEqual(e[0], e[e.length - 1])) {
                e.push(e[0].slice())
            }
            if (!isEqual(r[0], r[r.length - 1])) {
                r.push(r[0].slice())
            }
            var t = martinez.qP([e], [r]);
            if (!t || !t[0] || !t[0][0]) {
                return []
            }
            return this.br(t[0][0])
        }
        ;
        r.prototype["ringArea"] = function(e) {
            throw new Error("distance Not implemented!")
        }
        ;
        r.prototype["distanceOfLine"] = function(e) {
            e = this["normalizeLine"](e);
            var r = 0;
            for (var t = 0, i = e.length; t < i - 1; t++) {
                r += this["distance"](e[t], e[t + 1])
            }
            return r
        }
        ;
        r.prototype["isClockwise"] = function(e) {
            e = this.xr(e);
            return isClockwise(e)
        }
        ;
        r.prototype["typePolygon"] = function(e) {
            if (!isArray(e) || !isArray(e[0])) {
                return "never"
            }
            if (e[0][0].length === 2 || e[0][0]instanceof LngLat$2) {
                return "Polygon"
            } else if (e[0][0].length > 2 && (e[0][0][0].length === 2 || e[0][0][0]instanceof LngLat$2)) {
                return "MultiPolygon"
            } else {
                return "never"
            }
        }
        ;
        return r
    }();
    var GeometryUtilInstance = new GeometryUtilCls({});
    var PlaneGeometryUtilInstance = new GeometryUtilCls({});
    PlaneGeometryUtilInstance["setCrs"]("plane");
    var GeometryUtil = {
        distance: GeometryUtilInstance.distance.bind(GeometryUtilInstance),
        ringArea: GeometryUtilInstance.ringArea.bind(GeometryUtilInstance),
        isClockwise: GeometryUtilInstance.isClockwise.bind(GeometryUtilInstance),
        typePolygon: GeometryUtilInstance.typePolygon.bind(GeometryUtilInstance),
        makesureClockwise: GeometryUtilInstance.makesureClockwise.bind(GeometryUtilInstance),
        makesureAntiClockwise: GeometryUtilInstance.makesureAntiClockwise.bind(GeometryUtilInstance),
        distanceOfLine: GeometryUtilInstance.distanceOfLine.bind(GeometryUtilInstance),
        ringRingClip: GeometryUtilInstance.ringRingClip.bind(GeometryUtilInstance),
        doesSegmentsIntersect: GeometryUtilInstance.doesSegmentsIntersect.bind(GeometryUtilInstance),
        doesSegmentLineIntersect: GeometryUtilInstance.doesSegmentLineIntersect.bind(GeometryUtilInstance),
        doesSegmentRingIntersect: GeometryUtilInstance.doesSegmentRingIntersect.bind(GeometryUtilInstance),
        doesSegmentPolygonIntersect: GeometryUtilInstance.doesSegmentPolygonIntersect.bind(GeometryUtilInstance),
        doesLineLineIntersect: GeometryUtilInstance.doesLineLineIntersect.bind(GeometryUtilInstance),
        doesLineRingIntersect: GeometryUtilInstance.doesLineRingIntersect.bind(GeometryUtilInstance),
        doesRingRingIntersect: GeometryUtilInstance.doesRingRingIntersect.bind(GeometryUtilInstance),
        pointInRing: GeometryUtilInstance.pointInRing.bind(GeometryUtilInstance),
        isPointInRing: GeometryUtilInstance.isPointInRing.bind(GeometryUtilInstance),
        isPointInBbox: GeometryUtilInstance.isPointInBbox.bind(GeometryUtilInstance),
        isRingInRing: GeometryUtilInstance.isRingInRing.bind(GeometryUtilInstance),
        isPointInPolygon: GeometryUtilInstance.isPointInPolygon.bind(GeometryUtilInstance),
        isPointInPolygons: GeometryUtilInstance.isPointInPolygons.bind(GeometryUtilInstance),
        isPointOnSegment: GeometryUtilInstance.isPointOnSegment.bind(GeometryUtilInstance),
        isPointOnLine: GeometryUtilInstance.isPointOnLine.bind(GeometryUtilInstance),
        isPointOnRing: GeometryUtilInstance.isPointOnRing.bind(GeometryUtilInstance),
        isPointOnPolygon: GeometryUtilInstance.isPointOnPolygon.bind(GeometryUtilInstance),
        closestOnSegment: GeometryUtilInstance.closestOnSegment.bind(GeometryUtilInstance),
        closestOnLine: GeometryUtilInstance.closestOnLine.bind(GeometryUtilInstance),
        distanceToSegment: GeometryUtilInstance.distanceToSegment.bind(GeometryUtilInstance),
        distanceToLine: GeometryUtilInstance.distanceToLine.bind(GeometryUtilInstance)
    };
    var PlaneGeometryUtil = {
        distance: PlaneGeometryUtilInstance.distance.bind(PlaneGeometryUtilInstance),
        ringArea: PlaneGeometryUtilInstance.ringArea.bind(PlaneGeometryUtilInstance),
        isClockwise: PlaneGeometryUtilInstance.isClockwise.bind(PlaneGeometryUtilInstance),
        typePolygon: PlaneGeometryUtilInstance.typePolygon.bind(PlaneGeometryUtilInstance),
        makesureClockwise: PlaneGeometryUtilInstance.makesureClockwise.bind(PlaneGeometryUtilInstance),
        makesureAntiClockwise: PlaneGeometryUtilInstance.makesureAntiClockwise.bind(PlaneGeometryUtilInstance),
        distanceOfLine: PlaneGeometryUtilInstance.distanceOfLine.bind(PlaneGeometryUtilInstance),
        ringRingClip: PlaneGeometryUtilInstance.ringRingClip.bind(PlaneGeometryUtilInstance),
        doesSegmentsIntersect: PlaneGeometryUtilInstance.doesSegmentsIntersect.bind(PlaneGeometryUtilInstance),
        doesSegmentLineIntersect: PlaneGeometryUtilInstance.doesSegmentLineIntersect.bind(PlaneGeometryUtilInstance),
        doesSegmentRingIntersect: PlaneGeometryUtilInstance.doesSegmentRingIntersect.bind(PlaneGeometryUtilInstance),
        doesSegmentPolygonIntersect: PlaneGeometryUtilInstance.doesSegmentPolygonIntersect.bind(PlaneGeometryUtilInstance),
        doesLineLineIntersect: PlaneGeometryUtilInstance.doesLineLineIntersect.bind(PlaneGeometryUtilInstance),
        doesLineRingIntersect: PlaneGeometryUtilInstance.doesLineRingIntersect.bind(PlaneGeometryUtilInstance),
        doesRingRingIntersect: PlaneGeometryUtilInstance.doesRingRingIntersect.bind(PlaneGeometryUtilInstance),
        pointInRing: PlaneGeometryUtilInstance.pointInRing.bind(PlaneGeometryUtilInstance),
        isPointInRing: PlaneGeometryUtilInstance.isPointInRing.bind(PlaneGeometryUtilInstance),
        isPointInBbox: PlaneGeometryUtilInstance.isPointInBbox.bind(PlaneGeometryUtilInstance),
        isRingInRing: PlaneGeometryUtilInstance.isRingInRing.bind(PlaneGeometryUtilInstance),
        isPointInPolygon: PlaneGeometryUtilInstance.isPointInPolygon.bind(PlaneGeometryUtilInstance),
        isPointInPolygons: PlaneGeometryUtilInstance.isPointInPolygons.bind(PlaneGeometryUtilInstance),
        isPointOnSegment: PlaneGeometryUtilInstance.isPointOnSegment.bind(PlaneGeometryUtilInstance),
        isPointOnLine: PlaneGeometryUtilInstance.isPointOnLine.bind(PlaneGeometryUtilInstance),
        isPointOnRing: PlaneGeometryUtilInstance.isPointOnRing.bind(PlaneGeometryUtilInstance),
        isPointOnPolygon: PlaneGeometryUtilInstance.isPointOnPolygon.bind(PlaneGeometryUtilInstance),
        closestOnSegment: PlaneGeometryUtilInstance.closestOnSegment.bind(PlaneGeometryUtilInstance),
        closestOnLine: PlaneGeometryUtilInstance.closestOnLine.bind(PlaneGeometryUtilInstance),
        distanceToSegment: PlaneGeometryUtilInstance.distanceToSegment.bind(PlaneGeometryUtilInstance),
        distanceToLine: PlaneGeometryUtilInstance.distanceToLine.bind(PlaneGeometryUtilInstance)
    };
    var LngLat$2 = function() {
        function o(e, r, t) {
            if (t === void 0) {
                t = false
            }
            this.className = "AMap.LngLat";
            if (isNumberArray(e)) {
                e = parseFloat(e[0]);
                r = parseFloat(e[1])
            } else {
                r = parseFloat(r);
                e = parseFloat(e)
            }
            if (isNaN(e) || isNaN(r)) {
                if (!window._AMapConfig) {
                    throw Error("Invalid Object: LngLat(" + e + ", " + r + ")")
                }
            }
            if (t !== true) {
                r = Math.max(Math.min(r, 90), -90);
                e = (e + 180) % 360 + (e < -180 || e === 180 ? 180 : -180)
            }
            this.kT = r;
            this.KL = e;
            this.lng = Math.round(e * 1e6) / 1e6;
            this.lat = Math.round(r * 1e6) / 1e6;
            this.pos = ProjectionManager.getProjection("EPSG:3857").project(e, r)
        }
        o.from = function(e) {
            if (isLngLat(e)) {
                return new o(e.KL,e.kT)
            }
            return new o(e[0],e[1])
        }
        ;
        o.prototype["setLng"] = function(e) {
            this.KL = e;
            this.lng = Math.round(e * 1e6) / 1e6;
            return this
        }
        ;
        o.prototype["setLat"] = function(e) {
            this.kT = e;
            this.lat = Math.round(e * 1e6) / 1e6;
            return this
        }
        ;
        o.prototype["getLng"] = function() {
            return this.lng
        }
        ;
        o.prototype["getLat"] = function() {
            return this.lat
        }
        ;
        o.prototype["equals"] = function(e) {
            e = parseLngLatData(e);
            if (!(e instanceof o)) {
                return false
            }
            var r = Math.max(Math.abs(this.kT - e.kT), Math.abs(this.KL - e.KL));
            return r <= 1e-9
        }
        ;
        o.prototype["add"] = function(e, r) {
            return new o(this.KL + e.KL,this.kT + e.kT,r)
        }
        ;
        o.prototype["subtract"] = function(e, r) {
            return new o(this.KL - e.KL,this.kT - e.kT,r)
        }
        ;
        o.prototype["divideBy"] = function(e, r) {
            return new o(this.KL / e,this.kT / e,r)
        }
        ;
        o.prototype["multiplyBy"] = function(e, r) {
            return new o(this.KL * e,this.kT * e,r)
        }
        ;
        o.prototype["offset"] = function(e, r) {
            if (isNaN(e) || isNaN(r)) {
                throw Error("valid offset args, require number")
            }
            var t = 2 * Math.asin(Math.sin(Math.round(e) / (2 * 6378137)) / Math.cos(this.kT * Math.PI / 180));
            var i = this.KL + t * 180 / Math.PI;
            var a = 2 * Math.asin(Math.round(r) / (2 * 6378137));
            var n = this.kT + a * 180 / Math.PI;
            return new o(i,n)
        }
        ;
        o.prototype["toString"] = function() {
            return this.lng + "," + this.lat
        }
        ;
        o.prototype["toArray"] = function() {
            return [this.lng, this.lat]
        }
        ;
        o.prototype["toJSON"] = function() {
            return [this.lng, this.lat]
        }
        ;
        o.prototype["distanceTo"] = function(e) {
            return GeometryUtil["distance"](this, e)
        }
        ;
        o.prototype["distance"] = function(e) {
            return GeometryUtil["distance"](this, e)
        }
        ;
        return o
    }();
    var Size = function() {
        function e(e, r, t) {
            if (t === void 0) {
                t = false
            }
            this.className = "AMap.Size";
            if (isNaN(e) || isNaN(r)) {
                throw new Error("Invalid Object: Pixel(" + e + ", " + r + ")")
            }
            this["width"] = t ? Math.round(e) : Number(e);
            this["height"] = t ? Math.round(r) : Number(r)
        }
        e.prototype["getWidth"] = function() {
            return this.width
        }
        ;
        e.prototype["getHeight"] = function() {
            return this.height
        }
        ;
        e.prototype["toString"] = function() {
            return this.width + "," + this.height
        }
        ;
        e.prototype["toArray"] = function() {
            return [this.width, this.height]
        }
        ;
        return e
    }();
    var easing = function() {
        var o = {};
        var f = 4;
        var l = .001;
        var s = 1e-7;
        var u = 10;
        var v = 11;
        var c = 1 / (v - 1);
        var h = typeof Float32Array === "function";
        function i(e, r) {
            return 1 - 3 * r + 3 * e
        }
        function a(e, r) {
            return 3 * r - 6 * e
        }
        function n(e) {
            return 3 * e
        }
        function d(e, r, t) {
            return ((i(r, t) * e + a(r, t)) * e + n(r)) * e
        }
        function _(e, r, t) {
            return 3 * i(r, t) * e * e + 2 * a(r, t) * e + n(r)
        }
        function g(e, r, t, i, a) {
            var n, o, f = 0;
            do {
                o = r + (t - r) / 2;
                n = d(o, i, a) - e;
                if (n > 0) {
                    t = o
                } else {
                    r = o
                }
            } while (Math.abs(n) > s && ++f < u);
            return o
        }
        function y(e, r, t, i) {
            for (var a = 0; a < f; ++a) {
                var n = _(r, t, i);
                if (n === 0) {
                    return r
                }
                var o = d(r, t, i) - e;
                r -= o / n
            }
            return r
        }
        function m(e) {
            return e
        }
        return function(f, r, s, t) {
            if (!(0 <= f && f <= 1 && 0 <= s && s <= 1)) {
                throw new Error("bezier x values must be in [0, 1] range")
            }
            var e = arguments.toString();
            if (o[e]) {
                return o[e]
            }
            if (f === r && s === t) {
                return m
            }
            var u = h ? new Float32Array(v) : new Array(v);
            for (var i = 0; i < v; ++i) {
                u[i] = d(i * c, f, s)
            }
            function a(e) {
                var r = 0;
                var t = 1;
                var i = v - 1;
                for (; t !== i && u[t] <= e; ++t) {
                    r += c
                }
                --t;
                var a = (e - u[t]) / (u[t + 1] - u[t]);
                var n = r + a * c;
                var o = _(n, f, s);
                if (o >= l) {
                    return y(e, n, f, s)
                } else if (o === 0) {
                    return n
                } else {
                    return g(e, r, r + c, f, s)
                }
            }
            var n = function(e) {
                if (e === 0) {
                    return 0
                }
                if (e === 1) {
                    return 1
                }
                return d(a(e), r, t)
            };
            o[e] = n;
            return n
        }
    }();
    function getQuadBezierValue(e, r, t, i) {
        if (e >= 1) {
            return i
        }
        var a = 1 - e;
        return a * a * r + 2 * a * e * t + e * e * i
    }
    function cubic(e, r, t, i, a) {
        if (e >= 1) {
            return a
        }
        var n = 3 * (t.pos[0] - r.pos[0])
          , o = 3 * (i.pos[0] - t.pos[0]) - n
          , f = a.pos[0] - r.pos[0] - n - o;
        var s = 3 * (t.pos[1] - r.pos[1])
          , u = 3 * (i.pos[1] - t.pos[1]) - s
          , l = a.pos[1] - r.pos[1] - s - u;
        var v = f * Math.pow(e, 3) + o * Math.pow(e, 2) + n * e + r.pos[0];
        var c = l * Math.pow(e, 3) + u * Math.pow(e, 2) + s * e + r.pos[1];
        return ProjectionManager.getProjection("EPSG:3857").unproject(v, c)
    }
    function quad(e, r, t, i) {
        return ProjectionManager.getProjection("EPSG:3857").unproject(getQuadBezierValue(e, r.pos[0], t.pos[0], i.pos[0]), getQuadBezierValue(e, r.pos[1], t.pos[1], i.pos[1]))
    }
    function p20CoordsToLngLat(e, r) {
        if (Array.isArray(r)) {
            r = new Pixel(r[0],r[1])
        }
        return e.ai(r, 20)
    }
    function getSplitNum(e, r) {
        var t = {
            tolerance: 4,
            interpolateNumLimit: [3, 300]
        };
        var i = t["tolerance"]
          , a = t["interpolateNumLimit"];
        i = Math.max(2, i);
        var n = 0
          , o = 0;
        for (var f = 0, s = e.length; f < s - 1; f++) {
            var u = e[f]
              , l = e[f + 1];
            n += Math.abs(l.pos[0] - u.pos[0]);
            o += Math.abs(l.pos[1] - u.pos[1])
        }
        return Math.min(a[1], Math.max(a[0], Math.round(Math.max(n, o) / r / i)))
    }
    function interpolateCoords(e, r) {
        var t = null;
        switch (r.length) {
        case 3:
            t = quad;
            break;
        case 4:
            t = cubic;
            break;
        default:
            return null
        }
        var i = [];
        var a = [0].concat(r);
        for (var n = 1; n < e - 2; n++) {
            a[0] = n / e;
            i.push(t.apply(null, a))
        }
        i.push(r[r.length - 1]);
        return i
    }
    function getCoordsWithControlPoints(e, r, t, i) {
        var a = null;
        if (e && t && t.length) {
            var n = [e];
            n.push.apply(n, t);
            n.push(r);
            var o = n.length;
            var f = getSplitNum(n, i);
            a = interpolateCoords(f, n)
        }
        return a || [r]
    }
    var BezierUtil = {
        ag: quad,
        ug: cubic,
        sg: easing,
        xb: function(e, r) {
            var t, i, a = [];
            for (t = 0,
            i = e.length; t < i; t += 1) {
                a.push.apply(a, getCoordsWithControlPoints(e[t - 1], e[t], e[t]["controlPoints"], r))
            }
            return a
        },
        fg: function(e, r, t, i) {
            var a = this.xb(e, r, t, i);
            var n = [];
            for (var o = 0, f = a.length; o < f; o++) {
                n.push(p20CoordsToLngLat(r, a[o]))
            }
            return n
        }
    };
    var TagMap = {
        2: "all",
        3: "all",
        4: "all",
        5: "all",
        6: "lite",
        7: "all",
        8: "lite",
        9: "all",
        10: "lite",
        11: "lite",
        12: "all",
        13: "all",
        14: "all",
        15: "lite",
        16: "lite",
        17: "all",
        18: "all",
        19: "all",
        20: "all"
    };
    var colorNameDist = {
        aliceblue: "#f0f8ff",
        antiquewhite: "#faebd7",
        aqua: "#00ffff",
        aquamarine: "#7fffd4",
        azure: "#f0ffff",
        beige: "#f5f5dc",
        bisque: "#ffe4c4",
        black: "#000000",
        blanchedalmond: "#ffebcd",
        blue: "#0000ff",
        blueviolet: "#8a2be2",
        brown: "#a52a2a",
        burlywood: "#deb887",
        cadetblue: "#5f9ea0",
        chartreuse: "#7fff00",
        chocolate: "#d2691e",
        coral: "#ff7f50",
        cornflowerblue: "#6495ed",
        cornsilk: "#fff8dc",
        crimson: "#dc143c",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgoldenrod: "#b8860b",
        darkgray: "#a9a9a9",
        darkgreen: "#006400",
        darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        darkorchid: "#9932cc",
        darkred: "#8b0000",
        darksalmon: "#e9967a",
        darkseagreen: "#8fbc8f",
        darkslateblue: "#483d8b",
        darkslategray: "#2f4f4f",
        darkturquoise: "#00ced1",
        darkviolet: "#9400d3",
        deeppink: "#ff1493",
        deepskyblue: "#00bfff",
        dimgray: "#696969",
        dodgerblue: "#1e90ff",
        firebrick: "#b22222",
        floralwhite: "#fffaf0",
        forestgreen: "#228b22",
        fuchsia: "#ff00ff",
        gainsboro: "#dcdcdc",
        ghostwhite: "#f8f8ff",
        gold: "#ffd700",
        goldenrod: "#daa520",
        grey: "#808080",
        gray: "#808080",
        green: "#008000",
        greenyellow: "#adff2f",
        honeydew: "#f0fff0",
        hotpink: "#ff69b4",
        indianred: "#cd5c5c",
        indigo: "#4b0082",
        ivory: "#fffff0",
        khaki: "#f0e68c",
        lavender: "#e6e6fa",
        lavenderblush: "#fff0f5",
        lawngreen: "#7cfc00",
        lemonchiffon: "#fffacd",
        lightblue: "#add8e6",
        lightcoral: "#f08080",
        lightcyan: "#e0ffff",
        lightgoldenrodyellow: "#fafad2",
        lightgrey: "#d3d3d3",
        lightgreen: "#90ee90",
        lightpink: "#ffb6c1",
        lightsalmon: "#ffa07a",
        lightseagreen: "#20b2aa",
        lightskyblue: "#87cefa",
        lightslategray: "#778899",
        lightsteelblue: "#b0c4de",
        lightyellow: "#ffffe0",
        lime: "#00ff00",
        limegreen: "#32cd32",
        linen: "#faf0e6",
        magenta: "#ff00ff",
        maroon: "#800000",
        mediumaquamarine: "#66cdaa",
        mediumblue: "#0000cd",
        mediumorchid: "#ba55d3",
        mediumpurple: "#9370d8",
        mediumseagreen: "#3cb371",
        mediumslateblue: "#7b68ee",
        mediumspringgreen: "#00fa9a",
        mediumturquoise: "#48d1cc",
        mediumvioletred: "#c71585",
        midnightblue: "#191970",
        mintcream: "#f5fffa",
        mistyrose: "#ffe4e1",
        moccasin: "#ffe4b5",
        navajowhite: "#ffdead",
        navy: "#000080",
        oldlace: "#fdf5e6",
        olive: "#808000",
        olivedrab: "#6b8e23",
        orange: "#ffa500",
        orangered: "#ff4500",
        orchid: "#da70d6",
        palegoldenrod: "#eee8aa",
        palegreen: "#98fb98",
        paleturquoise: "#afeeee",
        palevioletred: "#d87093",
        papayawhip: "#ffefd5",
        peachpuff: "#ffdab9",
        peru: "#cd853f",
        pink: "#ffc0cb",
        plum: "#dda0dd",
        powderblue: "#b0e0e6",
        purple: "#800080",
        rebeccapurple: "#663399",
        red: "#ff0000",
        rosybrown: "#bc8f8f",
        royalblue: "#4169e1",
        saddlebrown: "#8b4513",
        salmon: "#fa8072",
        sandybrown: "#f4a460",
        seagreen: "#2e8b57",
        seashell: "#fff5ee",
        sienna: "#a0522d",
        silver: "#c0c0c0",
        skyblue: "#87ceeb",
        slateblue: "#6a5acd",
        slategray: "#708090",
        snow: "#fffafa",
        springgreen: "#00ff7f",
        steelblue: "#4682b4",
        tan: "#d2b48c",
        teal: "#008080",
        thistle: "#d8bfd8",
        tomato: "#ff6347",
        turquoise: "#40e0d0",
        violet: "#ee82ee",
        wheat: "#f5deb3",
        white: "#ffffff",
        whitesmoke: "#f5f5f5",
        yellow: "#ffff00",
        yellowgreen: "#9acd32"
    };
    var Util$3 = function() {
        var s = {
            CLASS_NAME: "AMap.Util",
            WorldAxesCenter: {
                x: 0,
                y: 0
            },
            Single: {},
            stamp: function() {
                var r = Browser.DW === true;
                var t = 1e5;
                var i = "_amap_id";
                return function(e) {
                    e[i] = r ? ++t : --t;
                    return e[i]
                }
            }(),
            singlton: function(e) {
                var r = c.Single;
                var t = r[e];
                if (!t) {
                    t = new e
                }
                return t
            },
            getOptimalZoom: function(e) {
                return e < Math.floor(e) + .8 ? Math.floor(e) : Math.ceil(e)
            },
            join: function(e, r) {
                if (e.join) {
                    return e.join(r)
                } else {
                    var t = [];
                    for (var i in e) {
                        if (e.hasOwnProperty(i)) {
                            t.push(i + "=" + (e[i] || ""))
                        }
                    }
                    return t.join(r)
                }
            },
            getGuid: function(e, r) {
                if (r === void 0) {
                    r = 10
                }
                return (e || "") + Math.round(Math.random() * Math.pow(10, r)) + "_" + (new Date).getTime()
            },
            uuid: function() {
                var o = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
                return function(e, r) {
                    if (e === void 0) {
                        e = 0
                    }
                    if (r === void 0) {
                        r = 62
                    }
                    var t = o;
                    var i = [];
                    var a;
                    r = r || t.length;
                    if (e) {
                        for (a = 0; a < e; a++) {
                            i[a] = t[0 | Math.random() * r]
                        }
                    } else {
                        var n = void 0;
                        i[8] = i[13] = i[18] = i[23] = "-";
                        i[14] = "4";
                        for (a = 0; a < 36; a++) {
                            if (!i[a]) {
                                n = 0 | Math.random() * 16;
                                i[a] = t[a === 19 ? n & 3 | 8 : n]
                            }
                        }
                    }
                    return i.join("")
                }
            }(),
            endsWith: function(e, r) {
                if (e["endsWith"]) {
                    return e["endsWith"](r)
                }
                if (e.length < r.length) {
                    return false
                }
                if (e.substr(e.length - r.length) === r) {
                    return true
                }
                return false
            },
            mg: [],
            gg: 268435456,
            wg: [215440491, 106744817],
            Mg: {
                start: function(r) {
                    r.startTime = new Date;
                    r.jg = [];
                    var t = (new Date).getTime();
                    var i = function() {
                        var e = (new Date).getTime();
                        r.jg.push(e - t);
                        t = e;
                        r.id = c.requestAnimFrame(i)
                    };
                    r.id = c.requestAnimFrame(i)
                },
                cancel: function(e) {
                    if (e.id) {
                        c.cancelAnimFrame(e.id)
                    }
                },
                stop: function(e) {
                    e.Ag = Date.now() - e.startTime;
                    this.cancel(e);
                    e.Mg = Math.round(1e3 / (e.Ag / (e.jg.length + 1)))
                }
            },
            kg: function(e, r, t, i, a) {
                if (i === void 0) {
                    i = "linear"
                }
                if (a === void 0) {
                    a = false
                }
                if (e === r) {
                    return r
                } else {
                    switch (i) {
                    case "ease":
                        t = BezierUtil.sg(.25, .1, .25, 1)(t);
                        break;
                    case "ease-in":
                        t = BezierUtil.sg(.42, 0, 1, 1)(t);
                        break;
                    case "ease-out":
                        t = BezierUtil.sg(0, 0, .58, 1)(t);
                        break;
                    case "ease-in-out":
                        t = BezierUtil.sg(.42, 0, .58, 1)(t);
                        break
                    }
                    var n = e + (r - e) * t;
                    if (a) {
                        n = n >> 0
                    }
                    return n
                }
            },
            createObjectURL: function(e, r) {
                if (r === void 0) {
                    r = "text/javascript; charset=utf-8"
                }
                var t = null;
                try {
                    var i = globalInstance["URL"] || globalInstance["webkitURL"];
                    t = i["createObjectURL"](new Blob([e],{
                        type: r
                    }))
                } catch (e) {
                    t = null
                }
                return t
            },
            revokeObjectURL: function(e) {
                try {
                    var r = globalInstance["URL"] || globalInstance["webkitURL"];
                    r["revokeObjectURL"](e)
                } catch (e) {}
            },
            Og: function(e) {
                var r = {};
                for (var t = 0, i = e.length; t < i; t++) {
                    r[e[t]] = t
                }
                return r
            },
            EV: function(e, r) {
                var t = Math.pow(2, r);
                var i = 360 / t;
                var a = 180 / t;
                var n = Math.floor(e[0] / i) + t / 2;
                var o = t / 2 - Math.ceil(e[1] / a);
                return [n, o]
            },
            WY: function(e, r, t) {
                if (t === void 0) {
                    t = false
                }
                var i = [e[0], e[3]];
                var a = [e[2], e[1]];
                var n = this.EV(i, r);
                var o = this.EV(a, r);
                var f = n[0];
                var s = n[1];
                var u = o[0];
                var l = o[1];
                if (t) {
                    return {
                        ja: f,
                        Fa: s,
                        Oa: u,
                        Ea: l,
                        z: r
                    }
                }
                this.Pa = {
                    ja: f,
                    Fa: s,
                    Oa: u,
                    Ea: l,
                    z: r
                };
                var v = [];
                for (var c = f; c <= u; c += 1) {
                    for (var h = s; h <= l; h += 1) {
                        v.push([r, c, h])
                    }
                }
                return v
            },
            xg: function(e) {
                var r = {};
                if (c.is(e, "object")) {
                    for (var t in e) {
                        if (e.hasOwnProperty(t)) {
                            r[e[t]] = t
                        }
                    }
                }
                return r
            },
            Ig: function(e, r) {
                if (r.length < 5e4) {
                    e.push.apply(e, r);
                    return
                }
                for (var t = 0, i = r.length; t < i; t += 1) {
                    e.push(r[t])
                }
            },
            clone: function(e) {
                if (typeof e === "object" && e !== null) {
                    if (e.Tg || this.is(e, "Float32Array") || this.is(e, "Uint16Array")) {
                        return e
                    } else {
                        var r = this.isArray(e) ? [] : {};
                        for (var t in e) {
                            if (e.hasOwnProperty(t)) {
                                r[t] = c.clone(e[t])
                            }
                        }
                        return r
                    }
                } else {
                    return e
                }
            },
            isInteger: function(e) {
                return (e | 0) === e
            },
            vB: function(e) {
                return !isNaN(e)
            },
            setPrototypeOf: function() {
                if (typeof Object["setPrototypeOf"] === "function") {
                    return Object["setPrototypeOf"]
                } else {
                    return function(e, r) {
                        for (var t in r) {
                            e[t] = r[t]
                        }
                    }
                }
            }(),
            isFunction: function(e) {
                return typeof e === "function"
            },
            _g: function(e, r) {
                if (r === void 0) {
                    r = "webgl"
                }
                if (!e) {
                    return e
                }
                var t = [];
                for (var i = 0, a = e.length; i < a; i += 2) {
                    var n = parseInt(e.substr(i, 2), 16);
                    if (r === "webgl" || r === "rgba" && i === 0) {
                        n = this.format(n / 255, 3)
                    }
                    t.push(n)
                }
                t.push(t.shift());
                return t
            },
            Sg: function() {},
            keys: function() {
                if (typeof Object.keys === "function") {
                    return Object.keys
                } else {
                    return function(e) {
                        var r = [];
                        for (var t in e) {
                            if (e.hasOwnProperty(t)) {
                                r.push(t)
                            }
                        }
                        return r
                    }
                }
            }(),
            map: function(i, a, n) {
                if (n === void 0) {
                    n = null
                }
                var o = [];
                if (i && i.length) {
                    c.Cg(i, function() {
                        var e = [];
                        for (var r = 0; r < arguments.length; r++) {
                            e[r] = arguments[r]
                        }
                        var t = e[1];
                        o[t] = a.apply(n || i, e)
                    })
                } else {
                    return i
                }
                return o
            },
            forEach: function(e, r) {
                if (e && e.length) {
                    var t = e.length;
                    if (t > 0) {
                        r(e[0], 0);
                        if (t > 1) {
                            r(e[1], 1);
                            for (var i = 2; i < t; i++) {
                                r(e[i], i)
                            }
                        }
                    }
                }
            },
            Cg: function(e, r, t) {
                if (t === void 0) {
                    t = null
                }
                if (e && e.length) {
                    for (var i = 0, a = e.length; i < a; i++) {
                        if (r.call(t, e[i], i, e) === false) {
                            break
                        }
                    }
                }
            },
            find: function(e, r, t) {
                if (t === void 0) {
                    t = null
                }
                for (var i = 0, a = e.length; i < a; i++) {
                    if (typeof r === "function") {
                        if (r.call(t, e[i], i, e)) {
                            return e[i]
                        }
                    } else {
                        if (e[i] === r) {
                            return e[i]
                        }
                    }
                }
                return null
            },
            isDOM: function(e) {
                if (typeof HTMLElement === "object") {
                    return e instanceof HTMLElement
                } else {
                    return e && typeof e === "object" && e.nodeType === 1 && typeof e.nodeName === "string"
                }
            },
            Eg: function(e, r) {
                var t = "ASDFGHJKLQWERTYUIO!sdfghjkleiu3~yr5-P&mq9`%zCN*b=8@^xpVM";
                r = r || "v5";
                var i, a;
                if (r > "v5") {
                    i = t.length;
                    a = 512
                } else {
                    i = 27;
                    t = t.substr(0, 27);
                    a = 333
                }
                var n, o, f, s, u;
                o = [];
                f = NaN;
                for (s = 0,
                u = e.length; s < u; s++) {
                    n = e[s];
                    n = t.indexOf(n);
                    if (isNaN(f)) {
                        f = n * i
                    } else {
                        o.push(f + n - a);
                        f = NaN
                    }
                }
                return o
            },
            Ng: function(e, r) {
                var t = 1;
                if (r.length > 512) {
                    t = Math.round(Math.pow(r.length, .5))
                } else {
                    t = r.length
                }
                var i = Math.ceil(r.length / t);
                for (var a = 0; a < i; a += 1) {
                    var n = t * a;
                    var o = n + t;
                    if (o > r.length) {
                        o = r.length
                    }
                    for (var f = n; f < o; f += 1) {
                        e(r[f])
                    }
                }
            },
            Lg: function(e) {
                if (/^rgba\(/.test(e)) {
                    return this.Dg(e)
                } else {
                    var r = this.colorNameToHex(e);
                    var t = r;
                    if (r[0] === "#") {
                        r = r.substring(1);
                        if (r.length === 3) {
                            r = r.replace(/./g, function(e) {
                                return e + e
                            })
                        }
                        t = this.argbHex2Rgba(r.length === 8 ? r : "ff" + r)
                    }
                    return this.Dg(t)
                }
            },
            colorNameToHex: function(e) {
                if (e === void 0) {
                    e = ""
                }
                e = e.toLowerCase();
                if (typeof e === "string") {
                    if (colorNameDist[e]) {
                        return colorNameDist[e]
                    } else {
                        return e
                    }
                } else {
                    return e
                }
            },
            Rg: function(e, r, t) {
                var i, a, n, o, f, s;
                o = Math.floor(t / 2);
                n = t - o;
                i = (1 << o) - 1 << n;
                a = (1 << n) - 1;
                s = r & i | e & a;
                f = e & i | r & a;
                return [t, f, s]
            },
            Pg: function(e) {
                if (e) {
                    return encodeURIComponent(e)
                }
                return ""
            },
            getStyle: function(e, r, t, i) {
                var a, n, o;
                o = e[r]["i"][t];
                if (typeof o === "undefined") {
                    return null
                }
                n = e[r]["s"];
                if (typeof o === "number") {
                    return n[o]
                }
                while (typeof o[i.toString()] === "undefined") {
                    i -= 1;
                    if (i < 3) {
                        break
                    }
                }
                a = o[i.toString()];
                if (typeof a === "number") {
                    return n[a]
                } else {
                    return null
                }
            },
            Dg: function(e) {
                var r = e.split(",");
                r[0] = parseFloat(r[0].split("rgba(")[1]) / 255;
                r[1] = parseFloat(r[1]) / 255;
                r[2] = parseFloat(r[2]) / 255;
                r[3] = parseFloat(r[3]);
                return r
            },
            Ug: function(e) {
                var r = e.split(",");
                r[0] = parseFloat(r[0].split("rgb(")[1]) / 255;
                r[1] = parseFloat(r[1]) / 255;
                r[2] = parseFloat(r[2]) / 255;
                return r
            },
            Bg: function(e) {
                return "rgba(" + e[0] * 255 + "," + e[1] * 255 + "," + e[2] * 255 + "," + e[3] + ")"
            },
            color2Rgba: function(e) {
                return this.Bg(this.color2RgbaArray(e))
            },
            color2RgbaArray: function(e) {
                if (e instanceof Array) {
                    if (e.length == 3) {
                        e.push(1)
                    }
                    return e
                }
                var r = this.colorNameToHex(e);
                if (r.indexOf("rgb(") == 0) {
                    var t = this.Ug(r);
                    t.push(1);
                    return t
                } else if (r.indexOf("rgba(") == 0) {
                    return this.Dg(r)
                } else if (r.indexOf("#") == 0) {
                    if (r.length === 4) {
                        var i = r.substr(1);
                        var a = i.replace(/./g, function(e) {
                            return e + e
                        });
                        return this.zg(a)
                    } else if (r.length == 7) {
                        return this.zg(r.substr(1))
                    } else if (r.length == 9) {
                        var n = r.substr(1);
                        return this.qg(n)
                    }
                } else if (r.indexOf("hsla") === 0) {
                    var o = e.substr(5).split(",");
                    var f = parseInt(o[0], 10) / 360;
                    var s = parseInt(o[1], 10) / 100;
                    var u = parseInt(o[2], 10) / 100;
                    var l = parseFloat(o[3]);
                    return this.GQ(f, s, u, l)
                } else if (r.indexOf("hsl") === 0) {
                    var o = e.substr(4).split(",");
                    var f = parseInt(o[0], 10) / 360;
                    var s = parseInt(o[1], 10) / 100;
                    var u = parseInt(o[2], 10) / 100;
                    return this.GQ(f, s, u, 1)
                } else {
                    if (r.length === 3) {
                        var a = r.replace(/./g, function(e) {
                            return e + e
                        });
                        return this.zg(a)
                    } else if (r.length == 6) {
                        return this.zg(r)
                    } else if (r.length == 8) {
                        return this.qg(r)
                    }
                }
            },
            GQ: function(e, r, t, i) {
                var a;
                var n;
                var o;
                if (r === 0) {
                    a = n = o = t
                } else {
                    var f = t < .5 ? t * (1 + r) : t + r - t * r;
                    var s = 2 * t - f;
                    a = this.uQ(s, f, e + 1 / 3);
                    n = this.uQ(s, f, e);
                    o = this.uQ(s, f, e - 1 / 3)
                }
                return [a, n, o, i]
            },
            uQ: function(e, r, t) {
                if (t < 0) {
                    t += 1
                }
                if (t > 1) {
                    t -= 1
                }
                if (t < 1 / 6) {
                    return e + (r - e) * 6 * t
                }
                if (t < 1 / 2) {
                    return r
                }
                if (t < 2 / 3) {
                    return e + (r - e) * (2 / 3 - t) * 6
                }
                return e
            },
            rgbHex2Rgba: function(e) {
                if (e.startsWith("#")) {
                    e = e.slice(1)
                }
                return c.argbHex2Rgba("ff" + e)
            },
            argbHex2Rgba: function(e) {
                if (e.startsWith("#")) {
                    e = e.slice(1)
                }
                var r = [];
                for (var t = 0, i = e.length; t < i; t += 2) {
                    r.push(parseInt(e.substr(t, 2), 16))
                }
                r.push((r.shift() / 255).toFixed(2));
                return "rgba(" + r.join(",") + ")"
            },
            Opacity2Rgba: function(e, r) {
                var t = r;
                if (r && r[3] && e) {
                    r[3] = Math.floor(255 * e)
                }
                return t
            },
            Hex2Rgba: function(e) {
                if (typeof e !== "string") {
                    return false
                }
                var r = [];
                for (var t = 0, i = e.length; t < i; t += 2) {
                    r.push(parseInt(e.substr(t, 2), 16))
                }
                r.push(parseInt(r.shift()));
                return r
            },
            zg: function(e) {
                return this.uE(e + "ff")
            },
            qg: function(e) {
                var r = [];
                for (var t = 0, i = e.length; t < i; t += 2) {
                    r.push(parseInt(e.substr(t, 2), 16) / 255)
                }
                r.push(r.shift());
                return r
            },
            uE: function(e) {
                var r = parseInt(e, 16);
                var t = [(r >> 24 & 255) / 255, (r >> 16 & 255) / 255, (r >> 8 & 255) / 255, (r & 255) / 255];
                return t
            },
            isEmpty: function(e) {
                for (var r in e) {
                    if (e.hasOwnProperty(r)) {
                        return false
                    }
                }
                return true
            },
            Wg: function(e, r) {
                if (r >= 0) {
                    e.splice(r, 1)
                }
                return e
            },
            startsWith: function(e, r) {
                if (e["startsWith"]) {
                    return e["startsWith"](r)
                } else {
                    return e.substr(0, r.length) === r
                }
            },
            deleteItemFromArray: function(e, r) {
                var t = c.indexOf(e, r);
                return c.Wg(e, t)
            },
            deleteItemFromArrayByIndex: function(e, r) {
                return c.Wg(e, r)
            },
            filter: function(e, t, i) {
                var a = [];
                c.Cg(e, function(e, r) {
                    if (t.call(i, e, r)) {
                        a.push(e)
                    }
                });
                return a
            },
            indexOf: function(e, r) {
                if (!e || !e.length) {
                    return -1
                }
                if (e["indexOf"]) {
                    return e["indexOf"](r)
                } else {
                    for (var t = 0; t < e.length; t += 1) {
                        if (e[t] === r) {
                            return t
                        }
                    }
                    return -1
                }
            },
            Gg: function(e, r) {
                if (e["endsWith"]) {
                    return e["endsWith"](r)
                }
                if (e.length < r.length) {
                    return false
                }
                if (e.substr(e.length - r.length) == r) {
                    return true
                }
                return false
            },
            bind: function() {
                var i = false;
                if (Boolean(Function.prototype.bind)) {
                    i = true
                }
                return function(e, r) {
                    var t = arguments.length > 2 ? Array.prototype.slice.call(arguments, 2) : null;
                    if (i) {
                        if (t) {
                            t.unshift(r);
                            return e.bind.apply(e, t)
                        } else {
                            return e.bind(r)
                        }
                    } else {
                        return function() {
                            return e.apply(r, t || arguments)
                        }
                    }
                }
            }(),
            setOptions: function(e, r) {
                r = r || {};
                e.opts = assign({}, e.opts, r);
                return e.opts
            },
            yn: function(e, r, t) {
                return typeof r == "function" ? this.Hg(e, true, this.Vg(r, t, 1)) : this.Hg(e, true)
            },
            Hg: function(t, i, a, e, r, n, o) {
                var f;
                if (a) {
                    f = r ? a(t, e, r) : a(t)
                }
                if (f !== undefined) {
                    return f
                }
                if (!this.$g(t)) {
                    return t
                }
                var s = this.isArray(t);
                if (s) {
                    f = this.Yg(t);
                    if (!i) {
                        return this.Kg(t, f)
                    }
                } else {
                    var u = Object.prototype.toString.call(t)
                      , l = u == "[object Function]";
                    if (u == "[object Object]" || u == "[object Arguments]" || l && !r) {
                        f = this.Xg(l ? {} : t);
                        if (!i) {
                            return this.baseAssign(f, t)
                        }
                    } else {
                        return r ? t : {}
                    }
                }
                n || (n = []);
                o || (o = []);
                var v = n.length;
                while (v--) {
                    if (n[v] == t) {
                        return o[v]
                    }
                }
                n.push(t);
                o.push(f);
                (s ? this.Zg : this.Qg)(t, function(e, r) {
                    f[r] = c.Hg(e, i, a, r, t, n, o)
                });
                return f
            },
            baseAssign: function(e, r) {
                return r == null ? e : this.tM(r, Object.keys(r), e)
            },
            $g: function(e) {
                var r = typeof e;
                return !!e && (r == "object" || r == "function")
            },
            rM: function(e) {
                return !!e && typeof e == "object"
            },
            nM: function(e) {
                return typeof e == "number" && e > -1 && e % 1 == 0 && e <= 9007199254740991
            },
            Yg: function(e) {
                var r = e.length
                  , t = new Array(r);
                if (r && typeof e[0] == "string" && Object.hasOwnProperty.call(e, "index")) {
                    t.index = e.index;
                    t.input = e.input
                }
                return t
            },
            Kg: function(e, r) {
                var t = -1
                  , i = e.length;
                r || (r = Array(i));
                while (++t < i) {
                    r[t] = e[t]
                }
                return r
            },
            Xg: function(e) {
                var r = e.constructor;
                if (!(typeof r == "function" && r instanceof r)) {
                    r = Object
                }
                return new r
            },
            Vg: function(n, o, e) {
                if (typeof n != "function") {
                    return this.identity
                }
                if (o === undefined) {
                    return n
                }
                switch (e) {
                case 1:
                    return function(e) {
                        return n.call(o, e)
                    }
                    ;
                case 3:
                    return function(e, r, t) {
                        return n.call(o, e, r, t)
                    }
                    ;
                case 4:
                    return function(e, r, t, i) {
                        return n.call(o, e, r, t, i)
                    }
                    ;
                case 5:
                    return function(e, r, t, i, a) {
                        return n.call(o, e, r, t, i, a)
                    }
                }
                return function() {
                    return n.apply(o, arguments)
                }
            },
            Zg: function(e, r) {
                var t = -1
                  , i = e.length;
                while (++t < i) {
                    if (r(e[t], t, e) === false) {
                        break
                    }
                }
                return e
            },
            identity: function(e) {
                return e
            },
            iM: function(s) {
                return function(e, r, t) {
                    var i = c.eM(e)
                      , a = t(e)
                      , n = a.length
                      , o = s ? n : -1;
                    while (s ? o-- : ++o < n) {
                        var f = a[o];
                        if (r(i[f], f, i) === false) {
                            break
                        }
                    }
                    return e
                }
            },
            Qg: function(e, r) {
                var t = c.iM();
                return t(e, r, Object.keys)
            },
            eM: function(e) {
                return c.$g(e) ? e : Object(e)
            },
            tM: function(e, r, t) {
                t || (t = {});
                var i = -1
                  , a = r.length;
                while (++i < a) {
                    var n = r[i];
                    t[n] = e[n]
                }
                return t
            },
            oM: function() {
                return false
            },
            aM: "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=",
            uM: function() {
                return Date.now ? function() {
                    return Date.now()
                }
                : function() {
                    return (new Date).getTime()
                }
            }(),
            sM: function(e, r, t, i) {
                var a;
                if (i) {
                    var n = 0, o, f = this.uM;
                    a = function() {
                        o = f();
                        if (o - n < r) {
                            return false
                        }
                        n = o;
                        e.apply(t, arguments)
                    }
                } else {
                    var s, u, l;
                    l = function() {
                        s = false;
                        if (u) {
                            a.apply(t, u);
                            u = false
                        }
                    }
                    ;
                    a = function() {
                        if (s) {
                            u = arguments
                        } else {
                            s = true;
                            e.apply(t, arguments);
                            setTimeout(l, r)
                        }
                    }
                }
                return a
            },
            format: function(e, r) {
                if (e === e << 0) {
                    return e
                }
                return +parseFloat(e + "").toFixed(r || 0)
            },
            isArray: function() {
                if (Array.isArray) {
                    return Array.isArray
                } else {
                    return function(e) {
                        return this.is(e, "array")
                    }
                }
            }(),
            is: function(e, r) {
                return Object.prototype.toString.call(e).split(" ")[1].slice(0, -1).toLowerCase() === r.toLowerCase()
            },
            includes: function() {
                if (typeof Array.prototype.includes === "function") {
                    return function(e, r) {
                        return e.includes(r)
                    }
                } else {
                    return function(e, r) {
                        return this.indexOf(e, r) !== -1
                    }
                }
            }(),
            fM: function(e) {
                var r = 0;
                if (e.length === 0) {
                    return r
                }
                var t;
                for (var i = 0, a = e.length; i < a; i += 1) {
                    t = e.charCodeAt(i);
                    r = (r << 5) - r + t;
                    r = r & r
                }
                return r
            },
            hM: function(e, r) {
                r = r ? Math.ceil(parseInt(r.substr(6)) / 24) : 1;
                var t = "";
                for (var i = 0, a = e.length; i < a; i++) {
                    t += String.fromCharCode((e.charCodeAt(i) - 256 - r + 65535) % 65535)
                }
                return t
            },
            cM: function(e, r) {
                var t = (e + "").slice(-2);
                var i = (r + "").slice(-2);
                e = e.slice(0, -2);
                r = r.slice(0, -2);
                var a = parseInt((i + t).slice(1));
                var n = Math.ceil(a / 250) % 2 ? 1 : -1;
                var o = a / 500 > 1 ? 1 : -1;
                var f = parseInt("1" + t) / 3e3;
                var s = parseInt("1" + i) / 3e3;
                e -= f * n;
                r -= s * o;
                return new LngLat$2(parseFloat(e).toFixed(5),parseFloat(r).toFixed(5))
            },
            vM: function(e) {
                if (typeof JSON !== "undefined" && JSON.stringify) {
                    return c.fM(JSON.stringify(e))
                }
                return null
            },
            lM: function(e, r) {
                var t = "_amap_hash";
                if (r || !e.hasOwnProperty(t)) {
                    var i = c.vM(e);
                    if (i) {
                        e[t] = i
                    }
                }
                return e[t]
            },
            parseLngLatData: function(e) {
                if (c.isArray(e)) {
                    if (c.isArray(e[0])) {
                        for (var r = 0; r < e.length; r += 1) {
                            e[r] = c.parseLngLatData(e[r])
                        }
                    } else {
                        var t = typeof e[0];
                        if (t === "string" || t === "number") {
                            return new LngLat$2(e[0],e[1])
                        } else {
                            return e
                        }
                    }
                }
                return e
            },
            dM: function(e) {
                var r = [];
                for (var t = 0, i = e.length; t < i; t += 1) {
                    r[t] = [e[t].x, e[t].y]
                }
                return r
            },
            bM: function(e) {
                if (c.isArray(e)) {
                    return new Size(e[0],e[1])
                }
                return e
            },
            hp: function(e, r) {
                var t = [0, 0];
                var i = c.getOptimalZoom(r.viewState.zoom);
                if (e && i >= LocalZoom) {
                    var a = lcs$2.getSize();
                    var n = r.viewState.centerCoord;
                    var o = lcs$2.getLocalByCoord([n[0], n[1]]);
                    var f = e.x - o.x;
                    var s = e.y - o.y;
                    if (f !== 0) {
                        f *= a[0];
                        t[0] = f
                    }
                    if (s !== 0) {
                        s *= a[1];
                        t[1] = s
                    }
                } else if (e) {
                    t[0] += e.center[0];
                    t[1] += e.center[1]
                }
                return t
            },
            dS: function(e, r) {
                return e >= r[0] && e <= r[1]
            },
            kD: function(e, r, t) {
                if (e === void 0) {
                    e = ""
                }
                if (t === void 0) {
                    t = false
                }
                var i = e.split(",")[0] || "";
                if (t) {
                    return "all"
                }
                if (i && r) {
                    return TagMap[r] || "all"
                }
                return "all"
            },
            VF: function(e, r, t) {
                if (t === void 0) {
                    t = false
                }
                if (t) {
                    return "all"
                }
                if (e && r) {
                    return TagMap[r] || "all"
                }
                return "all"
            },
            LD: function(e, r) {
                var t = false;
                if (!e || !r) {
                    return t
                }
                var i = this.iN(e);
                var a = this.iN(r);
                return i >= a
            },
            SD: function(e) {
                if (e === void 0) {
                    e = "all"
                }
                var r;
                switch (e) {
                case "lite":
                    {
                        r = 0;
                        break
                    }
                case "left":
                    {
                        r = 1;
                        break
                    }
                case "all":
                default:
                    {
                        r = 2
                    }
                }
                return r
            },
            pW: function(e) {
                if (e === void 0) {
                    e = 2
                }
                var r;
                switch (e) {
                case 0:
                    {
                        r = NebulaTagType.LITE;
                        break
                    }
                case 1:
                    {
                        r = NebulaTagType.LEFT;
                        break
                    }
                case 2:
                default:
                    {
                        r = NebulaTagType.ALL
                    }
                }
                return r
            },
            iN: function(e) {
                var r = 0;
                switch (e) {
                case "lite":
                    {
                        r = 0;
                        break
                    }
                case "left":
                    {
                        r = 1;
                        break
                    }
                case "all":
                    {
                        r = 2;
                        break
                    }
                }
                return r
            },
            loadBaxia: function() {
                return new Promise(function(e, r) {
                    if (!window) {
                        r({
                            code: "0",
                            I8t: "霸下加载失败"
                        })
                    }
                    if (window["__AMap_web_baxia__"]) {
                        e({
                            code: "2",
                            I8t: "霸下已加载，无需重复加载"
                        })
                    } else {
                        window["__AMap_web_baxia__"] = true;
                        var t = document.body || document.head;
                        var i = document.createElement("script");
                        i.type = "text/javascript";
                        i.src = "//g.alicdn.com/??AWSC/AWSC/awsc.js,sd/baxia-entry/baxiaCommon.js";
                        i.onerror = function(e) {
                            window["__AMap_web_baxia__"] = false;
                            r({
                                code: "0",
                                I8t: "霸下加载失败"
                            })
                        }
                        ;
                        i.onload = function() {
                            e({
                                code: "1",
                                I8t: "霸下加载成功"
                            })
                        }
                        ;
                        t.appendChild(i)
                    }
                }
                )
            },
            A8t: function() {
                var e = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEABAMAAACuXLVVAAAAGFBMVEUAAABOVWNOVWVNVWJOVWNNVV9OVWNOVWT0TI68AAAACHRSTlMAIwkPFQQbHt8aTjEAAAVJSURBVHja7MGBAAAAAICg/akXqQIAAAAAAAAAYHbqXrlNIArD8MvCipbD8tOCoyQtZCzSQpKx2sX2xC1IdtSyiSLffpyZ3AKoCE9zyvNW32q1Wq1Wq9VqtVqt/lPvKq7H8jDKxNUEUShZE3ElFj8OvrK5VkATobNnd60A1UnLXrKyvUaAqh4l206cnN2k4cDigqj/ud8MMB5V3rcsSxcoc2P6zz8qRF5qV7AkfesitpLWjUiLGBPIogFbJ5eYWrYu/xK0KkutloTF+KPITiVQPxr7sGcTGwLjEstCNtmurMigvPzai9hwyigHXwbmd4O6s36MdxwFysTLdid0MWqx+BXzc43kVif3nWRj4UukX+8NUH8wLEFvRQzg/p6+DZ5jynwC+m8tC9g6OT6m76A73hv6CW3QIpeBoGAJ9XG030UKyop8LylceDK3PwZm58eqAChf5Vy29Ie95N8MdP5C++NNYWKhkfPNIRwIJR8LEpR7+s0i6kp3BrrdnXKXiMDYvuIVlWhm93AH5OjPMlDfGNWdY4KUcPiYg2Vuai85BPHWyfNA/zkTKRKClCDqX5idaiT/UkApkhRAKO6T154IIlTF/AKRceKEestANRVBWvNxqm04MTtVgH8sglQn8NXqRmRAxf0oB2eXWH1vwLIpfBPE/zbwQwRJ+FyEv5idP+Ad9oYno5NwAuRc4MdwAjTz840qRQ5oVzn1vqkYAZVAbZmftiiR8VxWsBm6W5GWrkDtc7hnfltX+SLHrvIq4NbJuYBy10juWMC78e2hGie61Gt5GMX9BvAk/2ots3t7mI0FBOn2NfNa6my3iQA2iWUBWuQnZQu+yEvdVdyDHwNL7F7QPrzwKZzwJlB5E22kAFAJS7D8Yb/eldMGwiiO/9GNlk+rSytxS4uMZVqJxFa7IDtutcaOW+Tg8PoJSV5BotHvBfbM7MyePc8ib+CEeOG8ILJDV+65iOiBFVQSJAuwFEuRhrOrLPmXYEoPHHWaRkaBGxuJczAEY38jCd3TuGtGTW3i8Ad1KVF+BgznrOnl3bPVbRtQLsrNjW/IolZTajg/imj6MJdoC7/S8vEkr+zJ/v65iUY/X+jBzMTbfaXZlzs5LeafXALsGizl0r154cZf91VsVnC28z9h7qFMGE/gic7NjEzQSzk8ZQ2W1EZUXUALTkj3ls/x9tGHdDVvT4n25O4wi5wAOwCXHhjPvxMFIwnymSnGm+abP19nxSikczXgBracFhE4eW2i6ug6E3Xi3Uq6L50nsluDF1rBzERQV/F274TYvl8aRffcoDZy0OnWlz8n31+G/oNMFW4UOl813atF2k+N7UzW++pS/rkc0iKAX/Tii0jurAMYNXcSmJkeFdHMfESwoGtzgGW+1OOm1GTHHze1kcSbmHibKnpQ6noLh13hTJYvtKFVSdtghzONpelB+i4KTMyjyIsdqDbOx0csRQ+m/3c24MU7kQS3iaby5k3gJ93bxNDG1QSgLL/l0gCG+sP0M7I38QrW2vatB8VpFCIBkCXwSvdGr2ez4gmw4ssl6DJwg/IFdiv64Jgoz/ZVgHsrojRY6t1Wjnzg9RVgM01FEoy8S5zArsm+hCx9+mJKSd8yzW2ya6so4VlnbQGavrTt56ZIV0CZFplsceRMnxbfRVbOCiw1Probhffh0qc7UenMHGE3cUJI6Nvr4lTFDyFum1iKa1jKQS8VngLFNaTF3EQBbsGVjCS62QvX4/mX4tdcjR3LIdFcjxsnXFfOYDAYDAaDwWAwGAwGg9/twYEAAAAAgCB/60GuAAAAAAAAAOAnLYvrtEzzqQMAAAAASUVORK5CYII=";
                return e
            }
        };
        (function() {
            var i = 0;
            function e(e) {
                var r = +new Date
                  , t = Math.max(0, (Browser.HV ? 50 : 20) - (r - i));
                i = r + t;
                return globalInstance.setTimeout(e, 50)
            }
            var r = function(e) {
                globalInstance.clearTimeout(e)
            };
            if (Browser.DW) {
                var a = e;
                var t = r;
                s["requestAnimFrame"] = function(e, r, t, i) {
                    if (t) {
                        if (r) {
                            c.bind(e, r).call(r, i)
                        } else {
                            e()
                        }
                    } else {
                        return a(function() {
                            if (r) {
                                c.bind(e, r).call(r, i)
                            } else {
                                e()
                            }
                        })
                    }
                }
                ;
                s["cancelAnimFrame"] = function(e) {
                    if (e) {
                        t.call(globalInstance, e)
                    }
                }
                ;
                return
            }
            function n(e) {
                var r, t, i = ["webkit", "moz", "o", "ms"];
                for (r = 0; r < i.length && !t; r += 1) {
                    t = globalInstance[i[r] + e]
                }
                return t
            }
            var o = globalInstance.requestAnimationFrame || n("RequestAnimationFrame") || e;
            var f = globalInstance.cancelAnimationFrame || n("CancelAnimationFrame") || n("CancelRequestAnimationFrame") || r;
            s["requestAnimFrame"] = function(e, r, t, i) {
                if (t) {
                    if (r) {
                        c.bind(e, r).call(r, i)
                    } else {
                        e()
                    }
                } else {
                    return o(function() {
                        if (r) {
                            c.bind(e, r).call(r, i)
                        } else {
                            e()
                        }
                    })
                }
            }
            ;
            s["cancelAnimFrame"] = function(e) {
                if (e) {
                    f.call(globalInstance, e)
                }
            }
        }
        )();
        (function() {
            if (Browser.DW) {
                return
            }
            s["requestIdleCallback"] = globalInstance["requestIdleCallback"] ? function(e, r) {
                return globalInstance["requestIdleCallback"](e, r)
            }
            : function(e, r) {
                if (r === void 0) {
                    r = {}
                }
                var t = c.uM();
                return setTimeout(function() {
                    e({
                        didTimeout: false,
                        timeRemaining: function() {
                            return Math.max(0, 70 - (c.uM() - t))
                        }
                    })
                }, r.timeout || 0)
            }
            ;
            s["cancelIdleCallback"] = globalInstance["cancelIdleCallback"] ? function(e) {
                return globalInstance["cancelIdleCallback"](e)
            }
            : function(e) {
                clearTimeout(e)
            }
        }
        )();
        var c = s;
        return c
    }();
    var LabelFormat = function() {
        function e() {
            this.Fs = {};
            this.Es = {};
            this.Ts = []
        }
        e.prototype.Ns = function(e, r) {
            if (r.lang) {
                this.lang = r.lang
            }
        }
        ;
        e.prototype.rV = function(e, r) {
            return e
        }
        ;
        e.prototype.Ws = function(e, r) {
            if (e === void 0) {
                e = ""
            }
            if (r === void 0) {
                r = []
            }
            var t = this.Es;
            var i = labelsUtil$2.EQ(e, r);
            var a = labelsUtil$2.aQ(i);
            for (var n = 0; n < a.length; n++) {
                t[a[n]] = 1
            }
        }
        ;
        e.id = "labelFormat";
        return e
    }();
    var SmartTypedArray$2 = function() {
        function e(e, r) {
            if (r === void 0) {
                r = 1024
            }
            this.type = e;
            this.length = 0;
            this.uf = 0;
            this.value = this.cf(r)
        }
        e.prototype.set = function(e) {
            if (!this.check(e)) {
                assert("SmartTypedArray: error value");
                return
            }
            if (e.length < this.uf) {
                this.value.set(e);
                return
            }
            this.cf(e.length);
            this.value.set(e);
            this.length = e.length
        }
        ;
        e.prototype.check = function(e) {
            if (Array.isArray(e)) {
                return true
            }
            var r = false;
            switch (this.type) {
            case "uint8":
                r = e instanceof Uint8Array;
                break;
            case "int8":
                r = e instanceof Int8Array;
                break;
            case "uint16":
                r = e instanceof Uint16Array;
                break;
            case "int16":
                r = e instanceof Int16Array;
                break;
            case "uint32":
                r = e instanceof Uint32Array;
                break;
            case "int32":
                r = e instanceof Int32Array;
                break;
            case "float32":
                r = e instanceof Float32Array;
                break;
            case "float64":
                r = e instanceof Float64Array;
                break;
            default:
                r = false
            }
            return r
        }
        ;
        e.prototype.subarray = function(e, r) {
            return this.value.subarray(e, r)
        }
        ;
        e.prototype.ff = function(e) {
            if (this.uf < e) {
                this.cf(e)
            }
            this.length = e
        }
        ;
        e.prototype.cf = function(e) {
            var r = this.pf();
            var t = this.value;
            this.value = new r(e);
            if (t) {
                this.value.set(t)
            }
            this.uf = e;
            return this.value
        }
        ;
        e.prototype.pf = function() {
            var e = Float32Array;
            switch (this.type) {
            case "uint8":
                e = Uint8Array;
                break;
            case "int8":
                e = Int8Array;
                break;
            case "uint16":
                e = Uint16Array;
                break;
            case "int16":
                e = Int16Array;
                break;
            case "uint32":
                e = Uint32Array;
                break;
            case "int32":
                e = Int32Array;
                break;
            case "float32":
                e = Float32Array;
                break;
            case "float64":
                e = Float64Array;
                break;
            default:
                e = Float32Array
            }
            return e
        }
        ;
        e = __decorate([InnerClass("SmartTypedArray")], e);
        return e
    }();
    var EnumSDFProperty;
    (function(e) {
        e[e["fontWidth"] = 0] = "fontWidth";
        e[e["fontHeight"] = 1] = "fontHeight";
        e[e["horiBearingX"] = 2] = "horiBearingX";
        e[e["horiBearingY"] = 3] = "horiBearingY";
        e[e["horiAdvance"] = 4] = "horiAdvance";
        e[e["posX"] = 5] = "posX";
        e[e["posY"] = 6] = "posY";
        e[e["iconWidth"] = 7] = "iconWidth";
        e[e["iconHeight"] = 8] = "iconHeight"
    }
    )(EnumSDFProperty || (EnumSDFProperty = {}));
    var INF = 1e20;
    var ControlChars = [[0, 31], [127, 159]];
    var ZeroWidthChars = [[8203, 8207]];
    var f = new SmartTypedArray$2("float64",64 * 64);
    var d = new SmartTypedArray$2("float64",64 * 64);
    var z = new SmartTypedArray$2("float64",64 * 64);
    var v = new SmartTypedArray$2("float64",64 * 64);
    var ua = navigator.userAgent;
    var TinySDF = function() {
        function e(e, r, t, i, a, n, o) {
            this.fontSize = e || 24;
            this.buffer = t === undefined ? 3 : t;
            this.MU = a || .25;
            this.fontFamily = n || "sans-serif";
            this.fontWeight = o || "lighter";
            this.radius = i || 8;
            this.size = r;
            this.xU();
            var f = r[0] * r[1];
            this.TU = new SmartTypedArray$2("float64",f);
            this.AU = new SmartTypedArray$2("float64",f);
            var s = ua.indexOf("Gecko/") >= 0 || ua.indexOf("Windows") >= 0;
            this.wU = Math.round(r[1] / 2 * (s ? 1.2 : 1))
        }
        e.prototype.bU = function(e) {
            this.size = e || [];
            this.xU();
            this.mU()
        }
        ;
        e.prototype.Ce = function(e) {
            var r = this.size
              , t = r[0]
              , i = r[1];
            var a = t * i;
            var n = this.Nz;
            if (n) {
                n.clearRect(0, 0, t, i);
                n.fillText(e, this.buffer, this.wU);
                var o = n.getImageData(0, 0, t, i);
                var f = new Uint8ClampedArray(a);
                var s = new Array(9).fill(0);
                s[0] = 1;
                s[1] = 1;
                var u = e.charCodeAt(0);
                if (this.RU(u)) {
                    return {
                        bitmap: f,
                        info: s
                    }
                }
                var l = 0;
                var v = 0;
                var c = INF;
                var h = INF;
                var d = 0;
                var _ = 0;
                for (var g = 0; g < a; g++) {
                    l = Math.floor(g / t);
                    v = g - l * t;
                    var y = o.data[g * 4 + 3] / 255;
                    if (y > 0) {
                        if (c > l) {
                            c = l
                        }
                        if (h > v) {
                            h = v
                        }
                        if (d < l) {
                            d = l
                        }
                        if (_ < v) {
                            _ = v
                        }
                    }
                    this.TU.value[g] = y === 1 ? 0 : y === 0 ? INF : Math.pow(Math.max(0, .5 - y), 2);
                    this.AU.value[g] = y === 1 ? INF : y === 0 ? 0 : Math.pow(Math.max(0, y - .5), 2)
                }
                var m = this.buffer;
                var p = this.getWidth(e, _ - h);
                var b = d - c;
                var x = h;
                var M = -c + m;
                var C = p;
                var S = h - 3;
                var w = c - 3;
                var k = void 0;
                if (u === 32) {
                    k = [6, 6, 0, -21, 9, 0, 0, t, i]
                } else {
                    k = [p, b, x, M, C, S, w, t, i]
                }
                edt(this.TU.value, t, i);
                edt(this.AU.value, t, i);
                for (var g = 0; g < a; g++) {
                    var T = this.TU.value[g] - this.AU.value[g];
                    var A = Math.max(0, Math.min(255, Math.round(255 - 255 * (T / this.radius + this.MU))));
                    f[g] = A
                }
                return {
                    bitmap: f,
                    info: k
                }
            }
            return {}
        }
        ;
        e.prototype.getWidth = function(e, r) {
            var t;
            switch (e) {
            case "y":
            case "A":
            case "T":
            case "V":
            case "W":
            case "Y":
                {
                    t = r;
                    break
                }
            default:
                t = r + 2
            }
            return t
        }
        ;
        e.prototype.measureText = function(e) {
            return this.Nz.measureText(e)
        }
        ;
        e.prototype.xU = function() {
            var e = this.size;
            if (!this.canvas) {
                this.canvas = document.createElement("canvas")
            }
            this.canvas.width = e[0];
            this.canvas.height = e[1];
            this.Nz = this.canvas.getContext("2d");
            if (this.Nz) {
                this.Nz.font = this.fontWeight + " " + this.fontSize + "px " + this.fontFamily;
                this.Nz.textBaseline = "middle";
                this.Nz.fillStyle = "black"
            }
        }
        ;
        e.prototype.mU = function() {
            var e = this.size
              , r = e[0]
              , t = e[1];
            var i = r * t;
            if (i > this.AU.uf) {
                this.AU.ff(i);
                this.TU.ff(i)
            }
            var a = ua.indexOf("Gecko/") >= 0 || ua.indexOf("Windows") >= 0;
            this.wU = Math.round(t / 2 * (a ? 1.2 : 1))
        }
        ;
        e.prototype.RU = function(e) {
            var r = this.SU(e, ControlChars);
            var t = this.SU(e, ZeroWidthChars);
            return r || t
        }
        ;
        e.prototype.SU = function(e, r) {
            for (var t = 0, i = r; t < i.length; t++) {
                var a = i[t];
                if (e >= a[0] && e <= a[1]) {
                    return true
                }
            }
        }
        ;
        return e
    }();
    function expandTypeArray(e) {
        f.ff(e);
        d.ff(e);
        z.ff(e);
        v.ff(e)
    }
    function edt(e, r, t) {
        if (f.uf < t) {
            expandTypeArray(t)
        }
        for (var i = 0; i < r; i++) {
            for (var a = 0; a < t; a++) {
                f.value[a] = e[a * r + i]
            }
            edt1d(f.value, d.value, v.value, z.value, t);
            for (var a = 0; a < t; a++) {
                e[a * r + i] = d.value[a]
            }
        }
        if (f.uf < r) {
            expandTypeArray(t)
        }
        for (var a = 0; a < t; a++) {
            for (var i = 0; i < r; i++) {
                f.value[i] = e[a * r + i]
            }
            edt1d(f.value, d.value, v.value, z.value, r);
            for (var i = 0; i < r; i++) {
                e[a * r + i] = Math.sqrt(d.value[i])
            }
        }
    }
    function edt1d(e, r, t, i, a) {
        t[0] = 0;
        i[0] = -INF;
        i[1] = +INF;
        for (var n = 1, o = 0; n < a; n++) {
            var f = (e[n] + n * n - (e[t[o]] + t[o] * t[o])) / (2 * n - 2 * t[o]);
            while (f <= i[o]) {
                o--;
                f = (e[n] + n * n - (e[t[o]] + t[o] * t[o])) / (2 * n - 2 * t[o])
            }
            o++;
            t[o] = n;
            i[o] = f;
            i[o + 1] = +INF
        }
        for (var n = 0, o = 0; n < a; n++) {
            while (i[o + 1] < n) {
                o++
            }
            r[n] = (n - t[o]) * (n - t[o]) + e[t[o]]
        }
    }
    var LangConf = {
        ch: {
            type: "ch",
            size: [32, 32],
            fontSize: 24,
            name: "中文简体"
        },
        en: {
            type: "en",
            size: [32, 32],
            fontSize: 24,
            name: "英文"
        },
        th: {
            type: "th",
            size: [32, 32],
            fontSize: 24,
            name: "泰语"
        },
        my: {
            type: "my",
            size: [56, 56],
            fontSize: 24,
            name: "缅甸语"
        },
        ja: {
            type: "ja",
            size: [32, 32],
            fontSize: 24,
            name: "日文"
        },
        km: {
            type: "km",
            size: [40, 40],
            fontSize: 24,
            name: "高棉语"
        },
        PU: {
            type: "lao",
            size: [40, 40],
            fontSize: 24,
            name: "老挝语"
        },
        $U: {
            type: "arabic",
            size: [40, 40],
            fontSize: 24,
            name: "阿拉伯语"
        },
        default: {
            type: "ch",
            size: [32, 32],
            fontSize: 24,
            name: "汉语"
        }
    };
    var Lang = function() {
        function e(e) {
            var r = this.CU = e
              , t = r.type
              , i = r.size
              , a = r.fontSize;
            this.type = t;
            var n = this.width = i[0];
            var o = this.height = i[1];
            this.NU = new TinySDF(a,[n, o],(n - a) / 2,8,.25,"PingFang SC","normal")
        }
        e.prototype.Ce = function(e) {
            if (this.type === LangConf.PU.type) {
                this.width = Math.floor(this.NU.measureText(e).width / 10) * 10 + 10 + LangConf.PU.fontSize / 2;
                this.NU.bU([this.width, this.height])
            } else if ((this.type === LangConf.my.type || this.type === LangConf.PU.type || this.type === LangConf.th.type || this.type === LangConf.$U.type || this.type === LangConf.km.type) && e.length > 1) {
                this.width = Math.floor(this.CU.size[0] * e.length * .5);
                this.NU.bU([this.width, this.height])
            }
            return this.NU.Ce(e)
        }
        ;
        return e
    }();
    var LangManager = function() {
        function i() {
            this.OU = {}
        }
        i.gG = function(e) {
            var r = e.split("|");
            var t = parseInt(r[0], 10);
            return this.IU(t)
        }
        ;
        i.kU = function(e) {
            var r = [];
            for (var t = 0; t < e.length; t++) {
                r.push(e.charCodeAt(t))
            }
            return i.gG(r.join("|"))
        }
        ;
        i.LU = function(e) {
            var r = i.kU(e);
            return i.EU.indexOf(r) !== -1
        }
        ;
        i.IU = function(e) {
            var r = "";
            if (e > 32 && e < 591) {
                r = "en"
            } else if (e >= 4096 && e <= 4255) {
                r = "my"
            } else if (e >= 3584 && e <= 3711) {
                r = "th"
            } else if (e >= 6016 && e <= 6143) {
                r = "km"
            } else if (e >= 3712 && e <= 3839) {
                r = "lao"
            } else if (e >= 1536 && e <= 1791) {
                r = "arabic"
            } else {
                r = "ch"
            }
            return r
        }
        ;
        i.prototype.D$ = function(e) {
            if (e === void 0) {
                e = "ch"
            }
            var r = this.OU[e];
            if (!r) {
                var t = LangConf[e] ? LangConf[e] : LangConf.default;
                r = new Lang(t);
                this.OU[e] = r
            }
            return r
        }
        ;
        i.EU = [LangConf.my.type, LangConf.km.type, LangConf.PU.type];
        return i
    }();
    var CHN = [["90.398619", "47.62125"], ["90.493569", "47.512157"], ["90.491338", "47.430787"], ["90.552986", "47.391174"], ["90.522859", "47.318461"], ["90.584915", "47.224989"], ["90.782025", "47.019194"], ["90.923517", "46.97996"], ["90.986624", "46.807403"], ["91.082926", "46.722064"], ["91.051812", "46.596604"], ["91.100242", "46.564068"], ["91.102624", "46.537196"], ["90.935125", "46.300398"], ["91.032801", "46.126363"], ["91.055181", "46.011658"], ["90.741148", "45.713536"], ["90.699563", "45.544726"], ["90.700228", "45.504993"], ["90.795887", "45.449956"], ["90.832257", "45.31629"], ["90.919719", "45.26871"], ["90.920899", "45.225065"], ["91.136978", "45.244316"], ["91.253943", "45.164928"], ["91.372068", "45.142003"], ["91.45689", "45.18485"], ["91.624753", "45.098124"], ["92.062061", "45.11548"], ["92.497694", "45.030848"], ["92.887666", "45.075869"], ["93.513157", "44.997233"], ["94.22699", "44.694971"], ["94.374082", "44.542037"], ["94.620202", "44.47394"], ["94.737532", "44.366876"], ["95.002041", "44.282908"], ["95.42701", "44.324539"], ["95.440292", "44.292217"], ["95.365491", "44.054053"], ["95.549126", "44.026335"], ["95.883651", "43.431746"], ["95.911953", "43.287656"], ["96.385653", "42.919774"], ["96.41151", "42.75927"], ["97.176454", "42.824161"], ["98.198333", "42.682435"], ["99.505985", "42.597497"], ["99.964106", "42.676693"], ["100.259793", "42.666107"], ["100.320604", "42.718863"], ["100.867753", "42.700052"], ["101.814637", "42.530852"], ["102.090733", "42.250567"], ["102.451458", "42.173439"], ["102.716911", "42.181644"], ["103.426452", "41.910629"], ["103.858223", "41.830321"], ["104.537873", "41.903108"], ["104.55755", "41.881352"], ["104.554117", "41.69046"], ["104.929798", "41.682608"], ["105.010371", "41.620463"], ["105.220141", "41.778353"], ["105.732529", "41.977008"], ["106.776853", "42.319288"], ["107.167833", "42.354897"], ["107.458928", "42.487147"], ["107.937949", "42.433308"], ["108.241189", "42.489425"], ["108.84269", "42.425722"], ["109.022141", "42.487574"], ["109.290662", "42.465227"], ["109.534657", "42.500548"], ["109.723763", "42.60669"], ["110.100045", "42.671172"], ["110.420666", "42.80617"], ["111.005001", "43.354829"], ["111.446257", "43.521761"], ["111.555004", "43.519925"], ["111.780481", "43.697712"], ["111.925235", "43.719055"], ["111.936243", "43.796237"], ["111.863286", "43.902314"], ["111.643689", "44.039247"], ["111.386948", "44.360034"], ["111.610923", "44.793044"], ["111.742544", "44.988614"], ["111.9942", "45.118751"], ["112.435906", "45.103062"], ["112.61512", "44.956037"], ["112.806373", "44.882816"], ["113.624854", "44.775834"], ["113.896701", "44.9424"], ["114.104283", "44.984106"], ["114.437113", "45.233044"], ["114.543629", "45.41585"], ["114.742713", "45.46746"], ["114.977202", "45.40645"], ["115.36123", "45.421513"], ["115.688202", "45.487095"], ["116.023886", "45.712008"], ["116.163919", "45.717567"], ["116.251037", "45.785617"], ["116.213851", "45.874652"], ["116.246574", "45.98171"], ["116.56904", "46.316673"], ["116.83033", "46.413023"], ["117.353361", "46.390324"], ["117.41533", "46.528251"], ["117.39222", "46.582448"], ["117.411768", "46.610108"], ["117.618878", "46.628442"], ["117.72387", "46.549579"], ["118.312433", "46.768455"], ["118.774374", "46.71799"], ["118.828619", "46.795917"], ["118.912926", "46.804113"], ["118.941979", "46.764971"], ["119.028625", "46.769292"], ["119.119864", "46.669494"], ["119.724391", "46.623078"], ["119.791059", "46.707882"], ["119.884572", "46.701717"], ["119.896224", "46.880494"], ["119.836485", "46.899381"], ["119.742007", "47.13776"], ["119.30249", "47.404711"], ["119.315214", "47.461827"], ["119.134262", "47.517476"], ["119.10804", "47.64174"], ["118.755577", "47.747779"], ["118.55396", "47.96464"], ["118.177657", "48.02096"], ["117.82558", "47.987997"], ["117.388809", "47.612384"], ["117.082887", "47.796869"], ["116.816533", "47.867783"], ["116.452289", "47.808327"], ["116.270714", "47.845797"], ["115.938377", "47.653883"], ["115.559971", "47.900966"], ["115.500727", "48.161305"], ["115.791907", "48.279437"], ["115.776629", "48.537807"], ["116.042275", "48.826715"], ["116.024444", "48.890046"], ["116.709459", "49.880215"], ["117.07866", "49.723328"], ["117.482128", "49.648667"], ["117.801998", "49.554185"], ["117.858689", "49.620567"], ["118.068073", "49.643304"], ["118.210831", "49.759243"], ["118.358674", "49.80286"], ["118.37378", "49.853853"], ["118.468623", "49.857768"], ["118.559926", "49.957101"], ["119.082034", "50.01451"], ["119.311051", "50.169532"], ["119.328389", "50.327724"], ["119.175332", "50.320094"], ["119.103534", "50.373674"], ["119.103642", "50.409488"], ["119.224019", "50.469114"], ["119.262214", "50.625767"], ["119.470096", "50.763554"], ["119.480245", "50.910299"], ["119.734497", "51.106944"], ["119.73542", "51.228925"], ["119.789064", "51.243368"], ["119.787948", "51.298691"], ["119.896417", "51.365189"], ["119.889615", "51.410731"], ["119.950318", "51.426267"], ["120.067241", "51.699773"], ["120.632029", "51.963004"], ["120.755196", "52.16206"], ["120.733974", "52.23734"], ["120.60276", "52.318868"], ["120.688398", "52.529211"], ["120.459208", "52.615218"], ["120.046556", "52.554881"], ["120.004778", "52.785297"], ["120.275145", "52.889292"], ["120.877526", "53.323248"], ["121.231921", "53.310391"], ["122.338707", "53.534212"], ["122.443142", "53.474191"], ["122.84101", "53.486577"], ["123.278553", "53.592823"], ["123.480492", "53.533829"], ["123.518364", "53.587614"], ["123.901491", "53.509565"], ["124.129157", "53.379309"], ["124.260864", "53.404185"], ["124.447374", "53.252351"], ["124.696798", "53.233398"], ["124.854212", "53.143501"], ["124.877429", "53.19179"], ["125.14374", "53.233437"], ["125.500989", "53.129213"], ["125.53472", "53.084642"], ["125.631731", "53.108131"], ["125.776978", "53.000452"], ["125.732839", "52.906456"], ["125.842016", "52.927481"], ["125.973551", "52.795444"], ["126.049747", "52.829373"], ["126.137295", "52.788424"], ["126.136093", "52.7501"], ["126.082878", "52.727183"], ["126.094744", "52.66376"], ["126.01078", "52.632295"], ["126.225078", "52.557751"], ["126.227546", "52.502887"], ["126.28664", "52.498733"], ["126.378071", "52.405299"], ["126.365626", "52.334723"], ["126.460233", "52.313556"], ["126.457937", "52.278438"], ["126.370797", "52.21752"], ["126.587327", "52.146539"], ["126.544647", "52.022831"], ["126.483493", "52.004144"], ["126.489866", "51.963387"], ["126.757379", "51.72942"], ["126.769931", "51.638237"], ["126.737959", "51.604012"], ["126.870396", "51.541384"], ["126.8313", "51.449554"], ["126.939726", "51.411373"], ["126.952021", "51.35238"], ["127.007918", "51.339647"], ["126.930671", "51.194554"], ["126.950047", "51.077214"], ["127.328668", "50.761993"], ["127.324612", "50.674039"], ["127.399564", "50.585975"], ["127.336199", "50.480736"], ["127.39435", "50.441709"], ["127.374866", "50.346296"], ["127.622938", "50.242265"], ["127.624762", "50.159938"], ["127.524126", "50.053819"], ["127.569466", "49.832087"], ["127.686474", "49.798414"], ["127.716987", "49.69577"], ["127.838395", "49.617717"], ["128.185065", "49.56898"], ["128.533065", "49.633229"], ["128.791094", "49.618148"], ["128.84167", "49.562161"], ["128.811865", "49.506248"], ["129.022107", "49.485245"], ["129.11165", "49.389985"], ["129.230032", "49.429552"], ["129.343135", "49.392234"], ["129.383497", "49.461849"], ["129.526126", "49.451932"], ["129.593332", "49.316758"], ["129.745059", "49.313625"], ["129.972146", "49.057136"], ["130.243692", "48.89923"], ["130.444579", "48.939218"], ["130.526934", "48.889044"], ["130.693552", "48.907086"], ["130.705612", "48.869994"], ["130.580449", "48.630526"], ["130.630317", "48.609845"], ["130.648105", "48.52472"], ["130.790884", "48.525345"], ["130.778868", "48.413864"], ["130.875106", "48.300724"], ["130.708423", "48.108792"], ["130.914073", "47.945556"], ["131.01546", "47.736076"], ["131.552374", "47.750448"], ["131.621768", "47.692216"], ["131.711934", "47.740348"], ["131.958075", "47.703033"], ["131.995368", "47.741041"], ["132.235458", "47.739381"], ["132.340472", "47.796162"], ["132.546873", "47.750881"], ["132.655685", "47.89209"], ["132.654505", "47.979308"], ["132.811511", "47.967327"], ["133.097413", "48.158185"], ["133.473394", "48.128346"], ["133.728504", "48.281365"], ["134.199908", "48.411499"], ["134.519091", "48.448618"], ["134.797053", "48.402511"], ["134.903462", "48.476549"], ["135.111473", "48.465979"], ["135.087461", "48.366044"], ["134.70854", "48.235879"], ["134.705815", "48.142967"], ["134.587133", "47.991745"], ["134.802439", "47.732613"], ["134.805079", "47.701502"], ["134.584837", "47.454022"], ["134.327302", "47.405829"], ["134.203448", "47.309543"], ["134.189801", "47.25835"], ["134.258273", "47.190339"], ["134.25029", "47.096963"], ["134.160018", "47.066453"], ["134.091868", "46.968583"], ["134.042215", "46.633997"], ["133.892806", "46.477753"], ["133.978872", "46.404235"], ["133.938468", "46.35728"], ["133.943124", "46.251663"], ["133.734856", "46.143924"], ["133.77584", "46.068467"], ["133.698828", "45.924364"], ["133.509421", "45.823745"], ["133.533154", "45.779212"], ["133.488886", "45.719859"], ["133.513906", "45.69461"], ["133.496869", "45.599732"], ["133.229721", "45.489276"], ["133.168674", "45.414886"], ["133.124621", "45.258199"], ["133.158975", "45.105773"], ["132.946351", "44.989767"], ["131.996377", "45.226214"], ["131.893723", "45.301592"], ["131.80903", "45.186922"], ["131.72307", "45.208468"], ["131.688416", "45.166214"], ["131.716461", "45.107939"], ["131.507506", "44.942491"], ["131.131804", "44.90387"], ["131.021662", "44.839439"], ["131.137726", "44.723122"], ["131.338398", "44.056058"], ["131.273661", "43.974086"], ["131.248341", "43.566337"], ["131.344342", "43.514183"], ["131.346252", "43.391702"], ["131.236002", "43.22857"], ["131.242955", "43.131057"], ["131.13749", "43.018486"], ["131.168904", "42.923466"], ["131.065779", "42.901952"], ["131.05005", "42.834641"], ["130.673962", "42.819219"], ["130.46123", "42.731347"], ["130.617421", "42.685874"], ["130.650616", "42.595365"], ["130.604267", "42.509264"], ["130.661752", "42.412289"], ["130.553477", "42.421191"], ["130.507536", "42.571366"], ["130.42958", "42.523293"], ["130.416105", "42.590452"], ["130.235238", "42.692151"], ["130.237405", "42.873951"], ["130.125611", "42.876169"], ["130.075765", "42.911319"], ["130.092008", "42.956423"], ["129.933178", "42.975108"], ["129.878311", "42.92378"], ["129.795163", "42.719399"], ["129.815032", "42.601778"], ["129.735103", "42.411401"], ["129.607751", "42.418118"], ["129.55415", "42.330996"], ["129.383025", "42.403749"], ["129.268527", "42.323223"], ["129.222093", "42.257635"], ["129.2434", "42.19201"], ["128.992774", "42.075243"], ["128.959644", "41.997184"], ["128.121765", "41.974471"], ["128.183949", "41.746614"], ["128.336234", "41.608881"], ["128.268621", "41.434008"], ["128.127944", "41.337429"], ["128.00482", "41.417516"], ["127.65285", "41.377517"], ["127.537794", "41.447986"], ["127.248116", "41.454805"], ["127.234962", "41.492217"], ["127.08566", "41.544545"], ["127.128081", "41.592273"], ["127.012339", "41.679643"], ["127.035706", "41.702541"], ["126.914964", "41.778689"], ["126.809177", "41.669562"], ["126.74607", "41.691325"], ["126.612003", "41.629975"], ["126.529799", "41.412013"], ["126.562243", "41.368516"], ["126.557908", "41.33825"], ["126.45328", "41.325763"], ["126.3043", "41.139979"], ["126.148946", "41.070584"], ["126.132917", "40.990783"], ["126.041744", "40.924117"], ["126.041787", "40.876401"], ["125.721896", "40.840697"], ["125.681319", "40.80754"], ["125.712733", "40.774611"], ["125.686641", "40.761853"], ["125.582743", "40.75319"], ["125.423162", "40.624246"], ["125.310595", "40.636606"], ["125.022783", "40.521123"], ["125.06057", "40.46396"], ["124.907556", "40.445935"], ["124.374311", "40.078794"], ["124.388452", "39.905638"], ["123.741782", "39.10294"], ["123.282201", "38.058466"], ["124.24299", "31.368588"], ["124.714351", "29.508006"], ["125.072136", "25.859069"], ["124.935837", "25.676612"], ["123.836839", "25.5127"], ["122.751321", "24.515956"], ["121.947788", "22.070621"], ["121.867355", "21.383021"], ["119.827087", "18.177352"], ["119.026527", "16.873489"], ["119.11665", "15.951676"], ["119.101406", "14.063252"], ["119.100623", "12.209516"], ["119.07742", "11.23357"], ["116.011301", "7.678721"], ["114.941852", "6.182877"], ["114.229887", "5.47862"], ["113.86269", "4.692236"], ["112.795642", "3.695169"], ["111.953841", "3.231846"], ["109.894631", "3.714037"], ["108.659956", "4.748032"], ["107.925439", "5.91737"], ["107.96348", "6.895899"], ["108.555028", "9.28242"], ["110.128544", "11.338543"], ["109.983314", "12.44102"], ["109.806102", "13.535124"], ["109.636428", "14.636855"], ["109.78629", "15.402577"], ["109.449813", "15.723154"], ["108.117871", "17.258818"], ["107.310076", "18.842045"], ["107.374728", "19.467179"], ["108.051095", "20.114414"], ["108.328478", "21.187373"], ["108.278139", "21.403412"], ["108.107378", "21.47839"], ["107.943356", "21.509097"], ["107.849844", "21.619412"], ["107.547355", "21.55724"], ["107.470751", "21.571209"], ["107.451932", "21.615722"], ["107.357798", "21.570152"], ["107.289755", "21.702532"], ["107.207551", "21.682454"], ["106.998789", "21.79714"], ["107.015655", "21.907972"], ["106.951239", "21.893876"], ["106.768184", "21.978429"], ["106.703854", "21.935422"], ["106.674027", "21.941175"], ["106.664565", "22.266461"], ["106.643493", "22.30484"], ["106.538715", "22.328641"], ["106.53142", "22.460275"], ["106.592939", "22.622033"], ["106.708553", "22.615477"], ["106.793547", "22.794758"], ["106.548972", "22.899047"], ["106.304634", "22.830954"], ["106.190543", "22.951201"], ["106.088791", "22.966118"], ["105.867991", "22.889816"], ["105.712488", "23.033058"], ["105.550311", "23.045755"], ["105.530806", "23.163759"], ["105.330691", "23.346393"], ["105.249667", "23.237422"], ["105.089657", "23.231409"], ["104.951169", "23.149476"], ["104.843495", "23.101267"], ["104.893084", "22.94393"], ["104.747257", "22.79551"], ["104.567764", "22.813411"], ["104.371855", "22.65974"], ["104.249053", "22.720536"], ["104.237015", "22.806132"], ["104.137495", "22.783462"], ["104.078336", "22.740585"], ["104.017096", "22.520843"], ["103.962035", "22.492019"], ["103.666692", "22.741534"], ["103.525093", "22.580551"], ["103.458703", "22.648987"], ["103.394201", "22.751092"], ["103.341286", "22.767397"], ["103.306804", "22.661878"], ["103.215652", "22.621855"], ["103.205309", "22.536362"], ["103.107505", "22.486567"], ["103.076606", "22.416444"], ["102.91784", "22.456309"], ["102.840786", "22.583305"], ["102.610223", "22.696625"], ["102.558017", "22.673838"], ["102.493515", "22.738071"], ["102.424293", "22.676273"], ["102.446694", "22.621241"], ["102.327948", "22.511268"], ["102.278531", "22.386786"], ["102.142661", "22.36867"], ["102.03475", "22.427651"], ["101.942096", "22.415036"], ["101.8765", "22.355136"], ["101.764791", "22.472926"], ["101.700847", "22.453176"], ["101.694624", "22.349917"], ["101.579332", "22.223979"], ["101.631925", "21.988099"], ["101.718335", "21.962549"], ["101.79863", "21.848016"], ["101.778824", "21.73654"], ["101.856952", "21.62392"], ["101.847124", "21.595911"], ["101.773932", "21.558438"], ["101.760328", "21.349162"], ["101.862724", "21.239522"], ["101.764727", "21.115389"], ["101.663511", "21.164623"], ["101.587808", "21.146813"], ["101.574161", "21.209359"], ["101.53264", "21.221881"], ["101.293538", "21.149214"], ["101.203866", "21.206559"], ["101.216612", "21.289294"], ["101.114173", "21.403592"], ["101.183052", "21.497378"], ["101.111941", "21.560533"], ["101.095719", "21.734009"], ["100.901892", "21.659343"], ["100.747526", "21.494902"], ["100.586185", "21.423588"], ["100.468576", "21.433695"], ["100.414267", "21.504705"], ["100.248806", "21.434534"], ["100.115855", "21.481604"], ["100.078626", "21.591362"], ["100.11579", "21.654357"], ["100.010068", "21.666462"], ["99.916813", "21.811046"], ["99.968591", "21.972539"], ["99.950867", "22.023392"], ["99.699748", "22.008512"], ["99.585378", "22.083453"], ["99.19384", "22.094388"], ["99.129961", "22.147006"], ["99.24403", "22.347694"], ["99.226069", "22.407775"], ["99.348979", "22.505302"], ["99.289627", "22.751963"], ["99.429595", "22.858203"], ["99.422858", "22.95454"], ["99.521735", "22.955884"], ["99.488432", "23.051046"], ["99.336512", "23.101188"], ["99.258964", "23.048934"], ["99.10449", "23.057463"], ["98.99924", "23.151389"], ["98.858693", "23.167152"], ["98.89811", "23.292382"], ["98.844295", "23.323341"], ["98.878284", "23.4184"], ["98.821571", "23.466061"], ["98.773527", "23.530055"], ["98.849638", "23.617887"], ["98.774085", "23.751823"], ["98.656368", "23.775684"], ["98.63961", "23.801661"], ["98.649909", "23.978332"], ["98.828974", "24.077833"], ["98.83893", "24.109859"], ["98.594849", "24.051324"], ["98.534853", "24.098401"], ["98.118939", "24.06406"], ["97.652578", "23.812007"], ["97.500916", "23.916402"], ["97.507889", "23.946076"], ["97.703905", "24.12772"], ["97.7321", "24.242073"], ["97.646012", "24.307132"], ["97.678134", "24.384313"], ["97.653759", "24.420913"], ["97.528617", "24.403523"], ["97.504349", "24.421636"], ["97.520614", "24.750904"], ["97.686181", "24.867223"], ["97.726886", "24.872596"], ["97.701159", "24.901891"], ["97.691374", "25.085579"], ["97.816257", "25.289948"], ["97.844152", "25.297669"], ["97.93803", "25.249897"], ["98.122308", "25.413237"], ["98.152177", "25.643055"], ["98.313818", "25.589382"], ["98.380058", "25.615679"], ["98.510756", "25.863626"], ["98.635018", "25.83211"], ["98.668857", "25.860517"], ["98.546226", "26.121072"], ["98.648708", "26.171346"], ["98.686366", "26.138757"], ["98.648", "26.279486"], ["98.751404", "26.620222"], ["98.701365", "26.849746"], ["98.733852", "27.033154"], ["98.685272", "27.065508"], ["98.663149", "27.194049"], ["98.708661", "27.325701"], ["98.657935", "27.422786"], ["98.67244", "27.532116"], ["98.574979", "27.540164"], ["98.537407", "27.61841"], ["98.464022", "27.631357"], ["98.417695", "27.512725"], ["98.29766", "27.498051"], ["98.144388", "27.832814"], ["98.155954", "27.920407"], ["98.109477", "27.953809"], ["98.111", "28.123353"], ["97.991245", "28.182664"], ["97.984571", "28.253152"], ["97.904449", "28.328541"], ["97.775702", "28.306704"], ["97.650433", "28.498038"], ["97.537651", "28.471615"], ["97.537544", "28.300942"], ["97.374809", "28.215134"], ["97.340262", "28.085365"], ["97.441328", "28.020526"], ["97.41045", "27.866111"], ["97.271254", "27.865238"], ["97.104206", "27.710621"], ["97.074959", "27.716851"], ["96.959302", "27.833554"], ["96.799979", "27.862962"], ["96.485238", "28.041967"], ["96.435456", "28.122218"], ["96.282742", "28.11548"], ["96.254032", "28.195619"], ["96.011753", "28.160892"], ["95.866721", "28.266533"], ["95.747824", "28.246895"], ["95.417182", "28.118906"], ["95.304315", "27.916842"], ["94.898787", "27.716889"], ["94.531603", "27.567615"], ["94.290032", "27.552854"], ["93.888688", "27.17204"], ["93.845987", "27.008916"], ["93.571308", "26.909059"], ["92.944937", "26.862668"], ["92.647405", "26.922873"], ["92.093539", "26.830122"], ["92.094247", "26.952103"], ["91.998589", "27.089524"], ["92.083004", "27.299047"], ["92.034273", "27.387239"], ["91.995907", "27.444668"], ["91.729424", "27.433927"], ["91.566002", "27.518015"], ["91.534996", "27.644815"], ["91.602566", "27.75719"], ["91.514804", "27.82382"], ["91.562676", "27.867913"], ["91.468799", "27.914302"], ["91.441784", "27.98019"], ["91.319818", "28.026891"], ["91.09123", "27.815659"], ["90.700164", "28.042914"], ["90.592875", "27.992071"], ["90.457885", "28.016188"], ["90.284765", "28.126494"], ["90.15898", "28.149257"], ["90.026414", "28.108761"], ["89.808083", "28.209972"], ["89.741178", "28.14466"], ["89.620585", "28.133666"], ["89.394701", "27.853894"], ["89.261577", "27.776101"], ["89.129934", "27.494282"], ["89.208705", "27.359876"], ["89.003119", "27.183627"], ["88.914757", "27.261172"], ["88.754318", "27.459692"], ["88.742559", "27.575756"], ["88.824484", "27.683585"], ["88.858366", "27.84585"], ["88.817167", "27.989323"], ["88.749833", "28.04227"], ["88.643789", "28.079175"], ["88.414536", "27.948938"], ["88.164768", "27.927649"], ["88.119621", "27.836742"], ["87.844298", "27.914245"], ["87.731731", "27.773994"], ["87.596998", "27.78341"], ["87.555606", "27.832169"], ["87.226918", "27.783961"], ["87.114007", "27.809055"], ["87.017899", "27.921014"], ["86.741974", "28.007834"], ["86.699681", "28.06541"], ["86.621253", "28.036683"], ["86.583402", "28.057988"], ["86.53626", "27.935498"], ["86.410089", "27.875861"], ["86.21079", "27.953771"], ["86.168518", "28.117581"], ["86.115561", "28.075237"], ["86.149271", "27.905693"], ["85.994046", "27.879218"], ["85.944521", "27.932939"], ["85.832512", "28.151055"], ["85.730202", "28.210313"], ["85.701256", "28.320003"], ["85.608344", "28.222546"], ["85.501399", "28.296408"], ["85.379283", "28.245137"], ["85.108273", "28.308745"], ["85.080035", "28.467918"], ["85.159664", "28.55567"], ["85.153999", "28.618263"], ["85.06716", "28.641711"], ["84.995663", "28.560816"], ["84.851232", "28.539159"], ["84.68178", "28.609315"], ["84.624424", "28.696084"], ["84.465938", "28.711423"], ["84.389849", "28.828396"], ["84.202845", "28.880867"], ["84.21602", "29.009754"], ["84.163899", "29.035197"], ["84.138279", "29.170731"], ["84.156046", "29.210987"], ["84.110534", "29.218441"], ["84.085858", "29.266484"], ["83.922973", "29.294016"], ["83.658078", "29.134976"], ["83.571818", "29.151918"], ["83.232014", "29.552591"], ["83.070545", "29.578981"], ["82.920063", "29.684809"], ["82.816744", "29.661877"], ["82.675381", "29.739992"], ["82.684436", "29.800693"], ["82.605515", "29.811846"], ["82.541184", "29.930689"], ["82.159367", "30.045582"], ["82.174859", "30.141657"], ["82.084479", "30.216893"], ["82.077913", "30.310431"], ["81.993091", "30.291904"], ["81.637645", "30.413095"], ["81.562006", "30.34101"], ["81.44238", "30.380465"], ["81.419613", "30.185719"], ["81.306617", "30.138261"], ["81.279452", "29.994378"], ["81.11603", "30.010804"], ["80.995588", "30.23662"], ["80.302677", "30.539587"], ["80.0878", "30.543727"], ["79.94236", "30.672449"], ["79.868267", "30.834188"], ["79.825373", "30.839126"], ["79.673774", "30.945519"], ["79.593372", "30.894565"], ["79.4033", "31.036535"], ["79.346716", "31.001927"], ["79.336288", "30.933391"], ["79.224193", "30.920174"], ["79.170227", "30.981969"], ["79.092743", "30.963258"], ["78.986249", "31.027673"], ["78.96668", "31.157988"], ["78.856924", "31.281503"], ["78.783796", "31.274131"], ["78.728542", "31.344273"], ["78.757918", "31.433515"], ["78.697536", "31.51499"], ["78.804524", "31.611708"], ["78.62458", "31.82264"], ["78.722126", "31.927562"], ["78.578596", "31.999524"], ["78.497701", "32.11424"], ["78.432641", "32.118838"], ["78.402622", "32.220389"], ["78.4762", "32.31131"], ["78.36638", "32.528271"], ["78.381336", "32.554898"], ["78.75176", "32.731065"], ["78.809223", "32.496551"], ["78.976872", "32.364195"], ["79.005883", "32.376536"], ["79.080126", "32.394077"], ["79.114029", "32.491665"], ["79.279168", "32.61355"], ["79.272795", "32.713951"], ["79.195912", "32.779409"], ["79.222884", "32.93039"], ["79.135616", "33.001861"], ["79.1289", "33.16414"], ["78.818901", "33.407102"], ["78.715496", "33.540948"], ["78.723736", "33.604683"], ["78.656015", "33.65053"], ["78.749506", "33.746395"], ["78.715582", "33.972994"], ["78.631468", "34.014908"], ["78.635867", "34.100789"], ["78.894839", "34.171104"], ["78.97243", "34.341399"], ["78.867137", "34.336615"], ["78.698266", "34.491737"], ["78.566988", "34.478543"], ["78.527312", "34.539705"], ["78.430667", "34.514354"], ["78.403587", "34.570207"], ["78.247139", "34.615374"], ["78.238835", "34.685346"], ["78.179204", "34.722162"], ["78.172767", "34.964414"], ["78.056982", "35.102584"], ["77.985699", "35.231108"], ["77.986279", "35.376996"], ["78.060694", "35.455461"], ["77.904546", "35.435236"], ["77.815948", "35.484733"], ["77.688854", "35.41908"], ["77.389219", "35.439747"], ["77.300084", "35.509365"], ["77.189727", "35.490691"], ["76.844516", "35.637645"], ["76.746969", "35.630442"], ["76.675837", "35.721674"], ["76.57151", "35.752903"], ["76.552477", "35.878323"], ["76.372211", "35.795695"], ["76.149781", "35.797227"], ["76.124933", "35.947734"], ["75.922673", "36.058952"], ["75.911558", "36.150212"], ["76.037836", "36.25097"], ["75.962627", "36.325913"], ["75.987883", "36.449905"], ["75.852334", "36.642976"], ["75.645268", "36.740208"], ["75.442793", "36.696296"], ["75.389578", "36.92691"], ["75.229053", "36.936669"], ["75.144038", "36.993436"], ["74.896481", "36.905551"], ["74.830134", "37.022531"], ["74.731107", "36.999897"], ["74.690745", "37.052831"], ["74.559724", "37.004866"], ["74.481919", "37.046871"], ["74.437845", "37.120308"], ["74.499922", "37.266966"], ["74.739861", "37.314987"], ["74.866312", "37.253902"], ["75.109062", "37.372562"], ["75.102367", "37.44057"], ["74.921629", "37.536802"], ["74.864573", "37.654148"], ["74.964759", "37.773242"], ["74.890065", "37.834937"], ["74.908884", "37.982278"], ["74.777327", "38.153069"], ["74.761705", "38.335766"], ["74.838288", "38.415972"], ["74.834661", "38.466073"], ["74.630191", "38.571841"], ["74.373536", "38.625873"], ["74.159667", "38.646204"], ["74.103234", "38.581"], ["74.111817", "38.521998"], ["74.000838", "38.494528"], ["73.77285", "38.599147"], ["73.670411", "38.8621"], ["73.725193", "38.95749"], ["73.802719", "38.951449"], ["73.807654", "38.986017"], ["73.72221", "39.008697"], ["73.473516", "39.38863"], ["73.616295", "39.498494"], ["73.852308", "39.511093"], ["73.918183", "39.600828"], ["73.897347", "39.705585"], ["73.808942", "39.800228"], ["73.93934", "39.994893"], ["73.923182", "40.040084"], ["74.206402", "40.150079"], ["74.349396", "40.120697"], ["74.57305", "40.295485"], ["74.65313", "40.30568"], ["74.68538", "40.371234"], ["74.834146", "40.370645"], ["74.764109", "40.445837"], ["74.82522", "40.547706"], ["74.991968", "40.485604"], ["75.234225", "40.478276"], ["75.595829", "40.68866"], ["75.623209", "40.675235"], ["75.670288", "40.537172"], ["75.761075", "40.482617"], ["75.702088", "40.361506"], ["75.718331", "40.319244"], ["75.831778", "40.356634"], ["75.909498", "40.327457"], ["75.962198", "40.405801"], ["76.169136", "40.4108"], ["76.276231", "40.468254"], ["76.350474", "40.381058"], ["76.518981", "40.485522"], ["76.629574", "40.63177"], ["76.618888", "40.769281"], ["76.705191", "40.836687"], ["76.738064", "40.970777"], ["76.866853", "41.052415"], ["77.002079", "41.102703"], ["77.174621", "41.038594"], ["77.761016", "41.051719"], ["77.819595", "41.177071"], ["78.104789", "41.252161"], ["78.153155", "41.41058"], ["78.633699", "41.494773"], ["78.691227", "41.573398"], ["79.204752", "41.752489"], ["79.314144", "41.836204"], ["79.760721", "41.919283"], ["79.886034", "42.062214"], ["80.133162", "42.067184"], ["80.141101", "42.212865"], ["80.253518", "42.271926"], ["80.176806", "42.427971"], ["80.233755", "42.509043"], ["80.134728", "42.629333"], ["80.233669", "42.836907"], ["80.527124", "42.908804"], ["80.358446", "43.012006"], ["80.363209", "43.05597"], ["80.77054", "43.196057"], ["80.751915", "43.283969"], ["80.657952", "43.321619"], ["80.722024", "43.489763"], ["80.498157", "43.799783"], ["80.367136", "44.112086"], ["80.382199", "44.282371"], ["80.320637", "44.484489"], ["80.375676", "44.600628"], ["80.200903", "44.711915"], ["80.153825", "44.809898"], ["79.982829", "44.769908"], ["79.863417", "44.896027"], ["79.863911", "44.925979"], ["80.099001", "45.081976"], ["80.380783", "45.073778"], ["80.472493", "45.151008"], ["80.593643", "45.1361"], ["80.723355", "45.20611"], ["80.897505", "45.157969"], ["81.777248", "45.412611"], ["81.933761", "45.262246"], ["82.295816", "45.254845"], ["82.544253", "45.206065"], ["82.569401", "45.340669"], ["82.52743", "45.402307"], ["82.265496", "45.514783"], ["82.238288", "45.626914"], ["82.311373", "45.780155"], ["82.319655", "45.956177"], ["82.440763", "46.003982"], ["82.822452", "46.775333"], ["83.012309", "47.24291"], ["83.148286", "47.241861"], ["83.371532", "47.179737"], ["83.583899", "47.087891"], ["83.927865", "46.999324"], ["84.753106", "47.038661"], ["84.955001", "46.896507"], ["85.227256", "47.083274"], ["85.529079", "47.087102"], ["85.670679", "47.295094"], ["85.656817", "47.416255"], ["85.581307", "47.505664"], ["85.500433", "48.023314"], ["85.564678", "48.210504"], ["85.778117", "48.445159"], ["86.21609", "48.461326"], ["86.297393", "48.520485"], ["86.571493", "48.56921"], ["86.745751", "48.738276"], ["86.72689", "48.79468"], ["86.785169", "48.850936"], ["86.706676", "48.980358"], ["86.708565", "49.011387"], ["86.889625", "49.162344"], ["87.145572", "49.183316"], ["87.435358", "49.10582"], ["87.476878", "49.120484"], ["87.490826", "49.169134"], ["87.836187", "49.198616"], ["87.895432", "49.115877"], ["87.87024", "49.056222"], ["87.9406", "48.974696"], ["87.803872", "48.87904"], ["88.125672", "48.72326"], ["88.023491", "48.591881"], ["88.35763", "48.495848"], ["88.450756", "48.423234"], ["88.527102", "48.435337"], ["88.631537", "48.352171"], ["88.608513", "48.280466"], ["88.657844", "48.207172"], ["88.828926", "48.136652"], ["88.942652", "48.146274"], ["89.088199", "48.017358"], ["89.571383", "48.066881"], ["89.777999", "47.863838"], ["90.077548", "47.910676"], ["90.155997", "47.747389"], ["90.399628", "47.669347"], ["90.413575", "47.647046"], ["90.398619", "47.62125"]];
    var JPN = [["145.824681", "43.408601"], ["145.802708", "43.198731"], ["145.188847", "43.068449"], ["144.992667", "42.907867"], ["143.970938", "42.890345"], ["143.384543", "42.351736"], ["143.299399", "41.875506"], ["142.95333", "42.100071"], ["142.391844", "42.272669"], ["141.369501", "42.535185"], ["140.832544", "42.147648"], ["141.028863", "41.945699"], ["141.296655", "41.765675"], ["141.564447", "41.265171"], ["141.675004", "40.481173"], ["141.829516", "40.300647"], ["141.982619", "40.122464"], ["142.142627", "39.594221"], ["142.025897", "39.368452"], ["142.021777", "39.317473"], ["141.850783", "38.901626"], ["140.939308", "35.65669"], ["139.436268", "34.193845"], ["137.347075", "34.1603"], ["135.63835", "33.29272"], ["133.488103", "32.70897"], ["131.614016", "31.325292"], ["130.633951", "28.842845"], ["127.785048", "25.789008"], ["126.597333", "26.344784"], ["128.404011", "28.563857"], ["128.940636", "30.856318"], ["128.544464", "32.79621"], ["129.194642", "34.475467"], ["129.324698", "34.765641"], ["129.502255", "34.700627"], ["129.480282", "33.855809"], ["130.439605", "33.915587"], ["130.801388", "34.159716"], ["130.791882", "34.445506"], ["131.121471", "34.437578"], ["131.298124", "34.567727"], ["132.535148", "35.287335"], ["132.978675", "36.154847"], ["133.268559", "36.366209"], ["134.710796", "35.708808"], ["135.303308", "35.794165"], ["136.404389", "36.456814"], ["136.612743", "37.37546"], ["137.409252", "37.580348"], ["137.291471", "36.906421"], ["137.982237", "37.126821"], ["138.504087", "37.383552"], ["138.758146", "37.83898"], ["139.207212", "38.04151"], ["139.310081", "38.315743"], ["139.737028", "38.85997"], ["139.403318", "39.234917"], ["139.952634", "39.328459"], ["140.002073", "39.745742"], ["139.695586", "39.824208"], ["139.679107", "40.025361"], ["139.935912", "40.077921"], ["140.001079", "40.309807"], ["139.922802", "40.434311"], ["139.828954", "40.646676"], ["139.95008", "40.766604"], ["139.957833", "41.432491"], ["140.001779", "42.027933"], ["139.389291", "42.038134"], ["139.346719", "42.242877"], ["139.63099", "42.291658"], ["139.72712", "42.641155"], ["139.942098", "42.710048"], ["140.194783", "42.842098"], ["140.30396", "42.856193"], ["140.309453", "43.306551"], ["140.393224", "43.399418"], ["140.61976", "43.355264"], ["140.788732", "43.238558"], ["140.923314", "43.24556"], ["141.029058", "43.24356"], ["141.083989", "43.216544"], ["141.141667", "43.185513"], ["141.336675", "43.240561"], ["141.328435", "43.731771"], ["141.350408", "43.792272"], ["141.594854", "43.937818"], ["141.565334", "44.291761"], ["141.70541", "44.45177"], ["141.708157", "44.742196"], ["141.683437", "45.002047"], ["141.226594", "45.076599"], ["141.053559", "45.165748"], ["140.987641", "45.335905"], ["140.943696", "45.481498"], ["141.201589", "45.501064"], ["141.972005", "45.52416"], ["142.341996", "45.204477"], ["142.567215", "45.014513"], ["142.747966", "44.801094"], ["143.487122", "44.343678"], ["144.463587", "44.078617"], ["144.874201", "44.098345"], ["145.125513", "44.222478"], ["145.300832", "44.391445"], ["145.487445", "44.320885"], ["145.111163", "43.848378"], ["145.420933", "43.545126"], ["145.451146", "43.393635"]];
    var EAST = [["73.757316", "29.95798"], ["73.045185", "28.989136"], ["72.434431", "28.60931"], ["72.060098", "28.097836"], ["71.631749", "27.60702"], ["70.67236", "27.593264"], ["69.82743", "27.041579"], ["70.337045", "26.566045"], ["70.070655", "26.098932"], ["70.070655", "25.85946"], ["70.290717", "25.713457"], ["70.626599", "25.671708"], ["70.68451", "25.201036"], ["70.974064", "24.791642"], ["70.811914", "24.612763"], ["70.139151", "24.465923"], ["69.268014", "24.435423"], ["68.313114", "24.267539"], ["67.911051", "23.808548"], ["68.547651", "23.00912"], ["69.11724", "22.684917"], ["68.748682", "22.406416"], ["69.301519", "21.536417"], ["70.153052", "20.55461"], ["71.170222", "20.332219"], ["72.234349", "21.204506"], ["72.501752", "21.087142"], ["72.627589", "19.003767"], ["72.973641", "16.878922"], ["74.267757", "13.888898"], ["75.369075", "10.48118"], ["76.976429", "7.781262"], ["78.374805", "8.242836"], ["79.427902", "9.641233"], ["80.513091", "10.734685"], ["80.254133", "11.953377"], ["80.702994", "13.082541"], ["80.402232", "15.312175"], ["82.991824", "16.887862"], ["85.259974", "19.051683"], ["87.784208", "21.10679"], ["89.263244", "21.658877"], ["89.121009", "23.437218"], ["88.869364", "23.717994"], ["88.93501", "24.317619"], ["88.267603", "24.596482"], ["88.727129", "25.073086"], ["89.186656", "25.132532"], ["88.891246", "25.626788"], ["88.650542", "25.666241"], ["88.201956", "25.892841"], ["88.420778", "26.12883"], ["88.54113", "26.285892"], ["89.241361", "26.020727"], ["89.613359", "25.863309"], ["89.657124", "25.330477"], ["90.190932", "25.070998"], ["91.175632", "25.051175"], ["91.811935", "25.05221"], ["92.234344", "24.983507"], ["92.104658", "24.649259"], ["91.844714", "24.363456"], ["91.46563", "24.215373"], ["90.999897", "23.839459"], ["91.216517", "23.054471"], ["91.389672", "22.300847"], ["92.125338", "20.623521"], ["93.946113", "18.753448"], ["92.823675", "16.135717"], ["91.224276", "9.998681"], ["93.522355", "5.752403"], ["96.409859", "0.991383"], ["100.048825", "-3.72227"], ["104.60328", "-7.736835"], ["114.01025", "-9.967468"], ["123.433702", "-11.032285"], ["123.717523", "-10.536628"], ["124.427075", "-10.257467"], ["125.004217", "-9.79418"], ["125.026253", "-9.527403"], ["125.12788", "-9.406056"], ["125.031601", "-9.358561"], ["124.946021", "-9.216037"], ["125.026253", "-9.168516"], ["125.192065", "-9.13155"], ["125.181368", "-8.982538"], ["124.999509", "-8.987821"], ["124.919277", "-8.924418"], ["125.095787", "-8.596662"], ["125.555784", "-8.088612"], ["125.721597", "-8.120384"], ["127.292232", "-8.326419"], ["132.424841", "-8.447132"], ["139.646777", "-8.512957"], ["141.039618", "-9.209048"], ["141.025347", "-6.894209"], ["140.978655", "-6.887392"], ["140.92235", "-6.851943"], ["140.912737", "-6.791946"], ["140.905871", "-6.737397"], ["140.850939", "-6.695117"], ["140.903124", "-6.59554"], ["140.945696", "-6.509588"], ["141.000442", "-6.341825"], ["140.971754", "-2.322171"], ["131.63417", "2.671779"], ["125.182746", "17.665698"], ["120.512231", "19.243264"], ["119.486602", "16.550429"], ["119.820154", "12.379244"], ["115.293339", "5.77989"], ["114.675587", "4.850295"], ["114.550618", "4.735342"], ["113.923024", "4.60805"], ["113.581092", "3.881824"], ["113.423754", "3.763395"], ["113.138025", "3.427757"], ["112.798973", "3.119213"], ["111.886744", "2.960093"], ["109.608393", "3.016867"], ["108.771364", "3.935896"], ["107.181008", "7.101455"], ["110.509765", "12.398562"], ["108.171244", "18.449712"], ["108.482399", "21.923911"], ["106.042422", "23.36869"], ["103.861684", "23.500971"], ["101.853026", "22.51803"], ["101.964992", "22.453371"], ["102.079606", "22.467488"], ["102.100842", "22.366033"], ["102.192713", "22.264854"], ["102.457234", "22.052786"], ["102.482552", "21.991854"], ["102.505944", "21.933096"], ["102.598494", "21.858522"], ["102.635027", "21.768076"], ["102.625285", "21.706993"], ["102.652076", "21.663993"], ["102.749497", "21.641356"], ["102.798208", "21.673047"], ["102.841349", "21.836472"], ["102.857688", "21.688772"], ["102.936435", "21.684707"], ["102.971433", "21.587108"], ["102.897061", "21.530145"], ["102.831439", "21.416153"], ["102.853313", "21.363197"], ["102.770191", "21.249074"], ["102.905811", "20.975636"], ["103.219101", "20.747931"], ["103.517772", "20.714677"], ["103.809332", "20.581588"], ["103.928404", "20.796397"], ["104.17602", "20.868718"], ["104.456453", "20.674762"], ["104.597217", "20.649539"], ["104.494205", "20.561152"], ["104.33078", "20.448774"], ["104.574769", "20.381484"], ["104.658948", "20.213054"], ["104.76838", "20.149846"], ["104.838529", "20.181454"], ["104.938282", "20.043215"], ["104.745932", "19.888842"], ["104.790827", "19.817585"], ["104.656142", "19.725166"], ["104.580381", "19.643265"], ["104.41625", "19.738372"], ["104.315236", "19.711959"], ["104.004834", "19.776374"], ["103.978829", "19.601517"], ["104.057089", "19.487272"], ["103.795479", "19.292317"], ["104.05618", "19.205901"], ["104.303857", "19.041792"], ["104.509718", "18.953592"], ["104.638381", "18.819682"], ["104.82816", "18.734411"], ["105.081179", "18.714495"], ["105.090265", "18.561021"], ["105.010482", "18.494337"], ["105.215532", "18.232365"], ["105.507333", "18.022503"], ["105.673181", "17.706155"], ["105.959258", "17.411042"], ["106.393016", "16.974064"], ["106.530565", "16.907086"], ["106.511314", "16.705452"], ["106.610307", "16.485756"], ["106.704089", "16.360817"], ["106.855183", "16.430793"], ["106.917705", "16.245803"], ["107.074009", "16.275813"], ["107.178212", "16.130721"], ["107.386617", "15.975505"], ["107.256364", "15.895346"], ["107.157745", "15.729918"], ["107.270294", "15.538977"], ["107.541111", "15.371176"], ["107.598724", "15.119441"], ["107.495192", "15.087312"], ["107.421241", "14.973038"], ["107.491494", "14.719275"], ["107.439728", "14.558285"], ["107.365777", "14.619117"], ["107.28443", "14.576179"], ["107.125343", "14.454386"], ["107.058787", "14.468707"], ["106.862816", "14.336197"], ["106.714913", "14.443644"], ["106.640961", "14.525985"], ["106.467176", "14.622607"], ["106.385829", "14.475868"], ["106.219438", "14.500928"], ["106.189858", "14.42932"], ["106.104814", "14.407834"], ["105.956911", "14.346944"], ["105.993887", "14.192854"], ["106.138092", "14.056594"], ["106.067838", "13.984846"], ["105.938423", "13.948964"], ["105.860774", "14.067355"], ["105.570516", "14.196239"], ["105.40579", "14.123984"], ["105.307739", "14.207645"], ["105.23322", "14.257067"], ["105.284207", "14.321679"], ["105.397946", "14.336879"], ["105.499919", "14.435654"], ["105.550906", "14.617894"], ["105.535217", "14.773437"], ["105.660723", "14.90613"], ["105.629347", "15.019802"], ["105.511685", "15.118269"], ["105.543062", "15.182626"], ["105.660723", "15.2848"], ["105.543062", "15.368016"], ["105.641113", "15.40205"], ["105.652879", "15.526792"], ["105.672489", "15.723201"], ["105.433244", "15.866611"], ["105.419865", "16.077323"], ["105.134284", "16.158015"], ["105.05029", "16.303179"], ["104.798307", "16.56635"], ["104.815106", "16.791639"], ["104.807079", "17.064937"], ["104.834545", "17.357467"], ["104.746654", "17.518618"], ["104.392993", "17.569592"], ["104.221867", "17.790865"], ["104.071113", "18.043041"], ["103.989584", "18.196169"], ["103.880878", "18.236734"], ["103.692584", "18.269917"], ["103.616878", "18.310466"], ["103.546996", "18.349162"], ["103.428584", "18.336313"], ["103.370348", "18.277339"], ["103.302336", "18.221647"], ["103.237773", "18.102183"], ["103.207191", "18.040806"], ["103.105249", "17.937384"], ["102.938746", "17.875949"], ["102.914959", "17.827432"], ["102.813018", "17.765959"], ["102.687291", "17.753015"], ["102.568359", "17.830667"], ["102.510593", "17.840371"], ["102.422244", "17.921219"], ["102.310109", "17.960013"], ["102.204769", "17.995566"], ["102.143605", "18.066652"], ["101.987295", "17.966477"], ["101.868364", "17.927685"], ["101.647491", "17.811257"], ["101.148809", "17.535022"], ["101.023374", "17.599797"], ["101.039054", "17.808912"], ["101.175591", "17.993048"], ["101.212078", "18.10208"], ["101.219896", "18.173904"], ["101.206865", "18.240749"], ["101.212078", "18.344679"], ["101.107829", "18.394147"], ["101.092192", "18.45349"], ["101.206865", "18.578877"], ["101.31372", "18.655442"], ["101.272021", "18.741845"], ["101.272021", "18.830671"], ["101.311114", "18.939171"], ["101.370997", "19.079681"], ["101.298023", "19.148632"], ["101.279779", "19.212632"], ["101.22818", "19.375675"], ["101.230424", "19.441269"], ["101.295484", "19.487804"], ["101.29997", "19.591402"], ["101.239397", "19.633668"], ["101.145173", "19.587175"], ["101.093574", "19.629442"], ["100.94102", "19.650571"], ["100.893907", "19.65691"], ["100.864742", "19.616763"], ["100.835578", "19.570265"], ["100.770518", "19.534326"], ["100.611233", "19.574493"], ["100.584312", "19.52164"], ["100.525983", "19.540669"], ["100.46541", "19.637894"], ["100.436245", "19.777288"], ["100.541687", "19.91867"], ["100.597773", "20.190526"], ["100.502946", "20.195053"], ["100.428788", "20.262059"], ["100.379218", "20.36601"], ["100.329779", "20.413638"], ["100.244635", "20.387895"], ["100.173224", "20.317079"], ["100.160865", "20.244943"], ["100.114789", "20.260301"], ["100.107922", "20.315689"], ["100.155988", "20.507461"], ["100.211891", "20.650056"], ["100.290818", "20.736459"], ["100.399973", "20.811825"], ["100.562866", "20.788277"], ["100.673701", "20.899705"], ["100.59616", "20.954029"], ["100.751867", "21.254036"], ["100.998402", "21.338663"], ["101.211622", "21.580378"], ["101.209043", "21.846211"], ["100.909901", "21.894492"], ["100.207565", "21.580378"], ["100.051491", "22.159742"], ["99.466212", "22.436515"], ["99.71333", "23.024318"], ["99.352488", "23.281431"], ["99.03983", "24.3247"], ["98.101855", "24.95993"], ["98.855913", "25.608485"], ["98.911088", "26.812993"], ["98.892697", "27.483968"], ["98.340947", "27.826069"], ["98.120247", "28.377665"], ["97.586888", "28.733061"], ["97.108705", "28.329109"], ["96.658506", "27.787659"], ["96.296725", "28.010502"], ["95.847241", "28.136253"], ["95.288126", "27.787659"], ["94.685158", "27.233428"], ["93.281889", "26.647032"], ["92.037851", "26.720259"], ["90.141229", "26.820605"], ["88.984724", "26.833974"], ["88.731159", "27.080308"], ["88.794435", "27.331629"], ["88.741186", "27.914582"], ["88.223484", "27.953015"], ["88.169108", "27.50057"], ["87.774017", "27.262697"], ["88.215457", "26.870398"], ["87.783036", "26.535989"], ["87.047344", "26.702224"], ["84.777332", "27.310285"], ["83.539214", "27.665305"], ["82.385985", "27.880248"], ["81.010299", "28.662263"], ["80.547568", "29.001896"], ["81.345045", "30.304903"], ["80.749935", "31.185747"], ["80.056967", "31.015739"], ["79.215234", "31.178494"], ["78.991224", "31.346766"], ["79.254658", "31.656243"], ["78.835019", "31.979804"], ["78.65109", "32.12011"], ["78.595912", "32.240756"], ["78.591313", "32.400075"], ["78.669483", "32.54361"], ["78.739513", "32.426665"], ["78.84424", "32.288005"], ["79.067657", "32.24668"], ["79.277111", "32.470789"], ["79.395802", "32.676713"], ["79.235221", "33.233259"], ["79.011803", "33.47819"], ["78.84424", "33.681772"], ["79.182009", "34.336185"], ["78.821102", "34.565916"], ["78.428537", "34.758609"], ["78.294895", "34.940474"], ["78.23791", "35.530039"], ["77.722544", "35.628785"], ["77.294994", "35.609184"], ["76.821489", "35.701892"], ["76.684572", "35.988603"], ["76.399329", "35.919333"], ["76.159724", "35.933192"], ["76.165429", "36.20986"], ["76.074151", "36.366207"], ["75.999987", "36.641347"], ["75.714744", "36.787688"], ["75.562518", "36.805399"], ["75.481245", "36.811057"], ["75.45651", "36.950968"], ["75.253968", "37.023882"], ["75.084541", "37.066268"], ["74.894884", "37.084426"], ["74.735572", "37.122745"], ["74.643309", "37.074934"], ["74.566145", "37.033976"], ["74.549925", "36.938948"], ["74.491692", "37.015218"], ["74.377803", "36.998366"], ["74.261908", "36.915016"], ["74.132106", "36.91687"], ["74.057933", "36.840851"], ["73.967535", "36.853836"], ["73.925813", "36.892775"], ["73.808039", "36.908632"], ["73.728388", "36.920709"], ["73.639124", "36.911926"], ["73.699986", "36.820121"], ["73.765904", "36.814624"], ["73.833196", "36.776135"], ["73.853795", "36.743128"], ["73.834569", "36.711208"], ["73.735692", "36.736525"], ["73.657414", "36.707906"], ["73.599736", "36.711208"], ["73.57227", "36.735425"], ["73.517339", "36.729922"], ["73.397862", "36.757433"], ["73.319585", "36.738726"], ["73.241307", "36.720664"], ["73.175389", "36.732772"], ["73.112218", "36.71516"], ["73.032567", "36.686532"], ["73.039433", "36.651284"], ["73.060033", "36.609405"], ["73.036687", "36.564195"], ["73.002355", "36.529994"], ["72.958409", "36.507921"], ["72.944676", "36.475904"], ["72.886998", "36.474799"], ["72.855412", "36.448292"], ["72.848546", "36.400777"], ["72.832334", "36.374217"], ["72.796629", "36.362053"], ["72.685258", "36.303681"], ["72.663286", "36.274901"], ["72.574022", "36.267151"], ["72.461412", "36.175196"], ["73.530389", "35.809316"], ["74.170542", "35.420826"], ["74.340245", "35.055408"], ["73.9237", "34.612198"], ["73.676858", "34.332391"], ["73.880481", "33.422673"], ["74.651862", "32.906095"], ["75.747223", "32.542685"], ["75.623802", "32.256112"], ["74.975842", "31.968632"], ["74.759855", "31.732749"], ["74.759855", "30.955369"], ["74.327882", "30.584206"], ["74.18929", "30.224947"], ["74.035013", "29.864372"]];
    var getOutseaData = function(e) {
        if (e && e === "mapbox") {
            return {
                boundsP16: [[CHN]],
                nebulaOutSea: []
            }
        } else {
            return {
                boundsP16: [[CHN], [JPN], [EAST]],
                nebulaOutSea: [[JPN], [EAST]]
            }
        }
    };
    var geoUtil = new GeometryUtilCls({});
    var CONSTS$1 = {
        sV: "https://webapi.amap.com/style_icon/sprite_v8@2x.png",
        EU: [LangConf.my.type, LangConf.km.type, LangConf.PU.type, LangConf.th.type, LangConf.$U.type]
    };
    var MapboxLabelFormat$1 = function(t) {
        __extends(e, t);
        function e(e) {
            var r = t.call(this) || this;
            r.kQ = {};
            return r
        }
        e.prototype.Ns = function(e, r) {
            t.prototype.Ns.call(this, e, r);
            return this.rV(e, r)
        }
        ;
        e.prototype.rV = function(e, r) {
            var t = e.data || {};
            var i = t.poilabel || [];
            var a = e.road || [];
            var n = {};
            var o = this.hV(e);
            var f;
            for (var s in i) {
                if (i.hasOwnProperty(s)) {
                    f = i[s];
                    var u = this.nV(f, o, r.zoom, r.VQ);
                    n[s] = u
                }
            }
            var l = this.oV(a, {});
            return n
        }
        ;
        e.prototype.nV = function(e, r, t, i) {
            var a = [];
            var n = this.kQ;
            for (var o = 0, f = e; o < f.length; o++) {
                var s = f[o];
                var u = s.name
                  , l = s.position
                  , v = s.id
                  , c = s.minzoom
                  , h = s.maxzoom
                  , d = s.style
                  , _ = d === void 0 ? {} : d;
                if (i !== "mapbox" && t >= 9 && r && !this.uV(l[0])) {
                    continue
                }
                var g = _.lV
                  , y = _.th
                  , m = _.ih
                  , p = _.nh
                  , b = _.qU
                  , x = _.eh
                  , M = _.textAnchor
                  , C = M === void 0 ? "bottom" : M
                  , S = _.rh
                  , E = _.sh
                  , w = _.Kr
                  , k = _.Qr;
                var T = [c, h];
                var A = null;
                if (p && b && x) {
                    this.Fs[CONSTS$1.sV] = 1;
                    A = [{
                        image: CONSTS$1.sV,
                        size: [b[6] / 2, b[7] / 2],
                        clipOrigin: [b[4], b[5]],
                        clipSize: [b[6], b[7]]
                    }]
                }
                var I = null;
                if (u && w) {
                    if (!n[w]) {
                        n[w] = Util$3.color2RgbaArray(w)
                    }
                    var $ = n[w];
                    var F = void 0;
                    var P = 0;
                    if (!n[k]) {
                        n[k] = Util$3.color2RgbaArray(k || "")
                    }
                    if (n[k]) {
                        F = n[k];
                        P = 3
                    }
                    var L = this.fV(u) ? [0, u.length] : [];
                    this.Ws(u, L);
                    I = {
                        txt: u,
                        $G: L,
                        direction: C || "center",
                        offset: [0, 0],
                        style: {
                            fontSize: S,
                            fillColor: $,
                            strokeColor: F,
                            strokeWidth: P,
                            fold: false
                        }
                    }
                }
                var R = {
                    data: {
                        id: v,
                        name: u,
                        txt: u,
                        position: l[0],
                        positionType: "relative",
                        zooms: [y, m],
                        extData: {
                            id: v,
                            type: "poi"
                        }
                    },
                    opts: {
                        zIndex: g,
                        zooms: T,
                        visible: true
                    }
                };
                if (A) {
                    R.opts.icon = A
                }
                if (I) {
                    R.opts.text = I
                }
                if (R.opts.text || R.opts.icon) {
                    a.push(R)
                }
            }
            return a
        }
        ;
        e.prototype.oV = function(e, r) {
            var t;
            var i;
            for (var a = 0, n = e; a < n.length; a++) {
                var o = n[a];
                t = o.name;
                i = this.fV(t) ? [0, t.length] : [];
                if (t) {
                    this.Ws(t, i)
                }
            }
        }
        ;
        e.prototype.DQ = function(e, r) {
            var t = [];
            var i;
            var a;
            var n;
            var o;
            var f;
            for (var s = 0, u = e; s < u.length; s++) {
                var l = u[s];
                i = l.path;
                a = l.name || "";
                n = labelsUtil$2.aQ(a.split(""));
                f = l.distance || 1e5;
                o = l.style && l.style[1] || {};
                o = l.style || {};
                var v = o.th
                  , c = v === void 0 ? zoomRange[0] : v
                  , h = o.ih
                  , d = h === void 0 ? zoomRange[1] : h
                  , _ = o.Kr
                  , g = o.Qr
                  , y = o.rh
                  , m = o.zIndex
                  , p = o.Hr
                  , b = p === void 0 ? "name_en" : p;
                var x = _;
                var M = g;
                if (a && i && i.length) {
                    var C = {
                        path: i,
                        positionType: "absolute",
                        name: a,
                        $G: [],
                        CO: n,
                        rank: m,
                        BG: b,
                        distance: f,
                        zooms: [c, d],
                        style: {
                            fontSize: y || 12,
                            fillColor: x,
                            strokeColor: M
                        }
                    };
                    t.push(C)
                }
            }
            return t
        }
        ;
        e.prototype.fV = function(e) {
            var r = LangManager.kU(e);
            return CONSTS$1.EU.indexOf(r) !== -1
        }
        ;
        e.prototype.uV = function(e) {
            var r = "EPSG:3857";
            var t = ProjectionManager.getProjection(r).unproject(e[0], e[1]);
            var i = getOutseaData("mapbox");
            var a = i.boundsP16;
            for (var n = 0; n < a.length; n++) {
                var o = a[n];
                var f = geoUtil.isPointInPolygon(t, o[0]);
                if (f) {
                    return false
                }
            }
            return true
        }
        ;
        e.prototype.hV = function(e) {
            var r = e.Ro;
            var t = [[r[0], r[1]], [r[2], r[1]], [r[2], r[3]], [r[0], r[3]]];
            return this.cV(t)
        }
        ;
        e.prototype.cV = function(e) {
            var r = [];
            for (var t = 0; t < e.length; t++) {
                var i = e[t];
                var a = "EPSG:3857";
                var n = ProjectionManager.getProjection(a).unproject(i[0], i[1]);
                r.push(n)
            }
            var o = getOutseaData("mapbox");
            var f = o.boundsP16;
            for (var s = 0; s < f.length; s++) {
                var u = f[s];
                var l = geoUtil["isRingInRingByMapboxB"](r, u[0]);
                if (l) {
                    return true
                }
            }
            return false
        }
        ;
        e.id = "mapbox";
        return e
    }(LabelFormat);
    var Color$1 = function() {
        function e(e) {
            this.type = "AMap.Color";
            this.rgba = [];
            this.Vb(e)
        }
        r = e;
        e.Wb = function(e, r) {
            return {
                rgba: e.rgba
            }
        }
        ;
        e.Gb = function(e) {
            return new r(e.rgba)
        }
        ;
        e.FV = function(e, r) {
            var t = this.DV(e[0], e[1], e[2], e[3]);
            var i = t[0];
            var a = t[1];
            var n = t[2];
            var o = t[3];
            var f = .35;
            n = n - (n - .5) * r * f;
            a = a - (a - .5) * r * f * 5;
            i = i + (i > .5 ? -.5 : .5) * r * f;
            var s = this.GQ(i, a, n, o);
            return s
        }
        ;
        e.DV = function(e, r, t, i) {
            var a = Math.max(e, r, t), n = Math.min(e, r, t), o, f, s = (a + n) / 2, u;
            if (a === n) {
                o = f = 0
            } else {
                u = a - n;
                f = s > .5 ? u / (2 - a - n) : u / (a + n);
                switch (a) {
                case e:
                    o = (r - t) / u + (r < t ? 6 : 0);
                    break;
                case r:
                    o = (t - e) / u + 2;
                    break;
                case t:
                    o = (e - r) / u + 4;
                    break
                }
                o /= 6
            }
            return [o, f, s, i]
        }
        ;
        e.uQ = function(e, r, t) {
            if (t < 0) {
                t += 1
            }
            if (t > 1) {
                t -= 1
            }
            if (t < 1 / 6) {
                return e + (r - e) * 6 * t
            }
            if (t < 1 / 2) {
                return r
            }
            if (t < 2 / 3) {
                return e + (r - e) * (2 / 3 - t) * 6
            }
            return e
        }
        ;
        e.GQ = function(e, r, t, i) {
            var a, n, o;
            if (r === 0) {
                a = n = o = t
            } else {
                var f = t < .5 ? t * (1 + r) : t + r - t * r
                  , s = 2 * t - f;
                a = this.uQ(s, f, e + 1 / 3);
                n = this.uQ(s, f, e);
                o = this.uQ(s, f, e - 1 / 3)
            }
            return [a, n, o, i]
        }
        ;
        e.Hb = function(e, r) {
            if (r === void 0) {
                r = []
            }
            for (var t = 0, i = e.length; t < i; t += 2) {
                r[t / 2] = parseInt(e.substr(t, 2), 16)
            }
            return r
        }
        ;
        e.Yx = function(e) {
            if (e.startsWith("#")) {
                e = e.substr(1)
            }
            if (colorNames[e]) {
                return r.Hb(colorNames[e].substr(1))
            }
            if (e.length === 1) {
                return r.Hb(e + e + e + e + e + e)
            }
            if (e.length === 3) {
                return r.Hb(e[0] + e[0] + e[1] + e[1] + e[2] + e[2])
            }
            if (e.length === 6) {
                return r.Hb(e)
            }
            return [0, 0, 0]
        }
        ;
        e.normalize = function(e) {
            var r = [];
            for (var t = 0, i = e.length; t < i; t++) {
                r[t] = e[t] / 255
            }
            return r
        }
        ;
        e.create = function(e) {
            if (e) {
                return new r(e)
            } else {
                return null
            }
        }
        ;
        e.prototype.normalize = function() {
            return [this.rgba[0] / 255, this.rgba[1] / 255, this.rgba[2] / 255, this.rgba[3] / 255]
        }
        ;
        e.prototype.Vb = function(e) {
            if (typeof e === "string") {
                if (colorNames[e]) {
                    this.Hb(colorNames[e].substr(1));
                    return
                }
                if (e.length === 0) {
                    return
                }
                if (e.startsWith("#")) {
                    e = e.substr(1)
                }
                if (e.length === 1) {
                    this.Hb(e + e + e + e + e + e);
                    return
                }
                if (e.length === 3) {
                    this.Hb(e[0] + e[0] + e[1] + e[1] + e[2] + e[2]);
                    return
                }
                if (e.length === 6) {
                    this.Hb(e);
                    return
                }
                if (e.length === 8) {
                    this.$b(e);
                    return
                }
                if (e.startsWith("rgba(")) {
                    this.Yb(colorNames[e].substr(1));
                    return
                }
                if (e.startsWith("rgb(")) {
                    this.Kb(colorNames[e].substr(1));
                    return
                }
            } else if (e instanceof Array) {
                this.rgba[0] = e[0];
                this.rgba[1] = e[1];
                this.rgba[2] = e[2];
                this.rgba[3] = e[3] || 255
            }
        }
        ;
        e.prototype.Hb = function(e) {
            this.$b(e + "FF")
        }
        ;
        e.prototype.$b = function(e) {
            for (var r = 0, t = e.length; r < t; r += 2) {
                this.rgba[r / 2] = parseInt(e.substr(r, 2), 16)
            }
        }
        ;
        e.prototype.Kb = function(e) {
            var r = e.split(",");
            this.rgba[0] = parseInt(r[0].substr(4), 10);
            this.rgba[1] = parseInt(r[1], 10);
            this.rgba[2] = parseInt(r[2], 10);
            this.rgba[3] = 255
        }
        ;
        e.prototype.Yb = function(e) {
            var r = e.split(",");
            this.rgba[0] = parseInt(r[0].substr(5), 10);
            this.rgba[1] = parseInt(r[1], 10);
            this.rgba[2] = parseInt(r[2], 10);
            this.rgba[3] = parseFloat(r[3]) * 255 << 0
        }
        ;
        var r;
        e = r = __decorate([InnerClass("Color")], e);
        return e
    }();
    var colorNames = {
        aliceblue: "#f0f8ff",
        antiquewhite: "#faebd7",
        aqua: "#00ffff",
        aquamarine: "#7fffd4",
        azure: "#f0ffff",
        beige: "#f5f5dc",
        bisque: "#ffe4c4",
        black: "#000000",
        blanchedalmond: "#ffebcd",
        blue: "#0000ff",
        blueviolet: "#8a2be2",
        brown: "#a52a2a",
        burlywood: "#deb887",
        cadetblue: "#5f9ea0",
        chartreuse: "#7fff00",
        chocolate: "#d2691e",
        coral: "#ff7f50",
        cornflowerblue: "#6495ed",
        cornsilk: "#fff8dc",
        crimson: "#dc143c",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgoldenrod: "#b8860b",
        darkgray: "#a9a9a9",
        darkgreen: "#006400",
        darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        darkorchid: "#9932cc",
        darkred: "#8b0000",
        darksalmon: "#e9967a",
        darkseagreen: "#8fbc8f",
        darkslateblue: "#483d8b",
        darkslategray: "#2f4f4f",
        darkturquoise: "#00ced1",
        darkviolet: "#9400d3",
        deeppink: "#ff1493",
        deepskyblue: "#00bfff",
        dimgray: "#696969",
        dodgerblue: "#1e90ff",
        firebrick: "#b22222",
        floralwhite: "#fffaf0",
        forestgreen: "#228b22",
        fuchsia: "#ff00ff",
        gainsboro: "#dcdcdc",
        ghostwhite: "#f8f8ff",
        gold: "#ffd700",
        goldenrod: "#daa520",
        gray: "#808080",
        green: "#008000",
        greenyellow: "#adff2f",
        honeydew: "#f0fff0",
        hotpink: "#ff69b4",
        indianred: "#cd5c5c",
        indigo: "#4b0082",
        ivory: "#fffff0",
        khaki: "#f0e68c",
        lavender: "#e6e6fa",
        lavenderblush: "#fff0f5",
        lawngreen: "#7cfc00",
        lemonchiffon: "#fffacd",
        lightblue: "#add8e6",
        lightcoral: "#f08080",
        lightcyan: "#e0ffff",
        lightgoldenrodyellow: "#fafad2",
        lightgrey: "#d3d3d3",
        lightgreen: "#90ee90",
        lightpink: "#ffb6c1",
        lightsalmon: "#ffa07a",
        lightseagreen: "#20b2aa",
        lightskyblue: "#87cefa",
        lightslategray: "#778899",
        lightsteelblue: "#b0c4de",
        lightyellow: "#ffffe0",
        lime: "#00ff00",
        limegreen: "#32cd32",
        linen: "#faf0e6",
        magenta: "#ff00ff",
        maroon: "#800000",
        mediumaquamarine: "#66cdaa",
        mediumblue: "#0000cd",
        mediumorchid: "#ba55d3",
        mediumpurple: "#9370d8",
        mediumseagreen: "#3cb371",
        mediumslateblue: "#7b68ee",
        mediumspringgreen: "#00fa9a",
        mediumturquoise: "#48d1cc",
        mediumvioletred: "#c71585",
        midnightblue: "#191970",
        mintcream: "#f5fffa",
        mistyrose: "#ffe4e1",
        moccasin: "#ffe4b5",
        navajowhite: "#ffdead",
        navy: "#000080",
        oldlace: "#fdf5e6",
        olive: "#808000",
        olivedrab: "#6b8e23",
        orange: "#ffa500",
        orangered: "#ff4500",
        orchid: "#da70d6",
        palegoldenrod: "#eee8aa",
        palegreen: "#98fb98",
        paleturquoise: "#afeeee",
        palevioletred: "#d87093",
        papayawhip: "#ffefd5",
        peachpuff: "#ffdab9",
        peru: "#cd853f",
        pink: "#ffc0cb",
        plum: "#dda0dd",
        powderblue: "#b0e0e6",
        purple: "#800080",
        rebeccapurple: "#663399",
        red: "#ff0000",
        rosybrown: "#bc8f8f",
        royalblue: "#4169e1",
        saddlebrown: "#8b4513",
        salmon: "#fa8072",
        sandybrown: "#f4a460",
        seagreen: "#2e8b57",
        seashell: "#fff5ee",
        sienna: "#a0522d",
        silver: "#c0c0c0",
        skyblue: "#87ceeb",
        slateblue: "#6a5acd",
        slategray: "#708090",
        snow: "#fffafa",
        springgreen: "#00ff7f",
        steelblue: "#4682b4",
        tan: "#d2b48c",
        teal: "#008080",
        thistle: "#d8bfd8",
        tomato: "#ff6347",
        turquoise: "#40e0d0",
        violet: "#ee82ee",
        wheat: "#f5deb3",
        white: "#ffffff",
        whitesmoke: "#f5f5f5",
        yellow: "#ffff00",
        yellowgreen: "#9acd32"
    };
    var NebulaLabelFormat$1 = function(t) {
        __extends(e, t);
        function e(e) {
            var r = t.call(this) || this;
            r.bt = 20;
            r.WW = 20;
            r._opts = {};
            r.uA = {};
            r.vn = e.vn;
            return r
        }
        e.prototype.Ns = function(e, r) {
            if (r.lang) {
                this.lang = r.lang
            }
            this._opts = r;
            this.bt = r.zoom;
            this.WW = r.ZL;
            return this.rV(e, r.R_)
        }
        ;
        e.prototype.rV = function(e, r) {
            var t = e.data || {};
            var i = t.poilabel || [];
            var a = {};
            var n = e.cQ;
            for (var o in i) {
                if (i.hasOwnProperty(o)) {
                    var f = this.nV(i[o], {
                        aV: e.zo.z < 10,
                        cQ: n,
                        R_: r
                    });
                    a[o] = f
                }
            }
            var s = t.roadName || [];
            var u = this.oV(s, {
                R_: r
            });
            a.eA = u;
            return a
        }
        ;
        e.prototype.nV = function(O, e) {
            if (e === void 0) {
                e = {}
            }
            var r = CONSTS.Lj
              , B = r.file
              , D = r.size;
            var N = CONSTS.Mp
              , U = CONSTS.Rp
              , H = CONSTS.Pp
              , z = CONSTS.jp;
            var G = this._opts.scale;
            var j = e.aV
              , t = e.cQ;
            var V = this.bt;
            var W = [];
            var q = "https";
            var i;
            var K;
            var Y;
            var a;
            var Z;
            var n;
            var X;
            var o;
            var f;
            var s;
            var u;
            var l;
            var v;
            var c;
            var h;
            var J;
            var Q;
            var ee;
            var re;
            var d;
            var _;
            var g;
            var y;
            var m;
            var te;
            var p;
            var b;
            var ie = this.lang;
            var ae = !t && V >= 5;
            for (var x = 0, ne = O; x < ne.length; x++) {
                var M = ne[x];
                i = M.cw;
                K = M.ew;
                Y = M.pos;
                b = labelsUtil$2.tQ(M, ie, ae ? e.R_ : undefined);
                a = b.name;
                p = b.$G || [];
                Z = M.rank;
                n = M.minzoom || N;
                X = j ? zoomRange[1] : M.maxzoom || U;
                o = M.Fp;
                f = M.Ep;
                if (n === 3) {
                    n = 2
                }
                if (o === 10002 && f === 28) {
                    continue
                }
                s = this.oA(o, f, this.WW);
                if (!s) {
                    continue
                }
                u = G > 1 || s.fontSize > 9 ? s.fontSize : 9;
                h = s.sn;
                J = s.Qe;
                Q = s.forceShow;
                ee = s.zIndex;
                re = s.zooms;
                d = labelsUtil$2.Jj([n, zoomRange[1]], re);
                if (!d) {
                    continue
                }
                d = labelsUtil$2.$p(d);
                var C = d[0];
                var S = [];
                var w = {};
                while (C <= d[1]) {
                    var k = this.vn.dn(o, f, C);
                    if (k && k.iconID !== "0") {
                        var T = k.iconID;
                        if (S.indexOf(T) === -1) {
                            S.push(T);
                            w[T] = []
                        }
                        w[T].push(C)
                    }
                    C++
                }
                if ((o !== 10002 || f !== 20 && f !== 18) && u > 16) {
                    u = 15
                }
                var A = [d[0] - H, d[1] + z];
                if (o === 10002 && (f === 13 || f === 18) && A[0] <= 3) {
                    A[0] = 2.5
                }
                _ = h && q + B[h];
                g = {
                    data: {
                        id: i ? i + f : "",
                        position: Y,
                        name: a,
                        txt: a,
                        rank: Z,
                        BG: b.type,
                        extData: {
                            Lp: o,
                            Ap: f,
                            id: i,
                            type: "poi",
                            bz: K
                        }
                    },
                    opts: {
                        type: "billboard",
                        angle: 0,
                        forceShow: Q,
                        zIndex: ee,
                        visible: true,
                        zooms: A
                    }
                };
                y = s.nn || 0;
                m = s.an || 0;
                var I = [];
                var $ = void 0;
                var F = [0, 0];
                var P = void 0;
                for (var L = 0; L < S.length; L++) {
                    var T = S[L];
                    var oe = h && this.aA(parseInt(T, 10));
                    var fe = [0, 0];
                    var se = D;
                    var R = labelsUtil$2.$p(w[T]);
                    var ue = 1;
                    if (a && (T === "152" || T === "153" || T === "154")) {
                        ue = a.length <= 2 ? 1 : a.length / 2
                    }
                    P = vector$3.Up(se, 2);
                    if (_) {
                        I.push({
                            type: "image",
                            image: _,
                            clipOrigin: vector$3.add(oe, fe),
                            clipSize: se,
                            size: [P[0] * ue, P[1]],
                            anchor: [10 - y * 20 / 24, 10 - m * 20 / 24],
                            angle: 0,
                            qq: true,
                            zooms: [R[0] - H, R[R.length - 1] + z]
                        })
                    }
                }
                if (a) {
                    if (a === "曾母暗沙") {
                        $ = "top";
                        F = [0, 5]
                    } else if (I.length && I[0].image) {
                        var T = S[0];
                        if (t) {
                            $ = "bottom";
                            F = [0, 0]
                        }
                        if (a === "北京") {
                            $ = "top";
                            F = [0, 3]
                        } else if (a === "澳门") {
                            $ = "left";
                            F = [0, -2]
                        } else if (a === "合肥") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "太原") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "西安") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "郑州") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "杭州") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "台北") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (a === "加德满都") {
                            $ = "bottom";
                            F = [0, -3]
                        } else if (o === 10002 && (f === 31 || f === 32 || f === 34 || f === 5)) {
                            $ = "right";
                            F = [-3, -3]
                        } else if (J === 1) {
                            $ = "right";
                            F = [0, -2]
                        } else if (T === "152" || T === "153" || T === "154") {
                            $ = "center";
                            F = [0, -1]
                        } else {
                            $ = "right";
                            F = [0, -2]
                        }
                    } else {
                        $ = "center";
                        F = [y, m]
                    }
                }
                var le = "" + o + f;
                var E = this.uA[le];
                if (E) {
                    l = E.faceColor;
                    v = E.borderColor;
                    c = E.holoColor
                } else {
                    l = s.faceColor && Color$1.normalize(s.faceColor.rgba);
                    v = s.borderColor && Color$1.normalize(s.borderColor.rgba);
                    c = s.Qe === 3 ? s.holoColor && Color$1.normalize(s.holoColor.rgba) : null;
                    this.uA[le] = {
                        faceColor: l,
                        borderColor: v,
                        holoColor: c
                    }
                }
                te = a ? {
                    direction: $,
                    offset: F,
                    $G: p,
                    style: {
                        fontSize: u,
                        strokeWidth: 2,
                        padding: [0, 1, 0, 1],
                        fold: !t && V >= 5
                    }
                } : {};
                if (g.opts) {
                    g.opts.icon = I;
                    g.opts.text = te
                }
                if (_) {
                    this.Fs[_] = 1
                }
                if (a) {
                    this.Ws(a, p)
                }
                W.push(g);
                s = null
            }
            return W
        }
        ;
        e.prototype.oV = function(e, r) {
            if (r === void 0) {
                r = {}
            }
            var t = CONSTS.Lj
              , i = t.file
              , a = t.size;
            var n = CONSTS.Mp
              , o = CONSTS.Rp
              , f = CONSTS.Pp
              , s = CONSTS.jp;
            var u = [];
            var l = this._opts.scale;
            var v = this.lang;
            var c = r && r.R_ && r.R_["protocol"] || "https";
            var h;
            var d;
            var _;
            var g;
            var y;
            var m;
            var p;
            var b;
            for (var x = 0, M = e; x < M.length; x++) {
                var C = M[x];
                _ = C.path;
                g = C.rank;
                y = C.minzoom || n;
                m = C.maxzoom || o;
                p = C.shield;
                b = C.shieldType;
                var S = labelsUtil$2.tQ(C, v);
                h = S.name;
                d = S.$G;
                if (h) {
                    this.Ws(h, d)
                }
                if (!p) {
                    continue
                }
                var w = Math.floor(_.length / 2);
                if (w < 1) {
                    continue
                }
                var k = [_[w - 1], _[w]];
                var T = 40001;
                var A = b;
                var I = this.oA(T, A, this.WW);
                if (!I) {
                    continue
                }
                var $ = l > 1 || I.fontSize > 9 ? I.fontSize : 9;
                var F = I.faceColor;
                var P = I.iconID;
                var L = {
                    data: {
                        id: Util$3.stamp(this),
                        position: k,
                        positionType: "relative",
                        name: h,
                        txt: p,
                        rank: g,
                        BG: S.type,
                        extData: {
                            type: "shield",
                            Lp: T,
                            Ap: A
                        }
                    },
                    opts: {
                        type: "billboard",
                        angle: 0,
                        zooms: [y - f, m + s],
                        visible: true
                    }
                };
                var R = [0, 0];
                var O = "center";
                var B = this.aA(parseInt(P, 10));
                var E = p.length / 4 > 1 ? p.length / 4 : 1;
                E = l > 1 ? E : E * 9 / 7;
                var D = c + i["0"];
                var N = {
                    type: "image",
                    image: D,
                    clipOrigin: B,
                    clipSize: a,
                    size: [24 * E, 24],
                    anchor: "center",
                    angle: 0,
                    qq: true
                };
                var U = p ? {
                    direction: O,
                    offset: R,
                    style: {
                        fontSize: $,
                        fillColor: F && Color$1.normalize(F.rgba),
                        strokeWidth: 0,
                        padding: [0, 1, 0, 1]
                    }
                } : {};
                if (L.opts) {
                    L.opts.text = U;
                    L.opts.icon = [N]
                }
                if (p) {
                    this.Ws(p)
                }
                u.push(L)
            }
            return u
        }
        ;
        e.prototype.DQ = function(e, r) {
            var t;
            var i = CONSTS.Mp
              , a = CONSTS.Rp
              , n = CONSTS.Pp
              , o = CONSTS.jp;
            var f = this.WW = r.zoom;
            var s;
            var u;
            var l;
            var v;
            var c = 0;
            var h;
            var d;
            var _;
            var g;
            var y;
            var m;
            var p;
            var b;
            var x;
            var M;
            var C;
            var S;
            var w = [];
            var k = this.lang;
            var T;
            for (var A = 0, I = e; A < I.length; A++) {
                var $ = I[A];
                T = $.id;
                _ = labelsUtil$2.tQ($, k);
                s = _.name;
                g = _.$G || [];
                if (!s) {
                    continue
                }
                y = _.type;
                m = labelsUtil$2.EQ(s, g);
                p = labelsUtil$2.aQ(m);
                u = $.rank;
                l = $.Fp;
                v = $.Ep;
                c = $.distance || 0;
                h = $.minzoom || i;
                d = $.maxzoom || a;
                b = this.vn.dn(l, v, f);
                x = b && b[1] || {};
                M = x.fontSize;
                C = x.faceColor;
                S = x.borderColor;
                t = labelsUtil$2.$p([h, d]),
                h = t[0],
                d = t[1];
                d = d >= 20 ? zoomRange[1] : d;
                if (s && T && M && C) {
                    var F = {
                        id: T,
                        positionType: "relative",
                        name: s,
                        $G: g,
                        CO: p,
                        rank: u,
                        BG: y,
                        distance: c,
                        zooms: [h - n, d + o],
                        path: $.path.length > 0 ? $.path : undefined,
                        style: {
                            fontSize: M,
                            fillColor: vector$3.Up(C.rgba, 255),
                            strokeColor: S && vector$3.Up(S.rgba, 255)
                        },
                        extData: {
                            mainkey: l,
                            subkey: v
                        }
                    };
                    w.push(F)
                }
            }
            return w
        }
        ;
        e.prototype.aA = function(e) {
            var r = CONSTS.Lj
              , t = r.Rj
              , i = t
              , a = r.size;
            var n = Math.floor(e / i);
            if (e % 10 === 0) {
                n = n - 1
            }
            var o = e - i * n - 1;
            var f = [o * a[0], n * a[1]];
            return f
        }
        ;
        e.prototype.oA = function(e, r, t) {
            var i = [3, 20];
            var a = this.vn.dn(e, r, t);
            var n = this.vn.mn(e, r);
            if (!a) {
                var o = 1;
                while (t - o >= i[0] || t + o <= i[1]) {
                    a = this.vn.dn(e, r, t - o);
                    if (!a) {
                        a = this.vn.dn(e, r, t + o)
                    }
                    if (a) {
                        break
                    } else {
                        o++
                    }
                }
            }
            if (a) {
                a.zooms = n
            }
            return a
        }
        ;
        e.prototype.mN = function(e, r) {
            var t = [0, 0];
            if (e === "0") {
                switch (r) {
                case "3":
                    {
                        t = [6, 6];
                        break
                    }
                case "4":
                case "5":
                case "6":
                    {
                        t = [9, 9];
                        break
                    }
                }
            }
            return t
        }
        ;
        e.prototype.TN = function(e, r, t) {
            if (t === void 0) {
                t = [0, 0]
            }
            var i = t;
            if (e === "0") {
                switch (r) {
                case "3":
                    {
                        i = [30, 30];
                        break
                    }
                case "4":
                case "5":
                case "6":
                    {
                        i = [20, 20];
                        break
                    }
                }
            }
            return i
        }
        ;
        e.id = StaticSourceID.nebulaLabel;
        return e
    }(LabelFormat);
    function quickselect(e, r, t, i, a) {
        quickselectStep(e, r, t || 0, i || e.length - 1, a || defaultCompare)
    }
    function quickselectStep(e, r, t, i, a) {
        while (i > t) {
            if (i - t > 600) {
                var n = i - t + 1;
                var o = r - t + 1;
                var f = Math.log(n);
                var s = .5 * Math.exp(2 * f / 3);
                var u = .5 * Math.sqrt(f * s * (n - s) / n) * (o - n / 2 < 0 ? -1 : 1);
                var l = Math.max(t, Math.floor(r - o * s / n + u));
                var v = Math.min(i, Math.floor(r + (n - o) * s / n + u));
                quickselectStep(e, r, l, v, a)
            }
            var c = e[r];
            var h = t;
            var d = i;
            swap(e, t, r);
            if (a(e[i], c) > 0)
                swap(e, t, i);
            while (h < d) {
                swap(e, h, d);
                h++;
                d--;
                while (a(e[h], c) < 0)
                    h++;
                while (a(e[d], c) > 0)
                    d--
            }
            if (a(e[t], c) === 0)
                swap(e, t, d);
            else {
                d++;
                swap(e, d, i)
            }
            if (d <= r)
                t = d + 1;
            if (r <= d)
                i = d - 1
        }
    }
    function swap(e, r, t) {
        var i = e[r];
        e[r] = e[t];
        e[t] = i
    }
    function defaultCompare(e, r) {
        return e < r ? -1 : e > r ? 1 : 0
    }
    var quickselect$1 = Object.freeze({
        __proto__: null,
        default: quickselect
    });
    var quickselect$2 = getCjsExportFromNamespace(quickselect$1);
    var _rbush_2_0_2_rbush = rbush$1;
    var default_1 = rbush$1;
    function rbush$1(e, r) {
        if (!(this instanceof rbush$1))
            return new rbush$1(e,r);
        this.Bc = Math.max(4, e || 9);
        this.Wc = Math.max(2, Math.ceil(this.Bc * .4));
        if (r) {
            this.Gc(r)
        }
        this.clear()
    }
    rbush$1.prototype = {
        all: function() {
            return this.qc(this.data, [])
        },
        search: function(e) {
            var r = this.data
              , t = []
              , i = this.Yc;
            if (!intersects(e, r))
                return t;
            var a = [], n, o, f, s;
            while (r) {
                for (n = 0,
                o = r.children.length; n < o; n++) {
                    f = r.children[n];
                    s = r.Zc ? i(f) : f;
                    if (intersects(e, s)) {
                        if (r.Zc)
                            t.push(f);
                        else if (contains(e, s))
                            this.qc(f, t);
                        else
                            a.push(f)
                    }
                }
                r = a.pop()
            }
            return t
        },
        Vc: function(e) {
            var r = this.data
              , t = this.Yc;
            if (!intersects(e, r))
                return false;
            var i = [], a, n, o, f;
            while (r) {
                for (a = 0,
                n = r.children.length; a < n; a++) {
                    o = r.children[a];
                    f = r.Zc ? t(o) : o;
                    if (intersects(e, f)) {
                        if (r.Zc || contains(e, f))
                            return true;
                        i.push(o)
                    }
                }
                r = i.pop()
            }
            return false
        },
        load: function(e) {
            if (!(e && e.length))
                return this;
            if (e.length < this.Wc) {
                for (var r = 0, t = e.length; r < t; r++) {
                    this.Xc(e[r])
                }
                return this
            }
            var i = this.Hc(e.slice(), 0, e.length - 1, 0);
            if (!this.data.children.length) {
                this.data = i
            } else if (this.data.height === i.height) {
                this.Jc(this.data, i)
            } else {
                if (this.data.height < i.height) {
                    var a = this.data;
                    this.data = i;
                    i = a
                }
                this.Kc(i, this.data.height - i.height - 1, true)
            }
            return this
        },
        Xc: function(e) {
            if (e)
                this.Kc(e, this.data.height - 1);
            return this
        },
        clear: function() {
            this.data = createNode([]);
            return this
        },
        remove: function(e, r) {
            if (!e)
                return this;
            var t = this.data, i = this.Yc(e), a = [], n = [], o, f, s, u;
            while (t || a.length) {
                if (!t) {
                    t = a.pop();
                    f = a[a.length - 1];
                    o = n.pop();
                    u = true
                }
                if (t.Zc) {
                    s = findItem(e, t.children, r);
                    if (s !== -1) {
                        t.children.splice(s, 1);
                        a.push(t);
                        this.Qc(a);
                        return this
                    }
                }
                if (!u && !t.Zc && contains(t, i)) {
                    a.push(t);
                    n.push(o);
                    o = 0;
                    f = t;
                    t = t.children[0]
                } else if (f) {
                    o++;
                    t = f.children[o];
                    u = false
                } else
                    t = null
            }
            return this
        },
        Yc: function(e) {
            return e
        },
        tf: compareNodeMinX,
        if: compareNodeMinY,
        toJSON: function() {
            return this.data
        },
        ef: function(e) {
            this.data = e;
            return this
        },
        qc: function(e, r) {
            var t = [];
            while (e) {
                if (e.Zc)
                    r.push.apply(r, e.children);
                else
                    t.push.apply(t, e.children);
                e = t.pop()
            }
            return r
        },
        Hc: function(e, r, t, i) {
            var a = t - r + 1, n = this.Bc, o;
            if (a <= n) {
                o = createNode(e.slice(r, t + 1));
                calcBBox(o, this.Yc);
                return o
            }
            if (!i) {
                i = Math.ceil(Math.log(a) / Math.log(n));
                n = Math.ceil(a / Math.pow(n, i - 1))
            }
            o = createNode([]);
            o.Zc = false;
            o.height = i;
            var f = Math.ceil(a / n), s = f * Math.ceil(Math.sqrt(n)), u, l, v, c;
            multiSelect(e, r, t, s, this.tf);
            for (u = r; u <= t; u += s) {
                v = Math.min(u + s - 1, t);
                multiSelect(e, u, v, f, this.if);
                for (l = u; l <= v; l += f) {
                    c = Math.min(l + f - 1, v);
                    o.children.push(this.Hc(e, l, c, i - 1))
                }
            }
            calcBBox(o, this.Yc);
            return o
        },
        nf: function(e, r, t, i) {
            var a, n, o, f, s, u, l, v;
            while (true) {
                i.push(r);
                if (r.Zc || i.length - 1 === t)
                    break;
                l = v = Infinity;
                for (a = 0,
                n = r.children.length; a < n; a++) {
                    o = r.children[a];
                    s = bboxArea(o);
                    u = enlargedArea(e, o) - s;
                    if (u < v) {
                        v = u;
                        l = s < l ? s : l;
                        f = o
                    } else if (u === v) {
                        if (s < l) {
                            l = s;
                            f = o
                        }
                    }
                }
                r = f || r.children[0]
            }
            return r
        },
        Kc: function(e, r, t) {
            var i = this.Yc
              , a = t ? e : i(e)
              , n = [];
            var o = this.nf(a, this.data, r, n);
            o.children.push(e);
            extend$1(o, a);
            while (r >= 0) {
                if (n[r].children.length > this.Bc) {
                    this.af(n, r);
                    r--
                } else
                    break
            }
            this.sf(a, n, r)
        },
        af: function(e, r) {
            var t = e[r]
              , i = t.children.length
              , a = this.Wc;
            this.rf(t, a, i);
            var n = this.hf(t, a, i);
            var o = createNode(t.children.splice(n, t.children.length - n));
            o.height = t.height;
            o.Zc = t.Zc;
            calcBBox(t, this.Yc);
            calcBBox(o, this.Yc);
            if (r)
                e[r - 1].children.push(o);
            else
                this.Jc(t, o)
        },
        Jc: function(e, r) {
            this.data = createNode([e, r]);
            this.data.height = e.height + 1;
            this.data.Zc = false;
            calcBBox(this.data, this.Yc)
        },
        hf: function(e, r, t) {
            var i, a, n, o, f, s, u, l;
            s = u = Infinity;
            for (i = r; i <= t - r; i++) {
                a = distBBox(e, 0, i, this.Yc);
                n = distBBox(e, i, t, this.Yc);
                o = intersectionArea(a, n);
                f = bboxArea(a) + bboxArea(n);
                if (o < s) {
                    s = o;
                    l = i;
                    u = f < u ? f : u
                } else if (o === s) {
                    if (f < u) {
                        u = f;
                        l = i
                    }
                }
            }
            return l
        },
        rf: function(e, r, t) {
            var i = e.Zc ? this.tf : compareNodeMinX
              , a = e.Zc ? this.if : compareNodeMinY
              , n = this.lf(e, r, t, i)
              , o = this.lf(e, r, t, a);
            if (n < o)
                e.children.sort(i)
        },
        lf: function(e, r, t, i) {
            e.children.sort(i);
            var a = this.Yc, n = distBBox(e, 0, r, a), o = distBBox(e, t - r, t, a), f = bboxMargin(n) + bboxMargin(o), s, u;
            for (s = r; s < t - r; s++) {
                u = e.children[s];
                extend$1(n, e.Zc ? a(u) : u);
                f += bboxMargin(n)
            }
            for (s = t - r - 1; s >= r; s--) {
                u = e.children[s];
                extend$1(o, e.Zc ? a(u) : u);
                f += bboxMargin(o)
            }
            return f
        },
        sf: function(e, r, t) {
            for (var i = t; i >= 0; i--) {
                extend$1(r[i], e)
            }
        },
        Qc: function(e) {
            for (var r = e.length - 1, t; r >= 0; r--) {
                if (e[r].children.length === 0) {
                    if (r > 0) {
                        t = e[r - 1].children;
                        t.splice(t.indexOf(e[r]), 1)
                    } else
                        this.clear()
                } else
                    calcBBox(e[r], this.Yc)
            }
        },
        Gc: function(e) {
            var r = ["return a", " - b", ";"];
            this.tf = new Function("a","b",r.join(e[0]));
            this.if = new Function("a","b",r.join(e[1]));
            this.Yc = new Function("a","return {minX: a" + e[0] + ", minY: a" + e[1] + ", maxX: a" + e[2] + ", maxY: a" + e[3] + "};")
        }
    };
    function findItem(e, r, t) {
        if (!t)
            return r.indexOf(e);
        for (var i = 0; i < r.length; i++) {
            if (t(e, r[i]))
                return i
        }
        return -1
    }
    function calcBBox(e, r) {
        distBBox(e, 0, e.children.length, r, e)
    }
    function distBBox(e, r, t, i, a) {
        if (!a)
            a = createNode(null);
        a.ja = Infinity;
        a.Fa = Infinity;
        a.Oa = -Infinity;
        a.Ea = -Infinity;
        for (var n = r, o; n < t; n++) {
            o = e.children[n];
            extend$1(a, e.Zc ? i(o) : o)
        }
        return a
    }
    function extend$1(e, r) {
        e.ja = Math.min(e.ja, r.ja);
        e.Fa = Math.min(e.Fa, r.Fa);
        e.Oa = Math.max(e.Oa, r.Oa);
        e.Ea = Math.max(e.Ea, r.Ea);
        return e
    }
    function compareNodeMinX(e, r) {
        return e.ja - r.ja
    }
    function compareNodeMinY(e, r) {
        return e.Fa - r.Fa
    }
    function bboxArea(e) {
        return (e.Oa - e.ja) * (e.Ea - e.Fa)
    }
    function bboxMargin(e) {
        return e.Oa - e.ja + (e.Ea - e.Fa)
    }
    function enlargedArea(e, r) {
        return (Math.max(r.Oa, e.Oa) - Math.min(r.ja, e.ja)) * (Math.max(r.Ea, e.Ea) - Math.min(r.Fa, e.Fa))
    }
    function intersectionArea(e, r) {
        var t = Math.max(e.ja, r.ja)
          , i = Math.max(e.Fa, r.Fa)
          , a = Math.min(e.Oa, r.Oa)
          , n = Math.min(e.Ea, r.Ea);
        return Math.max(0, a - t) * Math.max(0, n - i)
    }
    function contains(e, r) {
        return e.ja <= r.ja && e.Fa <= r.Fa && r.Oa <= e.Oa && r.Ea <= e.Ea
    }
    function intersects(e, r) {
        return r.ja <= e.Oa && r.Fa <= e.Ea && r.Oa >= e.ja && r.Ea >= e.Fa
    }
    function createNode(e) {
        return {
            children: e,
            height: 1,
            Zc: true,
            ja: Infinity,
            Fa: Infinity,
            Oa: -Infinity,
            Ea: -Infinity
        }
    }
    function multiSelect(e, r, t, i, a) {
        var n = [r, t], o;
        while (n.length) {
            t = n.pop();
            r = n.pop();
            if (t - r <= i)
                continue;
            o = r + Math.ceil((t - r) / i / 2) * i;
            quickselect$2(e, o, r, t, a);
            n.push(r, o, o, t)
        }
    }
    _rbush_2_0_2_rbush.default = default_1;
    var Status;
    (function(e) {
        e["loading"] = "loading";
        e["loaded"] = "loaded"
    }
    )(Status || (Status = {}));
    var M$1;
    M$1 = getModule();
    function getModule() {
        if (!M$1) {
            M$1 = {
                nS: function(clsName, clsText) {
                    try {
                        eval(clsText)
                    } catch (e) {
                        throw e
                    }
                },
                eS: {
                    iS: "__detaiVersion__",
                    BY: Browser.DW ? false : config[13]
                }
            }
        }
        return M$1
    }
    var Module = getModule();
    function InnerClass(r) {
        return function(e) {
            getModule()[r] = e
        }
    }
    getModule()["MapboxLabelFormat"] = MapboxLabelFormat$1;
    getModule()["NebulaLabelFormat"] = NebulaLabelFormat$1;
    getModule()["_LocalZoom"] = {
        Ra: 13,
        za: 12,
        Ge: ""
    };
    getModule()["Support"] = Browser;
    getModule()["geo"] = {
        lcs: lcs$2,
        ProjectionManager: ProjectionManager
    };
    getModule()["transform"] = transform;
    getModule()["Util"] = Util$3;
    getModule()["TileState"] = TileState$1;
    getModule()["assign"] = assign;
    getModule()["labelsUtil"] = labelsUtil$2;
    getModule()["LabelsRenderUtil"] = LabelsRenderUtil$3;
    getModule()["vector"] = vector$3;
    getModule()["rbush"] = _rbush_2_0_2_rbush;
    getModule()["lodash"] = {
        map: map,
        find: find,
        assign: assign,
        every: every,
        some: some
    };
    var TileState$1;
    (function(e) {
        e["TOLOAD"] = "toload";
        e["LOADING"] = "loading";
        e["LOADED"] = "loaded";
        e["UNLOADED"] = "unloaded";
        e["ERROR"] = "error";
        e["RELOADING"] = "reloading"
    }
    )(TileState$1 || (TileState$1 = {}));
    var TileCoord = function() {
        function e(e, r, t) {
            this.x = r;
            this.y = t;
            this.z = e;
            this.key = e + "," + r + "," + t
        }
        a = e;
        e.prototype.s0 = function(e) {
            var r = e - this.z;
            var t = Math.floor(Math.pow(2, r) * this.x);
            var i = Math.floor(Math.pow(2, r) * this.y);
            return new a(e,t,i)
        }
        ;
        var a;
        e = a = __decorate([InnerClass("TileCoord")], e);
        return e
    }();
    var Tile = function() {
        function e(e) {
            this.type = "tile";
            this.Ro = [0, 0, 0, 0];
            this.stamp = 0;
            this.zo = e;
            this.status = TileState$1.TOLOAD;
            this._v = null;
            this.data = {}
        }
        e.YH = function(e) {
            return e.status === TileState$1.LOADED || e.status === TileState$1.RELOADING
        }
        ;
        e.prototype.destroy = function() {
            assert(true, "Tile should not implement destroy")
        }
        ;
        e.prototype.nG = function(e) {
            this.type = e
        }
        ;
        e = __decorate([InnerClass("Tile")], e);
        return e
    }();
    var NebulaTile = function(r) {
        __extends(e, r);
        function e() {
            var e = r !== null && r.apply(this, arguments) || this;
            e.type = "nebula";
            e.region = "world";
            e.aN = NebulaTagType.NONE;
            e.BN = NebulaTagType.NONE;
            return e
        }
        Object.defineProperty(e.prototype, "tag", {
            get: function() {
                return this.nN
            },
            set: function(e) {
                this.aN = this.tag;
                this.nN = e
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(e.prototype, "gB", {
            get: function() {
                return this.VN
            },
            set: function(e) {
                this.BN = this.gB;
                this.VN = e
            },
            enumerable: false,
            configurable: true
        });
        Object.defineProperty(e.prototype, "data", {
            get: function() {
                var e = [];
                if (this.qc && this.qc.Dc) {
                    e.push(this.qc)
                } else {
                    if (this.sN) {
                        e.push(this.sN)
                    }
                    if (this.uN) {
                        e.push(this.uN)
                    }
                }
                if (this.lU && this.lU.Dc) {
                    e.push(this.lU)
                } else {
                    if (this.fU) {
                        e.push(this.fU)
                    }
                    if (this.cU) {
                        e.push(this.cU)
                    }
                }
                if (this.zN && this.zN.labels) {
                    e.push(this.zN)
                } else {
                    if (this.HN) {
                        e.push(this.HN)
                    }
                    if (this.WN) {
                        e.push(this.WN)
                    }
                }
                if (this.dU && this.dU.labels) {
                    e.push(this.dU)
                } else {
                    if (this.vU) {
                        e.push(this.vU)
                    }
                    if (this.gU) {
                        e.push(this.gU)
                    }
                }
                return e
            },
            set: function(e) {},
            enumerable: false,
            configurable: true
        });
        e.prototype.xa = function(e, r) {
            if (r === void 0) {
                r = NebulaTagType.ALL
            }
            if (!Object.keys(e)) {
                return
            }
            switch (r) {
            case NebulaTagType.LITE:
                {
                    this.uN = __assign(__assign({}, this.uN), e);
                    break
                }
            case NebulaTagType.LEFT:
                {
                    this.sN = __assign(__assign({}, this.sN), e);
                    break
                }
            case NebulaTagType.ALL:
            default:
                {
                    this.qc = __assign(__assign({}, this.qc), e)
                }
            }
        }
        ;
        e.prototype.JE = function(e, r) {
            if (r === void 0) {
                r = NebulaTagType.ALL
            }
            if (!Object.keys(e)) {
                return
            }
            switch (r) {
            case NebulaTagType.LITE:
                {
                    this.cU = __assign(__assign({}, this.cU), e);
                    break
                }
            case NebulaTagType.LEFT:
                {
                    this.fU = __assign(__assign({}, this.fU), e);
                    break
                }
            case NebulaTagType.ALL:
            default:
                {
                    this.lU = __assign(__assign({}, this.lU), e)
                }
            }
        }
        ;
        e.prototype.rO = function(e, r) {
            if (r === void 0) {
                r = NebulaTagType.ALL
            }
            if (!Object.keys(e)) {
                return
            }
            switch (r) {
            case NebulaTagType.LITE:
                {
                    this.WN = __assign(__assign({}, this.WN), e);
                    break
                }
            case NebulaTagType.LEFT:
                {
                    this.HN = __assign(__assign({}, this.HN), e);
                    break
                }
            case NebulaTagType.ALL:
            default:
                {
                    this.zN = __assign(__assign({}, this.zN), e)
                }
            }
        }
        ;
        e.prototype.qE = function(e, r) {
            if (r === void 0) {
                r = NebulaTagType.ALL
            }
            if (!Object.keys(e)) {
                return
            }
            switch (r) {
            case NebulaTagType.LITE:
                {
                    this.gU = __assign(__assign({}, this.gU), e);
                    break
                }
            case NebulaTagType.LEFT:
                {
                    this.vU = __assign(__assign({}, this.vU), e);
                    break
                }
            case NebulaTagType.ALL:
            default:
                {
                    this.dU = __assign(__assign({}, this.dU), e)
                }
            }
        }
        ;
        e.prototype.ID = function(e) {
            if (e) {
                if (e.uN) {
                    this.uN = e.uN
                }
                if (e.sN) {
                    this.sN = e.sN
                }
                if (e.qc) {
                    this.qc = e.qc
                }
                if (e.WN) {
                    this.WN = e.WN
                }
                if (e.HN) {
                    this.HN = e.HN
                }
                if (e.zN) {
                    this.zN = e.zN
                }
            }
        }
        ;
        e.prototype.rN = function() {
            if (this.aN) {
                this.nN = this.aN
            } else {
                this.nN = NebulaTagType.NONE
            }
            if (this.BN) {
                this.VN = this.BN;
                this.nN = NebulaTagType.NONE
            }
        }
        ;
        e.prototype.AB = function() {
            var e = [];
            if (this.qc && this.qc.road && this.zN && this.zN.labels) {
                e.push(this.qc)
            } else {
                if (this.sN && this.sN.road && this.HN && this.HN.labels) {
                    e.push(this.sN)
                }
                if (this.uN && this.uN.road && this.WN && this.WN.labels) {
                    e.push(this.uN)
                }
            }
            return e
        }
        ;
        e.prototype.LY = function(e, r) {
            var t = this.data;
            for (var i = 0, a = t; i < a.length; i++) {
                var n = a[i];
                if (n.Dc) {
                    n.Dc.upload(e, true)
                }
                if (n.$c) {
                    n.$c.upload(e, true)
                }
                if (n.A_) {
                    n.A_.upload(e, true)
                }
                if (n.FD) {
                    n.FD.upload(e, true)
                }
            }
        }
        ;
        e.prototype.destroy = function() {
            var e = this.data;
            if (!e) {
                return
            }
            for (var r = 0, t = e; r < t.length; r++) {
                var i = t[r];
                if (i.Dc) {
                    i.Dc.destroy();
                    delete i.Dc
                }
                if (i.$c) {
                    i.$c.destroy();
                    delete i.$c
                }
                if (i.labels) {
                    for (var a = 0, n = i.labels; a < n.length; a++) {
                        var o = n[a];
                        o.destroy()
                    }
                    delete i.labels
                }
                i.Ta = false;
                delete i.gf
            }
        }
        ;
        e.prototype.KE = function(e) {
            var r = this.data;
            var t;
            var i = [];
            for (var a = 0, n = e; a < n.length; a++) {
                var o = n[a];
                if (o.road) {
                    i.push(o.road)
                }
            }
            for (var f = 0, s = r; f < s.length; f++) {
                var o = s[f];
                if (o.XE) {
                    t = o.XE;
                    this.pU(i, t)
                }
            }
        }
        ;
        e.prototype.reload = function() {
            delete this.lU;
            delete this.cU;
            delete this.fU;
            delete this.zN;
            delete this.WN;
            delete this.HN;
            delete this.VN;
            this.BN = NebulaTagType.NONE
        }
        ;
        e.prototype.pU = function(e, r) {
            for (var t = 0, i = r; t < i.length; t++) {
                var a = i[t];
                var n = a.id;
                if (!a.path) {
                    for (var o = 0, f = e; o < f.length; o++) {
                        var s = f[o];
                        a.path = this.yU(s, n)
                    }
                } else {
                    break
                }
            }
        }
        ;
        e.prototype.yU = function(e, r) {
            for (var t = 0, i = e; t < i.length; t++) {
                var a = i[t];
                if (a.id === r) {
                    return a.path
                }
            }
        }
        ;
        e.prototype.oN = function() {
            var e = false;
            var r = this.qc;
            var t = Object.keys(r || {});
            if (!t.length) {
                e = false
            } else if (t.length === 1 && t[0] === "sdfLoaded") {
                e = false
            } else {
                e = true
            }
            return e
        }
        ;
        e = __decorate([InnerClass("NebulaTile")], e);
        return e
    }(Tile);
    var RasterTile = function(r) {
        __extends(e, r);
        function e() {
            var e = r !== null && r.apply(this, arguments) || this;
            e.type = "raster";
            return e
        }
        e.prototype.destroy = function() {
            var e = this.data;
            if (!e) {
                return
            }
            if (e.texture) {
                e.texture.destroy();
                delete e.texture
            }
            if (e.zs) {
                e.zs.context.deleteBuffer(e.zs);
                delete e.zs
            }
            if (e.Js) {
                e.Js.destroy();
                delete e.Js
            }
        }
        ;
        return e
    }(Tile);
    var VectorTile = function(r) {
        __extends(e, r);
        function e() {
            var e = r !== null && r.apply(this, arguments) || this;
            e.type = "vector";
            return e
        }
        e.prototype.destroy = function() {
            var e = this.data;
            if (!e) {
                return
            }
            if (e.$c) {
                e.$c.destroy();
                delete e.$c
            }
            if (e.Uc) {
                delete e.Uc
            }
        }
        ;
        e = __decorate([InnerClass("VectorTile")], e);
        return e
    }(Tile);
    var Support$3 = M["Support"];
    var scale$2 = Support$3.scale;
    var MapboxVTRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            this.vdt = e;
            var a = t.viewState.optimalZoom;
            if (a < i.zooms[0] || a > i.zooms[1]) {
                return
            }
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            var n = r.tiles;
            n.sort(function(e, r) {
                return r.zo.z - e.zo.z
            });
            var o = i.layer["getLabelsLayer"]();
            var f = n.every(function(e) {
                return o.Ho.indexOf(e.zo.key) > -1
            });
            if (!f) {
                o.Ho = [];
                o["remove"](o["getAllOverlays"]())
            }
            if (n && n.length) {
                e.context.clear({
                    stencil: true
                });
                for (var s = 0, u = n; s < u.length; s++) {
                    var l = u[s];
                    if (l.status !== TileState$1.LOADED) {
                        continue
                    }
                    if (!f) {
                        this.pee(o, l.data.labels, l)
                    }
                    this.lH(e, l, t, 1);
                    this.Lc(e, l, t, i);
                    this.Ac(e, l, t, i);
                    this.Ic(e, l, t, i);
                    this.Cb(e, l, t, i);
                    this.lH(e, l, t, 2)
                }
                for (var v = 0, c = n; v < c.length; v++) {
                    var l = c[v];
                    if (l.status !== TileState$1.LOADED) {
                        continue
                    }
                    this.S_(e, l, t, i, true)
                }
                for (var h = 0, d = n; h < d.length; h++) {
                    var l = d[h];
                    if (l.status !== TileState$1.LOADED) {
                        continue
                    }
                    this.S_(e, l, t, i)
                }
            }
            var _ = t.map.getMask();
            this.PB(e, t, i, _)
        }
        ;
        e.prototype.pickRender = function(e, r, t) {
            var i = this.vdt;
            if (!i) {
                return
            }
            var a = i.context;
            var n = i.context.gl;
            var o = e.size[0] * scale$2;
            var f = e.size[1] * scale$2;
            var s = e.viewState.bounds.toString();
            var u = false;
            if (!this.bee) {
                this.bee = this.IC(n, o, f, s);
                u = true
            } else if (o !== this.bee.w || f !== this.bee.h) {
                this.xee(this.bee, n, o, f, s);
                u = true
            } else if (s !== this.bee.bounds) {
                this.bee.bounds = s;
                u = true
            }
            if (u) {
                a.bindFramebuffer.set(this.bee);
                a.be.set([0, 0, o, f]);
                a.clear({
                    color: true,
                    stencil: true,
                    depth: true
                });
                for (var l = 0, v = t; l < v.length; l++) {
                    var c = v[l];
                    if (c.status !== TileState$1.LOADED) {
                        continue
                    }
                    this.S_(i, c, e, r, false, true)
                }
                a.bindFramebuffer.set(null);
                a.setDirty()
            }
            return this.bee
        }
        ;
        e.prototype.destroy = function() {
            this["clearPickFbo"]()
        }
        ;
        e.prototype["clearPickFbo"] = function() {
            if (this.bee && this.vdt) {
                var e = this.vdt.context.gl;
                e.deleteTexture(this.bee.texture);
                e.deleteRenderbuffer(this.bee.AC);
                e.deleteFramebuffer(this.bee);
                delete this.bee
            }
        }
        ;
        e.prototype.Lc = function(e, r, t, i) {
            var a = e.Wn();
            var n = r.data;
            var o = n.Dc;
            if (!o) {
                return
            }
            o.upload(a.context, true);
            var f = o.zs;
            var s = o.Pc;
            var u = n.Oc;
            var l = i.opacity;
            var v = [r.Sa[0], r.Sa[1]];
            var c = 1;
            if (t.viewState.viewMode === "3D") {
                c = t.map.getView().EF()
            }
            var h = r.zo.z;
            if (h < LocalZoom) {
                var d = r.localCoord.center;
                v[0] += d[0];
                v[1] += d[1]
            }
            for (var _ = 0, g = u.jc; _ < g.length; _++) {
                var y = g[_];
                var m = y.mainkey.split(":")[0];
                m = m.split(",");
                m[3] *= l;
                var p = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_color: m,
                    u_offset: [0, 0],
                    u_localDeltaCenter: v,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: c
                };
                if (p) {
                    a.context.getExtension("OES_element_index_uint");
                    a.Ce(p, {
                        a_pos: {
                            type: "vec2",
                            Re: f.size,
                            offset: 0,
                            buffer: f
                        }
                    }, y.length, s, "TRIANGLES", i.depthTest, ColorMode.Si, StencilMode.iee, undefined, y.offset)
                }
            }
        }
        ;
        e.prototype.Ac = function(e, r, t, i) {
            var a = e.Vn();
            if (!r.data) {
                assert(r.data, "MapboxVT tile have no data");
                return
            }
            var n = r.data;
            var o = n.pX;
            if (!o) {
                return
            }
            o.upload(a.context, true);
            var f = o.zs.buffer;
            var s = o.Pc.buffer;
            var u = n.Fc;
            if (!u) {
                return
            }
            var l = u.jc;
            var v = [r.Sa[0], r.Sa[1]];
            var c = r.zo.z;
            if (c < LocalZoom) {
                var h = r.localCoord.center;
                v[0] += h[0];
                v[1] += h[1]
            }
            var d = i.opacity;
            var _ = 1;
            if (t.viewState.viewMode === "3D") {
                _ = t.map.getView().EF()
            }
            for (var g = 0, y = l.length; g < y; g++) {
                var m = l[g];
                var p = m.mainkey.split(":");
                var b = p[0].split(",");
                b[3] *= d;
                var x = p[1].split(",");
                var M = +p[2];
                var C = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_color: b,
                    u_border: 0,
                    u_width: M,
                    u_dash: x,
                    u_localDeltaCenter: v,
                    u_skyHeight: _,
                    u_viewHeight: t.viewState.size[1]
                };
                var S = f.size;
                a.context.getExtension("OES_element_index_uint");
                a.Ce(C, {
                    a_pos: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: f,
                        Re: S,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 5
                    }
                }, m.length, s, "TRIANGLES", i.depthTest, ColorMode.Si, StencilMode.iee, undefined, m.offset)
            }
        }
        ;
        e.prototype.S_ = function(e, r, t, i, a, n) {
            if (a === void 0) {
                a = false
            }
            if (n === void 0) {
                n = false
            }
            var o = r.data;
            var f = o.A_;
            if (!f) {
                return
            }
            f.upload(e.context, true);
            var s = t.map;
            var u = s.getView();
            var l = t.viewState.size;
            var v = 1;
            if (t.viewState.viewMode === "3D") {
                v = u.EF()
            } else {
                return
            }
            var c = u.X(l[0] / 2, u.Uu() + 5);
            var h = u.X(l[0] / 2, u.Uu() - 5);
            var d = u.pz().getPosition();
            var _ = c[0] - d[0];
            var g = c[1] - d[1];
            var y = h[0] - d[0];
            var m = h[1] - d[1];
            var p = [Math.sqrt(_ * _ + g * g), Math.sqrt(y * y + m * m)];
            var b = [r.Sa[0], r.Sa[1]];
            var x = r.zo.z;
            if (x < LocalZoom) {
                var M = r.localCoord.center;
                b[0] += M[0];
                b[1] += M[1]
            }
            var C = f.zs;
            var S = o.w0;
            for (var w = 0, k = S.jc; w < k.length; w++) {
                var T = k[w];
                var A = T.roofColor;
                var I = T.wallColor;
                var $ = !!T.bz;
                if (!$ && (!A || !I)) {
                    continue
                }
                var F = void 0;
                if (n) {
                    F = i.layer.colorPick.find(T.mainkey);
                    if (!F) {
                        F = i.layer.colorPick.add(T.mainkey)
                    }
                }
                if (n && !F) {
                    continue
                }
                if ($ && !n) {
                    var P = getFillExtPatternUniformValues(t, T, b, i, {
                        wallColor: I,
                        roofColor: A
                    }, v, p);
                    if (P) {
                        var L = s.getImage(P.u_image);
                        if (L) {
                            P.u_image = L;
                            P.u_imageSize = L.size;
                            var R = e.sH();
                            R.Ce(P, {
                                a_pos: {
                                    type: "vec4",
                                    Re: C.size,
                                    offset: 0,
                                    buffer: C
                                },
                                a_normal: {
                                    type: "vec3",
                                    Re: C.size,
                                    offset: 4 * 4,
                                    buffer: C
                                },
                                a_textureCoord: {
                                    type: "vec2",
                                    Re: C.size,
                                    offset: 4 * 7,
                                    buffer: C
                                }
                            }, T.length, undefined, "TRIANGLES", true, a ? ColorMode.disabled : ColorMode.Si, a ? StencilMode.disable : StencilMode.nee, CullFaceMode.back, T.offset)
                        } else if (P.u_image && s.I_ && !s.I_.MH(P.u_image)) {
                            s.addImage(P.u_image, {
                                url: P.u_image,
                                filter: "LINEAR",
                                wrap: "REPEAT",
                                cb: function() {
                                    return s.setNeedUpdate(true)
                                }
                            })
                        }
                    }
                } else {
                    var P = getFillExtUniformValues(t, T, b, i, {
                        wallColor: n ? F : I,
                        roofColor: n ? F : A
                    }, v, p);
                    if (P) {
                        P["u_isPick"] = n;
                        var R = e.k_();
                        R.Ce(P, {
                            a_pos: {
                                type: "vec4",
                                Re: C.size,
                                offset: 0,
                                buffer: C
                            },
                            a_normal: {
                                type: "vec3",
                                Re: C.size,
                                offset: 4 * 4,
                                buffer: C
                            }
                        }, T.length, undefined, "TRIANGLES", true, a ? ColorMode.disabled : ColorMode.Si, a ? StencilMode.disable : StencilMode.nee, CullFaceMode.back, T.offset)
                    }
                }
            }
        }
        ;
        e.prototype.Ic = function(e, r, t, i) {
            var a = e.Vn();
            if (!r.data) {
                assert(r.data, "MapboxVT tile have no data");
                return
            }
            var n = r.data;
            var o = n.$c;
            if (!o) {
                return
            }
            o.upload(a.context, true);
            var f = o.zs.buffer;
            var s = o.Pc.buffer;
            var u = n.Uc;
            if (!u) {
                return
            }
            var l = u.jc;
            var v = [r.Sa[0], r.Sa[1]];
            var c = r.zo.z;
            if (c < LocalZoom) {
                var h = r.localCoord.center;
                v[0] += h[0];
                v[1] += h[1]
            }
            var d = i.opacity;
            var _ = 1;
            if (t.viewState.viewMode === "3D") {
                _ = t.map.getView().EF()
            }
            for (var g = 0, y = l.length; g < y; g++) {
                var m = l[g];
                var p = m.mainkey.split(":")
                  , b = p[0]
                  , x = p[1]
                  , M = p[2];
                b = b.split(",");
                b[3] *= d;
                M = M.split(",");
                x = +x;
                var C = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_color: b,
                    u_border: 0,
                    u_width: x,
                    u_dash: M,
                    u_localDeltaCenter: v,
                    u_skyHeight: _,
                    u_viewHeight: t.viewState.size[1]
                };
                var S = f.size;
                a.context.getExtension("OES_element_index_uint");
                a.Ce(C, {
                    a_pos: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: f,
                        Re: S,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: f,
                        Re: S,
                        offset: 4 * 5
                    }
                }, m.length, s, "TRIANGLES", i.depthTest, ColorMode.Si, StencilMode.iee, undefined, m.offset)
            }
        }
        ;
        e.prototype.Cb = function(e, r, t, i) {
            var a = r.data;
            var n = a.dX;
            if (!n) {
                return
            }
            var o = e.Mee();
            n.upload(o.context);
            var f = n.zs;
            var s = a.IX;
            if (!s) {
                return
            }
            var u = 1;
            if (t.viewState.viewMode === "3D") {
                u = t.map.getView().EF()
            }
            var l = [r.Sa[0], r.Sa[1]];
            var v = r.zo.z;
            if (v < LocalZoom) {
                var c = r.localCoord.center;
                l[0] += c[0];
                l[1] += c[1]
            }
            var h = i.opacity;
            var d = s.jc;
            for (var _ = 0, g = d.length; _ < g; _++) {
                var y = d[_];
                var m = {
                    u_offset: [0, 0],
                    u_matrix: t.viewState.mvpMatrix,
                    u_localDeltaCenter: l,
                    u_skyHeight: u,
                    u_viewHeight: t.viewState.size[1],
                    u_retinaRatio: Support$3.Ue ? 2 : 1,
                    u_opacity: h
                };
                o.Ce(m, {
                    a_pos: {
                        buffer: f,
                        type: "vec2",
                        Re: f.size,
                        offset: 0
                    },
                    a_radius: {
                        buffer: f,
                        type: "float",
                        Re: f.size,
                        offset: 4 * 2
                    },
                    a_borderWidth: {
                        buffer: f,
                        type: "float",
                        Re: f.size,
                        offset: 4 * 3
                    },
                    a_color: {
                        buffer: f,
                        type: "vec4",
                        Re: f.size,
                        offset: 4 * 4
                    },
                    a_borderColor: {
                        buffer: f,
                        type: "vec4",
                        Re: f.size,
                        offset: 4 * 8
                    },
                    a_visible: {
                        buffer: f,
                        type: "float",
                        Re: f.size,
                        offset: 4 * 12
                    }
                }, y.length, undefined, "POINTS", i.depthTest, ColorMode.Si, StencilMode.iee, undefined, y.offset)
            }
        }
        ;
        e.prototype.lH = function(e, r, t, i) {
            if (i === void 0) {
                i = 1
            }
            var a = e.Wn();
            var n = a.context;
            var o = new Float32Array(this.fH(r));
            var f = [r.Sa[0], r.Sa[1]];
            var s = n.fe(o, 8);
            var u = r.zo.z;
            if (u < LocalZoom) {
                var l = r.localCoord.center;
                f[0] += l[0];
                f[1] += l[1]
            }
            var v = [0, 1, 0, 0];
            if (u === t.viewState.optimalZoom - 1) {
                v = [1, 0, 0, 0]
            }
            var c = {
                u_matrix: t.viewState.mvpMatrix,
                u_color: v,
                u_offset: [0, 0],
                u_localDeltaCenter: f,
                u_viewHeight: t.viewState.size[1],
                u_skyHeight: 1
            };
            a.Ce(c, {
                a_pos: {
                    type: "vec2",
                    buffer: s,
                    Re: 0,
                    offset: 0
                }
            }, 6, undefined, "TRIANGLES", false, ColorMode.Si, i === 1 ? StencilMode.ree : StencilMode.aee, undefined, 0)
        }
        ;
        e.prototype.PB = function(e, r, t, i) {
            e.context.clear({
                stencil: true
            });
            if (i) {
                var a = r.map.getLayerByClass("AMap.MaskLayer");
                if (a) {
                    var n = r.uo.getData(a.co(), r.viewState, e.context);
                    var o = a.getRender();
                    if (n && o) {
                        o.renderFrame(e, n, r, t)
                    }
                }
            }
        }
        ;
        e.prototype.pee = function(e, r, t) {
            if (!r) {
                return
            }
            if (e.Ho && e.Ho.indexOf(t.zo.key) > -1) {
                return
            }
            var i = [];
            for (var a = 0; a < r.length; a++) {
                var n = r[a];
                var o = new AMap.LabelMarker(n);
                i.push(o)
            }
            e.add(i);
            e.Ho.push(t.zo.key)
        }
        ;
        e.prototype.IC = function(e, r, t, i) {
            var a = e.createFramebuffer();
            a.w = r;
            a.h = t;
            a.bounds = i;
            var n = e.createTexture();
            a.texture = n;
            e.bindTexture(e.TEXTURE_2D, n);
            e.texImage2D(e.TEXTURE_2D, 0, e.RGBA, r, t, 0, e.RGBA, e.UNSIGNED_BYTE, null);
            var o = e.createRenderbuffer();
            a.AC = o;
            e.bindRenderbuffer(e.RENDERBUFFER, o);
            e.renderbufferStorage(e.RENDERBUFFER, e.DEPTH_COMPONENT16, r, t);
            e.bindFramebuffer(e.FRAMEBUFFER, a);
            e.framebufferTexture2D(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0, e.TEXTURE_2D, n, 0);
            e.framebufferRenderbuffer(e.FRAMEBUFFER, e.DEPTH_ATTACHMENT, e.RENDERBUFFER, o);
            var f = e.checkFramebufferStatus(e.FRAMEBUFFER);
            if (e.FRAMEBUFFER_COMPLETE !== f) {
                console.log("Frame buffer object is incomplete: " + f.toString());
                return
            }
            e.bindFramebuffer(e.FRAMEBUFFER, null);
            e.bindTexture(e.TEXTURE_2D, null);
            e.bindRenderbuffer(e.RENDERBUFFER, null);
            return a
        }
        ;
        e.prototype.xee = function(e, r, t, i, a) {
            e.w = t;
            e.h = i;
            e.bounds = a;
            r.bindTexture(r.TEXTURE_2D, e.texture);
            r.texImage2D(r.TEXTURE_2D, 0, r.RGBA, t, i, 0, r.RGBA, r.UNSIGNED_BYTE, null);
            r.bindRenderbuffer(r.RENDERBUFFER, e.AC);
            r.renderbufferStorage(r.RENDERBUFFER, r.DEPTH_COMPONENT16, t, i);
            r.bindTexture(r.TEXTURE_2D, null);
            r.bindRenderbuffer(r.RENDERBUFFER, null)
        }
        ;
        return e
    }(LayerRender);
    var Support$4 = M["Support"];
    var lcs$3 = M["geo"]["lcs"];
    var MaskLayerRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.SC = new Uint8Array(0);
            return e
        }
        e.prototype.EH = function(e, r) {
            var t = r.map.coordToContainer([e[0], e[1]]);
            var i = Math.round(t[0]);
            var a = Math.round(t[1]);
            var n = r.size[0];
            var o = r.size[1];
            var f = ((o - a) * n + i) * 4 + 3;
            if (i < 0 || a < 0 || i >= n || a >= o) {
                return false
            }
            if (this.SC[f]) {
                return true
            }
            return false
        }
        ;
        e.prototype.renderFrame = function(e, r, t, i, a, n) {
            var o = e.aS();
            var f = t.viewState;
            var s = f.centerCoord;
            var u = o.context;
            var l = [0, 0];
            if (t.viewState.optimalZoom < LocalZoom) {
                l = r.LS.center
            } else {
                var v = lcs$3.getLocalByCoord([s[0], s[1]]).center;
                l = [r.LS.center[0] - v[0], r.LS.center[1] - v[1]]
            }
            r.Dc.upload(o.context, true);
            var c = {
                data: r,
                $i: o,
                viewState: f,
                Sa: l
            };
            this.RC(u, t, c);
            this.zC(u, c)
        }
        ;
        e.prototype.zC = function(e, r) {
            var t = e.gl;
            var i = r.data.Dc.zs;
            var a = r.data.Dc.Pc;
            var n = r.data.Oc;
            var o = n.jc;
            for (var f = 0, s = o.length; f < s; f++) {
                var u = o[f];
                r.$i.Ce({
                    u_color: [0, 0, 0, 0],
                    u_deltaCenter: r.Sa,
                    u_mvpMatrix: r.viewState.mvpMatrix
                }, {
                    a_pos: {
                        type: "vec2",
                        buffer: i,
                        Re: i.size,
                        offset: 0
                    }
                }, u.length, a, "TRIANGLES", undefined, ColorMode.zi, StencilMode.Ai, undefined, u.offset)
            }
        }
        ;
        e.prototype.RC = function(e, r, t) {
            var i = e.gl;
            var a = r.size[0];
            var n = r.size[1];
            if (!this.CC) {
                this.CC = this.IC(i, r)
            }
            i.bindFramebuffer(i.FRAMEBUFFER, this.CC);
            i.viewport(0, 0, a, n);
            i.clear(i.COLOR_BUFFER_BIT);
            var o = t.data.Dc.zs;
            var f = t.data.Dc.Pc;
            var s = t.data.Oc;
            var u = s.jc;
            for (var l = 0, v = u.length; l < v; l++) {
                var c = u[l];
                t.$i.Ce({
                    u_color: [1, 0, 0, 1],
                    u_deltaCenter: t.Sa,
                    u_mvpMatrix: t.viewState.mvpMatrix
                }, {
                    a_pos: {
                        type: "vec2",
                        buffer: o,
                        Re: o.size,
                        offset: 0
                    }
                }, c.length, f, "TRIANGLES", undefined, ColorMode.zi, undefined, c.offset)
            }
            var h = a * n * 4;
            if (!this.SC || h !== this.SC.length) {
                this.SC = new Uint8Array(h)
            }
            i.readPixels(0, 0, a, n, i.RGBA, i.UNSIGNED_BYTE, this.SC);
            i.bindFramebuffer(i.FRAMEBUFFER, null);
            var d = Support$4.Ue ? 2 : 1;
            i.viewport(0, 0, a * d, n * d)
        }
        ;
        e.prototype.IC = function(e, r) {
            var t = e.createFramebuffer();
            var i = r.size[0];
            var a = r.size[1];
            var n = e.createRenderbuffer();
            t.AC = n;
            n.w = i;
            n.h = a;
            e.bindRenderbuffer(e.RENDERBUFFER, n);
            e.renderbufferStorage(e.RENDERBUFFER, e.RGBA4, i, a);
            e.bindFramebuffer(e.FRAMEBUFFER, t);
            e.framebufferRenderbuffer(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0, e.RENDERBUFFER, n);
            var o = e.checkFramebufferStatus(e.FRAMEBUFFER);
            if (e.FRAMEBUFFER_COMPLETE !== o) {
                console.log("Frame buffer object is incomplete: " + o.toString());
                return
            }
            e.bindFramebuffer(e.FRAMEBUFFER, null);
            e.bindRenderbuffer(e.RENDERBUFFER, null);
            return t
        }
        ;
        return e
    }(LayerRender);
    var lineFragmentString = "precision highp float;\n\nuniform vec4 u_color;\n// uniform float u_opacity;\nuniform vec4 u_dash;\nuniform float u_border;\nuniform float u_skyHeight;\nuniform float u_viewHeight;\n\nvarying highp float v_distance;\nvarying vec2 v_normal;\nvarying float v_width;\nvarying vec4 v_pos;\n\nvoid main() {\n    float offset = mod(v_distance, u_dash.r + u_dash.g + u_dash.b + u_dash.a);\n    if(offset>u_dash.r && offset < u_dash.r + u_dash.g){\n        discard;\n    }else if(offset> u_dash.r + u_dash.g + u_dash.b){\n        discard;\n    }\n\n    float startBlur = 0.0;\n    float endBlur = 1.0;\n    // float dist = length(v_normal) * 0.8;\n\n    // if(v_width < 4.0 && u_border == 1.0) {\n    //     startBlur = 0.0;\n    // } else {\n    //     // 线主体\n    //     if(v_width < 4.0 && u_border == 0.0) {\n    //         startBlur = 0.0;\n    //     } else {\n    //         startBlur = 0.4;\n    //     }\n    // }\n    // startBlur = clamp(0.0, 0.4, smoothstep(0.0, 4.0, v_width) - 0.6);\n\n    // startBlur = 0.0;\n    // float opacity = 1.0 - smoothstep(startBlur, endBlur, dist);\n    gl_FragColor = u_color;\n    // gl_FragColor.a *= opacity;\n\n    // gl_FragColor = vec4(1, 0, 0, 1);\n\n\n    // 雾化\n    float y = v_pos.y / v_pos.w;\n    float fogHeight = 2. / u_viewHeight * 30.;  // 10 像素高度作为模糊处理\n    vec3 fogColor= vec3(0.9, 0.9, 0.9);\n    if(u_skyHeight < 1.0 && y > 0.0) {\n        // 片元颜色 = 物体颜色 * 雾化因子 + 雾的颜色 * （1 - 雾化因子）\n        float fogFactor = smoothstep(u_skyHeight + fogHeight/2.0, u_skyHeight - fogHeight/2.0, y);\n        // float fogFactor = smoothstep(u_skyHeight + fogHeight, u_skyHeight, y);\n        // gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        gl_FragColor.a *= fogFactor;\n    }\n\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var lineVertextString = "precision highp float;\nattribute vec2 a_pos;\nattribute vec2 a_normal;\nattribute float a_distance;\nattribute vec2 a_dir;\n\n// attribute vec4 a_color;\n// attribute float a_width;\n// attribute vec4 a_dash;\n\nuniform float u_meter_per_pixel;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform float u_width;\n\nvarying float v_distance;\nvarying vec2 v_normal;\nvarying float v_width;\nvarying vec4 v_pos;\n\n#define WORLD_SIZE 20037508.342789244\n\nvoid main(){\n    float width = u_width + 0.0;\n    float cosValue = dot(a_normal , a_dir);\n    v_distance = a_distance/u_meter_per_pixel + (cosValue * width * 0.5);\n\n    vec2 pos=a_pos;\n    pos.x+=u_localDeltaCenter.x;\n    pos.y+=u_localDeltaCenter.y;\n    gl_Position = u_matrix * vec4(pos + a_normal * width * u_meter_per_pixel * 0.5, 0, 1);\n    v_normal = normalize(a_normal*sign(width*u_meter_per_pixel*.5));\n    v_width = width;\n    v_pos = gl_Position;\n}";
    var ColorConstructor = M["Color"];
    var lineUniforms = {
        u_skyHeight: "float",
        u_viewHeight: "float",
        u_width: "float",
        u_border: "float",
        u_matrix: "mat4",
        u_meter_per_pixel: "float",
        u_color: "vec4",
        u_dash: "vec4",
        u_localDeltaCenter: "vec2"
    };
    var lineAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        },
        a_normal: {
            Oe: "vec2",
            Me: "float32"
        },
        a_distance: {
            Oe: "float",
            Me: "float32"
        },
        a_dir: {
            Oe: "vec2",
            Me: "float32"
        }
    };
    var linePropertys = {
        roadColor: new LineProperty("roadColor","linear"),
        roadWidth: new LineProperty("roadWidth","linear"),
        roadDash: new LineProperty("roadDash","const"),
        borderColor: new LineBorderProperty("borderColor","linear"),
        borderWidth: new LineBorderProperty("borderWidth","linear"),
        borderDash: new LineBorderProperty("borderDash","const")
    };
    var line = {
        uniforms: lineUniforms,
        attributes: lineAttributes,
        vertexSource: lineVertextString,
        fragmentSource: lineFragmentString
    };
    var getLineUniformValues = function(e, r, t, i, a, n) {
        if (i === void 0) {
            i = [0, 0]
        }
        var o = r.minzoom;
        var f = r.maxzoom;
        var s = e.viewState.zoom;
        if (o - .2 > s && o > 3 || f + .8 < s && f < 18) {
            return
        }
        var u = r.mainkey;
        var l = r.subkey;
        if (t) {
            return getLineBorderUniformValues(e, r, i, a, n)
        } else {
            return getLineFaceUniformValues(e, r, i, a, n)
        }
    };
    function getLineBorderUniformValues(e, r, t, i, a) {
        if (t === void 0) {
            t = [0, 0]
        }
        var n = e.map.bZ.MZ.drawMode === "fast" ? e.viewState.optimalZoom : e.viewState.zoom;
        var o = r.mainkey
          , f = r.subkey
          , s = r.minzoom;
        var u = linePropertys.borderColor.An(i, o, f, n, s);
        if (!u) {
            return undefined
        }
        var l = e.map.bZ.MZ.KY;
        if (l && o === l.Lp && l.Ap.indexOf(f) > -1) {
            u = ColorConstructor.FV(u, l.NY)
        }
        var v = linePropertys.roadWidth.An(i, o, f, n, s) + linePropertys.borderWidth.An(i, o, f, n, s);
        var c = linePropertys.borderDash.An(i, o, f, n, s) || [1, 0, 1, 0];
        return {
            u_skyHeight: a,
            u_matrix: e.viewState.mvpMatrix,
            u_meter_per_pixel: e.viewState.resolution,
            u_width: v,
            u_border: 1,
            u_color: u,
            u_dash: c,
            u_localDeltaCenter: t,
            u_viewHeight: e.size[1]
        }
    }
    function getLineFaceUniformValues(e, r, t, i, a) {
        if (t === void 0) {
            t = [0, 0]
        }
        var n = e.map.bZ.MZ.drawMode === "fast" ? e.viewState.optimalZoom : e.viewState.zoom;
        var o = r.mainkey
          , f = r.subkey
          , s = r.minzoom
          , u = r.maxzoom;
        var l = linePropertys.roadColor.An(i, o, f, n, s);
        if (f < 0 && o < 0) {
            l = [1, 0, 0, e.debug ? 1 : 0]
        }
        if (!l) {
            return undefined
        }
        var v = e.map.bZ.MZ.KY;
        if (v && o === v.Lp && v.Ap.indexOf(f) > -1) {
            l = ColorConstructor.FV(l, v.NY)
        }
        var c = linePropertys.roadWidth.An(i, o, f, n, s);
        var h = linePropertys.roadDash.An(i, o, f, n, s) || [1, 0, 1, 0];
        return {
            u_matrix: e.viewState.mvpMatrix,
            u_meter_per_pixel: e.viewState.resolution,
            u_width: c,
            u_color: l,
            u_dash: h,
            u_border: 0,
            u_localDeltaCenter: t,
            u_viewHeight: e.size[1],
            u_skyHeight: a
        }
    }
    var fillFragmentString = "precision highp float;\nuniform vec4 u_color;\n// uniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\nuniform vec3 u_skyColor;\nvarying vec4 v_pos;\n\nvoid main() {\n    gl_FragColor = u_color;\n    // gl_FragColor = v_color;\n    // gl_FragColor = vec4(0,0,1.0,1.0);\n    // return;\n\n    // 雾化\n    float y = v_pos.y / v_pos.w;\n    float fogHeight = 2. / u_viewHeight * 30.;  // 10 像素高度作为模糊处理\n    vec3 fogColor= vec3(0.9, 0.9, 0.9);\n    if(u_skyHeight < 1.0 && y > 0.0) {\n        float fogFactor = smoothstep(u_skyHeight + fogHeight/2.0, u_skyHeight - fogHeight/2.0, y);\n        // float fogFactor = smoothstep(u_skyHeight + fogHeight, u_skyHeight, y);\n        // gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        gl_FragColor.a *= fogFactor;\n    }\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n\n";
    var fillVertextString = "precision highp float;\nattribute vec2 a_pos;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nvarying vec4 v_pos;\n\nvoid main() {\n    vec2 pos = vec2(a_pos.x+u_localDeltaCenter.x+u_offset.x,a_pos.y+u_localDeltaCenter.y+u_offset.y);\n    gl_Position = u_matrix * vec4(pos, 0, 1);\n    v_pos = gl_Position;\n}\n";
    var fillPatternFragmentString = "precision highp float;\n#define GLSLIFY 1\nuniform vec4 u_color;\nuniform sampler2D u_image;\n// uniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\nuniform float u_resolution;\nuniform vec2 u_imageSize;\nvarying vec4 v_pos;\nvarying vec2 v_coord;\n// varying vec2 v_texture_pos;\n\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n    vec2 v_texture_pos = mod(vec2(v_coord.x,v_coord.y*-1.0)/u_resolution/u_imageSize,1.0);\n    gl_FragColor = texture2D(u_image, v_texture_pos);\n    // gl_FragColor = vec4(1.0,0,0,1.0);\n   \n    float y = v_pos.y / v_pos.w;\n    if(u_skyHeight > 0.0 && y > u_skyHeight) {\n        gl_FragColor.a = 1.0 - smoothstep(u_skyHeight, 1.0, y);\n    }\n    gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight);\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var fillPatternVertextString = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform float u_resolution;\nuniform vec2 u_offset;\nvarying vec4 v_pos;\nvarying vec2 v_coord;\n\n// varying vec2 v_texture_pos;\nvoid main(){\n    vec2 pos = vec2(a_pos.x+u_localDeltaCenter.x+u_offset.x,a_pos.y+u_localDeltaCenter.y+u_offset.y);\n    gl_Position = u_matrix * vec4(pos, 0, 1);\n    v_pos = gl_Position;\n    v_coord = pos;\n}\n";
    var fillUniforms = {
        u_skyColor: "vec3",
        u_color: "vec4",
        u_skyHeight: "float",
        u_viewHeight: "float",
        u_offset: "vec2",
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2"
    };
    var fillAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec2"
        }
    };
    var fill = {
        uniforms: fillUniforms,
        attributes: fillAttributes,
        vertexSource: fillVertextString,
        fragmentSource: fillFragmentString
    };
    var fillProperties = {
        faceColor: new FillProperty("faceColor","linear"),
        borderWidth: new FillProperty("borderWidth","linear"),
        borderColor: new FillProperty("borderColor","linear"),
        texture: new FillProperty("texture","const")
    };
    var ColorConstructor$1 = M["Color"];
    var getFillUniformValues = function(e, r, t, i, a) {
        if (t === void 0) {
            t = [0, 0]
        }
        if (a === void 0) {
            a = 1
        }
        var n = e.map.bZ.MZ.drawMode === "fast" ? e.viewState.optimalZoom : e.viewState.zoom;
        if (r.minzoom - .2 > e.viewState.zoom && r.minzoom > 3) {
            return
        }
        var o = r.mainkey
          , f = r.subkey
          , s = r.minzoom;
        var u = fillProperties.faceColor.An(i, o, f, n, s);
        if (!u) {
            return undefined
        }
        var l = e.map;
        var v = e.map.bZ.MZ.KY;
        if (v && o === v.Lp && v.Ap.indexOf(f) > -1) {
            u = ColorConstructor$1.FV(u, v.NY)
        }
        var c = l.JF(e.viewState.optimalZoom);
        return {
            u_matrix: e.viewState.mvpMatrix,
            u_color: u,
            u_offset: [0, 0],
            u_skyHeight: a,
            u_viewHeight: e.viewState.size[1],
            u_skyColor: c || [1, 1, 1, 0],
            u_image: fillProperties.texture.An(i, o, f, n, s),
            u_imageSize: [1, 1],
            u_localDeltaCenter: t,
            u_resolution: e.viewState.optimalResolution
        }
    };
    var getFillPatternUniformValues = function(e, r, t, i, a) {
        if (t === void 0) {
            t = [0, 0]
        }
        if (a === void 0) {
            a = 1
        }
        var n = e.viewState.zoom;
        if (r.minzoom - .2 > e.viewState.zoom && r.minzoom > 3) {
            return
        }
        var o = r.mainkey
          , f = r.subkey
          , s = r.minzoom;
        var u = fillProperties.faceColor.An(i, o, f, n, s);
        var l = e.map;
        var v = l.JF(e.viewState.optimalZoom);
        if (!u) {
            return undefined
        }
        return {
            u_matrix: e.viewState.mvpMatrix,
            u_color: u,
            u_offset: [0, 0],
            u_skyHeight: a,
            u_viewHeight: e.viewState.size[1],
            u_skyColor: v || [1, 1, 1, 0],
            u_image: fillProperties.texture.An(i, o, f, n, s),
            u_imageSize: [1, 1],
            u_localDeltaCenter: t,
            u_resolution: e.viewState.optimalResolution
        }
    };
    var fillPatternUniforms = {
        u_color: "vec4",
        u_skyColor: "vec4",
        u_viewHeight: "float",
        u_skyHeight: "float",
        u_offset: "vec2",
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_image: "sampler2D",
        u_resolution: "float",
        u_imageSize: "vec2"
    };
    var fillPatternAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec2"
        }
    };
    var fillPattern = {
        uniforms: fillPatternUniforms,
        attributes: fillPatternAttributes,
        vertexSource: fillPatternVertextString,
        fragmentSource: fillPatternFragmentString
    };
    function drawFillPattern(e, r, t, i, a, n, o, f, s, u, l) {
        if (n === void 0) {
            n = "TRIANGLES"
        }
        if (o === void 0) {
            o = false
        }
        if (f === void 0) {
            f = ColorMode.Mi
        }
        if (s === void 0) {
            s = StencilMode.disable
        }
        if (u === void 0) {
            u = CullFaceMode.we
        }
        if (l === void 0) {
            l = 0
        }
        var v = e.aH();
        v.Ce(r, t, i, a, n, o, f, s, u, l)
    }
    var lineFragmentString$1 = "precision mediump float;\nuniform vec4 u_color;\nvarying vec4 v_pos;\n// uniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\n\nvoid main(){\n    // float dist=length(v_pos-gl_FragCoord.xy);\n    // float alpha=1.-smoothstep(0.,1.,dist);\n    gl_FragColor=u_color;\n    // gl_FragColor=vec4(0.8, 0.8, 0.8, 0.5);\n    // 雾化\n    float y = v_pos.y / v_pos.w;\n    float fogHeight = 2. / u_viewHeight * 30.;  // 10 像素高度作为模糊处理\n    vec3 fogColor= vec3(0.9, 0.9, 0.9);\n    if(u_skyHeight < 1.0 && y > 0.0) {\n        float fogFactor = smoothstep(u_skyHeight + fogHeight/2.0, u_skyHeight - fogHeight/2.0, y);\n        // float fogFactor = smoothstep(u_skyHeight + fogHeight, u_skyHeight, y);\n        // gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        gl_FragColor.a *= fogFactor;\n    }\n    #ifdef OVERDRAW_INSPECTOR\n    gl_FragColor=vec4(1.);\n    #endif\n}\n";
    var lineVertextString$1 = "precision highp float;\nattribute vec2 a_pos;\n\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\n\nvarying vec4 v_pos;\n\nvoid main(){\n    vec2 pos=vec2(a_pos.x+u_localDeltaCenter.x,a_pos.y+u_localDeltaCenter.y);\n    gl_Position=u_matrix*vec4(pos,0,1);\n    v_pos=gl_Position;\n}\n";
    var fillOutlineUniforms = {
        u_color: "vec4",
        u_normal: "vec3",
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var fillOutlineAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        }
    };
    var fillOutline = {
        uniforms: fillOutlineUniforms,
        attributes: fillOutlineAttributes,
        vertexSource: lineVertextString$1,
        fragmentSource: lineFragmentString$1
    };
    var fillOutlineProperties = {
        faceColor: new BuildingProperty("faceColor","linear"),
        borderWidth: new BuildingBorderProperty("borderWidth","const"),
        borderColor: new BuildingBorderProperty("borderColor","linear"),
        wallColor1: new BuildingProperty("wallColor1","linear"),
        wallColor2: new BuildingProperty("wallColor2","linear")
    };
    var getFillOutlineUniformValues = function(e, r, t, i, a) {
        if (a === void 0) {
            a = 1
        }
        var n = e.map.bZ.MZ.drawMode === "fast" ? e.viewState.optimalZoom : e.viewState.zoom;
        if (r.minzoom - .5 > e.viewState.zoom && r.minzoom > 3) {
            return
        }
        var o = r.mainkey
          , f = r.subkey
          , s = r.minzoom;
        var u = fillOutlineProperties.borderColor.An(i, o, f, n, s);
        if (!u) {
            return undefined
        }
        return {
            u_color: u,
            u_normal: [0, 0, -.1],
            u_matrix: e.viewState.mvpMatrix,
            u_localDeltaCenter: t,
            u_skyHeight: a,
            u_viewHeight: e.viewState.size[1]
        }
    };
    var Tile$1 = M["Tile"];
    var lcs$4 = M["geo"]["lcs"];
    var find$2 = M["lodash"]["find"];
    var map$2 = M["lodash"]["map"];
    var Browser$1 = M["Support"];
    var NebulaRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.yz = 1;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            if (!a.map.mapStyle.vn) {
                return
            }
            this.kG = i.rejectMapMask || !t.map["getMask"]() ? undefined : StencilMode["writeWithStencil"];
            var n = t.map.getOutseaState();
            var E = t.map.getOutseaDataType();
            if (n && t.viewState.optimalZoom >= 9.8) {
                this.kG = StencilMode["nbStencil"];
                var o = r && r.outseaData;
                if (o) {
                    this.wG(e, t, i, a, o)
                }
            }
            if (t.map.bZ.MZ.drawMode !== "fast" && !r.s_) {
                return
            }
            var f = r.tiles;
            var s = {};
            for (var u = 0, l = f; u < l.length; u++) {
                var v = l[u];
                if (Tile$1.YH(v)) {
                    var c = v.zo.z;
                    if (!s[c]) {
                        s[c] = []
                    }
                    s[c].push(v)
                }
            }
            var h = i.layer;
            var d = t.map.getStatus().features;
            var _ = h.uo.getSource(h.co());
            var g = _.ra(t.viewState.optimalZoom);
            var y = map$2(Object.keys(s), function(e) {
                return parseInt(e, 10)
            });
            y.sort(function(e, r) {
                return e - r
            });
            var m = y.indexOf(g);
            if (m >= 0) {
                y.splice(m, 1);
                y.push(g)
            }
            if (t.viewState.viewMode === "3D") {
                this.yz = t.map.getView().EF()
            }
            for (var p = 0, b = y; p < b.length; p++) {
                var x = b[p];
                var M = s[x];
                if (!(d && d.indexOf("bg") === -1)) {
                    for (var C = 0, S = M; C < S.length; C++) {
                        var v = S[C];
                        if (!Tile$1.YH(v)) {
                            continue
                        }
                        this.Lc(e, v, t, i, a)
                    }
                }
                if (!(d && d.indexOf("building") === -1)) {
                    for (var w = 0, k = M; w < k.length; w++) {
                        var v = k[w];
                        if (!Tile$1.YH(v)) {
                            continue
                        }
                        this.PD(e, v, t, i, a)
                    }
                }
                if (!(d && d.indexOf("building") === -1)) {
                    for (var T = 0, A = M; T < A.length; T++) {
                        var v = A[T];
                        if (!Tile$1.YH(v)) {
                            continue
                        }
                        this.Ac(e, v, t, i, a)
                    }
                }
                if (!(d && d.indexOf("road") === -1)) {
                    for (var I = 0, $ = M; I < $.length; I++) {
                        var v = $[I];
                        if (!Tile$1.YH(v)) {
                            continue
                        }
                        this.Tc(e, v, t, i, a)
                    }
                }
                if (!(d && d.indexOf("road") === -1)) {
                    for (var F = 0, P = M; F < P.length; F++) {
                        var v = P[F];
                        if (!Tile$1.YH(v)) {
                            continue
                        }
                        this.Ic(e, v, t, i, a)
                    }
                }
            }
            if (r.tiles && r.tiles.length > 0) {
                if (Browser$1["amapRunTime"] && Browser$1["amapRunTime"]["map-init"]) {
                    var L = Date.now() - Browser$1["amapRunTime"]["map-init"];
                    this.Cee("fp-time", L)
                }
                a.map.bZ.dynamic.set("firstPaint", true)
            }
            if (window["testTime"] && !window["testTime"]["renderTime"] && f && f.length > 0) {
                var R = (new Date).getTime();
                window["testTime"]["renderTime"] = R - window["testTime"]["start"]
            }
            if (n && t.viewState.optimalZoom >= 10) {
                this.kG = StencilMode["nbStencil"];
                var o = r && r.outseaData;
                if (o) {
                    e.context.clear({
                        stencil: true
                    })
                }
            }
        }
        ;
        e.prototype.destroy = function() {}
        ;
        e.prototype.wG = function(e, r, t, i, a) {
            if (!a || !a.LS) {
                return
            }
            var n = e.aS();
            var o = n.context;
            var f = r.viewState;
            var s = [0, 0];
            var u = f.centerCoord;
            if (r.viewState.optimalZoom < LocalZoom) {
                s = a.LS.center
            } else {
                var l = lcs$4.getLocalByCoord([u[0], u[1]]).center;
                var v = a.LS.center[0] - l[0];
                var c = a.LS.center[1] - l[1];
                s = [v, c]
            }
            var h = {
                outseaData: a,
                $i: n,
                viewState: f,
                Sa: s
            };
            this.zC(o, h)
        }
        ;
        e.prototype.zC = function(e, r) {
            r.outseaData.Dc.upload(e, true);
            var t = r.outseaData.Dc.zs;
            var i = r.outseaData.Dc.Pc;
            var a = r.outseaData.Oc;
            var n = a.jc;
            for (var o = 0, f = n.length; o < f; o++) {
                var s = n[o];
                r.$i.Ce({
                    u_color: [0, 0, 0, 0],
                    u_deltaCenter: r.Sa,
                    u_mvpMatrix: r.viewState.mvpMatrix
                }, {
                    a_pos: {
                        type: "vec2",
                        buffer: t,
                        Re: t.size,
                        offset: 0
                    }
                }, s.length, i, "TRIANGLES", undefined, ColorMode.zi, StencilMode["outseaStencil"], undefined, s.offset)
            }
        }
        ;
        e.prototype.Lc = function(e, r, t, i, a) {
            var n = e.Wn();
            var o = r.data;
            var f = true;
            for (var s = 0, u = o; s < u.length; s++) {
                var l = u[s];
                var v = l.Dc;
                if (!v) {
                    continue
                }
                v.upload(n.context, true);
                var c = v.zs;
                var h = v.Pc;
                var d = l.Oc;
                var _ = f ? 0 : 1;
                f = false;
                var g = {
                    a_pos: {
                        type: "vec2",
                        Re: c.size,
                        offset: 0,
                        buffer: c
                    }
                };
                for (var y = _, m = d.jc.length; y < m; y++) {
                    var p = d.jc[y];
                    var b = fillProperties.texture.An(a.map.mapStyle, p.mainkey, p.subkey, t.viewState.optimalZoom, p.minzoom);
                    if (b) {
                        var x = getFillPatternUniformValues(t, p, r.Sa, a.map.mapStyle, this.yz);
                        if (x) {
                            var M = a.map.getImage(x.u_image);
                            if (M && x.u_image) {
                                x.u_image = M;
                                x.u_imageSize = M.size;
                                drawFillPattern(e, x, g, p.length, h, "TRIANGLES", false, ColorMode.Si, this.kG, undefined, p.offset)
                            } else if (x.u_image && a.map.I_ && !a.map.I_.MH(x.u_image)) {
                                a.map.I_["addImage"](x.u_image, {
                                    url: x.u_image,
                                    filter: "LINEAR",
                                    wrap: "REPEAT",
                                    cb: function() {
                                        return a.map.setNeedUpdate(true)
                                    }
                                })
                            }
                        }
                    } else {
                        var x = getFillUniformValues(t, p, r.Sa, a.map.mapStyle, this.yz);
                        if (x) {
                            n.Ce(x, g, p.length, h, "TRIANGLES", false, ColorMode.Si, this.kG, undefined, p.offset)
                        }
                    }
                }
            }
        }
        ;
        e.prototype.PD = function(e, r, t, i, a) {
            var n = e.Wn();
            var o = r.data;
            var f = find$2(t.map.getLayers(), function(e) {
                return e.CLASS_NAME === "AMap.IndoorMap"
            });
            var s;
            if (f) {
                s = f.yD().show
            }
            var u = true;
            for (var l = 0, v = o; l < v.length; l++) {
                var c = v[l];
                var h = c.FD;
                if (!h) {
                    continue
                }
                h.upload(n.context, true);
                var d = h.zs;
                var _ = h.Pc;
                var g = c.BD;
                if (!g) {
                    continue
                }
                var y = u ? 0 : 1;
                u = false;
                var m = {
                    a_pos: {
                        type: "vec2",
                        Re: d.size,
                        offset: 0,
                        buffer: d
                    }
                };
                for (var p = y, b = g.jc.length; p < b; p++) {
                    var x = g.jc[p];
                    if (!f || !(x.bz && x.bz !== "indoor_out_building" && s)) {
                        var M = getFillUniformValues(t, x, r.Sa, a.map.mapStyle, this.yz);
                        if (M) {
                            n.Ce(M, m, x.length, _, "TRIANGLES", false, ColorMode.Si, this.kG, undefined, x.offset)
                        }
                    }
                }
            }
        }
        ;
        e.prototype.Ac = function(e, r, t, i, a) {
            var n = e.Yn();
            var o = r.data;
            var f = find$2(t.map.getLayers(), function(e) {
                return e.CLASS_NAME === "AMap.IndoorMap"
            });
            var s;
            if (f) {
                s = f.yD().show
            }
            for (var u = 0, l = o; u < l.length; u++) {
                var v = l[u];
                var c = v.Dc;
                if (!c) {
                    continue
                }
                c.upload(n.context, true);
                var h = v.Fc;
                var d = c.Ec;
                var _ = c.Nc;
                var g = {
                    a_pos: {
                        type: "vec2",
                        Re: 0,
                        offset: 0,
                        buffer: d
                    }
                };
                for (var y = 0, m = h.jc.length; y < m; y++) {
                    var p = h.jc[y];
                    var b = getFillOutlineUniformValues(t, p, r.Sa, a.map.mapStyle, this.yz);
                    if (!f || !(p.bz && p.bz !== "indoor_out_building" && s)) {
                        if (b) {
                            n.Ce(b, g, p.length, _, "LINES", false, ColorMode.Si, this.kG, undefined, p.offset)
                        }
                    }
                }
            }
        }
        ;
        e.prototype.Ic = function(e, r, t, i, a) {
            var n = e.Vn();
            if (!r.data) {
                assert(r.data, "tile have no data");
                return
            }
            var o = r.data;
            for (var f = 0, s = o; f < s.length; f++) {
                var u = s[f];
                var l = u.$c;
                if (!l) {
                    continue
                }
                n.context.getExtension("OES_element_index_uint");
                l.upload(n.context, true);
                var v = u.$c.zs.buffer;
                var c = u.$c.Pc.buffer;
                var h = u.Uc;
                var d = h.jc;
                var _ = v.size;
                var g = {
                    a_pos: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: v,
                        Re: _,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 5
                    }
                };
                for (var y = 0, m = d.length; y < m; y++) {
                    var p = d[y];
                    var b = getLineUniformValues(t, p, false, r.Sa, a.map.mapStyle, this.yz);
                    if (p.mainkey === -2 && a.map.debug) {
                        b = {
                            u_matrix: t.viewState.mvpMatrix,
                            u_meter_per_pixel: t.viewState.resolution,
                            u_width: 2,
                            u_color: [1, 0, 0, 1],
                            u_dash: 0,
                            u_border: 0,
                            u_localDeltaCenter: r.Sa,
                            u_skyHeight: 1,
                            u_viewHeight: 1
                        }
                    }
                    if (!b) {
                        continue
                    }
                    if (b) {
                        n.Ce(b, g, p.length, c, "TRIANGLES", false, ColorMode.Si, this.kG, undefined, p.offset)
                    }
                }
            }
        }
        ;
        e.prototype.Tc = function(e, r, t, i, a) {
            var n = e.Vn();
            if (!r.data) {
                assert(r.data, "tile have no data");
                return
            }
            var o = r.data;
            for (var f = 0, s = o; f < s.length; f++) {
                var u = s[f];
                var l = u.$c;
                if (!l) {
                    continue
                }
                n.context.getExtension("OES_element_index_uint");
                l.upload(n.context, true);
                var v = u.$c.zs.buffer;
                var c = u.$c.Pc.buffer;
                var h = u.Uc;
                var d = h.jc;
                var _ = v.size;
                var g = {
                    a_pos: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: v,
                        Re: _,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: v,
                        Re: _,
                        offset: 4 * 5
                    }
                };
                for (var y = 0, m = d.length; y < m; y++) {
                    var p = d[y];
                    var b = getLineUniformValues(t, p, true, r.Sa, a.map.mapStyle, this.yz);
                    if (b) {
                        n.Ce(b, g, p.length, c, "TRIANGLES", false, ColorMode.Si, this.kG, undefined, p.offset)
                    }
                }
            }
        }
        ;
        e.prototype.Cee = function(e, r) {
            if (Browser$1["amapRunTime"] && !Browser$1["amapRunTime"][e]) {
                Browser$1["amapRunTime"][e] = r
            }
        }
        ;
        return e
    }(LayerRender);
    var TileState$2 = M["TileState"];
    var ColorScaleConstructor = M["ColorScale"];
    var RasterLayerRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a, n) {
            if (!a.map.mapStyle.vn) {
                return
            }
            var o = t.viewState;
            var f = n.getLayerConfig();
            var s = f.zooms;
            var u = o.size
              , l = o.pitch
              , v = o.optimalZoom;
            if (o.optimalZoom < s[0] || o.optimalZoom > s[1]) {
                return
            }
            var c = e.Gn();
            var h = t.map;
            var d = h.JF(t.viewState.optimalZoom);
            var _ = d || [1, 1, 1, 0];
            var g = r.tiles;
            var y = 1;
            if (t.viewState.viewMode === "3D") {
                y = t.map.getView().EF()
            }
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            var m = null;
            if (f.OY) {
                var p = h.mapStyle.vn.MS("satellite");
                if (p && p["filter"]) {
                    m = ColorScaleConstructor["getTexture"](e.context, p["filter"]) || null
                }
            }
            for (var b = 0, x = g.length; b < x; b += 1) {
                var M = g[b];
                for (var C = 0, S = M.length; C < S; C += 1) {
                    var w = M[C];
                    if (w.status === TileState$2.LOADED) {
                        c.Ce({
                            u_skyColor: _,
                            u_viewHeight: o.size[1],
                            u_skyHeight: y,
                            u_opacity: f.opacity || 1,
                            u_texture: w.data.texture,
                            u_mvpMatrix: o.mvpMatrix,
                            u_localDeltaCenter: w.data.Sa,
                            u_flterFlag: m ? true : false,
                            u_colorscale: m
                        }, {
                            a_Position: {
                                type: "vec4",
                                buffer: w.data.zs,
                                Re: 0,
                                offset: 0
                            }
                        }, 6, undefined, undefined, i.depthTest, ColorMode.Si, i.rejectMapMask || !t.map.getMask() ? undefined : StencilMode.writeWithStencil)
                    }
                }
                for (var C = 0, S = M.length; C < S; C += 1) {
                    var w = M[C];
                    if (w.status === TileState$2.LOADED && t.debug) {
                        c.Ce({
                            u_skyColor: _,
                            u_viewHeight: o.size[1],
                            u_skyHeight: y,
                            u_opacity: f.opacity || 1,
                            u_texture: w.data.Js,
                            u_mvpMatrix: o.mvpMatrix,
                            u_localDeltaCenter: w.data.Sa,
                            u_flterFlag: false,
                            u_colorscale: null
                        }, {
                            a_Position: {
                                type: "vec4",
                                buffer: w.data.zs,
                                Re: 0,
                                offset: 0
                            }
                        }, 6, undefined, "TRIANGLES", undefined, undefined, undefined)
                    }
                }
            }
            return
        }
        ;
        return e
    }(LayerRender);
    var Util$4 = AMap["Util"];
    var PolygonWebglRender = function() {
        function e(e) {
            this.ydt = e
        }
        e.prototype.render = function(e, r, t) {
            if (!e._map) {
                return
            }
            if (e._opts.extrusionHeight && e._opts.extrusionHeight > 0) {
                this.S_(e, r, t)
            } else {
                this.Wx(e, r, t);
                this.Gx(e, r, t)
            }
        }
        ;
        e.prototype.S_ = function(e, r, t, i, a) {
            if (i === void 0) {
                i = false
            }
            if (a === void 0) {
                a = false
            }
            if (!e.visible) {
                return
            }
            var n = e.p0;
            if (!n) {
                return
            }
            n.upload(t.context, true);
            var o = r.map;
            var f = r.viewState.size;
            var s = o.getView();
            var u = e.rY(r);
            var l = {
                zooms: e._opts.zooms,
                layer: {
                    CLASS_NAME: "AMap.VectorLayer"
                },
                opacity: 1,
                heightFactor: 1,
                visible: true,
                zIndex: e._opts.zIndex,
                depthTest: true
            };
            var v = 1;
            if (r.viewState.viewMode === "3D") {
                v = s.EF()
            } else {
                return
            }
            var c = s.X(f[0] / 2, s.Uu() + 5);
            var h = s.X(f[0] / 2, s.Uu() - 5);
            var d = s.pz().getPosition();
            var _ = c[0] - d[0];
            var g = c[1] - d[1];
            var y = h[0] - d[0];
            var m = h[1] - d[1];
            var p = [Math.sqrt(_ * _ + g * g), Math.sqrt(y * y + m * m)];
            var b = e.m0;
            var x = n.zs;
            for (var M = 0, C = b.jc; M < C.length; M++) {
                var S = C[M];
                var w = Util$4.color2RgbaArray(e._opts.roofColor);
                var k = Util$4.color2RgbaArray(e._opts.wallColor);
                var T = void 0;
                if (a) {
                    T = this.ydt.find(S.mainkey);
                    if (!T) {
                        T = this.ydt.add(S.mainkey)
                    }
                }
                if (a && !T) {
                    return
                }
                var A = !!S.bz;
                if (A && !a) {
                    var I = getFillExtPatternUniformValues(r, S, u, l, {
                        wallColor: k,
                        roofColor: w
                    }, v, p);
                    if (I) {
                        var $ = o.getImage(I.u_image);
                        if ($) {
                            I.u_image = $;
                            I.u_imageSize = $.size;
                            var F = t.sH();
                            F.Ce(I, {
                                a_pos: {
                                    type: "vec4",
                                    Re: x.size,
                                    offset: 0,
                                    buffer: x
                                },
                                a_normal: {
                                    type: "vec3",
                                    Re: x.size,
                                    offset: 4 * 4,
                                    buffer: x
                                },
                                a_textureCoord: {
                                    type: "vec2",
                                    Re: x.size,
                                    offset: 4 * 7,
                                    buffer: x
                                }
                            }, S.length, undefined, "TRIANGLES", l.depthTest, i ? ColorMode.disabled : ColorMode.Si, undefined, CullFaceMode.back, S.offset)
                        } else if (I.u_image && o.I_ && !o.I_.MH(I.u_image)) {
                            o.addImage(I.u_image, {
                                url: I.u_image,
                                filter: "LINEAR",
                                wrap: "REPEAT",
                                cb: function() {
                                    return o.setNeedUpdate(true)
                                }
                            })
                        }
                    }
                } else {
                    var I = getFillExtUniformValues(r, S, u, l, {
                        wallColor: a ? T : k,
                        roofColor: a ? T : w
                    }, v, p);
                    if (I) {
                        var F = t.k_();
                        I.u_isPick = a;
                        F.Ce(I, {
                            a_pos: {
                                type: "vec4",
                                Re: x.size,
                                offset: 0,
                                buffer: x
                            },
                            a_normal: {
                                type: "vec3",
                                Re: x.size,
                                offset: 4 * 4,
                                buffer: x
                            }
                        }, S.length, undefined, "TRIANGLES", l.depthTest, i ? ColorMode.disabled : ColorMode.Si, undefined, CullFaceMode.back, S.offset)
                    }
                }
            }
        }
        ;
        e.prototype.qx = function(e, r, t) {
            var i = e.rY(r);
            var a = e._opts;
            if (!a.fillColor) {
                return
            }
            var n;
            if (typeof a.fillColor === "function") {
                n = Util$4.color2RgbaArray(a.fillColor(t))
            } else {
                n = Util$4.color2RgbaArray(a.fillColor)
            }
            if (!n) {
                return
            }
            n = n.slice(0, 3);
            var o;
            if (typeof a.fillOpacity === "function") {
                o = a.fillOpacity(t)
            } else {
                o = a.fillOpacity
            }
            n.push(o);
            return {
                u_offset: e.zx,
                u_color: n,
                u_matrix: r.viewState.mvpMatrix,
                u_localDeltaCenter: i,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.Vx = function(e, r, t) {
            var i = e.rY(r);
            var a = e._opts;
            if (!a.strokeColor) {
                return
            }
            var n;
            if (typeof a.strokeColor === "function") {
                n = Util$4.color2RgbaArray(a.strokeColor(t))
            } else {
                n = Util$4.color2RgbaArray(a.strokeColor)
            }
            if (!n) {
                return
            }
            n = n.slice(0, 3);
            n.push(a.strokeOpacity);
            var o = this.Xx(e);
            return {
                u_color: n,
                u_matrix: r.viewState.mvpMatrix,
                u_localDeltaCenter: i,
                u_width: a.strokeWeight,
                u_offset: e.zx,
                u_meter_per_pixel: r.viewState.resolution,
                u_dash: o.dash,
                u_dashType: o.Jx,
                u_border: false,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.Wx = function(e, r, t) {
            if (!e.Dx) {
                return
            }
            var i = e.Dx;
            var a = e.XB;
            i.upload(t.context);
            if (!i.uploaded || !i.zs) {
                return
            }
            if (!e.visible) {
                return
            }
            var n = i.zs;
            var o = t.Wn();
            o.context.getExtension("OES_element_index_uint");
            for (var f = 0, s = a.jc.length; f < s; f++) {
                var u = this.qx(e, r, f);
                if (!u) {
                    continue
                }
                var l = a.jc[f];
                o.Ce(u, {
                    a_pos: {
                        buffer: n,
                        type: "vec2",
                        Re: n.size,
                        offset: 0
                    }
                }, l.length, i.Pc, undefined, undefined, ColorMode.Si, undefined, undefined, l.offset)
            }
        }
        ;
        e.prototype.Gx = function(e, r, t) {
            if (!e.Px) {
                return
            }
            var i = e.Px;
            var a = e.qB;
            i.upload(t.context);
            if (!i.uploaded && !i.zs) {
                return
            }
            var n = i.zs.buffer;
            if (!e.visible) {
                return
            }
            var o = t.Xn();
            o.context.getExtension("OES_element_index_uint");
            for (var f = 0, s = a.jc.length; f < s; f++) {
                var u = this.Vx(e, r, f);
                if (!u) {
                    continue
                }
                var l = a.jc[f];
                o.Ce(u, {
                    a_pos: {
                        buffer: n,
                        type: "vec2",
                        Re: n.size,
                        offset: 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: n,
                        Re: n.size,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: n,
                        Re: n.size,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: n,
                        Re: n.size,
                        offset: 4 * 5
                    }
                }, l.length, i.Pc.buffer, undefined, undefined, ColorMode.Si, undefined, undefined, l.offset)
            }
        }
        ;
        e.prototype.Xx = function(e) {
            var r = e._opts;
            var t = 2;
            var i = [1, 0, 0];
            if (r.strokeStyle === "dashed" && r.strokeDasharray) {
                if (r.strokeDasharray.length >= 3) {
                    t = 3;
                    i = r.strokeDasharray.slice(0, 3)
                } else {
                    t = 2;
                    i = __spreadArrays(r.strokeDasharray.slice(0), [0])
                }
            }
            return {
                Jx: t,
                dash: i
            }
        }
        ;
        return e
    }();
    var Util$5 = AMap["Util"];
    var PolylineWebglRender = function() {
        function e() {
            this.See = performance.now()
        }
        e.prototype.render = function(e, r, t) {
            if (!e._map) {
                return
            }
            var i = e._opts;
            this.Tc(e, r, t);
            this.Ic(e, r, t);
            if (i.lineGradient) {
                this.nC(e, r, t)
            }
            if (i.showDir) {
                this.N_(e, r, t, i)
            }
        }
        ;
        e.prototype.fb = function(e, r) {
            var t = e.rY(r);
            var i = e._opts;
            var a = i.strokeWeight || -1;
            if (a <= 0) {
                return
            }
            if (i.strokeOpacity <= 0) {
                return
            }
            if (!i.strokeColor) {
                return
            }
            var n = Util$5.color2RgbaArray(i.strokeColor);
            if (!n) {
                return
            }
            n = n.slice(0, 3);
            var o = this.Xx(e);
            n.push(i.strokeOpacity);
            if (i.unit === "meter") {
                a /= r.viewState.resolution
            }
            return {
                u_color: n,
                u_matrix: r.viewState.mvpMatrix,
                u_meter_per_pixel: r.viewState.resolution,
                u_width: a,
                u_offset: e.zx,
                u_localDeltaCenter: t,
                u_dash: o.dash,
                u_dashType: o.Jx,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.pb = function(e, r) {
            var t = e.rY(r);
            var i = e._opts;
            if (!i.outlineColor) {
                return
            }
            var a = Util$5.color2RgbaArray(i.outlineColor);
            if (!a) {
                return
            }
            a = a.slice(0, 3);
            a.push(i.strokeOpacity);
            var n = i.strokeWeight + i.borderWeight * 2;
            if (n <= 0) {
                return
            }
            if (i.strokeOpacity <= 0) {
                return
            }
            var o = this.Xx(e);
            return {
                u_color: a,
                u_matrix: r.viewState.mvpMatrix,
                u_localDeltaCenter: t,
                u_width: n,
                u_offset: e.zx,
                u_meter_per_pixel: r.viewState.resolution,
                u_dash: o.dash,
                u_dashType: o.Jx,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.B_ = function(e, r, t) {
            if (!e._map) {
                return
            }
            var i = e.rY(r);
            var a = e._opts;
            var n = a.strokeWeight;
            var o;
            if (n <= 0) {
                return
            }
            var f = Boolean(a.dirImg);
            var s = a["dirImgGap"] ? a["dirImgGap"] : 1;
            if (f) {
                e.U_(t);
                o = e.E_
            } else {
                o = e._map.getImage("AMapArrow")
            }
            if (!o) {
                return
            }
            var u = Util$5.color2RgbaArray(a.dirColor).slice(0, 3);
            if (a.unit === "meter") {
                n /= r.viewState.resolution
            }
            return {
                u_matrix: r.viewState.mvpMatrix,
                u_meter_per_pixel: r.viewState.resolution,
                u_meter_per_pixel_optimal: r.viewState.optimalResolution,
                u_width: n,
                u_offset: e.zx,
                u_localDeltaCenter: i,
                u_texture: o,
                u_iconsize: o.size,
                u_custom_img_flag: f,
                u_dir_color: u,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1],
                u_timer: performance.now() - this.See,
                u_speed: a.speed,
                u_size_scale: s,
                u_animate: !!a.animate
            }
        }
        ;
        e.prototype.oC = function(e, r, t) {
            if (!e._map) {
                return
            }
            var i = e.rY(r);
            var a = e._opts;
            if (!a.lineGradient) {
                return
            }
            var n = a.strokeWeight;
            if (n <= 0) {
                return
            }
            if (!e.rC) {
                e.aC(t, a.lineGradient)
            }
            if (a.unit === "meter") {
                n /= r.viewState.resolution
            }
            return {
                u_matrix: r.viewState.mvpMatrix,
                u_meter_per_pixel: r.viewState.resolution,
                u_width: n,
                u_offset: e.zx,
                u_localDeltaCenter: i,
                u_texture: e.rC,
                u_total_distance: e.sC,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.U_ = function(e, r) {
            if (e.F_) {
                if (!e.E_) {
                    e.E_ = r.createTexture(e.F_, {
                        Zi: true
                    });
                    e.E_.bind(r.gl.CLAMP_TO_EDGE, r.gl.LINEAR, r.gl.LINEAR)
                }
            }
        }
        ;
        e.prototype.aC = function(e, r, t) {
            if (!e.rC) {
                var i = createLineGradinetCanvas(t.values);
                e.rC = r.createTexture(i, {
                    Zi: true
                });
                var a = r.gl;
                e.rC.bind(a.CLAMP_TO_EDGE, a.NEAREST, a.NEAREST_MIPMAP_NEAREST)
            }
        }
        ;
        e.prototype.Xx = function(e) {
            var r = e._opts;
            var t = 2;
            var i = [1, 0, 0];
            if (r.strokeStyle === "dashed" && r.strokeDasharray) {
                if (r.strokeDasharray.length >= 3) {
                    t = 3;
                    i = r.strokeDasharray.slice(0, 3)
                } else {
                    t = 2;
                    i = __spreadArrays(r.strokeDasharray.slice(0), [0])
                }
            }
            return {
                Jx: t,
                dash: i
            }
        }
        ;
        e.prototype.N_ = function(e, r, t, i) {
            if (!e.Px) {
                return
            }
            var a = e.Px;
            a.upload(t.context);
            if (!a.uploaded || !a.zs) {
                return
            }
            if (!e.visible) {
                return
            }
            var n = a.zs.buffer;
            var o = this.B_(e, r, t.context);
            if (!o) {
                return
            }
            var f = t.M_();
            f.Ce(o, {
                a_pos: {
                    buffer: n,
                    type: "vec2",
                    Re: n.size,
                    offset: 0
                },
                a_normal: {
                    type: "vec2",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 2
                },
                a_distance: {
                    type: "float",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 4
                },
                a_dir: {
                    type: "vec2",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 5
                }
            }, a.Qx.length * 3, a.Pc.buffer, undefined, undefined, ColorMode.Si, undefined, undefined, 0);
            f.Ni()
        }
        ;
        e.prototype.Ic = function(e, r, t) {
            if (!e.Px) {
                return
            }
            var i = e.Px;
            i.upload(t.context);
            if (!i.uploaded || !i.zs) {
                return
            }
            if (!e.visible) {
                return
            }
            var a = i.zs.buffer;
            var n = this.fb(e, r);
            if (!n) {
                return
            }
            var o = t.Xn();
            o.Ce(n, {
                a_pos: {
                    buffer: a,
                    type: "vec2",
                    Re: a.size,
                    offset: 0
                },
                a_normal: {
                    type: "vec2",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 2
                },
                a_distance: {
                    type: "float",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 4
                },
                a_dir: {
                    type: "vec2",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 5
                }
            }, i.Qx.length * 3, i.Pc.buffer, undefined, undefined, ColorMode.Si, undefined, undefined, 0);
            o.Ni()
        }
        ;
        e.prototype.nC = function(e, r, t) {
            if (!e.Px) {
                return
            }
            var i = e.Px;
            i.upload(t.context);
            if (!i.uploaded || !i.zs) {
                return
            }
            if (!e.visible) {
                return
            }
            var a = i.zs.buffer;
            var n = this.oC(e, r, t.context);
            if (!n) {
                return
            }
            var o = t.sS();
            o.Ce(n, {
                a_pos: {
                    buffer: a,
                    type: "vec2",
                    Re: a.size,
                    offset: 0
                },
                a_normal: {
                    type: "vec2",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 2
                },
                a_distance: {
                    type: "float",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 4
                },
                a_dir: {
                    type: "vec2",
                    buffer: a,
                    Re: a.size,
                    offset: 4 * 5
                }
            }, i.Qx.length * 3, i.Pc.buffer, undefined, undefined, ColorMode.Si, undefined, undefined, 0);
            o.Ni()
        }
        ;
        e.prototype.Tc = function(e, r, t) {
            var i = e._opts;
            if (!i || !i.isOutline) {
                return
            }
            if (!e.Px) {
                return
            }
            var a = e.Px;
            a.upload(t.context);
            if (!a.uploaded && !a.zs) {
                return
            }
            var n = a.zs.buffer;
            if (!e.visible) {
                return
            }
            var o = this.pb(e, r);
            if (!o) {
                return
            }
            var f = t.Xn();
            f.Ce(o, {
                a_pos: {
                    buffer: n,
                    type: "vec2",
                    Re: n.size,
                    offset: 0
                },
                a_normal: {
                    type: "vec2",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 2
                },
                a_distance: {
                    type: "float",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 4
                },
                a_dir: {
                    type: "vec2",
                    buffer: n,
                    Re: n.size,
                    offset: 4 * 5
                }
            }, a.Qx.length * 3, a.Pc.buffer, undefined, undefined, ColorMode.Si, undefined, undefined, 0);
            f.Ni()
        }
        ;
        return e
    }();
    function createLineGradinetCanvas(e) {
        var r = document.createElement("canvas");
        var t = 4096;
        r.height = 1;
        r.width = t;
        var i = r.getContext("2d");
        var a = i.createImageData(t, 1);
        var n = 0;
        for (var o = 0; o < t; o++) {
            var f = void 0;
            for (; n < e.length; n++) {
                if (n === e.length - 1) {
                    f = e[n][1];
                    break
                }
                var s = e[n + 1][0];
                if (o > s * t) {
                    continue
                }
                f = e[n][1];
                break
            }
            f = e[n][1];
            var u = Util$5.color2RgbaArray(f);
            a.data[o * 4] = u[0] * 255;
            a.data[o * 4 + 1] = u[1] * 255;
            a.data[o * 4 + 2] = u[2] * 255;
            a.data[o * 4 + 3] = u[3] * 255
        }
        i.putImageData(a, 0, 0);
        return r
    }
    var Util$6 = AMap["Util"];
    var Support$5 = M["Support"];
    var CircleWebglRender = function() {
        function e() {}
        e.prototype.render = function(e, r, t) {
            if (!e._map) {
                return
            }
            if (!e["getRadius"]()) {
                return
            }
            if (Support$5["safari"]) {
                this.wee(e, r, t)
            } else {
                this.Cb(e, r, t)
            }
        }
        ;
        e.prototype.Cb = function(e, r, t) {
            if (!e.visible) {
                return
            }
            var i = this.Ab(e, r);
            if (!i) {
                return
            }
            var a = t.Un();
            a.Ce(i, {}, 1, undefined, "POINTS", false, ColorMode.Si, undefined, undefined, 0)
        }
        ;
        e.prototype.wee = function(e, r, t) {
            var i = e.Sb;
            if (!i) {
                return
            }
            i.upload(t.context);
            if (!i.upload) {
                return
            }
            if (!e.visible) {
                return
            }
            var a = i.zs;
            var n = this.Ab(e, r);
            if (!n) {
                return
            }
            var o = t.kee();
            o.Ce(n, {
                a_pos: {
                    buffer: a,
                    type: "vec2",
                    Re: a.size,
                    offset: 0
                }
            }, i._b.length, undefined, "POINTS", false, ColorMode.Si, undefined, undefined, 0)
        }
        ;
        e.prototype.Lb = function(e, r, t) {
            if (!e.visible) {
                return
            }
            var i = this.Tb(e, r);
            if (!i) {
                return
            }
            var a = t.Un();
            a.Ce(i, {}, 1, undefined, "POINTS", false, ColorMode.Si, undefined, undefined, 0)
        }
        ;
        e.prototype.Ab = function(e, r) {
            var t = e.KB;
            if (!t) {
                return
            }
            var i = e.rY(r);
            var a = e._opts;
            if (!a.fillColor) {
                return
            }
            var n = Util$6.color2RgbaArray(a.fillColor).slice(0, 3);
            n.push(a.fillOpacity);
            var o = a.radius + (a.strokeWeight || 0);
            var f = [0, a.radius / o];
            var s = [0, 0, 0, 0];
            if (a.strokeColor && a.strokeWeight && a.strokeOpacity) {
                s = Util$6.color2RgbaArray(a.strokeColor).slice(0, 3);
                s.push(a.strokeOpacity)
            }
            return {
                u_pos: t,
                u_color: n,
                u_borderColor: s,
                u_offset: e.zx,
                u_matrix: r.viewState.mvpMatrix,
                u_localDeltaCenter: i,
                u_radius: this.Ib(e),
                u_range: f,
                u_retinaRatio: Support$5.Ue ? 2 : 1,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.Tb = function(e, r) {
            var t = e.KB;
            if (!t) {
                return
            }
            var i = e.rY(r);
            var a = e._opts;
            if (!a.strokeWeight) {
                return
            }
            if (!a.strokeColor) {
                return
            }
            var n = Util$6.color2RgbaArray(a.strokeColor).slice(0, 3);
            n.push(a.strokeOpacity);
            var o = a.radius + a.strokeWeight;
            var f = [a.radius / o, 1];
            var s = n.slice(0, 3);
            s.push(0);
            return {
                u_pos: t,
                u_color: n,
                u_borderColor: s,
                u_offset: e.zx,
                u_matrix: r.viewState.mvpMatrix,
                u_localDeltaCenter: i,
                u_radius: this.Ib(e),
                u_range: f,
                u_retinaRatio: Support$5.Ue ? 2 : 1,
                u_skyHeight: r.viewState.skyHeight,
                u_viewHeight: r.viewState.size[1]
            }
        }
        ;
        e.prototype.Ib = function(e) {
            var r = e._opts;
            var t = r.radius + (r.strokeWeight || 0);
            return Math.min(t, 64)
        }
        ;
        return e
    }();
    var ColorPick = function() {
        function e() {
            this.uA = {};
            this.cdt = [0, 0, 0, 255];
            this.ldt = 256
        }
        e.prototype.add = function(e) {
            this.uA[this.fdt()] = e;
            return __spreadArrays(this.cdt)
        }
        ;
        e.prototype.find = function(e) {
            var r = this.ldt;
            var t = r * r;
            for (var i in this.uA) {
                var a = Number(i);
                if (isEqual(this.uA[a], e)) {
                    return [a % r, ~~(a / r) % r, ~~(a / t) % r, 255]
                }
            }
            return undefined
        }
        ;
        e.prototype.clear = function() {
            this.cdt = [0, 0, 0, 255];
            this.uA = {}
        }
        ;
        e.prototype.ddt = function() {
            return __spreadArrays(this.cdt)
        }
        ;
        e.prototype.GV = function(e) {
            var r = this.ldt;
            var t = r * r;
            var i = e[0]
              , a = e[1]
              , n = e[2];
            var o = i + a * r + n * t;
            return this.uA[o]
        }
        ;
        e.prototype.fdt = function() {
            var e = this.ldt;
            var r = e * e;
            var t = this.cdt
              , i = t[0]
              , a = t[1]
              , n = t[2];
            var o = i + a * e + n * r + 1;
            this.cdt[0] = o % e;
            this.cdt[1] = ~~(o / e) % e;
            this.cdt[2] = ~~(o / r) % e;
            return o
        }
        ;
        return e
    }();
    var Support$6 = M["Support"];
    var scale$3 = Support$6.scale;
    var OverlayRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r !== null && r.apply(this, arguments) || this;
            e.ydt = new ColorPick;
            e.uC = new PolygonWebglRender(e.ydt);
            e.cC = new PolylineWebglRender;
            e.dC = new CircleWebglRender;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            var n = t.viewState.zoom;
            this.wf = t;
            this.vdt = e;
            this.Tee = r.sr;
            this.pu = scale$3;
            if (r.sr) {
                for (var o = 0, f = r.sr; o < f.length; o++) {
                    var s = f[o];
                    if (n < s._opts.zooms[0] || n > s._opts.zooms[1]) {
                        continue
                    }
                    switch (s.CLASS_NAME) {
                    case "Overlay.Polygon":
                    case "Overlay.Rectangle":
                    case "Overlay.Circle":
                    case "Overlay.Ellipse":
                        this.hC(s, t, e);
                        break;
                    case "Overlay.Polyline":
                    case "Overlay.BezierCurve":
                        this.lC(s, t, e);
                        break;
                    case "Overlay.CircleMarker":
                        this.fC(s, t, e);
                        break
                    }
                }
            }
        }
        ;
        e.prototype.pickRender = function(e, r) {
            if (!this.vdt) {
                return
            }
            var t = this.vdt.context;
            var i = this.vdt.context.gl;
            if (!e) {
                return
            }
            var a = e.size[0] * scale$3;
            var n = e.size[1] * scale$3;
            var o = e.viewState.bounds.toString();
            var f = false;
            if (!this.bee) {
                this.bee = this.Aee(i, a, n, o);
                f = true
            } else if (a !== this.bee.w || n !== this.bee.h) {
                this.Iee(this.bee, i, a, n, o);
                f = true
            } else if (o !== this.bee.bounds) {
                this.bee.bounds = o;
                f = true
            }
            if (f) {
                t.bindFramebuffer.set(this.bee);
                t.be.set([0, 0, a, n]);
                t.clear({
                    color: true,
                    stencil: true,
                    depth: true
                });
                if (this.Tee) {
                    var s = e.viewState.zoom;
                    for (var u = 0, l = this.Tee; u < l.length; u++) {
                        var v = l[u];
                        var c = v;
                        if (s < v._opts.zooms[0] || s > v._opts.zooms[1]) {
                            continue
                        }
                        if (v.CLASS_NAME === "Overlay.Polygon" && c._opts.extrusionHeight > 0) {
                            this.uC.S_(c, e, this.vdt, false, true)
                        }
                    }
                }
                t.bindFramebuffer.set(null)
            }
            return this.bee
        }
        ;
        e.prototype.mdt = function() {
            if (this.vdt) {
                var e = this.vdt.context;
                e.bindFramebuffer.set(null);
                e.setDirty()
            }
        }
        ;
        e.prototype.destroy = function() {
            if (this.ydt && this.ydt["clear"]) {
                this.ydt.clear();
                delete this.ydt
            }
            if (this.bee && this.vdt) {
                var e = this.vdt.context.gl;
                if (e) {
                    e.deleteFramebuffer(this.bee)
                }
                this.bee = null;
                delete this.bee
            }
            if (this.Tee) {
                this.Tee = []
            }
        }
        ;
        e.prototype.hC = function(e, r, t) {
            this.uC.render(e, r, t)
        }
        ;
        e.prototype.lC = function(e, r, t) {
            this.cC.render(e, r, t)
        }
        ;
        e.prototype.fC = function(e, r, t) {
            this.dC.render(e, r, t)
        }
        ;
        e.prototype.Aee = function(e, r, t, i) {
            var a = e.createFramebuffer();
            a.w = r;
            a.h = t;
            a.bounds = i;
            var n = e.createTexture();
            a.texture = n;
            e.bindTexture(e.TEXTURE_2D, n);
            e.texImage2D(e.TEXTURE_2D, 0, e.RGBA, r, t, 0, e.RGBA, e.UNSIGNED_BYTE, null);
            var o = e.createRenderbuffer();
            a.AC = o;
            e.bindRenderbuffer(e.RENDERBUFFER, o);
            e.renderbufferStorage(e.RENDERBUFFER, e.DEPTH_COMPONENT16, r, t);
            e.bindFramebuffer(e.FRAMEBUFFER, a);
            e.framebufferTexture2D(e.FRAMEBUFFER, e.COLOR_ATTACHMENT0, e.TEXTURE_2D, n, 0);
            e.framebufferRenderbuffer(e.FRAMEBUFFER, e.DEPTH_ATTACHMENT, e.RENDERBUFFER, o);
            var f = e.checkFramebufferStatus(e.FRAMEBUFFER);
            if (e.FRAMEBUFFER_COMPLETE !== f) {
                console.log("Frame buffer object is incomplete: " + f.toString());
                return
            }
            e.bindFramebuffer(e.FRAMEBUFFER, null);
            e.bindTexture(e.TEXTURE_2D, null);
            e.bindRenderbuffer(e.RENDERBUFFER, null);
            return a
        }
        ;
        e.prototype.Iee = function(e, r, t, i, a) {
            e.w = t;
            e.h = i;
            e.bounds = a;
            r.bindTexture(r.TEXTURE_2D, e.texture);
            r.texImage2D(r.TEXTURE_2D, 0, r.RGBA, t, i, 0, r.RGBA, r.UNSIGNED_BYTE, null);
            r.bindRenderbuffer(r.RENDERBUFFER, e.AC);
            r.renderbufferStorage(r.RENDERBUFFER, r.DEPTH_COMPONENT16, t, i);
            r.bindTexture(r.TEXTURE_2D, null);
            r.bindRenderbuffer(r.RENDERBUFFER, null)
        }
        ;
        return e
    }(LayerRender);
    var TileState$3 = M["TileState"];
    var VectorTrafficRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            var a = t.viewState.optimalZoom;
            if (a < i.zooms[0] || a > i.zooms[1]) {
                return
            }
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            var n = r.tiles[0];
            if (n && n.length) {
                for (var o = 0, f = n; o < f.length; o++) {
                    var s = f[o];
                    if (s.status !== TileState$3.LOADED) {
                        continue
                    }
                    this.Ic(e, s, t, i)
                }
            }
        }
        ;
        e.prototype.destroy = function() {}
        ;
        e.prototype.Ic = function(e, r, t, i) {
            var a = e.Vn();
            if (!r.data) {
                assert(r.data, "tile have no data");
                return
            }
            var n = r.data;
            var o = n.$c;
            if (!o) {
                return
            }
            o.upload(a.context, true);
            var f = o.zs.buffer;
            var s = o.Pc.buffer;
            var u = n.Uc;
            if (!u) {
                return
            }
            var l = u.jc;
            var v = t.viewState.optimalZoom;
            var c = n.Sa;
            var h = i.opacity;
            var d = 1;
            if (t.viewState.viewMode === "3D") {
                d = t.map.getView().EF()
            }
            for (var _ = 0, g = l.length; _ < g; _++) {
                var y = l[_];
                var m = i.HY[y.color] || i.HY[4];
                var p = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_width: y.width,
                    u_color: [m[0], m[1], m[2], m[3] * h],
                    u_border: 0,
                    u_dash: undefined,
                    u_localDeltaCenter: c,
                    u_skyHeight: d,
                    u_viewHeight: t.viewState.size[1]
                };
                var b = f.size;
                a.context.getExtension("OES_element_index_uint");
                a.Ce(p, {
                    a_pos: {
                        type: "vec2",
                        buffer: f,
                        Re: b,
                        offset: 4 * 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: f,
                        Re: b,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: f,
                        Re: b,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: f,
                        Re: b,
                        offset: 4 * 5
                    }
                }, y.length, s, "TRIANGLES", i.depthTest, ColorMode.Si, i.rejectMapMask || !t.map.getMask() ? undefined : StencilMode.writeWithStencil, undefined, y.offset)
            }
        }
        ;
        return e
    }(LayerRender);
    var VTRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i) {
            var a = e.context;
            var n = a.gl;
            return
        }
        ;
        return e
    }(LayerRender);
    var skyFragmentString = "precision highp float;\nuniform float u_opacity;\n\nvarying vec4 v_color;\nvarying vec4 v_pos;\n\nvoid main() {\n  gl_FragColor = v_color;\n  // float x = v_pos.x / v_pos.w;\n  gl_FragColor.a *= u_opacity;\n}";
    var skyVertextString = "precision highp float;\nuniform vec2 u_skyline;\nuniform mat4 u_mvpMatrix;\nattribute vec4 a_pos;\nattribute vec4 a_color;\nvarying vec4 v_color;\nvarying vec4 v_pos;\n\nvoid main() {\n    vec4 pos = vec4(a_pos.xy, 0, 1);\n    // vec4 pos = u_mvpMatrix * vec4(a_pos.xyz, 1);\n    gl_Position = pos;\n    v_color = a_color;\n    v_pos = gl_Position;\n}\n";
    var skyPatternFragmentString = "precision highp float;\nuniform sampler2D u_texture;\n\nuniform float u_opacity;\nuniform float u_rotation;\nvarying vec2 v_coord;\nvarying float v_opacity;\nvoid main() {\n  // gl_FragColor = vec4(1.0,0,0,1.0);\n  gl_FragColor = texture2D(u_texture, vec2(v_coord.x+u_rotation, v_coord.y));\n  // // float x = v_pos.x / v_pos.w;\n  gl_FragColor.a *= u_opacity*v_opacity;\n}";
    var skyPatternVertextString = "precision highp float;\nuniform vec2 u_skyline;\nuniform mat4 u_mvpMatrix;\nattribute vec4 a_pos;\nattribute float a_opacity;\nattribute vec2 a_coord;\n\n\nvarying vec2 v_coord;\nvarying float v_opacity;\n\nvoid main() {\n    vec4 pos = u_mvpMatrix * vec4(a_pos.xyz, 1);\n    gl_Position = pos;\n    v_coord = a_coord;\n    v_opacity = a_opacity;\n}\n";
    var skyUniforms = {
        u_mvpMatrix: "mat4",
        u_opacity: "float",
        u_skyline: "vec2"
    };
    var skyAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec4"
        },
        a_color: {
            Me: "float32",
            Oe: "vec4"
        }
    };
    function getAtmosphereTexture(e, r) {
        var t = e.vn.nX("00001", "2", "texture", r);
        if (t) {
            return "https://" + t
        }
        return ""
    }
    function getSkyTexture(e, r) {
        var t = e.vn.nX("00001", "1", "texture", r);
        if (t) {
            return "https://" + t
        }
        return ""
    }
    var sky = {
        uniforms: skyUniforms,
        attributes: skyAttributes,
        vertexSource: skyVertextString,
        fragmentSource: skyFragmentString
    };
    var skyPatternUniforms = {
        u_mvpMatrix: "mat4",
        u_opacity: "float",
        u_skyline: "vec2",
        u_texture: "sampler2D",
        u_rotation: "float"
    };
    var skyPatternAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec4"
        },
        a_opacity: {
            Me: "float32",
            Oe: "float"
        },
        a_coord: {
            Me: "float32",
            Oe: "vec2"
        }
    };
    var skyPattern = {
        uniforms: skyPatternUniforms,
        attributes: skyPatternAttributes,
        vertexSource: skyPatternVertextString,
        fragmentSource: skyPatternFragmentString
    };
    var SmartArrayBuffer$3 = M["SmartArrayBuffer"];
    var SkyLayerRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a, n) {
            if (!a.map.mapStyle.vn) {
                return
            }
            var o = getAtmosphereTexture(a.map.mapStyle, t.viewState.optimalZoom);
            var f = o ? a.map.getImage(o) : undefined;
            if (f) {
                this.jB(e, t, a, o, t.viewState.zoom < 12 ? 1 : 0)
            } else if (o && a.map.I_ && !a.map.I_.MH(o)) {
                a.map.addImage(o, {
                    url: o,
                    filter: "LINEAR",
                    wrap: "REPEAT",
                    cb: function() {
                        return a.map.setNeedUpdate(true)
                    }
                })
            }
            if (!o || !f) {
                this.NB(e, t, t.map.sX(t.viewState.optimalZoom), t.viewState.zoom < 12 ? 1 : 0)
            }
            var s = getSkyTexture(a.map.mapStyle, t.viewState.optimalZoom);
            var u = s ? a.map.getImage(s) : undefined;
            if (u) {
                this.jB(e, t, a, s, step(t.viewState.zoom - 11, 1, 0))
            } else if (s && a.map.I_ && !a.map.I_.MH(s)) {
                a.map.addImage(s, {
                    url: s,
                    filter: "LINEAR",
                    wrap: "REPEAT",
                    cb: function() {
                        return a.map.setNeedUpdate(true)
                    }
                })
            }
            if (!s || !u) {
                this.NB(e, t, t.map.JF(t.viewState.optimalZoom), step(t.viewState.zoom - 11, 1, 0))
            }
            return
        }
        ;
        e.prototype.NB = function(e, r, t, i) {
            var a = e.rS();
            var n = r.viewState;
            var o = e.context;
            var f = r.map.getView();
            var s = n.size
              , u = n.pitch
              , l = n.optimalZoom;
            if (f.type === "2D") {
                return
            }
            var v = f.Uu();
            v = this.yz(f.getStatus().pitch, v, f.getStatus().size[1], l);
            if (v <= 0 || u <= 0) {
                return
            }
            var c = r.map;
            var h = this.WF(v, t, n, f);
            if (!this.zs) {
                this.zs = new SmartArrayBuffer$3(o,h,32,"ARRAY_BUFFER")
            } else {
                this.zs.update(h)
            }
            var d = this.zs.buffer;
            a.Ce({
                u_opacity: i,
                u_mvpMatrix: n.mvpMatrix,
                u_skyline: [0, 0]
            }, {
                a_pos: {
                    type: "vec4",
                    buffer: d,
                    Re: d.size,
                    offset: 0
                },
                a_color: {
                    type: "vec4",
                    buffer: d,
                    Re: d.size,
                    offset: 16
                }
            }, d.length, undefined, "TRIANGLES", undefined, ColorMode.Si, undefined)
        }
        ;
        e.prototype.jB = function(e, r, t, i, a) {
            if (a === 0) {
                return
            }
            var n = e.EB();
            var o = e.rS();
            var f = r.viewState;
            var s = e.context;
            var u = r.map.getView();
            var l = f.size
              , v = f.pitch
              , c = f.optimalZoom;
            if (u.type === "2D") {
                return
            }
            var h = u.Uu();
            h = this.yz(u.getStatus().pitch, h, u.getStatus().size[1], c);
            if (h <= 0 || v <= 0) {
                return
            }
            var d = r.map;
            var _ = d.JF(f.optimalZoom);
            var g = (180 - f.rotation) / 360;
            var y = this.WB(h, f, u);
            if (!this.zs) {
                this.zs = new SmartArrayBuffer$3(s,y,32,"ARRAY_BUFFER")
            } else {
                this.zs.update(y)
            }
            var m = this.zs.buffer;
            var p = t.map.getImage(i);
            if (p) {
                n.Ce({
                    u_opacity: a,
                    u_mvpMatrix: f.mvpMatrix,
                    u_skyline: [0, 0],
                    u_texture: p,
                    u_rotation: g
                }, {
                    a_pos: {
                        type: "vec4",
                        buffer: m,
                        Re: m.size,
                        offset: 0
                    },
                    a_opacity: {
                        type: "float",
                        buffer: m,
                        Re: m.size,
                        offset: 16
                    },
                    a_coord: {
                        type: "vec2",
                        buffer: m,
                        Re: m.size,
                        offset: 24
                    }
                }, m.length / 2, undefined, "TRIANGLES", undefined, ColorMode.Si, undefined);
                o.Ce({
                    u_opacity: 1,
                    u_mvpMatrix: f.mvpMatrix,
                    u_skyline: [0, 0]
                }, {
                    a_pos: {
                        type: "vec4",
                        buffer: m,
                        Re: m.size,
                        offset: 0
                    },
                    a_color: {
                        type: "vec4",
                        buffer: m,
                        Re: m.size,
                        offset: 16
                    }
                }, m.length / 2, undefined, "TRIANGLES", undefined, ColorMode.Si, undefined, undefined, m.length / 2)
            }
        }
        ;
        e.prototype.WF = function(e, r, t, i) {
            var a = t.size[1] / 2;
            var n = t.pitch
              , o = t.zoom;
            var f = [-1, 1 - e / a];
            var s = [1, 1 - e / a];
            r[3] = 1;
            var u = i.map.mapStyle.dn(30001, 1, o);
            var l = [.988, .976, .949, 1];
            if (u && u.faceColor) {
                l = u.faceColor.normalize()
            }
            var v = __spreadArrays(l);
            v[3] = 1;
            var c = __spreadArrays(l) || [1, 1, 1, 0];
            c[3] = 0;
            var h = __spreadArrays(l) || [1, 1, 1, Math.min(.1, 1 / o)];
            h[3] = Math.min(.1, 5 / o);
            var d = __spreadArrays(r) || [1, 1, 1, 1];
            d[3] = -.7;
            var _ = new Float32Array(__spreadArrays([f[0], f[1], 0, 0], v, [-1, 1, 0, 0], r, [s[0], s[1], 0, 0], v, [s[0], s[1], 0, 0], v, [-1, 1, 0, 0], r, [1, 1, 0, 0], r, [f[0], f[1], 0, 0], h, [-1, 1, 0 / 3, 0], c, [s[0], s[1], 0, 0], h, [s[0], s[1], 0, 0], h, [-1, 1, 0 / 3, 0], c, [1, 1, 0 / 3, 0], c));
            return _
        }
        ;
        e.prototype.WB = function(e, r, t) {
            var i = r.size[1] / 2;
            var a = r.pitch
              , n = r.zoom
              , o = r.rotation;
            var f = t.X(0, e);
            var s = t.X(r.size[0], e);
            var u = t.X(0, e + 0);
            var l = t.X(r.size[0], e + 0);
            var v = .5;
            var c = t.pz().dz() * v;
            var h = 0;
            var d = r.fov / Math.PI / 2 / 2;
            var _ = 0 - d;
            var g = 0 + d;
            var y = -0;
            var m = 1;
            var p = t.map.mapStyle.dn(30001, 1, n);
            var b = [.988, .976, .949, 1];
            if (p && p.faceColor) {
                b = p.faceColor.normalize()
            }
            var x = __spreadArrays(b) || [1, 1, 1, 0];
            x[3] = 0;
            var M = __spreadArrays(b) || [1, 1, 1, Math.min(.1, 1 / n)];
            M[3] = Math.min(.1, 5 / n);
            var C = new Float32Array(__spreadArrays([f[0], f[1], h, 0, 1, 0, _, m, f[0], f[1], c, 0, 1, 0, _, y, s[0], s[1], h, 0, 1, 0, g, m, s[0], s[1], h, 0, 1, 0, g, m, s[0], s[1], c, 0, 1, 0, g, y, f[0], f[1], c, 0, 1, 0, _, y, u[0], u[1], 0, 0], M, [u[0], u[1], c / 3, 0], x, [l[0], l[1], 0, 0], M, [l[0], l[1], 0, 0], M, [l[0], l[1], c / 3, 0], x, [u[0], u[1], c / 3, 0], x));
            return C
        }
        ;
        e.prototype.yz = function(e, r, t, i) {
            var a = Math.max(e / 100 - .4, 0);
            var n = Math.max((i - 24) * 4, 0) / 100;
            return t * (a - n)
        }
        ;
        return e
    }(LayerRender);
    function step(e, r, t) {
        return Math.max(Math.min(e, r), t)
    }
    var indoorStyles = {
        PoiStyle: [{
            $: {
                mainkey: "10001",
                subkey: "1"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,2,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "2"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,7,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "3"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,8,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "4"
            },
            StyleItem: [{
                $: {
                    level: "18",
                    desc: "50,25,2,1,0,0xff84638E,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "19,20",
                    desc: "50,25,1,1,12,0xff84638E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "5"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,59,2,1,0,0xff84638E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "6"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,3,1,1,12,0xff84638E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "7"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,4,1,1,12,0xff816B4E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "8"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,9,1,1,12,0xff935676,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "9"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,10,1,1,12,0xff935676,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "10"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,14,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "11"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,15,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "12"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,17,1,1,12,0xff816B4E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "13"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,18,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "14"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,19,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "15"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,22,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "16"
            },
            StyleItem: [{
                $: {
                    level: "20",
                    desc: "50,24,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "17"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,1,0,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "18"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,27,1,1,12,0xff84638E,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "19"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,36,1,1,12,0xff8E3A3D,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "20"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,39,1,1,12,0xff935676,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "21"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,44,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,44,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "22"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,45,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,45,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "23"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,46,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,46,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "24"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,47,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,47,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "25"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,48,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,48,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "26"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,49,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,49,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "27"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,50,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,50,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "28"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,51,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,51,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "29"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,52,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,52,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "30"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,53,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,53,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "31"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,54,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,54,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "32"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,55,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,55,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "33"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,56,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,56,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "34"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,57,2,1,0,0xff696F71,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,57,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "35"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,6,2,1,0,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "36"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,12,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "37"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,13,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "38"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,16,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "39"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,20,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,20,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "40"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,5,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,5,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "41"
            },
            StyleItem: [{
                $: {
                    level: "20",
                    desc: "50,28,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "42"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,29,1,1,12,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "43"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,31,1,1,12,0xff000000,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "44"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,32,2,1,0,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "45"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,33,2,1,0,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "46"
            },
            StyleItem: [{
                $: {
                    level: "18",
                    desc: "50,34,2,1,0,0xff935676,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "19,20",
                    desc: "50,34,1,1,12,0xff935676,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "47"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,35,2,1,0,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "48"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,37,1,1,12,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "49"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,38,2,1,0,0xff4C6776,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "50"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,42,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "51"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,41,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "52"
            },
            StyleItem: [{
                $: {
                    level: "20",
                    desc: "50,43,1,1,12,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "53"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,58,2,1,0,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "54"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,21,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "55"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,30,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "56"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,1,0,1,12,0xff000000,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "57"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,23,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "58"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,40,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "59"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,11,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "60"
            },
            StyleItem: [{
                $: {
                    level: "16,17",
                    desc: "50,1,0,1,20,0xff6590a6,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "62"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,61,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "63"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,60,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "64"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,62,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,62,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "65"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,63,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,63,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "66"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,64,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,64,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "67"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,65,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,65,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "68"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,66,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,66,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "69"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,67,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,67,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "70"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,68,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,68,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "71"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,69,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,69,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "72"
            },
            StyleItem: [{
                $: {
                    level: "17",
                    desc: "50,70,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "18,19,20",
                    desc: "50,70,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "73"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,71,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "74"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "50,72,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "75"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,73,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,73,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "76"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,74,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,74,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "77"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,75,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,75,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "78"
            },
            StyleItem: [{
                $: {
                    level: "18,19",
                    desc: "50,76,2,1,0,0xff62686B,0xffffffff,0x0,0"
                }
            }, {
                $: {
                    level: "20",
                    desc: "50,76,1,1,12,0xff62686B,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "79"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,81,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "80"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,89,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "81"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,85,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "82"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,90,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "83"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,83,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "84"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,103,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "85"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,98,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "86"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,86,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "87"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,87,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "88"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,111,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "89"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,88,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "90"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,91,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "91"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,93,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "92"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,113,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "93"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,95,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "94"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,100,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "95"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,97,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "96"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,94,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "97"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,108,1,1,12,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "98"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,1,2,1,0,0xff69859F,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "99"
            },
            StyleItem: [{
                $: {
                    level: "18,19,20",
                    desc: "50,1,0,1,12,0xff696F71,0xffffffff,0x0,1"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "105"
            },
            StyleItem: [{
                $: {
                    level: "16,17,18",
                    desc: "50,1,0,1,14,0xff696F71,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "106"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "50,1,0,1,12,0xff4d4e51,0xffffffff,0x0,0"
                }
            }]
        }, {
            $: {
                mainkey: "10001",
                subkey: "107"
            },
            StyleItem: [{
                $: {
                    level: "16,17,18,19,20",
                    desc: "50,109,1,1,12,0xff4d4e51,0xffffffff,0x0,0"
                }
            }]
        }],
        LineStyle: [{
            $: {
                mainkey: "20001",
                subkey: "1"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "88,2,10,0,0,0,0xffe68e00,0xffe68e00,0xffe68e00,0xffe68e00"
                }
            }]
        }, {
            $: {
                mainkey: "20001",
                subkey: "2"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "88,2,8,0,0,0,0xffababab,0xffababab,0xff5e492f,0xffffe080"
                }
            }]
        }],
        RegionStyle: [{
            $: {
                mainkey: "30001",
                subkey: "1"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "90,1,0xfffcfbed"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "99"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "90,1,0x1a151b2b"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "98"
            },
            StyleItem: [{
                $: {
                    level: "19,20",
                    desc: "90,1,0x8c151b2b"
                }
            }]
        }],
        BuildingStyle: [{
            $: {
                mainkey: "30001",
                subkey: "2"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "99,0xffe6e6e6,0xff858585,0xffcfcfcf,0xffb8b8b8,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "3"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffe6e6e6,0xff858585,0xffcfcfcf,0xffb8b8b8,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "4"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffbfd6e9 ,0xff6f7c87,0xffacc0d1,0xff99abba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "5"
            },
            StyleItem: [{
                $: {
                    level: "17,18,19,20",
                    desc: "99,0xffcce3e8 ,0xff778487,0xffb7ccd0,0xffa3b6ba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "6"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffcce3e8,0xff778487,0xffb7ccd0,0xffa3b6ba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "7"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffe5dae8,0xff857f87,0xffcec4d0,0xffb7aeba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "8"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xfffaeccd,0xff948c79,0xffe1d4b8,0xffc8bda4,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "9"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xff90bae8,0xff546d87,0xff81a7d0,0xff7395ba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "10"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xfff0e3df,0xff8f8d8c,0xffd8ccc8,0xffc0b6b2,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "11"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffbfd3e9,0xff6f7b87,0xffacbdd1,0xff99a9ba,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "12"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffdae2f4,0xff7f848f,0xffc4cbdb,0xffaeb5c3,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "13"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xff95e3f5,0xff598791,0xff86ccdc,0xff77b6c4,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "14"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffc9edf5,0xff778d91,0xffb5d5dc,0xffa1bec4,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "15"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xfff4c4ba,0xff8f726d,0xffdbb0a7,0xffc39d95,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "16"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xffdbe8b3,0xff7f8768,0xffc5d0a1,0xffafba8f,255"
                }
            }]
        }, {
            $: {
                mainkey: "30001",
                subkey: "17"
            },
            StyleItem: [{
                $: {
                    level: "15,16,17,18,19,20",
                    desc: "99,0xfff5cedc,0xff917a82,0xffdcb9c6,0xffc4a5b0,255"
                }
            }]
        }]
    };
    var allowedStyleKeys = ["strokeColor", "strokeOpacity", "strokeWeight", "fillColor", "fillOpacity", "strokeStyle", "strokeDasharray", "fontSize"];
    var indoorStyleSet = {
        setOptions: function(e) {},
        init: function() {
            this.NC = {};
            this.UC(indoorStyles);
            this.WC = [];
            return this.NC
        },
        GC: function(e) {
            var r = AMap.getConfig()["protocol"] + "://webapi.amap.com" + "/images/indoor_icon/" + ("36/" + e + "@2x") + ".png";
            return r
        },
        HC: function(e) {
            var r = true;
            for (var t = 13; t <= 22; t++) {
                var i = this.VC(e, t);
                if (i && (i.fontSize > 0 || i.iconId)) {
                    r = false;
                    break
                }
            }
            return r
        },
        VC: function(e, r) {
            return this.Tn(e, r)
        },
        ZC: function(e, r) {
            return this.Tn(e, r)
        },
        Pr: function(e, r) {
            return this.Tn(e, r)
        },
        YC: function(e, r) {
            var t = allowedStyleKeys;
            if (r) {
                for (var i in r) {
                    if (r.hasOwnProperty(i) && t.indexOf(i) >= 0 && e[i] === void 0) {
                        e[i] = r[i]
                    }
                }
            }
            return e
        },
        JC: function(e, r) {
            if (!e) {
                return null
            }
            var t = allowedStyleKeys;
            var i = {};
            for (var a = 0, n = t.length; a < n; a++) {
                var o = t[a];
                if (e.hasOwnProperty(o) && e[o] !== void 0) {
                    i[o] = e[o]
                }
            }
            this.YC(i, r);
            return i
        },
        Tn: function(e, r) {
            var t = e[0];
            var i = e[1];
            var a = t + "_" + i;
            r = Math.round(r);
            var n = this.NC[a];
            if (!n) {
                if (this.WC.indexOf(a) < 0) {
                    this.WC.push(a)
                }
                return null
            }
            for (var o = 0, f = n.length; o < f; o++) {
                if (n[o].levels.indexOf("" + r) >= 0) {
                    return n[o].styleOpts
                }
            }
            if (r > 20) {
                var s = n[n.length - 1];
                if (s.levels.indexOf("20") >= 0) {
                    s.levels.push("" + r);
                    return s.styleOpts
                }
            }
            return null
        },
        UC: function(e) {
            for (var r in e) {
                if (e.hasOwnProperty(r)) {
                    this.XC(e[r], r)
                }
            }
        },
        XC: function(e, r) {
            for (var t = 0, i = e.length; t < i; t++) {
                this.qC(e[t], r)
            }
        },
        qC: function(e, r) {
            var t = e["$"];
            if (!t) {
                return
            }
            var i = t["mainkey"];
            var a = t["subkey"];
            var n = e["StyleItem"];
            var o = i + "_" + a;
            if (this.NC[o])
                ;this.NC[o] = this.QC(n, r)
        },
        QC: function(e, r) {
            var t = [];
            for (var i = 0, a = e.length; i < a; i++) {
                var n = this.KC(e[i], r);
                if (!n) {
                    continue
                }
                t.push(n)
            }
            return t
        },
        KC: function(e, r) {
            var t = e["$"];
            if (!t) {
                return
            }
            var i = {};
            i["levels"] = t["level"].split(",");
            i["desc"] = t["desc"].split(",");
            i["styleOpts"] = this.$I(i, i.desc, r);
            i["styleOpts"]["levels"] = i.levels;
            return i
        },
        BI: function(e) {
            var r = [];
            for (var t = 0, i = e.length; t < i; t += 2) {
                r.push(parseInt(e.substr(t, 2), 16))
            }
            r.push(r.shift());
            r = r.slice(0, 3);
            var a = "rgb(" + r.join(",") + ")";
            return a
        },
        WI: function(e) {
            if (e.trim) {
                e = e.trim()
            }
            var r = this.BI(e.substr(2, 8));
            return r
        },
        GI: function() {
            return this.HI++
        },
        $I: function(e, r, t) {
            var i;
            switch (t) {
            case "PoiStyle":
                i = {
                    iconId: parseInt(r[1], 10),
                    fontSize: Math.min(15, parseInt(r[4], 10) || 0),
                    fillColor: this.WI(r[5]),
                    strokeColor: this.WI(r[6]),
                    VI: parseInt(r[8], 10) === 0
                };
                if (i.iconId === 1) {
                    i.iconId = 0
                }
                break;
            case "LineStyle":
                i = {
                    fillColor: "rgb(252, 249, 242)",
                    strokeColor: this.WI(r[7]),
                    strokeWeight: Math.max(2, Math.round(parseInt(r[2], 10) / 3))
                };
                break;
            case "RegionStyle":
                i = {
                    fillColor: this.WI(r[2])
                };
                break;
            case "BuildingStyle":
                i = {
                    fillColor: this.WI(r[1]),
                    strokeColor: this.WI(r[2])
                };
                break;
            default:
                return null
            }
            i["_uid"] = this.GI();
            return i
        }
    };
    var assign$1 = M["lodash"]["assign"];
    var outLineStyle = {
        high: {
            fillColor: "rgb(252, 249, 242)",
            strokeColor: "rgb(230,142,0)"
        },
        unHigh: {
            fillColor: "rgb(252, 249, 242)",
            strokeColor: "rgb(171,171,171)"
        }
    };
    var IndoorRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e._config = AMap["getConfig"]();
            e.YI = [17, zoomRange[1]];
            e.l$ = indoorStyleSet.init();
            e.a$ = new AMap["LabelsLayer"]({
                opacity: 1,
                collision: true,
                allowCollision: true,
                zIndex: 9,
                zooms: [17, zoomRange[1]],
                visible: false
            });
            e.GL = false;
            e.i$ = false;
            e.WL = "";
            e._map = null;
            e.f$ = false;
            e.UL = null;
            e.pD = false;
            e.t$();
            e.sZ = false;
            return e
        }
        e.prototype["getContainer"] = function() {
            return document.createElement("div")
        }
        ;
        e.prototype.renderFrame = function(e, r, t, i, a) {}
        ;
        e.prototype.render = function(e) {
            var r = this;
            var t = e.data;
            if (!t || t.length < 1 || !this.gs) {
                return false
            }
            this._map = this.gs.map;
            if (!this.sZ) {
                this.sZ = true;
                this._map.on("click", function(e) {
                    if (r.e$) {
                        r.e$.hide()
                    }
                    r.gs.oZ()
                })
            }
            var i = this.gs.ZI();
            var a = i.indoor
              , n = i.floorInfo;
            if (i && i["visible"] === false) {
                return false
            }
            var o = this.qI(t);
            this.UL = t;
            if (!o) {
                return false
            }
            if (!i["changeFloor"]) {
                var f = {
                    centerId: o
                };
                this.gs.setRenderData(f)
            }
            var s = Object.keys(t);
            for (var u = 0; u < s.length; u++) {
                var l = s[u];
                var v = t[l];
                var c = v["building"];
                if (!c || !c["properties"]) {
                    continue
                }
                var h = c["properties"]["outside_outline"];
                var d = this.gs["getIntersect"](h) || this.gs["getRingInRing"](h);
                if (!d) {
                    continue
                }
                if (!a[l]) {
                    var _ = "1";
                    if (n[l]) {
                        _ = n[l]
                    }
                    this.gS(v, _)
                } else {
                    var g = a[l]["curFloor"];
                    var _ = n[l];
                    if (g !== _) {
                        this.gS(v, _)
                    }
                }
            }
        }
        ;
        e.prototype.HF = function(e) {
            this.gs = e
        }
        ;
        e.prototype.t$ = function() {
            var e = this;
            if (!this.i$) {
                AMap["plugin"]("AMap.IndoorMapFloorBar", function() {
                    e.i$ = true
                })
            }
        }
        ;
        e.prototype.QI = function(e, r) {
            var o = this;
            var t = this.gs.ZI();
            var i = t.floorInfo
              , a = t.showId
              , n = t.centerId
              , f = t.hideFloorBar;
            var s = null;
            var u = null;
            var l = Number(i[r]) || 1;
            var v = e.properties;
            if (v && v["floor_nonas"]) {
                s = v["floor_nonas"];
                u = v["floor_indexs"]
            }
            if (!s || !u) {
                return false
            }
            if (u.length > 1) {
                if (u[0] > u[1]) {
                    s = s.reverse();
                    u = u.reverse()
                }
            }
            if (!this.e$ && this.i$) {
                if (M && M["IndoorMapFloorBar"]) {
                    this.e$ = new M["IndoorMapFloorBar"]({
                        floors: s,
                        floorIndexs: u,
                        currentFloor: l
                    });
                    this.WL = n;
                    this.gs.setBar(this.e$);
                    this.gs.setRenderData({
                        barCenterId: this.WL
                    })
                }
                if (!this.e$) {
                    return false
                }
                this._map.addControl(this.e$);
                this.e$.on("floorchange", function(e) {
                    o.pD = true;
                    var r = e["currentIndex"];
                    var t = o.gs.ZI();
                    var i = r;
                    var a = t.centerId;
                    o.gs.n$(a, i);
                    var n = o._map.Mv.getSource(o.gs.co());
                    if (a) {
                        o.gs.setRenderData({
                            changeFloor: true
                        });
                        n.JR(a, i)
                    }
                })
            } else if (this.e$) {
                this.WL = n;
                this.e$["setOption"]({
                    floors: s,
                    floorIndexs: u,
                    currentFloor: l
                });
                this.gs.setRenderData({
                    barCenterId: this.WL
                })
            }
            if (f && this.e$) {
                this.e$.hide()
            }
            if (!this.GL && this._map) {
                this.GL = true;
                this._map.on("moveend", function() {
                    var e = o.vD();
                    if (e) {
                        var r = {
                            centerId: e
                        };
                        o.gs.setRenderData(r)
                    }
                    o.gs.setRenderData({
                        changeFloor: false
                    });
                    o.$ee();
                    o.gs.uD()
                })
            }
        }
        ;
        e.prototype.$ee = function() {
            var e = 1;
            var r = this.gs.ZI();
            var t = r.centerId;
            if (t === this.WL) {
                return false
            }
            if (r["floorInfo"][t]) {
                e = r["floorInfo"][t]
            }
            if (this.UL && this.UL[t] && this.UL[t]["building"]) {
                var i = this.UL[t]["building"];
                var a = i.properties;
                var n = null;
                var o = null;
                if (a && a["floor_nonas"]) {
                    n = a["floor_nonas"];
                    o = a["floor_indexs"]
                }
                if (!n || !o) {
                    return false
                }
                if (o.length > 1) {
                    if (o[0] > o[1]) {
                        n = n.reverse();
                        o = o.reverse()
                    }
                }
                if (this.e$) {
                    this.e$["setOption"]({
                        floors: n,
                        floorIndexs: o,
                        currentFloor: Number(e)
                    });
                    this.WL = t;
                    this.gs.setRenderData({
                        barCenterId: this.WL
                    })
                }
            }
        }
        ;
        e.prototype.qI = function(e) {
            var r = this._map.getCenter().toJSON();
            var t = null;
            var i = "";
            var a = Object.keys(e);
            for (var n = 0; n < a.length; n++) {
                var o = a[n];
                var f = e[o];
                if (f["building"] && f["building"]["properties"] && f["building"]["properties"]["centerPoint"]) {
                    var s = f["building"]["properties"]["centerPoint"];
                    var u = new AMap["LngLat"](s[0],s[1]);
                    var l = AMap["GeometryUtil"]["distance"](r, u);
                    if (t) {
                        if (l < t) {
                            t = l;
                            i = o
                        }
                    } else {
                        t = l;
                        i = o
                    }
                }
            }
            return i
        }
        ;
        e.prototype.vD = function() {
            var e = this._map.getCenter().toJSON();
            var r = null;
            var t = "";
            var i = this.gs.ZI();
            var a = i["indoor"];
            var n = Object.keys(a);
            for (var o = 0; o < n.length; o++) {
                var f = n[o];
                var s = a[f];
                if (s["buildingCenter"]) {
                    var u = s["buildingCenter"];
                    var l = new AMap["LngLat"](u[0],u[1]);
                    var v = AMap["GeometryUtil"]["distance"](e, l);
                    if (r) {
                        if (v < r) {
                            r = v;
                            t = f
                        }
                    } else {
                        r = v;
                        t = f
                    }
                }
            }
            return t
        }
        ;
        e.prototype.fD = function(e, r) {
            var t = this.gs.ZI();
            if (!t["floorInfo"]) {
                t["floorInfo"] = {}
            }
            t["floorInfo"][e] = r;
            this.gs.setRenderData(t)
        }
        ;
        e.prototype.gS = function(e, r) {
            var t = Object.keys(e["data"])[0];
            if (r) {
                t = r
            }
            if (!e["data"][t]) {
                return false
            }
            var i = e["data"][t]["floor"];
            var a = e["data"][t]["id"];
            if (!i || !a) {
                return false
            }
            var n = this.gs.ZI();
            this.fD(a, r);
            var o = n["centerId"];
            if (o === a) {
                this.QI(e["building"], a)
            }
            this.o$(i, a, e["building"], r);
            this.gs.mD(true)
        }
        ;
        e.prototype.ar = function(e) {
            var r = this.gs.ZI();
            var t = r.indoor;
            if (t && t[e]) {
                if (t[e]["polygons"]) {
                    this.gs.$F.remove(t[e]["polygons"])
                }
                if (t[e] && t[e]["markers"] && t[e]["markers"].length > 0) {
                    this.a$.remove(t[e]["markers"])
                }
            }
        }
        ;
        e.prototype.h$ = function(e) {
            var r = this;
            if (!e || !e["properties"]) {
                return null
            }
            var t = e["properties"]["poitype"];
            var i = e["position"];
            var a = t.join("_");
            var n = this.l$[a] && this.l$[a][0];
            var o = this.gs.ZI();
            if (n && n["styleOpts"] && n["styleOpts"]["iconId"]) {
                var f = n["styleOpts"]["iconId"];
                var s = indoorStyleSet.GC(f);
                var u = {
                    position: i,
                    zooms: e["zooms"],
                    opacity: o["opacity"],
                    rank: 3e5,
                    icon: {
                        size: [18, 18],
                        image: s,
                        anchor: "center"
                    },
                    extData: e["properties"],
                    innerOverlay: true,
                    bubble: true
                };
                if (e["text"]) {
                    var l = n["styleOpts"]["fillColor"] || "rgb(132,99,142)";
                    var v = n["styleOpts"]["strokeColor"] || "#FFFFFF";
                    u = assign$1(u, {
                        text: {
                            content: e["text"],
                            direction: "bottom",
                            style: {
                                fillColor: l,
                                strokeColor: v,
                                fontSize: 12
                            }
                        }
                    })
                }
                var c = new AMap["LabelMarker"](u);
                c.on("click", function(e) {
                    if (r._map && r._map.getStatus().isHotspot) {
                        r.bi("click", e)
                    }
                });
                c.on("mousemove", function(e) {
                    if (r._map && r._map.getStatus().isHotspot) {
                        r.bi("mousemove", e)
                    }
                });
                c.on("mouseout", function(e) {
                    if (r._map && r._map.getStatus().isHotspot) {
                        r.bi("mousemove", e)
                    }
                });
                return c
            }
            return null
        }
        ;
        e.prototype.bi = function(e, r) {
            var t = "hotspot";
            var i;
            var a = r.target.getExtData();
            switch (e) {
            case "click":
                i = t + e;
                break;
            case "mouseover":
                i = t + "over";
                break;
            case "mouseout":
                i = t + "out";
                break
            }
            if (i) {
                var n = this._map.getProjection();
                var o = n["unproject"](r["lnglat"]["lng"], r["lnglat"]["lat"])
                  , f = o[0]
                  , s = o[1];
                var u = {
                    type: i,
                    id: a["pid"],
                    name: a["name"],
                    lnglat: r["lnglat"],
                    originEvent: r["originEvent"]
                };
                this._map.emit(i, u)
            }
        }
        ;
        e.prototype.o$ = function(e, r, t, i) {
            var s = this;
            if (!this._map || !r) {
                return false
            }
            var a = this.gs.ZI();
            this.ar(r);
            var O = this._map["getZoom"]();
            var n = [];
            var o = [];
            var f = e["shops"];
            var u = e["floor"];
            var l = e["cons"];
            var B = {
                building_id: r,
                floor: u
            };
            var D = f.length;
            var v = [];
            var c = [];
            var h = [];
            var d = [];
            var _ = [];
            for (var g = 0; g < D; g++) {
                var y = f[g];
                var m = y.properties
                  , p = y.geometry;
                var N = assign$1({
                    shop: {
                        id: m["cpid"],
                        name: m["name"]
                    }
                }, B);
                var b = "#FFFFFF";
                var x = "#FE9A2E";
                if (m && m["regiontype"]) {
                    var M = m["regiontype"].join("_");
                    var C = this.l$[M] && this.l$[M][0];
                    if (C && C["styleOpts"]) {
                        b = C["styleOpts"]["fillColor"];
                        x = C["styleOpts"]["strokeColor"]
                    }
                }
                c.push(b);
                h.push(x);
                _.push(a["opacity"]);
                d.push({
                    indoorPolygon: true,
                    indoorData: N,
                    id: r
                });
                v.push(p.coordinates);
                if (m["zoom_min"] && m["zoom_max"] && m["font_anthor_point"]) {
                    var S = m["font_anthor_point"];
                    var w = {
                        position: [S[0], S[1]],
                        text: m["name"],
                        properties: m,
                        zooms: [m["zoom_min"], m["zoom_max"]]
                    };
                    var k = this.h$(w);
                    if (k) {
                        o.push(k)
                    }
                }
            }
            var T = new AMap["Polygon"]({
                bubble: true,
                path: v,
                innerOverlay: true,
                cursor: a["cursor"],
                zooms: this.YI,
                async: true,
                fillColor: function(e) {
                    return c[e]
                },
                strokeColor: function(e) {
                    return h[e]
                },
                fillOpacity: function(e) {
                    return _[e]
                },
                strokeOpacity: a["opacity"],
                strokeWeight: 1,
                zIndex: a["zIndex"] + 2
            });
            T.on("click", function(e) {
                var r = d[e.vectorIndex];
                var t = r.id;
                if (s.e$) {
                    s.e$.show()
                }
                if (t) {
                    var i = {
                        centerId: t,
                        changeFloor: true
                    };
                    s.gs.setRenderData(i);
                    s.$ee();
                    s.gs.uD()
                }
                r["indoorData"]["ext"] = {
                    type: "polygon",
                    data: e
                };
                s.gs.emit("click", r["indoorData"])
            });
            T.on("mouseover", function(e) {
                for (var r = 0, t = d.length; r < t; r++) {
                    var i = d[r];
                    if (i["originFillOpacity"] && i["originFillColor"]) {
                        c[r] = i["originFillColor"];
                        _[r] = i["originFillOpacity"]
                    }
                }
                var a = d[e.vectorIndex];
                var n = e.lnglat;
                a["originFillOpacity"] = _[e.vectorIndex];
                a["originFillColor"] = c[e.vectorIndex];
                c[e.vectorIndex] = "#B45F04";
                _[e.vectorIndex] = .4;
                if (a["indoorData"] && a["indoorData"]["shop"] && a["indoorData"]["shop"]["name"]) {
                    var o = a["indoorData"]["shop"]["name"];
                    var f = "<div style=" + '"padding: 5px; border: 1px solid rgba(41,38,36,0.87);; white-space:nowrap; border-radius: 2px;color: #000000; background: #FBF5EF; font-size:12px;">' + o + "</div>";
                    s.JT = new AMap["Marker"]({
                        position: n,
                        content: f,
                        anchor: "bottom-left",
                        map: s._map,
                        noSelect: true
                    })
                }
                T.to()
            });
            T.on("mouseout", function(e) {
                for (var r = 0, t = d.length; r < t; r++) {
                    var i = d[r];
                    if (i["originFillOpacity"] && i["originFillColor"]) {
                        c[r] = i["originFillColor"];
                        _[r] = i["originFillOpacity"]
                    }
                }
                if (s.JT) {
                    s._map.remove(s.JT)
                }
                T.to()
            });
            n.push(T);
            var A = null;
            var U = [];
            if (u["geometry"] && u["geometry"]["coordinates"]) {
                var b = outLineStyle["unHigh"]["fillColor"];
                var x = outLineStyle["unHigh"]["strokeColor"];
                if (a["centerId"] === r) {
                    x = outLineStyle["high"]["strokeColor"]
                }
                var I = u["geometry"]["coordinates"];
                if (u["geometry"]["type"] && u["geometry"]["type"] === "MultiPolygon") {
                    if (I[0] && I[0].length > 1) {
                        var H = [];
                        for (var $ = 0; $ < I[0].length; $++) {
                            H.push(I[0][$])
                        }
                        I = H
                    }
                }
                for (var F = 0; F < I.length; F++) {
                    var P = new AMap["Polygon"]({
                        bubble: true,
                        async: true,
                        strokeWeight: 3,
                        innerOverlay: true,
                        zooms: this.YI,
                        path: I[F],
                        fillColor: b,
                        strokeColor: x,
                        zIndex: a["zIndex"] + 1,
                        fillOpacity: a["opacity"],
                        strokeOpacity: a["opacity"],
                        extData: {
                            indoorPolygon: true,
                            id: r
                        }
                    });
                    A = true;
                    P.on("click", function(e) {
                        var r = e.target.getExtData();
                        var t = r.id;
                        if (s.e$) {
                            s.e$.show()
                        }
                        if (t) {
                            var i = {
                                centerId: t,
                                changeFloor: true
                            };
                            s.gs.setRenderData(i)
                        }
                        s.$ee();
                        s.gs.uD()
                    });
                    U.push(P);
                    n.push(P)
                }
            }
            if (a["showId"]) {
                this.gs.setRenderData({
                    centerId: a["showId"]
                })
            }
            this.gs.uD();
            this.gs.$F.add(n);
            if (!a["indoor"][r]) {
                a["indoor"][r] = {}
            }
            a["indoor"][r]["curFloor"] = i;
            a["indoor"][r]["polygons"] = n;
            a["indoor"][r]["outline"] = U;
            if (t && t["properties"] && t["properties"]["centerPoint"]) {
                a["indoor"][r]["buildingCenter"] = t["properties"]["centerPoint"]
            }
            if (a && a["showId"] === r && A) {
                if (a["animateState"] !== "undefined") {
                    this._map.setStatus({
                        animateEnable: a["animateState"]
                    })
                }
            }
            var z = l.length;
            for (var L = 0; L < z; L++) {
                var G = l[L];
                var p = G.geometry
                  , m = G.properties;
                var j = m.zoom_min
                  , V = m.zoom_max;
                var w = {
                    properties: m,
                    position: [p["coordinates"][0], p["coordinates"][1]],
                    zooms: [j, V]
                };
                var k = this.h$(w);
                if (k) {
                    o.push(k)
                }
            }
            if (!this.f$) {
                this._map.add(this.a$);
                this.gs.setRenderData({
                    labelsLayer: this.a$
                });
                this.f$ = true
            }
            a["indoor"][r]["markers"] = o;
            if (typeof a["zIndex"] !== "undefined") {
                this.a$.setzIndex(a["zIndex"] + 9)
            }
            this.a$.add(o);
            var W = this._map && this._map["showLabel"];
            if (a["showLabels"] && W) {
                this.a$.show()
            }
            this.gs.KI(a);
            var R = {
                status: 0,
                id: r,
                building: {
                    id: r
                }
            };
            if (t["properties"]) {
                var E = t["properties"];
                R["building"]["name"] = E["name_cn"];
                R["building"]["lnglat"] = E["centerPoint"];
                R["building"]["floor"] = i || 1;
                R["building"]["floor_details"] = {
                    floor_indexs: E["floor_indexs"],
                    floor_nonas: E["floor_nonas"],
                    floor_names: E["floor_names"]
                };
                this.gs.setRenderData(R)
            }
            if (!a["complete"]) {
                a["complete"] = {}
            }
            if (!a["complete"][r]) {
                a["complete"][r] = {}
            }
            if (!a["complete"][r][i]) {
                this.gs.emit("floor_complete", R);
                a["complete"][r][i] = true
            } else if (this.pD) {
                this.gs.emit("floor_complete", R);
                this.pD = false
            }
            this.gs.setRenderData(a["complete"])
        }
        ;
        return e
    }(LayerRender);
    var map$3 = M["lodash"]["map"];
    var DistrictRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.gC = {
                Nation_Border_China: "nation-stroke",
                Nation_Border_Foreign: "nation-stroke",
                Coastline_China: "coastline-stroke",
                Coastline_Foreign: "coastline-stroke",
                Province_Border_China: "province-stroke",
                Province_Border_Foreign: "province-stroke",
                City_Border_China: "city-stroke",
                County_Border_China: "county-stroke"
            };
            e.xC = {};
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            if (!r.s_) {
                return
            }
            var n = t.viewState.optimalZoom;
            var o = t.map.getMask();
            var f = r.tiles;
            var s = i.layer.getLayerOptions()
              , u = s.styles
              , l = s.adcode;
            if (i.ce) {
                e.context.clear({
                    depth: true
                })
            }
            for (var v = 0, c = f; v < c.length; v++) {
                var h = c[v];
                if (h.status !== TileState$1.LOADED) {
                    continue
                }
                this.Lc(e, h, t, i, u, l);
                this.Ic(e, h, t, i, u, l);
                this.lH(e, h, t, i)
            }
            this.PB(e, t, i, o)
        }
        ;
        e.prototype.destroy = function() {}
        ;
        e.prototype.clearStyleCache = function() {
            this.xC = {}
        }
        ;
        e.prototype.Lc = function(e, r, t, i, a, n) {
            var o = e.Wn();
            var f = r.data;
            var s = f.Dc;
            if (!s) {
                return
            }
            s.upload(o.context, true);
            var u = s.zs;
            var l = s.Pc;
            var v = f.Oc;
            var c = i.opacity;
            var h = [r.Sa[0], r.Sa[1]];
            var d = 1;
            if (t.viewState.viewMode === "3D") {
                d = t.map.getView().EF()
            }
            var _ = r.zo.z;
            if (_ < LocalZoom) {
                var g = r.localCoord.center;
                h[0] += g[0];
                h[1] += g[1]
            }
            for (var y = 0, m = v.jc; y < m.length; y++) {
                var p = m[y];
                var b = JSON.parse("" + p["mainkey"]);
                var x = this.Mn(a, b, "polygon");
                var M = x["fill"];
                if (!M || M.length !== 4 || n && !n[b.adcode] && !n[b.SOC] && !n[b.adcode_pro] && !n[b.adcode_cit]) {
                    continue
                }
                var C = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_color: [M[0], M[1], M[2], M[3] * c],
                    u_offset: [0, 0],
                    u_localDeltaCenter: h,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: d
                };
                if (C) {
                    o.context.getExtension("OES_element_index_uint");
                    o.Ce(C, {
                        a_pos: {
                            type: "vec2",
                            Re: u.size,
                            offset: 0,
                            buffer: u
                        }
                    }, p.length, l, "TRIANGLES", i.depthTest, ColorMode.Si, this.OH(i.rejectMapMask, !!t.map.getMask()), undefined, p.offset)
                }
            }
        }
        ;
        e.prototype.Ic = function(e, r, t, i, s, a) {
            var u = this;
            var n = e.Vn();
            if (!r.data) {
                console.error("tile have no data");
                return
            }
            var o = r.data;
            var f = o.$c;
            if (!f) {
                return
            }
            f.upload(n.context, true);
            var l = f.zs.buffer;
            var v = f.Pc.buffer;
            var c = o.Uc;
            var h = c.jc;
            var d = t.viewState.optimalZoom;
            var _ = [r.Sa[0], r.Sa[1]];
            var g = i.opacity;
            var y = 1;
            if (t.viewState.viewMode === "3D") {
                y = t.map.getView().EF()
            }
            var m = r.zo.z;
            if (m < LocalZoom) {
                var p = r.localCoord.center;
                _[0] += p[0];
                _[1] += p[1]
            }
            var b = {};
            var x = {};
            if (h.length === 1) {
                var M = h[0]["mainkey"];
                var C = JSON.parse(M, undefined);
                var S = this.Mn(s, C, "line");
                b[M] = S;
                x[M] = C
            }
            h.sort(function(e, r) {
                var t = e["mainkey"];
                var i = JSON.parse(t, undefined);
                var a = u.Mn(s, i, "line");
                if (!b[t]) {
                    b[t] = a
                }
                if (!x[t]) {
                    x[t] = i
                }
                var n = r["mainkey"];
                var o = JSON.parse(n, undefined);
                var f = u.Mn(s, o, "line");
                if (!b[n]) {
                    b[n] = f
                }
                if (!x[n]) {
                    x[n] = o
                }
                return a.zIndex - f.zIndex
            });
            for (var w = 0, k = h.length; w < k; w++) {
                var T = h[w];
                var M = T["mainkey"];
                var C = x[M];
                var A = +C.type_;
                var S = b[M];
                var I = [1, 0, 1, 0];
                if (S["dash"]) {
                    I = __spreadArrays(S["dash"])
                }
                if (A === 1 || A === 8) {
                    I = [3, 2, 3, 2]
                } else if (A === 10) {
                    I = [1, 2, 1, 2]
                } else if (A === 11) {
                    I = [5, 5, 5, 5]
                } else if (A === 4) {
                    I = [2, 2, 6, 2]
                }
                var $ = S[this.gC[C.type]];
                if (!$ || $.length !== 4 || a && !a[C.adcode] && !a[C.SOC] && !a[C.adcode_pro] && !a[C.adcode_cit]) {
                    continue
                }
                var F = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_width: S["stroke-width"],
                    u_color: [$[0], $[1], $[2], $[3] * g],
                    u_dash: I,
                    u_border: 0,
                    u_localDeltaCenter: _,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: y
                };
                if (!F) {
                    continue
                }
                if (F) {
                    var P = l.size;
                    n.context.getExtension("OES_element_index_uint");
                    n.Ce(F, {
                        a_pos: {
                            type: "vec2",
                            buffer: l,
                            Re: P,
                            offset: 4 * 0
                        },
                        a_normal: {
                            type: "vec2",
                            buffer: l,
                            Re: P,
                            offset: 4 * 2
                        },
                        a_distance: {
                            type: "float",
                            buffer: l,
                            Re: P,
                            offset: 4 * 4
                        },
                        a_dir: {
                            type: "vec2",
                            buffer: l,
                            Re: P,
                            offset: 4 * 5
                        }
                    }, T.length, v, "TRIANGLES", i.depthTest, ColorMode.Si, this.OH(i.rejectMapMask, !!t.map.getMask()), undefined, T.offset)
                }
            }
        }
        ;
        e.prototype.lH = function(e, r, t, i) {
            var a = e.Wn();
            var n = a.context;
            var o = new Float32Array(this.fH(r));
            var f = [r.Sa[0], r.Sa[1]];
            var s = n.fe(o, 8);
            var u = r.zo.z;
            if (u < LocalZoom) {
                var l = r.localCoord.center;
                f[0] += l[0];
                f[1] += l[1]
            }
            var v = [0, 1, 0, 0];
            if (u === t.viewState.optimalZoom - 1) {
                v = [1, 0, 0, 0]
            }
            var c = {
                u_matrix: t.viewState.mvpMatrix,
                u_color: v,
                u_offset: [0, 0],
                u_localDeltaCenter: f,
                u_viewHeight: t.viewState.size[1],
                u_skyHeight: 1
            };
            a.Ce(c, {
                a_pos: {
                    type: "vec2",
                    buffer: s,
                    Re: 0,
                    offset: 0
                }
            }, 6, undefined, "TRIANGLES", false, ColorMode.Si, this.BH(i.rejectMapMask, !!t.map.getMask()), undefined, 0)
        }
        ;
        e.prototype.Mn = function(e, r, t) {
            var i = JSON.stringify(r);
            if (this.xC[i]) {
                return this.xC[i]
            }
            var a = ["nation-stroke", "coastline-stroke", "province-stroke", "city-stroke", "county-stroke"];
            var n = {};
            if (!r) {
                return n
            }
            if (t === "line") {
                for (var o = 0, f = a.length; o < f; o++) {
                    var s = a[o];
                    if (e[s]) {
                        if (AMap.Util["isFunction"](e[s])) {
                            n[s] = AMap.Util.color2RgbaArray(e[s](r))
                        } else {
                            n[s] = AMap.Util.color2RgbaArray(e[s])
                        }
                    }
                }
                if (AMap.Util["isFunction"](e["stroke-width"])) {
                    n["stroke-width"] = e["stroke-width"](r)
                } else {
                    n["stroke-width"] = e["stroke-width"]
                }
                if (r && (r["adcode_pro"] || r["adcode_city"])) {
                    n["dash"] = AMap.Util["isFunction"](e["dash"]) ? e["dash"](r) : e["dash"]
                }
                if (r && e["zIndex"] !== undefined) {
                    n["zIndex"] = AMap.Util["isFunction"](e["zIndex"]) ? e["zIndex"](r) : e["zIndex"]
                }
            } else if (t === "polygon") {
                if (AMap.Util["isFunction"](e["fill"])) {
                    n["fill"] = AMap.Util.color2RgbaArray(e["fill"](r))
                } else {
                    n["fill"] = AMap.Util.color2RgbaArray(e["fill"])
                }
                n["stroke-width"] = e["stroke-width"]
            }
            this.xC[i] = n;
            return n
        }
        ;
        e.prototype.PB = function(e, r, t, i) {
            e.context.clear({
                stencil: true
            });
            if (i) {
                var a = r.map.getLayerByClass("AMap.MaskLayer");
                if (a) {
                    var n = r.uo.getData(a.co(), r.viewState, e.context);
                    var o = a.getRender();
                    if (n && o) {
                        o.renderFrame(e, n, r, t)
                    }
                }
            }
        }
        ;
        return e
    }(LayerRender);
    var Util$7 = AMap["Util"];
    var MapboxRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.yz = 1;
            e.VG = 1;
            e.VG = 1;
            e.ZG = StencilMode.HE;
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            if (!r.s_) {
                return
            }
            if (!r || r.tiles.length < 1) {
                return
            }
            var n = t.viewState.optimalZoom;
            if (n <= 9.8) {
                this.ZG = undefined
            } else {
                this.ZG = StencilMode.HE
            }
            for (var o = 0; o < r.tiles.length; o++) {
                var f = r.tiles[o];
                if (!f) {
                    continue
                }
                this.GG(e, f, t, i);
                this.Lc(e, f, t, i);
                this.sT(e, f, t, i);
                this.Ic(e, f, t, i)
            }
            if (r.tiles && r.tiles.length > 0) {
                a.map.bZ.dynamic.set("firstPaint", true)
            }
            var s = t.map.getMask();
            this.PB(e, t, i, s)
        }
        ;
        e.prototype.GG = function(e, r, t, i) {
            var a = e.Wn();
            var n = r.stencil;
            if (!n) {
                return
            }
            var o = n.Dc;
            if (!o) {
                return
            }
            o.upload(a.context);
            var f = o.zs;
            var s = o.Pc;
            var u = n.Oc;
            var l = t.viewState.optimalZoom;
            var v = i.opacity;
            var c = [r.Sa[0], r.Sa[1]];
            var h = 1;
            if (t.viewState.viewMode === "3D") {
                h = t.map.getView().EF()
            }
            if (l < LocalZoom) {
                c = [0, 0]
            }
            for (var d = 0, _ = u.jc; d < _.length; d++) {
                var g = _[d];
                var y = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_color: [0, 0, 0, 0],
                    u_offset: [0, 0],
                    u_localDeltaCenter: c,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: h
                };
                if (y && o.Qx) {
                    a.context.getExtension("OES_element_index_uint");
                    a.Ce(y, {
                        a_pos: {
                            type: "vec2",
                            Re: f.size,
                            offset: 0,
                            buffer: f
                        }
                    }, g.length, s, "TRIANGLES", undefined, ColorMode.Si, StencilMode.TE, undefined, g.offset)
                }
            }
        }
        ;
        e.prototype.UG = function(e, r, t, i) {
            var a = e.Wn();
            var n = r.stencil;
            if (!n) {
                return
            }
            var o = n.Dc;
            if (!o) {
                return
            }
            o.upload(a.context);
            var f = o.zs;
            var s = o.Pc;
            var u = n.Oc;
            var l = t.viewState.optimalZoom;
            var v = i.opacity;
            var c = [r.Sa[0], r.Sa[1]];
            var h = 1;
            if (t.viewState.viewMode === "3D") {
                h = t.map.getView().EF()
            }
            if (l < LocalZoom) {
                c = [0, 0]
            }
            for (var d = 0, _ = u.jc; d < _.length; d++) {
                var g = _[d];
                var y = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_color: [0, 0, 0, 0],
                    u_offset: [0, 0],
                    u_localDeltaCenter: c,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: h
                };
                if (y && o.Qx) {
                    a.context.getExtension("OES_element_index_uint");
                    a.Ce(y, {
                        a_pos: {
                            type: "vec2",
                            Re: f.size,
                            offset: 0,
                            buffer: f
                        }
                    }, g.length, s, "TRIANGLES", undefined, ColorMode.Si, StencilMode.FE, undefined, g.offset)
                }
            }
        }
        ;
        e.prototype.Lc = function(e, r, t, i) {
            var a = e.Wn();
            var n = r.data;
            if (!n) {
                return
            }
            var o = n.Dc;
            if (!o) {
                return
            }
            o.upload(a.context);
            var f = o.zs;
            var s = o.Pc;
            var u = n.Oc;
            var l = t.viewState.optimalZoom;
            var v = i.opacity;
            var c = [r.Sa[0], r.Sa[1]];
            var h = 1;
            if (t.viewState.viewMode === "3D") {
                h = t.map.getView().EF()
            }
            if (l < LocalZoom) {
                if (r.fY)
                    ;
                else {
                    c = [0, 0]
                }
            }
            for (var d = 0, _ = u.jc; d < _.length; d++) {
                var g = _[d];
                var y = {
                    u_matrix: t.viewState.mvpMatrix,
                    u_color: n.bgColor,
                    u_offset: [0, 0],
                    u_localDeltaCenter: c,
                    u_viewHeight: t.viewState.size[1],
                    u_skyHeight: h
                };
                if (g.style) {
                    var m = g.style;
                    if (m.fillColor) {
                        var p = m.fillColor;
                        y["u_color"] = p
                    }
                }
                if (y && o.Qx) {
                    a.context.getExtension("OES_element_index_uint");
                    a.Ce(y, {
                        a_pos: {
                            type: "vec2",
                            Re: f.size,
                            offset: 0,
                            buffer: f
                        }
                    }, g.length, s, "TRIANGLES", undefined, ColorMode.Si, this.ZG, undefined, g.offset)
                }
            }
        }
        ;
        e.prototype.Ic = function(e, r, t, i) {
            var a = r.data;
            if (!a || !a.$c) {
                return
            }
            var n = a.Uc;
            var o = t.viewState.optimalZoom;
            var f = [r.Sa[0], r.Sa[1]];
            if (o < LocalZoom) {
                f = [0, 0]
            }
            var s = a.$c;
            s.upload(e.context);
            if (!s.uploaded || !s.zs) {
                return
            }
            n.jc.sort(function(e, r) {
                if (e.style && e.style[0] && e.style[0].jr && r.style && r.style[0] && r.style[0].jr) {
                    if (r.style[0].jr < e.style[0].jr) {
                        return 1
                    } else {
                        return -1
                    }
                }
                return 0
            });
            for (var u = 0, l = n.jc; u < l.length; u++) {
                var v = l[u];
                var c = s.zs.buffer;
                if (!v.style || v.style.length < 1) {
                    continue
                }
                var h = {
                    u_color: [0, 0, 0, .5],
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_width: 1,
                    u_offset: [0, 0],
                    u_localDeltaCenter: f,
                    u_dash: "vec3",
                    u_dashType: "int",
                    u_skyHeight: 1,
                    u_viewHeight: 1
                };
                var d = v.style[0];
                if (d && d["faceColor"] && d["faceWidth"] && d["type"] && d["type"] === "line") {
                    var _ = v.style[0]["faceColor"];
                    h["u_color"] = _;
                    h["u_width"] = d["faceWidth"]
                } else {
                    continue
                }
                if (!h) {
                    return
                }
                var g = e.Xn();
                g.context.getExtension("OES_element_index_uint");
                g.Ce(h, {
                    a_pos: {
                        buffer: c,
                        type: "vec2",
                        Re: c.size,
                        offset: 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 5
                    }
                }, v.length, s.Pc.buffer, undefined, undefined, ColorMode.Si, this.ZG, undefined, v.offset)
            }
        }
        ;
        e.prototype.sT = function(e, r, t, i) {
            var a = r.data;
            if (!a || !a.$c) {
                return
            }
            var n = a.Uc;
            var o = t.viewState.optimalZoom;
            var f = [r.Sa[0], r.Sa[1]];
            if (o < LocalZoom) {
                f = [0, 0]
            }
            var s = a.$c;
            s.upload(e.context);
            if (!s.uploaded || !s.zs) {
                return
            }
            n.jc.sort(function(e, r) {
                if (e.style && e.style[0] && e.style[0].Or && r.style && r.style[0] && r.style[0].Or) {
                    if (r.style[0].Or < e.style[0].Or) {
                        return 1
                    } else {
                        return -1
                    }
                }
                return 0
            });
            for (var u = 0, l = n.jc; u < l.length; u++) {
                var v = l[u];
                var c = s.zs.buffer;
                if (!v.style || v.style.length < 1) {
                    continue
                }
                var h = {
                    u_color: [0, 0, 0, .5],
                    u_matrix: t.viewState.mvpMatrix,
                    u_meter_per_pixel: t.viewState.resolution,
                    u_width: 1,
                    u_offset: [0, 0],
                    u_localDeltaCenter: f,
                    u_dash: "vec3",
                    u_dashType: "int",
                    u_skyHeight: 1,
                    u_viewHeight: 1
                };
                var d = v.style[0];
                if (d && d["borderColor"] && d["borderWidth"] && d["type"] && d["type"] === "line") {
                    var _ = v.style[0]["borderColor"];
                    h["u_color"] = _;
                    h["u_width"] = d["borderWidth"]
                } else {
                    continue
                }
                if (!h) {
                    return
                }
                var g = e.Xn();
                g.context.getExtension("OES_element_index_uint");
                g.Ce(h, {
                    a_pos: {
                        buffer: c,
                        type: "vec2",
                        Re: c.size,
                        offset: 0
                    },
                    a_normal: {
                        type: "vec2",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 2
                    },
                    a_distance: {
                        type: "float",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 4
                    },
                    a_dir: {
                        type: "vec2",
                        buffer: c,
                        Re: c.size,
                        offset: 4 * 5
                    }
                }, v.length, s.Pc.buffer, undefined, undefined, ColorMode.Si, this.ZG, undefined, v.offset)
            }
        }
        ;
        e.prototype.PB = function(e, r, t, i) {
            e.context.clear({
                stencil: true
            });
            if (i) {
                var a = r.map.getLayerByClass("AMap.MaskLayer");
                if (a) {
                    var n = r.uo.getData(a.co(), r.viewState, e.context);
                    var o = a.getRender();
                    if (n && o) {
                        o.renderFrame(e, n, r, t)
                    }
                }
            }
        }
        ;
        e.prototype.fb = function(e, r) {
            var t = {
                strokeWeight: 2,
                strokeOpacity: 1,
                strokeColor: "rgb(0,0,0)"
            };
            var i = t.strokeWeight;
            var a = Util$7.color2RgbaArray(t.strokeColor);
            if (!a) {
                return
            }
            a = a.slice(0, 3);
            a.push(t.strokeOpacity);
            return {
                u_color: a,
                u_matrix: r.viewState.mvpMatrix,
                u_meter_per_pixel: r.viewState.resolution,
                u_width: i,
                u_offset: e.zx
            }
        }
        ;
        return e
    }(LayerRender);
    var Attribute = function() {
        function e(e, r, t, i, a, n) {
            if (a === void 0) {
                a = false
            }
            if (n === void 0) {
                n = 0
            }
            this.context = e;
            this.type = r;
            this.Me = t;
            this.location = i;
            this.normalize = a;
            this.Fee = n
        }
        e.prototype.set = function(e) {
            var r = this.context.gl;
            if (!e) {
                this.current = undefined;
                r.disableVertexAttribArray(this.location);
                return
            }
            if (e.type !== this.type) {
                console.error("error webglBuffer type,need " + this.type + ", receive " + e.type + " ");
                return
            }
            if (!this.ke(e)) {
                return
            }
            this.current = e;
            var t;
            r.enableVertexAttribArray(this.location);
            this.context.de.set(e.buffer);
            switch (this.type) {
            case "float":
            case "int":
                t = 1;
                break;
            case "vec2":
                t = 2;
                break;
            case "vec3":
                t = 3;
                break;
            case "vec4":
                t = 4;
                break;
            default:
                t = 1
            }
            var i;
            switch (this.Me) {
            case "int8":
                i = r.BYTE;
                break;
            case "uint8":
                i = r.UNSIGNED_BYTE;
                break;
            case "int16":
                i = r.SHORT;
                break;
            case "uint16":
                i = r.UNSIGNED_SHORT;
                break;
            case "float32":
                i = r.FLOAT;
                break
            }
            assert(i, "invalid datatype " + this.Me);
            r.vertexAttribPointer(this.location, t, i, this.normalize, e.Re, e.offset);
            var a = this.context.getExtension("ANGLE_instanced_arrays");
            if (a) {
                a.vertexAttribDivisorANGLE(this.location, 0)
            }
            if (this.Fee > 0) {
                if (a) {
                    a.vertexAttribDivisorANGLE(this.location, this.Fee)
                }
            }
        }
        ;
        e.prototype.ke = function(e) {
            return true
        }
        ;
        return e
    }();
    var Uniform = function() {
        function e(e, r, t) {
            this.context = e;
            this.type = r;
            this.location = t;
            this.Re = 0;
            this.offset = 0;
            this.te = same;
            this.ze = e.gl
        }
        e.prototype.set = function(e) {
            if (this.te(this.current, e)) {
                return
            }
            if (e !== undefined) {
                switch (this.type) {
                case "bool":
                case "int":
                case "sampler2D":
                    this.ze.uniform1i(this.location, e);
                    break;
                case "sampler2D[]":
                    this.ze.uniform1iv(this.location, e);
                    break;
                case "float":
                    this.ze.uniform1f(this.location, e);
                    break;
                case "float[]":
                    this.ze.uniform1fv(this.location, e);
                    break;
                case "vec2":
                    this.ze.uniform2f(this.location, e[0], e[1]);
                    break;
                case "vec3":
                    this.ze.uniform3f(this.location, e[0], e[1], e[2]);
                    break;
                case "vec4":
                    this.ze.uniform4f(this.location, e[0], e[1], e[2], e[3]);
                    break;
                case "mat2":
                    this.ze.uniformMatrix2fv(this.location, false, new Float32Array(e));
                    break;
                case "mat3":
                    this.ze.uniformMatrix3fv(this.location, false, new Float32Array(e));
                    break;
                case "mat4":
                    this.ze.uniformMatrix4fv(this.location, false, new Float32Array(e));
                    break
                }
                this.current = e
            }
        }
        ;
        return e
    }();
    var Program = function() {
        function d(e, r, t, i, a, n) {
            this.name = n;
            this.Ie = [];
            this.context = e;
            this.attributes = {};
            this.Te = {};
            var o = e.gl;
            var f = d.createProgram(o, r, t);
            this.$i = f;
            if (!this.$i) {
                console.log("createProgram fail", this.name);
                this.je = false;
                return this
            }
            this.je = true;
            for (var s in i) {
                if (i.hasOwnProperty(s)) {
                    var u = i[s].Oe;
                    var l = i[s].Me;
                    var v = o.getAttribLocation(this.$i, s);
                    var c = i[s].normalize;
                    var h = i[s].Fee;
                    this.attributes[s] = new Attribute(e,u,l,v,c,h)
                }
            }
            for (var s in a) {
                if (a.hasOwnProperty(s)) {
                    var u = a[s];
                    var v = o.getUniformLocation(this.$i, s);
                    if (v) {
                        this.Te[s] = new Uniform(e,u,v)
                    }
                    if (u === "sampler2D") {
                        this.Ie.push(s)
                    }
                }
            }
        }
        d.createProgram = function(e, r, t) {
            var i = d.Se(e, e.VERTEX_SHADER, r);
            var a = d.Se(e, e.FRAGMENT_SHADER, t);
            if (!i || !a) {
                return null
            }
            var n = e.createProgram();
            if (!n) {
                return null
            }
            e.attachShader(n, i);
            e.attachShader(n, a);
            e.bindAttribLocation(n, 0, "a_Position");
            e.linkProgram(n);
            var o = e.getProgramParameter(n, e.LINK_STATUS);
            e.deleteShader(a);
            e.deleteShader(i);
            if (!o) {
                var f = e.getProgramInfoLog(n);
                console.log("Failed to link program: " + f);
                e.deleteProgram(n);
                return null
            }
            return n
        }
        ;
        d.Se = function(e, r, t) {
            var i = e.createShader(r);
            if (i === null) {
                return null
            }
            e.shaderSource(i, t);
            e.compileShader(i);
            var a = e.getShaderParameter(i, e.COMPILE_STATUS);
            if (!a) {
                var n = e.getShaderInfoLog(i);
                console.log("Failed to compile shader: " + n);
                e.deleteShader(i);
                return null
            }
            return i
        }
        ;
        d.prototype.Ce = function(e, r, t, i, a, n, o, f, s, u, l) {
            if (a === void 0) {
                a = "TRIANGLES"
            }
            if (n === void 0) {
                n = false
            }
            if (o === void 0) {
                o = ColorMode.Mi
            }
            if (f === void 0) {
                f = StencilMode.disable
            }
            if (s === void 0) {
                s = CullFaceMode.we
            }
            if (u === void 0) {
                u = 0
            }
            if (!this.je) {
                return
            }
            var v = this.context;
            var c = v.gl;
            v.ge.set(this);
            v.ne(n);
            v.ae(f);
            v.Qi(o);
            v.re(s);
            v.hB.set(false);
            this.Le(e);
            this.Ae(r);
            if (i) {
                v.me.set(i);
                if (l) {
                    var h = v.getExtension("ANGLE_instanced_arrays");
                    if (h) {
                        if (i.size === 32) {
                            h.drawElementsInstancedANGLE(c[a], t, c.UNSIGNED_INT, i.size / 8 * u, l.divisor)
                        } else {
                            h.drawElementsInstancedANGLE(c[a], t, c.UNSIGNED_SHORT, i.size / 8 * u, l.divisor)
                        }
                    }
                } else {
                    if (i.size === 32) {
                        c.drawElements(c[a], t, c.UNSIGNED_INT, i.size / 8 * u)
                    } else {
                        c.drawElements(c[a], t, c.UNSIGNED_SHORT, i.size / 8 * u)
                    }
                }
            } else {
                if (l) {
                    var h = v.getExtension("ANGLE_instanced_arrays");
                    if (h) {
                        h.drawArraysInstancedANGLE(c[a], u, t, l.divisor)
                    }
                } else {
                    c.drawArrays(c[a], u, t)
                }
            }
        }
        ;
        d.prototype.Le = function(e) {
            if (!this.je) {
                return
            }
            for (var r in this.Te) {
                var t = this.Te[r];
                if (t.type === "sampler2D") {
                    var i = this.Ie.indexOf(r);
                    if (i >= 0) {
                        var a = e[r];
                        if (a) {
                            this.context.bindTexture.setDirty();
                            this.context.activeTexture.set(i);
                            this.context.bindTexture.set(a.texture)
                        } else {
                            this.context.activeTexture.set(i);
                            this.context.bindTexture.setDirty();
                            this.context.bindTexture.set(null)
                        }
                        t.set(i)
                    }
                } else if (t.type === "sampler2D[]") {
                    var n = e[r];
                    var o = n.De;
                    var f = n.offset || 0;
                    var s = n.count || o.length;
                    var u = [];
                    for (var l = 0; l < s; l++) {
                        var a = o[l];
                        var i = f + l;
                        if (a) {
                            this.context.activeTexture.set(i);
                            this.context.bindTexture.set(a.texture);
                            u.push(i)
                        } else {
                            this.context.activeTexture.set(f);
                            var v = o[0];
                            this.context.bindTexture.set(v.texture);
                            u.push(f)
                        }
                    }
                    t.set(u)
                } else {
                    t.set(e[r])
                }
            }
        }
        ;
        d.prototype.Ae = function(e) {
            if (!this.je) {
                return
            }
            for (var r in this.attributes) {
                if (this.attributes.hasOwnProperty(r)) {
                    this.attributes[r].set(e[r])
                }
            }
        }
        ;
        d.prototype.Pe = function(e, r, t, i, a, n, o, f) {
            if (r === void 0) {
                r = 0
            }
            if (i === void 0) {
                i = "TRIANGLES"
            }
            if (a === void 0) {
                a = false
            }
            if (n === void 0) {
                n = ColorMode.Mi
            }
            if (o === void 0) {
                o = StencilMode.disable
            }
            if (f === void 0) {
                f = CullFaceMode.we
            }
            if (!this.je) {
                return
            }
            var s = this.context;
            var u = s.gl;
            s.ge.set(this);
            s.ne(a);
            s.ae(o);
            s.Qi(n);
            s.re(f);
            if (t) {
                s.me.set(t);
                u.drawElements(u[i], e, u.UNSIGNED_SHORT, t.size / 8 * r)
            } else {
                u.drawArrays(u[i], r, e)
            }
        }
        ;
        d.prototype.Ni = function() {
            if (!this.je) {
                return
            }
            for (var e in this.attributes) {
                if (this.attributes.hasOwnProperty(e)) {
                    this.attributes[e].set(null)
                }
            }
        }
        ;
        d.prototype.destroy = function() {
            if (!this.je) {
                return
            }
            this.context.gl.deleteProgram(this.$i);
            this.$i = null
        }
        ;
        return d
    }();
    M["Program"] = Program;
    var rasterVertextString = "precision highp float;\n\nuniform mat4 u_mvpMatrix;\nuniform vec2 u_localDeltaCenter;\n\nvarying vec2 v_TextureCoord;\nvarying vec4 v_pos;\n\nattribute vec4 a_Position;\n\nvoid main(){\n    v_TextureCoord=a_Position.zw;\n    vec4 pos=vec4(a_Position.x+u_localDeltaCenter.x,a_Position.y+u_localDeltaCenter.y,0.,1.);\n    gl_Position=u_mvpMatrix*pos;\n    v_pos = gl_Position;\n}\n";
    var rasterFragmentString = "precision highp float;\n\nuniform sampler2D u_texture;\nuniform float u_opacity;\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\nuniform vec3 u_skyColor;\nuniform bool u_flterFlag;\nuniform sampler2D u_colorscale; \n\nvarying vec2 v_TextureCoord;\nvarying vec4 v_pos;\n\nvoid main(void) {\n    // gl_FragColor = vec4(1., 0, 0, 1.);\n    // return;\n    vec4 smpColor = texture2D(u_texture, v_TextureCoord);\n    if(smpColor.a < 0.01){\n        discard;\n    }\n    if(u_flterFlag){\n       float grey = (smpColor.r*0.299) + (smpColor.g*0.587) + (smpColor.b*0.114);\n       smpColor = texture2D(u_colorscale,vec2(grey,0.5));\n    }\n    gl_FragColor = smpColor;\n    gl_FragColor.a *= u_opacity;\n\n    float y = v_pos.y / v_pos.w;\n    float fogHeight = 2. / u_viewHeight * 30.;  // 10 像素高度作为模糊处理\n    vec3 fogColor = vec3(0.9, 0.9, 0.9);\n    if(u_skyHeight < 1.0 && y > 0.0) {\n        // 片元颜色 = 物体颜色 * 雾化因子 + 雾的颜色 * （1 - 雾化因子）\n        // float fogFactor = smoothstep(u_skyHeight + fogHeight, u_skyHeight, y);\n        float fogFactor = smoothstep(u_skyHeight + fogHeight/2.0, u_skyHeight - fogHeight/2.0, y);\n        gl_FragColor.rgb = gl_FragColor.rgb * fogFactor + fogColor * (1.0 - fogFactor);\n        gl_FragColor.a *= fogFactor;\n    }\n    // gl_FragColor = vec4(1.0,0,0,1.0);\n}\n";
    var rasterUniforms = {
        u_skyColor: "vec3",
        u_viewHeight: "float",
        u_skyHeight: "float",
        u_texture: "sampler2D",
        u_opacity: "float",
        u_mvpMatrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_flterFlag: "bool",
        u_colorscale: "sampler2D"
    };
    var rasterAttributes = {
        a_Position: {
            Oe: "vec4",
            Me: "float32"
        }
    };
    var raster = {
        uniforms: rasterUniforms,
        attributes: rasterAttributes,
        vertexSource: rasterVertextString,
        fragmentSource: rasterFragmentString
    };
    var labelsLayerVertexString = "// precision mediump float;\nprecision highp float;\nattribute highp vec2 a_vertex;\nattribute vec2 a_texcoord;\nattribute highp vec2 a_origin;\nattribute lowp float a_type;\nattribute lowp float a_texIndex;\nattribute vec4 a_color;\nattribute lowp vec2 a_zooms;\nattribute lowp float a_visible;\nattribute highp float a_height;\nattribute lowp float a_angle;\nattribute lowp float a_highflag;\n\nuniform lowp vec2 u_gl_size;\nuniform highp mat4 u_matrix;\nuniform float u_texsize[_TotalTextureLenTwice];\nuniform lowp int u_event;\nuniform float u_zoom;\nuniform highp vec2 u_delta_center;\nuniform highp vec2 u_offset;\n// u_transform.x => rotate; u_transform.y => pinch; u_transform.z => scale\nuniform lowp vec4 u_transform;\nuniform lowp float u_skyHeight;\nuniform lowp float u_fontSizeFactor;\n\nvarying vec2 v_texcoord;\nvarying float v_texIndex;\nvarying float v_type;\nvarying vec4 v_color;\nvarying vec2 v_pos;\nvarying float v_highflag;\n\nmat4 rotateMat4(mat4 a, float rad, vec3 axis) {\n    float x = axis[0], y = axis[1], z = axis[2];\n    float len = length(axis);\n    float s, c, t;\n    float a00, a01, a02, a03;\n    float a10, a11, a12, a13;\n    float a20, a21, a22, a23;\n    float b00, b01, b02;\n    float b10, b11, b12;\n    float b20, b21, b22;\n\n    len = 1.0 / len;\n    x *= len;\n    y *= len;\n    z *= len;\n\n    s = sin(rad);\n    c = cos(rad);\n    t = 1.0 - c;\n\n    a00 = a[0][0];\n    a01 = a[0][1];\n    a02 = a[0][2];\n    a03 = a[0][3];\n    a10 = a[1][0];\n    a11 = a[1][1];\n    a12 = a[1][2];\n    a13 = a[1][3];\n    a20 = a[2][0];\n    a21 = a[2][1];\n    a22 = a[2][2];\n    a23 = a[2][3];\n\n    // Construct the elements of the rotation matrix\n    b00 = x * x * t + c;\n    b01 = y * x * t + z * s;\n    b02 = z * x * t - y * s;\n    b10 = x * y * t - z * s;\n    b11 = y * y * t + c;\n    b12 = z * y * t + x * s;\n    b20 = x * z * t + y * s;\n    b21 = y * z * t - x * s;\n    b22 = z * z * t + c;\n\n    // Perform rotation-specific matrix multiplication\n    mat4 outMatrix = mat4(a00 * b00 + a10 * b01 + a20 * b02, a00 * b10 + a10 * b11 + a20 * b12, a00 * b20 + a10 * b21 + a20 * b22, a[3][0], a01 * b00 + a11 * b01 + a21 * b02, a01 * b10 + a11 * b11 + a21 * b12, a01 * b20 + a11 * b21 + a21 * b22, a[3][1], a02 * b00 + a12 * b01 + a22 * b02, a02 * b10 + a12 * b11 + a22 * b12, a02 * b20 + a12 * b21 + a22 * b22, a[3][2], a03 * b00 + a13 * b01 + a23 * b02, a03 * b10 + a13 * b11 + a23 * b12, a03 * b20 + a13 * b21 + a23 * b22, a[3][3]);\n\n    //    outMatrix = mat4(1.0);\n    return outMatrix;\n}\n\nmat4 scaleMat4(mat4 a, vec3 v) {\n    float x = v.x, y = v.y, z = v.z;\n\n    mat4 outMatrix = mat4(a[0][0] * x, a[1][0] * y, a[2][0] * z, a[3][0], a[0][1] * x, a[1][1] * y, a[2][1] * z, a[3][1], a[0][2] * x, a[1][2] * y, a[2][2] * z, a[3][2], a[0][3] * x, a[1][3] * y, a[2][3] * z, a[3][3]);\n\n    return outMatrix;\n}\n\nvec2 getSizeFromArray(float u_texsize[_TotalTextureLenTwice], int ndx) {\n    vec2 texsize;\n\n    if(ndx == 0) {\n        texsize = vec2(u_texsize[0], u_texsize[1]);\n    } else if(ndx == 1) {\n        texsize = vec2(u_texsize[2], u_texsize[3]);\n    } \n    // else if (ndx==2){\n    //     texsize=vec2(u_texsize[4], u_texsize[5]);\n    // } else if (ndx==3){\n    //     texsize=vec2(u_texsize[6], u_texsize[7]);\n    // } else if (ndx==4){\n    //     texsize=vec2(u_texsize[8], u_texsize[9]);\n    // } else if (ndx==5){\n    //     texsize=vec2(u_texsize[10], u_texsize[11]);\n    // }else if (ndx==6){\n    //     texsize=vec2(u_texsize[12], u_texsize[13]);\n    // }\n\n    return texsize;\n}\n\nvoid main() {\n    float pi = 3.141592653589793238462;\n    v_highflag = a_highflag;\n\n    if(a_visible != 0. && u_zoom >= a_zooms.x && u_zoom <= a_zooms.y) {\n        // 绘制背景 || border\n        if(a_type == 6. || a_type == 8.) {\n            mat4 originMatirx = mat4(1.0);\n            mat4 scaleMatrix = scaleMat4(originMatirx, vec3(u_transform.z));\n            mat4 rotateMatrix = rotateMat4(originMatirx, a_angle + u_transform.x, vec3(0, 0, 1));\n            vec4 trans_position = rotateMatrix * scaleMatrix * vec4(a_vertex.x * u_fontSizeFactor, -a_vertex.y * u_fontSizeFactor, 0, 1);\n\n            vec4 trans_normalize_position = vec4(trans_position.xy / u_gl_size.xy * 2., 0, 1);\n\n            vec4 cur_position = u_matrix * vec4(a_origin.xy + u_delta_center + u_offset, a_height, 1);\n\n            //gl_Position=cur_position/cur_position.w+vec4(a_vertex.x/u_gl_size.x*2.*u_fontSizeFactor, -a_vertex.y/u_gl_size.y*2.*u_fontSizeFactor, 0, 0);\n            // 修复文字超出 z 轴范围没有裁剪的问题，现象就是在地图最大级别、pitch 最大的时候出现文字（不应该在地图中的）悬浮在高空中。\n            gl_Position = vec4(cur_position.xy / cur_position.w + trans_normalize_position.xy / trans_normalize_position.w, cur_position.z / cur_position.w, 1);\n\n            v_type = a_type;\n            v_pos = gl_Position.xy;\n        } else if(a_type == 1.) {\n            // 绘制贴地效果文字\n            gl_Position = u_matrix * vec4(a_vertex.xy * u_fontSizeFactor, 0, 1);\n        } else {\n            // 绘制 billborad 效果\n            mat4 originMatirx = mat4(1.0);\n            mat4 scaleMatrix = scaleMat4(originMatirx, vec3(u_transform.z));\n            mat4 rotateMatrix = rotateMat4(originMatirx, a_angle + u_transform.x, vec3(0, 0, 1));\n            vec4 trans_position = rotateMatrix * scaleMatrix * vec4(a_vertex.x * u_fontSizeFactor, -a_vertex.y * u_fontSizeFactor, 0, 1);\n\n            vec4 trans_normalize_position = vec4(trans_position.xy / u_gl_size.xy * 2., 0, 1);\n\n            vec4 cur_position = u_matrix * vec4(a_origin.xy + u_delta_center + u_offset, a_height, 1);\n\n            // 修复文字超出 z 轴范围没有裁剪的问题，现象就是在地图最大级别、pitch 最大的时候出现文字（不应该在地图中的）悬浮在高空中。\n            gl_Position = vec4(cur_position.xy / cur_position.w + trans_normalize_position.xy / trans_normalize_position.w, cur_position.z / cur_position.w, 1);\n        }\n\n        v_texcoord = a_texcoord / getSizeFromArray(u_texsize, int(a_texIndex));\n        v_texIndex = a_texIndex;\n        v_type = floor(a_type + 0.1);\n        v_pos = gl_Position.xy;\n    } else {\n        v_type = -1.0;\n    }\n}";
    var labelsLayerFragmentString = "// precision mediump float;\nprecision highp float;\nuniform sampler2D u_texture[_TotalTextureLen];\nuniform vec4 u_color;\nuniform vec4 u_borderColor;\nuniform vec4 u_strokeColor;\nuniform vec4 u_backgroundColor;\nuniform float u_borderBuffer;\nuniform float u_buffer;\nuniform float u_gamma;\nuniform lowp int u_event;\nuniform float u_zoom;\nuniform lowp float u_opacity;\nuniform lowp float u_skyHeight;\nuniform lowp vec3 u_hlColorFactor;\nuniform vec4 u_highlightFillColor;\nuniform vec4 u_highlightStrokeColor;\n\n\nvarying vec2 v_texcoord;\nvarying float v_texIndex;\nvarying float v_type;\nvarying vec4 v_color;\nvarying vec2 v_pos;\nvarying float v_highflag;\n\nfloat skyOpacity() {\n    if (v_pos.y > u_skyHeight){\n        return 1.0 - smoothstep(u_skyHeight, u_skyHeight + (1.0 - u_skyHeight) / 6.0, v_pos.y);\n    }\n    return 1.0;\n}\n\n/*由于片元着色器的数据index必须是常量，只能如此取出相应的值*/\n\nvoid main() {\n    // gl_FragColor = vec4(1.0, 0, 0, 1.);\n    // return;\n    \n    // texture5\n    int texIdx = int(floor(v_texIndex + 0.1));\n\n    vec4 distColor;\n    if (texIdx == 0) { \n        distColor = texture2D(u_texture[0], v_texcoord); \n    } else if(texIdx == 1){ \n        distColor = texture2D(u_texture[1], v_texcoord); \n    }\n\n    if (v_type == 6.0){\n        // bg\n        gl_FragColor = vec4(u_backgroundColor.rgb, u_backgroundColor.a * u_opacity);\n        gl_FragColor.a *= skyOpacity();\n    } else if (v_type == 8.0){\n        // textborder\n        gl_FragColor = vec4(u_borderColor.rgb, u_borderColor.a * u_opacity);\n        // gl_FragColor.r*= mix(1.0,u_hlColorFactor.r,step(v_highflag,0.1));\n        // gl_FragColor.g*= mix(1.0,u_hlColorFactor.g,step(v_highflag,0.1));\n        // gl_FragColor.b*= mix(1.0,u_hlColorFactor.b,step(v_highflag,0.1));\n        gl_FragColor.a *= skyOpacity();\n    } else if(v_type == 0.0){\n        // icon\n        // gl_FragColor = vec4(0,0,1.0,0.2);\n        // return;\n        float opacity = u_opacity * distColor.a;\n        if (opacity < 0.03){\n            discard;\n        }\n        gl_FragColor = vec4(distColor.rgb, opacity);\n        // gl_FragColor.g*= mix(1.0,u_hlColorFactor.r,step(v_highflag,0.1));\n        // gl_FragColor.g*= mix(1.0,u_hlColorFactor.g,step(v_highflag,0.1));\n        // gl_FragColor.b*= mix(1.0,u_hlColorFactor.b,step(v_highflag,0.1));\n        // gl_FragColor = vec4(distColor.rgb, u_opacity * distColor.a);\n        gl_FragColor.a *= skyOpacity();\n    }else if(v_type==2.0){\n        // text\n        float dist;\n        float alpha;\n\n        // 后端加载纹理 rgba => [[0, 255], [0, 255], [0, 255], [0, 1]]\n        // 前端加载纹理 rgba => [[0, 0, 0, [0, 1]]\n        if (distColor.r != 0.){\n            dist = distColor.r;\n        } else if (distColor.a == 1.0){\n            dist = 0.0;\n        } else {\n            dist = distColor.a;\n        }\n\n        //    gl_FragColor = vec4(dist, dist, dist, 1);\n        //    return;\n        //    dist = 255.0;\n       vec4 color = mix(u_color,u_highlightFillColor,step(0.1,v_highflag));\n       vec4 stokeColor =mix(u_strokeColor,u_highlightStrokeColor,step(0.1,v_highflag));\n        if (u_borderBuffer == 0.0){\n            alpha = smoothstep(u_buffer - u_gamma, u_buffer, dist);\n            gl_FragColor = vec4(color.rgb, alpha * color.a * u_opacity);\n        } else {\n            if (dist <= u_buffer - u_gamma){\n                // border 与外界交界\n                alpha = smoothstep(u_borderBuffer - u_gamma, u_borderBuffer, dist);\n                float opacity = alpha * stokeColor.a * u_opacity;\n                if (opacity == 0.0){\n                    // gl_FragColor = vec4(1.0,0,0,0.1);\n                    // return;\n                    discard;\n                }\n                gl_FragColor = vec4(stokeColor.rgb, opacity);\n                // gl_FragColor = vec4(0,0,1.0,0.2);\n            } else if (dist < u_buffer){\n                // 文字与 border 交界\n                alpha = smoothstep(u_buffer - u_gamma, u_buffer, dist);\n                gl_FragColor = vec4(alpha * color.rgb + (1.0 - alpha) * stokeColor.rgb, 1.0 * color.a *\n                alpha * u_opacity + (1.0 - alpha) * stokeColor.a * u_opacity);\n            } else {\n                // 文字\n                alpha = 1.0;\n                gl_FragColor = vec4(color.rgb, alpha * color.a * u_opacity);\n                // gl_FragColor = vec4(1.0, 0, 0, 1.);\n            }\n        }\n        // gl_FragColor.r*= 1.0;\n        // gl_FragColor.g*=1.0;\n        // gl_FragColor.b*= 1.0;\n        // gl_FragColor.r*= mix(1.0,u_hlColorFactor.r,step(0.1,v_highflag));\n        // gl_FragColor.g*= mix(1.0,u_hlColorFactor.g,step(0.1,v_highflag));\n        // gl_FragColor.b*= mix(1.0,u_hlColorFactor.b,step(0.1,v_highflag));\n\n        // if(v_highflag>0.1){\n        //     gl_FragColor.r*= mix(1.0,u_hlColorFactor.r,step(v_highflag,0.1));\n        //     gl_FragColor.g*=u_hlColorFactor.g;\n        //     gl_FragColor.b*=u_hlColorFactor.b;\n        // } \n        gl_FragColor.a *= skyOpacity();\n\n    }else {\n        discard;\n    }\n   \n}\n";
    var labelsLayerUniforms = {
        u_gl_size: "vec2",
        u_matrix: "mat4",
        u_texsize: "float[]",
        u_texture: "sampler2D[]",
        u_event: "int",
        u_zoom: "float",
        u_color: "vec4",
        u_borderColor: "vec4",
        u_strokeColor: "vec4",
        u_backgroundColor: "vec4",
        u_borderBuffer: "float",
        u_buffer: "float",
        u_gamma: "float",
        u_opacity: "float",
        u_delta_center: "vec2",
        u_offset: "vec2",
        u_transform: "vec4",
        u_skyHeight: "float",
        u_fontSizeFactor: "float",
        u_hlColorFactor: "vec3",
        u_highlightFillColor: "vec4",
        u_highlightStrokeColor: "vec4"
    };
    var labelsLayerAttributes = {
        a_vertex: {
            Oe: "vec2",
            Me: "float32"
        },
        a_texcoord: {
            Oe: "vec2",
            Me: "float32"
        },
        a_origin: {
            Oe: "vec2",
            Me: "float32"
        },
        a_zooms: {
            Oe: "vec2",
            Me: "float32"
        },
        a_height: {
            Oe: "float",
            Me: "float32"
        },
        a_angle: {
            Oe: "float",
            Me: "float32"
        },
        a_type: {
            Oe: "float",
            Me: "float32"
        },
        a_texIndex: {
            Oe: "float",
            Me: "float32"
        },
        a_visible: {
            Oe: "float",
            Me: "uint8"
        },
        a_highflag: {
            Oe: "float",
            Me: "float32"
        }
    };
    var labelsLayer = {
        uniforms: labelsLayerUniforms,
        attributes: labelsLayerAttributes,
        vertexSource: labelsLayerVertexString,
        fragmentSource: labelsLayerFragmentString
    };
    var combineTextVertexString = "precision highp float;\n\nattribute vec2 a_targetTexturePos;\nattribute vec2 a_sourceTexturePos;\n\nuniform vec2 u_targetTextureSize;\nuniform vec2 u_sourceTextureSize;\n\nvarying vec2 v_textureCoord;\n\n\nvoid main() {\n  v_textureCoord = a_sourceTexturePos / u_sourceTextureSize;\n  // v_textureCoord = vec2(a_sourceTexturePos.x / u_sourceTextureSize.x, a_sourceTexturePos.y / u_sourceTextureSize.y);\n\n  vec2 pos = (a_targetTexturePos / u_targetTextureSize) *2.0 -vec2(1.0,1.0);\n  // vec2 pos = vec2(a_targetTexturePos.x / u_targetTextureSize.x, a_targetTexturePos.y / u_targetTextureSize.y);\n  // pos.x = (pos.x - 0.5)*2.0;\n  // pos.y = (pos.y - 0.5)*2.0;\n\n  gl_Position = vec4(pos, 0, 1.0);\n  // gl_Position = vec4(a_targetTexturePos,0,1.0);\n}";
    var combineTextFragmentString = "precision highp float;\n\nuniform sampler2D u_sourceTexture; \n\nvarying vec2 v_textureCoord;\n\n\nvoid main(){\n  gl_FragColor = texture2D(u_sourceTexture, v_textureCoord);\n  // gl_FragColor = vec4(1,0, 0, 0 ,1.0);\n    // gl_FragColor = vec4(0,0,1.0,1.0);\n}";
    var combineTextUniforms = {
        u_sourceTexture: "sampler2D",
        u_sourceTextureSize: "vec2",
        u_targetTextureSize: "vec2"
    };
    var combineTextAttributes = {
        a_targetTexturePos: {
            Oe: "vec2",
            Me: "uint16"
        },
        a_sourceTexturePos: {
            Oe: "vec2",
            Me: "uint16"
        }
    };
    var combineText = {
        uniforms: combineTextUniforms,
        attributes: combineTextAttributes,
        vertexSource: combineTextVertexString,
        fragmentSource: combineTextFragmentString
    };
    var debugImageVertexString = "precision highp float;\n\nattribute vec2 a_pos;\nattribute vec2 a_coord;\n\nvarying vec2 v_coord;\n\nvoid main() {\n  v_coord = vec2(a_coord.x,1.0 - a_coord.y) ;\n  vec2 pos = a_pos *2.0 -vec2(1.0,1.0);\n  // vec2 pos = a_pos -vec2(,1.0);\n  gl_Position = vec4(pos,0,1.0);\n}";
    var debugImageFragmentString = "precision highp float;\n\nvarying vec2 v_coord;\n\nuniform sampler2D u_texture;\n\nvoid main(){\n  gl_FragColor = texture2D(u_texture, v_coord);\n  // gl_FragColor = vec4(1.0,0,0,1.0);\n}\n";
    var debugImageUniforms = {
        u_texture: "sampler2D"
    };
    var debugImageAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "uint16"
        },
        a_coord: {
            Oe: "vec2",
            Me: "uint16"
        }
    };
    var debugImage = {
        uniforms: debugImageUniforms,
        attributes: debugImageAttributes,
        vertexSource: debugImageVertexString,
        fragmentSource: debugImageFragmentString
    };
    var circleFragmentString = "\n\nprecision highp float;\n#define GLSLIFY 1\nuniform  float u_radius;\nuniform  vec2 u_range;\nuniform highp vec4 u_color;\nuniform highp vec4 u_borderColor;\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\n\nvarying vec4 v_pos;\n\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n  float offset = distance(gl_PointCoord, vec2(0.5,0.5))*2.0;\n  float flag = step(1.0,offset);\n  float borderFlag = 1.- smoothstep(1.0 -1./u_radius, 1.0, offset);\n  if(flag > 0.0){\n    discard;\n  }\n  float opacity_t = 1.-smoothstep(u_range[1] -1./u_radius, u_range[1], offset);\n  gl_FragColor = mix(u_borderColor, u_color, opacity_t);\n  gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight)*borderFlag;\n}";
    var circleVertextString = "precision highp float;\n#define GLSLIFY 1\nuniform vec2 u_pos;\nuniform mat4 u_matrix;\nuniform float u_radius;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nuniform float u_retinaRatio;\nvarying vec4 v_pos;\n\nvoid main(){\n    vec2 pos=vec2(u_pos.x+u_localDeltaCenter.x+u_offset.x,u_pos.y+u_localDeltaCenter.y+u_offset.y);\n    gl_Position=u_matrix * vec4(pos,0,1);\n    gl_PointSize = u_radius * u_retinaRatio * 2.0;\n    v_pos = gl_Position;\n}\n";
    var circleAttrVertextString = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nuniform mat4 u_matrix;\nuniform float u_radius;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nuniform float u_retinaRatio;\nvarying vec4 v_pos;\n\nvoid main(){\n    vec2 pos=vec2(a_pos.x+u_localDeltaCenter.x+u_offset.x,a_pos.y+u_localDeltaCenter.y+u_offset.y);\n    gl_Position=u_matrix * vec4(pos,0,1);\n    gl_PointSize = u_radius * u_retinaRatio * 2.0;\n    v_pos = gl_Position;\n}\n";
    var circleUniforms = {
        u_pos: "vec2",
        u_color: "vec4",
        u_borderColor: "vec4",
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_offset: "vec2",
        u_radius: "float",
        u_range: "vec2",
        u_retinaRatio: "float",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var circleAttributes = {};
    var circle = {
        uniforms: circleUniforms,
        attributes: circleAttributes,
        vertexSource: circleVertextString,
        fragmentSource: circleFragmentString
    };
    var circleAttrUniforms = {
        u_color: "vec4",
        u_borderColor: "vec4",
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_offset: "vec2",
        u_radius: "float",
        u_range: "vec2",
        u_retinaRatio: "float",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var circleAttrAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        }
    };
    var circleAttr = {
        uniforms: circleAttrUniforms,
        attributes: circleAttrAttributes,
        vertexSource: circleAttrVertextString,
        fragmentSource: circleFragmentString
    };
    var pointFragmentString = "\n\nprecision highp float;\n#define GLSLIFY 1\nuniform float u_skyHeight;\nuniform float u_viewHeight; // 地图容器高度，单位像素\nuniform bool u_pick;\n\nvarying vec4 v_pos;\nvarying float v_radius;\nvarying float v_visible;\nvarying float v_borderWidth;\nvarying vec4 v_color;\nvarying vec4 v_borderColor;\n// varying vec4 v_pickColor;\n\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n  float offset = distance(gl_PointCoord, vec2(0.5,0.5))*2.0;\n  float flag = step(1.0,offset);\n  float borderFlag = 1.- smoothstep(1.0 - 1. / v_radius, 1.0, offset);\n  if(flag > 0.0){\n    discard;\n  }\n  float range = v_radius / (v_radius + v_borderWidth);\n  float opacity_t = 1. - smoothstep(range - 1. / v_radius, range, offset);\n  gl_FragColor = mix(v_borderColor, v_color, opacity_t);\n  gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight) * borderFlag * v_visible;\n\n  // if(u_pick) {\n  //   gl_FragColor = vec4(v_pickColor.xyz / 256.0, 1);\n  // }\n}";
    var pointVertextString = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nattribute float a_radius;\nattribute float a_borderWidth;\nattribute float a_visible;\nattribute vec4 a_color;\nattribute vec4 a_borderColor;\nattribute vec4 a_pickColor;\n\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\nuniform vec2 u_offset;\nuniform float u_retinaRatio;\n\nvarying vec4 v_pos;\nvarying float v_radius;\nvarying float v_visible;\nvarying float v_borderWidth;\nvarying vec4 v_color;\nvarying vec4 v_borderColor;\nvarying vec4 v_pickColor;\n\nvoid main() {\n    vec2 pos = vec2(a_pos.x + u_localDeltaCenter.x + u_offset.x,a_pos.y+u_localDeltaCenter.y+u_offset.y);\n    gl_Position = u_matrix * vec4(pos,0,1);\n    gl_PointSize = (a_radius + a_borderWidth) * u_retinaRatio * 2.0;\n    v_pos = gl_Position;\n    v_radius = a_radius;\n    v_borderWidth = a_borderWidth;\n    v_color = a_color;\n    v_borderColor = a_borderColor;\n    v_visible = a_visible;\n    v_pickColor = a_pickColor;\n}\n";
    var pointUniforms = {
        u_matrix: "mat4",
        u_localDeltaCenter: "vec2",
        u_offset: "vec2",
        u_retinaRatio: "float",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var pointAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        },
        a_radius: {
            Oe: "float",
            Me: "float32"
        },
        a_borderWidth: {
            Oe: "float",
            Me: "float32"
        },
        a_color: {
            Oe: "vec4",
            Me: "float32"
        },
        a_borderColor: {
            Oe: "vec4",
            Me: "float32"
        },
        a_visible: {
            Oe: "float",
            Me: "float32"
        }
    };
    var point = {
        uniforms: pointUniforms,
        attributes: pointAttributes,
        vertexSource: pointVertextString,
        fragmentSource: pointFragmentString
    };
    var lineFragmentString$2 = "precision mediump float;\n#define GLSLIFY 1\n\nuniform vec4 u_color;\nuniform vec4 u_outLinecolor;\n// uniform float u_opacity;\nuniform vec3 u_dash;\nuniform int u_dashType;\nuniform sampler2D u_texture;\nuniform  mediump float u_width;\n\nvarying highp float v_distance;\nvarying float v_width;\nvarying vec2 v_normal;\nvarying float v_flag;\nvarying vec4 v_pos;\n\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n    // vec2 icon_offset = vec2(80.0, 0.0);\n    // vec2 icon_size = vec2(40.0, 40.0);\n    // vec2 icon_offset = vec2(0.0, 0.0);\n    // vec2 icon_size = vec2(44.0, 32.0);\n    // vec2 texture_size = vec2(512.0, 2048.0);\n    float offset = 0.0;\n    if( u_dashType == 2 ){\n        float offset = mod(v_distance, u_dash.r + u_dash.g);\n        if(offset>u_dash.r && offset < u_dash.r + u_dash.g){\n            discard;\n        }\n    }else if(u_dashType == 3) {\n        float all = u_dash[0] * 2.0 + u_dash[1] * 2.0 + u_dash[2] * 2.0;\n        offset = mod(v_distance, all);\n        vec3 solidBound = vec3(u_dash[0], u_dash[0] + u_dash[1] + u_dash[2], u_dash[0] * 2.0 + u_dash[1] * 2.0 + u_dash[2]);\n        vec3 dashBound = vec3(solidBound.x + u_dash[1], solidBound.y + u_dash[0], solidBound.z + u_dash[2]);\n\n        if (offset > solidBound.x && offset <= dashBound.x\n            || offset > solidBound.y && offset <= dashBound.y\n            || offset > solidBound.z && offset <= dashBound.z) {\n            discard;\n        }\n    }\n\n    float startBlur = 0.0;\n    float endBlur = 1.0;\n    // float dist = length(v_normal) * 0.8;\n\n    // if(v_width < 4.0 && u_border == 1.0) {\n    //     startBlur = 0.0;\n    // } else {\n    //     // 线主体\n    //     if(v_width < 4.0 && u_border == 0.0) {\n    //         startBlur = 0.0;\n    //     } else {\n    //         startBlur = 0.4;\n    //     }\n    // }\n    // startBlur = clamp(0.0, 0.4, smoothstep(0.0, 4.0, v_width) - 0.6);\n\n    // startBlur = 0.0;\n    // float opacity = 1.0 - smoothstep(startBlur, endBlur, dist);\n\n    gl_FragColor = u_color;\n    gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight);\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var lineVertextString$2 = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nattribute vec2 a_normal;\nattribute float a_distance;\nattribute vec2 a_dir;\n\n// attribute vec4 a_color;\n// attribute float a_width;\n// attribute vec4 a_dash;\n\nuniform float u_meter_per_pixel;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\n\nuniform  mediump float u_width;\nuniform vec2 u_offset;  // polygin 拖动平移的时候使用\nvarying float v_distance;\nvarying float v_width;\nvarying vec2 v_normal;\nvarying float v_flag;\nvarying vec4 v_pos;\n\n#define WORLD_SIZE 20037508.342789244\n\nvoid main(){\n\n    float width = u_width;\n    float cosValue = dot(a_normal , a_dir);\n    float sinValue = cross(vec3(normalize(a_normal),0.0), vec3(a_dir,0.0))[2];\n    sinValue = step(0.0, sinValue);\n    sinValue = (sinValue - 0.5)* 2.0;\n    v_distance = a_distance/u_meter_per_pixel + (cosValue * width *0.5);\n\n    vec2 pos = a_pos;\n    pos.x+=u_localDeltaCenter.x + u_offset.x;\n    pos.y+=u_localDeltaCenter.y + u_offset.y;\n//    gl_PointSize = 3.0;\n//    gl_Position = u_matrix * vec4(pos, 0, 1);\n    gl_Position = u_matrix * vec4(pos + a_normal * width * u_meter_per_pixel * 0.5, 0, 1);\n    v_width = width;\n    v_normal = normalize(a_normal * sqrt(1.0 - pow(cosValue, 2.0)));\n    v_flag = sinValue;\n    v_pos = gl_Position;\n\n}";
    var linePatternFragmentString = "precision mediump float;\n#define GLSLIFY 1\n\nuniform sampler2D u_texture;\nuniform  highp vec2 u_iconsize;\nuniform  mediump vec3 u_dir_color;\nuniform  bool u_custom_img_flag;\nuniform  highp float u_width;\nuniform highp float u_meter_per_pixel;\nuniform highp float u_meter_per_pixel_optimal;\n\nvarying highp float v_distance;\n// varying float v_width;\n// varying vec2 v_normal;\nvarying highp float v_flag;\nvarying vec4 v_pos;\n\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\n\nuniform float u_timer;\nuniform float u_speed;\nuniform float u_size_scale;\nuniform bool u_animate;\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n    // if(abs(v_flag)>=1.0){\n    //     discard;\n    // }\n    vec2 icon_size = u_iconsize * vec2(u_width/u_iconsize.y, 1.0 );\n    float offset = 0.0;\n    float ani = u_animate ? u_timer / 1000.0 : 0.0;\n    float text_offset_x = mod(v_distance - ani * u_speed, (icon_size.x*4.0*u_size_scale)*u_meter_per_pixel_optimal/u_meter_per_pixel);\n    // float text_offset_x = mod(v_distance - ani * u_speed, (icon_size.x*4.0)*u_meter_per_pixel_optimal/u_meter_per_pixel);\n\n    // float text_offset_x = mod(v_distance, 120.0);\n\n    if(text_offset_x<=0.0){\n        discard;\n    }\n    if(text_offset_x>=icon_size.x){\n        discard;\n    }\n\n    text_offset_x = 1.0 - clamp(0.0,1.0,text_offset_x/icon_size.x);\n\n    // float  text_offset_y  = (v_flag+1.0)/2.0  ;\n    float  text_offset_y  = v_flag  ;\n\n    if(u_custom_img_flag){\n        gl_FragColor = texture2D(u_texture, vec2(text_offset_y, text_offset_x));\n    }else{\n        vec4 color = texture2D(u_texture, vec2(text_offset_y,text_offset_x));\n        if(color.a>0.0){\n            gl_FragColor = vec4(u_dir_color,1.0);\n        }\n    }\n   gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight);\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var linePatternVertextString = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nattribute vec2 a_normal;\nattribute float a_distance;\nattribute vec2 a_dir;\n\n// attribute vec4 a_color;\n// attribute float a_width;\n// attribute vec4 a_dash;\n\nuniform highp float u_meter_per_pixel;\nuniform highp float u_meter_per_pixel_optimal;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\n\nuniform  highp float u_width;\nuniform vec2 u_offset;  // polygin 拖动平移的时候使用\nvarying highp float v_distance;\n// varying mediump float v_width;\n// varying vec2 v_normal;\nvarying highp float v_flag;\nvarying vec4 v_pos;\n\n#define WORLD_SIZE 20037508.342789244\n\nvoid main(){\n\n    float width = u_width + 0.0;\n    float cosValue = dot(a_normal , a_dir);\n    float sinValue = cross(vec3(normalize(a_normal),0.0), vec3(a_dir,0.0))[2];\n    sinValue = step(0.0, sinValue);\n    // sinValue = (sinValue - 0.5)* 2.0;\n    v_distance = a_distance/u_meter_per_pixel + (cosValue * width *0.5);\n\n    vec2 pos = a_pos;\n    pos.x+=u_localDeltaCenter.x + u_offset.x;\n    pos.y+=u_localDeltaCenter.y + u_offset.y;\n//    gl_PointSize = 3.0;\n//    gl_Position = u_matrix * vec4(pos, 0, 1);\n    gl_Position = u_matrix * vec4(pos + a_normal * width * u_meter_per_pixel * 0.5, 0, 1);\n    // v_width = width;\n    // v_normal = normalize(a_normal * sqrt(1.0 - pow(cosValue, 2.0)));\n    v_flag = sinValue;\n    v_pos = gl_Position;\n\n}";
    var lineGradinetFragmentString = "precision mediump float;\n#define GLSLIFY 1\n\nuniform vec4 u_outLinecolor;\n// uniform float u_opacity;\nuniform vec3 u_dash;\nuniform int u_dashType;\nuniform sampler2D u_texture;\nuniform  mediump float u_width;\nuniform  highp float u_total_distance;\n\nvarying highp float v_distance;\nvarying vec4 v_pos;\n\nuniform float u_skyHeight;\nuniform float u_viewHeight;  // 地图容器高度，单位像素\nfloat fogcalc(vec4 pos, float skyHeight, float viewHeight) {\n    float y = pos.y / pos.w;\n    float fogHeight = 2. / viewHeight * 30.;\n    float fogFactor = 1.0;\n    if(skyHeight < 1.0 && y > 0.0) {\n        fogFactor = smoothstep(skyHeight + fogHeight/2.0, skyHeight - fogHeight/2.0, y);\n    }\n    return fogFactor;\n}\n\nvoid main() {\n\n    float offset = 0.0;\n    if( u_dashType == 2 ){\n        float offset = mod(v_distance, u_dash.r + u_dash.g);\n        if(offset>u_dash.r && offset < u_dash.r + u_dash.g){\n            discard;\n        }\n    }else if(u_dashType == 3) {\n        float all = u_dash[0] * 2.0 + u_dash[1] * 2.0 + u_dash[2] * 2.0;\n        offset = mod(v_distance, all);\n        vec3 solidBound = vec3(u_dash[0], u_dash[0] + u_dash[1] + u_dash[2], u_dash[0] * 2.0 + u_dash[1] * 2.0 + u_dash[2]);\n        vec3 dashBound = vec3(solidBound.x + u_dash[1], solidBound.y + u_dash[0], solidBound.z + u_dash[2]);\n\n        if (offset > solidBound.x && offset <= dashBound.x\n            || offset > solidBound.y && offset <= dashBound.y\n            || offset > solidBound.z && offset <= dashBound.z) {\n            discard;\n        }\n    }\n    float texture_x = v_distance/u_total_distance;\n    gl_FragColor = texture2D(u_texture, vec2(texture_x,0.5));\n    gl_FragColor.a *= fogcalc(v_pos, u_skyHeight, u_viewHeight);\n    #ifdef OVERDRAW_INSPECTOR\n        gl_FragColor = vec4(1.0);\n    #endif\n}\n";
    var lineGradientVertextString = "precision highp float;\n#define GLSLIFY 1\nattribute vec2 a_pos;\nattribute vec2 a_normal;\nattribute float a_distance;\nattribute vec2 a_dir;\n\n// attribute vec4 a_color;\n// attribute float a_width;\n// attribute vec4 a_dash;\n\nuniform float u_meter_per_pixel;\nuniform mat4 u_matrix;\nuniform vec2 u_localDeltaCenter;\n\nuniform  mediump float u_width;\nuniform vec2 u_offset;  // polygin 拖动平移的时候使用\n\nvarying float v_distance;\nvarying float v_width;\nvarying vec2 v_normal;\nvarying float v_flag;\nvarying vec4 v_pos;\n\n#define WORLD_SIZE 20037508.342789244\n\nvoid main(){\n\n    float width = u_width;\n    float cosValue = dot(a_normal , a_dir);\n\n    v_distance = a_distance + (cosValue * width *0.5)*u_meter_per_pixel;\n    // v_distance = a_distance;\n\n    vec2 pos = a_pos;\n    pos.x+=u_localDeltaCenter.x + u_offset.x;\n    pos.y+=u_localDeltaCenter.y + u_offset.y;\n    gl_Position = u_matrix * vec4(pos + a_normal * width * u_meter_per_pixel * 0.5, 0, 1);\n    v_pos = gl_Position;\n}";
    var lineUniforms$1 = {
        u_width: "float",
        u_dashType: "int",
        u_matrix: "mat4",
        u_offset: "vec2",
        u_meter_per_pixel: "float",
        u_color: "vec4",
        u_dash: "vec3",
        u_localDeltaCenter: "vec2",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var lineAttributes$1 = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        },
        a_normal: {
            Oe: "vec2",
            Me: "float32"
        },
        a_distance: {
            Oe: "float",
            Me: "float32"
        },
        a_dir: {
            Oe: "vec2",
            Me: "float32"
        }
    };
    var linePatternUniforms = {
        u_width: "float",
        u_matrix: "mat4",
        u_offset: "vec2",
        u_meter_per_pixel: "float",
        u_meter_per_pixel_optimal: "float",
        u_localDeltaCenter: "vec2",
        u_texture: "sampler2D",
        u_iconsize: "vec2",
        u_dir_color: "vec3",
        u_custom_img_flag: "bool",
        u_skyHeight: "float",
        u_viewHeight: "float",
        u_timer: "float",
        u_animate: "bool",
        u_speed: "float",
        u_size_scale: "float"
    };
    var linePatternAttributes = {
        a_pos: {
            Oe: "vec2",
            Me: "float32"
        },
        a_normal: {
            Oe: "vec2",
            Me: "float32"
        },
        a_distance: {
            Oe: "float",
            Me: "float32"
        },
        a_dir: {
            Oe: "vec2",
            Me: "float32"
        }
    };
    var lineGradientUniforms = {
        u_width: "float",
        u_matrix: "mat4",
        u_offset: "vec2",
        u_meter_per_pixel: "float",
        u_localDeltaCenter: "vec2",
        u_texture: "sampler2D",
        u_total_distance: "float",
        u_skyHeight: "float",
        u_viewHeight: "float"
    };
    var polyline = {
        uniforms: lineUniforms$1,
        attributes: lineAttributes$1,
        vertexSource: lineVertextString$2,
        fragmentSource: lineFragmentString$2
    };
    var polylinePattern = {
        uniforms: linePatternUniforms,
        attributes: linePatternAttributes,
        vertexSource: linePatternVertextString,
        fragmentSource: linePatternFragmentString
    };
    var polylineGradient = {
        uniforms: lineGradientUniforms,
        attributes: linePatternAttributes,
        vertexSource: lineGradientVertextString,
        fragmentSource: lineGradinetFragmentString
    };
    var maskFragmentString = "precision highp float;\n// uniform float u_opacity;\nuniform vec4 u_color;\n\n// varying vec4 v_color;\n\nvoid main() {\n    gl_FragColor = u_color;\n    // gl_FragColor = vec4(0, 0, 0, 0);\n}";
    var maskVertextString = "precision highp float;\nuniform mat4 u_mvpMatrix;\nuniform vec2 u_deltaCenter;\nattribute vec2 a_pos;\n\nvoid main() {\n    gl_Position = u_mvpMatrix * vec4(a_pos.xy+u_deltaCenter, 0, 1);\n}\n";
    var maskUniforms = {
        u_mvpMatrix: "mat4",
        u_color: "vec4",
        u_deltaCenter: "vec2"
    };
    var maskAttributes = {
        a_pos: {
            Me: "float32",
            Oe: "vec2"
        }
    };
    var mask = {
        uniforms: maskUniforms,
        attributes: maskAttributes,
        vertexSource: maskVertextString,
        fragmentSource: maskFragmentString
    };
    var programs = {
        fill: fill,
        fillPattern: fillPattern,
        sky: sky,
        skyPattern: skyPattern,
        mask: mask,
        fillExtrusion: fillExt,
        fillExtrusionPattern: fillExtPattern,
        raster: raster,
        labelsLayer: labelsLayer,
        fillOutline: fillOutline,
        line: line,
        combineText: combineText,
        debugImage: debugImage,
        circle: circle,
        circleAttr: circleAttr,
        point: point,
        polyline: polyline,
        "polyline-pattern": polylinePattern,
        "polyline-gradient": polylineGradient
    };
    var ProgramManager = function() {
        function e(e) {
            this.context = e;
            this.context = e;
            this.cache = {}
        }
        e.prototype.Un = function() {
            return this.Bn("circle")
        }
        ;
        e.prototype.kee = function() {
            return this.Bn("circleAttr")
        }
        ;
        e.prototype.Mee = function() {
            return this.Bn("point")
        }
        ;
        e.prototype.rS = function() {
            return this.Bn("sky")
        }
        ;
        e.prototype.EB = function() {
            return this.Bn("skyPattern")
        }
        ;
        e.prototype.aS = function() {
            return this.Bn("mask")
        }
        ;
        e.prototype.Wn = function() {
            return this.Bn("fill")
        }
        ;
        e.prototype.aH = function() {
            return this.Bn("fillPattern")
        }
        ;
        e.prototype.k_ = function() {
            return this.Bn("fillExtrusion")
        }
        ;
        e.prototype.sH = function() {
            return this.Bn("fillExtrusionPattern")
        }
        ;
        e.prototype.Gn = function() {
            return this.Bn("raster")
        }
        ;
        e.prototype.Zn = function() {
            var e = this.context;
            var r = e.gl;
            var t = "labelsLayer";
            if (!this.cache[t]) {
                var i = r.getParameter(r.MAX_TEXTURE_IMAGE_UNITS);
                var a = Math.min(i, 16, TextureMaxLength);
                var n = programs[t];
                var o = [];
                for (var f = 0; f < a; f++) {
                    o.push((f > 0 ? "else " : " ") + "if(texIdx==" + f + "){ distColor = texture2D(u_texture[" + f + "], v_texcoord); }")
                }
                n.vertexSource = n.vertexSource.replace(/_TotalTextureLenTwice/g, (a * 2).toString()).replace(/_TotalTextureLen/g, a.toString());
                n.fragmentSource = n.fragmentSource.replace(/_TotalTextureLen/g, a.toString()).replace(/__SmpColorLoop__/g, o.join("\n"))
            }
            return this.Bn(t)
        }
        ;
        e.prototype.Yn = function() {
            return this.Bn("fillOutline")
        }
        ;
        e.prototype.Vn = function() {
            return this.Bn("line")
        }
        ;
        e.prototype.Xn = function() {
            return this.Bn("polyline")
        }
        ;
        e.prototype.M_ = function() {
            return this.Bn("polyline-pattern")
        }
        ;
        e.prototype.sS = function() {
            return this.Bn("polyline-gradient")
        }
        ;
        e.prototype.Hn = function() {
            return this.Bn("combineText")
        }
        ;
        e.prototype.Jn = function() {
            return this.Bn("debugImage")
        }
        ;
        e.prototype.destroy = function() {
            for (var e in this.cache) {
                if (this.cache.hasOwnProperty(e)) {
                    var r = this.cache[e];
                    r.destroy();
                    delete this.cache[e]
                }
            }
        }
        ;
        e.prototype.Bn = function(e) {
            if (this.cache[e]) {
                return this.cache[e]
            }
            var r = programs[e];
            if (r) {
                this.cache[e] = new Program(this.context,r.vertexSource,r.fragmentSource,r.attributes,r.uniforms,e)
            }
            return this.cache[e]
        }
        ;
        return e
    }();
    var Util$8 = AMap["Util"];
    var LandRender = function(r) {
        __extends(e, r);
        function e() {
            var e = r.call(this) || this;
            e.CLASS_NAME = "AMap.Land";
            e.e_ = [0, 0, 0, 0];
            e.Qn = {
                color: [.9882352941176471, .9764705882352941, .9490196078431372, 1]
            };
            return e
        }
        e.prototype.renderFrame = function(e, r, t, i, a) {
            return
        }
        ;
        return e
    }(LayerRender);
    var MapRenderBase = function() {
        function e(e) {
            this.map = e
        }
        e.prototype.lS = function(e, r) {
            var t = this.map.fS();
            var i = t.uS();
            var a = {};
            for (var n in i) {
                if (i.hasOwnProperty(n)) {
                    var o = i[n];
                    var f = r.uo.getData(o.co(), r.viewState, this.context || null);
                    a[n] = f
                }
            }
            t.cS(e, r, a)
        }
        ;
        return e
    }();
    var Support$7 = M["Support"];
    var DomUtil$1 = AMap["DomUtil"];
    var utils$1 = AMap["Util"];
    var scale$4 = Support$7.scale;
    var MapRender = function(t) {
        __extends(e, t);
        function e(e) {
            var r = t.call(this, e) || this;
            r.map = e;
            r.gl = e["getGL"]();
            r.io = r.gl.canvas;
            r.gl.getExtension("OES_standard_derivatives");
            r.gl.getExtension("OES_element_index_uint");
            r.context = new Context(r.gl);
            r.oo = new ProgramManager(r.context);
            r.z_ = e.z_;
            r.R_ = e.R_;
            r.no = new LandRender;
            r.Pee();
            r.uee = r.context.createTexture({
                height: 4096,
                width: 4096,
                data: null
            });
            r.uee.bind(r.context.gl.CLAMP_TO_EDGE, r.context.gl.LINEAR, r.context.gl.LINEAR);
            return r
        }
        e.prototype.getMapState = function() {
            return this.map && this.map.getMapState() || {}
        }
        ;
        e.prototype.Lee = function() {
            return this.map
        }
        ;
        e.prototype.to = function() {
            if (this.map) {
                this.map.setNeedUpdate(true)
            }
        }
        ;
        e.prototype.renderFrame = function(e) {
            if (e.size[0] * scale$4 !== this.io.width) {
                this.io.width = e.size[0] * scale$4;
                this.io.style.width = e.size[0] + "px"
            }
            if (e.size[1] * scale$4 !== this.io.height) {
                this.io.height = e.size[1] * scale$4;
                this.io.style.height = e.size[1] + "px"
            }
            this.context.be.set([0, 0, this.io.width, this.io.height]);
            this.context.clear({
                stencil: true,
                color: true,
                depth: true
            });
            var r = e.viewState.zoom;
            var t = e.layers.sort(function(e, r) {
                if (e.CLASS_NAME === "AMap.MarkLayer") {
                    return 1
                } else {
                    return e.getLayerConfig().zIndex - r.getLayerConfig().zIndex
                }
            });
            this.no.renderFrame(this.oo, this.map.options.backgroundColor, e, {}, this);
            this.map.so.update(this.oo);
            var i = this.map.TL();
            if (!i && this.map.bZ.MZ.gY.Kv) {
                this.uee.update(this.map.bZ.MZ.gY.mY);
                this.map.bZ.MZ.gY.Kv = false
            }
            if (this.map.bZ.MZ.drawMode !== "fast" && (this.map.bZ.dynamic.get("firstLabelDataAllLoaded") || this.map.bZ.dynamic.get("firstLabelLayerDataAllLoaded"))) {
                this.lS(this.oo, e)
            } else if (this.map.bZ.MZ.drawMode === "fast" && (this.map.bZ.dynamic.get("firstLabelDataAllLoaded") || this.map.bZ.dynamic.get("firstLabelLayerDataAllLoaded")) && !i) {
                this.lS(this.oo, e)
            }
            var a;
            var n = true;
            for (var o = 0, f = t.length; o < f; o += 1) {
                a = t[o];
                var s = a.getState();
                var u = a.getLayerConfig().zooms;
                var l = s.opacity > 0;
                if (a.CLASS_NAME === "AMap.IndoorMap") {
                    l = s.opacity >= 0
                }
                if (s.visible && l && utils$1.dS(r, u) || a.CLASS_NAME === "AMap.CustomLayer") {
                    var v = a.getRender();
                    if (v) {
                        if (!a.lo()) {
                            n = false
                        }
                        var c = e.uo.getData(t[o].co(), e.viewState, this.context);
                        if (c) {
                            a.beforeRender();
                            v.renderFrame(this.oo, c, e, s, this, t[o], c.fo);
                            a.afterRender()
                        }
                    }
                }
            }
            if (!this.map.complete && n) {
                this.map.emit("complete");
                this.map.complete = true
            }
            if (this.map["getView"]() && !this.map["getView"]().h_()) {
                this.map.getView().aD();
                this.map.emit("viewchange", {
                    zoom: e.viewState.zoom,
                    center: e.viewState.center,
                    pitch: e.viewState.pitch,
                    rotation: e.viewState.rotation
                })
            }
            if (!this.map.bZ) {
                return
            }
            var h = this.map.bZ.dynamic.get("stamp");
            var d = Date.now();
            this.map.bZ.dynamic.set("stamp", d);
            var _ = Math.floor(1e3 / (d - h));
            this.map.bZ.MZ.tY.update();
            if (_ >= 50) {
                this.map.bZ.MZ.tY.RZ("default")
            }
            if (this.map.bZ.MZ.tY.BZ("default") > 0 || this.map.bZ.MZ.gY.Kv) {
                this.to()
            }
        }
        ;
        e.prototype.destroy = function() {
            if (this.oo && this.oo["destroy"]) {
                this.oo.destroy()
            }
            if (this.R_ && this.R_["destroy"]) {
                this.R_.destroy();
                delete this.R_
            }
            if (this.context) {
                delete this.context
            }
        }
        ;
        e.prototype.Pee = function() {
            var e = this;
            this.map.bZ.MZ.tY.add({
                NZ: function() {
                    e.oo.Hn()
                },
                UZ: function() {
                    return Boolean(e.oo.cache["combineText"])
                },
                group: "default",
                FZ: function() {
                    return 1
                }
            });
            if (this.map["getViewMode_"]() === "3D") {
                this.map.bZ.MZ.tY.add({
                    NZ: function() {
                        e.oo.rS()
                    },
                    UZ: function() {
                        return Boolean(e.oo.cache["sky"])
                    },
                    group: "default",
                    FZ: function() {
                        return 1
                    }
                })
            }
            if (!this.map.yS()) {
                this.map.bZ.MZ.tY.add({
                    NZ: function() {
                        e.oo.Yn()
                    },
                    UZ: function() {
                        return Boolean(e.oo.cache["fillOutline"])
                    },
                    group: "default",
                    FZ: function() {
                        return 1
                    }
                })
            }
            this.map.bZ.MZ.tY.add({
                NZ: function() {
                    e.oo.Wn()
                },
                UZ: function() {
                    return Boolean(e.oo.cache["fill"])
                },
                group: "default",
                FZ: function() {
                    return 1
                }
            });
            this.map.bZ.MZ.tY.add({
                NZ: function() {
                    e.oo.Vn()
                },
                UZ: function() {
                    return Boolean(e.oo.cache["line"])
                },
                group: "default",
                FZ: function() {
                    return 1
                }
            });
            this.map.setNeedUpdate(true)
        }
        ;
        return e
    }(MapRenderBase);
    M.Ree = MapRender;
    M.WebGLRender = MapRender;
    M["LayerRenderManager"]["addLayerRender"]("AMap.NebulaLayer", NebulaRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.Buildings", BuildingRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.Inner.LabelsLayer", TileLabelsLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.LabelsLayer", LabelsLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.ImageLayer", ImageRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.CanvasLayer", ImageRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.RasterLayer", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.Flexible", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.RoadNet", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.Satellite", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.Traffic", VectorTrafficRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.WMTSLayer", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.WMS", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.TileLayer.WMTS", RasterLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.CustomLayer", CustomRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.GLCustomLayer", GLCustomRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.VTLayer", VTRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.SkyLayer", SkyLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.MaskLayer", MaskLayerRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.MapboxLayer", MapboxRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.VectorLayer", OverlayRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.VectorSvgLayer", OverlayRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.IndoorMap", IndoorRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.DistrictLayer", DistrictRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.DistrictLayer.World", DistrictRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.DistrictLayer.Country", DistrictRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.DistrictLayer.Province", DistrictRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.MapboxVectorTileLayer", MapboxVTRender);
    M["LayerRenderManager"]["addLayerRender"]("AMap.MarkLayer", RasterLayerRender)
}
)();
