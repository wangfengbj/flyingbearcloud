define(["angular","angularAMD"], function(angular, angularAMD){
	var app=angular.module(webApp,[]);
	
	app.config(function($controllerProvider, $compileProvider, $filterProvider, $provide) {
		//暴露
		app.register = {
				controller: $controllerProvider.register,
				directive: $compileProvider.directive,
				filter: $filterProvider.register,
				factory: $provide.factory,
				service: $provide.service,
				value: $provide.value,
                constant: $provide.constant
		};
	});
	
	return app;
    //return angularAMD.bootstrap(app);

//    function configure($locationProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
//        app.registerController = $controllerProvider.register;
//        app.registerDirective = $compileProvider.directive;
//        app.registerFilter = $filterProvider.register;
//        app.registerFactory = $provide.factory;
//        app.registerService = $provide.service;
//    }
})