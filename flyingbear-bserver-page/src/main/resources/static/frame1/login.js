// require.js设置url参数，解决缓存问题
require.config({
    urlArgs: 'v=' + version
});

require([ '../js/require-config' ], function(config) {
	require(['jquery', "domready", "bootstrap", "avalon", "moment", "JSON", "flyingBear"], function() {
		let flyingBear = require("flyingBear");
		let avalon=require("avalon");
		avalon.config({debug: false});
		
		
		let login_vm = avalon.define({
            $id: 'login_vm',
            loginid:"",
            loginpwd:"",
            doLogin:function(e){
            	let loginid = login_vm.loginid;
            	if(loginid.trim() == ""){
            		login_vm.loginid="";
            		//flyingBear.alert("warning","提示","账号不能为空");
            		return;
            	}
            	let loginpwd = login_vm.loginpwd;
            	if(loginpwd.trim() == ""){
            		login_vm.loginpwd="";
            		//flyingBear.alert("warning","提示","密码不能为空");
            		return;
            	}
            	//event.stopPropagation(); 阻止了事件冒泡，但不会阻击默认行为
            	//return false;阻止了事件冒泡，也阻止了默认行为
            	//event.preventDefault(); 事件处理过程中，不阻击事件冒泡，但阻击默认行为
            	e.preventDefault();
            	flyingBear.postJsonNoConfirm("/admin/login",{loginId:loginid,loginPwd:loginpwd},null,function(result){
            		if(flyingBear.ajaxJsonHandle(result)){
            			flyingBear.jump("/page/index.html");
            		}
            		return false;
            	},function(result){
            		flyingBear.alert("error","提示","服务异常，请稍后再试");
            		return false;
            	});
            }
		});
		avalon.scan(document.body);
		//hxflow.openWait();
		//avalon.scan($j('div[ms-controller="videocutlogPage"]')[0]);
	});
});

