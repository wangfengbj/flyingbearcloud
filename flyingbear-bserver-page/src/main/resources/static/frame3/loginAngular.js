// require.js设置url参数
require.config({
    urlArgs: 'v=' + version
});

require([ '../js/require-config' ], function(config) {
	require(['jquery', "domready", "bootstrap", "vue", "moment", "JSON", "flyingBear"], function($j) {
		let flyingBear = require("flyingBear");
		let Vue=require("vue");
		
		//Object.freeze(obj); //这会阻止修改现有的 property，也意味着响应系统无法再追踪变化
		
		let loginVM = new Vue({
			el: '#login_app',
			data: {
				message: 'Hello Angular!',
				loginid: "",
				loginpwd: "",
				rember_pwd: []
			},
			methods:{
				doLogin:function(){
					let loginid = loginVM.loginid;
	            	if(loginid.trim() == ""){
	            		loginVM.loginid="";
	            	}
	            	let loginpwd = loginVM.loginpwd;
	            	if(loginpwd.trim() == ""){
	            		loginVM.loginpwd="";
	            	}
	            	if($j("#login_app_form")[0].checkValidity()){
	            		flyingBear.postJsonNoConfirm("/admin/login",{loginId:loginid,loginPwd:loginpwd},null,function(result){
		            		if(flyingBear.ajaxJsonHandle(result)){
		            			//记住密码
		            			if(loginVM.rember_pwd[0] === "rember"){
		            				let date = new Date();
		                    		date.setTime(date.getTime() + 30*24*60*60*1000);
		            				$j.cookie('loginid',loginid,{expires:date,path: '/'});
		            				$j.cookie('loginpwd',loginpwd,{expires:date,path: '/'});
		            			}
		            			flyingBear.jump("/page/indexA.html");
		            		}
		            	},function(result){
		            		flyingBear.alert("error","提示","连接超时，请稍后再试");
		            	});
	            	}else{
	            		//html5 验证提示
	            		$j("#login_app_form")[0].reportValidity();
	            	}
				}
			},
			created:function(){
				//实例被创建之后执行代码  this
				let loginid = $j.cookie('loginid');
				let loginpwd = $j.cookie('loginpwd');
				if(loginid){
					this.loginid = loginid;
					this.loginpwd = loginpwd;
					this.rember_pwd[0] = "rember";
				}
			},
			mounted:function(){},
			updated:function(){},
			destroyed:function(){}
		})
		
		
//		vm.$data === data // => true
//		vm.$el === document.getElementById('example') // => true
//
//		// $watch 是一个实例方法
//		vm.$watch('a', function (newValue, oldValue) {
//		  // 这个回调将在 `vm.a` 改变后调用
//		})
		
//		<span v-once>这个将不会改变: {{ msg }}</span> 一次性地插值
//		v-html
//		<div v-bind:id="dynamicId"></div> 属性绑定
//		v-bind:href 缩写 :href
//		v-on:click  缩写 @click
	});
});

