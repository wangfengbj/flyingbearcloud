define(['jquery', "angular", "app",  "fyw", "ztree"], function($j, angular, app, fyw, ztree){
	let _handle;
	let _data;
	let _callback;
	
	app.register.controller('userPage', function ($scope) {
		$scope.name = 'Help Info';
		// 页面加载完成后调用
		$scope.$watch('$viewContentLoaded', function() {  
			
		}); 
	});
	
	
    return {
        start: function(){
        	$j('div[ng-controller="userPage"]').injector().invoke(function($compile, $rootScope, $controller) {
                $compile($j('div[ng-controller="userPage"]'))($rootScope);
                $rootScope.$apply();
            });
        },
		setHandle: function(handle) {
	        _handle = handle;
	    },
	    setData : function(data) {
	        _data = data;
	    },
	    setCallback: function(callback) {
	        _callback = callback;
	    }
    }
});