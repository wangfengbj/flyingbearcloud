define(['jquery', "angular", "app",  "fyw", "ztree"], function($j, angular, app, fyw, ztree){
	let _handle;
	let _data;
	let _callback;
	
//	app.registerController('orgPage', orgPage);
//	// 定义 controller 的注入对象；
//	orgPage.$inject = ['$scope'];
//	// controller 具体实现
//	function orgPage($scope) {
//		$scope.name = 'Help Info';
//	}
	
	let orgVM = app.register.controller('orgPage', function ($scope) {
		$scope.name = 'Help Info';
		// 页面加载完成后调用
		$scope.$watch('$viewContentLoaded', function() {  
			$j.fn.zTree.init($j("#orgPage_tree"), {}, [
				{name: "test1", open: true, children: [{ name: "test1_1" }, { name: "test1_2" }]},
		        {name: "test2", open: true, children: [{ name: "test2_1" }, { name: "test2_2" }]}
			]);
		}); 
	});
	
	
    return {
        start: function(){
        	$j('div[ng-controller="orgPage"]').injector().invoke(function($compile, $rootScope, $controller) {
                $compile($j('div[ng-controller="orgPage"]'))($rootScope);
//                $controller('orgPage', {}, true);
//                var el = angular.element("div_id");
//                $scope = el.scope();
//                $injector = el.injector();
//                $injector.invoke(function($compile){
//                   $compile(el)($scope)
//                })
                $rootScope.$apply();
            });
        },
		setHandle: function(handle) {
	        _handle = handle;
	    },
	    setData : function(data) {
	        _data = data;
	    },
	    setCallback: function(callback) {
	        _callback = callback;
	    }
    }
});