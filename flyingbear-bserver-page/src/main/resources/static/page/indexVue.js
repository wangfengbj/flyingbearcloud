//require.js设置url参数
require.config({
    urlArgs: 'v=' + version
});

require([ '../js/require-config' ], function(config) {
	require(['jquery',"domready","bootstrap","vue","moment","JSON", "fyw","vue-router"], function($j) {
		let fyw = require("fyw");
		let Vue=require("vue");
		let VueRouter = require("vue-router");
		//使用路由
		Vue.use(VueRouter);
		
		// 自定义加载方法
		let vueloader = {
				install : function(Vue,options){
					Vue.ImportFrom = function(url,resolve){
						fyw.getHtml(url,{},function(context){
							let result;
							if(context){
								let script  = getTagContext('script',context)
				                let options = eval("("+script+")")
				                options.template = getTagContext('template',context)
				                result = options            
				            }
				            resolve(result);//返回值
						});
			    }
			}
		}
		function getTagContext(tag,str){
		    var reg = new RegExp("<"+tag+">([\\s\\S]*)<\/"+tag+">")
		    var matchs = reg.exec(str)
		    return matchs[1]
		}
		Vue.use(vueloader);
		
		
		const router = new VueRouter({
			mode: 'history',
			routes: []
		});
		
		const originalPush = VueRouter.prototype.push
		VueRouter.prototype.push = function push(location) {
		   return originalPush.call(this, location).catch(err => err)
		}
		
		let loginVM = new Vue({
			el: '#index_app',
			router,
			data: {
				
			},
			methods:{
				openMenu:function(menu_url,menu_name){
					let all_router = this.$router.options.routes;
					let is_have = false;
					for(let i=0;i<all_router.length;i++){
						if(all_router[i].path === menu_url){
							is_have = true;
							break;
						}
					}
					if(!is_have){
						this.$router.addRoutes([{
							path: menu_url,
							name: menu_name,
//							meta: {
//								title: '登录页面',
//								hideInMenu: true
//							},
							component: function(resolve, reject){
								Vue.ImportFrom(menu_url,resolve)
							}
						}]);
					}
					this.$router.push({path: menu_url});//component params: { userId: wise }
					// fyw.openPageInDiv("index_content",menu_url); 
				}
			},
			created:function(){
			},
			mounted:function(){},
			updated:function(){},
			destroyed:function(){}
		})
	});
});

