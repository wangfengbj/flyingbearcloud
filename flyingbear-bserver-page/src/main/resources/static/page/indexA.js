//require.js设置url参数
require.config({
    urlArgs: 'v=' + version
});

require([ '../js/require-config' ], function(config) {
	require(['jquery',"domready","bootstrap","angular", "app", "moment","JSON", "fyw"], function($j) {
		let fyw = require("fyw");
		let angular = require("angular");
		let app = require("app");
		
		
		
		//ng-controller可以嵌套
		app.controller('indexPage', function($scope) {
			$scope.hname="首页";
		    $scope.openMenu = function(menu_url,menu_name){
		    	fyw.openPageInDiv("index_content",menu_url); 
		    }
		});
		
		//页面不要添加ng-app指令，否则会报错，使用require.js时推荐使用angularjs手动加载方式。不使用自动加载方式
		////加载闪烁  ng-cloak解决
		//angular.bootstrap(document,["myApp"]);
		angular.bootstrap("#index_app",[webApp]);
		
		
		//  oclazyload 延时按需加载方式
		// Angular-async-loader 
	});
});

