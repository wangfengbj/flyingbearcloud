package pers.flyingbear;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableAdminServer  // 开启admin服务端
@EnableDiscoveryClient
public class AdminMonitorApplication {
	public static void main(String[] args) {
		SpringApplication.run(AdminMonitorApplication.class, args);
	}
}
